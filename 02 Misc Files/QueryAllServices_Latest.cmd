@echo off
goto menu

:menu
cls
echo -------------------------------------------------------------------------------
echo MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Welcome to the Master Services Management Window.  This tool is used to start
echo and/or stop ALL SKi services throughout the Production Environment.  Please use
echo with extreme caution!
echo.
echo.
echo.
echo.
echo RESTART OPTIONS:
echo.
echo 1 - Restart SKiDocMail (safe to run during production if needed)
echo 2 - Restart SKiQuoteService (safe to run during production if needed)
echo 3 - Restart SKiIDService (ONLY USED WHEN ID CACHE ERRORS RECEIVED!!!!)
echo 4 - Unattended shut down of ALL services (after hours ONLY!)
echo 5 - Unattended start up of ALL services (after hours ONLY!)
echo.
echo.
echo 9 - QUIT
echo.
echo.
set /P C=[1,2,3,4,5,9]?
if "%C%"=="9" goto EXITMENU
if "%C%"=="5" goto UNATTENDEDSTART
if "%C%"=="4" goto UNATTENDEDSTOP
if "%C%"=="3" goto RESTARTIDSERVICE
if "%C%"=="2" goto RESTARTQUOTESERVICE
if "%C%"=="1" goto RESTARTDOCMAIL
goto ERRORSELECTION


:ERRORSELECTION
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo                     ! !   E R R O R   S E L E C T I O N   ! !
echo.
echo                      The selection you entered is incorrect
echo                            Press any key to try again
echo.
echo.
echo.
echo.
echo.
echo.
pause
goto menu


:RESTARTDOCMAIL
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo [Stopping SKiDocMail service ...]
echo.
net Stop SSKiDocMailService
echo.
echo [Querying SKiDocMail service ...]
echo.
net Query SKiDocMailService
echo.
echo [Starting SKiDocMail service ...]
echo.
net Start SKiDocMailService
echo.
echo.
echo.
echo.
echo.
pause
goto menu



:RESTARTQUOTESERVICE
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo [Stopping SKiQuoteService service ...]
echo.
net Stop SKiQuoteService
echo.
echo.
echo [Starting SKiQuoteService service ...]
echo.
net Start SKiQuoteService
echo.
echo.
echo.
echo.
echo.
pause
goto menu


:RESTARTIDSERVICE
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo [Stopping SKiIDService service ...]
echo.
net Stop SKiIDService
echo.
echo.
echo [Starting SKiIDService service ...]
echo.
net Start SKiIDService
echo.
echo.
echo.
echo.
echo.
pause
goto menu


:UNATTENDEDSTOP
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo            ! !   W A R N I N G   W A R N I N G   W A R N I N G   ! !
echo.
echo This process will stop ALL production SKi services EVERYWHERE.  There will be
echo no way to stop this process once started!  Be very sure this is what you want
echo to do.
echo.
echo - Make sure all users are out of SKi
echo - Notify SOURCING for maintenance purposes (firstline@sourcing.co.za)
echo - Be very sure you want to do this
echo.
echo.
echo.
echo 1 - Continue
echo 2 - Exit
echo.
echo.
set /P C=[1,2]?
if "%C%"=="2" goto menu
if "%C%"=="1" goto UNATTENDEDSTOPCONTINUE
goto ERRORSELECTION


:UNATTENDEDSTART
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo            ! !   W A R N I N G   W A R N I N G   W A R N I N G   ! !
echo.
echo DO NOT run this option if you have not yet stopped all services.
echo.
echo - Make sure all users are out of SKi
echo - Notify SOURCING for maintenance purposes (firstline@sourcing.co.za)
echo - Be very sure you want to do this
echo.
echo.
echo.
echo 1 - STOP all services
echo 2 - Only START the Application (114) and Static (118)
echo 3 - START all services
echo 4 - Exit
echo.
echo.
set /P C=[1,2,3,4]?
if "%C%"=="4" goto menu
if "%C%"=="3" goto UNATTENDEDSTARTTHREE
if "%C%"=="2" goto UNATTENDEDSTARTTWO
if "%C%"=="1" goto UNATTENDEDSTOPCONTINUE
goto ERRORSELECTION


:UNATTENDEDSTOPCONTINUE
cls
sc \\10.0.3.116 stop SKiWebService
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo WebServices Environment (116) Stopped ...
echo.
echo.
echo.
echo.
pause
echo sc \\10.0.3.120 stop SKiDocGenService
echo sc \\10.0.3.120 stop SKiSystemService
echo sc \\10.0.3.120 stop SKiRemoteDataService
echo sc \\10.0.3.121 stop SKiDocGenService
echo sc \\10.0.3.121 stop SKiSystemService
echo sc \\10.0.3.121 stop SKiRemoteDataService
echo sc \\10.0.3.122 stop SKiDocGenService
echo sc \\10.0.3.122 stop SKiSystemService
echo sc \\10.0.3.122 stop SKiRemoteDataService
echo sc \\10.0.3.123 stop SKiDocGenService
echo sc \\10.0.3.123 stop SKiSystemService
echo sc \\10.0.3.123 stop SKiRemoteDataService
echo sc \\10.0.3.124 stop SKiDocGenService
echo sc \\10.0.3.124 stop SKiSystemService
echo sc \\10.0.3.124 stop SKiRemoteDataService
sc \\10.0.3.125 stop SKiDocGenService
sc \\10.0.3.125 stop SKiSystemService
sc \\10.0.3.125 stop SKiRemoteDataService
sc \\10.0.3.126 stop SKiDocGenService
sc \\10.0.3.126 stop SKiSystemService
sc \\10.0.3.126 stop SKiRemoteDataService
sc \\10.0.3.127 stop SKiDocGenService
sc \\10.0.3.127 stop SKiSystemService
sc \\10.0.3.127 stop SKiRemoteDataService
sc \\10.0.3.128 stop SKiDocGenService
sc \\10.0.3.128 stop SKiSystemService
sc \\10.0.3.128 stop SKiRemoteDataService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo WebServices Environment (116) Stopped ...
echo Cluster Environment (120 - 128) Stopped ...
echo.
echo.
echo.
echo.
pause
echo sc \\10.0.3.118 Stop SKiDocGenService
echo sc \\10.0.3.118 Stop SKiSystemService
echo sc \\10.0.3.118 Stop SKiRemoteDataService
sc \\10.0.3.171 Stop SKiDocGenService
sc \\10.0.3.171 Stop SKiSystemService
sc \\10.0.3.171 Stop SKiRemoteDataService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo WebServices Environment (116) Stopped ...
echo Cluster Environment (120 - 128) Stopped ...
echo Static Environment (118,171) Stopped ...
echo.
echo.
echo.
echo.
pause
net stop SKiWebService
net stop SKiCashbackService
net stop SKiClaimAggregateService
net stop SKiDocGenService
net stop SKiEventsService
net stop SKiExcel2PDF
net stop SKiICSService
net stop SKiProcessingService
net stop SKiQuoteServiceNet
net stop SKiDocMailService
net stop SKiIDService
net stop SKiMessagingService
net stop SKiQuoteService
net stop SKiRemoteDataService
net stop SKiSubscriptionService
net stop SKiSystemService
net stop SKiSystemServiceNet
net stop SKiSystemConfiguration
net stop SKiTelesureUploadService
net stop SantamYellowboxService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo WebServices Environment (116) Stopped ...
echo Cluster Environment (120 - 128) Stopped ...
echo Static Environment (118,171) Stopped ...
echo Application Environment (114) Stopped ...
echo.
echo Action Complete!
echo.
echo.
echo.
echo.
pause
goto menu


:UNATTENDEDSTARTTHREE
net start SKiSystemConfiguration
net start SKiSystemServiceNet
net start SKiSystemService
net start SKiSubscriptionService
net start SKiRemoteDataService
net start SKiQuoteService
net start SKiMessagingService
net start SKiIDService
net start SKiDocMailService
net start SKiQuoteServiceNet
net start SKiProcessingService
net start SKiICSService
net start SKiExcel2PDF
net start SKiEventsService
net start SKiDocGenService
net start SKiCashbackService
net start SKiClaimAggregateService
net start SKiTelesureUploadService
net start SantamYellowboxService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo.
echo.
echo.
echo.
pause
echo sc \\10.0.3.118 start SKiDocGenService
echo sc \\10.0.3.118 start SKiSystemService
echo sc \\10.0.3.118 start SKiRemoteDataService
sc \\10.0.3.171 start SKiDocGenService
sc \\10.0.3.171 start SKiSystemService
sc \\10.0.3.171 start SKiRemoteDataService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo Static Environment (118,171) Started ...
echo.
echo.
echo.
echo.
pause
echo sc \\10.0.3.120 start SKiDocGenService
echo sc \\10.0.3.120 start SKiSystemService
echo sc \\10.0.3.120 start SKiRemoteDataService
echo sc \\10.0.3.121 start SKiDocGenService
echo sc \\10.0.3.121 start SKiSystemService
echo sc \\10.0.3.121 start SKiRemoteDataService
echo sc \\10.0.3.122 start SKiDocGenService
echo sc \\10.0.3.122 start SKiSystemService
echo sc \\10.0.3.122 start SKiRemoteDataService
echo sc \\10.0.3.123 start SKiDocGenService
echo sc \\10.0.3.123 start SKiSystemService
echo sc \\10.0.3.123 start SKiRemoteDataService
echo sc \\10.0.3.124 start SKiDocGenService
echo sc \\10.0.3.124 start SKiSystemService
echo sc \\10.0.3.124 start SKiRemoteDataService
sc \\10.0.3.125 start SKiDocGenService
sc \\10.0.3.125 start SKiSystemService
sc \\10.0.3.125 start SKiRemoteDataService
sc \\10.0.3.126 start SKiDocGenService
sc \\10.0.3.126 start SKiSystemService
sc \\10.0.3.126 start SKiRemoteDataService
sc \\10.0.3.127 start SKiDocGenService
sc \\10.0.3.127 start SKiSystemService
sc \\10.0.3.127 start SKiRemoteDataService
sc \\10.0.3.128 start SKiDocGenService
sc \\10.0.3.128 start SKiSystemService
sc \\10.0.3.128 start SKiRemoteDataService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo Static Environment (118,171) Started ...
echo Cluster Environment (120 - 128) Started ...
echo.
echo.
echo.
echo.
pause
sc \\10.0.3.116 stop SKiWebService
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo Static Environment (118,171) Started ...
echo Cluster Environment (120 - 128) Started ...
echo WebServices Environment (116) Started ...
echo.
echo Action Complete!
echo.
echo.
echo.
echo.
pause
goto menu


:UNATTENDEDSTARTTWO
net start SKiSystemConfiguration
net start SKiSystemServiceNet
net start SKiSystemService
net start SKiSubscriptionService
net start SKiRemoteDataService
net start SKiQuoteService
net start SKiMessagingService
net start SKiIDService
net start SKiDocMailService
net start SKiQuoteServiceNet
net start SKiProcessingService
net start SKiICSService
net start SKiExcel2PDF
net start SKiEventsService
net start SKiDocGenService
net start SKiCashbackService
net start SKiClaimAggregateService
net start SKiTelesureUploadService
net start SantamYellowboxService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo.
echo.
echo.
echo.
pause
echo sc \\10.0.3.118 start SKiDocGenService
echo sc \\10.0.3.118 start SKiSystemService
echo sc \\10.0.3.118 start SKiRemoteDataService
sc \\10.0.3.171 start SKiDocGenService
sc \\10.0.3.171 start SKiSystemService
sc \\10.0.3.171 start SKiRemoteDataService
pause
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Application Environment (114) Started ...
echo Static Environment (118,171) Started ...
echo.
echo Action Complete!
echo.
echo.
echo.
echo.
pause
goto menu


:EXITMENU
cls
echo -------------------------------------------------------------------------------
echo PSG MASTER SERVICES MANAGEMENT WINDOW                                v1.8.3 SP5
echo -------------------------------------------------------------------------------
echo.
echo.
echo Thank you for using the Master Services Window to manage PSG services.
echo Please test all SKi functions are working normally.
echo.
echo.
echo Yours in service
echo Genasys Technologies Pty Ltd
echo.
echo.
echo.
echo.
echo.
pause
exit