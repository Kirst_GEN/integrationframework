--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES THE SJI_INT SCHEMA IF THE SCHEMA DOES NOT EXIST
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
	--================================================================================
	--CREATE SCHEMA FOR SKI INTEGRATION FRAMEWORK
	--================================================================================
	IF NOT EXISTS 
	(
		SELECT  
			SCHEMA_NAME
		FROM    
			INFORMATION_SCHEMA.SCHEMATA
		WHERE   
			SCHEMA_NAME = 'ski_int' 
	) 
	BEGIN
		EXEC SP_EXECUTESQL N'CREATE SCHEMA ski_int'
	END
