/****** Object:  Table [ski_int].[APIRawData]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[APIRawData](
	[C_CountryId] [int] NULL,
	[C_ExternalRef] [int] NULL,
	[C_Initials] [nvarchar](8) NULL,
	[C_FirstName] [nvarchar](30) NULL,
	[C_LastName] [nvarchar](30) NULL,
	[C_PreferredCommsLanguage] [nvarchar](15) NULL,
	[C_Title] [nvarchar](15) NULL,
	[C_IDType] [nvarchar](15) NULL,
	[C_IDNumber] [nvarchar](255) NULL,
	[C_Nationality] [nvarchar](255) NULL,
	[C_DOB] [date] NULL,
	[C_Gender] [nvarchar](15) NULL,
	[C_EMailAddress] [nvarchar](255) NULL,
	[C_Cellnumber] [nvarchar](255) NULL,
	[C_EmergencyContactName] [nvarchar](255) NULL,
	[C_EmergencyContactNumber] [nvarchar](255) NULL,
	[C_PhysicalAddressLine1] [nvarchar](50) NULL,
	[C_PhysicalAddressLine2] [nvarchar](50) NULL,
	[C_PhysicalAddressLine3] [nvarchar](50) NULL,
	[C_PhysicalAddressSuburb] [nvarchar](50) NULL,
	[C_PhysicalAddressPostalCode] [nvarchar](13) NULL,
	[P_PolicyCountryId] [int] NULL,
	[P_PolicyReference] [int] NULL,
	[P_PolicyNumber] [nvarchar](30) NULL,
	[P_CoverStartDate] [date] NULL,
	[P_CoverTerm] [nvarchar](15) NULL,
	[P_PaymentTerm] [nvarchar](15) NULL,
	[P_PaymentMethod] [nvarchar](15) NULL,
	[P_CollectionDay] [int] NULL,
	[P_AccountHolder] [nvarchar](50) NULL,
	[P_Bank] [nvarchar](50) NULL,
	[P_BankBranchCode] [nvarchar](10) NULL,
	[P_BankAccountNumber] [nvarchar](16) NULL,
	[R_M_MotorEffectiveDate] [date] NULL,
	[R_M_MotorYearOfManufacture] [nvarchar](10) NULL,
	[R_M_MotorMake] [nvarchar](255) NULL,
	[R_M_MotorModel] [nvarchar](255) NULL,
	[R_M_MotorColour] [nvarchar](15) NULL,
	[R_M_MotorRegistrationNumber] [nvarchar](255) NULL,
	[R_M_MotorVinNumber] [nvarchar](255) NULL,
	[R_M_MotorChasisNumber] [nvarchar](255) NULL,
	[R_M_MotorEngineNumber] [nvarchar](255) NULL,
	[R_M_MotorVehicleFinanced] [bit] NULL,
	[R_M_MotorInterestNoted] [nvarchar](255) NULL,
	[R_M_MotorValue] [int] NULL,
	[R_M_MotorVehicleCondition] [nvarchar](15) NULL,
	[R_M_MotorParkedAtNight] [nvarchar](15) NULL,
	[R_M_MotorCurrentMileage] [int] NULL,
	[R_M_MotorDailyMileage] [int] NULL,
	[R_M_MotorUseMonday] [bit] NULL,
	[R_M_MotorUseTuesday] [bit] NULL,
	[R_M_MotorUseWednesday] [bit] NULL,
	[R_M_MotorUseThursday] [bit] NULL,
	[R_M_MotorUseFriday] [bit] NULL,
	[R_M_MotorUseSaturday] [bit] NULL,
	[R_M_MotorUseSunday] [bit] NULL,
	[R_M_BaseCover] [bit] NULL,
	[R_M_TheftAndHijackCover] [bit] NULL,
	[R_M_NaturalCausesCover] [bit] NULL,
	[R_M_WriteOffCover] [bit] NULL,
	[R_M_WriteOffPercentage] [int] NULL,
	[R_M_PolicyAnnualPremium] [decimal](18, 2) NULL,
	[R_M_PolicyAnnualCommission] [decimal](18, 2) NULL,
	[R_M_PolicyAnnualSasria] [decimal](18, 2) NULL,
	[R_M_RegularDriverLicenseCode] [nvarchar](15) NULL,
	[R_M_RegularDriverYearsWithLicense] [int] NULL,
	[R_M_RegularDriverBehaviourScore] [int] NULL,
	[R_M_RegularDriverDOB] [date] NULL,
	[R_M_RegularDriverGender] [int] NULL,
	[R_M_RegularDriverIDNumber] [nvarchar](255) NULL,
	[R_M_RegularDriverIDType] [int] NULL,
	[R_M_RegularDriverName] [nvarchar](255) NULL,
	[R_M_RegularDriverRelationshipToInsured] [int] NULL,
	[R_VA_EffectiveDate] [date] NULL,
	[R_VA_EmergencyAssistance] [bit] NULL,
	[R_VA_ExternalReference] [nvarchar](255) NULL,
	[R_VA_HomeAssistance] [bit] NULL,
	[R_VA_TelematicsDevice] [bit] NULL,
	[R_VA_TransactionType] [int] NULL,
	[R_VA_AnnualCommission] [decimal](18, 2) NULL,
	[R_VA_AnnualPremium] [decimal](18, 2) NULL,
	[R_VA_AnnualSasria] [decimal](18, 2) NULL,
	[R_SP_EffectiveDate] [date] NULL,
	[R_SP_ExternalReference] [nvarchar](255) NULL,
	[R_SP_MonthlyContribution] [int] NULL,
	[R_SP_TransactionType] [int] NULL,
	[R_SP_AnnualCommission] [decimal](18, 2) NULL,
	[R_SP_AnnualPremium] [decimal](18, 2) NULL,
	[ProcessDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[BatchUser]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[CollectionImport]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[CollectionImport](
	[Id] [uniqueidentifier] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[Productid] [int] NOT NULL,
	[FileSumInsured] [money] NOT NULL,
	[FilePremium] [money] NOT NULL,
	[SortByDate] [date] NOT NULL,
	[TransactionPeriod] [date] NOT NULL,
 CONSTRAINT [batch_ImportFileData_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[DailyImportExceptions]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[DailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL CONSTRAINT [Imported_D_0]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[DailyLTINewBusiness_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[DailyLTINewBusiness_Log](
	[sRecord_Id] [varchar](1) NULL,
	[dtTransaction_Date] [datetime] NULL,
	[sReason_Code] [varchar](2) NULL,
	[sCIF_Number] [varchar](50) NULL,
	[sCustomer_Title] [varchar](30) NULL,
	[sCustomer_First_Name] [varchar](30) NULL,
	[sCustomer_Surname] [varchar](30) NULL,
	[sGender] [varchar](1) NULL,
	[dtDate_of_Birth] [datetime] NULL,
	[sID_Passport_Number] [varchar](20) NULL,
	[sPostal_Address_1] [varchar](30) NULL,
	[sPostal_Address_2] [varchar](30) NULL,
	[sPostal_Address_3] [varchar](30) NULL,
	[sPostal_Code] [varchar](10) NULL,
	[sTel_No] [varchar](25) NULL,
	[sEmployer_Name] [varchar](50) NULL,
	[sEmployer_Contact_Tel_No] [varchar](25) NULL,
	[sMarital_Status] [varchar](5) NULL,
	[sLoan_Account_No] [varchar](16) NULL,
	[sSOL_ID] [varchar](8) NULL,
	[cLoan_Amount] [numeric](18, 2) NULL,
	[sLoan_Term] [varchar](3) NULL,
	[sPlan_Type] [varchar](2) NULL,
	[dtLoan_Value_Date] [datetime] NULL,
	[cPremium] [numeric](18, 2) NULL,
	[dtRepayment_Date] [datetime] NULL,
	[sSecond_Life_Insured_Title] [varchar](30) NULL,
	[sSecond_Life_Insured_First_Name] [varchar](30) NULL,
	[sSecond_Life_Insured_Surname] [varchar](30) NULL,
	[sSecond_Life_Insured_ID_Passport_No] [varchar](20) NULL,
	[sSecond_Life_DOB] [date] NULL,
	[sLTI_Flag] [int] NULL,
	[dtLTI_Expiry] [datetime] NULL,
	[cOutstanding_Balance] [numeric](18, 2) NULL,
	[sScheme_Code] [varchar](2) NULL,
	[sCurrency_Code] [varchar](3) NULL,
	[sInsurer_Own_insurance] [varchar](50) NULL,
	[sPolicy_ID_Own_insurance] [varchar](15) NULL,
	[cAmount_Own_insurance] [numeric](18, 2) NULL,
	[dtExpiry_Date_Own_Insurance] [datetime] NULL,
	[sProduct_Scheme_Code] [varchar](5) NULL,
	[sSegment] [varchar](3) NULL,
	[sSub_segment] [varchar](10) NULL,
	[sSerial_Number] [varchar](5) NULL,
	[sUnderwriter_Name] [varchar](20) NULL,
	[sPIN_number] [varchar](10) NULL,
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NOT NULL,
	[dtProcessDate] [datetime] NOT NULL,
	[iSourceFileID] [bigint] NOT NULL,
	[sNewLoanAccountNo] [varchar](18) NOT NULL,
	[sCountry] [varchar](50) NULL,
	[sCountryClassification] [varchar](5) NULL,
	[sCountry_Cif] [varchar](50) NULL,
	[iCountryID] [int] NULL,
	[RH_CoInsurer] [varchar](50) NULL,
	[PH_ProductID] [int] NULL,
	[FileName] [varchar](255) NULL,
 CONSTRAINT [PK_DailyLTINewBusiness_Log] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_Src_RawData]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSIS_Src_RawData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NOT NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL,
 CONSTRAINT [PK_SSIS_Src_RawData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_Src_RawData_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSIS_Src_RawData_Log](
	[ID] [int] NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISBatchUser]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISBatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISCommunication]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationId] [int] NOT NULL,
	[CommunicationTypeId] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[InAttachment] [bit] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISCommunicationType]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISCommunicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISDailyImportExceptions]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISDailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL CONSTRAINT [Imported_D_SSISDailyImportExceptions]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISDailyImportExceptions_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISDailyImportExceptions_Log](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISDataErrorLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISDataErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorSource] [nvarchar](255) NOT NULL,
	[SourceRecord] [nvarchar](255) NOT NULL,
	[ErrorNumber] [nvarchar](100) NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISEmailConfig]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISEmailConfig](
	[uID] [int] NULL,
	[MailServer] [varbinary](100) NOT NULL,
	[MailPort] [int] NOT NULL,
	[MailUserName] [varchar](100) NOT NULL,
	[MailPassword] [varbinary](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISErrorLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISFileLocations]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFileLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [varchar](100) NULL,
	[FileName] [varchar](100) NULL,
	[FileType] [int] NULL,
	[FileHasHeaderRow] [bit] NULL,
	[FileHasFooterRow] [bit] NULL,
	[PickupLocation] [varchar](255) NULL,
	[ProcessedLocation] [varchar](255) NULL,
	[ArchiveLocation] [varchar](255) NULL,
	[LogFileLocation] [varchar](255) NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFileType]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFileType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](10) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCancelSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashCancelSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCancelSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCancelSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashCancelSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCancelSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCustomerSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashCustomerSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[CustomerId] [int] NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCustomerSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCustomerSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashCustomerSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[CustomerId] [int] NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCustomerSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashPolicyUpdateSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashPolicyUpdateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashPolicyUpdateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashPolicyUpdateSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashPolicyUpdateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashPolicyUpdateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashReInstateSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashReInstateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[RiskId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashReInstateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashReInstateSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashReInstateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[RiskId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashReInstateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashRiskUpdateSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashRiskUpdateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[RiskId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashRiskUpdateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashRiskUpdateSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashRiskUpdateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[RiskId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashRiskUpdateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashSuspendSource]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashSuspendSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashSuspendSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashSuspendSource_Log]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISFlashSuspendSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashSuspendSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISImportLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISImportLog](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISIntegrationLookupMap]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISIntegrationLookupMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[SourceGroup] [varchar](50) NOT NULL,
	[SourceDBValue] [varchar](255) NOT NULL,
	[TargetGroup] [varchar](255) NOT NULL,
	[TargetDBValue] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CL_SSISLookupMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISJob]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISJob](
	[SSISJobID] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[Success] [bit] NULL CONSTRAINT [DF_SSISJobID_Success]  DEFAULT ((0)),
 CONSTRAINT [PK_SSISJob] PRIMARY KEY CLUSTERED 
(
	[SSISJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISMailsToBeSent]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISMailsToBeSent](
	[MailID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[MailFrom] [varchar](200) NULL,
	[MailTO] [varchar](400) NULL,
	[MailCC] [varchar](200) NULL,
	[MailBCC] [varchar](200) NULL,
	[MailSubject] [varchar](200) NULL,
	[MailBody] [varchar](max) NULL,
	[DateToSend] [date] NULL,
	[Sent] [bit] NULL,
	[DateSent] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISPackageLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISPackageLog](
	[PackageLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISPackageLog] PRIMARY KEY CLUSTERED 
(
	[PackageLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISProcess]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISProcess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](400) NULL,
	[ProcessTypeId] [int] NULL,
	[CommunicationId] [int] NULL,
	[FileLocationId] [int] NULL,
	[ScriptId] [int] NULL,
	[SSISDBCatalogFolderName] [varchar](100) NULL,
	[SSISDBCatalogProjectName] [varchar](100) NULL,
	[SSISDBCatalogPackageName] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISProcessType]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISProcessType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ScriptId] [int] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISProgressLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISProgressLog](
	[ProgressLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ContinueNextStep] [bit] NULL,
	[Message] [nvarchar](255) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NOT NULL CONSTRAINT [DF_ProgressLog_Success]  DEFAULT ((0)),
 CONSTRAINT [PK_SSISProgressLog] PRIMARY KEY CLUSTERED 
(
	[ProgressLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISRaisingLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISRaisingLog](
	[RaisingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[RaisingQueryCount] [int] NOT NULL,
	[RaisedCount] [int] NOT NULL,
	[NotRaisedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISReceiptingLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISReceiptingLog](
	[ReceiptingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[ReceiptingQueryCount] [int] NOT NULL,
	[ReceiptingCount] [int] NOT NULL,
	[NotReceiptedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [ski_int].[SSISScript]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISScript](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScriptId] [int] NOT NULL,
	[ScriptTypeId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISScriptType]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ski_int].[SSISScriptType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISSuspendLog]    Script Date: 22/01/2018 08:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ski_int].[SSISSuspendLog](
	[SuspendLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[SuspendQueryCount] [int] NOT NULL,
	[SuspendedCount] [int] NOT NULL,
	[NotSuspendedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
