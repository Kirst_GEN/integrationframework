/****** Object:  Table [batch].[CustomerUpdateJobValue]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[CustomerUpdateJobValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[CustomerUpdateJobValue](
	[Id] [uniqueidentifier] NOT NULL,
	[JobTaskStatusId] [uniqueidentifier] NOT NULL,
	[CustomerId] [float] NOT NULL,
	[sFirstName] [varchar](30) NULL,
	[sInitials] [varchar](8) NULL,
	[sLastName] [varchar](30) NULL,
	[sCompanyName] [varchar](255) NULL,
	[sContactName] [varchar](80) NULL,
	[PhysAddrLine1] [varchar](50) NULL,
	[PhysAddrLine2] [varchar](50) NULL,
	[PhysAddrLine3] [varchar](50) NULL,
	[PhysAddrSuburb] [varchar](50) NULL,
	[PhysAddrPostCode] [varchar](13) NULL,
	[PhysAddrRatingArea] [varchar](30) NULL,
	[PostAddrLine1] [varchar](50) NULL,
	[PostAddrLine2] [varchar](50) NULL,
	[PostAddrLine3] [varchar](50) NULL,
	[PostAddrSuburb] [varchar](50) NULL,
	[PostAddrPostCode] [varchar](13) NULL,
	[PostAddrRatingArea] [varchar](30) NULL,
 CONSTRAINT [PK_batch_CustomerUpdateJobValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[FieldUpdateJobValue]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[FieldUpdateJobValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[FieldUpdateJobValue](
	[Id] [uniqueidentifier] NOT NULL,
	[JobTaskStatusId] [uniqueidentifier] NOT NULL,
	[ContextId] [float] NOT NULL,
	[ContextType] [tinyint] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](255) NULL,
	[cFieldValue] [float] NULL,
 CONSTRAINT [PK_batch_FieldUpdateJobValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ImportErrorLog]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportErrorLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportErrorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[ErrorProcedure] [varchar](100) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorSeverity] [int] NULL,
	[ErrorState] [int] NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ImportFileData]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportFileData]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportFileData](
	[Id] [uniqueidentifier] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[FileSumInsured] [money] NOT NULL,
	[FilePremium] [money] NOT NULL,
	[TransactionPeriod] [datetime] NOT NULL,
 CONSTRAINT [batch_ImportFileData_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [batch].[ImportPolicy]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportPolicy]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportPolicy](
	[Id] [uniqueidentifier] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[FileSumInsured] [money] NOT NULL,
	[FilePremium] [money] NOT NULL,
	[TransactionPeriod] [datetime] NOT NULL,
	[AdminFee] [money] NULL,
	[BrokerFee] [money] NULL,
	[PolicyFee] [money] NULL,
	[BatchNumber] [int] NOT NULL,
	[RunReference] [varchar](255) NOT NULL,
	[PolicyStatus] [int] NOT NULL,
 CONSTRAINT [batch_ImportPolicy_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ImportPolicyError]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportPolicyError]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportPolicyError](
	[Id] [uniqueidentifier] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[RunReference] [varchar](255) NOT NULL,
	[ErrorMessage] [varchar](255) NOT NULL,
 CONSTRAINT [batch_ImportPolicyError_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ImportPolicyFieldUpdate]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportPolicyFieldUpdate]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportPolicyFieldUpdate](
	[Id] [uniqueidentifier] NOT NULL,
	[ImportPolicyId] [uniqueidentifier] NOT NULL,
	[PolicyField] [varchar](15) NOT NULL,
	[Value] [varchar](255) NOT NULL,
 CONSTRAINT [batch_ImportPolicyFieldUpdate_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ImportRiskFieldUpdate]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ImportRiskFieldUpdate]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ImportRiskFieldUpdate](
	[Id] [uniqueidentifier] NOT NULL,
	[ImportPolicyId] [uniqueidentifier] NOT NULL,
	[RiskField] [varchar](15) NOT NULL,
	[Value] [varchar](255) NOT NULL,
 CONSTRAINT [batch_ImportRiskFieldUpdate_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[Job]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[Job]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[Job](
	[Id] [uniqueidentifier] NOT NULL,
	[ProcessId] [uniqueidentifier] NOT NULL,
	[Reference] [varchar](100) NOT NULL,
	[CreateEndorsement] [bit] NOT NULL,
	[EndorsementCreated] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [varchar](20) NOT NULL,
	[ExecutionStart] [datetime] NULL,
	[ExecutionEnd] [datetime] NULL,
	[TaskCount] [tinyint] NOT NULL,
	[TasksCompleted] [tinyint] NOT NULL,
	[Progress] [tinyint] NOT NULL,
	[EndorseCode] [varchar](15) NULL,
 CONSTRAINT [PK_batch_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[JobPolicyTaskStatus]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[JobPolicyTaskStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[JobPolicyTaskStatus](
	[Id] [uniqueidentifier] NOT NULL,
	[JobTaskStatusId] [uniqueidentifier] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Message] [varchar](255) NULL,
 CONSTRAINT [PK_batch_JobPolicyTaskStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[JobTaskStatus]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[JobTaskStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[JobTaskStatus](
	[Id] [uniqueidentifier] NOT NULL,
	[JobId] [uniqueidentifier] NOT NULL,
	[TaskId] [uniqueidentifier] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Message] [varchar](255) NULL,
	[ExecutionStart] [datetime] NULL,
	[ExecutionEnd] [datetime] NULL,
	[ConfigurationXML] [text] NULL,
 CONSTRAINT [PK_batch_JobTaskStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[kobusCounts]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[kobusCounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[kobusCounts](
	[Jobreference] [varchar](30) NOT NULL,
	[ImportLogID] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[Process]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[Process]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[Process](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](255) NULL,
	[Enabled] [bit] NOT NULL,
	[UserLastUpdated] [varchar](20) NOT NULL,
	[DateLastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_batch_Process] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [batch].[ProcessTask]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[ProcessTask]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[ProcessTask](
	[Id] [uniqueidentifier] NOT NULL,
	[ProcessId] [uniqueidentifier] NOT NULL,
	[TaskType] [tinyint] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[ConfigurationXML] [text] NULL,
 CONSTRAINT [PK_batch_ProcessTask] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [batch].[tempRaw]    Script Date: 23/10/2017 10:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[batch].[tempRaw]') AND type in (N'U'))
BEGIN
CREATE TABLE [batch].[tempRaw](
	[CountryCIF] [varchar](76) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [batch].[tempRaw] ADD [SerialNo] [varchar](5) NULL
SET ANSI_PADDING OFF
ALTER TABLE [batch].[tempRaw] ADD [ProductId] [varchar](8000) NOT NULL
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_CustomerUpdateJobValue_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[CustomerUpdateJobValue]'))
ALTER TABLE [batch].[CustomerUpdateJobValue]  WITH CHECK ADD  CONSTRAINT [FK_batch_CustomerUpdateJobValue_JobTaskStatus_Id] FOREIGN KEY([JobTaskStatusId])
REFERENCES [batch].[JobTaskStatus] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_CustomerUpdateJobValue_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[CustomerUpdateJobValue]'))
ALTER TABLE [batch].[CustomerUpdateJobValue] CHECK CONSTRAINT [FK_batch_CustomerUpdateJobValue_JobTaskStatus_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_FieldUpdateJobValue_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[FieldUpdateJobValue]'))
ALTER TABLE [batch].[FieldUpdateJobValue]  WITH CHECK ADD  CONSTRAINT [FK_batch_FieldUpdateJobValue_JobTaskStatus_Id] FOREIGN KEY([JobTaskStatusId])
REFERENCES [batch].[JobTaskStatus] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_FieldUpdateJobValue_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[FieldUpdateJobValue]'))
ALTER TABLE [batch].[FieldUpdateJobValue] CHECK CONSTRAINT [FK_batch_FieldUpdateJobValue_JobTaskStatus_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_Job_Process_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[Job]'))
ALTER TABLE [batch].[Job]  WITH CHECK ADD  CONSTRAINT [FK_batch_Job_Process_Id] FOREIGN KEY([ProcessId])
REFERENCES [batch].[Process] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_Job_Process_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[Job]'))
ALTER TABLE [batch].[Job] CHECK CONSTRAINT [FK_batch_Job_Process_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobPolicyTaskStatus_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobPolicyTaskStatus]'))
ALTER TABLE [batch].[JobPolicyTaskStatus]  WITH NOCHECK ADD  CONSTRAINT [FK_batch_JobPolicyTaskStatus_JobTaskStatus_Id] FOREIGN KEY([JobTaskStatusId])
REFERENCES [batch].[JobTaskStatus] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobPolicyTaskStatus_JobTaskStatus_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobPolicyTaskStatus]'))
ALTER TABLE [batch].[JobPolicyTaskStatus] CHECK CONSTRAINT [FK_batch_JobPolicyTaskStatus_JobTaskStatus_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobTaskStatus_Job_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobTaskStatus]'))
ALTER TABLE [batch].[JobTaskStatus]  WITH CHECK ADD  CONSTRAINT [FK_batch_JobTaskStatus_Job_Id] FOREIGN KEY([JobId])
REFERENCES [batch].[Job] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobTaskStatus_Job_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobTaskStatus]'))
ALTER TABLE [batch].[JobTaskStatus] CHECK CONSTRAINT [FK_batch_JobTaskStatus_Job_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobTaskStatus_Task_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobTaskStatus]'))
ALTER TABLE [batch].[JobTaskStatus]  WITH CHECK ADD  CONSTRAINT [FK_batch_JobTaskStatus_Task_Id] FOREIGN KEY([TaskId])
REFERENCES [batch].[ProcessTask] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_JobTaskStatus_Task_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[JobTaskStatus]'))
ALTER TABLE [batch].[JobTaskStatus] CHECK CONSTRAINT [FK_batch_JobTaskStatus_Task_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_ProcessTask_Process_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[ProcessTask]'))
ALTER TABLE [batch].[ProcessTask]  WITH CHECK ADD  CONSTRAINT [FK_batch_ProcessTask_Process_Id] FOREIGN KEY([ProcessId])
REFERENCES [batch].[Process] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[batch].[FK_batch_ProcessTask_Process_Id]') AND parent_object_id = OBJECT_ID(N'[batch].[ProcessTask]'))
ALTER TABLE [batch].[ProcessTask] CHECK CONSTRAINT [FK_batch_ProcessTask_Process_Id]
GO
