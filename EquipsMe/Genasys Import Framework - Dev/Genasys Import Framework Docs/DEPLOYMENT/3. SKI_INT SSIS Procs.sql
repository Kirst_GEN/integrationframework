USE [SBAB_082017]
GO
/****** Object:  StoredProcedure [ski_int].[BatchUserLogin]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[BatchUserLogin]
    @pLoginName NVARCHAR(254),
    @pPassword NVARCHAR(50),
    @responseMessage NVARCHAR(250)='' OUTPUT
AS
BEGIN

    SET NOCOUNT ON

    DECLARE @userID INT

    IF EXISTS (SELECT TOP 1 uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName)
    BEGIN
        SET @userID=(SELECT uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName AND Password = HASHBYTES('SHA2_512', @pPassword))

       IF(@userID IS NULL)
           SET @responseMessage='Incorrect password'
       ELSE 
           SET @responseMessage='User successfully logged in'
    END
    ELSE
       SET @responseMessage='Invalid login'

	   select @responseMessage

END


GO
/****** Object:  StoredProcedure [ski_int].[Execute_SSIS_Packages]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [ski_int].[Execute_SSIS_Packages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;






GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddEmailConfig]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_AddEmailConfig]
	
    @MailServer VARCHAR(8000),
    @MailPort INT = 0,
    @MailUserName VARCHAR(100) = NULL,
    @MailPassword VARCHAR(8000) 
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISEmailConfig];

	INSERT INTO [ski_int].[SSISEmailConfig]
	(
		MailServer,
		MailPort,
		MailUserName,
		MailPassword
	)
	VALUES
	(
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailServer)),
		@MailPort,
		@MailUserName,
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailPassword))
	);
		

END
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddSSISUser]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_AddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISBatchUser];

	INSERT INTO [ski_int].[SSISBatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES EPROCSTATUS TO 777 FOR ALL RECORDS IN IMPORTRECORD TABLE. THESE RECORDS
--				:	WILL BE UPDATED IN BATCHES OF 1000 RECORDS TO EPROCSTATUS 1, TO IMPROVE EFFICIENCY
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter] 
	(
	@BatchAmountOfRecords VARCHAR(10)
	,@TableName SYSNAME
	)

AS

BEGIN

	--=====================================================
	--SET @BatchAmountOfRecords DEFAULT IF NO VALUE RECIEVED
	--=====================================================
	IF @BatchAmountOfRecords > 0 
		BEGIN
			SET @BatchAmountOfRecords = @BatchAmountOfRecords
		END
	ELSE
		BEGIN
			SET @BatchAmountOfRecords = 1000
		END;	

	--=====================================================
	--UPDATE IMPORTRECNO TO -1 - TO BE EXECUTED IN BATCHES
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000);

		SET @SQLCommand = 'UPDATE ' + N'' + @TableName + ' SET iImportRecNo = -1 WHERE iID IN(SELECT TOP ' + (@BatchAmountOfRecords) + ' iID FROM ' + N'' + @TableName + ' WHERE iImportRecNo = 999999)';
	
		EXECUTE (@SQLCommand);

END 








GO
/****** Object:  StoredProcedure [ski_int].[SSIS_CanContinue]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONTINUE NEXT STEP INDICATOR FOR THE PROCESS
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_CanContinue]
(
		@ProcessName VARCHAR(100)
		,@PackageName VARCHAR(100)
)
AS

BEGIN

	--==============================================================================================================
	--GET PACKAGE ID
	--==============================================================================================================
	DECLARE @PackageLogID INT;
	
	SELECT TOP 1 
		@PackageLogID = PackageLogID
	FROM
		[ski_int].[SSISPackageLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
	ORDER BY
		EndTime DESC;
		
	--==============================================================================================================
	--GET CONTINUE NEXT STEP INDICATOR
	--==============================================================================================================
	SELECT TOP 1 
		ContinueNextStep
	FROM
		[ski_int].[SSISProgressLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
		AND PackageLogID = @PackageLogID
	ORDER BY
		EndTime DESC;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_DataErrorLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_DataErrorLog]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorSource NVARCHAR(255)
	, @SourceRecord NVARCHAR(255)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISDataErrorLog(
		PackageLogID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorSource
		,SourceRecord
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorSource
	, @SourceRecord
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EmailsToBeSent_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [ski_int].[SSIS_EmailsToBeSent_Insert]
		@PackageLogID INT
		,@ProcessName VARCHAR(255)
		,@JobReference VARCHAR(100)
		,@From VARCHAR(200)
		,@TO VARCHAR(400)
		,@CC VARCHAR(200)
		,@BCC VARCHAR(200)
		,@Subject VARCHAR(200)
		,@Body VARCHAR(8000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO [ski_int].[SSISMailsToBeSent](
		PackageLogID
		,ProcessName
		,JobReference
		,MailFrom
		,MailTO
		,MailCC
		,MailBCC
		,MailSubject
		,MailBody
		,DateToSend
		,Sent
		,DateSent
	)
	VALUES (
		@PackageLogID
		,@ProcessName
		,@JobReference
		,@From
		,@TO
		,@CC
		,@BCC
		,@Subject
		,@Body
		,GetDate()
		,0
		,NULL
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndExtractLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndExtractLog]
  @ExtractLogID INT
, @ExtractCount INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastExtractDateTime DATETIME, @SQL NVARCHAR(255)
	SELECT @SQL = N'SELECT @LastExtractDateTime = ISNULL(MAX(ModifiedDate), ''1900-01-01'') FROM ski_int.imp_' + TableName FROM ski_int.SSISExtractLog WHERE ExtractLogID = @ExtractLogID
	EXEC sp_executeSQL @SQL, N'@LastExtractDateTime DATETIME OUTPUT', @LastExtractDateTime OUTPUT
	
	UPDATE ski_int.SSISExtractLog
	SET
	  EndTime = GetDate()
	, ExtractCount = @ExtractCount
	, LastExtractDateTime = @LastExtractDateTime
	, Success = 1
	WHERE ExtractLogID = @ExtractLogID

	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndJob]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES THE [ski_int].[SSIS_EndJob] TALE ON COMPLETION OF THE JOB
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [ski_int].[SSIS_EndJob]
	@SSISJobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN

	--==============================================================================================================
	--UPDATE JOB
	--==============================================================================================================
	UPDATE 
		[ski_int].[SSISJob] 
	SET
		EndTime = GetDate()
		,JobReference = @JobReference 
		,Success = @Success
	WHERE 
		SSISJobID = @SSISJobID;
	

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndPackageLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndPackageLog]
	@JobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN


	UPDATE 
		ski_int.SSISPackageLog 
	SET
		EndTime = GetDate()
		,Success = @Success
		,JobReference = @JobReference
	WHERE 
		JobID = @JobID;

	UPDATE 
		ski_int.SSISProgressLog 
	SET
		EndTime = GetDate()
		,Success = @Success
	WHERE 
		JobID = @JobID;
	

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndProgressLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndProgressLog]
  @PackageLogID INT
  ,@Success INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ski_int.SSISProgressLog SET
	  EndTime = GetDate()
	, Success = @Success
	WHERE PackageLogID = @PackageLogID

	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ErrorLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ErrorLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100) 
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISErrorLog(
		JobID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ExecutePackages]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [ski_int].[SSIS_ExecutePackages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;






GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRaisingRecordCount]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE NUMBER OF POLICIES FOR THE CURRENT RAISING RUN
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashRaisingRecordCount] 
(
		@NextPaymentDate		DATE
		,@SplitDay				INT 
		,@PaymentMethod			VARCHAR(10)
		,@PaymentTerm			VARCHAR(10)
		,@Count					INT OUTPUT
)
AS

--EXEC [ski_int].[SSIS_FlashRaisingRecordCount] 0, 0, '2017-04-01','DEFAULT' ,10 ,'0,1' ,'0.3' ,'ZAM',0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		@Count = COUNT(P.iPolicyId)
	FROM 
		Policy p
	WHERE
		CONVERT(NVARCHAR(6),P.iPaymentTerm) IN (@PaymentTerm)
		AND CONVERT(NVARCHAR(6),P.iPaymentMethod) IN (@PaymentMethod) 
		AND P.iCollection <= @SplitDay
		AND (CONVERT(NVARCHAR(6), p.dNextPaymentDate, 112) = CONVERT(NVARCHAR(6), CAST(@NextPaymentDate AS datetime), 112))
		AND (p.iPolicyStatus < 2);

	SELECT @Count;

END 


GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRecieptingRecordCount]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE NUMBER OF POLICIES FOR THE CURRENT RECIEPTING RUN
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashRecieptingRecordCount] 
(
		@OnlyFeeJournals		INT
		,@InForceOnly			VARCHAR(1)
		,@NextPaymentDate		DATE
		,@FinancialAccount		VARCHAR(20) 
		,@CollectionDay			INT 
		,@PaymentMethod			VARCHAR(10)
		,@PaymentTerm			VARCHAR(10)
		,@CountryProductName	VARCHAR(3)
		,@Count					INT OUTPUT
)
AS

--EXEC [ski_int].[UspINT_FlashRecieptingRecordCount] 0, 0, '2017-04-01','DEFAULT' ,10 ,'0,1' ,'0.3' ,'ZAM',0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		@Count = COUNT(P.iPolicyId)
	FROM 
		DebitNoteBalance DNB (NOLOCK)
		INNER JOIN PolicyTransactions PT (NOLOCK) ON 
			PT.iDebitNoteID = DNB.iDebitNoteID 
			AND PT.iPolicyID = DNB.iPolicyID
		INNER JOIN Policy P (NOLOCK) ON 
			P.iPolicyID = PT.iPolicyID
		INNER JOIN Products PR (NOLOCK) ON 
			P.iProductid = PR.iProductID
	WHERE
		(PT.iTranType in  (1, 3, 5, 6, 9))
		AND (PT.iParentTranID < 0)
		AND (P.bFinanceEnabled = 1)
		AND (@OnlyFeeJournals = 0 OR ((PT.iTranType = 9) AND (PT.cPremium = 0) AND ((PT.cAdminFee + PT.cPolicyFee + PT.cBrokerFee) <> 0)))
		AND (@InForceOnly = 0 OR (p.dCoverStart <= @NextPaymentDate))
		AND (PT.sFinAccount = @FinancialAccount)
		AND (P.iCollection = @CollectionDay)
		AND P.iPaymentMethod IN (0,1) 
		AND P.iPaymentTerm IN (0,3)
		AND P.iPolicyStatus IN (0,1)
		AND (PR.sProductName LIKE @CountryProductName + '%')
		AND DNB.cPremiumBal <> 0;

	SELECT @Count;

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashTotalRecordCountForProcess]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_FlashTotalRecordCountForProcess] 
(
		@JobReference				VARCHAR(100)
		,@Created					INT OUTPUT
		,@Valid						INT OUTPUT			
		,@Warning					INT OUTPUT
		,@Error						INT OUTPUT
		,@Completed					INT OUTPUT
		,@Total						INT OUTPUT
)
AS

--EXEC [ski_int].[SSIS_FlashTotalRecordCountForProcess] 'RNG201705101400',0,0,0,0,0,0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		'CREATED'	= CASE	WHEN BJP.STATUS = 0 THEN COUNT(1) ELSE 0 END
		,'VALID'	= CASE	WHEN BJP.STATUS = 1 THEN COUNT(1) ELSE 0 END
		,'WARNING'	= CASE	WHEN BJP.STATUS = 2 THEN COUNT(1) ELSE 0 END
		,'ERROR'	= CASE	WHEN BJP.STATUS = 3 THEN COUNT(1) ELSE 0 END
		,'COMPLETED'= CASE	WHEN BJP.STATUS = 4 THEN COUNT(1) ELSE 0 END
	INTO 
		##FlashTotals
	FROM BATCH.JOB BJ
		INNER JOIN BATCH.JOBTASKSTATUS BJT (NOLOCK) ON 
			BJT.JOBID = BJ.ID
		INNER JOIN BATCH.JOBPOLICYTASKSTATUS BJP (NOLOCK) ON 
			BJP.JOBTASKSTATUSID = BJT.ID
	WHERE 
		BJ.REFERENCE = @JobReference
	GROUP BY 
		BJP.STATUS;

	SELECT 
		@Created	= SUM(CREATED)
		,@Valid		= SUM(VALID)
		,@Warning	= SUM(WARNING)
		,@Error		= SUM(ERROR)
		,@Completed	= SUM(COMPLETED)
		,@Total		= SUM(CREATED+VALID+WARNING+ERROR+COMPLETED)
	FROM 
		##FlashTotals;

	DROP TABLE ##FlashTotals;

	SELECT 
		@Created	
		,@Valid		
		,@Warning	
		,@Error		
		,@Completed	
		,@Total		

END 

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerField]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerField] 
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @EndorsementCode VARCHAR(15)
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewFieldUpdateJob @BatchReference, @EndorsementCode, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
			AND CDFields.sFieldCode IN
			(
				 'C_CLIENTTYPE'
				,'C_ENTITYNAME'
				,'C_TITLE'
				,'C_IDNUMPAS'
				,'C_CCTRUREG'
				,'C_DOB'
				,'C_PREFSHOCOMMET'
				,'C_HOMETEL'
				,'C_DAYTWORPHONUM'
				,'C_CELLNO'
				,'CL_TE_TP1EML'
				,'C_CLIENTTYPE0'
				,'C_ENTITYNAME0'
				,'C_TITLE0'
				,'CL_PRIMNAME'
				,'C_SURNAME'
				,'C_IDNUMPAS0'
				,'C_CCTRUREG0'
				,'C_DOB0'
				,'C_HOMETEL0'
				,'C_DAYTWORPHONU0'
				,'C_CELLNO0'
				,'CL_TE_TP1EML0'
				,'C_CLIENTTYPE1'
				,'C_ENTITYNAME1'
				,'C_TITLE1'
				,'CL_PRIMNAME0'
				,'C_SURNAME0'
				,'C_IDNUMPAS1'
				,'C_CCTRUREG1'
				,'C_DOB1'
				,'C_HOMETEL1'
				,'C_DAYTWORPHONU1'
				,'C_CELLNO1'
				,'CL_TE_TP1EML1'
				,'C_CLIENTTYPE2'
				,'C_ENTITYNAME2'
				,'C_TITLE2'
				,'CL_PRIMNAME1'
				,'C_SURNAME1'
				,'C_IDNUMPAS2'
				,'C_CCTRUREG2'
				,'C_DOB2'
				,'C_HOMETEL2'
				,'C_DAYTWORPHONU2'
				,'C_CELLNO2'
				,'CL_TE_TP1EML2'
				,'C_CLIENTTYPE3'
				,'C_ENTITYNAME3'
				,'C_TITLE3'
				,'CL_PRIMNAME2'
				,'C_SURNAME2'
				,'C_IDNUMPAS3'
				,'C_CCTRUREG3'
				,'C_DOB3'
				,'C_HOMETEL3'
				,'C_DAYTWORPHONU3'
				,'C_CELLNO3'
				,'CL_TE_TP1EML3'
			)


	--==================================================
	--3. CREATE ENTRY IN FIELD UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.FieldUpdateJobValue 
			(
			Id
			,JobTaskStatusId
			,ContextId
			,ContextType
			,FieldCode
			,sFieldValue
			,cFieldValue
			)
	SELECT 
		cfg.NewCombGUID()
		,@JobTaskId
		,CH.iCustomerID
		,1
		,CDFields.sFieldCode
		,CASE 
			CDFields.sFieldCode 
				WHEN 'C_CLIENTTYPE'		THEN FS.CH_CustomerType
				WHEN 'C_ENTITYNAME'		THEN FS.CH_EntityName
				WHEN 'C_TITLE'			THEN FS.CH_Title
				WHEN 'C_IDNUMPAS'		THEN FS.CH_IDNumber
				WHEN 'C_CCTRUREG'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB'			THEN FS.CH_DOB
				WHEN 'C_HOMETEL'		THEN FS.CH_HomeTelNumber
				WHEN 'C_DAYTWORPHONUM'	THEN FS.CH_WorkTelnumber
				WHEN 'C_CELLNO'			THEN FS.CH_CellNumber
				WHEN 'CL_TE_TP1EML'		THEN FS.CH_Email
				WHEN 'C_CLIENTTYPE0'	THEN FS.CD1_CustomerType
				WHEN 'C_ENTITYNAME0'	THEN FS.CH_EntityName
				WHEN 'C_TITLE0'			THEN FS.CD1_Title
				WHEN 'CL_PRIMNAME'		THEN FS.CD1_FirstName
				WHEN 'C_SURNAME'		THEN FS.CD1_Surname
				WHEN 'C_IDNUMPAS0'		THEN FS.CD1_IDNumber
				WHEN 'C_CCTRUREG0'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB0'			THEN FS.CD1_DOB
				WHEN 'C_HOMETEL0'		THEN FS.CD1_HomeTelNumber
				WHEN 'C_DAYTWORPHONU0'	THEN FS.CD1_WorkTelnumber
				WHEN 'C_CELLNO0'		THEN FS.CD1_CellNumber
				WHEN 'CL_TE_TP1EML0'	THEN FS.CD1_Email
				WHEN 'C_CLIENTTYPE1'	THEN FS.CD1_CustomerType
				WHEN 'C_ENTITYNAME1'	THEN FS.CH_EntityName
				WHEN 'C_TITLE1'			THEN FS.CD2_Title
				WHEN 'CL_PRIMNAME0'		THEN FS.CD2_FirstName
				WHEN 'C_SURNAME0'		THEN FS.CD2_Surname
				WHEN 'C_IDNUMPAS1'		THEN FS.CD2_IDNumber
				WHEN 'C_CCTRUREG1'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB1'			THEN FS.CD2_DOB
				WHEN 'C_HOMETEL1'		THEN FS.CD2_HomeTelNumber
				WHEN 'C_DAYTWORPHONU1'	THEN FS.CD2_WorkTelnumber
				WHEN 'C_CELLNO1'		THEN FS.CD2_CellNumber
				WHEN 'CL_TE_TP1EML1'	THEN FS.CD2_Email
				WHEN 'C_CLIENTTYPE2'	THEN FS.CD2_CustomerType
				WHEN 'C_ENTITYNAME2'	THEN FS.CH_EntityName
				WHEN 'C_TITLE2'			THEN FS.CD3_Title
				WHEN 'CL_PRIMNAME1'		THEN FS.CD3_FirstName
				WHEN 'C_SURNAME1'		THEN FS.CD3_Surname
				WHEN 'C_IDNUMPAS2'		THEN FS.CD3_IDNumber
				WHEN 'C_CCTRUREG2'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB2'			THEN FS.CD3_DOB
				WHEN 'C_HOMETEL2'		THEN FS.CD3_HomeTelNumber
				WHEN 'C_DAYTWORPHONU2'	THEN FS.CD3_WorkTelnumber
				WHEN 'C_CELLNO2'		THEN FS.CD3_CellNumber
				WHEN 'CL_TE_TP1EML2'	THEN FS.CD3_Email
				WHEN 'C_CLIENTTYPE3'	THEN FS.CD3_CustomerType
				WHEN 'C_ENTITYNAME3'	THEN FS.CH_EntityName
				WHEN 'C_TITLE3'			THEN FS.CD4_Title
				WHEN 'CL_PRIMNAME2'		THEN FS.CD4_FirstName
				WHEN 'C_SURNAME2'		THEN FS.CD4_Surname
				WHEN 'C_IDNUMPAS3'		THEN FS.CD4_IDNumber
				WHEN 'C_CCTRUREG3'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB3'			THEN FS.CD4_DOB
				WHEN 'C_HOMETEL3'		THEN FS.CD4_HomeTelNumber
				WHEN 'C_DAYTWORPHONU3'	THEN FS.CD4_WorkTelnumber
				WHEN 'C_CELLNO3'		THEN FS.CD4_CellNumber
				WHEN 'CL_TE_TP1EML3'	THEN FS.CD4_Email
			ELSE 
				CDFields.sFieldValue 
			END
		,1
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
			AND CDFields.sFieldCode IN
			(
				 'C_CLIENTTYPE'
				,'C_ENTITYNAME'
				,'C_TITLE'
				,'C_IDNUMPAS'
				,'C_CCTRUREG'
				,'C_DOB'
				,'C_PREFSHOCOMMET'
				,'C_HOMETEL'
				,'C_DAYTWORPHONUM'
				,'C_CELLNO'
				,'CL_TE_TP1EML'
				,'C_CLIENTTYPE0'
				,'C_ENTITYNAME0'
				,'C_TITLE0'
				,'CL_PRIMNAME'
				,'C_SURNAME'
				,'C_IDNUMPAS0'
				,'C_CCTRUREG0'
				,'C_DOB0'
				,'C_HOMETEL0'
				,'C_DAYTWORPHONU0'
				,'C_CELLNO0'
				,'CL_TE_TP1EML0'
				,'C_CLIENTTYPE1'
				,'C_ENTITYNAME1'
				,'C_TITLE1'
				,'CL_PRIMNAME0'
				,'C_SURNAME0'
				,'C_IDNUMPAS1'
				,'C_CCTRUREG1'
				,'C_DOB1'
				,'C_HOMETEL1'
				,'C_DAYTWORPHONU1'
				,'C_CELLNO1'
				,'CL_TE_TP1EML1'
				,'C_CLIENTTYPE2'
				,'C_ENTITYNAME2'
				,'C_TITLE2'
				,'CL_PRIMNAME1'
				,'C_SURNAME1'
				,'C_IDNUMPAS2'
				,'C_CCTRUREG2'
				,'C_DOB2'
				,'C_HOMETEL2'
				,'C_DAYTWORPHONU2'
				,'C_CELLNO2'
				,'CL_TE_TP1EML2'
				,'C_CLIENTTYPE3'
				,'C_ENTITYNAME3'
				,'C_TITLE3'
				,'CL_PRIMNAME2'
				,'C_SURNAME2'
				,'C_IDNUMPAS3'
				,'C_CCTRUREG3'
				,'C_DOB3'
				,'C_HOMETEL3'
				,'C_DAYTWORPHONU3'
				,'C_CELLNO3'
				,'CL_TE_TP1EML3'
			)

END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerHeader]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerHeader]
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewCustomerUpdateJob @BatchReference, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID


	--==================================================
	--3. CREATE ENTRY IN CUSTOMER UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.CustomerUpdateJobValue 
			(
				Id
				,JobTaskStatusId
				,CustomerId
				,sLastName
				,PhysAddrLine1
				,PhysAddrLine2
				,PhysAddrLine3
				,PhysAddrSuburb
				,PhysAddrPostCode
				,PhysAddrRatingArea
				,PostAddrLine1
				,PostAddrLine2
				,PostAddrLine3
				,PostAddrSuburb
				,PostAddrPostCode
				,PostAddrRatingArea
			)
	SELECT DISTINCT
		cfg.NewCombGUID()
		,@JobTaskId
		,CH.iCustomerID 
		,FS.CH_Surname
		,FS.CH_ResidentialAddress1
		,FS.CH_ResidentialAddress2
		,FS.CH_ResidentialAddressCitySuburb
		,FS.CH_ResidentialAddressCountryCode
		,FS.CH_ResidentialAddressPostalCode
		,FS.CH_ResidentialAddressCitySuburb
		,FS.CH_PostalAddress1
		,FS.CH_PostalAddress2
		,FS.CH_PostalAddressCitySuburb
		,FS.CH_PostalAddressCountryCode
		,FS.CH_PostalAddressPostalCode
		,FS.CH_PostalAddressCitySuburb
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
		LEFT JOIN [Address] RESADD ON
			RESADD.intAddressID = CH.rPhysAddressID
			AND UPPER(RESADD.strAddressType) = 'PHYSICAL'
		LEFT JOIN [Address] POSTADD ON
			POSTADD.intAddressID = CH.rPostAddressID
			AND UPPER(POSTADD.strAddressType) = 'POSTAL'

END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdatePolicyField]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdatePolicyField] 
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @EndorsementCode VARCHAR(15)
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewFieldUpdateJob @BatchReference, @EndorsementCode, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'


	--==================================================
	--3. CREATE ENTRY IN FIELD UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.FieldUpdateJobValue 
			(
			Id
			,JobTaskStatusId
			,ContextId
			,ContextType
			,FieldCode
			,sFieldValue
			,cFieldValue
			)
	SELECT 
		cfg.NewCombGUID()
		,@JobTaskId
		,PD.iPolicyID
		,2
		,PD.sFieldCode
		,CASE 
			PD.sFieldCode 
				WHEN 'P_MORTLOAACCNUM'	THEN FS.CH_CustomerPolicyLink 
			ELSE 
				PD.sFieldValue 
			END
		,1
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'


END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForFlash]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT EXCTRACTS ALL RECORDS FROM PROVIDED PARAMATER TABLE NAME TO BE UPDATED BY FLASH
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetAllDataForFlash] 
	(
	@TableName SYSNAME
	)

AS

BEGIN

	--DECLARE @TableName SYSNAME
	--SET @TableName = '[SBAB_src].[DailyLTINewBusiness]'

	--=====================================================
	--EXTRACT ALL DATA FOR FLASH TO PROCESS
	--=====================================================
	DECLARE 
		@SQLCommand varchar(8000)

	SET @SQLCommand = 'SELECT * INTO ski_int.##FlashData FROM ' + N'' + @TableName + ' WHERE sReason_Code NOT IN(''N'')';

	EXEC(@SQLCommand);

	--=====================================================
	--EXTRACT ALL CUSTOMER UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashCustomerSource]
	SELECT 
		'' JobReference, 
		C.iCustomerID CustomerId,
		CD.sFieldCode FieldCode,
		CD.sFieldValue FieldValue,
		CD.sFieldValue FieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Customer C WITH(NOLOCK) ON
			C.iCustomerID = P.iCustomerID
		INNER JOIN CustomerDetails CD WITH(NOLOCK) ON
			C.iCustomerID = CD.iCustomerID
	WHERE
		SD.sReason_Code IN('U','P');

	--=====================================================
	--EXTRACT ALL CANCEL DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashCancelSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		Getdate()
	FROM 
		ski_int.##FlashData SD
	WHERE
		SD.sReason_Code = 'C';

	--=====================================================
	--EXTRACT ALL SUSPEND DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashSuspendSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		Getdate()
	FROM 
		ski_int.##FlashData SD
	WHERE
		SD.sReason_Code = 'S';

	--=====================================================
	--EXTRACT ALL REINSTATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashReInstateSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		R.intRiskID,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
	WHERE
		SD.sReason_Code = 'R';

	--=====================================================
	--EXTRACT ALL POLICY UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashPolicyUpdateSource] 
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		PD.sFieldCode,
		PD.sFieldValue,
		PD.sFieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN PolicyDetails PD WITH(NOLOCK) ON
			P.iPolicyID = PD.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
	WHERE
		SD.sReason_Code IN('U','P');
		
	--=====================================================
	--EXTRACT ALL RISK UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashRiskUpdateSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		R.intRiskID,
		RD.sFieldCode,
		RD.sFieldValue,
		RD.sFieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
		INNER JOIN RiskDetails RD WITH(NOLOCK) ON
			R.intRiskID = RD.iRiskID
	WHERE
		SD.sReason_Code IN('U','P');

	--=====================================================
	--DROP TABLE ski_int.##FlashData IF EXISTS
	--=====================================================
    DROP TABLE 
		ski_int.##FlashData;

END 

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForSKiImporter]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetAllDataForSKiImporter] 
(
	@TableName SYSNAME
)
AS

BEGIN

	--=====================================================
	--GET ALL DATA WHERE IMPORTRECNO IS 999999
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'SELECT * FROM ' + N'' + @TableName + ' WHERE   iImportRecNo = 999999';

	EXEC(@SQLCommand);
	
END 

	
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetDataForSKiImporter]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetDataForSKiImporter] 

AS

BEGIN

	--=====================================================
	--GET ALL RECORDS
	--=====================================================
	SELECT 
		* 
	FROM 
		dbo.ImportRecord IR 
	WHERE
		IR.eProcStatus = 8
	ORDER BY 
		IR.iImportRecNo
	
END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetFlashProcessConfig]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetFlashProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		--INNER JOIN [ski_int].[SSISCommunicationType] CT (NOLOCK) ON
		--	C.CommunicationTypeId = CT.Id
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISSript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISSript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISSript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISSript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISSript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISSript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISSript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetJobSchedulerResults]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_GetJobSchedulerResults]

AS

BEGIN

	--==============================================================================================================
	--SELECT
	--==============================================================================================================
	SELECT 
	[sJOB].[name] AS [JobName]
    , CASE 
        WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
        ELSE CAST(
                CAST([sJOBH].[run_date] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [LastRunDateTime]
    , CASE [sJOBH].[run_status]
        WHEN 0 THEN 'Failed'
        WHEN 1 THEN 'Succeeded'
        WHEN 2 THEN 'Retry'
        WHEN 3 THEN 'Canceled'
        WHEN 4 THEN 'Running' -- In Progress
      END AS [LastRunStatus]
    , STUFF(
            STUFF(RIGHT('000000' + CAST([sJOBH].[run_duration] AS VARCHAR(6)),  6)
                , 3, 0, ':')
            , 6, 0, ':') 
        AS [LastRunDuration (HH:MM:SS)]
    , [sJOBH].[message] AS [LastRunStatusMessage]
    , CASE [sJOBSCH].[NextRunDate]
        WHEN 0 THEN NULL
        ELSE CAST(
                CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [NextRunDateTime]
	FROM 
		[msdb].[dbo].[sysjobs] AS [sJOB]
		LEFT JOIN (
					SELECT
						[job_id]
						, MIN([next_run_date]) AS [NextRunDate]
						, MIN([next_run_time]) AS [NextRunTime]
					FROM [msdb].[dbo].[sysjobschedules]
					GROUP BY [job_id]
				) AS [sJOBSCH]
			ON [sJOB].[job_id] = [sJOBSCH].[job_id]
		LEFT JOIN (
					SELECT 
						[job_id]
						, [run_date]
						, [run_time]
						, [run_status]
						, [run_duration]
						, [message]
						, ROW_NUMBER() OVER (
												PARTITION BY [job_id] 
												ORDER BY [run_date] DESC, [run_time] DESC
						  ) AS RowNumber
					FROM [msdb].[dbo].[sysjobhistory]
					WHERE [step_id] = 0
				) AS [sJOBH]
			ON [sJOB].[job_id] = [sJOBH].[job_id]
			AND [sJOBH].[RowNumber] = 1
	ORDER BY [JobName]

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcess]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcess]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessConfig]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessToExecute]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE FILE NAME THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcessToExecute] 
(
		@sFileDirection VARCHAR(50)
		,@sPackageNameToExecute VARCHAR(50)
		,@sFileName VARCHAR(255)
)
AS

BEGIN

	--==============================================================================================================
	--GET RELEVANT INFO OF PACKAGE TO EXECUTE
	--==============================================================================================================
	SELECT 
		MF.sPackageName
		,MF.sFileLocation
		,CASE WHEN MF.sPackageName = 'ClaimsMigration' THEN MF.sFileName ELSE MF.sFileName END AS 'sFileName'
		,MF.sFileProcessedLocation  
		,MF.sPackageFolderName
		,MF.sPackageProjectName
		,MS.iFileFormatID
	FROM 
		[ski_int].[IntegrationMasterFiles] MF (NOLOCK)
		INNER JOIN [ski_int].[IntegrationMasterScripts] MS (NOLOCK) ON 
			MF.ID = MS.iIntegrationMasterFileID
	WHERE 
		MF.sFileDirection = @sFileDirection 
		AND MF.sPackageName = @sPackageNameToExecute
		AND MF.sFileName = @sFileName
		AND MF.iStatusID = 1 


END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ImportLog_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_ImportLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @FileName NVARCHAR(255)
	, @FileRecordCount INT
	, @InsertCount INT
	, @UpdateCount INT
	, @CancelCount INT
	, @RejectCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISImportLog (
		JobID
		,ProcessName
		,JobReference
		,FileName
		,FileRecordCount
		,InsertCount
		,UpdateCount
		,CancelCount
		,RejectCount
		,StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @FileName
	, @FileRecordCount
	, @InsertCount
	, @UpdateCount
	, @CancelCount
	, @RejectCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_MonitorSKiImporterProcess]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT SHOWS THE AMOUNT OF RECORDS PROCESSED/UNPROCESSED FOR SKi IMPORTER
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_MonitorSKiImporterProcess] 

AS

BEGIN

	--=====================================================
	--GET TOTALS BY THE HOUR
	--=====================================================
	SELECT 
		'Import Date And Hour'				= DATEADD(HOUR,DATEDIFF(HOUR, 0, dDateCreated), 0)
		,'Total Imported Policies By Hour'	= COUNT(1)
	FROM 
		[dbo].[Policy]
	GROUP BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0)
	ORDER BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0);

	--=====================================================
	--GET SUMMARY
	--=====================================================
	SELECT 
		'Current Time'		= GETDATE()
		,'Process Status'	= CASE WHEN eProcStatus = 1 THEN 'Busy Importing' WHEN eProcStatus = 3 THEN 'Imports Completed' WHEN eProcStatus = 8 THEN 'To Be Imported' ELSE '' END
		,'Total Summary'	= COUNT(1) 
	FROM 
		ImportRecord 
	GROUP BY 
		eProcStatus;
	
END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ProgressLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ProgressLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ContinueNextStep INT
	, @Message NVARCHAR(255)
	,@Success INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISProgressLog (
	  JobID
	, ProcessName
	, JobReference
	, PackageName
	, TaskName
	, ContinueNextStep
	, Message
	, StartTime
	, Success
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ContinueNextStep
	, @Message
	, GetDate()
	, @Success
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_RaisingLog_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_RaisingLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @RaisingQueryCount INT
	, @RaisedCount INT
	, @NotRaisedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISRaisingLog (
		PackageLogID
		,ProcessName
		,JobReference
		,RaisingQueryCount
		,RaisedCount
		,NotRaisedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @RaisingQueryCount
	, @RaisedCount
	, @NotRaisedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ReceiptingLog_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ReceiptingLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @ReceiptingQueryCount INT
	, @ReceiptingCount INT
	, @NotReceiptedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISReceiptingLog (
		PackageLogID
		,ProcessName
		,JobReference
		,ReceiptingQueryCount
		,ReceiptingCount
		,NotReceiptedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @ReceiptingQueryCount
	, @ReceiptingCount
	, @NotReceiptedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartExtractLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_StartExtractLog]
  @PackageLogID INT
, @ProcessName NVARCHAR(255)
, @TableName NVARCHAR(100)
AS
BEGIN
	DECLARE @LastExtractDateTime	DATETIME
	SET NOCOUNT ON;
	
	SELECT @LastExtractDateTime = ISNULL(MAX(LastExtractDateTime), '1900-01-01')
	FROM ski_int.SSISExtractLog
	WHERE TableName = @TableName
		
	INSERT INTO ski_int.SSISExtractLog (
	  PackageLogID
	, ProcessName
	, TableName
	, StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @TableName
	, GetDate()
	)

	SELECT 
	  CAST(Scope_Identity() AS INT) ExtractLogID
	, CONVERT(NVARCHAR(23), @LastExtractDateTime, 121) LastExtractDateTimeString
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartJob]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENTRY [ski_int].[SSISJob] EVERY TIME A NEW SSIS JOB IS EXECUTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [ski_int].[SSIS_StartJob]
	@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)

AS

BEGIN
	--==============================================================================================================
	--SAMPLE EXECUTE
	--==============================================================================================================
	--EXEC [ski_int].[SSIS_StartJob] 'Import Daily LTI File - BW','REFERENCE1'

	--==============================================================================================================
	--INSERT JOB
	--==============================================================================================================
	INSERT INTO [ski_int].[SSISJob]
	(
		ProcessName
		,JobReference
		,StartTime
		,EndTime
		,DateCreated
		,Success
	)
	VALUES 
	(
		@ProcessName
		,@JobReference
		,GetDate()
		,NULL
		,GetDate()
		,0
	);

	--==============================================================================================================
	--RETURN LATEST JOB ID
	--==============================================================================================================
	SELECT CAST(Scope_Identity() AS INT) SSISJobID;

END






GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartPackageLog]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_StartPackageLog] 
	@JobID INT
	,@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO ski_int.SSISPackageLog 
	(
		JobID,
		ProcessName,
		JobReference,
		PackageName,
		StartTime
	)
	VALUES 
	(
		@JobID,
		@ProcessName,
		@JobReference,
		@PackageName,
		GetDate()
	);

	SELECT MAX(PackageLogID) FROM ski_int.SSISPackageLog;

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_SuspendLog_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_SuspendLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @SuspendQueryCount INT
	, @SuspendedCount INT
	, @NotSuspendedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISSuspendLog (
		PackageLogID
		,ProcessName
		,JobReference
		,SuspendQueryCount
		,SuspendedCount
		,NotSuspendedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @SuspendQueryCount
	, @SuspendedCount
	, @NotSuspendedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES EPROCSTATUS TO 777 FOR ALL RECORDS IN IMPORTRECORD TABLE. THESE RECORDS
--				:	WILL BE UPDATED IN BATCHES OF 1000 RECORDS TO EPROCSTATUS 1, TO IMPROVE EFFICIENCY
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter] 
(
	@UserName VARCHAR(100)
	,@TableName SYSNAME
)
AS

BEGIN

	--=====================================================
	--UPDATE USER ACCOUNT TO NOT EXPIRE
	--=====================================================
	UPDATE SecUsers SET dLastUpdated = GETDATE() WHERE sUserLogin = @UserName;

	--=====================================================
	--UPDATE IMPORTRECNO TO 999999 - TO BE EXECUTED IN BATCHES
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'UPDATE ' + N'' + @TableName + ' SET iImportRecNo = 999999 WHERE iImportRecNo = -1'

	EXEC(@SQLCommand);
	
END 







GO
/****** Object:  StoredProcedure [ski_int].[SSISFileLocations_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISFileLocations_Insert]
	 @LocationId VARCHAR(100)
	,@FileName VARCHAR(100)
	,@FileType INT
	,@FileHasHeaderRow INT
	,@FileHasFooterRow INT
	,@PickupLocation VARCHAR(255)
	,@ProcessedLocation VARCHAR(255)
	,@ArchiveLocation VARCHAR(255)
	,@LogFileLocation VARCHAR(255)
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISFileLocations]
		(
			LocationId
			,FileName
			,FileType
			,FileHasHeaderRow
			,FileHasFooterRow
			,PickupLocation
			,ProcessedLocation
			,ArchiveLocation
			,LogFileLocation
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			 @LocationId
			,@FileName 
			,@FileType
			,@FileHasHeaderRow 
			,@FileHasFooterRow 
			,@PickupLocation 
			,@ProcessedLocation 
			,@ArchiveLocation 
			,@LogFileLocation 
			,@UserLastUpdated 
			,GetDate()
		   );

END

GO
/****** Object:  StoredProcedure [ski_int].[SSISProcess_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISProcess_Insert]
	@Name VARCHAR(100)
	,@Description VARCHAR(400)
	,@ProcessTypeId INT
	,@CommunicationId INT
	,@FileLocationId INT
	,@ScriptId INT
	,@SSISDBCatalogFolderName VARCHAR(100)
	,@SSISDBCatalogProjectName VARCHAR(100)
	,@SSISDBCatalogPackageName VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISProcess]
		(
			Name
			,Description
			,ProcessTypeId
			,CommunicationId
			,FileLocationId
			,ScriptId
			,SSISDBCatalogFolderName
			,SSISDBCatalogProjectName
			,SSISDBCatalogPackageName
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@Name 
			,@Description 
			,@ProcessTypeId 
			,@CommunicationId 
			,@FileLocationId 
			,@ScriptId 
			,@SSISDBCatalogFolderName 
			,@SSISDBCatalogProjectName 
			,@SSISDBCatalogPackageName 
			,@Enabled 
			,@UserLastUpdated 
			,GetDate()
		   );

END

GO
/****** Object:  StoredProcedure [ski_int].[SSISScript_Insert]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISScript_Insert]
	@ScriptId INT
	,@ScriptTypeId INT
	,@Name VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISScript]
		(
			ScriptId
			,ScriptTypeId
			,Name
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@ScriptId
			,@ScriptTypeId
			,@Name
			,@Enabled
			,@UserLastUpdated
			,GetDate()
		   );

END

GO
/****** Object:  StoredProcedure [ski_int].[uspAddBatchUser]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[uspAddBatchUser]
    @pLogin NVARCHAR(50), 
    @pPassword NVARCHAR(50), 
    @responseMessage NVARCHAR(250) OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO [ski_int].[BatchUser] (UserName, Password)
        VALUES(@pLogin, HASHBYTES('SHA2_512', @pPassword))

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END


GO
/****** Object:  StoredProcedure [ski_int].[uspAddSSISUser]    Script Date: 16/10/2017 17:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[uspAddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[BatchUser];

	INSERT INTO [ski_int].[BatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END


GO
