/****** Object:  Table [ski_int].[AllPremiumFiles]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[AllPremiumFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[AllPremiumFiles](
	[Id] [int] NULL,
	[Name] [varchar](max) NULL,
	[FileName] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[BatchUser]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[BatchUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[CollectionImport]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[CollectionImport]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[CollectionImport](
	[Id] [uniqueidentifier] NOT NULL,
	[PolicyId] [float] NOT NULL,
	[Productid] [int] NOT NULL,
	[FileSumInsured] [money] NOT NULL,
	[FilePremium] [money] NOT NULL,
	[SortByDate] [date] NOT NULL,
	[TransactionPeriod] [date] NOT NULL,
 CONSTRAINT [batch_ImportFileData_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[DailyImportExceptions]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[DailyImportExceptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[DailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL CONSTRAINT [Imported_D_0]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[DailyLTINewBusiness_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[DailyLTINewBusiness_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[DailyLTINewBusiness_Log](
	[sRecord_Id] [varchar](1) NULL,
	[dtTransaction_Date] [datetime] NULL,
	[sReason_Code] [varchar](2) NULL,
	[sCIF_Number] [varchar](50) NULL,
	[sCustomer_Title] [varchar](30) NULL,
	[sCustomer_First_Name] [varchar](30) NULL,
	[sCustomer_Surname] [varchar](30) NULL,
	[sGender] [varchar](1) NULL,
	[dtDate_of_Birth] [datetime] NULL,
	[sID_Passport_Number] [varchar](20) NULL,
	[sPostal_Address_1] [varchar](30) NULL,
	[sPostal_Address_2] [varchar](30) NULL,
	[sPostal_Address_3] [varchar](30) NULL,
	[sPostal_Code] [varchar](10) NULL,
	[sTel_No] [varchar](25) NULL,
	[sEmployer_Name] [varchar](50) NULL,
	[sEmployer_Contact_Tel_No] [varchar](25) NULL,
	[sMarital_Status] [varchar](5) NULL,
	[sLoan_Account_No] [varchar](16) NULL,
	[sSOL_ID] [varchar](8) NULL,
	[cLoan_Amount] [numeric](18, 2) NULL,
	[sLoan_Term] [varchar](3) NULL,
	[sPlan_Type] [varchar](2) NULL,
	[dtLoan_Value_Date] [datetime] NULL,
	[cPremium] [numeric](18, 2) NULL,
	[dtRepayment_Date] [datetime] NULL,
	[sSecond_Life_Insured_Title] [varchar](30) NULL,
	[sSecond_Life_Insured_First_Name] [varchar](30) NULL,
	[sSecond_Life_Insured_Surname] [varchar](30) NULL,
	[sSecond_Life_Insured_ID_Passport_No] [varchar](20) NULL,
	[sSecond_Life_DOB] [date] NULL,
	[sLTI_Flag] [int] NULL,
	[dtLTI_Expiry] [datetime] NULL,
	[cOutstanding_Balance] [numeric](18, 2) NULL,
	[sScheme_Code] [varchar](2) NULL,
	[sCurrency_Code] [varchar](3) NULL,
	[sInsurer_Own_insurance] [varchar](50) NULL,
	[sPolicy_ID_Own_insurance] [varchar](15) NULL,
	[cAmount_Own_insurance] [numeric](18, 2) NULL,
	[dtExpiry_Date_Own_Insurance] [datetime] NULL,
	[sProduct_Scheme_Code] [varchar](5) NULL,
	[sSegment] [varchar](3) NULL,
	[sSub_segment] [varchar](10) NULL,
	[sSerial_Number] [varchar](5) NULL,
	[sUnderwriter_Name] [varchar](20) NULL,
	[sPIN_number] [varchar](10) NULL,
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NOT NULL,
	[dtProcessDate] [datetime] NOT NULL,
	[iSourceFileID] [bigint] NOT NULL,
	[sNewLoanAccountNo] [varchar](18) NOT NULL,
	[sCountry] [varchar](50) NULL,
	[sCountryClassification] [varchar](5) NULL,
	[sCountry_Cif] [varchar](50) NULL,
	[iCountryID] [int] NULL,
	[RH_CoInsurer] [varchar](50) NULL,
	[PH_ProductID] [int] NULL,
	[FileName] [varchar](255) NULL,
 CONSTRAINT [PK_DailyLTINewBusiness_Log] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[DailySTINewBusiness_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[DailySTINewBusiness_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[DailySTINewBusiness_Log](
	[sRecord_Id] [varchar](1) NULL,
	[dtTransaction_Date] [datetime] NULL,
	[sReason_Code] [varchar](2) NULL,
	[sCIF_Number] [varchar](50) NULL,
	[sCustomer_Title] [varchar](30) NULL,
	[sCustomer_First_Name] [varchar](30) NULL,
	[sCustomer_Surname] [varchar](30) NULL,
	[sGender] [varchar](1) NULL,
	[dtDate_of_Birth] [datetime] NULL,
	[sID_Passport_Number] [varchar](20) NULL,
	[sPostal_Address_1] [varchar](30) NULL,
	[sPostal_Address_2] [varchar](30) NULL,
	[sPostal_Address_3] [varchar](30) NULL,
	[sPostal_Code] [varchar](10) NULL,
	[sTel_No] [varchar](25) NULL,
	[sEmployer_Name] [varchar](50) NULL,
	[sEmployer_Contact_Tel_No] [varchar](25) NULL,
	[sMarital_Status] [varchar](5) NULL,
	[sLoan_Account_No] [varchar](16) NULL,
	[sSOL_ID] [varchar](8) NULL,
	[cLoan_Amount] [numeric](18, 2) NULL,
	[sPhys_Add1] [varchar](30) NULL,
	[sPhys_Add2] [varchar](30) NULL,
	[sPhys_Add3] [varchar](30) NULL,
	[dtLoan_Value_Date] [datetime] NULL,
	[cPremium] [numeric](18, 2) NULL,
	[cProperty_value] [numeric](18, 2) NULL,
	[dtRepayment_Date] [datetime] NULL,
	[dtPolicy_Expiry_Date] [datetime] NULL,
	[sScheme_code] [varchar](2) NULL,
	[sCurrency_code] [varchar](3) NULL,
	[sConstruction_Type] [varchar](4) NULL,
	[sCover_Note] [varchar](1) NULL,
	[sPremium_Payment_Frequency] [varchar](1) NULL,
	[sInsurer_Own_insurance] [varchar](50) NULL,
	[sPolicy_ID_Own_insurance] [varchar](15) NULL,
	[cAmount_Own_insurance] [numeric](18, 2) NULL,
	[dtExpiry_Date_Own_Insurance] [datetime] NULL,
	[sLoan_Account_No_Own_Insurance] [varchar](16) NULL,
	[sProduct_Scheme_Code] [varchar](5) NULL,
	[sSegment] [varchar](3) NULL,
	[sSub_segment] [varchar](10) NULL,
	[sSerial_Number] [varchar](5) NULL,
	[sRisk_Address] [varchar](100) NULL,
	[sUnderwriter_Name] [varchar](20) NULL,
	[sPIN_number] [varchar](10) NULL,
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NOT NULL,
	[dtProcessDate] [datetime] NOT NULL,
	[iSourceFileID] [bigint] NOT NULL,
	[sNewLoanAccountNo] [varchar](18) NOT NULL,
	[sCountry] [varchar](50) NULL,
	[sCountryClassification] [varchar](5) NULL,
	[sCountry_Cif] [varchar](50) NULL,
	[iCountryID] [int] NULL,
	[PH_ProductID] [int] NULL,
	[RH_CoInsurer] [varchar](50) NULL,
	[Nasria] [money] NULL,
	[StampDuty] [money] NULL,
	[Namfisa] [money] NULL,
	[NamfisaComm] [money] NULL,
	[RiskPremCPC] [money] NULL,
	[RiskNasriaCPC] [money] NULL,
	[RiskComm] [money] NULL,
	[FileName] [varchar](255) NULL,
 CONSTRAINT [PK_DailySTINewBusiness_Log] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[DynamicSQLStatements]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[DynamicSQLStatements]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[DynamicSQLStatements](
	[ExecuteOrder] [int] NULL,
	[sMainTableName] [varchar](255) NULL,
	[sSQLStatement] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_SKiHeaderFields]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSIS_SKiHeaderFields]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSIS_SKiHeaderFields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FieldLevel] [varchar](15) NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[FieldDescription] [varchar](255) NOT NULL,
 CONSTRAINT [PK_SKiHeaderFields] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_Src_RawData]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSIS_Src_RawData]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSIS_Src_RawData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NOT NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL,
 CONSTRAINT [PK_SSIS_Src_RawData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_Src_RawData_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSIS_Src_RawData_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSIS_Src_RawData_Log](
	[ID] [int] NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSIS_Src_RawData_UG]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSIS_Src_RawData_UG]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSIS_Src_RawData_UG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NOT NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISBatchUser]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISBatchUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISBatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISCommunication]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISCommunication]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationId] [int] NOT NULL,
	[CommunicationTypeId] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[InAttachment] [bit] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISCommunicationType]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISCommunicationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISCommunicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISDataErrorLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISDataErrorLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISDataErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorSource] [nvarchar](255) NOT NULL,
	[SourceRecord] [nvarchar](255) NOT NULL,
	[ErrorNumber] [nvarchar](100) NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISEmailConfig]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISEmailConfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISEmailConfig](
	[uID] [int] NULL,
	[MailServer] [varbinary](100) NOT NULL,
	[MailPort] [int] NOT NULL,
	[MailUserName] [varchar](100) NOT NULL,
	[MailPassword] [varbinary](100) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISErrorLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISErrorLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISExtractLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISExtractLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISExtractLog](
	[ExtractLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[ExtractCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[LastExtractDateTime] [datetime] NULL,
	[Success] [bit] NOT NULL,
 CONSTRAINT [PK_SSISExtractLog] PRIMARY KEY CLUSTERED 
(
	[ExtractLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISFileLocations]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFileLocations]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFileLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [varchar](100) NULL,
	[FileName] [varchar](100) NULL,
	[FileType] [int] NULL,
	[FileHasHeaderRow] [bit] NULL,
	[FileHasFooterRow] [bit] NULL,
	[PickupLocation] [varchar](255) NULL,
	[ProcessedLocation] [varchar](255) NULL,
	[ArchiveLocation] [varchar](255) NULL,
	[LogFileLocation] [varchar](255) NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFileType]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFileType]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFileType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](10) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCancelSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashCancelSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashCancelSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCancelSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCancelSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashCancelSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashCancelSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCancelSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCustomerSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashCustomerSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashCustomerSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[CustomerId] [int] NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCustomerSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashCustomerSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashCustomerSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashCustomerSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[CustomerId] [int] NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashCustomerSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashPolicyUpdateSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashPolicyUpdateSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashPolicyUpdateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashPolicyUpdateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashPolicyUpdateSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashPolicyUpdateSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashPolicyUpdateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashPolicyUpdateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashReInstateSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashReInstateSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashReInstateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[RiskId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashReInstateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashReInstateSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashReInstateSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashReInstateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[RiskId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashReInstateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashRiskUpdateSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashRiskUpdateSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashRiskUpdateSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[RiskId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashRiskUpdateSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashRiskUpdateSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashRiskUpdateSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashRiskUpdateSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [float] NOT NULL,
	[RiskId] [float] NOT NULL,
	[FieldCode] [varchar](15) NOT NULL,
	[sFieldValue] [varchar](15) NULL,
	[cFieldValue] [float] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashRiskUpdateSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashSuspendSource]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashSuspendSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashSuspendSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashSuspendSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISFlashSuspendSource_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISFlashSuspendSource_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISFlashSuspendSource_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[ProductId] [int] NULL,
	[PolicyId] [int] NULL,
	[ProcessDate] [datetime] NULL,
 CONSTRAINT [PK_SSISFlashSuspendSource_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISImportLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISImportLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISImportLog](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISImportLogTest]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISImportLogTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISImportLogTest](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISIntegrationLookupMap]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISIntegrationLookupMap]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISIntegrationLookupMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[SourceGroup] [varchar](50) NOT NULL,
	[SourceDBValue] [varchar](255) NOT NULL,
	[TargetGroup] [varchar](255) NOT NULL,
	[TargetDBValue] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CL_LookupMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISJob]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISJob]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISJob](
	[SSISJobID] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[Success] [bit] NULL CONSTRAINT [DF_SSISJobID_Success]  DEFAULT ((0)),
 CONSTRAINT [PK_SSISJob] PRIMARY KEY CLUSTERED 
(
	[SSISJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISMailsToBeSent]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISMailsToBeSent]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISMailsToBeSent](
	[MailID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[MailFrom] [varchar](200) NULL,
	[MailTO] [varchar](400) NULL,
	[MailCC] [varchar](200) NULL,
	[MailBCC] [varchar](200) NULL,
	[MailSubject] [varchar](200) NULL,
	[MailBody] [varchar](max) NULL,
	[DateToSend] [date] NULL,
	[Sent] [bit] NULL,
	[DateSent] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISPackageLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISPackageLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISPackageLog](
	[PackageLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISPackageLog] PRIMARY KEY CLUSTERED 
(
	[PackageLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISProcess]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISProcess]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISProcess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](400) NULL,
	[ProcessTypeId] [int] NULL,
	[CommunicationId] [int] NULL,
	[FileLocationId] [int] NULL,
	[ScriptId] [int] NULL,
	[SSISDBCatalogFolderName] [varchar](100) NULL,
	[SSISDBCatalogProjectName] [varchar](100) NULL,
	[SSISDBCatalogPackageName] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISProcessType]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISProcessType]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISProcessType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ScriptId] [int] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISProgressLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISProgressLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISProgressLog](
	[ProgressLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ContinueNextStep] [bit] NULL,
	[Message] [nvarchar](255) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NOT NULL CONSTRAINT [DF_ProgressLog_Success]  DEFAULT ((0)),
 CONSTRAINT [PK_SSISProgressLog] PRIMARY KEY CLUSTERED 
(
	[ProgressLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISRaisingLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISRaisingLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISRaisingLog](
	[RaisingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[RaisingQueryCount] [int] NOT NULL,
	[RaisedCount] [int] NOT NULL,
	[NotRaisedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISReceiptingLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISReceiptingLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISReceiptingLog](
	[ReceiptingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[ReceiptingQueryCount] [int] NOT NULL,
	[ReceiptingCount] [int] NOT NULL,
	[NotReceiptedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[SSISScript]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISScript]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISScript](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScriptId] [int] NOT NULL,
	[ScriptTypeId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISScriptType]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISScriptType]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISScriptType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ski_int].[SSISSuspendLog]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SSISSuspendLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[SSISSuspendLog](
	[SuspendLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[SuspendQueryCount] [int] NOT NULL,
	[SuspendedCount] [int] NOT NULL,
	[NotSuspendedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [ski_int].[StandaloneCrossSell_Log]    Script Date: 16/10/2017 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[StandaloneCrossSell_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [ski_int].[StandaloneCrossSell_Log](
	[sRecord_Id] [varchar](1) NULL,
	[sTransaction_Date] [varchar](8) NULL,
	[sReason_Code] [varchar](2) NULL,
	[sCIF_Number] [varchar](50) NULL,
	[sCustomer_Title] [varchar](30) NULL,
	[sCustomer_First_Name] [varchar](30) NULL,
	[sCustomer_Surname] [varchar](30) NULL,
	[sGender] [varchar](1) NULL,
	[sDate_of_Birth] [date] NULL,
	[sID_Passport_Number] [varchar](20) NULL,
	[sPostal_Address_1] [varchar](30) NULL,
	[sPostal_Address_2] [varchar](30) NULL,
	[sPostal_Address_3] [varchar](30) NULL,
	[sPostal_Code] [varchar](10) NULL,
	[sTel_No] [varchar](25) NULL,
	[sEmployer_Name] [varchar](50) NULL,
	[sEmployer_Contact_Tel_No] [varchar](25) NULL,
	[sMarital_Status] [varchar](5) NULL,
	[sAccount_No] [varchar](16) NULL,
	[sSerial_Number] [varchar](10) NULL,
	[sProduct_Scheme_Code] [varchar](16) NULL,
	[sSOL_ID] [varchar](8) NULL,
	[sSegment] [varchar](3) NULL,
	[sSub_segment] [varchar](3) NULL,
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NOT NULL,
	[ProcessDate] [datetime] NOT NULL,
	[sCountry] [varchar](3) NOT NULL,
	[iSourceFileID] [bigint] NOT NULL,
	[sCountry_Cif] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[ski_int].[DF_DataErrorLog_ErrorTime]') AND type = 'D')
BEGIN
ALTER TABLE [ski_int].[SSISDataErrorLog] ADD  CONSTRAINT [DF_DataErrorLog_ErrorTime]  DEFAULT (getdate()) FOR [ErrorTime]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[ski_int].[DF_ExtractLog_ExtractCount]') AND type = 'D')
BEGIN
ALTER TABLE [ski_int].[SSISExtractLog] ADD  CONSTRAINT [DF_ExtractLog_ExtractCount]  DEFAULT ((0)) FOR [ExtractCount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[ski_int].[DF_ExtractLog_Success]') AND type = 'D')
BEGIN
ALTER TABLE [ski_int].[SSISExtractLog] ADD  CONSTRAINT [DF_ExtractLog_Success]  DEFAULT ((0)) FOR [Success]
END

GO
