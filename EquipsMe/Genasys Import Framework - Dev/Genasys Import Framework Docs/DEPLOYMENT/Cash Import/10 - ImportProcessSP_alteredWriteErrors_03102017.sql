SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.ImportBatch')) 
	DROP PROCEDURE [batch].[ImportBatch]
GO

CREATE PROCEDURE [batch].[ImportBatch] (@BatchNo int)
AS
BEGIN 
	DECLARE @Username varchar(20) = 'SYSTEM'
	DECLARE @TransactionDate datetime = (SELECT TOP 1 ip.TransactionPeriod FROM batch.ImportPolicy ip WITH (NOLOCK) WHERE ip.BatchNumber = @BatchNo)
	DECLARE @TransactionStart datetime = (DATEADD(mm, DATEDIFF(mm, 0, @TransactionDate), 0)) --Period Start, First day of the month of the transactions
	DECLARE @TransactionEnd datetime = (DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @TransactionDate) + 1, 0))) --PeriodEnd, Last day of the month of the transaction

	IF OBJECT_ID('tempdb..#ImportBatchNewTranIds') IS NOT NULL
		DROP TABLE #ImportBatchNewTranIds

	CREATE TABLE #ImportBatchNewTranIds
	(
		[PolicyId] [FLOAT] NOT NULL,
		[TranId] [numeric] NOT NULL
	)

	DECLARE @StartTranID numeric = (SELECT Max(iTranId) FROM Policy WITH (NOLOCK))

	INSERT INTO #ImportBatchNewTranIds (PolicyId, TranId)
	SELECT 
		PolicyId,
		@StartTranID + (rank() OVER (ORDER BY PolicyId))
	
	FROM batch.ImportPolicy WITH (NOLOCK)
	WHERE BatchNumber = @BatchNo


--Disable the Policy_Insert Trigger to prevent multiple Arch entries
	;DISABLE TRIGGER dbo.Policy_Insert ON dbo.Policy;

--Disable the trg_CoinsRiskRelHistory Trigger to prevent it being triggered the whole time
	;DISABLE TRIGGER dbo.trg_CoinsRiskRelHistory ON dbo.Policy;

--Disable the Risk_Insert Trigger to prevent multiple Arch entries
	;DISABLE TRIGGER dbo.Risks_Insert ON dbo.Risks

--***************************************************************************************************************************************************
-- Insert new RiskCoverType if SumInsured differs
--***************************************************************************************************************************************************
BEGIN TRY
	INSERT INTO RiskCoverType (sRiskClass, iRisktypeID, iRiskID, sFieldCode, cSumInsured, bInclRiskSI, cPremiumPerc, sUserLastUpdated, dLastUpdated, bLinkedSI, CoverTypeID)
	SELECT 
		rct.sRiskClass,
		r.intRiskTypeID,
		r.intRiskID,
		rct.sFieldCode,
		--r.cSumInsured,
		ip.FileSumInsured,
		rct.bInclRiskSI,
		rct.cPremiumPerc,
		@Username as 'sUserLastUpdated',
		GETDATE() as 'dLastUpdated',
		rct.bLinkedSI,
		--r.CoverTypeID,
		(
			CASE	
				WHEN r.cSumInsured <> ip.FileSumInsured THEN
					cfg.NewCombGUID()
				ELSE
					r.CoverTypeID
			END
		) AS 'NewCoverTypeId'

	FROM 
		Risks r WITH (NOLOCK)
		INNER JOIN batch.ImportPolicy ip WITH (NOLOCK) ON ip.PolicyId = r.intPolicyID AND ip.BatchNumber = @BatchNo AND (r.cSumInsured <> ip.FileSumInsured)
		INNER JOIN RiskCoverType rct WITH (NOLOCK) ON rct.CoverTypeID = r.CoverTypeID
END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error inserting RiskCoverTypes' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error inserting RiskCoverTypes', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Update CPCs etc on RiskDetails
--***************************************************************************************************************************************************
BEGIN TRY
	UPDATE rd
	SET
		rd.iFieldType = fc.iFieldType,
		rd.sFieldValue = 
				(
					CASE 
						WHEN ((fc.iFieldType IN (0, 6)) OR (fc.iFieldType = 7 AND fc.sLookupGroup = 'Commercial Prem Calc')) AND ru.Value IS NOT NULL THEN
							ru.Value
						ELSE
							rd.sFieldValue
					END	
				),
		rd.cFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType IN (1, 8, 9, 10) AND ru.Value IS NOT NULL THEN
							CAST(ru.Value AS float) 
						ELSE
							rd.cFieldValue
					END	
				),
		rd.bFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType = 2 AND ru.Value IS NOT NULL THEN
							CAST(ru.Value AS bit) 
						ELSE
							rd.bFieldValue
					END	
				),
		rd.dtFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType IN (3, 5) AND ru.Value IS NOT NULL THEN
							CAST(ru.Value AS datetime) 
						ELSE
							rd.dtFieldValue
					END	
				),
		rd.iFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType = 4 AND ru.Value IS NOT NULL THEN
							CAST(ru.Value AS int) 
						ELSE
							rd.iFieldValue
					END	
				)
	FROM 
		batch.ImportRiskFieldUpdate ru WITH (NOLOCK)
		INNER JOIN batch.ImportPolicy ip WITH (NOLOCK) ON ip.Id = ru.ImportPolicyId AND ip.BatchNumber = @BatchNo
		INNER JOIN Risks r WITH (NOLOCK) ON r.intPolicyID = ip.PolicyId
		INNER JOIN RiskDetails rd WITH (NOLOCK) ON rd.iRiskID = r.intRiskID and rd.sFieldCode = ru.RiskField
		INNER JOIN FieldCodes fc WITH (NOLOCK) ON fc.sFieldCode = ru.RiskField

	UPDATE rd
	SET 
		rd.iTranId = itran.TranId
	FROM 
		#ImportBatchNewTranIds itran
		INNER JOIN Risks r WITH (NOLOCK) ON r.intPolicyID = itran.PolicyId AND r.intStatus = 2
		INNER JOIN RiskDetails rd WITH (NOLOCK) ON rd.iRiskID = r.intRiskID

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error updating RiskDetails' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error updating RiskDetails', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Update Risk Headers (SumInsured and RiskCoverType)
--***************************************************************************************************************************************************
BEGIN TRY
	;WITH RiskCT AS
	(
		SELECT 
			rct.iRiskID,
			r.cSumInsured,
			ip.FileSumInsured,
			MAX(rct.CoverTypeID) as 'CoverTypeId'
		FROM 
			batch.ImportPolicy ip WITH (NOLOCK) 
			INNER JOIN Risks r WITH (NOLOCK) ON r.intPolicyID = ip.PolicyId AND (r.cSumInsured <> ip.FileSumInsured) AND ip.BatchNumber = @BatchNo 
			INNER JOIN RiskCoverType rct WITH (NOLOCK) ON rct.iRiskID = r.intRiskID
		GROUP BY 
			rct.iRiskID, r.cSumInsured, ip.FileSumInsured
	) 
	UPDATE r
	SET 
		r.cSumInsured = rct.FileSumInsured,
		r.CoverTypeID = rct.CoverTypeId
	FROM 
		Risks r WITH (NOLOCK) 
		INNER JOIN RiskCT rct ON rct.iRiskID = r.intRiskID

	UPDATE r
	SET 
		r.iTranId = itran.TranId
	FROM 
		#ImportBatchNewTranIds itran 
		INNER JOIN Risks r WITH (NOLOCK) ON r.intPolicyID = itran.PolicyId AND r.intStatus = 2
END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error updating Risk Headers' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error updating Risk Headers', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Update Policy Details (e.g. Fees)
--***************************************************************************************************************************************************
BEGIN TRY
	UPDATE pd
	SET
		pd.iFieldType = fc.iFieldType,
		pd.sFieldValue = 
				(
					CASE 
						WHEN ((fc.iFieldType IN (0, 6)) OR (fc.iFieldType = 7 AND fc.sLookupGroup = 'Commercial Prem Calc')) AND pu.Value IS NOT NULL THEN
							pu.Value
						ELSE
							pd.sFieldValue
					END	
				),
		pd.cFieldValue =
				(
					CASE 
						WHEN fc.iFieldType IN (1, 8, 9, 10) AND pu.Value IS NOT NULL THEN
							CAST(pu.Value AS float) 
						ELSE
							pd.cFieldValue
					END	
				),
		pd.bFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType = 2 AND pu.Value IS NOT NULL THEN
							CAST(pu.Value AS bit) 
						ELSE
							pd.bFieldValue
					END	
				),
		pd.dtFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType IN (3, 5) AND pu.Value IS NOT NULL THEN
							CAST(pu.Value AS datetime) 
						ELSE
							pd.dtFieldValue
					END	
				),
		pd.iFieldValue = 
				(
					CASE 
						WHEN fc.iFieldType = 4 AND pu.Value IS NOT NULL THEN
							CAST(pu.Value AS int) 
						ELSE
							pd.iFieldValue
					END	
				)
	FROM 
		batch.ImportPolicyFieldUpdate pu WITH (NOLOCK)
		INNER JOIN batch.ImportPolicy ip WITH (NOLOCK) ON ip.Id = pu.ImportPolicyId AND ip.BatchNumber = @BatchNo
		INNER JOIN PolicyDetails pd WITH (NOLOCK) ON pd.iPolicyID = ip.PolicyId and pd.sFieldCode = pu.PolicyField
		INNER JOIN FieldCodes fc WITH (NOLOCK) ON fc.sFieldCode = pu.PolicyField

	UPDATE pd
	SET 
		pd.iTranId = itran.TranId
	FROM 
		#ImportBatchNewTranIds itran 
		INNER JOIN PolicyDetails pd WITH (NOLOCK) ON pd.iPolicyID = itran.PolicyId

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error updating PolicyDetails' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error updating PolicyDetails', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Update Policy Header (e.g. Fees)
--***************************************************************************************************************************************************
BEGIN TRY
	UPDATE P
		SET 
			p.cAdminFee = ip.AdminFee,
			p.cBrokerFee = ip.BrokerFee,
			p.cPolicyFee = ip.PolicyFee
	FROM 
			batch.ImportPolicy ip WITH (NOLOCK) 
			INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = ip.PolicyId  AND ip.BatchNumber = @BatchNo 

	UPDATE p
	SET 
		p.iTranId = itran.TranId
	FROM 
		#ImportBatchNewTranIds itran 
		INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = itran.PolicyId
END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error updating Policy Headers' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error updating Policy Headers', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Rerate the Policy
--***************************************************************************************************************************************************
BEGIN TRY
--Re-enable the Risk_Insert Trigger, Before the last Risk update in the process
	;ENABLE TRIGGER dbo.Risks_Insert ON dbo.Risks


	;WITH inputs AS
	(
		SELECT
			ip.PolicyId, MAX(CASE WHEN (ISNULL(pd.bFieldValue, 0) = 1) THEN 0 ELSE 1 END) AS CalcComm , r.intRiskID, r.intRiskTypeID, r.iPaymentTerm, r.cCommissionRebate,
			MAX(CASE WHEN (rd.sFieldCode = 'R_TSASRIA') THEN dbo.CalcCommCalcPremium(rd.sFieldValue) ELSE 0 END) AS AnnualSasria,
			SUM(CASE WHEN (rd.sFieldCode <> 'R_TSASRIA' AND (fc.sLookupGroup = 'Commercial Prem Calc')) THEN dbo.CalcCommCalcPremium(rd.sFieldValue) ELSE 0 END) AS AnnualPremium,
			MAX(CASE WHEN (rd.sFieldCode = 'R_MATCHINGPERC') THEN CAST(ISNULL(rd.sFieldValue, 0) AS FLOAT)/100.0 + 1.0 ELSE 1 END) AS MatchingModifier,
			MAX(rd.cFieldValue) AS CommPerc
		FROM
			batch.ImportPolicy ip WITH (NOLOCK)
			LEFT OUTER JOIN PolicyDetails pd WITH (NOLOCK) ON (pd.iPolicyID = ip.PolicyId) AND (pd.sFieldCode = 'P_NOCOMMISSION') AND (pd.bFieldValue = 1)
			INNER JOIN Risks r WITH (NOLOCK) ON (r.intPolicyID = ip.PolicyId) AND (r.intStatus < 3)
			INNER JOIN RiskDetails rd WITH (NOLOCK) ON (rd.iRiskID = r.intRiskID)
			INNER JOIN FieldCodes fc WITH (NOLOCK) ON (fc.sFieldCode = rd.sFieldCode)
		WHERE
			ip.BatchNumber = @BatchNo AND
			(((fc.iFieldType = 7) AND (fc.sLookupGroup = 'Commercial Prem Calc'))
				OR ((fc.iFieldType IN (1, 9, 10)) AND (fc.sFieldCode LIKE 'R_COM%') AND (fc.sDescription LIKE '%Commission%'))
				OR (fc.sFieldCode = 'R_MATCHINGPERC'))
		GROUP BY
			ip.PolicyId, r.intRiskID, r.intRiskTypeID, r.iPaymentTerm, r.cCommissionRebate
	),
	commAndFees AS
	(
		SELECT PolicyId, intRiskID, intRiskTypeID, iPaymentTerm,
			cCommissionRebate,
			AnnualPremium * MatchingModifier AS AnnualPremium,
			AnnualSasria,
			AnnualPremium * MatchingModifier * CommPerc * CalcComm / 100.0 AS AnnualCommission,
			AnnualSasria * dbo.GetSasriaIntermediaryFeePercentage(inputs.intRiskID) / 100.0 AS AnnualSasriaComm
		FROM inputs
	),
	annualValues AS
	(
		SELECT PolicyId, intRiskID, pt.fValue / 12.0 AS TermMultiplier,
			AnnualPremium - (AnnualCommission * cCommissionRebate) / 100.0 AS AnnualPremium,
			AnnualSasria,
			AnnualCommission * (100.0 - cCommissionRebate) / 100.0 + AnnualSasriaComm AS AnnualCommission
		FROM commAndFees
		INNER JOIN Lookup pt WITH (NOLOCK, NOEXPAND) ON (pt.sGroup = 'Payment Term') AND (pt.iIndex = commAndFees.iPaymentTerm)
	)
	UPDATE r
	SET
		r.cPremium = ROUND((v.AnnualPremium + v.AnnualSasria) * v.TermMultiplier, 2),
		r.cSASRIA = ROUND(v.AnnualSasria * v.TermMultiplier, 2),
		r.cCommission = ROUND(v.AnnualCommission * v.TermMultiplier, 2),
		r.cAnnualPremium = v.AnnualPremium + v.AnnualSasria,
		r.cAnnualComm = v.AnnualCommission,
		r.cAnnualSASRIA = v.AnnualSasria,
		r.dEffectiveDate = @TransactionStart,
		r.dDateModified = GETDATE() 
	FROM Risks r WITH (NOLOCK)
		INNER JOIN annualValues v ON (r.intRiskId = v.intRiskId) AND (r.intPolicyId = v.PolicyId) AND (v.AnnualPremium > 0)

	;WITH totals AS
	(
		SELECT ip.PolicyId, SUM(r.cAnnualPremium) AS AnnualPremium, SUM(r.cAnnualComm) AS AnnualCommission, SUM(r.cAnnualSASRIA) AS AnnualSasria, SUM(r.cPremium) AS NextPaymentAmt
		FROM
			batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN Risks r WITH (NOLOCK) ON (r.intPolicyID = ip.PolicyId) AND (r.intStatus < 3)
		WHERE ip.BatchNumber = @BatchNo
		GROUP BY ip.PolicyId
	)
	UPDATE p
	SET p.cAnnualPremium = t.AnnualPremium,
		p.cAnnualSASRIA = t.AnnualSasria,
		p.cCommissionAmt = t.AnnualCommission,
		p.cNextPaymentAmt = t.NextPaymentAmt,
		p.dEffectiveDate = @TransactionStart,
		p.dDateModified = GETDATE()
	FROM Policy p WITH (NOLOCK)
		INNER JOIN batch.ImportPolicy ip WITH (NOLOCK) ON (ip.PolicyId = p.iPolicyID)
		INNER JOIN totals t ON (t.PolicyId = ip.PolicyId) AND (ip.PolicyId = p.iPolicyID)
	WHERE ip.BatchNumber = @BatchNo

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error Rerating Policies' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error Rerating Policies', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Create a Policy Endorsement
--***************************************************************************************************************************************************
BEGIN TRY
--Re-enable the Policy_Insert Trigger, before the last Policy update in the process
	;ENABLE TRIGGER dbo.Policy_Insert ON dbo.Policy;

--Re-enable the trg_CoinsRiskRelHistory Trigger, before the last Policy update in the process
	;ENABLE TRIGGER dbo.trg_CoinsRiskRelHistory ON dbo.Policy;

--CREATE ENDORSEMENT
	;WITH policyIds AS
    (
        SELECT DISTINCT ip.PolicyID
        FROM batch.ImportPolicy ip WITH (NOLOCK)
        WHERE ip.BatchNumber = @BatchNo 
    )
    UPDATE p
    SET 
		p.sComment = 'Batch ReRate by: ' + @Username + ' on ' + CAST(GETDATE() AS varchar(30)),
		p.iEndorsementID = cfg.NewCombGUID(),
        p.sEndorseCode = '17' --14 = General, 18 = Endorsement, 17 = System
    FROM
        Policy p WITH (NOLOCK)
        INNER JOIN policyIds ids ON (ids.PolicyID = p.iPolicyID)
    WHERE (p.iPolicyStatus IN (0, 1, 2, 3, 5))

    ;WITH endorsementIds AS
    (
        SELECT DISTINCT p.iEndorsementID, ip.PolicyID,
			CASE iPolicyStatus
			WHEN 2 THEN 3
			WHEN 3 THEN 6
			ELSE 2 END AS [iEndorseType]
        FROM batch.ImportPolicy ip WITH (NOLOCK)
            INNER JOIN Policy p WITH (NOLOCK) ON (p.iPolicyID = ip.PolicyId) AND (p.iPolicyStatus IN (0, 1, 2, 3, 5))
        WHERE ip.BatchNumber = @BatchNo 
    )
    INSERT INTO uw.PolicyEndorsementHistory (ID, iPolicyID, iEndorseType)
    SELECT iEndorsementID, PolicyId, iEndorseType
    FROM endorsementIds

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error Creating Policy Endorsements' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 

	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error Creating Policy Endorsements', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Raise Journal
--***************************************************************************************************************************************************
BEGIN TRY
--Activate Pending Policies
	--UPDATE p 
	--SET 
	--	p.iPolicyStatus = 1, 
	--	p.sComment = 'Batch Activation by: ' + @Username + ' on ' + CAST(GETDATE() as varchar(30)), 
	--	p.dDateModified = getdate()
 --   FROM batch.ImportPolicy ip WITH (NOLOCK)
	--	INNER JOIN Policy P WITH (NOLOCK) ON ip.PolicyID = p.iPolicyID
	--WHERE 
	--	(p.iPolicyStatus = 0) AND (ip.BatchNumber = @BatchNo)

--Get Batch PolicyTransaction IDs
	IF OBJECT_ID('tempdb..#ImportBatchTransIds') IS NOT NULL
		DROP TABLE #ImportBatchTransIds

	CREATE TABLE #ImportBatchTransIds
	(
		[PolicyId] [FLOAT] NOT NULL,
		[NextId] [FLOAT] NOT NULL,
		[RunReference] varchar(255) NOT NULL,
		[TransactionPeriod] datetime NOT NULL
	)

	DECLARE @maxTranId int = (SELECT ISNULL(MAX(iTransactionID), 0) FROM PolicyTransactions WITH (NOLOCK)) --14295264

	INSERT INTO #ImportBatchTransIds (PolicyId, NextId, RunReference, TransactionPeriod)
	SELECT 
		PolicyId,
		(ROW_NUMBER() OVER (PARTITION BY ip.BatchNumber ORDER BY p.iPolicyId) + @maxTranId) as 'TransactionId',
		ip.RunReference,
		ip.TransactionPeriod
	FROM 
		batch.ImportPolicy ip WITH (NOLOCK)
		INNER JOIN Policy P WITH (NOLOCK) ON ip.PolicyID = p.iPolicyID
	WHERE 
		ip.BatchNumber = @BatchNo AND (p.bFinanceEnabled = 1)

--Insert PolicyTransactionDetails
	;WITH PolicyInfo AS
	(
		SELECT ip.PolicyId, temp.NextId,
			--p.dNextPaymentDate AS PeriodStart, (DATEADD(MONTH, pterm.fValue, p.dNextPaymentDate) - 1) AS PeriodEnd, p.dExpiryDate AS CoverEnd,
			--batch.CalculatePeriodMultiplier(p.dNextPaymentDate, (DATEADD(MONTH, pterm.fValue, p.dNextPaymentDate) - 1), p.dExpiryDate, pterm.iIndex) AS Multiplier
			--temp.TransactionPeriod AS dDateActioned,
			batch.CalculatePeriodMultiplier(@TransactionStart, @TransactionEnd, @TransactionEnd, pterm.iIndex) AS Multiplier
		FROM batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN #ImportBatchTransIds temp ON (temp.PolicyId = ip.PolicyId)
			INNER JOIN Policy p WITH (NOLOCK) ON (ip.PolicyId = p.iPolicyID)
			INNER JOIN Lookup pterm WITH (NOLOCK, NOEXPAND) ON (pterm.sGroup = 'Payment Term') AND (pterm.iIndex = p.iPaymentTerm)
		WHERE ip.BatchNumber = @BatchNo
	)
	INSERT INTO PolicyTransactionDetails(iTransactionID,  iPolicyID,  iRiskID,  iRiskTypeID,  dDateActioned,  iTranType,  cPremium,  cCommission,  cAdminFee,  cSASRIA,  sUser)
	SELECT 
		pInfo.NextId,
		pInfo.PolicyId,
		r.intRiskID,
		r.intRiskTypeID,
		GETDATE(),
		8,
		ROUND(CAST(r.cPremium * pInfo.Multiplier AS MONEY), 2),
		ROUND(CAST(r.cCommission * pInfo.Multiplier AS MONEY), 2),
		ROUND(CAST(r.cAdminFee * pInfo.Multiplier AS MONEY), 2),
		ROUND(CAST(r.cSASRIA * pInfo.Multiplier AS MONEY), 2),
		@Username
	FROM PolicyInfo pInfo
		INNER JOIN Risks r WITH (NOLOCK) ON (r.intPolicyID = pInfo.PolicyID) AND (r.intStatus = 2)

--Insert PolicyTransactions
	;WITH PolicyInfo AS
    (
        SELECT ip.PolicyId, temp.NextId,
            --p.dNextPaymentDate AS PeriodStart, (DATEADD(MONTH, pterm.fValue, p.dNextPaymentDate) - 1) AS PeriodEnd, p.dExpiryDate AS CoverEnd,
            --batch.CalculatePeriodMultiplier(p.dNextPaymentDate, (DATEADD(MONTH, pterm.fValue, p.dNextPaymentDate) - 1), p.dExpiryDate, pterm.iIndex) AS Multiplier,
			--temp.TransactionPeriod AS dTransaction,
			batch.CalculatePeriodMultiplier(@TransactionStart, @TransactionEnd, @TransactionEnd, pterm.iIndex) AS Multiplier,
            pterm.fValue / 12 as Ratio,
			ip.RunReference
        FROM batch.ImportPolicy ip WITH (NOLOCK)
            INNER JOIN #ImportBatchTransIds temp ON (temp.PolicyId = ip.PolicyId)
            INNER JOIN Policy p WITH (NOLOCK) ON (ip.PolicyId = p.iPolicyID)
            INNER JOIN Lookup pterm WITH (NOLOCK, NOEXPAND) ON (pterm.sGroup = 'Payment Term') AND (pterm.iIndex = p.iPaymentTerm)
        WHERE ip.BatchNumber = @BatchNo
    )
	INSERT INTO PolicyTransactions(iTransactionID, iPolicyID, sReference,  iAgencyID,  iTranType, cPremium, cCommission, cSASRIA, cAdminFee, dTransaction,
        sUser, bReplicated, dPeriodStart, dPeriodEnd, cPolicyFee, iParentTranID, iDebitNoteID, cBrokerFee, dEffective, sFinAccount, iSourceCurrency, sPolicyGUID, iCoverTermID)
    SELECT pInfo.NextId,
        pInfo.PolicyId,
        pInfo.RunReference,
        p.iAgencyID,
        9,
        SUM(ptd.cPremium),
        SUM(ptd.cCommission),
        SUM(ptd.cSASRIA),
        CASE WHEN adminFee.fValue = 1 THEN SUM(ptd.cAdminFee) ELSE ROUND(CAST((p.cAdminFee * pInfo.Ratio) * pInfo.Multiplier AS MONEY), 2) END,
		@TransactionDate,
        @Username,
        0,
        @TransactionStart, --Period Start, First day of the month of the transactions
        @TransactionEnd, --PeriodEnd, Last day of the month of the transactions
        ROUND(CAST((p.cPolicyFee * pInfo.Ratio) * pInfo.Multiplier AS MONEY), 2),
        -1,
        pInfo.NextId,
        ROUND(CAST((p.cBrokerFee * pInfo.Ratio) * pInfo.Multiplier AS MONEY), 2),
        p.dEffectiveDate, --Period Start, First day of the month of the transactions
        p.sFinAccount,
        p.iCurrency,
        p.sGUID,
        p.iCoverTermID
    FROM PolicyInfo pInfo
        INNER JOIN Policy p WITH (NOLOCK) ON (p.iPolicyID = pInfo.PolicyId)
        INNER JOIN PolicyTransactionDetails ptd WITH (NOLOCK) ON (ptd.iTransactionID = pInfo.NextId) AND (ptd.iPolicyID = p.iPolicyID) 
        INNER JOIN Lookup adminFee WITH (NOLOCK, NOEXPAND) ON (adminFee.sGroup = 'Implementation') AND (adminFee.sDBValue = 'RISKADMIN')
    GROUP BY pInfo.NextId, pInfo.PolicyId, p.iAgencyID, adminFee.fValue, p.cAdminFee, p.cPolicyFee, p.dNextPaymentDate, p.cBrokerFee, pInfo.Multiplier, p.dEffectiveDate,p.sFinAccount,
        p.iCurrency,
        p.sGUID,
        p.iCoverTermID, pInfo.Ratio, pInfo.RunReference

--Insert DebitnoteRiskCoInsRel
	INSERT DebitnoteRiskCoInsRel
		SELECT pt.iDebitNoteID, pt.iPolicyID, ins.iRiskID, CoIns.sCoInsurer, CoIns.cPerc, CoIns.bPrinciple, pt.sUser, @TransactionDate, coins.iCoinsAgreementID
	FROM PolicyTransactions pt WITH (NOLOCK) 
		INNER JOIN PolicyTransactionDetails ins WITH (NOLOCK) ON (ins.iTransactionID = pt.iTransactionID)
		INNER JOIN #ImportBatchTransIds temp ON (pt.iTransactionID = temp.NextID) AND (temp.PolicyId = pt.iPolicyId)
		INNER JOIN CoInsurersRiskRel CoIns WITH (NOLOCK) ON (CoIns.iRiskID = ins.iRiskID)
	WHERE (pt.iparenttranid = -1) 
	AND (pt.iTranType in (1,3,5,6,9)) 
	AND (pt.iDebitNoteID > 0)

/*
--Insert Collection History
	DECLARE @NewHisGuid AS UNIQUEIDENTIFIER = [cfg].[NewCombGUID]();
	--DECLARE @nextPayDate datetime = @TransactionEnd + 1
	--(SELECT TOP 1 p.dNextPaymentDate FROM #ImportBatchTransIds temp INNER JOIN Policy p ON (p.iPolicyID = temp.PolicyID))
	--DECLARE @RaiseStartDate datetime = (SELECT TOP 1 temp.TransactionPeriod FROM #ImportBatchTransIds temp)--GETDATE()
	DECLARE @RunReference varchar(255) = (SELECT TOP 1 RunReference FROM #ImportBatchTransIds)

	IF (@BatchNo > 1)
		SET @RunReference = @RunReference + 'B' + CAST(@BatchNo AS varchar(20))

	INSERT PremCollectionHist(ID, sReference, dPayDate, dExtract, cValue, dRunStartDate)
	SELECT
		@NewHisGuid,
		@RunReference,
		@TransactionEnd + 1,
		GETDATE(),
		SUM(pt.cPremium + pt.cAdminFee + pt.cBrokerFee + pt.cPolicyFee),
		@TransactionDate
	FROM
		#ImportBatchTransIds temp
		INNER JOIN PolicyTransactions pt WITH (NOLOCK) ON (pt.iTransactionID = temp.NextId) AND (pt.iPolicyID = temp.PolicyID)

	;WITH pcpm AS
	(
		SELECT DISTINCT @NewHisGuid AS NewHisGuid, p.iPaymentMethod
		FROM #ImportBatchTransIds temp
			INNER JOIN Policy p WITH (NOLOCK) ON (temp.PolicyId = p.iPolicyID)
	)
	INSERT PremCollectionPayMethod (ID, CollectionID, PaymentMethod)
	SELECT cfg.NewCombGUID(), pcpm.NewHisGuid, pcpm.iPaymentMethod
	FROM pcpm
*/

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error Creating Journals' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error Creating Journals', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Auto Deposit money for Receipts
--***************************************************************************************************************************************************
BEGIN TRY
--Get batch Account Transactions IDs for Deposits
	IF OBJECT_ID('tempdb..#ImportBatchAccTransIds') IS NOT NULL
		DROP TABLE #ImportBatchAccTransIds

	CREATE TABLE #ImportBatchAccTransIds
	(
		[PolicyId] [FLOAT] NOT NULL,
		[NextId] [FLOAT] NOT NULL,
		[DepositAmount] money NOT NULL,
		[RunReference] varchar(255) NOT NULL,
		[TranPeriod] datetime NOT NULL
	)

	DECLARE @maxAccTranId int = (select isnull(max(iTransactionID), 0) as ID from AccountSharedData WITH (NOLOCK)) --14295264

	INSERT INTO #ImportBatchAccTransIds (PolicyId, NextId, DepositAmount, RunReference, TranPeriod)
	SELECT 
		PolicyId,
		(ROW_NUMBER() OVER (PARTITION BY ip.BatchNumber ORDER BY p.iPolicyId) + @maxAccTranId) as 'AccountTranId',
		ip.FilePremium,
		ip.RunReference,
		ip.TransactionPeriod
	FROM 
		batch.ImportPolicy ip WITH (NOLOCK)
		INNER JOIN Policy P WITH (NOLOCK) ON ip.PolicyID = p.iPolicyID
	WHERE 
		ip.BatchNumber = @BatchNo 

--Insert Account entries
	INSERT INTO AccountPolicy
    SELECT
	    b.NextId AS iBankDepositID,
	    b.NextId AS iTransactionID,
	    p.iCustomerID,
	    p.iPolicyID,
	    1 AS iTranType,
	    p.iCurrency AS iSourceCurrency,
	    -CAST(b.DepositAmount AS money) AS cSourceAmount,
	    p.iCurrency AS iTargetCurrency,
	    -CAST(b.DepositAmount AS money) AS cTargetAmount,
	    1 AS cTargetExchangeRate,
	    1 AS eContraAccount
    FROM
	    #ImportBatchAccTransIds b
	    INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = b.PolicyId

    INSERT INTO AccountPolicyBalance
    SELECT
	    b.NextId AS iBankDepositID,
	    b.PolicyId AS iPolicyId,
	    -CAST(b.DepositAmount AS money) AS cBalance
    FROM
	    #ImportBatchAccTransIds b

    INSERT INTO AccountBank
    SELECT
	    b.NextId AS iBankDepositID,
	    b.NextId AS iTransactionID,
	    5 AS iSourceType,
	    p.iPolicyId AS iSourceId,
	    'DEFAULT' AS sFinAccount,
	    1 AS iTranType,
	    p.iCurrency AS iCurrency,
	    CAST(b.DepositAmount AS money) AS cGrossAmount,
	    0 AS cDeductionAmount,
	    CAST(b.DepositAmount AS money) AS cNettAmount,
	    6 AS eContraAccount,
	    0 AS bNonCashDeposit,
	    'Debit' AS sPaymentMethod,
	    'Auto Deposit' as sComment
    FROM
	    #ImportBatchAccTransIds b
	    INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = b.PolicyId

    INSERT INTO AccountBankBalance
    SELECT
	    b.NextId AS iBankDepositID,
	    CAST(b.DepositAmount AS money) AS cBalance
    FROM
	    #ImportBatchAccTransIds b

    INSERT INTO AccountSharedData
	    SELECT
		    b.NextId AS iTransactionID,
		    b.RunReference AS sReference,
		    @TransactionDate AS dProductionDate,		
		    @TransactionDate AS dEffectiveDate,
		    1 AS eCommPayMethod,
		    @Username AS sUser
	    FROM
		    #ImportBatchAccTransIds b
            INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = b.PolicyId

END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error Inserting Auto Deposits' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage; 
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error Inserting Auto Deposits', 16, 1)
	RETURN
END CATCH

--***************************************************************************************************************************************************
-- Receipt Journals
--***************************************************************************************************************************************************
BEGIN TRY
--Get Batch Policy transaction IDs for Receipts
	IF OBJECT_ID('tempdb..#ImportBatchReceiptTransIds') IS NOT NULL
		DROP TABLE #ImportBatchReceiptTransIds

	CREATE TABLE #ImportBatchReceiptTransIds
	(
		[PolicyId] [FLOAT] NOT NULL,
		[ParentTranId] [FLOAT] NOT NULL,
		[NextId] [FLOAT] NOT NULL,
		[RunReference] varchar(255)
	)

	DECLARE @maxReceiptTranId int = (SELECT ISNULL(MAX(iTransactionID), 0) FROM PolicyTransactions WITH (NOLOCK))

	INSERT INTO #ImportBatchReceiptTransIds (PolicyId, ParentTranId, NextId, RunReference)
	SELECT 
		ip.PolicyId,
		ppt.NextId,
		(ROW_NUMBER() OVER (PARTITION BY ip.BatchNumber ORDER BY p.iPolicyId) + @maxReceiptTranId) as 'TransactionId',
		ip.RunReference
	FROM 
		batch.ImportPolicy ip WITH (NOLOCK)
		INNER JOIN #ImportBatchTransIds ppt ON ip.PolicyId = ppt.PolicyId
		INNER JOIN Policy P WITH (NOLOCK) ON ip.PolicyID = p.iPolicyID
	WHERE 
		ip.BatchNumber = @BatchNo AND (p.bFinanceEnabled = 1)

--Insert PolicyTransactionDetails and PolicyTransactions
	IF OBJECT_ID('tempdb..#TempImportPolicyTransactionDetails') IS NOT NULL
		DROP TABLE #TempImportPolicyTransactionDetails

    CREATE TABLE #TempImportPolicyTransactionDetails
    (
		[iTransactionID] [float],
		[iPolicyID] [float],
		[iRiskID] [float],
		[iRiskTypeID] [int],
		[dDateActioned] [datetime],
		[iTranType] [int],
		[cPremium] [money],
		[cCommission] [money],
		[cAdminFee] [money],
		[cSASRIA] [money],
		[sUser] varchar(20)
    )

    INSERT INTO #TempImportPolicyTransactionDetails
		SELECT
			b.NextID AS iTransactionID,
			ptd.iPolicyID,
			ptd.iRiskID,
			ptd.iRiskTypeID,
			GETDATE() AS dDateActioned,
			6 as iTranType,
			-SUM(ptd.cPremium) AS cPremium,
			-SUM(ptd.cCommission) AS cCommission,
			-SUM(ptd.cAdminFee) AS cAdminFee,
			-SUM(ptd.cSASRIA) AS cSASRIA,
			@UserName AS sUSer
		FROM
			#ImportBatchReceiptTransIds b
			INNER JOIN PolicyTransactionDetails ptd WITH (NOLOCK) ON b.PolicyID = ptd.iPolicyID
			INNER JOIN PolicyTransactions opt WITH (NOLOCK) ON ptd.iTransactionID = opt.iTransactionId AND b.ParentTranId = opt.iDebitNoteId
		GROUP BY ptd.iPolicyID, opt.iDebitNoteId, ptd.iRiskID, ptd.iRiskTypeID, b.NextId

    ;WITH PolicyTransDetailsSums AS
    (
	    SELECT
		    nptd.iTransactionID,
		    nptd.iPolicyID,
		    SUM(nptd.cPremium) AS cPremium,
		    SUM(nptd.cCommission) AS cCommission,
		    SUM(nptd.cAdminFee) as cAdminFee,
		    SUM(nptd.cSASRIA) AS cSasria
	    FROM
		    #ImportBatchReceiptTransIds b
		    INNER JOIN #TempImportPolicyTransactionDetails nptd on nptd.iTransactionID = b.NextID
	    GROUP BY nptd.iTransactionID, nptd.iPolicyID, b.ParentTranId
    )
    INSERT INTO PolicyTransactions
	    SELECT
			b.NextID as iTransactionId,
			opt.iPolicyID,
			b.RunReference + '_' + CAST(p.iPaymentMethod AS varchar(3)) AS sReference,
			opt.iAgencyId,
			2 as iTranType,
			ptds.cPremium,
			ptds.cCommission,
			CASE
				WHEN ISNULL(l.fValue, 0) = 0 then -opt.cAdminFee
			ELSE
				ptds.cAdminFee
			END AS [cAdminFee],
			ptds.cSASRIA,
			opt.dTransaction,
			@Username AS sUser,
			0 AS bReplicated,
			0 AS eExtraTranProp,
			opt.dPeriodStart,
			opt.dPeriodEnd,
			opt.iCommStatus,
			b.ParentTranId AS iParentTranID,
			opt.iDebitNoteId,
			-opt.cPolicyFee,
			opt.sArchivedBy,
			opt.dArchived,
			'' AS sComment,
			@TransactionDate AS dEffective,
			-opt.cBrokerFee,
			'DEFAULT' as sFinAccount,
			p.iCurrency as iSourceCurrency,
			p.sGUID AS sPolicyGUID,
			opt.iCoverTermID
	FROM
		#ImportBatchReceiptTransIds b
		INNER JOIN PolicyTransactions opt WITH (NOLOCK) on opt.iTransactionId = b.ParentTranId
		INNER JOIN PolicyTransDetailsSums ptds ON ptds.iPolicyID = opt.iPolicyId and ptds.iTransactionID = b.NextId
		INNER JOIN Policy p WITH (NOLOCK) on p.iPolicyID = opt.iPolicyId
        LEFT OUTER JOIN lookup l WITH (NOLOCK, NOEXPAND) ON l.sGroup = 'implementation' AND l.sDBValue = 'RISKADMIN'

	INSERT INTO PolicyTransactionDetails
		SELECT * FROM #TempImportPolicyTransactionDetails

	IF OBJECT_ID('tempdb..#TempPolicyTransactionDetails') IS NOT NULL
		DROP TABLE #TempPolicyTransactionDetails
END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error Inserting Receipts' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage;
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error Inserting Receipts', 16, 1)
	RETURN
END CATCH

BEGIN TRY --Receipt Account Entries
--Get batch Account Transaction Ids for Receipts
	IF OBJECT_ID('tempdb..#ImportBatchReceiptAccTransIds') IS NOT NULL
		DROP TABLE #ImportBatchReceiptAccTransIds

	CREATE TABLE #ImportBatchReceiptAccTransIds
	(
		[PolicyId] [FLOAT] NOT NULL,
		[NextId] [FLOAT] NOT NULL,
		[DepositAmount] money NOT NULL,
		[RunReference] varchar(255) NOT NULL,
		[TranPeriod] datetime NOT NULL
	)

	DECLARE @maxReceiptAccTranId int = (select isnull(max(iTransactionID), 0) as ID from AccountSharedData WITH (NOLOCK)) --14295264

	INSERT INTO #ImportBatchReceiptAccTransIds (PolicyId, NextId, DepositAmount, RunReference, TranPeriod)
	SELECT 
		PolicyId,
		(ROW_NUMBER() OVER (PARTITION BY ip.BatchNumber ORDER BY p.iPolicyId) + @maxReceiptAccTranId) as 'AccountTranId',
		ip.FilePremium,
		ip.RunReference,
		ip.TransactionPeriod
	FROM 
		batch.ImportPolicy ip WITH (NOLOCK)
		INNER JOIN Policy P WITH (NOLOCK) ON ip.PolicyID = p.iPolicyID
	WHERE 
		ip.BatchNumber = @BatchNo

--INSERT AccountPolicy Values
    INSERT INTO AccountPolicy
	    SELECT
		    ab.iBankDepositID,
		    ids.NextId,
		    p.iCustomerID,
		    ids.PolicyID,
		    2,
		    p.iCurrency,
		    ids.DepositAmount,
		    p.iCurrency,
		    ids.DepositAmount,
		    1,
		    7
	    FROM
			AccountBank ab WITH (NOLOCK)
			INNER JOIN #ImportBatchAccTransIds bd ON bd.NextId = ab.iBankDepositID
		    INNER JOIN #ImportBatchReceiptAccTransIds ids ON ids.PolicyId = bd.PolicyId
		    INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = ids.PolicyID


	UPDATE apb 
        SET apb.cBalance = 0
    FROM
		AccountPolicyBalance apb WITH (NOLOCK)
	    INNER JOIN #ImportBatchAccTransIds b ON b.NextId = apb.iBankDepositID and b.PolicyId = apb.iPolicyID

--INSERT AccountPremiumAllocated Values
    INSERT INTO AccountPremiumAllocated
	    SELECT
		    araise.NextId,
		    areceipt.NextId,
		    areceipt.PolicyID,
		    pt.iDebitNoteId,
		    rt.NextId,
		    p.iCurrency,
		    2,
		    -areceipt.DepositAmount,
		    6
	    FROM
			PolicyTransactions pt WITH (NOLOCK)
			INNER JOIN #ImportBatchReceiptTransIds rt ON pt.iTransactionId = rt.NextId
			INNER JOIN #ImportBatchReceiptAccTransIds areceipt ON areceipt.PolicyId = rt.PolicyId
		    INNER JOIN #ImportBatchAccTransIds araise ON araise.PolicyId = areceipt.PolicyId
		    INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = areceipt.PolicyID

--INSERT AccountPremiumAllocatedBalance
	INSERT INTO AccountPremiumAllocatedBalance
		SELECT 
			araise.NextId,
			pt.iDebitNoteId,
			-araise.DepositAmount
	    FROM
			PolicyTransactions pt WITH (NOLOCK)
			INNER JOIN #ImportBatchReceiptTransIds rt ON pt.iTransactionId = rt.NextId
			INNER JOIN #ImportBatchReceiptAccTransIds areceipt ON areceipt.PolicyId = rt.PolicyId
		    INNER JOIN #ImportBatchAccTransIds araise ON araise.PolicyId = areceipt.PolicyId

--INSERT AccountSharedData
    INSERT INTO AccountSharedData
	    SELECT
		    ids.NextID,
		    'AA-' + ids.RunReference + '_' + CAST(p.iPaymentMethod AS varchar(3)) AS sReference,
			@TransactionDate AS dProductionDate,
		    @TransactionDate AS dEffectiveDate,
		    1 AS eCommPayMethod,
		    @Username AS sUser
	    FROM
			#ImportBatchReceiptAccTransIds ids
			INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = ids.PolicyId
END TRY
BEGIN CATCH
    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Error inserting Receipt Account Entries' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage;
	--SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage; 
	RAISERROR('Error inserting Receipt Account Entries', 16, 1)
	RETURN
END CATCH

	IF OBJECT_ID('tempdb..#ImportBatchTransIds') IS NOT NULL
		DROP TABLE #ImportBatchTransIds

	IF OBJECT_ID('tempdb..#ImportBatchAccTransIds') IS NOT NULL
		DROP TABLE #ImportBatchAccTransIds


	IF OBJECT_ID('tempdb..#ImportBatchReceiptTransIds') IS NOT NULL
		DROP TABLE #ImportBatchReceiptTransIds

	IF OBJECT_ID('tempdb..#ImportBatchReceiptAccTransIds') IS NOT NULL
		DROP TABLE #ImportBatchReceiptAccTransIds

	IF OBJECT_ID('tempdb..#ImportBatchNewTranIds') IS NOT NULL
		DROP TABLE #ImportBatchNewTranIds
END
GO


IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.ImportProcess')) 
	DROP PROCEDURE [batch].[ImportProcess]
GO

CREATE PROCEDURE [batch].[ImportProcess]
AS
BEGIN 
	DECLARE @CurrentDate datetime = GETDATE()
	DECLARE @Username varchar(20) = 'SYSTEM'

	BEGIN TRANSACTION

	BEGIN TRY

	--Get Cancelled PolicyIds IDs
		IF OBJECT_ID('tempdb..#ImportBatchCancelledIds') IS NOT NULL
			DROP TABLE #ImportBatchCancelledIds

		CREATE TABLE #ImportBatchCancelledIds
		(
			[PolicyId] [FLOAT] NOT NULL
		)

		INSERT INTO #ImportBatchCancelledIds (PolicyId)
		SELECT DISTINCT ip.PolicyId FROM batch.ImportPolicy ip WITH (NOLOCK) WHERE ip.PolicyStatus = 2

--***************************************************************************************************************************************************
-- ReInstate Cancelled Policies
--***************************************************************************************************************************************************
		UPDATE p
			SET
				p.iPolicyStatus = 1,
				p.sActionedBy = @Username,
				p.sComment = 'Batch ReInstatement by: ' + @Username + ' on ' + CAST(@CurrentDate as varchar(30)),
				p.dDateModified =  @CurrentDate,
				p.sGUID = cfg.NewCombGUID(),
				p.iEndorsementID = cfg.NewCombGUID(),
				p.sEndorseCode = '17', --14 = General, 18 = Endorsement, 17 = System
				p.dDateModified = @CurrentDate
		FROM
			batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN #ImportBatchCancelledIds cp ON ip.PolicyId = cp.PolicyId
			INNER JOIN Policy p WITH (NOLOCK) ON (p.iPolicyID = ip.PolicyId)

		UPDATE r
		SET
			r.intStatus = 2,
			r.sLastUpdatedBy = @Username,
			r.dDateModified = @CurrentDate
		FROM
			Risks r WITH (NOLOCK)
			INNER JOIN #ImportBatchCancelledIds cp ON r.intPolicyId = cp.PolicyId and (r.intStatus = 3)

		;WITH EndorsementIds AS
		(
			SELECT DISTINCT p.iEndorsementID, ip.PolicyID, 4 AS 'iEndorseType'
			FROM batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN Policy p ON (p.iPolicyID = ip.PolicyId)
		)
		INSERT INTO uw.PolicyEndorsementHistory (ID, iPolicyID, iEndorseType)
		SELECT 
			ei.iEndorsementID, ei.PolicyId, ei.iEndorseType
		FROM 
			EndorsementIds ei
			INNER JOIN #ImportBatchCancelledIds cp ON ei.PolicyId = cp.PolicyId



--***************************************************************************************************************************************************
-- Process Batches
--***************************************************************************************************************************************************
		DECLARE @MaxBatch int = (select MAX(BatchNumber) from batch.ImportPolicy)

		DECLARE @BatchCnt int = 0

		WHILE (@BatchCnt < @MaxBatch)
		BEGIN
			SET @BatchCnt = @BatchCnt + 1


			EXEC batch.ImportBatch @BatchCnt

		END

		
--***************************************************************************************************************************************************
-- Re-Cancel Cancelled Policies
--***************************************************************************************************************************************************
		SET @CurrentDate = GETDATE()	

		UPDATE p
		SET 
			--p.dCancelled = p.dNextPaymentDate,
			--p.dEffectiveDate = p.dNextPaymentDate,
			p.sComment = 'Batch Cancellation by: ' + @Username + ' on ' + CAST(@CurrentDate as varchar(30)),
			p.sActionedBy = @Username,
			--p.sCancelReason = p.sCancelReason, --The Policy was cancelled before so there should be reasons
			--p.CancelReasonCode = p.CancelReasonCode,
			p.sGUID = cfg.NewCombGUID(),
			p.iPolicyStatus = 2,
			p.iEndorsementID = cfg.NewCombGUID(),
			p.dDateModified = @CurrentDate
			--,p.sEndorseCode = '17' --14 = General, 18 = Endorsement, 17 = System -- Cancellation does not changes this
		FROM
			batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN #ImportBatchCancelledIds cp ON ip.PolicyId = cp.PolicyId
			INNER JOIN Policy p WITH (NOLOCK) ON (p.iPolicyID = ip.PolicyId)

		UPDATE r
		SET
			r.intStatus = 3,
			r.sLastUpdatedBy = @Username,
			r.dDateModified = @CurrentDate
		FROM
			Risks r WITH (NOLOCK)
			INNER JOIN #ImportBatchCancelledIds cp ON r.intPolicyID = cp.PolicyId and (r.intStatus = 2)

		;WITH EndorsementIds AS
		(
			SELECT DISTINCT p.iEndorsementID, ip.PolicyID, 3 AS 'iEndorseType'
			FROM batch.ImportPolicy ip WITH (NOLOCK)
			INNER JOIN Policy p ON (p.iPolicyID = ip.PolicyId)
		)
		INSERT INTO uw.PolicyEndorsementHistory (ID, iPolicyID, iEndorseType)
		SELECT 
			ei.iEndorsementID, ei.PolicyId, ei.iEndorseType
		FROM 
			EndorsementIds ei
			INNER JOIN #ImportBatchCancelledIds cp on ei.PolicyId = cp.PolicyId

		IF OBJECT_ID('tempdb..#ImportBatchCancelledIds') IS NOT NULL
			DROP TABLE #ImportBatchCancelledIds

	END TRY
	BEGIN CATCH
	    INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Reinstate Cancelled Policies' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage;

		--SELECT   
		--	ERROR_NUMBER() AS ErrorNumber  
		--	,ERROR_SEVERITY() AS ErrorSeverity  
		--	,ERROR_STATE() AS ErrorState  
		--	,ERROR_PROCEDURE() AS ErrorProcedure  
		--	,ERROR_LINE() AS ErrorLine  
		--	,ERROR_MESSAGE() AS ErrorMessage;  

		IF @@TRANCOUNT > 0  
			ROLLBACK TRANSACTION;
	END CATCH

	IF @@TRANCOUNT > 0  
		COMMIT TRANSACTION; 
END
GO


IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.ImportProduct')) 
	DROP PROCEDURE [batch].[ImportProduct]
GO

CREATE PROCEDURE [batch].[ImportProduct] (@RunReference varchar(255), @ProductId int)
AS
BEGIN 
	;TRUNCATE TABLE batch.ImportPolicy
	;TRUNCATE TABLE batch.ImportRiskFieldUpdate
	;TRUNCATE TABLE batch.ImportPolicyFieldUpdate
	--;TRUNCATE TABLE batch.ImportPolicyError

/*
	IF ((SELECT sReference FROM PremCollectionHist WITH (NOLOCK) WHERE sReference = @RunReference) IS NOT NULL)
	BEGIN TRY
		DECLARE @errorMsg varchar(100) = 'Run Reference <' + @RunReference + '>, already exists.'
		RAISERROR (@errorMsg, 16, 1)

	END TRY
	BEGIN CATCH
	  INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
		SELECT  
			 GETDATE()
			,'Reference Previously used' AS ErrorProcedure 
			,ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_LINE() AS ErrorLine  
			,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage;
	END CATCH
	ELSE 
	BEGIN
*/
	--RERATE VALIDATIONS
		INSERT INTO batch.ImportPolicyError (Id, ProductId, PolicyId, RunReference, ErrorMessage)
		SELECT 
			cfg.NewCombGUID(),
			p.iProductID,
			p.iPolicyID,
			'',
			'Has Cover Type Based Discount/Loading'
		FROM
			batch.ImportFileData fd WITH (NOLOCK)
			INNER JOIN Risks r WITH (NOLOCK) ON (r.intPolicyID = fd.PolicyID)
			INNER JOIN v_ProdRiskFieldRelCombined fr ON (fr.iRiskTypeID = r.intRiskTypeID) AND (fr.iProductID IN (r.iProductID, -1)) AND (fr.sFieldCode = 'R_MATCHINGCT')
			INNER JOIN RiskDetails rd WITH (NOLOCK) ON r.intRiskID = rd.iRiskID and rd.sFieldCode = 'R_MATCHINGCT' AND (rd.sFieldValue <> '' and rd.sFieldValue is not null)
			INNER JOIN Policy p WITH (NOLOCK) ON r.intPolicyID = p.iPolicyID
						AND r.intStatus IN 
							(
								case 
									when p.iPolicyStatus = 2 then 3
									when p.iPolicyStatus = 7 then 6
									else 2
								end 
							)

		INSERT INTO batch.ImportPolicyError (Id, ProductId, PolicyId, RunReference, ErrorMessage)
		SELECT
			cfg.NewCombGUID(), 
			p.iProductID, 
			p.iPolicyId, 
			'', 
			'Not of R_CTDEFAULT Cover Type.'
		FROM
			batch.ImportFileData fd WITH (NOLOCK)
			INNER JOIN Risks r WITH (NOLOCK) ON (r.intPolicyID = fd.PolicyID)
			INNER JOIN RiskCoverType rct WITH (NOLOCK) ON (r.CoverTypeID = rct.CoverTypeID) AND (rct.sFieldCode <> 'R_CTDEFAULT')	 
			INNER JOIN Policy p WITH (NOLOCK) ON r.intPolicyID = p.iPolicyID
				AND r.intStatus IN 
					(
						case 
							when p.iPolicyStatus = 2 then 3
							when p.iPolicyStatus = 7 then 6
							else 2
						end 
					) 

	--EXECUTE Product Splits SP
		IF (@ProductId = 11201)	
			BEGIN
				EXEC batch.GetNAMHOCSplits @RunReference, @Productid
				EXEC batch.ImportProcess
			END
		ELSE IF (@ProductId in ( 11203,11204,11206))	
			BEGIN
				EXEC batch.GetNAMOtherSplits @RunReference, @Productid
				EXEC batch.ImportProcess
			END
		ELSE IF (@ProductId in ( 11205))	
			BEGIN
				EXEC batch.GetVFPOtherSplits @RunReference, @Productid
				EXEC batch.ImportProcess
			END
		ELSE 
			--PRINT 'ProductID: <' + CAST(@ProductId AS VARCHAR(50)) + '>: Not catered for'
			BEGIN TRY
				DECLARE @errorMsg2 varchar(100) = 'ProductID: <' + CAST(@ProductId AS VARCHAR(50)) + '>: Not catered for'
				RAISERROR (@errorMsg2, 16, 1)

			END TRY
			BEGIN CATCH
			  INSERT INTO [batch].[ImportErrorLog] ([Date],[ErrorProcedure],[ErrorNumber],[ErrorSeverity],[ErrorState],[ErrorLine],[ErrorMessage])
				SELECT  
					 GETDATE()
					,'Product Not Catered for' AS ErrorProcedure 
					,ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE() AS ErrorLine  
					,LEFT(ERROR_MESSAGE(),255) AS ErrorMessage;
			END CATCH
/*
	END
*/
	--This is just for incase and error occurs
	--Re-enable the Policy_Insert Trigger
	;ENABLE TRIGGER dbo.Policy_Insert ON dbo.Policy;
	--Re-enable the Risk_Insert Trigger
	;ENABLE TRIGGER dbo.Risks_Insert ON dbo.Risks
	--Re-enable the trg_CoinsRiskRelHistory Trigger
	;ENABLE TRIGGER dbo.trg_CoinsRiskRelHistory ON dbo.Policy;

END
GO

