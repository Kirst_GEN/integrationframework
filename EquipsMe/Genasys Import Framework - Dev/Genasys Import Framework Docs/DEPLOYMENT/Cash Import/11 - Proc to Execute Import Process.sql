IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.ImportExecute')) 
	DROP PROCEDURE [batch].[ImportExecute]
GO

--exec [batch].[ImportExecute] '123467', 56
CREATE PROCEDURE [batch].[ImportExecute] @RunRef VARCHAR(20),@ImportLogId INT AS 

BEGIN

	DECLARE @ImportRef TABLE ([ProductId] int, RowOrder int , RunReference VARCHAR(50))

	INSERT INTO @ImportRef
	SELECT 
		ip.ProductId
		,ROW_NUMBER() OVER (ORDER BY ip.ProductId ) 'RowOrder'
		,p.sProductCode + @RunRef
	FROM 
		ski_int.CollectionImport ip WITH (NOLOCK)
		INNER JOIN Products p WITH (NOLOCK) ON p.iProductID = ip.ProductId
	GROUP BY ip.ProductId, p.sProductCode


--SELECT *FROM @ImportRef


    DECLARE @numberOfRuns INT 
	DECLARE @maxOfRuns INT 
	DECLARE @TableRunRef VARCHAR(50)
	DECLARE @TableProductid int
	DECLARE @CountImport INT
	DECLARE @CountSucess INT
	DECLARE @CountReject INT

    SET @numberOfRuns = 0
	SET @maxOfRuns = (SELECT MAX(RowOrder) FROM @ImportRef)
	SET @CountImport = (SELECT Count(*) FROM ski_int.CollectionImport WITH (NOLOCK))

    WHILE @numberOfRuns <= @maxOfRuns
        BEGIN
            
            SET @numberOfRuns = @numberOfRuns + 1 
			SET @TableRunRef = (SELECT RunReference FROM @ImportRef WHERE @numberOfRuns =  RowOrder)
			SET @TableProductid = (SELECT ProductId FROM @ImportRef WHERE @numberOfRuns =  RowOrder)

            IF @numberOfRuns > 0
                BEGIN
				    --PRINT CONVERT(VARCHAR(50),@TableRunRef) + '/////' + CONVERT(VARCHAR(50),@TableProductid)
                    EXEC batch.ImportProduct @RunReference = @TableRunRef , @ProductId = @TableProductid	
                END
			
        END 

		SET @CountSucess = (
							SELECT Count(1) 
							FROM PolicyTransactions pt WITH (NOLOCK)
							WHERE pt.sReference IN (SELECT RunReference FROM @ImportRef)
								)
				--PRINT @CountSucess	

		SET @CountReject = (
							SELECT Count(1) 
							FROM [ski_int].[CollectionImport] pr WITH (NOLOCK)
							LEFT JOIN PolicyTransactions pt WITH (NOLOCK) ON pr.PolicyId = pt.iPolicyId
							WHERE pt.sReference IN (SELECT RunReference FROM @ImportRef)  
							AND (pt.iTransactionId IS NULL)
								)
			--PRINT @CountReject

		UPDATE s
		SET s.UpdateCount = @CountSucess, s.RejectCount = @CountReject
		FROM [ski_int].[SSISImportLog] s
		WHERE s.Jobreference = @RunRef AND s.importlogid = @ImportLogId
END

