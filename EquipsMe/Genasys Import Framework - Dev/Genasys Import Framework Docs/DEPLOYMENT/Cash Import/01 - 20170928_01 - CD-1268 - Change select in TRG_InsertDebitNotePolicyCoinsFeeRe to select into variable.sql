SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[dbo].[TRG_InsertDebitNotePolicyCoinsFeeRel]') AND [type] = 'TR')
BEGIN
	DROP TRIGGER [dbo].[TRG_InsertDebitNotePolicyCoinsFeeRel];
END
GO

CREATE TRIGGER [dbo].[TRG_InsertDebitNotePolicyCoinsFeeRel] ON [dbo].[PolicyTransactions] 
FOR INSERT
AS
BEGIN
	DECLARE @ExistCnt int

	select @ExistCnt = IsNull(COUNT(*), 0)
	From Inserted ins
	where ins.iparenttranid = -1
		and ins.iTranType in (1,3,5,6,9)
			and ins.iDebitNoteID > 0

	if @ExistCnt > 0 
	Begin
		Delete DebitNotePolicyCoinsFeeRel 
		from inserted ins
		where  DebitNotePolicyCoinsFeeRel.iDebitnoteId = ins.iDebitNoteID and
		       DebitNotePolicyCoinsFeeRel.iPolicyID    = ins.iPolicyID

		Insert DebitNotePolicyCoinsFeeRel
		select Distinct ins.iDebitNoteID, ins.iPolicyID, pcf.sCoInsurer, pcf.cAdminFeePerc, pcf.cPolicyFeePerc, pcf.cBrokerFeePerc, ins.sUser, ins.dTransaction
		From Inserted ins, PolicyCoInsurerFees pcf
		where ins.iparenttranid = -1
        	and ins.iTranType in (1,3,5,6,9)
		      and ins.iDebitNoteID > 0
				  and pcf.iPolicyID = ins.iPolicyID
	End
END
GO