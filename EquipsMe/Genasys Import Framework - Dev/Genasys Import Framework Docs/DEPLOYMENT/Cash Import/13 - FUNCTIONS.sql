USE [SBAB_082017]
GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetSumInsured]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetSumInsured]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[SBAB_GetSumInsured]
GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID_STI]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID_STI]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[SBAB_GetPolicyID_STI]
GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID_LTI]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID_LTI]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[SBAB_GetPolicyID_LTI]
GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[SBAB_GetPolicyID]
GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_EXTRACTNUMBERS]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_EXTRACTNUMBERS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[SBAB_EXTRACTNUMBERS]
GO
/****** Object:  UserDefinedFunction [ski_int].[RemoveSpecialChars]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[RemoveSpecialChars]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[RemoveSpecialChars]
GO
/****** Object:  UserDefinedFunction [ski_int].[GetPremiumSplitsSplitValue]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[GetPremiumSplitsSplitValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[GetPremiumSplitsSplitValue]
GO
/****** Object:  UserDefinedFunction [ski_int].[GetHOCPremiumSplits]    Script Date: 14/11/2017 09:34:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[GetHOCPremiumSplits]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [ski_int].[GetHOCPremiumSplits]
GO
/****** Object:  UserDefinedFunction [ski_int].[GetHOCPremiumSplits]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[GetHOCPremiumSplits]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/*
SELECT [ski_int].[GetHOCPremiumSplits](200000000,400000,4,''UG'',''UGX'')
SELECT [ski_int].[GetHOCPremiumSplits](200000000,400000,4,''UG'',''USD'')
SELECT [ski_int].[GetHOCPremiumSplits](200000000,400000,3,''UG'',''USD'')
SELECT [ski_int].[GetHOCPremiumSplits](200000000,400000,1,''UG'',''USD'')
SELECT [ski_int].[GetHOCPremiumSplits](200000000,400000,1,''UG'',''UGX'')
*/

CREATE FUNCTION [ski_int].[GetHOCPremiumSplits](@ID Int, @FSI float, @FPremium float, @Country VARCHAR(2), @Currency VARCHAR(10), @PaymentFrequency VARCHAR(10))
RETURNS Float
AS
BEGIN 
		DECLARE @RtnValue Float
		DECLARE @SI as Float
		DECLARE @FilePremium as Float
		DECLARE @Ratio as Float
		DECLARE @Nasria as Float 
		DECLARE @PolicyFee as Float
		DECLARE @StampDuty as Float
		DECLARE @Namfisa as Float
		DECLARE @NamfisaComm as Float

		SET @SI = @FSI
		SET @FilePremium = @FPremium--515.35
		SET @Ratio = 116.9941
		SET @PolicyFee = 10 
		SET @Nasria = @SI * 0.00001 * 1.15
		SET @StampDuty = case when (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio > 20.83 then 20.83 else (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio end
		SET @Namfisa = (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio
		SET @NamfisaComm = @SI * 0.00001 * 0.01
		SET @RtnValue = 0

		IF @Country = ''UG'' 
			BEGIN
				SET @FilePremium = (@SI * 0.002)
			END

		SELECT @RtnValue = 
			CASE WHEN @Country = ''NA'' THEN				
				CASE 
					WHEN @Id = 1 then --SI
						cast(round(@SI,2)  as money)
					WHEN @Id = 2 then --File Premium
						cast(round(@FilePremium,2)  as money)
					WHEN @Id = 3 then --Nasria
						cast(round(@Nasria,2) as money)
					WHEN @Id = 4 then --StampDuty
						cast(round(@StampDuty,2)  as money)
					WHEN @Id = 5 then --Namfisa
						cast(round(@Namfisa,2)  as money)
					WHEN @Id = 6 then --NamfisaComm
						cast(round(@SI * 0.00001 * 0.01,2) as money)
					WHEN @Id = 7 then --RiskCPCPremium
						cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm - @Nasria)*12,2)  as money)
					WHEN @Id = 8 then --RiskNasriaCPC
						cast(round(@Nasria*12,2) as money)
					WHEN @Id = 9 then --RiskHeaderComm
						cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm)* 0.2,2)  as money)
					ELSE	
						0
					END	
			WHEN @Country = ''UG'' THEN
				CASE	
					WHEN @ID = 1 THEN --VAT	
						round((@FilePremium + (@FilePremium * 0.005)) * 0.18,0) -- (PREMIUM + TRAINING LEVY) * 18%
					WHEN @ID = 2 THEN --IRA LEVY	
						round(@FilePremium * 0.015,0)
					WHEN @ID = 3 THEN --TRAINING LEVY	
						round(@FilePremium * 0.005,0)
					WHEN @ID = 4 THEN --STAMP DUTY	
						CASE WHEN @Currency = ''UGX'' THEN
							35000
						ELSE
							11
						END
					WHEN @ID = 5 THEN --ANNUALPREMIUM	
						round(@FilePremium,0)
					WHEN @ID = 6 THEN --ANNUAL COMMISSION
						ROUND((round(@FilePremium,0)-round(@FilePremium * 0.015,0)) * 0.2,0)
					WHEN @ID = 7 THEN --TERM PREMIUM
						CASE WHEN @PaymentFrequency = ''A''
							THEN 
								ROUND(@FilePremium,0)
							ELSE
								ROUND(@FilePremium/12,0)
						END
				END
			ELSE
				0
			END

       RETURN @RtnValue
END' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[GetPremiumSplitsSplitValue]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[GetPremiumSplitsSplitValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[GetPremiumSplitsSplitValue](@FSI float, @FilePremium float, @SplitType VARCHAR(20), @PremiumSplitGroup varchar(50), @PremiumSplitField varchar(50))
RETURNS MONEY
AS
BEGIN 
		DECLARE @RtnValue AS MONEY
		DECLARE @AnnualRiskPremium AS MONEY
		DECLARE @TermPremium AS MONEY
		DECLARE @sqlCommand nvarchar(MAX)
		DECLARE @ReturnedResult MONEY

		SET @AnnualRiskPremium = @FilePremium
		--SET @AnnualRiskPremium = 400000
		
		SET @sqlCommand = ''SELECT '' +
						( 
						SELECT 
							''@Result=('' + REPLACE(REPLACE(TargetDBValue,''AnnualRiskPremium'',@AnnualRiskPremium),''SourceDBValue'',SourceDBValue) + '')''
						FROM 
							ski_int.SSISIntegrationLookupMap
						WHERE 
							Type = ''FEES_UG_STI''					-- @SplitType
							AND SourceGroup = ''IRALevy''				-- @PremiumSplitGroup
							AND TargetGroup = ''P_IRALEVYAMOU''		-- @PremiumSplitField
						)

						EXECUTE sp_executesql @sqlCommand, N''@Result MONEY OUTPUT'', @Result=@ReturnedResult OUTPUT
						--SELECT @ReturnedResult

       RETURN @ReturnedResult
END
' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[RemoveSpecialChars]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[RemoveSpecialChars]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[RemoveSpecialChars](@StringToValidate AS VARCHAR(4000))
RETURNS VARCHAR(4000)
AS
BEGIN 

/*########################################################################################################################
THE FOLLOWING sql WILL ALLOW YOU TO IDENTIFY SPECIAL CHARACTERS TO REMOVE OR VALIDATE
SET THE @STRING VARIABLE TO BE THE TEXT YOU WANT TO TEST
##########################################################################################################################
			DECLARE @String_Position int, @String varchar (max)
			-- Initialize the current position and the string variables.
			SET @String_Position = 1

			SET @String = (Select strSuburb from Address where intAddressID = 354263)
			SET @String = ''''

			WHILE @String_Position <= DATALENGTH(@String)
			   BEGIN
			   SELECT ASCII(SUBSTRING(@String, @String_Position, 1)), 
				  CHAR(ASCII(SUBSTRING(@String, @String_Position, 1)))
			   SET @String_Position = @String_Position + 1
			   END
			GO
########################################################################################################################*/

		RETURN 
			(
			replace(replace(replace(@StringToValidate, char(131),''''),char(160),'' ''), char(194),'''')
			)

END
' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_EXTRACTNUMBERS]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_EXTRACTNUMBERS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[SBAB_EXTRACTNUMBERS](@FIELDVALUE VARCHAR(30)) 
RETURNS BIGINT
AS
BEGIN

RETURN (
	   CAST(CASE WHEN ISNUMERIC(RIGHT(stuff(@FIELDVALUE, 1, patindex(''%[0-9]%'', @FIELDVALUE)-1, ''''),1)) = 0 THEN 
		LEFT(stuff(@FIELDVALUE, 1, patindex(''%[0-9]%'', @FIELDVALUE)-1, ''''),LEN(stuff(@FIELDVALUE, 1, patindex(''%[0-9]%'', @FIELDVALUE)-1, ''''))-1)
		else
		stuff(@FIELDVALUE, 1, patindex(''%[0-9]%'', @FIELDVALUE)-1, '''') END AS FLOAT) 
		)


END


' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[SBAB_GetPolicyID](@CIFNumber VARCHAR(30), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
		
				SELECT 
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					TOP 1 P.iPolicyID
				FROM 
					Customer c
				INNER JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
				INNER JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = ''C_CUSTOMNUMBER''
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumber
					AND P.iProductID = @ProductID
			
		)
END


' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID_LTI]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID_LTI]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[SBAB_GetPolicyID_LTI](@CIFNumberCountry VARCHAR(30), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
		
				SELECT 
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					TOP 1 P.iPolicyID
				FROM 
					Customer c
				INNER JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
				INNER JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = ''C_COUNTRYCIF''
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumberCountry
					AND P.iProductID = @ProductID
			
		)
END


' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID_STI]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetPolicyID_STI]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [ski_int].[SBAB_GetPolicyID_STI](@CIFNumberCountry VARCHAR(30),@SerialNo VARCHAR(50), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
				SELECT 
					top 1
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					P.iPolicyID
				FROM 
					Customer c
					JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
					JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = ''C_COUNTRYCIF''
					JOIN
					PolicyDetails PD 
						ON PD.iPolicyID = P.iPolicyID
						and PD.sFieldCode = ''P_SERIALNUMBER''
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumberCountry
					AND P.iProductID = @ProductID
					AND PD.sFieldValue = @SerialNo
			
		)
END


' 
END

GO
/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetSumInsured]    Script Date: 14/11/2017 09:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ski_int].[SBAB_GetSumInsured]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [ski_int].[SBAB_GetSumInsured](@CIFNumber VARCHAR(30), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
		
				SELECT 
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					TOP 1 R.cSumInsured
				FROM 
					Customer c
				INNER JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
				INNER JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = ''C_CUSTOMNUMBER''
				INNER JOIN Risks R
					ON R.intPolicyID = P.iPolicyID
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumber
					AND P.iProductID = @ProductID
			
		)
END' 
END

GO
