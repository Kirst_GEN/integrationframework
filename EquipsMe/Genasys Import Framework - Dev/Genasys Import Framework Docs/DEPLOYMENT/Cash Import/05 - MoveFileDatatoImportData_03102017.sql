IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.MoveFileDataToImportData')) 
	DROP PROCEDURE [batch].[MoveFileDataToImportData]
GO
CREATE PROC [batch].[MoveFileDataToImportData] (@TableName SYSNAME)
AS 
BEGIN 

	;TRUNCATE TABLE [ski_int].[CollectionImport]

	DECLARE 
		@SQLCommand varchar(8000);

	SET @SQLCommand = 'INSERT INTO [ski_int].[CollectionImport]([Id],[PolicyId],[Productid],[FileSumInsured],[FilePremium],[SortByDate],[TransactionPeriod]) '
	SET @SQLCommand = @SQLCommand + 'SELECT '
	SET @SQLCommand = @SQLCommand + 'cfg.NewCombGUID() '
	SET @SQLCommand = @SQLCommand + ',PH_Policyid '
	SET @SQLCommand = @SQLCommand + ',PH_ProductId '
	SET @SQLCommand = @SQLCommand + ',cProperty_Value '
	SET @SQLCommand = @SQLCommand + ',cPremium_Received '
	SET @SQLCommand = @SQLCommand + ',dtTransaction_Date '
	SET @SQLCommand = @SQLCommand + ',dtProcessDate '
	SET @SQLCommand = @SQLCommand + 'FROM ' + N'' + @TableName + ' WITH (NOLOCK) '
	SET @SQLCommand = @SQLCommand + 'WHERE PH_PolicyID IS NOT NULL; '

	EXEC (@SQLCommand)
END


