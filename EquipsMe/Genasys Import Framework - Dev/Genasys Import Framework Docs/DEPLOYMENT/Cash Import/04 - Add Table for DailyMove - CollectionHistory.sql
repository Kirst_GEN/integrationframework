

IF OBJECT_ID(N'ski_int.CollectionImport', N'U') IS NOT NULL
DROP TABLE ski_int.CollectionImport
GO
BEGIN
	CREATE TABLE [ski_int].[CollectionImport](
		Id uniqueidentifier NOT NULL,
		PolicyId float NOT NULL,
		Productid int NOT NULL,
		FileSumInsured money NOT NULL,
		FilePremium money NOT NULL,
		SortByDate date NOT NULL,
		TransactionPeriod date NOT NULL
	 CONSTRAINT [batch_ImportFileData_SKi_PK_CL] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (FILLFACTOR = 80) ON [PRIMARY]
	) ON [PRIMARY]
END 
GO