/****** Object:  UserDefinedFunction [dbo].[GetNAMPLPSplits]    Script Date: 13/09/2017 2:20:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.GetNAMOtherSplits')) 
	DROP PROCEDURE [batch].[GetNAMOtherSplits]
GO

CREATE PROCEDURE [batch].[GetNAMOtherSplits](@RunReference varchar(255),@Productid int)
AS
BEGIN 
	--DECLARE @Ratio as Float = 116.9941
	--DECLARE @PolicyFee as Float = 10

--Insert header values		
	INSERT INTO batch.ImportPolicy (Id, ProductId, PolicyId, FileSumInsured, FilePremium, TransactionPeriod, AdminFee, BrokerFee, PolicyFee, BatchNumber, RunReference, PolicyStatus)
	SELECT 
		cfg.NewCombGUID(),
		p.iProductID,
		p.iPolicyID,
		cast(round(i.FileSumInsured,2)  as money), 
		cast(round(i.FilePremium,2)  as money), 
		i.TransactionPeriod, 
		0 as 'AdminFee',
		0 as 'BrokerFee',
		0 as 'PolicyFee',
		(ROW_NUMBER() OVER (PARTITION BY p.iPolicyId ORDER BY p.iPolicyId, i.SortByDate)) as 'BatchNo',
		@RunReference,
		p.iPolicyStatus
	FROM 
		[ski_int].[CollectionImport] i WITH (NOLOCK)
		INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = i.PolicyId AND p.iPolicyStatus <= 2 AND i.Productid = @Productid  -- 0 = Pending, 1 = Active, 2 = Cancelled
	WHERE 
		NOT EXISTS (SELECT NULL FROM batch.ImportPolicyError ipe WITH (NOLOCK) WHERE ipe.PolicyID = i.PolicyID)
	order by (ROW_NUMBER() OVER (PARTITION BY p.iPolicyId ORDER BY p.iPolicyId, i.SortByDate)) 	

--Insert Risk detail fields
	INSERT INTO batch.ImportRiskFieldUpdate (Id, ImportPolicyId, RiskField, Value)
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'R_LOANAMOUNTSI', 
		ltrim(rtrim(str(cast(round(i.FileSumInsured,2)  as money),25,2) + ' @ 0.000 @ 100.00000 [' + 
			cast(cast(round(
				(
					i.FilePremium 
				) * 12
			,2)  as money) as varchar) + ']'))
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'R_LOANAMT1', 
		ltrim(rtrim(str(cast(round(i.FileSumInsured,2)  as money),25,2))) 
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	
END
