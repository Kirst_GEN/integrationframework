--Drop table batch.ImportPolicy
--Drop table batch.ImportRiskFieldUpdate
--Drop table batch.ImportPolicyFieldUpdate
--Drop table batch.ImportPolicyError


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'batch.ImportPolicy', N'U') IS NOT NULL
	DROP TABLE [batch].[ImportPolicy]


CREATE TABLE [batch].[ImportPolicy](
	Id uniqueidentifier NOT NULL,
	ProductId int NOT NULL,
	PolicyId float NOT NULL,
	FileSumInsured money NOT NULL,
	FilePremium money NOT NULL,
	TransactionPeriod datetime NOT NULL,
	AdminFee money NULL,
	BrokerFee money NULL,
	PolicyFee money NULL,
	BatchNumber int NOT NULL,
	RunReference varchar(255) NOT NULL,
	PolicyStatus int NOT NULL
	CONSTRAINT [batch_ImportPolicy_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Name = 'batch_ImportPolicy_SKi_I_NC_Id_BatchNumber' AND I.object_id = OBJECT_ID('batch.ImportPolicy'))
BEGIN
	CREATE NONCLUSTERED INDEX [batch_ImportPolicy_SKi_I_NC_Id_BatchNumber] ON [batch].[ImportPolicy]
	(
		[Id] ASC,
		[BatchNumber] ASC
	)WITH (FILLFACTOR = 80) ON [PRIMARY]
END
GO

IF OBJECT_ID(N'batch.ImportRiskFieldUpdate', N'U') IS NOT NULL
	DROP TABLE [batch].[ImportRiskFieldUpdate]

CREATE TABLE [batch].[ImportRiskFieldUpdate](
	Id uniqueidentifier NOT NULL,
	ImportPolicyId uniqueidentifier NOT NULL,
	RiskField varchar(15) NOT NULL,
	Value varchar(255) NOT NULL,
	CONSTRAINT [batch_ImportRiskFieldUpdate_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Name = 'batch_ImportRiskFieldUpdate_SKi_I_NC_Id_ImportPolicyId' AND I.object_id = OBJECT_ID('batch.ImportRiskFieldUpdate'))
BEGIN
	CREATE NONCLUSTERED INDEX [batch_ImportRiskFieldUpdate_SKi_I_NC_Id_ImportPolicyId] ON [batch].[ImportRiskFieldUpdate]
	(
		[Id] ASC,
		[ImportPolicyId] ASC
	)WITH (FILLFACTOR = 80) ON [PRIMARY]
END
GO


IF OBJECT_ID(N'batch.ImportPolicyFieldUpdate', N'U') IS NOT NULL
	DROP TABLE [batch].[ImportPolicyFieldUpdate]

CREATE TABLE [batch].[ImportPolicyFieldUpdate](
	Id uniqueidentifier NOT NULL,
	ImportPolicyId uniqueidentifier NOT NULL,
	PolicyField varchar(15) NOT NULL,
	Value varchar(255) NOT NULL,
	CONSTRAINT [batch_ImportPolicyFieldUpdate_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Name = 'batch_ImportPolicyFieldUpdate_SKi_I_NC_Id_ImportPolicyId' AND I.object_id = OBJECT_ID('batch.ImportPolicyFieldUpdate'))
BEGIN
	CREATE NONCLUSTERED INDEX [batch_ImportPolicyFieldUpdate_SKi_I_NC_Id_ImportPolicyId] ON [batch].[ImportPolicyFieldUpdate]
	(
		[Id] ASC,
		[ImportPolicyId] ASC
	)WITH (FILLFACTOR = 80) ON [PRIMARY]
END
GO


IF OBJECT_ID(N'batch.ImportPolicyError', N'U') IS NOT NULL
	DROP TABLE [batch].[ImportPolicyError]

CREATE TABLE [batch].[ImportPolicyError](
	Id uniqueidentifier NOT NULL,
	ProductId int NOT NULL,
	PolicyId float NOT NULL,
	RunReference varchar(255) NOT NULL,
	ErrorMessage varchar(255) NOT NULL
	CONSTRAINT [batch_ImportPolicyError_SKi_PK_CL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO


