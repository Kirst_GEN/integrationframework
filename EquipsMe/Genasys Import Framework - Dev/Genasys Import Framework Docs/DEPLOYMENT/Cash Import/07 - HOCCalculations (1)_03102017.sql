/****** Object:  UserDefinedFunction [dbo].[GetNAMHOCSplits]    Script Date: 13/09/2017 2:20:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'batch.GetNAMHOCSplits')) 
	DROP PROCEDURE [batch].[GetNAMHOCSplits]
GO

CREATE PROCEDURE [batch].[GetNAMHOCSplits](@RunReference varchar(255),@Productid int)
AS
BEGIN 
	DECLARE @Ratio as Float = 116.9941
	DECLARE @PolicyFee as Float = 10
	

--Insert header values		
	INSERT INTO batch.ImportPolicy (Id, ProductId, PolicyId, FileSumInsured, FilePremium, TransactionPeriod, AdminFee, BrokerFee, PolicyFee, BatchNumber, RunReference, PolicyStatus)
	SELECT 
		cfg.NewCombGUID(),
		p.iProductID,
		p.iPolicyID,
		cast(round(i.FileSumInsured,2)  as money), 
		cast(round(i.FilePremium,2)  as money), 
		i.TransactionPeriod, 
		0,
		(
			cast(round(case when (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio > 20.83 then 20.83 else (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio end,2) as money) * 12
		) as 'BrokerFee',
		(
			cast(round((((i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio) + (i.FileSumInsured * 0.00001 * 0.01)),2) as money) * 12
		) as 'PolicyFee',
		(ROW_NUMBER() OVER (PARTITION BY p.iPolicyId ORDER BY p.iPolicyId, i.SortByDate)) as 'BatchNo',
		@RunReference,
		p.iPolicyStatus
	FROM 
		[ski_int].[CollectionImport] i WITH (NOLOCK)
		INNER JOIN Policy p WITH (NOLOCK) ON p.iPolicyID = i.PolicyId AND p.iPolicyStatus <= 2 AND i.Productid = @Productid  -- 0 = Pending, 1 = Active, 2 = Cancelled
	WHERE 
		NOT EXISTS (SELECT NULL FROM batch.ImportPolicyError ipe WITH (NOLOCK) WHERE ipe.PolicyID = i.PolicyID)
	order by (ROW_NUMBER() OVER (PARTITION BY p.iPolicyId ORDER BY p.iPolicyId, i.SortByDate)) 	

--Insert Risk detail fields
	INSERT INTO batch.ImportRiskFieldUpdate (Id, ImportPolicyId, RiskField, Value)
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'R_LIABISUMINSU', 
		ltrim(rtrim(str(cast(round(i.FileSumInsured,2)  as money),25,2) + ' @ 0.000 @ 100.00000 [' + 
			cast(cast(round(
				(
					i.FilePremium - 
					(case when (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio > 20.83 then 20.83 else (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio end) - 
					((i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio) - 
					(i.FileSumInsured * 0.00001 * 0.01) - 
					(i.FileSumInsured * 0.00001 * 1.15)
				) * 12
			,2)  as money) as varchar) + ']'))
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'R_TSASRIA', 
		ltrim(rtrim(str(cast(round(i.FileSumInsured,2)  as money),25,2) + ' @ 0.000 @ 100.00000 [' + cast(cast(round((i.FileSumInsured * 0.00001 * 1.15)*12,2) as money) as varchar) + ']'))
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL 
	SELECT
		cfg.NewCombGUID(), i.Id, 'R_INSUREDVALUE', CAST(cast(round(i.FileSumInsured,2)  as money) as varchar)
	FROM
		batch.ImportPolicy i WITH (NOLOCK)

--Insert Policy Detail fields
	INSERT INTO batch.ImportPolicyFieldUpdate (Id, ImportPolicyId, PolicyField, Value)
	SELECT 
		cfg.NewCombGUID(), 
		i.Id, 
		'P_STAMPDUTYAMOU', 
		CAST(cast(round(case when (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio > 20.83 then 20.83 else (i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio end,2)  as money) as varchar)
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'P_NAMFILEVYAMOU', 
		CAST(cast(round((i.FilePremium - (i.FileSumInsured * 0.00001 * 1.15) - @PolicyFee) / @Ratio,2)  as money) as varchar)
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'P_NASRINAMFLEVY', 
		CAST(cast(round(i.FileSumInsured * 0.00001 * 0.01,2) as money) as varchar)
	FROM
		batch.ImportPolicy i WITH (NOLOCK)
	UNION ALL
	SELECT
		cfg.NewCombGUID(), 
		i.Id, 
		'P_NASRIACOMMIS', 
		CAST((cast(round(i.FileSumInsured * 0.00001 * 1.15,2)  as money) * 0.2) as varchar)
	FROM
		batch.ImportPolicy i WITH (NOLOCK)

	/*	
		select @RtnValue = Case 

							when @Id = 1 then --SI
								cast(round(@SI,2)  as money)
							when @Id = 2 then --File Premium
								cast(round(@FilePremium,2)  as money)
							when @Id = 3 then --Nasria
								cast(round(@Nasria,2) as money)
							when @Id = 4 then --StampDuty
								cast(round(@StampDuty,2)  as money)
							when @Id = 5 then --Namfisa
								cast(round(@Namfisa,2)  as money)
							when @Id = 6 then --NamfisaComm
								cast(round(@SI * 0.00001 * 0.01,2) as money)
							when @Id = 7 then --RiskCPCPremium
								cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm - @Nasria)*12,2)  as money)
							when @Id = 8 then --RiskNasriaCPC
								cast(round(@Nasria*12,2) as money)
							when @Id = 9 then --RiskHeaderComm ???????????????????????????
								cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm)* 0.2,2)  as money)
						   End
						   
						   
       RETURN @RtnValue*/
END
