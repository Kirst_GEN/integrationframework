/****** Object:  StoredProcedure [ski_int].[SSISScript_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSISScript_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSISProcess_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSISProcess_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSISFileLocations_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSISFileLocations_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_SuspendLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_SuspendLog_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartPackageLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_StartPackageLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartJob]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_StartJob]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartExtractLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_StartExtractLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_SplitDataToImport]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_SplitDataToImport]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ReceiptingLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_ReceiptingLog_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_RaisingLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_RaisingLog_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ProgressLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_ProgressLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_MonitorSKiImporterProcess]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_MonitorSKiImporterProcess]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ImportLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_ImportLog_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessToExecute]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetProcessToExecute]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessConfig]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetProcessConfig]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcess]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetProcess]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetJobSchedulerResults]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetJobSchedulerResults]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetFlashProcessConfig]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetFlashProcessConfig]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetDataForSKiImporter]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetAllDataForSKiImporter]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForFlash]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_GetAllDataForFlash]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdatePolicyField]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashUpdatePolicyField]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerHeader]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerHeader]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerField]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerField]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashTotalRecordCountForProcess]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashTotalRecordCountForProcess]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRecieptingRecordCount]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashRecieptingRecordCount]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRaisingRecordCount]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_FlashRaisingRecordCount]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ExecutePackages]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_ExecutePackages]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ErrorLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_ErrorLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndProgressLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_EndProgressLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndPackageLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_EndPackageLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndJob]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_EndJob]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndExtractLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_EndExtractLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EmailsToBeSent_Insert]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_EmailsToBeSent_Insert]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_DataErrorLog]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_DataErrorLog]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_CanContinue]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_CanContinue]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddSSISUser]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_AddSSISUser]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddEmailConfig]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SSIS_AddEmailConfig]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STIRawDataExceptions]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_STIRawDataExceptions]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STIPremiums]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_STIPremiums]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STINewBusiness]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_STINewBusiness]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTIRawDataExceptions]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_LTIRawDataExceptions]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTIPremiums]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_LTIPremiums]
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTINewBusiness]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[SBAB_LTINewBusiness]
GO
/****** Object:  StoredProcedure [ski_int].[Execute_SSIS_Packages]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[Execute_SSIS_Packages]
GO
/****** Object:  StoredProcedure [ski_int].[BatchUserLogin]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[BatchUserLogin]
GO
/****** Object:  StoredProcedure [ski_int].[AlterTablesAddColumns]    Script Date: 23/10/2017 13:22:41 ******/
DROP PROCEDURE [ski_int].[AlterTablesAddColumns]
GO
/****** Object:  StoredProcedure [ski_int].[AlterTablesAddColumns]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC SKI_INT.AlterTablesAddColumns 'ski_int', 'test','TestColumn6','Varchar(30)';

CREATE PROC [ski_int].[AlterTablesAddColumns] 
	(
	@SchemaName VARCHAR(10), 
	@TableName VARCHAR(100), 
	@ColumnName Varchar(100),
	@ColumnType Varchar(100)
	)
AS
BEGIN
DECLARE @AlterString VARCHAR(2000)
SET @AlterString = 'Alter Table ' + @SchemaName + '.' + @TableName + ' ADD ' + @ColumnName + ' ' + @ColumnType + ' NULL'

	IF NOT EXISTS 
	(
		SELECT
	        *
        FROM
            INFORMATION_SCHEMA.COLUMNS
        WHERE
			TABLE_SCHEMA = @SchemaName
            AND TABLE_NAME = @TableName
            AND COLUMN_NAME = @ColumnName
	)
		BEGIN
			EXEC (@AlterString)
			--PRINT '=======================================================================================================';
			PRINT char(13);
			PRINT 'Table was altered, ' + @ColumnName +' column added successfully to table ' + @TableName;
			PRINT '=======================================================================================================';
		END
	ELSE
		BEGIN
			--PRINT '#######################################################################################################';
			PRINT char(13);
			PRINT 'Table ' + @TableName + ' was not altered, as column ' + @ColumnName + ' already exists on the table.';
			PRINT '#######################################################################################################';
		END
END
GO
/****** Object:  StoredProcedure [ski_int].[BatchUserLogin]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[BatchUserLogin]
    @pLoginName NVARCHAR(254),
    @pPassword NVARCHAR(50),
    @responseMessage NVARCHAR(250)='' OUTPUT
AS
BEGIN

    SET NOCOUNT ON

    DECLARE @userID INT

    IF EXISTS (SELECT TOP 1 uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName)
    BEGIN
        SET @userID=(SELECT uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName AND Password = HASHBYTES('SHA2_512', @pPassword))

       IF(@userID IS NULL)
           SET @responseMessage='Incorrect password'
       ELSE 
           SET @responseMessage='User successfully logged in'
    END
    ELSE
       SET @responseMessage='Invalid login'

	   select @responseMessage

END


GO
/****** Object:  StoredProcedure [ski_int].[Execute_SSIS_Packages]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [ski_int].[Execute_SSIS_Packages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;






GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTINewBusiness]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.3156)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

CREATE PROCEDURE [ski_int].[SBAB_LTINewBusiness]
AS
BEGIN

	--========================================================================================================================
	--CLEAR PROCESSED DATA TABLES
	--========================================================================================================================
	TRUNCATE TABLE [ski_int].[SBABDailyLTINewBusiness];

	--========================================================================================================================
	--RUN EXCEPTIONS
	--========================================================================================================================
	EXEC [ski_int].[SBAB_LTIRawDataExceptions];

	--========================================================================================================================
	--INSERT DATA INTO PROCESS TABLE FROM RAW DATA
	--========================================================================================================================
	INSERT INTO 
		[ski_int].[SBABDailyLTINewBusiness] 
	SELECT 
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 1, 1))) Record_Id
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))) = '',convert(datetime,substring(FileName,19,6)),CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))))) Transaction_Date
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) Reason_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) CIF_Number
		,ISNULL(LMPCustTitle.TargetDBValue,'19') Customer_Title
		,IIF(RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 92, 30))) = '',IIF(ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))) = '001','Customer','Company'), RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 92, 30)))) Customer_First_Name
		,IIF(RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30))) = '',IIF(ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))) = '001','Customer','Company'), RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30)))) Customer_Surname
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1))) = '', 'O', RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1)))) Gender
		,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8)))) = 0, '1900-01-01', CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))))) Date_of_Birth
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20))) = '', 'TBA', RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20)))) ID_Passport_Number
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30))) = '', 'TBA', RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30)))) Postal_Address_1
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30))) = '', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30)))) Postal_Address_2
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30))) = '', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30)))) Postal_Address_3
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10))) = '', '.', RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10)))) Postal_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 281, 25))) Tel_No
		,RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 306, 50))) Employer_Name
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 356, 25))) Employer_Contact_Tel_No
		,ISNULL(LMPMaritalS.TargetDBValue,'010')  Marital_Status
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) Loan_Account_No
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8))) = '',LMPDefaultCommstuct.TargetDBValue,CASE WHEN Country = 'KE' 
																										THEN ISNULL(LMSOLID.TargetDBValue,10177)
																										ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
																								  END) SOL_ID
		,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ',''))) Loan_Amount
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) = '',0,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) < '0','0',RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))))) Loan_Term
		,ISNULL(LMPlanType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2)))) Plan_Type
		,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6))) AS Loan_Value_Date
		,IIF(ISNUMERIC(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 443, 20))),' ','')) = 1,CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 443, 20))),' ','')),CONVERT(NUMERIC(18, 2),0)) AS Premium
		,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8)))) = 0, convert (date,DATEADD(YEAR, DATEDIFF(YEAR,0,GETDATE()) + 1, -1)), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8)))))  Repayment_Date
		,ISNULL(LMPSecondTitle.TargetDBValue,'') Second_Life_Insured_Title
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 501, 30))) Second_Life_Insured_First_Name
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 531, 30))) Second_Life_Insured_Surname
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 561, 20))) Second_Life_Insured_ID_Passport_No
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 581, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 581, 8))))) Second_Life_DOB
		,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 1)))) = 1
			,CASE WHEN (SUBSTRING(SourceRecord, 589, 1) =  '1' OR SUBSTRING(SourceRecord, 589, 1) = '3')
					THEN '3'
				  WHEN SUBSTRING(SourceRecord, 589, 1) = '2' 
					THEN '0' 
				  WHEN SUBSTRING(SourceRecord, 589, 1) = '4' 
					THEN '2' 
				  WHEN SUBSTRING(SourceRecord, 589, 1) = '5' 
					THEN '1' 
				  WHEN SUBSTRING(SourceRecord, 589, 1) = '6' 
					THEN '4' 
				  ELSE  '3' 
			 END
			,'3') LTI_Flag
		,CASE WHEN ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))) = 0 THEN
			case when DATEADD(m,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) = '',0,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3)))),
								IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6)))) > IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6)))
                 then DATEADD(m,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) = '',0,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3)))),
								IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6))))
				 else 
					 '20791231'
			 end
		 ELSE
			Case when IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6))) > IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))) = ' ', EOMONTH(GETDATE()), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))))
				then
					case when DATEADD(m,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) = '',0,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3)))),
										IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6)))) > IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6)))
                         then DATEADD(m,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) = '',0,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3)))),
										IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),convert(datetime,substring(FileName,19,6))))
						 else 
							'20791231'
					end
				else
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))) = ' ', EOMONTH(GETDATE()), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))))
			end
		 END AS LTI_Expiry
		,CASE WHEN isnumeric(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20)))) = 0 THEN
			0.01
		 ELSE
			IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))) = ' ', 0.01, ABS(CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))),' ',''))))
		 END AS Outstanding_Balance
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) Scheme_Code
		,ISNULL(LMPCurrencyCode.TargetDBValue,NULL)Currency_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 623, 50))) Insurer_Own_insurance
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 673, 15))) Policy_ID_Own_insurance
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 20))) = '', 0, CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 20))),' ',''))) Amount_Own_insurance
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 708, 8))) = '', '', CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 708, 8))))) Expiry_Date_Own_Insurance
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 716, 5))) Product_Scheme_Code
		,ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))) segment
		,ISNULL(LMPSubSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))) Sub_segment         
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 727, 5))) Serial_Number
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 732, 20))) Underwriter_Name
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 752, 10))) = ' ', '0', RTRIM(LTRIM(SUBSTRING(SourceRecord, 752, 10)))) PIN_number
		,- 1 iImportRecNo
		,convert(datetime,substring(FileName,19,6)) AS ProcessDate
		,t.ID AS SourceFileID
		,CONCAT (
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16)))
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2)))
		) NewLoanAccNo
		,Country
		,CASE WHEN Country = 'KE' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))) 
			WHEN '001' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('101','102','103','104','105','106','107','108','110','111','112','120','121','123','124','130','141','132','142','143','144','150','152','153','154','156','155','157','161','191','210','220','300','390','391','151') 
			THEN '2'
			ELSE	
				CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))
					WHEN  '100' THEN '1'
					WHEN  '148' THEN '10'
					WHEN  '149' THEN '11'
					WHEN  '151' THEN '12'
					WHEN  '159' THEN '13'
					WHEN  '190' THEN '14'
					WHEN  '199' THEN '15'
					WHEN  '200' THEN '16'
					WHEN  '201' THEN '17'
					WHEN  '202' THEN '18'
					WHEN  '203' THEN '19'
					WHEN  '221' THEN '20'
					WHEN  '222' THEN '21'
					WHEN  '223' THEN '22'
					WHEN  '224' THEN '23'
					WHEN  '225' THEN '24'
					WHEN  '226' THEN '25'
					WHEN  '227' THEN '26'
					WHEN  '230' THEN '27'
					WHEN  '301' THEN '28'
					WHEN  '302' THEN '29'
					WHEN  '122' THEN '3'
					WHEN  '303' THEN '30'
					WHEN  '304' THEN '31'
					WHEN  '310' THEN '32'
					WHEN  '321' THEN '33'
					WHEN  '330' THEN '34'
					WHEN  '331' THEN '35'
					WHEN  '332' THEN '36'
					WHEN  '333' THEN '37'
					WHEN  '392' THEN '38'
					WHEN  '393' THEN '39'
					WHEN  '125' THEN '4'
					WHEN  '394' THEN '40'
					WHEN  '400' THEN '41'
					WHEN  '401' THEN '42'
					WHEN  '402' THEN '43'
					WHEN  '403' THEN '43'
					WHEN  '404' THEN '44'
					WHEN  '405' THEN '45'
					WHEN  '406' THEN '46'
					WHEN  '407' THEN '47'
					WHEN  '408' THEN '48'
					WHEN  '409' THEN '49'
					WHEN  '131' THEN '5'
					WHEN  '410' THEN '50'
					WHEN  '411' THEN '51'
					WHEN  '412' THEN '52'
					WHEN  '413' THEN '53'
					WHEN  '414' THEN '54'
					WHEN  '415' THEN '55'
					WHEN  '416' THEN '56'
					WHEN  '417' THEN '57'
					WHEN  '421' THEN '58'
					WHEN  '422' THEN '59'
					WHEN  '140' THEN '6'
					WHEN  '423' THEN '60'
					WHEN  '424' THEN '61'
					WHEN  '425' THEN '62'
					WHEN  '426' THEN '63'
					WHEN  '500' THEN '64'
					WHEN  '501' THEN '65'
					WHEN  '502' THEN '66'
					WHEN  '503' THEN '67'
					WHEN  '145' THEN '7'
					WHEN  '146' THEN '8'
					WHEN  '147' THEN '9'
				END
			END
			WHEN '002' THEN
				CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))
				WHEN '000' THEN '68'
				WHEN '100' THEN '69'
				WHEN '102' THEN '71'
				WHEN '103' THEN '72'
				WHEN '104' THEN '73'
				WHEN '125' THEN  '4'
				WHEN '300' THEN '74'
				WHEN '301' THEN '30'
				WHEN '302' THEN '75'
				WHEN '401' THEN '42'
				WHEN '402' THEN '76'
				WHEN '403' THEN '47'
				WHEN '404' THEN '77'
				WHEN '405' THEN '53'
				WHEN '406' THEN '54'
				WHEN '407' THEN '55'
				WHEN '408' THEN '56'
				WHEN '409' THEN '78'
				WHEN '410' THEN '61'
				WHEN '499' THEN '80'
				WHEN '101' THEN '70'
				END
			END
		ELSE ''
		END CountryClassification
		,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50)))) Country_Cif
		,COU.Id [CountryID]
		,LMPCoInsurer.TargetDBValue [CoInsurer]
		,LMPProductID.TargetDBValue [ProductID]
		,T.FileName
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN CFG.Country COU on COU.Alpha2Code = t.Country
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMSOLID 
			ON LMSOLID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
			AND LMSOLID.Type = 'SOLID_KE_LTI'
			AND LMSOLID.SourceGroup = 'SOL_ID'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPlanType 
			ON LMPlanType.SourceDBValue = CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2))))
			AND LMPlanType.Type = 'Plan_Type_LTI'
			AND LMPlanType.SourceGroup = 'Plan_Type'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
			ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 620, 3)))
			AND LMPCurrencyCode.Type = 'Currency_Code'
			AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCoInsurer  
			ON LMPCoInsurer.SourceDBValue = t.Country
			AND LMPCoInsurer.Type = 'CoInsurer_LTI'
			AND LMPCoInsurer.SourceGroup = 'CoInsurer'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
			ON LMPProductID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) +'_' + t.Country
			AND LMPProductID.Type = 'Product_Code'
			AND LMPProductID.SourceGroup = 'Scheme_Code'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
			ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))))
			AND LMPSegmentType.Type = 'Cust_Segments'
			AND LMPSegmentType.SourceGroup = 'Segment'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSubSegmentType
			ON LMPSubSegmentType.SourceDBValue = Concat(t.Country,'_',ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))),'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))))
			AND LMPSubSegmentType.Type = 'Cust_Segments'
			AND LMPSubSegmentType.SourceGroup = 'Sub_Segment'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCustTitle
			ON LMPCustTitle.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 62, 30)))
			AND LMPCustTitle.Type = 'Cust_Title'
			AND LMPCustTitle.SourceGroup = 'Title'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPMaritalS
			ON LMPMaritalS.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 381, 5)))
			AND LMPMaritalS.Type = 'Cust_MaritalS'
			AND LMPMaritalS.SourceGroup = 'Marital_Status'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPDefaultCommstuct
			ON LMPDefaultCommstuct.SourceDBValue = t.Country
			AND LMPDefaultCommstuct.Type = 'Commstuct'
			AND LMPDefaultCommstuct.SourceGroup = 'Country_Default'
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSecondTitle
			ON LMPSecondTitle.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 471, 30)))
			AND LMPSecondTitle.Type = 'Cust_Title'
			AND LMPSecondTitle.SourceGroup = 'Title'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND ProcessStatus = 0;

	--============================================================
	--UPDATE PROCESS SOURCE RECORD TO PROCESSED
	--============================================================
	--UPDATE 
	--	SSF
	--SET 
	--	ProcessStatus = 1
	--FROM 
	--	[ski_int].[SSIS_Src_RawData] SSF
	--WHERE 
	--	ProcessStatus = 0
	--	AND SSF.FileName LIKE '%lti.newbus%'
	--	AND SUBSTRING(SourceRecord, 1, 1) = '2';

	--========================================================================================================================
	--THIS GETS ONLY THE TRAILER ROW FROM THE FILE WITH A COUNT OF RECORDS IN THE FILE TO INSERT INTO THE AUDIT TABLE
	--========================================================================================================================
	INSERT INTO 
		SBAB_Src.StagingFileProcessAudit
	SELECT 
		SUBSTRING(SourceRecord, 1, 1) Record_ID
		,SUBSTRING(SourceRecord, 2, 8) Count
		,filename FileName
		,DateImported ProcessDate
		,Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = 9
		AND FileName LIKE '%lti.newbus%'
                
	EXCEPT
                
	SELECT 
		Record_ID
		,Count
		,sFileName
		,dtDAte
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%lti.newbus%'
                
	UNION
                
	--========================================================================================================================
	--ALTER A RECORD FOR AUDIT TABLE WITH COUNT OF RECORDS FOR THE SPECIFIC FILE,COUNTRY AND DATE
	--========================================================================================================================
	SELECT 
		0 AS Record_ID
		,COUNT(*) Count
		,SSF.FileName FileName
		,DateImported ProcessDate
		,SSF.Country Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData] SSF
		JOIN [ski_int].[SBABDailyLTINewBusiness] TMP ON 
			TMP.iSourceFileID = SSF.ID
	WHERE 
		SSF.FileName LIKE '%lti.newbus%'
	GROUP BY 
		SSF.FileName
		,DateImported
		,SSF.Country
                
	EXCEPT
                
	SELECT 
		Record_ID
		,Count
		,sFileName
		,dtDate
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%lti.newbus%';

	--========================================================================================================================
	--INSERT DATA FROM FILE INTO LOG TABLE
	--========================================================================================================================
	INSERT INTO 
		[ski_int].[SBABDailyLTINewBusiness_Log]
			(
			dtTransaction_Date
			,sReason_Code
			,sCIF_Number
			,sCustomer_Title
			,sCustomer_First_Name
			,sCustomer_Surname
			,sGender
			,dtDate_of_Birth
			,sID_Passport_Number
			,sPostal_Address_1
			,sPostal_Address_2
			,sPostal_Address_3
			,sPostal_Code
			,sTel_No
			,sEmployer_Name
			,sEmployer_Contact_Tel_No
			,sMarital_Status
			,sLoan_Account_No
			,sSOL_ID
			,cLoan_Amount
			,sLoan_Term
			,sPlan_Type
			,dtLoan_Value_Date
			,cPremium
			,dtRepayment_Date
			,sSecond_Life_Insured_Title
			,sSecond_Life_Insured_First_Name
			,sSecond_Life_Insured_Surname
			,sSecond_Life_Insured_ID_Passport_No
			,sSecond_Life_DOB
			,sLTI_Flag
			,dtLTI_Expiry
			,cOutstanding_Balance
			,sScheme_Code
			,sCurrency_Code
			,sInsurer_Own_insurance
			,sPolicy_ID_Own_insurance
			,cAmount_Own_insurance
			,dtExpiry_Date_Own_Insurance
			,sProduct_Scheme_Code
			,sSegment
			,sSub_segment
			,sSerial_Number
			,sUnderwriter_Name
			,sPIN_number
			,iImportRecNo
			,dtProcessDate
			,iSourceFileID
			,sNewLoanAccountNo
			,sCountry
			,sCountryClassification
			,sCountry_Cif
			,iCountryID
			,RH_CoInsurer
			,PH_ProductID
			,FileName
			)
	SELECT 
		dtTransaction_Date
		,sReason_Code
		,sCIF_Number
		,sCustomer_Title
		,sCustomer_First_Name
		,sCustomer_Surname
		,sGender
		,dtDate_of_Birth
		,sID_Passport_Number
		,sPostal_Address_1
		,sPostal_Address_2
		,sPostal_Address_3
		,sPostal_Code
		,sTel_No
		,sEmployer_Name
		,sEmployer_Contact_Tel_No
		,sMarital_Status
		,sLoan_Account_No
		,sSOL_ID
		,cLoan_Amount
		,sLoan_Term
		,sPlan_Type
		,dtLoan_Value_Date
		,cPremium
		,dtRepayment_Date
		,sSecond_Life_Insured_Title
		,sSecond_Life_Insured_First_Name
		,sSecond_Life_Insured_Surname
		,sSecond_Life_Insured_ID_Passport_No
		,sSecond_Life_DOB
		,sLTI_Flag
		,dtLTI_Expiry
		,cOutstanding_Balance
		,sScheme_Code
		,sCurrency_Code
		,sInsurer_Own_insurance
		,sPolicy_ID_Own_insurance
		,cAmount_Own_insurance
		,dtExpiry_Date_Own_Insurance
		,sProduct_Scheme_Code
		,sSegment
		,sSub_segment
		,sSerial_Number
		,sUnderwriter_Name
		,sPIN_number
		,iImportRecNo
		,dtProcessDate
		,iSourceFileID
		,sNewLoanAccountNo
		,sCountry
		,sCountryClassification
		,sCountry_Cif
		,iCountryID
		,RH_CoInsurer
		,PH_ProductID
		,FileName
	FROM
		[ski_int].[SBABDailyLTINewBusiness];

	--========================================================================================================================
	--CLEAR PROCESSED DATA TABLES
	--========================================================================================================================
	TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

	--========================================================================================================================
	--GET TOTAL NUMBER OF EXCEPTIONS
	--========================================================================================================================
	SELECT COUNT(1) FROM [ski_int].[SSISDailyImportExceptions] WHERE Imported = 0;

SET NOCOUNT ON;

END
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTIPremiums]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [ski_int].[SBAB_LTIPremiums] 
	
AS
BEGIN
-------------------------------------------------------------------------------
--Delete exising temp tables 
-------------------------------------------------------------------------------
--IF OBJECT_ID('tempdb..##Temp') IS NOT NULL
--DROP TABLE ##Temp;
--IF OBJECT_ID('tempdb..##TempInvalidDate') IS NOT NULL
--DROP TABLE ##TempInvalidDate;
--IF OBJECT_ID('tempdb..##TempInvalidNumeric') IS NOT NULL
--DROP TABLE ##TempInvalidNumeric;

------------------------------------------------------------------------------------------------------
--Create Variable @Maxdate to restirct only the latest records
------------------------------------------------------------------------------------------------------
TRUNCATE table [ski_int].[SBABLTIPremium];

DECLARE @Date date = (SELECT CONVERT(DATE,DATEADD(DD,0,GETDATE())))

-------------------------------------------------------------------
--Get all validated records
-------------------------------------------------------------------
--SELECT *
--INTO ##Temp
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and ISDATE(SUBSTRING(SourceRecord,2,8)) = 1
--		and ISDATE(SUBSTRING(SourceRecord,233,8)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,76,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,97,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 1
--		and CONVERT(DATE,dtDate) = @Date

-------------------------------------------------------------------
--Get all invalid date records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidDate
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and (ISDATE(SUBSTRING(SourceRecord,2,8)) = 0
--		   or ISDATE(SUBSTRING(SourceRecord,233,8)) = 0)
--		and CONVERT(DATE,dtDate) = @Date
-------------------------------------------------------------------
--Get all invalid numeric records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidNumeric
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and (ISNUMERIC(SUBSTRING(SourceRecord,76,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,97,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 0)
--		and CONVERT(DATE,dtDate) = @Date
-------------------------------------------------------------------
--Insert the data for the latest records
-------------------------------------------------------------------
INSERT INTO [ski_int].[SBABLTIPremium]
SELECT 
					RTRIM(LTRIM(SUBSTRING(SourceRecord,1,1))) Record_Id,
					CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,2,8)))) Transaction_Date ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))) Loan_Account_No	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,26,50))) CIF_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,76,20))) = ' ', NULL,CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,76,20))) Premium_Received ,
					SUBSTRING(SourceRecord,96,1) Balance_Sign ,
					CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,97,20)) OutstandingBalance ,
					IIF(SUBSTRING(SourceRecord,117,20) = ' ' ,NULL,CONVERT(NUMERIC(18,2),rtrim(ltrim(SUBSTRING(SourceRecord,117,20)))))  Premium_due ,
					IIF(SUBSTRING(SourceRecord,137,2) = ' ' ,NULL,SUBSTRING(SourceRecord,137,2)) Scheme_code ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,139,3))) = ' ' ,NULL,CASE RTRIM(LTRIM(SUBSTRING(SourceRecord,139,3))) 
																					WHEN 'BWP' THEN '8'
																					WHEN 'KES' THEN '6'
																					WHEN 'USD' THEN '1'
																					WHEN 'EUR' THEN '3'
																					WHEN 'GBP' THEN '2'
																					WHEN 'SZL' THEN '9'
																					WHEN 'GHS' THEN '13'
																					WHEN 'ZMW' THEN '12'
																					WHEN 'ZAR' THEN '0'
																					WHEN 'UGX' THEN '14'
																					WHEN 'LSL' THEN '11'
																					WHEN 'MWK' THEN '15'
																					WHEN 'NAD' THEN '10'
																				  ELSE NULL 
																				END) Currency_Code,
					IIF(SUBSTRING(SourceRecord,142,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,142,30)))) Customer_Title,	
					IIF(SUBSTRING(SourceRecord,172,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,172,30)))) Customer_First_Name,
					IIF(SUBSTRING(SourceRecord,202,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,202,30)))) Customer_Surname ,
					SUBSTRING(SourceRecord,232,1) Gender,
					IIF(SUBSTRING(SourceRecord,233,8) = ' ' ,NULL,CONVERT(DATE,SUBSTRING(SourceRecord,233,8))) Date_of_Birth,
					IIF(SUBSTRING(SourceRecord,241,20)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,241,20)))) ID_Passport_Number	,
					IIF(SUBSTRING(SourceRecord,261,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,261,30)))) Postal_Address_1,	
					IIF(SUBSTRING(SourceRecord,291,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,291,30)))) Postal_Address_2,	
					IIF(SUBSTRING(SourceRecord,321,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,321,30)))) Postal_Address_3,	
					IIF(SUBSTRING(SourceRecord,351,10)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,351,10)))) Postal_Code	,
					IIF(SUBSTRING(SourceRecord,361,25)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,361,25)))) Tel_No ,
					IIF(SUBSTRING(SourceRecord,386,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,386,5)))) Marital_Status,
					IIF(SUBSTRING(SourceRecord,391,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,391,30)))) Phys_Add1,
					IIF(SUBSTRING(SourceRecord,421,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,421,30)))) Phys_Add2,	
					IIF(SUBSTRING(SourceRecord,451,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,451,30)))) Phys_Add3,	
					IIF(SUBSTRING(SourceRecord,481,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,481,30)))) Second_Life_Insured_First_Name,
					IIF(SUBSTRING(SourceRecord,511,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,511,30)))) Second_Life_Insured_Surname,
					IIF(SUBSTRING(SourceRecord,541,20) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,541,20)))) Second_Life_Insured_ID_Passport_No,
					IIF(SUBSTRING(SourceRecord,561,8) = ' ' ,NULL,SUBSTRING(SourceRecord,561,8)) Second_Life_DOB	,
					IIF(SUBSTRING(SourceRecord,569,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,569,5)))) Product_Scheme_Code ,
					IIF(SUBSTRING(SourceRecord,574,1) = ' ' ,NULL,SUBSTRING(SourceRecord,574,1)) Status_Flag ,
					IIF(SUBSTRING(SourceRecord,575,1) = ' ' ,NULL,SUBSTRING(SourceRecord,575,1)) Exception	,
					IIF(SUBSTRING(SourceRecord,576,2) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,576,2)))) Plan_Type,
					IIF(SUBSTRING(SourceRecord,578,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,578,5)))) Serial_Number,
					IIF(SUBSTRING(SourceRecord,583,20) = ' ',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,583,20)))) Underwriter_Name,
					IIF(SUBSTRING(SourceRecord,603,10) = ' ',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,603,10)))) PIN_number,
					-1 iImportRecNo,
					convert(datetime,substring(FileName,21,6)) as ProcessDate,
					[ski_int].[SSIS_Src_RawData].ID iSourceFileID,
					CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))) sNewLoanAccNo
					,Country
					,LMPProductID.TargetDBValue
					,ski_int.SBAB_GetPolicyID_LTI (Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 26, 50)))), CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))), LMPProductID.TargetDBValue)
					,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 26, 50)))) Country_Cif
					,COU.Id
					,ISNULL([ski_int].[SBAB_GetSumInsured] (RTRIM(LTRIM(SUBSTRING(SourceRecord,26,50))), CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))), LMPProductID.TargetDBValue),0)
FROM [ski_int].[SSIS_Src_RawData]
LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 137, 2))) +'_' + Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
LEFT JOIN CFG.Country COU on COU.Alpha2Code = [ski_int].[SSIS_Src_RawData].Country
WHERE FileName like '%lti.premium%'
		and SUBSTRING(SourceRecord,1,1) = 2
		and ProcessStatus = 0
		and CONVERT(DATE,DateImported) = @Date
---------------------------------------------------------------------------------------------------------------------------
--Update the source record to processed to eliminate processing duplicates
---------------------------------------------------------------------------------------------------------------------------
--Validated Records
------------------------------------------------------------------
UPDATE SSF
		SET ProcessStatus = 1
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [ski_int].[SBABLTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE SSF.FileName like '%lti.premium%'
		and CONVERT(DATE,SSF.DateImported) = @Date 
------------------------------------------------------------------
--Invalid date records
------------------------------------------------------------------
--UPDATE SSF
--		SET ProcessStatus = 2
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidDate TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.FileName like '%lti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date 
------------------------------------------------------------------
--Invalid numeric records
------------------------------------------------------------------
--UPDATE SSF
--		SET ProcessStatus = 3
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidNumeric TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.FileName like '%lti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--IF ( SELECT TOP 1 FileName
--			FROM SBAB_Src.StagingFileProcessAudit
--		 WHERE FileName like '%lti.premiums%'
--				and CONVERT(DATE,dtDate) = @Date) IS NOT NULL

--BEGIN

--	DELETE FROM SBAB_Src.StagingFileProcessAudit
--					WHERE FileName like '%lti.premiums%'
--							and CONVERT(DATE,dtDate) = @Date
--END

INSERT INTO SBAB_Src.StagingFileProcessAudit
SELECT  SUBSTRING(SourceRecord,1,1) Record_ID,
					iif(RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12))) = ' ',0,RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12)))) Count
					,FileName FileName
					,DateImported ProcessDate
					,Country
					,CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,2,20)) TotalPremiumsReceived				
	FROM [ski_int].[SSIS_Src_RawData]
 WHERE SUBSTRING(SourceRecord,1,1) = 9
		and FileName like '%lti.premium%'
		and CONVERT(DATE,DateImported) = @Date

 EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%lti.premium%'
		AND CONVERT(DATE,dtDate) = @Date

 UNION
-------------------------------------------------------------------------------------------------------------------------------------------------
--Create a record for audit table with count of records for the specific file,country and date
-------------------------------------------------------------------------------------------------------------------------------------------------
SELECT	0 AS Record_ID,
					COUNT(*) Count,
					FileName FileName,
					DateImported ProcessDate,
					SSF.Country Country,
					SUM(cPremium_Received) TotalPremiumsReceived
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [ski_int].[SBABLTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE FileName like '%lti.premium%'
		AND CONVERT(DATE,DateImported) = @Date
GROUP BY FileName, DateImported,SSF.Country

EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
WHERE sFileName like '%lti.premium%'
		AND CONVERT(DATE,dtDate) = @Date



INSERT INTO [ski_int].[SBABLTIPremium_Log] 
(
dtTransaction_Date
,sLoan_Account_No
,sCIF_Number
,cPremium_Received
,sBalance_Sign
,cOutstandingBalance
,cPremium_Due
,sScheme_Code
,sCurrency_Code
,sCustomer_Title
,sCustomer_First_Name
,sCustomer_Surname
,sGender
,dtDate_of_Birth
,sID_Passport_Number
,sPostal_Address_1
,sPostal_Address_2
,sPostal_Address_3
,sPostal_Code
,sTel_No
,sMarital_Status
,sPhys_Add1
,sPhys_Add2
,sPhys_Add3
,sSecond_Life_Insured_First_Name
,sSecond_Life_Insured_Surname
,sSecond_Life_Insured_ID_Passport_No
,sSecond_Life_DOB
,sProduct_Scheme_Code
,sStatus_Flag
,sException
,sPlan_Type
,sSerial_Number
,sUnderwriter_Name
,sPIN_number
,iImportRecNo
,dtProcessDate
,iSourceFileID
,sNewLoanAccNo
,sCountry
,PH_ProductID
,PH_PolicyID
,sCountry_Cif
,iCountryID
,cProperty_Value
)

SELECT 
dtTransaction_Date
,sLoan_Account_No
,sCIF_Number
,cPremium_Received
,sBalance_Sign
,cOutstandingBalance
,cPremium_Due
,sScheme_Code
,sCurrency_Code
,sCustomer_Title
,sCustomer_First_Name
,sCustomer_Surname
,sGender
,dtDate_of_Birth
,sID_Passport_Number
,sPostal_Address_1
,sPostal_Address_2
,sPostal_Address_3
,sPostal_Code
,sTel_No
,sMarital_Status
,sPhys_Add1
,sPhys_Add2
,sPhys_Add3
,sSecond_Life_Insured_First_Name
,sSecond_Life_Insured_Surname
,sSecond_Life_Insured_ID_Passport_No
,sSecond_Life_DOB
,sProduct_Scheme_Code
,sStatus_Flag
,sException
,sPlan_Type
,sSerial_Number
,sUnderwriter_Name
,sPIN_number
,iImportRecNo
,dtProcessDate
,iSourceFileID
,sNewLoanAccNo
,sCountry
,PH_ProductID
,PH_PolicyID
,sCountry_Cif
,iCountryID
,cProperty_Value
 FROM [ski_int].[SBABLTIPremium];




--TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

SET NOCOUNT ON;
END


GO
/****** Object:  StoredProcedure [ski_int].[SBAB_LTIRawDataExceptions]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
CREATE PROCEDURE [ski_int].[SBAB_LTIRawDataExceptions]
AS			

BEGIN

	--====================================================================================================
	--TRUNCATE PROCESS TABLE
	--====================================================================================================
	TRUNCATE TABLE [ski_int].[SSISDailyImportExceptions];

	--====================================================================================================
	--REMOVE SPECIAL CHARACTERS FIRST BEFORE VALIDATING ANY FURTHER
	--====================================================================================================
	UPDATE
		ski_int.SSIS_Src_RawData
	SET
		SourceRecord = ski_int.RemoveSpecialChars(SourceRecord);

	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Record Id',
		SUBSTRING(SourceRecord,1,1),
		'Invalid Record Id in position 1',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		FileName like '%lti.newbus%'
		AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 0
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Record Id',
		SUBSTRING(SourceRecord,1,1),
		'Invalid Record Id - Invalid Character in position 1',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		FileName like '%lti.newbus%'
		AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 1
		AND SUBSTRING(SourceRecord,1,1) NOT IN ('1','2','9')
		AND NOT EXISTS(
					   SELECT * 
					   FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Fixed Width format',
		LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
		'File Format Fixed Width Length Incorrect - Expecting 730',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND (
			LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')) < 730
			)
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--REASON CODE BLANK
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Reason Code',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))),
		'Reason Code is Invalid',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) = '' or RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) not in ('N','P','C','S','R'))
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CIFF NUMBER
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'CIFF Number',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))),
		'CIFF Number is blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) = ''
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CUSTOMER FIRST NAME BLANK 
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Customer First Name',
		RTRIM(LTRIM(SUBSTRING(replace(t.SourceRecord, char(131),' '), 92, 30))),
		'Customer First Name is blank. Segment 001',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
			ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))))
			AND LMPSegmentType.Type = 'Cust_Segments'
			AND LMPSegmentType.SourceGroup = 'Segment'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 92, 30))) = '' 
		AND ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))) = '001'
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CUSTOMER SURNAME IS BLANK 
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Customer Surname',
		RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 122, 30))),
		'Customer Surname is blank',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 122, 30))) = '' 
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--LOAN ACCOUNT NUMBER IS BLANK
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Account No',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))),
		'Loan Account No blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) = '' 
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;
 
	--====================================================================================================
	--PLAN TYPE IS NOT VAILD
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Plan Type',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2))),
		'Plan Type is not Valid/Mapped',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPlanType 
			ON LMPlanType.SourceDBValue = CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2))))
			AND LMPlanType.Type = 'Plan_Type_LTI'
			AND LMPlanType.SourceGroup = 'Plan_Type'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND ISNULL(LMPlanType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2)))) is null
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--LOAN AMOUNT IS BLANK OR NOT NUMERIC
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Amount',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),
		'Loan Amount is blank/Non-Numeric',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20)))) = 0
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--LOAN VALUE DATE/DISBURSEMENT DATE IS BLANK
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Value Date/Disbursement Date',
		RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8))),
		'Loan Value Date/Disbursement Date is blank(Defaulted to file Date)',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8))) = ''
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--PAYMENT TERM INDICATOR
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'LTI Flag',
		RTRIM(LTRIM(SUBSTRING(t.SourceRecord, 589, 1))),
		'LTI Flag is blank/invalid(Defaulted to 1)',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 1))) = '' or RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 1))) not in ('1','2','3','4','5','6'))
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--SCHEME CODE BLANK OR INVALID
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Scheme Code',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))),
		'Scheme code is invalid or blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) = '' or RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) not in ('A','B','C','D','Z'))
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--CURRENCY CODE CHECK
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Currency Code',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 620, 3))),
		'Currency Code is invalid/not mapped',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
			ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 620, 3)))
			AND LMPCurrencyCode.Type = 'Currency_Code'
			AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND ISNULL(LMPCurrencyCode.TargetDBValue,NULL) is null
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--SEGMENT CODE IS INVALID
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Segment Code',
		ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))),
		'Segment Code is Invalid',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
			ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))))
			AND LMPSegmentType.Type = 'Cust_Segments'
			AND LMPSegmentType.SourceGroup = 'Segment'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%lti.newbus%'
		AND ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))) not in ('001','004','005')
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--REMOVE EXCEPTION RECORDS FROM SOURCE TABLE
	--====================================================================================================
	DELETE 
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE EXISTS(
				SELECT 
					* 
				FROM 
					[ski_int].[SSISDailyImportExceptions] EXC
				WHERE
					EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					AND Imported = 0
				);

	--====================================================================================================
	--INSERT DAILY EXCEPTIONS INTO LOG TABLE
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions_Log] 
	SELECT 
		* 
	FROM 
		[ski_int].[SSISDailyImportExceptions];


	SET NOCOUNT ON;

END	
	
	
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STINewBusiness]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SBAB_STINewBusiness]
AS
BEGIN


	--========================================================================================================================
	--CLEAR PROCESSED DATA TABLES
	--========================================================================================================================
	TRUNCATE TABLE [ski_int].[SBABDailySTINewBusiness];

	--========================================================================================================================
	--RUN EXCEPTIONS
	--========================================================================================================================
    EXEC [ski_int].[SBAB_STIRawDataExceptions];

	------------------------------------------------------------------------------------------------------
	--alter Variable @Date to restirct only the latest records
	------------------------------------------------------------------------------------------------------
	SET DATEFORMAT YMD;
	
	DECLARE @Date DATE = (SELECT DATEADD(DD, 0, CONVERT(DATE, GETDATE())))
	
	-------------------------------------------------------------------
	--Insert the data for the latest records
	-------------------------------------------------------------------
	INSERT INTO [SKI_int].[SBABDailySTINewBusiness]
	SELECT 
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 1, 1))) Record_Id
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))) = '',convert(datetime,substring(FileName,19,6)),CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))))) Transaction_Date
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) Reason_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) CIF_Number
		,ISNULL(LMPCustTitle.TargetDBValue,'19') Customer_Title
		,IIF(RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 92, 30))) = '',IIF(ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))) = '001','Customer','Company'), RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 92, 30)))) Customer_First_Name
		,IIF(RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30))) = '',IIF(ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))) = '001','Customer','Company'), RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30)))) Customer_Surname
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1))) = '', 'O', RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1)))) Gender
		,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8)))) = 0, '1900-01-01', CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))))) Date_of_Birth
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20))) = '', 'TBA', RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20)))) ID_Passport_Number
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30))) = '', 'TBA', RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30)))) Postal_Address_1
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30))) = '', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30)))) Postal_Address_2
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30))) = '', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30)))) Postal_Address_3
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10))) = '', '.', RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10)))) Postal_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 281, 25))) Tel_No
		,RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 306, 50))) Employer_Name
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 356, 25))) Employer_Contact_Tel_No
		,ISNULL(LMPMaritalS.TargetDBValue,'010')  Marital_Status
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) Loan_Account_No
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8))) = '',LMPDefaultCommstuct.TargetDBValue,CASE WHEN Country = 'KE' 
																										THEN ISNULL(LMSOLID.TargetDBValue,10177)
																										ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
																								  END) SOL_ID
	 ,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ',''))) Loan_Amount
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 30))) = '','TBA',RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 30)))) Phys_Add1
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 460, 30))) Phys_Add2
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 490, 30))) Phys_Add3
	 ,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8)))),convert(datetime,substring(FileName,19,6))) AS Loan_Value_Date
	 ,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18,2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))) Premium
	 ,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18,2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))) Property_value
     ,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 568, 8)))) = 0, convert(date,DATEADD(YEAR, DATEDIFF(YEAR,0,GETDATE()) + 1, -1)), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 568, 8))))) Repayment_Date
	 ,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 576, 8)))) = 0, 
		  DATEADD(m,12,IIF(ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8)))) = 1,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8)))),convert(datetime,substring(FileName,19,6)))), 
		  CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 576, 8))))) Policy_Expiry_Date
	 ,REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O') Scheme_code
	 ,ISNULL(LMPCurrencyCode.TargetDBValue,NULL) Currency_Code
	 ,ISNULL(LMPConstructType.TargetDBValue,NULL) Construction_Type
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 593, 1))) Cover_Note
	 ,CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1))) 
		WHEN 'M' THEN '3'
	 	WHEN 'A' THEN '0'
	             ELSE '3'
	  END Premium_Payment_Frequency
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 595, 50))) Insurer_Own_insurance
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 645, 15))) Policy_ID_Own_insurance
	 ,IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 660, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 660, 20))))) Amount_Own_insurance
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 680, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 680, 8))))) Expiry_Date_Own_Insurance
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 16))) Loan_Account_No_Own_Insurance
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 704, 5))) Product_Scheme_Code
	 ,ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))) segment
	 ,ISNULL(LMPSubSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))) Sub_segment
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 715, 5))) Serial_Number
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 720, 100))) Risk_Address
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 820, 20))) Underwriter_Name
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 840, 10))) = ' ', '0', RTRIM(LTRIM(SUBSTRING(SourceRecord, 840, 10)))) PIN_number
	 ,- 1 iImportRecNo
	 ,convert(datetime,substring(FileName,19,6)) Processdate
	 ,t.ID SourceFileID
	 ,CONCAT (RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O')) NewLoanAccNo
	 ,Country
	 ,CASE WHEN Country = 'KE' THEN
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))) 
		WHEN '001' THEN
		CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('101','102','103','104','105','106','107','108','110','111','112','120','121','123','124','130','141','132','142','143','144','150','152','153','154','156','155','157','161','191','210','220','300','390','391','151') 
		THEN '2'
		ELSE	
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
				WHEN  '100' THEN '1'
				WHEN  '148' THEN '10'
				WHEN  '149' THEN '11'
				WHEN  '151' THEN '12'
				WHEN  '159' THEN '13'
				WHEN  '190' THEN '14'
				WHEN  '199' THEN '15'
				WHEN  '200' THEN '16'
				WHEN  '201' THEN '17'
				WHEN  '202' THEN '18'
				WHEN  '203' THEN '19'
				WHEN  '221' THEN '20'
				WHEN  '222' THEN '21'
				WHEN  '223' THEN '22'
				WHEN  '224' THEN '23'
				WHEN  '225' THEN '24'
				WHEN  '226' THEN '25'
				WHEN  '227' THEN '26'
				WHEN  '230' THEN '27'
				WHEN  '301' THEN '28'
				WHEN  '302' THEN '29'
				WHEN  '122' THEN '3'
				WHEN  '303' THEN '30'
				WHEN  '304' THEN '31'
				WHEN  '310' THEN '32'
				WHEN  '321' THEN '33'
				WHEN  '330' THEN '34'
				WHEN  '331' THEN '35'
				WHEN  '332' THEN '36'
				WHEN  '333' THEN '37'
				WHEN  '392' THEN '38'
				WHEN  '393' THEN '39'
				WHEN  '125' THEN '4'
				WHEN  '394' THEN '40'
				WHEN  '400' THEN '41'
				WHEN  '401' THEN '42'
				WHEN  '402' THEN '43'
				WHEN  '403' THEN '43'
				WHEN  '404' THEN '44'
				WHEN  '405' THEN '45'
				WHEN  '406' THEN '46'
				WHEN  '407' THEN '47'
				WHEN  '408' THEN '48'
				WHEN  '409' THEN '49'
				WHEN  '131' THEN '5'
				WHEN  '410' THEN '50'
				WHEN  '411' THEN '51'
				WHEN  '412' THEN '52'
				WHEN  '413' THEN '53'
				WHEN  '414' THEN '54'
				WHEN  '415' THEN '55'
				WHEN  '416' THEN '56'
				WHEN  '417' THEN '57'
				WHEN  '421' THEN '58'
				WHEN  '422' THEN '59'
				WHEN  '140' THEN '6'
				WHEN  '423' THEN '60'
				WHEN  '424' THEN '61'
				WHEN  '425' THEN '62'
				WHEN  '426' THEN '63'
				WHEN  '500' THEN '64'
				WHEN  '501' THEN '65'
				WHEN  '502' THEN '66'
				WHEN  '503' THEN '67'
				WHEN  '145' THEN '7'
				WHEN  '146' THEN '8'
				WHEN  '147' THEN '9'
			END
		END
		WHEN '002' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
			WHEN '000' THEN '68'
			WHEN '100' THEN '69'
			WHEN '102' THEN '71'
			WHEN '103' THEN '72'
			WHEN '104' THEN '73'
			WHEN '125' THEN  '4'
			WHEN '300' THEN '74'
			WHEN '301' THEN '30'
			WHEN '302' THEN '75'
			WHEN '401' THEN '42'
			WHEN '402' THEN '76'
			WHEN '403' THEN '47'
			WHEN '404' THEN '77'
			WHEN '405' THEN '53'
			WHEN '406' THEN '54'
			WHEN '407' THEN '55'
			WHEN '408' THEN '56'
			WHEN '409' THEN '78'
			WHEN '410' THEN '61'
			WHEN '499' THEN '80'
			WHEN '101' THEN '70'
			END
		END
	ELSE ''
	END sCountryClassification,
	CONCAT(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50)))) Country_Cif
	,COU.Id
	,LMPProductID.TargetDBValue [ProductID]
	,LMPCoInsurer.TargetDBValue [CoInsurer]
	,Round(((dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),7)
	 +dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),8)) / 12),2) as 'Premium' 
	,(dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),7)
	 +dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),8)) as 'AnnualPremium'
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),3) as 'Sasria'
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),8) as 'AnnualSasria'
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),9) AS 'Commission'
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),9) * 12 as 'AnnualCommission'
	,20 as 'CommissionPerc'
	,case when Country = 'NA' 
			then dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),4) 
		  when Country = 'UG'
		    then case when ISNULL(LMPCurrencyCode.TargetDBValue,NULL) = 14 
						then Round((35000 / 12),2)
			          when ISNULL(LMPCurrencyCode.TargetDBValue,NULL) = 1 
						then Round((11 / 12),2)
				 end
		  else
			0
	 end AS 'PolicyFee'
	,case when Country = 'NA' 
			then dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),4) * 12
		  when Country = 'UG'
		    then case when ISNULL(LMPCurrencyCode.TargetDBValue,NULL) = 14 
						then 35000
			          when ISNULL(LMPCurrencyCode.TargetDBValue,NULL) = 1 
						then 11
				 end
		  else
			0
	 end AS 'AnnualPolicyFee'
	,0 AS 'AnnualAdminFee'
	,(dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),5)
	 + dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),6) * 12)AS 'AnnualBrokerFee'
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),7) AS 'CustomCalc1' --RiskPremCPC
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),5) AS 'CustomCalc2' --Namfisa
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),8) AS 'CustomCalc3' --RiskNasriaCPC
	,dbo.GetNAMHOCSplits(IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20)))) = 0, 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),6) AS 'CustomCalc4' --NamfisaComm
	,t.FileName
	FROM SKI_int.SSIS_Src_RawData t
	LEFT JOIN CFG.Country COU on COU.Alpha2Code = t.Country
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMSOLID 
		ON LMSOLID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
		AND LMSOLID.Type = 'SOLID_KE_LTI'
		AND LMSOLID.SourceGroup = 'SOL_ID'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O') +'_' + t.Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
		ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 586, 3)))
		AND LMPCurrencyCode.Type = 'Currency_Code'
		AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPConstructType
		ON LMPConstructType.SourceDBValue = Concat(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O'),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 4))))
		AND LMPConstructType.Type = 'Construct_Type_STI'
		AND LMPConstructType.SourceGroup = 'Construction_Type'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
		ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))))
		AND LMPSegmentType.Type = 'Cust_Segments'
		AND LMPSegmentType.SourceGroup = 'Segment'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSubSegmentType
		ON LMPSubSegmentType.SourceDBValue = Concat(t.Country,'_',ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))),'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))))
		AND LMPSubSegmentType.Type = 'Cust_Segments'
		AND LMPSubSegmentType.SourceGroup = 'Sub_Segment'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCoInsurer  
		ON LMPCoInsurer.SourceDBValue = t.Country
		AND LMPCoInsurer.Type = 'CoInsurer_STI'
		AND LMPCoInsurer.SourceGroup = 'CoInsurer'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCustTitle
		ON LMPCustTitle.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 62, 30)))
		AND LMPCustTitle.Type = 'Cust_Title'
		AND LMPCustTitle.SourceGroup = 'Title'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPMaritalS
		ON LMPMaritalS.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 381, 5)))
		AND LMPMaritalS.Type = 'Cust_MaritalS'
		AND LMPMaritalS.SourceGroup = 'Marital_Status'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPDefaultCommstuct
		ON LMPDefaultCommstuct.SourceDBValue = t.Country
		AND LMPDefaultCommstuct.Type = 'Commstuct'
		AND LMPDefaultCommstuct.SourceGroup = 'Country_Default'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND ProcessStatus = 0

--SELECT dtLoan_Value_Date, dtPolicy_Expiry_Date,DATEADD(M,-1,dtPolicy_Expiry_Date),* FROM 
--[ski_int].[SBABDailySTINewBusiness]
--WHERE
--		dtLoan_Value_Date > dtPolicy_Expiry_Date

	---------------------------------------------------------------------------------------------------------------------------
	--Update the source record to processed to eliminate processing duplicates
	---------------------------------------------------------------------------------------------------------------------------
	--Validated Records
	------------------------------------------------------------------
	UPDATE SSF
		SET ProcessStatus = 1
	FROM 
		[ski_int].[SSIS_Src_RawData] SSF
	WHERE 
		SSF.FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, SSF.DateImported) = @Date;
	
	--and CONVERT(DATE,dtProcessDate) = @Date;
	------------------------------------------------------------------
	--Invalid date records
	------------------------------------------------------------------
	--UPDATE SSF
	--SET iProcessStatus = 2
	--FROM SBAB_src.StagingSourceFiles SSF
	--JOIN ##TempInvalidDate TMP ON TMP.iID = SSF.iID
	--WHERE SSF.sFileName LIKE '%sti.newbus%'
	--	AND CONVERT(DATE, SSF.dtDate) = @Date;
	
	------------------------------------------------------------------
	--Invalid numeric records
	------------------------------------------------------------------
	--UPDATE SSF
	--		SET iProcessStatus = 3
	--	FROM SBAB_src.StagingSourceFiles SSF
	--					JOIN ##TempInvalidNumeric TMP
	--					on TMP.iID = SSF.iID
	--WHERE SSF.sFileName like '%sti.newbus%'
	--		and CONVERT(DATE,SSF.dtDate) = @Date;
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	INSERT INTO SBAB_Src.StagingFileProcessAudit
	SELECT 
		SUBSTRING(SourceRecord, 1, 1) Record_ID
		,SUBSTRING(SourceRecord, 2, 8) Count
        ,filename FileName
        ,DateImported ProcessDate
        ,Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = 9
		AND FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, DateImported) = @Date
	
	EXCEPT
	
	SELECT Record_ID
		,Count
		,sFileName
		,dtDate
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, dtDate) = @Date
	
	UNION
	
	-------------------------------------------------------------------------------------------------------------------------------------------------
	--Create a record for audit table with count of records for the specific file,country and date
	-------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT 0 AS Record_ID
		,COUNT(*) Count
		,SSF.FileName FileName
		,DateImported ProcessDate
		,SSF.Country Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData]  SSF
		JOIN [ski_int].[SBABDailySTINewBusiness] TMP ON TMP.iSourceFileID = SSF.ID
	WHERE 
		SSF.FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, DateImported) = @Date
	GROUP BY 
		SSF.FileName
		,DateImported
		,SSF.Country
	
	EXCEPT
	
	SELECT Record_ID
		,Count
		,sFileName
		,dtDate
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, dtDate) = @Date
	
	
	
	
	
	INSERT INTO 
		[ski_int].[SBABDailySTINewBusiness_Log]
		(
		dtTransaction_Date
		,sReason_Code
		,sCIF_Number
		,sCustomer_Title
		,sCustomer_First_Name
		,sCustomer_Surname
		,sGender
		,dtDate_of_Birth
		,sID_Passport_Number
		,sPostal_Address_1
		,sPostal_Address_2
		,sPostal_Address_3
		,sPostal_Code
		,sTel_No
		,sEmployer_Name
		,sEmployer_Contact_Tel_No
		,sMarital_Status
		,sLoan_Account_No
		,sSOL_ID
		,cLoan_Amount
		,sPhys_Add1
		,sPhys_Add2
		,sPhys_Add3
		,dtLoan_Value_Date
		,cPremium
		,cProperty_value
		,dtRepayment_Date
		,dtPolicy_Expiry_Date
		,sScheme_code
		,sCurrency_code
		,sConstruction_Type
		,sCover_Note
		,sPremium_Payment_Frequency
		,sInsurer_Own_insurance
		,sPolicy_ID_Own_insurance
		,cAmount_Own_insurance
		,dtExpiry_Date_Own_Insurance
		,sLoan_Account_No_Own_Insurance
		,sProduct_Scheme_Code
		,sSegment
		,sSub_segment
		,sSerial_Number
		,sRisk_Address
		,sUnderwriter_Name
		,sPIN_number
		,iID
		,iImportRecNo
		,dtProcessDate
		,iSourceFileID
		,sNewLoanAccountNo
		,sCountry
		,sCountryClassification
		,sCountry_Cif
		,iCountryID
		,PH_ProductID
		,RH_CoInsurer
		,Premium
		,AnnualPremium
		,Sasria
		,AnnualSasria
		,Commission
		,AnnualCommission
		,CommissionPerc
		,PolicyFee
		,AnnualPolicyFee
		,AnnualAdminFee
		,AnnualBrokerFee
		,CustomCalc1
		,CustomCalc2
		,CustomCalc3
		,CustomCalc4
		,FileName
		)
	SELECT
		dtTransaction_Date
		,sReason_Code
		,sCIF_Number
		,sCustomer_Title
		,sCustomer_First_Name
		,sCustomer_Surname
		,sGender
		,dtDate_of_Birth
		,sID_Passport_Number
		,sPostal_Address_1
		,sPostal_Address_2
		,sPostal_Address_3
		,sPostal_Code
		,sTel_No
		,sEmployer_Name
		,sEmployer_Contact_Tel_No
		,sMarital_Status
		,sLoan_Account_No
		,sSOL_ID
		,cLoan_Amount
		,sPhys_Add1
		,sPhys_Add2
		,sPhys_Add3
		,dtLoan_Value_Date
		,cPremium
		,cProperty_value
		,dtRepayment_Date
		,dtPolicy_Expiry_Date
		,sScheme_code
		,sCurrency_code
		,sConstruction_Type
		,sCover_Note
		,sPremium_Payment_Frequency
		,sInsurer_Own_insurance
		,sPolicy_ID_Own_insurance
		,cAmount_Own_insurance
		,dtExpiry_Date_Own_Insurance
		,sLoan_Account_No_Own_Insurance
		,sProduct_Scheme_Code
		,sSegment
		,sSub_segment
		,sSerial_Number
		,sRisk_Address
		,sUnderwriter_Name
		,sPIN_number
		,iID
		,iImportRecNo
		,dtProcessDate
		,iSourceFileID
		,sNewLoanAccountNo
		,sCountry
		,sCountryClassification
		,sCountry_Cif
		,iCountryID
		,PH_ProductID
		,RH_CoInsurer
		,Premium
		,AnnualPremium
		,Sasria
		,AnnualSasria
		,Commission
		,AnnualCommission
		,CommissionPerc
		,PolicyFee
		,AnnualPolicyFee
		,AnnualAdminFee
		,AnnualBrokerFee
		,CustomCalc1
		,CustomCalc2
		,CustomCalc3
		,CustomCalc4
		,FileName
	FROM
		[SKI_int].[SBABDailySTINewBusiness];
	    
	TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

	--========================================================================================================================
	--GET TOTAL NUMBER OF EXCEPTIONS
	--========================================================================================================================
	SELECT COUNT(1) FROM [ski_int].[SSISDailyImportExceptions] WHERE Imported = 0;


	SET NOCOUNT ON;
	
END
GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STIPremiums]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [ski_int].[SBAB_STIPremiums] 
	
AS
BEGIN
-------------------------------------------------------------------------------
--Delete exising temp tables 
-------------------------------------------------------------------------------
--IF OBJECT_ID('tempdb..##Temp') IS NOT NULL
--DROP TABLE ##Temp;
--IF OBJECT_ID('tempdb..##TempInvalidDate') IS NOT NULL
--DROP TABLE ##TempInvalidDate;
--IF OBJECT_ID('tempdb..##TempInvalidNumeric') IS NOT NULL
--DROP TABLE ##TempInvalidNumeric;
------------------------------------------------------------------------------------------------------
--Create Variable @Dateto restirct only todays records
------------------------------------------------------------------------------------------------------

TRUNCATE TABLE [ski_int].[SBABSTIPremium];

DECLARE @Date date = (SELECT CONVERT(DATE,DATEADD(DD,0,GETDATE())))

-------------------------------------------------------------------
--Get all validated records
-------------------------------------------------------------------
--SELECT *
--INTO ##Temp
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and ISDATE(SUBSTRING(SourceRecord,2,8)) = 1
--		and ISDATE(SUBSTRING(SourceRecord,253,8)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,46,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,137,20)) = 1
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Get all invalid date records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidDate
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and (ISDATE(SUBSTRING(SourceRecord,2,8)) = 0
--		or ISDATE(SUBSTRING(SourceRecord,253,8)) = 0)
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Get all invalid numeric records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidNumeric
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and (ISNUMERIC(SUBSTRING(SourceRecord,46,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,137,20)) = 0)
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Insert the data for the latest records
-------------------------------------------------------------------

INSERT INTO [ski_int].[SBABSTIPremium]
SELECT 
					SUBSTRING(SourceRecord,1,1) Record_Id,
					CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,2,8)))) Transaction_Date ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))) Loan_Account_No	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,26,20))) ID_Number_Business_Reg_Number,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,46,20))) = '',0,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,46,20))))) Premium_Received ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,66,50))) CIF_Number	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,116,1))) Balance_Sign,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,117,20))) = '' ,NULL,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,117,20))))) Property_Value ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,137,20))) = '',0,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,20))))) Premium_due ,
					Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O') Scheme_code ,
					ISNULL(LMPCurrencyCode.TargetDBValue,NULL) Currency_Code,
					IIF(SUBSTRING(SourceRecord,162,30) = '' ,NULL,SUBSTRING(SourceRecord,162,30)) Customer_Title	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,192,30))) Customer_First_Name	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,222,30))) Customer_Surname ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,252,1))) Gender	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,253,8))) = '' ,NULL,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,253,8))))) Date_of_Birth ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,261,20))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,261,20)))) ID_Passport_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,281,30))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,281,30)))) Postal_Address_1,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,311,30))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,311,30)))) Postal_Address_2,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,341,30))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,341,30)))) Postal_Address_3,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,371,10))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,371,10)))) Postal_Code,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,381,25))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,381,25)))) Tel_No,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,406,5))) = '',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,406,5)))) Marital_Status ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,411,30))) = '' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,411,30)))) Phys_Add1,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,441,30))) = '' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,441,30)))) Phys_Add2,	
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,471,30))) = '' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,471,30)))) Phys_Add3,	
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,501,5))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,501,5)))) Product_Scheme_Code ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,506,1))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,506,1)))) Status_Flag ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,507,1))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,507,1)))) Exception,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,508,2))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,508,2)))) Plan_Type,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5)))) Serial_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,515,20))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,515,20)))) Underwriter_Name,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,535,10))) = '' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,535,10))))	PIN_number	,
					-1 iImportRecNo,
					convert(datetime,substring(FileName,21,6)) ProcessDate,
					[ski_int].[SSIS_Src_RawData].ID iSourceFileID,
					CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O')) sNewLoanAccNo,
					Country
					,LMPProductID.TargetDBValue
					,ski_int.SBAB_GetPolicyID_STI(CONCAT(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 66, 50)))),
												  IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5)))), 
					                              CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O')),LMPProductID.TargetDBValue)
					,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 66, 50)))) Country_Cif
					,COU.Id
	FROM [ski_int].[SSIS_Src_RawData]
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O') +'_' + Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
	LEFT JOIN CFG.Country COU on COU.Alpha2Code = [ski_int].[SSIS_Src_RawData].Country
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
		ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord,159,3)))
		AND LMPCurrencyCode.Type = 'Currency_Code'
		AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
WHERE FileName like '%sti.premium%'
		and SUBSTRING(SourceRecord,1,1) = 2
		and ProcessStatus = 0
		and CONVERT(DATE,DateImported) = @Date		

---------------------------------------------------------------------------------------------------------------------------
--Update the source record to processed to eliminate processing duplicates
---------------------------------------------------------------------------------------------------------------------------
--Validated Records
------------------------------------------------------------------
UPDATE SSF
		SET ProcessStatus = 1
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [ski_int].[SBABSTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE SSF.FileName like '%sti.premium%'
		and CONVERT(DATE,SSF.DateImported) = @Date;
------------------------------------------------------------------
--Invalid date records
------------------------------------------------------------------
--UPDATE SSF
--		SET iProcessStatus = 2
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidDate TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.sFileName like '%sti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date;
------------------------------------------------------------------
--Invalid numeric records
------------------------------------------------------------------
--UPDATE SSF
--		SET iProcessStatus = 3
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidNumeric TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.sFileName like '%sti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--IF ( SELECT TOP 1 sFileName
--			FROM SBAB_Src.StagingFileProcessAudit
--		 WHERE sFileName like '%sti.premiums%'
--				and CONVERT(DATE,dtDate) = @Date) IS NOT NULL

--BEGIN

--	DELETE FROM SBAB_Src.StagingFileProcessAudit
--					WHERE sFileName like '%sti.premiums%'
--							and CONVERT(DATE,dtDate) = @Date
--END

INSERT INTO SBAB_Src.StagingFileProcessAudit
SELECT  SUBSTRING(SourceRecord,1,1) Record_ID,
					iif(RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12))) = ' ',0,RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12)))) Count
					,filename FileName
					,DateImported ProcessDate
					,Country
					,CONVERT(FLOAT,SUBSTRING(SourceRecord,2,20)) TotalPremiumsReceived				
FROM [ski_int].[SSIS_Src_RawData]
 WHERE SUBSTRING(SourceRecord,1,1) = 9
 and FileName like '%sti.premium%'
 and CONVERT(DATE,DateImported) = @Date

EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%sti.premium%'
		and CONVERT(DATE,dtDate) = @Date
 UNION
-------------------------------------------------------------------------------------------------------------------------------------------------
--Create a record for audit table with count of records for the specific file,country and date
-------------------------------------------------------------------------------------------------------------------------------------------------
SELECT	0 AS Record_ID,
					COUNT(*) Count,
					FileName FileName,
					DateImported ProcessDate,
					SSF.Country Country,
					SUM(cPremium_Received) TotalPremiumsReceived
FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [ski_int].[SBABSTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID 
WHERE FileName like '%sti.premium%'
		and CONVERT(DATE,DateImported) = @Date
 GROUP BY FileName,DateImported,SSF.Country

 EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%sti.premium%'
		and CONVERT(DATE,dtDate) = @Date


TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];


INSERT INTO [ski_int].[SBABSTIPremium_Log] 
(
dtTransaction_Date
,sLoan_Account_No
,sID_Number_Business_Reg_Number
,cPremium_Received
,sCIF_Number
,sBalance_Sign
,cProperty_Value
,cPremium_due
,sScheme_code
,sCurrency_code
,sCustomer_Title
,sCustomer_First_Name
,sCustomer_Surname
,sGender
,dtDate_of_Birth
,sID_Passport_Number
,sPostal_Address_1
,sPostal_Address_2
,sPostal_Address_3
,sPostal_Code
,sTel_No
,sMarital_Status
,sPhys_Add1
,sPhys_Add2
,sPhys_Add3
,sProduct_Scheme_Code
,sStatus_Flag
,sException
,sPlan_Type
,sSerial_Number
,sUnderwriter_Name
,sPIN_number
,iImportRecNo
,dtProcessDate
,iSourceFileID
,sNewLoanAccNo
,sCountry
,PH_ProductID
,PH_PolicyID
,sCountry_Cif
,iCountryID
)

SELECT 
dtTransaction_Date
,sLoan_Account_No
,sID_Number_Business_Reg_Number
,cPremium_Received
,sCIF_Number
,sBalance_Sign
,cProperty_Value
,cPremium_due
,sScheme_code
,sCurrency_code
,sCustomer_Title
,sCustomer_First_Name
,sCustomer_Surname
,sGender
,dtDate_of_Birth
,sID_Passport_Number
,sPostal_Address_1
,sPostal_Address_2
,sPostal_Address_3
,sPostal_Code
,sTel_No
,sMarital_Status
,sPhys_Add1
,sPhys_Add2
,sPhys_Add3
,sProduct_Scheme_Code
,sStatus_Flag
,sException
,sPlan_Type
,sSerial_Number
,sUnderwriter_Name
,sPIN_number
,iImportRecNo
,dtProcessDate
,iSourceFileID
,sNewLoanAccNo
,sCountry
,PH_ProductID
,PH_PolicyID
,sCountry_Cif
,iCountryID
 FROM [ski_int].[SBABSTIPremium];

 

SET NOCOUNT ON;
END



GO
/****** Object:  StoredProcedure [ski_int].[SBAB_STIRawDataExceptions]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
CREATE PROCEDURE [ski_int].[SBAB_STIRawDataExceptions]
AS			

BEGIN

	--====================================================================================================
	--TRUNCATE PROCESS TABLE
	--====================================================================================================
	TRUNCATE TABLE [ski_int].[SSISDailyImportExceptions];

	--====================================================================================================
	--REMOVE SPECIAL CHARACTERS FIRST BEFORE VALIDATING ANY FURTHER
	--====================================================================================================
	UPDATE
		ski_int.SSIS_Src_RawData
	SET
		SourceRecord = ski_int.RemoveSpecialChars(SourceRecord);

	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Fixed Width format',
		LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
		'File Format Fixed Width Lenth Incorrect',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND (
			LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')) < 719
			)
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;


	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Record Id',
		SUBSTRING(SourceRecord,1,1),
		'Invalid Record Id',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		FileName like '%sti.newbus%'
		AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 0
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--FILE FORMAT INCORRECT
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Record Id',
		SUBSTRING(SourceRecord,1,1),
		'Invalid Record Id - Invalid Character in position 1',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		FileName like '%sti.newbus%'
		AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 1
		AND SUBSTRING(SourceRecord,1,1) NOT IN ('1','2','9')
		AND NOT EXISTS(
					   SELECT * 
					   FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--REASON CODE BLANK
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Reason Code',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))),
		'Reason Code is Invalid',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) = '' or RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) not in ('N','P','C','S','R'))
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CIFF NUMBER
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID-1,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'CIFF Number',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))),
		'CIFF Number is blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) = ''
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CUSTOMER FIRST NAME BLANK 
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Customer First Name',
		RTRIM(LTRIM(SUBSTRING(replace(t.SourceRecord, char(131),' '), 92, 30))),
		'Customer First Name is blank. Segment 001',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
			ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))))
			AND LMPSegmentType.Type = 'Cust_Segments'
			AND LMPSegmentType.SourceGroup = 'Segment'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 92, 30))) = '' 
		AND ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))) = '001'
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--CUSTOMER SURNAME BLANK 
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Customer Surname',
		RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30))),
		'Customer Surname is blank',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(REPLACE(SourceRecord, char(131),' '), 122, 30))) = '' 
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--SCHEME CODE WAS NOT SUPPLIED
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Scheme Code',
		REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O'),
		'Scheme Code blank/invalid',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord,1,1) = '2'
		AND FileName like '%sti.newbus%'
		AND ProcessStatus = 0
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))) = '' or REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O') <> 'O')
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--SEGMENT WAS NOT SUPPLIED
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		t.FileName,
		t.ID,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Segment Code',
		RTRIM(LTRIM(SUBSTRING(t.SourceRecord, 709, 3))),
		'Segment Code blank/invalid',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPSegmentType
			ON LMPSegmentType.SourceDBValue = Concat(t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))))
			AND LMPSegmentType.Type = 'Cust_Segments'
			AND LMPSegmentType.SourceGroup = 'Segment'
	WHERE 
		SUBSTRING(SourceRecord,1,1) = '2'
		AND FileName like '%sti.newbus%'
		AND ProcessStatus = 0
		AND ISNULL(LMPSegmentType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))) not in ('001','004','005')
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--DATE OF BIRTH INVALID
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		ID,
		FileName,
		ID,
		Country,
		DateImported,
		REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
		'Date Of Birth',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))),
		'Date Of Birth Invalid',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8)))) = 0
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--LOAN ACCOUNT NUMBER IS BLANK
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Account No',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))),
		'Loan Account No blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) = '' 
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;


	--====================================================================================================
	--LOAN AMOUNT IS BLANK OR NON NUME
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Amount',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),
		'Loan Amount is blank/Non-Numeric',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND ISNUMERIC(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20)))) = 0
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--LOAN VALUE DATE/DISBURSEMENT DATE IS BLANK
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Loan Value Date/Disbursement Date',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8))),
		'Loan Value Date/Disbursement Date is blank(Defaulted to file Date)',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8))) = ''
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;

	--====================================================================================================
	--PROPERTY VALUE IS BLANK OR ZERO
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Property Value',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))),
		'Property Value is blank or Zero',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND (RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '' or RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '0')
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					   )
	ORDER BY 
		1;


		Select isNumeric('0')
	--====================================================================================================
	--CURRENCY CODE NOT MAPPED/BLANK
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Currency Code',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 586, 3))),
		'Currency Code is invalid/not mapped',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
			ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 586, 3)))
			AND LMPCurrencyCode.Type = 'Currency_Code'
			AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND ISNULL(LMPCurrencyCode.TargetDBValue,NULL) is null
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--CONSTRUCTION TYPE NOT MAPPED/BLANK
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Construction Type',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 4))),
		'Construction Type is blank/not mapped',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPConstructType
			ON LMPConstructType.SourceDBValue = Concat(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O'),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 4))))
			AND LMPConstructType.Type = 'Construct_Type_STI'
			AND LMPConstructType.SourceGroup = 'Construction_Type'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND ISNULL(LMPConstructType.TargetDBValue,NULL)  is null
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--PREMIUM PAYMENT FREQUENCY
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Premium payment frequency',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1))),
		'Premium payment frequency is blank/not mapped(Defaulted to Monthly)',
		1
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1))) not in ('A','M')
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	--====================================================================================================
	--PREMIUM PAYMENT FREQUENCY
	--====================================================================================================

	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions]
	SELECT 
		t.ID,
		FileName,
		t.ID-1,
		t.Country,
		t.DateImported,
		REPLACE(REPLACE(t.SourceRecord, CHAR(10),''), CHAR(13),''),
		'Serial Number',
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 715, 5))),
		'Serial Number is blank',
		0
	FROM 
		[ski_int].[SSIS_Src_RawData] t
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND FileName like '%sti.newbus%'
		AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 715, 5))) = ''
		AND NOT EXISTS(
					   SELECT * FROM [ski_int].[SSISDailyImportExceptions] EXC
					   WHERE EXC.ImportRecordiID = t.ID
					   AND EXC.ImportFileName = t.FileName
					  )
	ORDER BY 
		1;

	
	--====================================================================================================
	--REMOVE EXCEPTION RECORDS FROM SOURCE TABLE
	--====================================================================================================
	DELETE 
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE EXISTS(
				SELECT 
					* 
				FROM 
					[ski_int].[SSISDailyImportExceptions] EXC
				WHERE
					EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
					AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
					AND Imported = 0
				);



	--====================================================================================================
	--INSERT DAILY EXCEPTIONS INTO LOG TABLE
	--====================================================================================================
	INSERT INTO 
		[ski_int].[SSISDailyImportExceptions_Log] 
	SELECT 
		* 
	FROM 
		[ski_int].[SSISDailyImportExceptions];

END
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddEmailConfig]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_AddEmailConfig]
	
    @MailServer VARCHAR(8000),
    @MailPort INT = 0,
    @MailUserName VARCHAR(100) = NULL,
    @MailPassword VARCHAR(8000) 
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISEmailConfig];

	INSERT INTO [ski_int].[SSISEmailConfig]
	(
		MailServer,
		MailPort,
		MailUserName,
		MailPassword
	)
	VALUES
	(
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailServer)),
		@MailPort,
		@MailUserName,
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailPassword))
	);
		

END
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_AddSSISUser]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_AddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISBatchUser];

	INSERT INTO [ski_int].[SSISBatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES EPROCSTATUS TO 777 FOR ALL RECORDS IN IMPORTRECORD TABLE. THESE RECORDS
--				:	WILL BE UPDATED IN BATCHES OF 1000 RECORDS TO EPROCSTATUS 1, TO IMPROVE EFFICIENCY
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_BatchImportProcessSourceDataForSKiImporter] 
	(
	@BatchAmountOfRecords VARCHAR(10)
	,@TableName SYSNAME
	,@TableCounter VARCHAR(1)
	)

AS

BEGIN
	
	--SET @TableName = CONCAT(@TableName,@TableCounter)

	--=====================================================
	--SET @BatchAmountOfRecords DEFAULT IF NO VALUE RECIEVED
	--=====================================================
	IF @BatchAmountOfRecords > 0 
		BEGIN
			SET @BatchAmountOfRecords = @BatchAmountOfRecords
		END
	ELSE
		BEGIN
			SET @BatchAmountOfRecords = 1000
		END;	

	--=====================================================
	--UPDATE IMPORTRECNO TO -1 - TO BE EXECUTED IN BATCHES
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000);

		SET @SQLCommand = 'UPDATE ' + N'' + @TableName + @TableCounter + ' SET iImportRecNo = -1 WHERE iID IN(SELECT TOP ' + (@BatchAmountOfRecords) + ' iID FROM ' + N'' + @TableName + @TableCounter + ' WHERE iImportRecNo = 999999)';
	
		EXECUTE (@SQLCommand);

END 








GO
/****** Object:  StoredProcedure [ski_int].[SSIS_CanContinue]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONTINUE NEXT STEP INDICATOR FOR THE PROCESS
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_CanContinue]
(
		@ProcessName VARCHAR(100)
		,@PackageName VARCHAR(100)
)
AS

BEGIN

	--==============================================================================================================
	--GET PACKAGE ID
	--==============================================================================================================
	DECLARE @PackageLogID INT;
	
	SELECT TOP 1 
		@PackageLogID = PackageLogID
	FROM
		[ski_int].[SSISPackageLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
	ORDER BY
		EndTime DESC;
		
	--==============================================================================================================
	--GET CONTINUE NEXT STEP INDICATOR
	--==============================================================================================================
	SELECT TOP 1 
		ContinueNextStep
	FROM
		[ski_int].[SSISProgressLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
		AND PackageLogID = @PackageLogID
	ORDER BY
		EndTime DESC;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_DataErrorLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_DataErrorLog]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorSource NVARCHAR(255)
	, @SourceRecord NVARCHAR(255)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISDataErrorLog(
		PackageLogID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorSource
		,SourceRecord
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorSource
	, @SourceRecord
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EmailsToBeSent_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [ski_int].[SSIS_EmailsToBeSent_Insert]
		@PackageLogID INT
		,@ProcessName VARCHAR(255)
		,@JobReference VARCHAR(100)
		,@From VARCHAR(200)
		,@TO VARCHAR(400)
		,@CC VARCHAR(200)
		,@BCC VARCHAR(200)
		,@Subject VARCHAR(200)
		,@Body VARCHAR(8000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO [ski_int].[SSISMailsToBeSent](
		PackageLogID
		,ProcessName
		,JobReference
		,MailFrom
		,MailTO
		,MailCC
		,MailBCC
		,MailSubject
		,MailBody
		,DateToSend
		,Sent
		,DateSent
	)
	VALUES (
		@PackageLogID
		,@ProcessName
		,@JobReference
		,@From
		,@TO
		,@CC
		,@BCC
		,@Subject
		,@Body
		,GetDate()
		,0
		,NULL
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndExtractLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndExtractLog]
  @ExtractLogID INT
, @ExtractCount INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastExtractDateTime DATETIME, @SQL NVARCHAR(255)
	SELECT @SQL = N'SELECT @LastExtractDateTime = ISNULL(MAX(ModifiedDate), ''1900-01-01'') FROM ski_int.imp_' + TableName FROM ski_int.SSISExtractLog WHERE ExtractLogID = @ExtractLogID
	EXEC sp_executeSQL @SQL, N'@LastExtractDateTime DATETIME OUTPUT', @LastExtractDateTime OUTPUT
	
	UPDATE ski_int.SSISExtractLog
	SET
	  EndTime = GetDate()
	, ExtractCount = @ExtractCount
	, LastExtractDateTime = @LastExtractDateTime
	, Success = 1
	WHERE ExtractLogID = @ExtractLogID

	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndJob]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES THE [ski_int].[SSIS_EndJob] TALE ON COMPLETION OF THE JOB
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [ski_int].[SSIS_EndJob]
	@SSISJobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN

	--==============================================================================================================
	--UPDATE JOB
	--==============================================================================================================
	UPDATE 
		[ski_int].[SSISJob] 
	SET
		EndTime = GetDate()
		,JobReference = @JobReference 
		,Success = @Success
	WHERE 
		SSISJobID = @SSISJobID;
	

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndPackageLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndPackageLog]
	@JobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN


	UPDATE 
		ski_int.SSISPackageLog 
	SET
		EndTime = GetDate()
		,Success = @Success
		,JobReference = @JobReference
	WHERE 
		JobID = @JobID;

	UPDATE 
		ski_int.SSISProgressLog 
	SET
		EndTime = GetDate()
		,Success = @Success
	WHERE 
		JobID = @JobID;
	

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_EndProgressLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_EndProgressLog]
  @PackageLogID INT
  ,@Success INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ski_int.SSISProgressLog SET
	  EndTime = GetDate()
	, Success = @Success
	WHERE PackageLogID = @PackageLogID

	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ErrorLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ErrorLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100) 
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISErrorLog(
		JobID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ExecutePackages]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [ski_int].[SSIS_ExecutePackages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;






GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRaisingRecordCount]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE NUMBER OF POLICIES FOR THE CURRENT RAISING RUN
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashRaisingRecordCount] 
(
		@NextPaymentDate		DATE
		,@SplitDay				INT 
		,@PaymentMethod			VARCHAR(10)
		,@PaymentTerm			VARCHAR(10)
		,@Count					INT OUTPUT
)
AS

--EXEC [ski_int].[SSIS_FlashRaisingRecordCount] 0, 0, '2017-04-01','DEFAULT' ,10 ,'0,1' ,'0.3' ,'ZAM',0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		@Count = COUNT(P.iPolicyId)
	FROM 
		Policy p
	WHERE
		CONVERT(NVARCHAR(6),P.iPaymentTerm) IN (@PaymentTerm)
		AND CONVERT(NVARCHAR(6),P.iPaymentMethod) IN (@PaymentMethod) 
		AND P.iCollection <= @SplitDay
		AND (CONVERT(NVARCHAR(6), p.dNextPaymentDate, 112) = CONVERT(NVARCHAR(6), CAST(@NextPaymentDate AS datetime), 112))
		AND (p.iPolicyStatus < 2);

	SELECT @Count;

END 


GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRecieptingRecordCount]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE NUMBER OF POLICIES FOR THE CURRENT RECIEPTING RUN
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashRecieptingRecordCount] 
(
		@OnlyFeeJournals		INT
		,@InForceOnly			VARCHAR(1)
		,@NextPaymentDate		DATE
		,@FinancialAccount		VARCHAR(20) 
		,@CollectionDay			INT 
		,@PaymentMethod			VARCHAR(10)
		,@PaymentTerm			VARCHAR(10)
		,@CountryProductName	VARCHAR(3)
		,@Count					INT OUTPUT
)
AS

--EXEC [ski_int].[UspINT_FlashRecieptingRecordCount] 0, 0, '2017-04-01','DEFAULT' ,10 ,'0,1' ,'0.3' ,'ZAM',0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		@Count = COUNT(P.iPolicyId)
	FROM 
		DebitNoteBalance DNB (NOLOCK)
		INNER JOIN PolicyTransactions PT (NOLOCK) ON 
			PT.iDebitNoteID = DNB.iDebitNoteID 
			AND PT.iPolicyID = DNB.iPolicyID
		INNER JOIN Policy P (NOLOCK) ON 
			P.iPolicyID = PT.iPolicyID
		INNER JOIN Products PR (NOLOCK) ON 
			P.iProductid = PR.iProductID
	WHERE
		(PT.iTranType in  (1, 3, 5, 6, 9))
		AND (PT.iParentTranID < 0)
		AND (P.bFinanceEnabled = 1)
		AND (@OnlyFeeJournals = 0 OR ((PT.iTranType = 9) AND (PT.cPremium = 0) AND ((PT.cAdminFee + PT.cPolicyFee + PT.cBrokerFee) <> 0)))
		AND (@InForceOnly = 0 OR (p.dCoverStart <= @NextPaymentDate))
		AND (PT.sFinAccount = @FinancialAccount)
		AND (P.iCollection = @CollectionDay)
		AND P.iPaymentMethod IN (0,1) 
		AND P.iPaymentTerm IN (0,3)
		AND P.iPolicyStatus IN (0,1)
		AND (PR.sProductName LIKE @CountryProductName + '%')
		AND DNB.cPremiumBal <> 0;

	SELECT @Count;

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashTotalRecordCountForProcess]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_FlashTotalRecordCountForProcess] 
(
		@JobReference				VARCHAR(100)
		,@Created					INT OUTPUT
		,@Valid						INT OUTPUT			
		,@Warning					INT OUTPUT
		,@Error						INT OUTPUT
		,@Completed					INT OUTPUT
		,@Total						INT OUTPUT
)
AS

--EXEC [ski_int].[SSIS_FlashTotalRecordCountForProcess] 'RNG201705101400',0,0,0,0,0,0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		'CREATED'	= CASE	WHEN BJP.STATUS = 0 THEN COUNT(1) ELSE 0 END
		,'VALID'	= CASE	WHEN BJP.STATUS = 1 THEN COUNT(1) ELSE 0 END
		,'WARNING'	= CASE	WHEN BJP.STATUS = 2 THEN COUNT(1) ELSE 0 END
		,'ERROR'	= CASE	WHEN BJP.STATUS = 3 THEN COUNT(1) ELSE 0 END
		,'COMPLETED'= CASE	WHEN BJP.STATUS = 4 THEN COUNT(1) ELSE 0 END
	INTO 
		##FlashTotals
	FROM BATCH.JOB BJ
		INNER JOIN BATCH.JOBTASKSTATUS BJT (NOLOCK) ON 
			BJT.JOBID = BJ.ID
		INNER JOIN BATCH.JOBPOLICYTASKSTATUS BJP (NOLOCK) ON 
			BJP.JOBTASKSTATUSID = BJT.ID
	WHERE 
		BJ.REFERENCE = @JobReference
	GROUP BY 
		BJP.STATUS;

	SELECT 
		@Created	= SUM(CREATED)
		,@Valid		= SUM(VALID)
		,@Warning	= SUM(WARNING)
		,@Error		= SUM(ERROR)
		,@Completed	= SUM(COMPLETED)
		,@Total		= SUM(CREATED+VALID+WARNING+ERROR+COMPLETED)
	FROM 
		##FlashTotals;

	DROP TABLE ##FlashTotals;

	SELECT 
		@Created	
		,@Valid		
		,@Warning	
		,@Error		
		,@Completed	
		,@Total		

END 

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerField]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerField] 
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @EndorsementCode VARCHAR(15)
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewFieldUpdateJob @BatchReference, @EndorsementCode, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
			AND CDFields.sFieldCode IN
			(
				 'C_CLIENTTYPE'
				,'C_ENTITYNAME'
				,'C_TITLE'
				,'C_IDNUMPAS'
				,'C_CCTRUREG'
				,'C_DOB'
				,'C_PREFSHOCOMMET'
				,'C_HOMETEL'
				,'C_DAYTWORPHONUM'
				,'C_CELLNO'
				,'CL_TE_TP1EML'
				,'C_CLIENTTYPE0'
				,'C_ENTITYNAME0'
				,'C_TITLE0'
				,'CL_PRIMNAME'
				,'C_SURNAME'
				,'C_IDNUMPAS0'
				,'C_CCTRUREG0'
				,'C_DOB0'
				,'C_HOMETEL0'
				,'C_DAYTWORPHONU0'
				,'C_CELLNO0'
				,'CL_TE_TP1EML0'
				,'C_CLIENTTYPE1'
				,'C_ENTITYNAME1'
				,'C_TITLE1'
				,'CL_PRIMNAME0'
				,'C_SURNAME0'
				,'C_IDNUMPAS1'
				,'C_CCTRUREG1'
				,'C_DOB1'
				,'C_HOMETEL1'
				,'C_DAYTWORPHONU1'
				,'C_CELLNO1'
				,'CL_TE_TP1EML1'
				,'C_CLIENTTYPE2'
				,'C_ENTITYNAME2'
				,'C_TITLE2'
				,'CL_PRIMNAME1'
				,'C_SURNAME1'
				,'C_IDNUMPAS2'
				,'C_CCTRUREG2'
				,'C_DOB2'
				,'C_HOMETEL2'
				,'C_DAYTWORPHONU2'
				,'C_CELLNO2'
				,'CL_TE_TP1EML2'
				,'C_CLIENTTYPE3'
				,'C_ENTITYNAME3'
				,'C_TITLE3'
				,'CL_PRIMNAME2'
				,'C_SURNAME2'
				,'C_IDNUMPAS3'
				,'C_CCTRUREG3'
				,'C_DOB3'
				,'C_HOMETEL3'
				,'C_DAYTWORPHONU3'
				,'C_CELLNO3'
				,'CL_TE_TP1EML3'
			)


	--==================================================
	--3. CREATE ENTRY IN FIELD UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.FieldUpdateJobValue 
			(
			Id
			,JobTaskStatusId
			,ContextId
			,ContextType
			,FieldCode
			,sFieldValue
			,cFieldValue
			)
	SELECT 
		cfg.NewCombGUID()
		,@JobTaskId
		,CH.iCustomerID
		,1
		,CDFields.sFieldCode
		,CASE 
			CDFields.sFieldCode 
				WHEN 'C_CLIENTTYPE'		THEN FS.CH_CustomerType
				WHEN 'C_ENTITYNAME'		THEN FS.CH_EntityName
				WHEN 'C_TITLE'			THEN FS.CH_Title
				WHEN 'C_IDNUMPAS'		THEN FS.CH_IDNumber
				WHEN 'C_CCTRUREG'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB'			THEN FS.CH_DOB
				WHEN 'C_HOMETEL'		THEN FS.CH_HomeTelNumber
				WHEN 'C_DAYTWORPHONUM'	THEN FS.CH_WorkTelnumber
				WHEN 'C_CELLNO'			THEN FS.CH_CellNumber
				WHEN 'CL_TE_TP1EML'		THEN FS.CH_Email
				WHEN 'C_CLIENTTYPE0'	THEN FS.CD1_CustomerType
				WHEN 'C_ENTITYNAME0'	THEN FS.CH_EntityName
				WHEN 'C_TITLE0'			THEN FS.CD1_Title
				WHEN 'CL_PRIMNAME'		THEN FS.CD1_FirstName
				WHEN 'C_SURNAME'		THEN FS.CD1_Surname
				WHEN 'C_IDNUMPAS0'		THEN FS.CD1_IDNumber
				WHEN 'C_CCTRUREG0'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB0'			THEN FS.CD1_DOB
				WHEN 'C_HOMETEL0'		THEN FS.CD1_HomeTelNumber
				WHEN 'C_DAYTWORPHONU0'	THEN FS.CD1_WorkTelnumber
				WHEN 'C_CELLNO0'		THEN FS.CD1_CellNumber
				WHEN 'CL_TE_TP1EML0'	THEN FS.CD1_Email
				WHEN 'C_CLIENTTYPE1'	THEN FS.CD1_CustomerType
				WHEN 'C_ENTITYNAME1'	THEN FS.CH_EntityName
				WHEN 'C_TITLE1'			THEN FS.CD2_Title
				WHEN 'CL_PRIMNAME0'		THEN FS.CD2_FirstName
				WHEN 'C_SURNAME0'		THEN FS.CD2_Surname
				WHEN 'C_IDNUMPAS1'		THEN FS.CD2_IDNumber
				WHEN 'C_CCTRUREG1'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB1'			THEN FS.CD2_DOB
				WHEN 'C_HOMETEL1'		THEN FS.CD2_HomeTelNumber
				WHEN 'C_DAYTWORPHONU1'	THEN FS.CD2_WorkTelnumber
				WHEN 'C_CELLNO1'		THEN FS.CD2_CellNumber
				WHEN 'CL_TE_TP1EML1'	THEN FS.CD2_Email
				WHEN 'C_CLIENTTYPE2'	THEN FS.CD2_CustomerType
				WHEN 'C_ENTITYNAME2'	THEN FS.CH_EntityName
				WHEN 'C_TITLE2'			THEN FS.CD3_Title
				WHEN 'CL_PRIMNAME1'		THEN FS.CD3_FirstName
				WHEN 'C_SURNAME1'		THEN FS.CD3_Surname
				WHEN 'C_IDNUMPAS2'		THEN FS.CD3_IDNumber
				WHEN 'C_CCTRUREG2'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB2'			THEN FS.CD3_DOB
				WHEN 'C_HOMETEL2'		THEN FS.CD3_HomeTelNumber
				WHEN 'C_DAYTWORPHONU2'	THEN FS.CD3_WorkTelnumber
				WHEN 'C_CELLNO2'		THEN FS.CD3_CellNumber
				WHEN 'CL_TE_TP1EML2'	THEN FS.CD3_Email
				WHEN 'C_CLIENTTYPE3'	THEN FS.CD3_CustomerType
				WHEN 'C_ENTITYNAME3'	THEN FS.CH_EntityName
				WHEN 'C_TITLE3'			THEN FS.CD4_Title
				WHEN 'CL_PRIMNAME2'		THEN FS.CD4_FirstName
				WHEN 'C_SURNAME2'		THEN FS.CD4_Surname
				WHEN 'C_IDNUMPAS3'		THEN FS.CD4_IDNumber
				WHEN 'C_CCTRUREG3'		THEN (CASE FS.CH_CustomerType WHEN 'A' THEN FS.CH_IDNumber ELSE '' END)
				WHEN 'C_DOB3'			THEN FS.CD4_DOB
				WHEN 'C_HOMETEL3'		THEN FS.CD4_HomeTelNumber
				WHEN 'C_DAYTWORPHONU3'	THEN FS.CD4_WorkTelnumber
				WHEN 'C_CELLNO3'		THEN FS.CD4_CellNumber
				WHEN 'CL_TE_TP1EML3'	THEN FS.CD4_Email
			ELSE 
				CDFields.sFieldValue 
			END
		,1
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
			AND CDFields.sFieldCode IN
			(
				 'C_CLIENTTYPE'
				,'C_ENTITYNAME'
				,'C_TITLE'
				,'C_IDNUMPAS'
				,'C_CCTRUREG'
				,'C_DOB'
				,'C_PREFSHOCOMMET'
				,'C_HOMETEL'
				,'C_DAYTWORPHONUM'
				,'C_CELLNO'
				,'CL_TE_TP1EML'
				,'C_CLIENTTYPE0'
				,'C_ENTITYNAME0'
				,'C_TITLE0'
				,'CL_PRIMNAME'
				,'C_SURNAME'
				,'C_IDNUMPAS0'
				,'C_CCTRUREG0'
				,'C_DOB0'
				,'C_HOMETEL0'
				,'C_DAYTWORPHONU0'
				,'C_CELLNO0'
				,'CL_TE_TP1EML0'
				,'C_CLIENTTYPE1'
				,'C_ENTITYNAME1'
				,'C_TITLE1'
				,'CL_PRIMNAME0'
				,'C_SURNAME0'
				,'C_IDNUMPAS1'
				,'C_CCTRUREG1'
				,'C_DOB1'
				,'C_HOMETEL1'
				,'C_DAYTWORPHONU1'
				,'C_CELLNO1'
				,'CL_TE_TP1EML1'
				,'C_CLIENTTYPE2'
				,'C_ENTITYNAME2'
				,'C_TITLE2'
				,'CL_PRIMNAME1'
				,'C_SURNAME1'
				,'C_IDNUMPAS2'
				,'C_CCTRUREG2'
				,'C_DOB2'
				,'C_HOMETEL2'
				,'C_DAYTWORPHONU2'
				,'C_CELLNO2'
				,'CL_TE_TP1EML2'
				,'C_CLIENTTYPE3'
				,'C_ENTITYNAME3'
				,'C_TITLE3'
				,'CL_PRIMNAME2'
				,'C_SURNAME2'
				,'C_IDNUMPAS3'
				,'C_CCTRUREG3'
				,'C_DOB3'
				,'C_HOMETEL3'
				,'C_DAYTWORPHONU3'
				,'C_CELLNO3'
				,'CL_TE_TP1EML3'
			)

END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdateCustomerHeader]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdateCustomerHeader]
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewCustomerUpdateJob @BatchReference, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID


	--==================================================
	--3. CREATE ENTRY IN CUSTOMER UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.CustomerUpdateJobValue 
			(
				Id
				,JobTaskStatusId
				,CustomerId
				,sLastName
				,PhysAddrLine1
				,PhysAddrLine2
				,PhysAddrLine3
				,PhysAddrSuburb
				,PhysAddrPostCode
				,PhysAddrRatingArea
				,PostAddrLine1
				,PostAddrLine2
				,PostAddrLine3
				,PostAddrSuburb
				,PostAddrPostCode
				,PostAddrRatingArea
			)
	SELECT DISTINCT
		cfg.NewCombGUID()
		,@JobTaskId
		,CH.iCustomerID 
		,FS.CH_Surname
		,FS.CH_ResidentialAddress1
		,FS.CH_ResidentialAddress2
		,FS.CH_ResidentialAddressCitySuburb
		,FS.CH_ResidentialAddressCountryCode
		,FS.CH_ResidentialAddressPostalCode
		,FS.CH_ResidentialAddressCitySuburb
		,FS.CH_PostalAddress1
		,FS.CH_PostalAddress2
		,FS.CH_PostalAddressCitySuburb
		,FS.CH_PostalAddressCountryCode
		,FS.CH_PostalAddressPostalCode
		,FS.CH_PostalAddressCitySuburb
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'
		INNER JOIN Customer CH ON
			CH.iCustomerID = p.iCustomerID
		INNER JOIN CustomerDetails CDFields ON
			CH.iCustomerID = CDFields.iCustomerID
		LEFT JOIN [Address] RESADD ON
			RESADD.intAddressID = CH.rPhysAddressID
			AND UPPER(RESADD.strAddressType) = 'PHYSICAL'
		LEFT JOIN [Address] POSTADD ON
			POSTADD.intAddressID = CH.rPostAddressID
			AND UPPER(POSTADD.strAddressType) = 'POSTAL'

END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdatePolicyField]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_FlashUpdatePolicyField] 
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @EndorsementCode VARCHAR(15)
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewFieldUpdateJob @BatchReference, @EndorsementCode, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'


	--==================================================
	--3. CREATE ENTRY IN FIELD UPDATE JOB VALUE
	--==================================================
	INSERT INTO 
		batch.FieldUpdateJobValue 
			(
			Id
			,JobTaskStatusId
			,ContextId
			,ContextType
			,FieldCode
			,sFieldValue
			,cFieldValue
			)
	SELECT 
		cfg.NewCombGUID()
		,@JobTaskId
		,PD.iPolicyID
		,2
		,PD.sFieldCode
		,CASE 
			PD.sFieldCode 
				WHEN 'P_MORTLOAACCNUM'	THEN FS.CH_CustomerPolicyLink 
			ELSE 
				PD.sFieldValue 
			END
		,1
	FROM 
		Policy P 
		INNER JOIN PolicyDetails PD ON
			P.iPolicyID = PD.iPolicyID
			AND PD.sFieldCode = 'P_MORTLOAACCNUM'
		INNER JOIN [ski_int].[STG_FlashSource] FS ON
			FS.CH_CustomerPolicyLink = PD.sFieldValue
			AND FS.RD_ReferenceNumber1 = P.sOldPolicyNo
			AND FS.PH_ProductStatus = 'U'


END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForFlash]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT EXCTRACTS ALL RECORDS FROM PROVIDED PARAMATER TABLE NAME TO BE UPDATED BY FLASH
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetAllDataForFlash] 
	(
	@TableName SYSNAME
	)

AS

BEGIN

	--DECLARE @TableName SYSNAME
	--SET @TableName = '[SBAB_src].[DailyLTINewBusiness]'

	--=====================================================
	--EXTRACT ALL DATA FOR FLASH TO PROCESS
	--=====================================================
	DECLARE 
		@SQLCommand varchar(8000)

	SET @SQLCommand = 'SELECT * INTO ski_int.##FlashData FROM ' + N'' + @TableName + ' WHERE sReason_Code NOT IN(''N'')';

	EXEC(@SQLCommand);

	--=====================================================
	--EXTRACT ALL CUSTOMER UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashCustomerSource]
	SELECT 
		'' JobReference, 
		C.iCustomerID CustomerId,
		CD.sFieldCode FieldCode,
		CD.sFieldValue FieldValue,
		CD.sFieldValue FieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Customer C WITH(NOLOCK) ON
			C.iCustomerID = P.iCustomerID
		INNER JOIN CustomerDetails CD WITH(NOLOCK) ON
			C.iCustomerID = CD.iCustomerID
	WHERE
		SD.sReason_Code IN('U','P');

	--=====================================================
	--EXTRACT ALL CANCEL DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashCancelSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		Getdate()
	FROM 
		ski_int.##FlashData SD
	WHERE
		SD.sReason_Code = 'C';

	--=====================================================
	--EXTRACT ALL SUSPEND DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashSuspendSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		Getdate()
	FROM 
		ski_int.##FlashData SD
	WHERE
		SD.sReason_Code = 'S';

	--=====================================================
	--EXTRACT ALL REINSTATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashReInstateSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		R.intRiskID,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
	WHERE
		SD.sReason_Code = 'R';

	--=====================================================
	--EXTRACT ALL POLICY UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashPolicyUpdateSource] 
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		PD.sFieldCode,
		PD.sFieldValue,
		PD.sFieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN PolicyDetails PD WITH(NOLOCK) ON
			P.iPolicyID = PD.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
	WHERE
		SD.sReason_Code IN('U','P');
		
	--=====================================================
	--EXTRACT ALL RISK UPDATE DATA FOR FLASH TO PROCESS
	--=====================================================
	INSERT INTO	
		[ski_int].[SSISFlashRiskUpdateSource]
	SELECT 
		'' JobReference, 
		SD.PH_ProductID ProductID,
		ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) PolicyId,
		R.intRiskID,
		RD.sFieldCode,
		RD.sFieldValue,
		RD.sFieldValue,
		Getdate()
	FROM 
		ski_int.##FlashData SD
		INNER JOIN Policy P WITH(NOLOCK) ON
			ski_int.SBAB_GetPolicyID_LTI (SD.sCountry_Cif,SD.sNewLoanAccountNo,SD.PH_ProductID) = P.iPolicyID
		INNER JOIN Risks R WITH(NOLOCK) ON
			P.iPolicyID = R.intPolicyID
		INNER JOIN RiskDetails RD WITH(NOLOCK) ON
			R.intRiskID = RD.iRiskID
	WHERE
		SD.sReason_Code IN('U','P');

	--=====================================================
	--DROP TABLE ski_int.##FlashData IF EXISTS
	--=====================================================
    DROP TABLE 
		ski_int.##FlashData;

END 

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetAllDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetAllDataForSKiImporter] 
(
	@TableName SYSNAME
)
AS

BEGIN

	--=====================================================
	--GET ALL DATA WHERE IMPORTRECNO IS 999999
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'SELECT * FROM ' + N'' + @TableName + ' WHERE   iImportRecNo = 999999';

	EXEC(@SQLCommand);
	
END 

	
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetDataForSKiImporter] 

AS

BEGIN

	--=====================================================
	--GET ALL RECORDS
	--=====================================================
	SELECT 
		* 
	FROM 
		dbo.ImportRecord IR 
	WHERE
		IR.eProcStatus = 8
	ORDER BY 
		IR.iImportRecNo
	
END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetFlashProcessConfig]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetFlashProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		--INNER JOIN [ski_int].[SSISCommunicationType] CT (NOLOCK) ON
		--	C.CommunicationTypeId = CT.Id
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISSript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISSript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISSript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISSript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISSript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISSript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISSript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetJobSchedulerResults]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_GetJobSchedulerResults]

AS

BEGIN

	--==============================================================================================================
	--SELECT
	--==============================================================================================================
	SELECT 
	[sJOB].[name] AS [JobName]
    , CASE 
        WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
        ELSE CAST(
                CAST([sJOBH].[run_date] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [LastRunDateTime]
    , CASE [sJOBH].[run_status]
        WHEN 0 THEN 'Failed'
        WHEN 1 THEN 'Succeeded'
        WHEN 2 THEN 'Retry'
        WHEN 3 THEN 'Canceled'
        WHEN 4 THEN 'Running' -- In Progress
      END AS [LastRunStatus]
    , STUFF(
            STUFF(RIGHT('000000' + CAST([sJOBH].[run_duration] AS VARCHAR(6)),  6)
                , 3, 0, ':')
            , 6, 0, ':') 
        AS [LastRunDuration (HH:MM:SS)]
    , [sJOBH].[message] AS [LastRunStatusMessage]
    , CASE [sJOBSCH].[NextRunDate]
        WHEN 0 THEN NULL
        ELSE CAST(
                CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [NextRunDateTime]
	FROM 
		[msdb].[dbo].[sysjobs] AS [sJOB]
		LEFT JOIN (
					SELECT
						[job_id]
						, MIN([next_run_date]) AS [NextRunDate]
						, MIN([next_run_time]) AS [NextRunTime]
					FROM [msdb].[dbo].[sysjobschedules]
					GROUP BY [job_id]
				) AS [sJOBSCH]
			ON [sJOB].[job_id] = [sJOBSCH].[job_id]
		LEFT JOIN (
					SELECT 
						[job_id]
						, [run_date]
						, [run_time]
						, [run_status]
						, [run_duration]
						, [message]
						, ROW_NUMBER() OVER (
												PARTITION BY [job_id] 
												ORDER BY [run_date] DESC, [run_time] DESC
						  ) AS RowNumber
					FROM [msdb].[dbo].[sysjobhistory]
					WHERE [step_id] = 0
				) AS [sJOBH]
			ON [sJOB].[job_id] = [sJOBH].[job_id]
			AND [sJOBH].[RowNumber] = 1
	ORDER BY [JobName]

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcess]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcess]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
		,ISNULL(@ProcessType,'') ProcessType
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessConfig]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
		,ISNULL(@ProcessType,'') ProcessType
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 







GO
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessToExecute]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE FILE NAME THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcessToExecute] 
(
		@sFileDirection VARCHAR(50)
		,@sPackageNameToExecute VARCHAR(50)
		,@sFileName VARCHAR(255)
)
AS

BEGIN

	--==============================================================================================================
	--GET RELEVANT INFO OF PACKAGE TO EXECUTE
	--==============================================================================================================
	SELECT 
		MF.sPackageName
		,MF.sFileLocation
		,CASE WHEN MF.sPackageName = 'ClaimsMigration' THEN MF.sFileName ELSE MF.sFileName END AS 'sFileName'
		,MF.sFileProcessedLocation  
		,MF.sPackageFolderName
		,MF.sPackageProjectName
		,MS.iFileFormatID
	FROM 
		[ski_int].[IntegrationMasterFiles] MF (NOLOCK)
		INNER JOIN [ski_int].[IntegrationMasterScripts] MS (NOLOCK) ON 
			MF.ID = MS.iIntegrationMasterFileID
	WHERE 
		MF.sFileDirection = @sFileDirection 
		AND MF.sPackageName = @sPackageNameToExecute
		AND MF.sFileName = @sFileName
		AND MF.iStatusID = 1 


END 





GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ImportLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_ImportLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @FileName NVARCHAR(255)
	, @FileRecordCount INT
	, @InsertCount INT
	, @UpdateCount INT
	, @CancelCount INT
	, @RejectCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISImportLog (
		JobID
		,ProcessName
		,JobReference
		,FileName
		,FileRecordCount
		,InsertCount
		,UpdateCount
		,CancelCount
		,RejectCount
		,StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @FileName
	, @FileRecordCount
	, @InsertCount
	, @UpdateCount
	, @CancelCount
	, @RejectCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_MonitorSKiImporterProcess]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT SHOWS THE AMOUNT OF RECORDS PROCESSED/UNPROCESSED FOR SKi IMPORTER
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_MonitorSKiImporterProcess] 

AS

BEGIN

	--=====================================================
	--GET TOTALS BY THE HOUR
	--=====================================================
	SELECT 
		'Import Date And Hour'				= DATEADD(HOUR,DATEDIFF(HOUR, 0, dDateCreated), 0)
		,'Total Imported Policies By Hour'	= COUNT(1)
	FROM 
		[dbo].[Policy]
	GROUP BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0)
	ORDER BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0);

	--=====================================================
	--GET SUMMARY
	--=====================================================
	SELECT 
		'Current Time'		= GETDATE()
		,'Process Status'	= CASE WHEN eProcStatus = 1 THEN 'Busy Importing' WHEN eProcStatus = 3 THEN 'Imports Completed' WHEN eProcStatus = 8 THEN 'To Be Imported' ELSE '' END
		,'Total Summary'	= COUNT(1) 
	FROM 
		ImportRecord 
	GROUP BY 
		eProcStatus;
	
END 




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ProgressLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ProgressLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ContinueNextStep INT
	, @Message NVARCHAR(255)
	,@Success INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISProgressLog (
	  JobID
	, ProcessName
	, JobReference
	, PackageName
	, TaskName
	, ContinueNextStep
	, Message
	, StartTime
	, Success
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ContinueNextStep
	, @Message
	, GetDate()
	, @Success
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_RaisingLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_RaisingLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @RaisingQueryCount INT
	, @RaisedCount INT
	, @NotRaisedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISRaisingLog (
		PackageLogID
		,ProcessName
		,JobReference
		,RaisingQueryCount
		,RaisedCount
		,NotRaisedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @RaisingQueryCount
	, @RaisedCount
	, @NotRaisedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_ReceiptingLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_ReceiptingLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @ReceiptingQueryCount INT
	, @ReceiptingCount INT
	, @NotReceiptedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISReceiptingLog (
		PackageLogID
		,ProcessName
		,JobReference
		,ReceiptingQueryCount
		,ReceiptingCount
		,NotReceiptedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @ReceiptingQueryCount
	, @ReceiptingCount
	, @NotReceiptedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_SplitDataToImport]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [ski_int].[SSIS_SplitDataToImport] 
	(
	@UserName VARCHAR(100)
	,@ImportFromTableName SYSNAME
	)
AS 
BEGIN 

	DECLARE 
		@TotalNumberOfRecords INT
		,@TotalNumberOfProcesses INT
		,@TableNumberCounter INT 
		,@TotalNumberOfRecordsSplit INT
		,@SQLCommand NVARCHAR(MAX)

	--=====================================================
	--UPDATE USER ACCOUNT TO NOT EXPIRE
	--=====================================================
	UPDATE SecUsers SET dLastUpdated = GETDATE() WHERE sUserLogin = @UserName;

	--=====================================================
	--UPDATE IMPORTRECNO TO 999999 - TO BE EXECUTED IN BATCHES
	--=====================================================
	SET @SQLCommand = 'UPDATE ' + N'' + @ImportFromTableName + ' SET iImportRecNo = 999999 WHERE iImportRecNo = -1'

	EXEC(@SQLCommand);
	
	--==============================================================================================================
	--GET TOTAL NUMBER OF RECORDS TO IMPORT
	--==============================================================================================================
	SET @TotalNumberOfRecords = (	
									SELECT 
										COUNT_BIG(iSourceFileID)  
									FROM 
										[ski_int].[SBABDailyLTINewBusiness] LTI (NOLOCK)
									)                                                                    

	SET @TotalNumberOfProcesses = 5
	SET @TotalNumberOfRecordsSplit = @TotalNumberOfRecords / @TotalNumberOfProcesses
	SET @SQLCommand = ''
	SET @TableNumberCounter = 1

	--==============================================================================================================
	--SPLIT TOTAL NUMBER OF RECORDS TO IMPORT INTO 5
	--==============================================================================================================
	IF @TotalNumberOfRecords > 2000
	BEGIN

		WHILE @TableNumberCounter <= @TotalNumberOfProcesses 
		BEGIN
			SET @SQLCommand = @SQLCommand + '
								TRUNCATE TABLE [ski_int].[SBABDailyLTINewBusiness' + CONVERT(NVARCHAR(5),@TableNumberCounter) + ']
							 
								SELECT 
									* 
								INTO 
									#TEMP
								FROM 
									[ski_int].[SBABDailyLTINewBusiness]
								WHERE 
									iID IN (
											SELECT TOP ' + CONVERT(NVARCHAR(5), @TotalNumberOfRecordsSplit) + ' iID 
											FROM [ski_int].[SBABDailyLTINewBusiness]
											WHERE iImportRecNo = -1
											)
		
								DELETE 
									LTI
								FROM  
									[ski_int].[SBABDailyLTINewBusiness] LTI
									INNER JOIN #TEMP T ON 
										LTI.iSourceFileID = T.iSourceFileID  
		
								INSERT INTO 
									[ski_int].[SBABDailyLTINewBusiness' + Convert(nvarchar(5), @TableNumberCounter) + ']
								SELECT 
									*
								FROM 
									#TEMP

								DROP TABLE 
									#TEMP
								
								GO
								'
			SET @TableNumberCounter = @TableNumberCounter + 1
		END
	
		--PRINT (@SQLCommand) --Print to test
		EXEC (@SQLCommand) --Print to test

	END

END




GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartExtractLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_StartExtractLog]
  @PackageLogID INT
, @ProcessName NVARCHAR(255)
, @TableName NVARCHAR(100)
AS
BEGIN
	DECLARE @LastExtractDateTime	DATETIME
	SET NOCOUNT ON;
	
	SELECT @LastExtractDateTime = ISNULL(MAX(LastExtractDateTime), '1900-01-01')
	FROM ski_int.SSISExtractLog
	WHERE TableName = @TableName
		
	INSERT INTO ski_int.SSISExtractLog (
	  PackageLogID
	, ProcessName
	, TableName
	, StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @TableName
	, GetDate()
	)

	SELECT 
	  CAST(Scope_Identity() AS INT) ExtractLogID
	, CONVERT(NVARCHAR(23), @LastExtractDateTime, 121) LastExtractDateTimeString
	
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartJob]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENTRY [ski_int].[SSISJob] EVERY TIME A NEW SSIS JOB IS EXECUTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [ski_int].[SSIS_StartJob]
	@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)

AS

BEGIN
	--==============================================================================================================
	--SAMPLE EXECUTE
	--==============================================================================================================
	--EXEC [ski_int].[SSIS_StartJob] 'Import Daily LTI File - BW','REFERENCE1'

	--==============================================================================================================
	--INSERT JOB
	--==============================================================================================================
	INSERT INTO [ski_int].[SSISJob]
	(
		ProcessName
		,JobReference
		,StartTime
		,EndTime
		,DateCreated
		,Success
	)
	VALUES 
	(
		@ProcessName
		,@JobReference
		,GetDate()
		,NULL
		,GetDate()
		,0
	);

	--==============================================================================================================
	--RETURN LATEST JOB ID
	--==============================================================================================================
	SELECT CAST(Scope_Identity() AS INT) SSISJobID;

END






GO
/****** Object:  StoredProcedure [ski_int].[SSIS_StartPackageLog]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ski_int].[SSIS_StartPackageLog] 
	@JobID INT
	,@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO ski_int.SSISPackageLog 
	(
		JobID,
		ProcessName,
		JobReference,
		PackageName,
		StartTime
	)
	VALUES 
	(
		@JobID,
		@ProcessName,
		@JobReference,
		@PackageName,
		GetDate()
	);

	SELECT MAX(PackageLogID) FROM ski_int.SSISPackageLog;

END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_SuspendLog_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSIS_SuspendLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @SuspendQueryCount INT
	, @SuspendedCount INT
	, @NotSuspendedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISSuspendLog (
		PackageLogID
		,ProcessName
		,JobReference
		,SuspendQueryCount
		,SuspendedCount
		,NotSuspendedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @SuspendQueryCount
	, @SuspendedCount
	, @NotSuspendedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END

GO
/****** Object:  StoredProcedure [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES EPROCSTATUS TO 777 FOR ALL RECORDS IN IMPORTRECORD TABLE. THESE RECORDS
--				:	WILL BE UPDATED IN BATCHES OF 1000 RECORDS TO EPROCSTATUS 1, TO IMPROVE EFFICIENCY
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_UpdateImportProcessSourceDataForSKiImporter] 
(
	@UserName VARCHAR(100)
	,@TableName SYSNAME
)
AS

BEGIN

	--=====================================================
	--UPDATE USER ACCOUNT TO NOT EXPIRE
	--=====================================================
	UPDATE SecUsers SET dLastUpdated = GETDATE() WHERE sUserLogin = @UserName;

	--=====================================================
	--UPDATE IMPORTRECNO TO 999999 - TO BE EXECUTED IN BATCHES
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'UPDATE ' + N'' + @TableName + ' SET iImportRecNo = 999999 WHERE iImportRecNo = -1'

	EXEC(@SQLCommand);
	
END 







GO
/****** Object:  StoredProcedure [ski_int].[SSISFileLocations_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISFileLocations_Insert]
	 @LocationId VARCHAR(100)
	,@FileName VARCHAR(100)
	,@FileType INT
	,@FileHasHeaderRow INT
	,@FileHasFooterRow INT
	,@PickupLocation VARCHAR(255)
	,@ProcessedLocation VARCHAR(255)
	,@ArchiveLocation VARCHAR(255)
	,@LogFileLocation VARCHAR(255)
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISFileLocations]
		(
			LocationId
			,FileName
			,FileType
			,FileHasHeaderRow
			,FileHasFooterRow
			,PickupLocation
			,ProcessedLocation
			,ArchiveLocation
			,LogFileLocation
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			 @LocationId
			,@FileName 
			,@FileType
			,@FileHasHeaderRow 
			,@FileHasFooterRow 
			,@PickupLocation 
			,@ProcessedLocation 
			,@ArchiveLocation 
			,@LogFileLocation 
			,@UserLastUpdated 
			,GetDate()
		   );

END

GO
/****** Object:  StoredProcedure [ski_int].[SSISProcess_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISProcess_Insert]
	@Name VARCHAR(100)
	,@Description VARCHAR(400)
	,@ProcessTypeId INT
	,@CommunicationId INT
	,@FileLocationId INT
	,@ScriptId INT
	,@SSISDBCatalogFolderName VARCHAR(100)
	,@SSISDBCatalogProjectName VARCHAR(100)
	,@SSISDBCatalogPackageName VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISProcess]
		(
			Name
			,Description
			,ProcessTypeId
			,CommunicationId
			,FileLocationId
			,ScriptId
			,SSISDBCatalogFolderName
			,SSISDBCatalogProjectName
			,SSISDBCatalogPackageName
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@Name 
			,@Description 
			,@ProcessTypeId 
			,@CommunicationId 
			,@FileLocationId 
			,@ScriptId 
			,@SSISDBCatalogFolderName 
			,@SSISDBCatalogProjectName 
			,@SSISDBCatalogPackageName 
			,@Enabled 
			,@UserLastUpdated 
			,GetDate()
		   );

END

GO
/****** Object:  StoredProcedure [ski_int].[SSISScript_Insert]    Script Date: 23/10/2017 13:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ski_int].[SSISScript_Insert]
	@ScriptId INT
	,@ScriptTypeId INT
	,@Name VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISScript]
		(
			ScriptId
			,ScriptTypeId
			,Name
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@ScriptId
			,@ScriptTypeId
			,@Name
			,@Enabled
			,@UserLastUpdated
			,GetDate()
		   );

END

GO
