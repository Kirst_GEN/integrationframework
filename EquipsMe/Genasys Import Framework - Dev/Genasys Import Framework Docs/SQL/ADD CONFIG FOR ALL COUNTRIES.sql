--====================================================================================================
--PROCESS SETUP BY COUNTRY
--====================================================================================================

--GHANA				GH
--KENYA				KE
--MALAWI			MW
--NAMIBIA			NA
--SOUTH AFRICA		ZA
--SWAZILAND			SZ
--UGANDA			UG
--BOTSWANA			BW
--ZAMBIA			ZM
--ZIMBABWE			ZW

--===================================================
--GET CURRENT PROCESS SETUP - NAMIBIA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily LTI New - NA','fcb.NA.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - NA','fcb.NA.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - NA','fcb.NA.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - NA','fcb.NA.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Miscellaneous','Import Daily CrossSell - NA','fcb.NA.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - GHANA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - GH','fcb.GH.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - GH','fcb.GH.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - GH','fcb.GH.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - GH','fcb.GH.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - GH','fcb.GH.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - KENYA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - KE','fcb.KE.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - KE','fcb.KE.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - KE','fcb.KE.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - KE','fcb.KE.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - KE','fcb.KE.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - MALAWI
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - MW','fcb.MW.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - MW','fcb.MW.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - MW','fcb.MW.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - MW','fcb.MW.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - MW','fcb.UG.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - SOUTH AFRICA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - SZ','fcb.SZ.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - SZ','fcb.SZ.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - SZ','fcb.SZ.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - SZ','fcb.SZ.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - SZ','fcb.SZ.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - SWAZILAND
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - UG','fcb.UG.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - UG','fcb.UG.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - UG','fcb.UG.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - UG','fcb.UG.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - UG','fcb.UG.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - UGANDA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - UG','fcb.UG.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - UG','fcb.UG.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - UG','fcb.UG.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - UG','fcb.UG.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - UG','fcb.UG.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - BOTSWANA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - BW','fcb.BW.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - BW','fcb.BW.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - BW','fcb.BW.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - BW','fcb.BW.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - BW','fcb.BW.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - ZAMBIA
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - ZM','fcb.ZM.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - ZM','fcb.ZM.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - ZM','fcb.ZM.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - ZM','fcb.ZM.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - ZM','fcb.ZM.fnrl.crosssell'--LTI CROSS SELL

--===================================================
--GET CURRENT PROCESS SETUP - ZIMBABWE
--===================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Migration LTI - ZW','fcb.ZW.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - ZW','fcb.ZW.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - ZW','fcb.ZW.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - ZW','fcb.ZW.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily CrossSell - ZW','fcb.ZW.fnrl.crosssell'--LTI CROSS SELL



SELECT * FROM [ski_int].[SSISProcess]
SELECT * FROM [ski_int].[SSISCommunication]
SELECT * FROM [ski_int].[SSISFileLocations]
SELECT * FROM [ski_int].[SSISScript] 

Migration LTI - UG
Migration STI - UG

--===================================================
--SSISProcess
--===================================================
INSERT INTO [ski_int].[SSISProcess] 
(
	Name
	,Description
	,ProcessTypeId
	,CommunicationId
	,FileLocationId
	,ScriptId
	,SSISDBCatalogFolderName
	,SSISDBCatalogProjectName
	,SSISDBCatalogPackageName
	,Enabled
	,UserLastUpdated
	,DateLastUpdated
)
SELECT 
	Name
	,Description
	,ProcessTypeId
	,CommunicationId
	,FileLocationId
	,ScriptId
	,SSISDBCatalogFolderName
	,SSISDBCatalogProjectName
	,SSISDBCatalogPackageName
	,Enabled
	,UserLastUpdated
	,DateLastUpdated
FROM  
	[ski_int].[SSISProcess] 
WHERE 
	Name = 1
	AND ScriptId = 1;
	
--===================================================
--SCRIPT
--===================================================
INSERT INTO [ski_int].[SSISScript] 
(
	ScriptId
	,ScriptTypeId
	,Name
	,Enabled
	,UserLastUpdated
	,DateLastUpdated
)
SELECT 
	7
	,ScriptTypeId
	,'[SBAB_src].[STIPremium]'
	,Enabled
	,UserLastUpdated
	,DateLastUpdated
FROM  
	[ski_int].[SSISScript] 
WHERE 
	ScriptId = 6
