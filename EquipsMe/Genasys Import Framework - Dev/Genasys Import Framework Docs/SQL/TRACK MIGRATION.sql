	SELECT 
		'Import Date And Hour'				= DATEADD(HOUR,DATEDIFF(HOUR, 0, dDateCreated), 0)
		,'Total Imported Policies By Hour'	= COUNT(1)
	FROM 
		[dbo].[Policy]
	GROUP BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0)
	ORDER BY 
		DATEADD(HOUR, DATEDIFF(HOUR, 0, dDateCreated), 0) DESC;



SELECT 
	iImportID
	,eProcStatus
	,COUNT(1) 
FROM 
	ImportRecord
WHERE
	eProcStatus =1
GROUP BY
	iImportID
	,eProcStatus




--====================================================================================================
--MIGRATION CHECKS
--====================================================================================================
--==================================================
--DETAILED POLICY INFO
--==================================================
SELECT * FROM Policy (NOLOCK) WHERE sComment = 'Automated Integration'

--==================================================
--IMPORTED
--==================================================
SELECT COUNT(1) FROM Policy (NOLOCK) WHERE sComment = 'Automated Integration'

--==================================================
--DUPLICATES
--==================================================
SELECT sOldPolicyNo, COUNT(1) FROM Policy (NOLOCK) WHERE sComment = 'Automated Integration' GROUP BY sOldPolicyNo HAVING COUNT(1) > 1


SELECT * FROM Policy (NOLOCK) WHERE sComment = 'Automated Integration' AND sOldPolicyNo IN
(
SELECT sOldPolicyNo FROM Policy (NOLOCK) WHERE sComment = 'Automated Integration' GROUP BY sOldPolicyNo HAVING COUNT(1) > 1
)
ORDER BY sOldPolicyNo,dDateCreated DESC

select * from products

select * from lookup where sGroup like '%payment%'	
