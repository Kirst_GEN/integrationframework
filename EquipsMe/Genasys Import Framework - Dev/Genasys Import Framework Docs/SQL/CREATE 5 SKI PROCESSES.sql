--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT SPLITS ALL DATA TO BE IMPORTED INTO MULTIPLE TABLES. THESE RECORDS
--				:	WILL BE SPLIT IN BATCHES OF TOTAL NUMBER OF RECORDS TO TO BE IMPORTED, TO IMPROVE EFFICIENCY
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [ski_int].[SSIS_SplitDataToImport] 
	--(
		--@TableNumberCounter INT 
		--,@ImportFromTableName SYSNAME 
		--,@SQLCommand NVARCHAR(MAX)
	--)
AS 
BEGIN 

	DECLARE 
		@TotalNumberOfRecords INT
		,@TotalNumberOfProcesses INT
		,@TableNumberCounter INT 
		,@ImportFromTableName SYSNAME 
		,@TotalNumberOfRecordsSplit INT
		,@SQLCommand NVARCHAR(MAX)

	--==============================================================================================================
	--GET TOTAL NUMBER OF RECORDS TO IMPORT
	--==============================================================================================================
	SET @TotalNumberOfRecords = (	
									SELECT 
										COUNT_BIG(iSourceFileID)  
									FROM 
										[ski_int].[SBABDailyLTINewBusiness] LTI (NOLOCK)
									--WHERE 
									--	iImportRecNo = 999999										
									)                                                                    

	SET @TotalNumberOfProcesses = 5
	SET @TotalNumberOfRecordsSplit = @TotalNumberOfRecords / @TotalNumberOfProcesses
	SET @SQLCommand = ''
	SET @TableNumberCounter = 1

	--==============================================================================================================
	--SPLIT TOTAL NUMBER OF RECORDS TO IMPORT INTO 5
	--==============================================================================================================
	IF @TotalNumberOfRecords > 2000
	BEGIN

		WHILE @TableNumberCounter <= @TotalNumberOfProcesses 
		BEGIN
			SET @SQLCommand = @SQLCommand + '
								TRUNCATE TABLE [ski_int].[SBABDailyLTINewBusiness' + CONVERT(NVARCHAR(5),@TableNumberCounter) + ']
							 
								SELECT 
									* 
								INTO 
									#TEMP
								FROM 
									[ski_int].[SBABDailyLTINewBusiness]
								WHERE 
									iID IN (
											SELECT TOP ' + CONVERT(NVARCHAR(5), @TotalNumberOfRecordsSplit) + ' iID 
											FROM [ski_int].[SBABDailyLTINewBusiness]
											WHERE iImportRecNo = -1
											)
		
								DELETE 
									LTI
								FROM  
									[ski_int].[SBABDailyLTINewBusiness] LTI
									INNER JOIN #TEMP T ON 
										LTI.iSourceFileID = T.iSourceFileID  
		
								INSERT INTO 
									[ski_int].[SBABDailyLTINewBusiness' + Convert(nvarchar(5), @TableNumberCounter) + ']
									(
									sRecord_Id
									,dtTransaction_Date
									,sReason_Code
									,sCIF_Number
									,sCustomer_Title
									,sCustomer_First_Name
									,sCustomer_Surname
									,sGender
									,dtDate_of_Birth
									,sID_Passport_Number
									,sPostal_Address_1
									,sPostal_Address_2
									,sPostal_Address_3
									,sPostal_Code
									,sTel_No
									,sEmployer_Name
									,sEmployer_Contact_Tel_No
									,sMarital_Status
									,sLoan_Account_No
									,sSOL_ID
									,cLoan_Amount
									,sLoan_Term
									,sPlan_Type
									,dtLoan_Value_Date
									,cPremium
									,dtRepayment_Date
									,sSecond_Life_Insured_Title
									,sSecond_Life_Insured_First_Name
									,sSecond_Life_Insured_Surname
									,sSecond_Life_Insured_ID_Passport_No
									,sSecond_Life_DOB
									,sLTI_Flag
									,dtLTI_Expiry
									,cOutstanding_Balance
									,sScheme_Code
									,sCurrency_Code
									,sInsurer_Own_insurance
									,sPolicy_ID_Own_insurance
									,cAmount_Own_insurance
									,dtExpiry_Date_Own_Insurance
									,sProduct_Scheme_Code
									,sSegment
									,sSub_segment
									,sSerial_Number
									,sUnderwriter_Name
									,sPIN_number
									,iImportRecNo
									,dtProcessDate
									,iSourceFileID
									,sNewLoanAccountNo
									,sCountry
									,sCountryClassification
									,sCountry_Cif
									,iCountryID
									,RH_CoInsurer
									,PH_ProductID
									,FileName
									)
								SELECT 
									sRecord_Id
									,dtTransaction_Date
									,sReason_Code
									,sCIF_Number
									,sCustomer_Title
									,sCustomer_First_Name
									,sCustomer_Surname
									,sGender
									,dtDate_of_Birth
									,sID_Passport_Number
									,sPostal_Address_1
									,sPostal_Address_2
									,sPostal_Address_3
									,sPostal_Code
									,sTel_No
									,sEmployer_Name
									,sEmployer_Contact_Tel_No
									,sMarital_Status
									,sLoan_Account_No
									,sSOL_ID
									,cLoan_Amount
									,sLoan_Term
									,sPlan_Type
									,dtLoan_Value_Date
									,cPremium
									,dtRepayment_Date
									,sSecond_Life_Insured_Title
									,sSecond_Life_Insured_First_Name
									,sSecond_Life_Insured_Surname
									,sSecond_Life_Insured_ID_Passport_No
									,sSecond_Life_DOB
									,sLTI_Flag
									,dtLTI_Expiry
									,cOutstanding_Balance
									,sScheme_Code
									,sCurrency_Code
									,sInsurer_Own_insurance
									,sPolicy_ID_Own_insurance
									,cAmount_Own_insurance
									,dtExpiry_Date_Own_Insurance
									,sProduct_Scheme_Code
									,sSegment
									,sSub_segment
									,sSerial_Number
									,sUnderwriter_Name
									,sPIN_number
									,iImportRecNo
									,dtProcessDate
									,iSourceFileID
									,sNewLoanAccountNo
									,sCountry
									,sCountryClassification
									,sCountry_Cif
									,iCountryID
									,RH_CoInsurer
									,PH_ProductID
									,FileName
								FROM 
									#TEMP

								DROP TABLE 
									#TEMP
		
								GO
								'
			SET @TableNumberCounter = @TableNumberCounter + 1
		END
	
		PRINT (@SQLCommand) --Print to test

	END

END



