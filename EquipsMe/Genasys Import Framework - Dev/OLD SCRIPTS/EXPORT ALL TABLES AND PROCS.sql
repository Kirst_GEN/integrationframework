--========================================================================================================================
--CREATE TABLE
--========================================================================================================================
DECLARE @ListOfTableNames TABLE (sSchema VARCHAR(50), sTableName VARCHAR(255));

--========================================================================================================================
--INSERT TABLE NAMES INTO TABLE
--========================================================================================================================
INSERT INTO 
	@ListOfTableNames
SELECT DISTINCT      
	S.name AS 'sSchema',
	T.name AS 'sTableName'
FROM        
    SYS.COLUMNS C
    INNER JOIN SYS.TABLES T ON 
            C.OBJECT_ID = T.OBJECT_ID
    INNER JOIN SYS.schemas S ON 
            S.schema_id = T.schema_id
WHERE       
       S.name LIKE '%ski_int%'
       AND T.name NOT LIKE '%STAND%'
ORDER BY    
       T.name;

--========================================================================================================================
--GET TABLE NAMES FROM TABLE
--========================================================================================================================
SELECT * FROM @ListOfTableNames;


SELECT DISTINCT      
	S.name
FROM        
    SYS.COLUMNS C
    INNER JOIN SYS.TABLES T ON 
            C.OBJECT_ID = T.OBJECT_ID
    INNER JOIN SYS.schemas S ON 
            S.schema_id = T.schema_id
			order by S.name