USE [SKi_Equipsme_UAT]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashRaisingRecordCount]    Script Date: 2/22/2018 10:33:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE NUMBER OF POLICIES FOR THE CURRENT RAISING RUN
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	ALL
--==============================================================================================================
ALTER PROCEDURE [ski_int].[SSIS_FlashRiskFieldUpdateRecordCount] 
(
		 --@SourceFieldValue		VARCHAR(255)
		--,@SourceField			VARCHAR(50)
		--,@UpdateFieldValue		VARCHAR(255)
		--,
		@Count					INT OUTPUT
)
AS

--EXEC [ski_int].[SSIS_FlashRaisingRecordCount] 0, 0, '2017-04-01','DEFAULT' ,10 ,'0,1' ,'0.3' ,'ZAM',0

BEGIN

	--==============================================================================================================
	--GET COUNT OF POLICIES
	--==============================================================================================================
	SELECT 
		@Count = COUNT(iID)
	FROM 
		ski_int.AXASyncRecords


	SELECT @Count;

END 





