ALTER PROC [SKI_INT].[AXA_ExportPolicy] (@TableName varchar(50))
AS
--========================================================================================================================
--*** GROUP LOAD FILE ***
--exec [SKI_INT].[AXA_ExportPolicy] 'TEST'
--========================================================================================================================
--============================================================
--Group File Header
--============================================================
DECLARE 
	  @SQLCommandHeader varchar(MAX)
	, @SQLCommandDetail varchar(MAX)
	, @SQLCommandFooter varchar(MAX)

SET @SQLCommandHeader =
	
		'--========================================================================================================================
		--*** POLICY LOAD FILE ***
		--========================================================================================================================
		--============================================================
		--Policy File Header
		--============================================================
		DECLARE @iCount INT
		SET @iCount = 2

		SELECT 
			@iCount = @iCount + COUNT(*)
		FROM Customer c WITH (NOLOCK)
		JOIN Address a WITH (NOLOCK) ON a.intAddressID = c.rPhysAddressID
		JOIN Policy p WITH (NOLOCK) ON p.iCustomerID = c.iCustomerID AND p.iPolicyStatus < 4
		JOIN policydetails pd WITH (NOLOCK) ON pd.sFieldCode = ''P_AXAGROUNUMB'' AND pd.iPolicyID = p.iPolicyID
		JOIN Risks sme WITH (NOLOCK) ON sme.intpolicyid = p.iPolicyID AND sme.intRiskTypeID = 1005
		JOIN Risks emp WITH (NOLOCK) ON emp.intpolicyid = p.iPolicyID AND emp.intRiskTypeID = 1001 
			--AND (CAST(emp.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) OR CAST(emp.dDateModified AS DATE) = CAST(GETDATE() AS DATE))
		JOIN RiskDetails rd1 WITH (NOLOCK) ON rd1.iriskid = emp.intriskid AND rd1.sFieldCode = ''R_SMELEVEL''
		LEFT JOIN Risks support WITH (NOLOCK) ON support.intRiskID = sme.intRiskID AND support.intRiskTypeID = 1002
		LEFT JOIN Risks dentopt WITH (NOLOCK) ON dentopt.intRiskID = sme.intRiskID AND dentopt.intRiskTypeID = 1003
		JOIN RiskDetails axa WITH (NOLOCK) ON axa.iriskid = emp.intriskid AND axa.sFieldCode = ''R_AXAPPPPOLNO''
		JOIN RiskDetails epn WITH (NOLOCK) ON epn.iriskid = emp.intriskid AND epn.sFieldCode = ''R_EMPLOPOLINUMB''
		WHERE 
		(
			--CAST(p.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) 
			--OR CAST(p.dDateModified AS DATE) = CAST(GETDATE() AS DATE)
			--OR 
			axa.sFieldValue IS NULL
			OR axa.sFieldValue = ''''
		)
		
		INSERT INTO ' + @TableName + '
		SELECT * FROM (
			SELECT (''0''
					+ (REPLICATE(''0'', 4) + ''4'')																--Extract Count
					+ (FORMAT(GETDATE(), ''ddMMyyyy''))
					+ REPLACE(CONVERT(char(5), GETDATE(), 108), '':'', '''')) AS data

			UNION
		'
		SET @SQLCommandDetail =
			'--============================================================
			--Policy File Detail
			--============================================================
			SELECT 
				--emp.*, 
				--============================================================
				--Policy Data
				--============================================================
				(''1''																						--Record ID				1	Alphanumeric	Mandatory	Must be 1
				+ CASE 
					WHEN ISNULL(axa.sFieldValue, '''') = '''' 
					THEN ''NB'' 
					ELSE ''UP'' 
				END																							--Type of Update		2 	Alphanumeric	Mandatory	NB = new business, UP =update
				+ ''ESM''
				+ CONCAT(ISNULL(axa.sFieldValue, ''''),REPLICATE('' '', 8 - LEN(ISNULL(axa.sFieldValue, ''''))))	--AXA PPP Policy No		8	Alphanumeric	Optional	PPP Registration (Policy) No. Will not be known (so blank) when Type of Update is �NB�
				+ CASE 
					WHEN LEN(ISNULL(RTRIM(epn.sFieldValue), '''')) > 16 
					THEN LEFT(RTRIM(epn.sFieldValue), 16) 
					ELSE CONCAT(ISNULL(RTRIM(epn.sFieldValue), ''''),REPLICATE('' '', 16 - LEN(ISNULL(RTRIM(epn.sFieldValue), '''')))) 
				END																							--3rd Party Policy No	16 	Alphanumeric	Mandatory	Third Party Policy No
				+ CONCAT(p.iPolicyStatus,REPLICATE('' '', 1 - LEN(p.iPolicyStatus)))							--Policy Status			1 	Alphanumeric	Mandatory	For a new Policy or renewal it doesn�t matter what value is supplied, a value of 6 (Frozen) will be used at AXA PPP. A lapse will be reported using a Cover end date. Suspended Benefits will be handled via the non-payment flag.
				+ (FORMAT(ISNULL(emp.dInception, ''1899-12-31''), ''ddMMyyyy''))								--Cover Start Date		8 	Alphanumeric	Mandatory	 Format is DDMMCCYY
				+ CASE 
					WHEN emp.intstatus <> 2
					THEN (FORMAT(ISNULL(emp.dEffectiveDate, ''1899-12-31''), ''ddMMyyyy''))
					ELSE REPLICATE('' '', 8)
				END																							--Cover End Date		8 	Alphanumeric	Optional	If entered Policy status will be changed to lapsed when end date is < todays date. Format is DDMMCCYY
				+ (FORMAT(p.dRenewalDate, ''ddMMyyyy''))														--Renewal Date			8 	Alphanumeric	Mandatory  	Date Policy will renew. Format is DDMMCCYY (AXA PPP will change format to the DDMM required for Crest)
				+ CASE 
					WHEN LEN(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue)) + '' '' + RTRIM(efn.sfieldvalue) + '' '' + RTRIM(eln.sfieldvalue)) > 36 
					THEN LEFT(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue)) + '' '' + RTRIM(efn.sfieldvalue) + '' '' + RTRIM(eln.sfieldvalue), 36) 
					ELSE CONCAT(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue)) + '' '' + RTRIM(efn.sfieldvalue) + '' '' + RTRIM(eln.sfieldvalue),REPLICATE('' '', 36 - LEN(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue)) + '' '' + RTRIM(efn.sfieldvalue) + '' '' + RTRIM(eln.sfieldvalue)))) 
				END																							--Addressee				36 	Alphanumeric	Mandatory 	Policy holder address name : eg  Mr J R Smith
				+ CASE 
					WHEN LEN(RTRIM(ISNULL(eadr1.sfieldvalue, ''''))) > 36 
					THEN LEFT(RTRIM(ISNULL(eadr1.sfieldvalue, '''')), 36) 
					ELSE CONCAT(RTRIM(ISNULL(eadr1.sfieldvalue, '''')),REPLICATE('' '', 36 - LEN(RTRIM(ISNULL(eadr1.sfieldvalue, ''''))))) 
				END																							--Address Line 1		36 	Alphanumeric	Mandatory	Address line.     -     Premises / Street details
				+ CASE 
					WHEN LEN(RTRIM(ISNULL(eadr2.sfieldvalue, ''''))) > 36 
					THEN LEFT(RTRIM(ISNULL(eadr2.sfieldvalue, '''')), 36) 
					ELSE CONCAT(RTRIM(ISNULL(eadr2.sfieldvalue, '''')),REPLICATE('' '', 36 - LEN(RTRIM(ISNULL(eadr2.sfieldvalue, ''''))))) 
				END																							--Address Line 2		36 	Alphanumeric	Mandatory	Address line.     -     Town
				+ CASE 
					WHEN LEN(RTRIM(ISNULL(eadr3.sfieldvalue, ''''))) > 36 
					THEN LEFT(RTRIM(ISNULL(eadr3.sfieldvalue, '''')), 36) 
					ELSE CONCAT(RTRIM(ISNULL(eadr3.sfieldvalue, '''')),REPLICATE('' '', 36 - LEN(RTRIM(ISNULL(eadr3.sfieldvalue, ''''))))) 
				END																							--Address Line 3		36	Alphanumeric	Optional	Address line     -      County
				+ CASE 
					WHEN LEN(RTRIM(ISNULL(eadr4.sfieldvalue, ''''))) > 8 
					THEN LEFT(RTRIM(ISNULL(eadr4.sfieldvalue, '''')), 8) 
					ELSE CONCAT(RTRIM(ISNULL(eadr4.sfieldvalue, '''')),REPLICATE('' '', 8 - LEN(RTRIM(ISNULL(eadr4.sfieldvalue, '''')))))
				END																							--Post Code				8 	Alphanumeric	Mandatory	Post Code.     Format XX99 9XX
				+ CONCAT('''',REPLICATE('' '', 1))																--Phone No 1  Type		1 	Alphanumeric	Blank		Phone numbers not supplied for this arrangement
				+ CONCAT('''',REPLICATE('' '', 13))																--Phone No 1  			13 	Alphanumeric	Blank		Phone numbers not supplied for this arrangement
				+ CONCAT('''',REPLICATE('' '', 1))																--Phone No 2 Type		1 	Alphanumeric	Blank		Phone numbers not supplied for this arrangement
				+ CONCAT('''',REPLICATE('' '', 13))																--Phone No 2			13	Alphanumeric	Blank		Phone numbers not supplied for this arrangement
				+ CONCAT('''',REPLICATE('' '', 5))																--Agent Reference no	5 	Alphanumeric	Blank		Not applicable, no commission paid by AXA PPP
				+ CONCAT(ISNULL(pd.sFieldValue, ''''),REPLICATE('' '', 5 - LEN(ISNULL(pd.sFieldValue, ''''))))	--AXA PPP Group Number	5 	Alphanumeric	Mandatory	AXA PPP Group no, will be checked to make sure it is in allocated range.
				+ CASE 
					WHEN p.iPaymentStatus = 0 
					THEN ''N'' 
					ELSE ''Y''
				END
																											--Non Payment Flag		1 	Alphanumeric	Mandatory	�Y� = include non-payment exclusion text on Persons. �N� = don�t include non payment exclusion text
				--============================================================
				--Plan Data
				--============================================================
				+ CASE 
					WHEN ISNULL(axa.sFieldValue, '''') = '''' 
					THEN ''NB'' 
					ELSE ''UP'' 
				END																							--Type of Update		2 	Alphanumeric	Mandatory	�NB� = new business, �RN� = renewal, �UP� = update
				+ ''ESM''																						--3rd Party ID			3 	Alphanumeric	Mandatory	Identifies the 3rd party sourcing the data so ESM
				+ CONCAT(ISNULL(axa.sFieldValue, ''''),REPLICATE('' '', 8 - LEN(ISNULL(axa.sFieldValue, ''''))))	--AXA PPP Policy No		8 	Alphanumeric	Optional	PPP Registration (Policy) No. Will not be known (so blank) when Type of Update is �NB�
				+ CASE 
					WHEN LEN(ISNULL(RTRIM(epn.sFieldValue), '''')) > 16 
					THEN LEFT(RTRIM(epn.sFieldValue), 16) 
					ELSE CONCAT(ISNULL(RTRIM(epn.sFieldValue), ''''),REPLICATE('' '', 16 - LEN(ISNULL(RTRIM(epn.sFieldValue), '''')))) 
				END																							--3rd Party Policy No	16 	Alphanumeric	Mandatory	Third Party Policy No
				+ CONCAT(
					CASE 
						WHEN buyupLvl.sFieldValue IS NOT NULL 
						THEN CONCAT(ISNULL(buyupLvl.sFieldValue, ''''),REPLICATE('' '', 1 - LEN(ISNULL(buyupLvl.sFieldValue, '''')))) 
						ELSE CONCAT(ISNULL(rd1.sfieldvalue, ''''),REPLICATE('' '', 1 - LEN(ISNULL(rd1.sfieldvalue, '''')))) 
					END
				+ (CASE WHEN support.intRiskID IS NULL AND dentopt.intRiskID IS NULL THEN ''N''
					WHEN support.intRiskID IS NOT NULL AND dentopt.intRiskID IS NOT NULL THEN ''B''
					WHEN support.intRiskID IS NULL AND dentopt.intRiskID IS NOT NULL THEN ''D''
					WHEN support.intRiskID IS NOT NULL AND dentopt.intRiskID IS NULL THEN ''S''
				END),REPLICATE('' '', 2))																		--Plan Code				4	Alphanumeric	Mandatory	Code to identify specific plan. Char 1 � Level of cover - 1 to 4; Char 2 � Optional extras - N [none], S [support only], D [dental/optical] or B [both]; Char 3 & 4 - blank.
				+ ''M''																						--Payment Frequency		1	Alphanumeric	Mandatory	�A� = annual, �Q� = quarterly, M =- �monthly�. Will always be �M�.
				--============================================================
				--Personal Data (Policy Holder)
				--============================================================
				+ CASE 
					WHEN ISNULL(axa.sFieldValue, '''') = '''' 
					THEN ''NB'' 
					ELSE ''UP'' 
				END																							--Type of Update		2 	Alphanumeric	Mandatory	�NB� = new business, �RN� = renewal, �UP� = update
				+ ''ESM''																						--3rd Party ID			3 	Alphanumeric	Mandatory	Identifies the 3rd party sourcing the data so ESM
				+ CONCAT(ISNULL(axa.sFieldValue, ''''),REPLICATE('' '', 8 - LEN(ISNULL(axa.sFieldValue, ''''))))	--AXA PPP Policy No		8 	Alphanumeric	Optional	PPP Registration (Policy) No. Will not be known (so blank) when Type of Update is �NB�
				+ CASE 
					WHEN LEN(ISNULL(RTRIM(epn.sFieldValue), '''')) > 16 
					THEN LEFT(RTRIM(epn.sFieldValue), 16) 
					ELSE CONCAT(ISNULL(RTRIM(epn.sFieldValue), ''''),REPLICATE('' '', 16 - LEN(ISNULL(RTRIM(epn.sFieldValue), '''')))) 
				END																							--3rd Party Policy No	16 	Alphanumeric	Mandatory	Third Party Policy No
				+ ''1''																						--Life Id				1	Alphanumeric	Mandatory	Identifies the person details on a Policy, needed when amendments to personal details are sent set to 1-9
				+ ''X''																						--Relationship			1 	Alphanumeric	Mandatory.	X = subscriber ; H = husband ; W = wife ; D = daughter ; S = son ; E = ex-subscriber, A = adult relation ; C = child relation
				+ CASE 
					WHEN ISNULL(axa.sFieldValue, '''') = '''' 
					THEN ''2'' 
					ELSE 
						CASE 
							WHEN emp.intStatus = 2 
							THEN ''2'' 
							ELSE ''1'' 
						END 
				END																							--Person Status			1 	Alphanumeric	Mandatory	1 = Lapsed, 2 = Current
				+ CONCAT(LTRIM(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue))),REPLICATE('' '', 15 - LEN(LTRIM(RTRIM(dbo.GetLookupDescription(''EquipsmeTitle'', ett.sFieldValue))))))--Title	15	Alphanumeric	Blank		Title  
				+ CASE 
					WHEN LEN(RTRIM(efn.sfieldvalue)) > 12 
					THEN LEFT(RTRIM(efn.sfieldvalue), 12) 
					ELSE CONCAT(RTRIM(efn.sfieldvalue),REPLICATE('' '', 12 - LEN(RTRIM(efn.sfieldvalue)))) 
				END																							--Forename				12	Alphanumeric	Mandatory	 
				+ CASE
					WHEN LEN(RTRIM(eln.sfieldvalue)) > 20 
					THEN LEFT(RTRIM(eln.sfieldvalue), 20) 
					ELSE CONCAT(RTRIM(eln.sfieldvalue),REPLICATE('' '', 20 - LEN(RTRIM(eln.sfieldvalue)))) 
				END																							--Surname				20	Alphanumeric	Mandatory	 
				+ (FORMAT(edb.dtfieldvalue, ''ddMMyyyy''))													--Date of Birth			8 	Alphanumeric	Mandatory	 Format id DDMMCCYY.
				+ CASE 
					WHEN egd.sfieldvalue = ''MALE'' 
					THEN ''M'' 
					ELSE ''F''
				END																							--Sex					1 	Alphanumeric	Mandatory	�M'' or ''F''
				+ (FORMAT(ISNULL(emp.dInception, ''1899-12-31''), ''ddMMyyyy''))								--Start Date			8 	Alphanumeric	Mandatory	The enrolment date. It is important that the Third party send AXA PPP the actual enrolment dates on the initial load. Format id DDMMCCYY.
				+ CASE 
					WHEN emp.intstatus <> 2
					THEN (FORMAT(ISNULL(emp.dEffectiveDate, ''1899-12-31''), ''ddMMyyyy''))
					ELSE REPLICATE('' '', 8)
				END																							--End Date				8	Alphanumeric	Optional	Format id DDMMCCYY. 
				+ ''3''																						--Underwriting Method	1 	Alphanumeric	Mandatory	�3� indicating 3 year moratorium. 
				--============================================================
				--Personal Data (Beneficiaries. Up to 8 beneficiaries)
				--============================================================
				+ dep.dependents) AS data

			FROM Customer c WITH (NOLOCK)
			JOIN Policy p WITH (NOLOCK) ON p.iCustomerID = c.iCustomerID AND p.iPolicyStatus < 4
			JOIN policydetails pd WITH (NOLOCK) ON pd.sFieldCode = ''P_AXAGROUNUMB'' AND pd.iPolicyID = p.iPolicyID
			--JOIN Risks sme WITH (NOLOCK) ON sme.intpolicyid = p.iPolicyID AND sme.intRiskTypeID = 1005
			JOIN Risks emp WITH (NOLOCK) ON emp.intpolicyid = p.iPolicyID AND emp.intRiskTypeID = 1001 
				--AND (CAST(emp.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) OR CAST(emp.dDateModified AS DATE) = CAST(GETDATE() AS DATE))
			JOIN RiskDetails rd1 WITH (NOLOCK) ON rd1.iriskid = emp.intriskid and rd1.sFieldCode = ''R_SMELEVEL''
			LEFT JOIN Risks support WITH (NOLOCK) ON support.intPolicyID = p.iPolicyID AND support.intRiskTypeID = 1002
			LEFT JOIN Risks dentopt WITH (NOLOCK) ON dentopt.intPolicyID = p.iPolicyID AND dentopt.intRiskTypeID = 1003
			JOIN RiskDetails efn WITH (NOLOCK) ON efn.iriskid = emp.intriskid and efn.sFieldCode = ''R_FIRSTNAME''
			JOIN RiskDetails eln WITH (NOLOCK) ON eln.iriskid = emp.intriskid and eln.sFieldCode = ''R_LASTNAME''
			JOIN RiskDetails ett WITH (NOLOCK) ON ett.iriskid = emp.intriskid and ett.sFieldCode = ''R_INSTITLE''
			JOIN RiskDetails egd WITH (NOLOCK) ON egd.iriskid = emp.intriskid and egd.sFieldCode = ''R_G_GENDER''
			JOIN RiskDetails edb WITH (NOLOCK) ON edb.iriskid = emp.intriskid and edb.sFieldCode = ''R_DOB''
			LEFT JOIN RiskDetails eadr1 WITH (NOLOCK) ON eadr1.iriskid = emp.intriskid and eadr1.sFieldCode = ''R_ADDRLINE1''
			LEFT JOIN RiskDetails eadr2 WITH (NOLOCK) ON eadr2.iriskid = emp.intriskid and eadr2.sFieldCode = ''RatingArea''
			LEFT JOIN RiskDetails eadr3 WITH (NOLOCK) ON eadr3.iriskid = emp.intriskid and eadr3.sFieldCode = ''R_PROVINCE''
			LEFT JOIN RiskDetails eadr4 WITH (NOLOCK) ON eadr4.iriskid = emp.intriskid and eadr4.sFieldCode = ''ZIPCODE''
			JOIN RiskDetails axa WITH (NOLOCK) ON axa.iriskid = emp.intriskid and axa.sFieldCode = ''R_AXAPPPPOLNO''
			JOIN RiskDetails epn WITH (NOLOCK) ON epn.iriskid = emp.intriskid and epn.sFieldCode = ''R_EMPLOPOLINUMB''
			LEFT JOIN uw.ItemLink il WITH (NOLOCK) ON il.InwardID = emp.intRiskID
			LEFT JOIN cfg.ItemLinkType lt WITH (NOLOCK) ON lt.Name = ''EquipsmeEmployeeRisktoBuyupRisk'' AND il.LinkTypeID = lt.ID
			LEFT JOIN Risks buyup WITH (NOLOCK) ON buyup.intRiskID = il.OutwardID AND buyup.intRiskTypeID = 1004 AND buyup.intStatus = 2
			LEFT JOIN RiskDetails buyupLvl WITH (NOLOCK) ON buyupLvl.iRiskID = buyup.intRiskID AND buyupLvl.sFieldCode = ''R_OPTILEVBUYUP''
			LEFT JOIN (
				SELECT
					r.intRiskID,
					ISNULL((SELECT ''''
							+ CASE 
								WHEN ISNULL(axa.sFieldValue, '''') = '''' 
								THEN ''NB'' 
								WHEN CAST(ri.DateCreated AS DATE) = CAST(GETDATE() AS DATE)
								THEN ''NB''
								WHEN CAST(ri.DateLastUpdated AS DATE) = CAST(GETDATE() AS DATE)
								THEN ''UP''
							END																				--Type of Update		2 	Alphanumeric	Mandatory	�NB� = new business, �RN� = renewal, �UP� = update
							+ ''ESM''																			--3rd Party ID			3 	Alphanumeric	Mandatory	Identifies the 3rd party sourcing the data so ESM
							+ CASE 
								WHEN LEN(ISNULL(axa.sFieldValue, '''')) > 8 
								THEN LEFT(axa.sFieldValue, 8) 
								ELSE CONCAT(ISNULL(axa.sFieldValue, ''''),REPLICATE('' '', 8 - LEN(ISNULL(axa.sFieldValue, '''')))) 
							END																				--AXA PPP Policy No		8	Alphanumeric	Optional	PPP Registration (Policy) No. Will not be known (so blank) when Type of Update is �NB�
							+ CASE 
								WHEN LEN(ISNULL(epn.sFieldValue, '''')) > 16 
								THEN left(epn.sFieldValue, 16) 
								ELSE CONCAT(ISNULL(epn.sFieldValue, ''''),REPLICATE('' '', 16 - LEN(ISNULL(epn.sFieldValue, '''')))) 
							END																				--3rd Party Policy No	16 	Alphanumeric	Mandatory	Third Party Policy No
							+ CONVERT(VARCHAR(1), ((ROW_NUMBER() OVER (ORDER BY ip.ID)) + 1))				--Life Id				1	Alphanumeric	Mandatory	Identifies the person details on a Policy, needed when amendments to personal details are sent set to 1-9
							--+ CASE
							--	WHEN buyupLvl.sFieldValue IS NOT NULL
							--	THEN
							--		CASE
							--			WHEN ISNULL(buyupCT.sfieldvalue, '''') = ''1'' --Couple
							--			THEN 
							--				CASE 
							--					WHEN ISNULL(ip.Gender, '''') = ''0'' THEN ''H'' 
							--					ELSE ''W'' 
							--				END
							--			WHEN ISNULL(buyupCT.sfieldvalue, '''') = ''2'' --Couple + Family
							--			THEN 
							--				CASE 
							--					WHEN ISNULL(ip.Gender, '''') = ''0'' --Male
							--					THEN (
							--						CASE 
							--							WHEN CONVERT(INT,DATEDIFF(d, ip.DOB, GETDATE())/365.25) > 24 --Child gets to 25 years old, must be removed as child dependent
							--							THEN ''H'' --Husband
							--							ELSE ''S'' --Son
							--						END) 
							--					ELSE (	--Female
							--						CASE 
							--							WHEN CONVERT(INT,DATEDIFF(d, ip.DOB, GETDATE())/365.25) > 24 --Child gets to 25 years old, must be removed as child dependent
							--							THEN ''W'' --Wife
							--							ELSE ''D'' --Daughter
							--						END) 
							--					END
							--			WHEN ISNULL(buyupCT.sfieldvalue, '''') = ''4'' --Single + Family
							--			THEN CASE WHEN ISNULL(ip.Gender, '''') = ''0'' THEN ''S'' ELSE ''D'' END
							--		END
							--	ELSE '' ''
							--  END																				--Relationship			1 	Alphanumeric	Mandatory.	X = subscriber ; H = husband ; W = wife ; D = daughter ; S = son ; E = ex-subscriber, A = adult relation ; C = child relation
							+ CASE 
								WHEN ip.Relationship = 0 THEN ''H'' 
								WHEN ip.Relationship = 1 THEN ''W'' 
								WHEN ip.Relationship = 2 THEN ''S'' 
								WHEN ip.Relationship = 3 THEN ''D'' 
								WHEN ip.Relationship = 4 THEN ''A'' 
								WHEN ip.Relationship = 5 THEN ''C''
							END																					--Relationship			1 	Alphanumeric	Mandatory.	X = subscriber ; H = husband ; W = wife ; D = daughter ; S = son ; E = ex-subscriber, A = adult relation ; C = child relation
							+ CASE 
								WHEN ISNULL(axa.sFieldValue, '''') = '''' 
								THEN ''2'' 
								ELSE 
									CASE 
										WHEN rr.intStatus = 2 
										THEN ''2'' 
										ELSE ''1'' 
									END
								END																				--Person Status			1 	Alphanumeric	Mandatory	1 = Lapsed, 2 = Current
							+ CONCAT(ISNULL(ip.Title, ''''),REPLICATE('' '', 15 - LEN(ISNULL(ip.Title, ''''))))		--Title					15	Alphanumeric	Blank		Title  
							+ CASE 
								WHEN LEN(RTRIM(ip.FirstName)) > 12 
								THEN LEFT(RTRIM(ip.FirstName), 12) 
								ELSE CONCAT(RTRIM(ip.FirstName),REPLICATE('' '', 12 - LEN(RTRIM(ip.FirstName))))
							END																					--Forename				12	Alphanumeric	Mandatory	 
							+ CASE
								WHEN LEN(RTRIM(ip.LastName)) > 20 
								THEN LEFT(RTRIM(ip.LastName), 20) 
								ELSE CONCAT(RTRIM(ip.LastName),REPLICATE('' '', 20 - LEN(RTRIM(ip.LastName)))) 
							END																					--Surname				20	Alphanumeric	Mandatory	 
							+ (FORMAT(ip.DOB, ''ddMMyyyy''))														--Date of Birth			8 	Alphanumeric	Mandatory	 Format id DDMMCCYY.
							+ CASE
								WHEN ip.Gender = ''0'' 
								THEN ''M'' 
								ELSE ''F'' 
							END																					--Sex					1 	Alphanumeric	Mandatory	�M'' or ''F''
							+ (FORMAT(ri.EffectiveDate, ''ddMMyyyy''))											--Start Date			8 	Alphanumeric	Mandatory	The enrolment date. It is important that the Third party send AXA PPP the actual enrolment dates on the initial load. Format id DDMMCCYY.
							+ CASE 
								WHEN ISNULL(axa.sFieldValue, '''') = '''' 
								THEN CONCAT('''',REPLICATE('' '', 8)) 
								ELSE 
									CASE 
										WHEN rr.intStatus = 2 
										THEN CONCAT('''',REPLICATE('' '', 8)) 
										ELSE (FORMAT(ISNULL(rr.dEffectiveDate, ''1899-12-31''), ''ddMMyyyy'')) 
									END
							END																					--End Date				8	Alphanumeric	Optional	Format id DDMMCCYY. 
							+ ''3''																				--Underwriting Method	1 	Alphanumeric	Mandatory	�3� indicating 3 year moratorium. 
						FROM RiskItem ri WITH (NOLOCK)
						JOIN ItemPerson ip WITH (NOLOCK) ON ip.RiskItemId = ri.ID
						JOIN RiskDetails axa WITH (NOLOCK) ON axa.iriskid = r.intRiskID AND axa.sFieldCode = ''R_AXAPPPPOLNO''
						JOIN RiskDetails epn WITH (NOLOCK) ON epn.iriskid = r.intRiskID AND epn.sFieldCode = ''R_EMPLOPOLINUMB''
						JOIN Risks rr WITH (NOLOCK) ON rr.intriskid = r.intRiskID
						JOIN Policy pp WITH (NOLOCK) ON pp.iPolicyID = rr.intPolicyID AND pp.iPolicyStatus < 4
						LEFT JOIN uw.ItemLink il WITH (NOLOCK) ON il.InwardID = r.intRiskID
						LEFT JOIN cfg.ItemLinkType lt WITH (NOLOCK) ON lt.Name = ''EquipsmeEmployeeRisktoBuyupRisk'' AND il.LinkTypeID = lt.ID
						LEFT JOIN Risks buyup WITH (NOLOCK) ON buyup.intRiskID = il.OutwardID AND buyup.intRiskTypeID = 1004 AND buyup.intStatus = 2
						LEFT JOIN RiskDetails buyupLvl WITH (NOLOCK) ON buyupLvl.iRiskID = buyup.intRiskID AND buyupLvl.sFieldCode = ''R_OPTILEVBUYUP'' AND buyupLvl.sFieldValue IS NOT NULL
						LEFT JOIN RiskDetails buyupCT WITH (NOLOCK) ON buyupCT.iRiskID = buyup.intRiskID AND buyupCT.sFieldCode = ''R_OPTIDEPCOVBUY''
						WHERE ri.RiskId = r.intRiskID
							AND (CAST(ri.DateCreated AS DATE) = CAST(GETDATE() AS DATE) OR CAST(ri.DateLastUpdated AS DATE) = CAST(GETDATE() AS DATE))
						FOR XML PATH('''')
					), '''') dependents
				FROM Risks r WITH (NOLOCK)
				GROUP BY r.intRiskID
			) dep ON dep.intriskid = emp.intriskid
			WHERE 
			(
				--CAST(p.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) 
				--OR CAST(p.dDateModified AS DATE) = CAST(GETDATE() AS DATE)
				--OR 
				axa.sFieldValue IS NULL
				OR axa.sFieldValue = ''''
			)

			UNION
		'
		Set @SQLCommandFooter = 
			
				'--============================================================
				--Policy File Trailer
				--============================================================
				SELECT 
					(''2''
					+ (REPLICATE(''0'', 4) + ''4'')																--Extract Count
					+ (FORMAT(GETDATE(), ''ddMMyyyy''))
					+ REPLACE(CONVERT(char(5), GETDATE(), 108), '':'', '''')
					+ REPLICATE(''0'', 5 - LEN(@iCount)) + CONVERT(VARCHAR, @iCount)) AS data
				) r
				ORDER BY r.data
				'
			

exec (@SQLCommandHeader + @SQLCommandDetail + @SQLCommandFooter)
--print @SQLCommandHeader + @SQLCommandDetail + @SQLCommandFooter
--exec [SKI_INT].[AXA_ExportPolicy] 'TEST'