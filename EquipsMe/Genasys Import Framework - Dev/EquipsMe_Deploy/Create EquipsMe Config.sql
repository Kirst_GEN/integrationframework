/*===========================================================================================================================================================================================
--RUN ONCE ONLY WHEN IMPLEMENTED
CREATE TABLE [ski_int].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
GO
===========================================================================================================================================================================================*/


SET IDENTITY_INSERT [ski_int].[SSISFileType] ON 
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'.dat', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'.csv', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'.txt', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISFileType] OFF

INSERT [ski_int].[BatchUser] ([uID], [UserName], [Password]) VALUES (NULL, N'MonthEnd', 0x01000000A922098B7E7385AC2707DC05817E51C45BC837CE02A9D9EEBF6AD664EB6B7C71)
INSERT [ski_int].[SSISBatchUser] ([uID], [UserName], [Password]) VALUES (NULL, N'MonthEnd', 0x010000007EC0A02033FF482964AB3815051CA7BC88B5DE3038589334BBAE3FA24F26988F)

SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] ON 
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'ProcessFailure', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'ProcessErrors', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Progress', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Exceptions', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] OFF

--truncate table [ski_int].[SSISFileLocations]
SET IDENTITY_INSERT [ski_int].[SSISProcessType] ON 
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'Import', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'Export', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Update', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Raising', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'Receipting', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'DocumentMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ClaimMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (8, N'UnderwritingMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (9, N'CancelPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (10, N'ReinstatePolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (11, N'SuspendPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (12, N'CashImport', NULL, 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISProcessType] OFF

SET IDENTITY_INSERT [ski_int].[SSISScriptType] ON 
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'SQLCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.900' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'SKiImporterTable', 1, N'Integration', CAST(N'2017-09-28 15:30:25.907' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'ASQueryName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.910' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'FlashImporterTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.917' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'SKiImporterSPPName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.923' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'ExceptionsCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.927' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ExceptionsTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.930' AS DateTime))
SET IDENTITY_INSERT [ski_int].[SSISScriptType] OFF

--===========================================================================================================================================================================================
--END RUN ONCE ONLY WHEN IMPLEMENTED
--===========================================================================================================================================================================================


--===========================================================================================================================================================================================
--START CONFIG
--===========================================================================================================================================================================================
--truncate table [ski_int].[SSISProcess]
SET IDENTITY_INSERT [ski_int].[SSISProcess] ON 
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (1, N'1', N'Export AXA Group File', 2, 1, 1, 1, N'SKI_Automation', N'GenasysIntegrationFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (2, N'1', N'Export AXA Policy File', 2, 1, 2, 2, N'SKI_Automation', N'GenasysIntegrationFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (3, N'1', N'Import AXA Group Errors', 1, 1, 3, 3, N'SKI_Automation', N'GenasysIntegrationFramework', N'ImportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (4, N'1', N'Import AXA Policy Errors', 1, 1, 4, 3, N'SKI_Automation', N'GenasysIntegrationFramework', N'ImportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (5, N'1', N'Import AXA Policy Response', 1, 1, 5, 4, N'SKI_Automation', N'GenasysIntegrationFramework', N'ImportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (6, N'1', N'Export Thriva File', 2, 1, 6, 5, N'SKI_Automation', N'GenasysIntegrationFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (7, N'1', N'Export MedSol File', 2, 1, 7, 6, N'SKI_Automation', N'GenasysIntegrationFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (8, N'1', N'Export HealthAss File', 2, 1, 8, 7, N'SKI_Automation', N'GenasysIntegrationFramework', N'ExportMultiple', 1, N'Integration', GETDATE())

INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (9, N'1', N'Import SalesApp Policies', 1, 1, 0, 8, N'SKI_Automation', N'GenasysIntegrationFramework', N'NewBusinessSalesApp', 1, N'Integration', GETDATE())

SET IDENTITY_INSERT [ski_int].[SSISProcess] OFF

--truncate table [ski_int].[SSISScript]

SET IDENTITY_INSERT [ski_int].[SSISScript] ON 
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (1, 1, 1, N'EXEC [SKI_INT].[AXA_ExportGroup]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (2, 2, 1, N'[SKI_INT].[AXA_ExportPolicy]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (3, 3, 1, N'[SKI_INT].[AXA_ImportErrors]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (4, 4, 1, N'[SKI_INT].[AXA_SyncPolicyNumbers]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (5, 5, 1, N'[SKI_INT].[Thriva_Export]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (6, 6, 1, N'[SKI_INT].[MEDSOL_Export]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (7, 7, 1, N'[SKI_INT].[HealthAss_Export]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (8, 8, 1, N'[dbo].[Populate_Staging_From_SalesApp]', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISScript] OFF

--truncate table [ski_int].[SSISFileLocations]
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] ON 
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (1, N'1', N'Equipsme_AXA_Group_file', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\OUT\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (2, N'2', N'Equipsme_AXA_Policy_file', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\OUT\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (3, N'3', N'ESMGRPEX', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\IN\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (4, N'4', N'ESMPOLEX', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\IN\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (5, N'5', N'ESMPOLLD', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\IN\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (6, N'6', N'Equipsme_THRIVA_file', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\OUT\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (7, N'7', N'Equipsme_MedSol_file', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\OUT\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation], [UserLastUpdated], [DateLastUpdated]) 
VALUES (8, N'8', N'Equipsme_HealthAssist_file', 3, 1, 1, N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\OUT\', N'C:\Genasys Integration Framework\SFTP\Processed\', N'C:\Genasys Integration Framework\SFTP\AXA\UAT01\Archive\', N'C:\Genasys Integration Framework\SFTP\LogFiles\LogFile\', N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] OFF

--===========================================================================================================================================================================================
--END CONFIG
--===========================================================================================================================================================================================

--===========================================================================================================================================================================================
--CHECK CONFIG
--===========================================================================================================================================================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Export','Export AXA Group File','Equipsme_AXA_Group_file'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Export','Export AXA Policy File','Equipsme_AXA_Policy_file'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import AXA Group Errors','ESMGRPEX'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import AXA Policy Errors','ESMPOLEX'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import AXA Policy Response','ESMPOLLD'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Export','Export Thriva File','Equipsme_THRIVA_file'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Export','Export MedSol File','Equipsme_MedSol_file'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Export','Export HealthAss File','Equipsme_HealthAssist_file'
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import SalesApp Policies',''
