USE [SKi_Equipsme_UAT]
GO

/****** Object:  Table [dbo].[ski_int.AXASyncRecords]    Script Date: 2/21/2018 4:30:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--drop table  [ski_int].[AXASyncRecords]
CREATE TABLE [ski_int].[AXASyncRecords](
	[iID] [int] IDENTITY(1,1) NOT NULL,
	[AXAPolicyNo] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[ThirdPartyPolicyNo] [varchar](100) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
 CONSTRAINT [PK_ski_int.AXASyncRecords] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--drop table  [ski_int].[AXASyncRecords_Log]
CREATE TABLE [ski_int].[AXASyncRecords_Log](
	[iID] [int] NOT NULL,
	[AXAPolicyNo] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[ThirdPartyPolicyNo] [varchar](100) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL
) ON [PRIMARY]
GO