
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES A TABLE TABLE
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationCreateTable] 
(
		@TableName SYSNAME
)
AS

BEGIN

	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'CREATE TABLE ' + N'' + @TableName + '([iID] [INT] IDENTITY(1,1) NOT NULL, [sDataRecord] [VARCHAR](MAX) NULL, [dtDate] [DATETIME] NULL, [sFileName] [VARCHAR](250) NULL, [iProcessStatus] [INT] NULL)'

	EXEC(@SQLCommand);

END 
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES A TABLE TABLE
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationDropTable] 
(
		@TableName SYSNAME
)
AS

BEGIN

	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'DROP TABLE ' + N'' + @TableName

	EXEC(@SQLCommand);

END 
GO


