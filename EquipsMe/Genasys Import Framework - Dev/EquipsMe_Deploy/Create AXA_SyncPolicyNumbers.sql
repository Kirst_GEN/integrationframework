USE [SKi_Equipsme_UAT]
GO
/****** Object:  StoredProcedure [ski_int].[AXA_SyncPolicyNumbers]    Script Date: 2/22/2018 9:16:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [ski_int].[AXA_SyncPolicyNumbers]
		
	AS
	BEGIN TRY


	--=====================================================
	--CLEAR DATA TABLE FOR LAST DATA FILE
	--=====================================================
	TRUNCATE TABLE [ski_int].[AXASyncRecords];
	
	--=====================================================
	--INSERT DATA TABLE FOR NEW DATA FILE
	--=====================================================
	INSERT INTO 
		[ski_int].[AXASyncRecords]
		(
		 AXAPoliCyNo
		, SurName
		, ThirdPartyPolicyNo 
		, DateImported
		, FileName
		)
	SELECT 
		  REPLACE([ski_int].[UFN_SEPARATES_COLUMNS](LTRIM(RTRIM(SourceRecord)), 1, ','),' ','') AS AXAPolicyNo 
		, REPLACE([ski_int].[UFN_SEPARATES_COLUMNS](LTRIM(RTRIM(SourceRecord)), 2, ','),' ','') AS SurName
		, REPLACE([ski_int].[UFN_SEPARATES_COLUMNS](LTRIM(RTRIM(SourceRecord)), 3, ','),' ','') AS ThirdPartyPolicyNo  
		, getdate()
		, FileName			
	FROM 
		SKI_INT.SSIS_Src_RawData 
	WHERE 
		FileName Like 'ESMPOLLD%' 
		AND ISNUMERIC(LEFT(SourceRecord,1)) = 1 ORDER BY ID;

	--=====================================================
	--WRITE LOG DATA FOR HISTORICAL TRACKKING OF RECEIVED DATA
	--=====================================================
	INSERT INTO 
		[ski_int].[AXASyncRecords_Log]
		(
		  iID
		, AXAPoliCyNo
		, SurName
		, ThirdPartyPolicyNo 
		, DateImported
		, FileName
		)
	
	SELECT
		iID 
		, AXAPoliCyNo
		, SurName
		, ThirdPartyPolicyNo 
		, getdate()
		, FileName 
	FROM 
		[ski_int].[AXASyncRecords];

	SELECT 0;

END TRY
BEGIN CATCH
  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_PROCEDURE() + ' : ' + ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


