USE [SKi_Equipsme_UAT]
GO
/****** Object:  StoredProcedure [ski_int].[SSIS_FlashUpdatePolicyField]    Script Date: 2/22/2018 10:51:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--====================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES 
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--====================================================================================================
ALTER PROCEDURE [ski_int].[SSIS_FlashUpdateRiskField] 
(
		@BatchReference VARCHAR(100)
)
AS

BEGIN

	--==================================================
	--VARIABLES
	--==================================================
	DECLARE @EndorsementCode VARCHAR(15)
	DECLARE @JobId UNIQUEIDENTIFIER
	DECLARE @JobTaskId UNIQUEIDENTIFIER

	--==================================================
	--1. CREATE ENTRY FOR JOB TASK
	--==================================================
	EXEC batch.NewFieldUpdateJob @BatchReference, @JobId OUTPUT, @JobTaskId OUTPUT

	--==================================================
	--2. CREATE ENTRY IN JOB POLICY TASK STATUS
	--This Task is for the AXA received policy Number 
	--Different Step 2 Inserts must be created for each Field which needs to be updated from different sources
	--==================================================
	INSERT INTO 
		batch.JobPolicyTaskStatus 
		(
			Id
			,JobTaskStatusId
			,PolicyId
			,Status
		)
	SELECT
			cfg.NewCombGUID()
			,@JobTaskId
			,P.iPolicyID
			,0
	FROM 
		Policy P 
		INNER JOIN Risks R
			ON R.intPolicyID = P.iPolicyID
		INNER JOIN RiskDetails RD ON
			R.intRiskID = RD.iRiskID
			AND RD.sFieldCode = 'R_EMPLOPOLINUMB'
		INNER JOIN [ski_int].[AXASyncRecords] FS ON
			FS.ThirdPartyPolicyNo = P.sPolicyNo
		INNER JOIN RiskDetails RD_UPDATE ON
			R.intRiskID = RD_UPDATE.iRiskID
			AND RD_UPDATE.sFieldCode = 'R_AXAPPPPOLNO'
			AND FS.AXAPolicyNo <> RD_UPDATE.sFieldValue

	--==================================================
	--3. CREATE ENTRY IN FIELD UPDATE JOB VALUE
	--This Task is for the AXA received policy Number 
	--Different Step 2 Inserts must be created for each Field which needs to be updated from different sources
	--==================================================
	INSERT INTO 
		batch.FieldUpdateJobValue 
			(
			Id
			,JobTaskStatusId
			,ContextId
			,ContextType
			,FieldCode
			,sFieldValue
			,cFieldValue
			)
	SELECT 
		cfg.NewCombGUID()
		,@JobTaskId
		,RD.iRiskID
		,3
		,RD_UPDATE.sFieldCode
		,FS.AXAPolicyNo
		,NULL
	FROM Policy P 
		INNER JOIN Risks R
			ON R.intPolicyID = P.iPolicyID
		INNER JOIN RiskDetails RD ON
			R.intRiskID = RD.iRiskID
			AND RD.sFieldCode = 'R_EMPLOPOLINUMB'
		INNER JOIN [ski_int].[AXASyncRecords] FS ON
			FS.ThirdPartyPolicyNo = P.sPolicyNo
		INNER JOIN RiskDetails RD_UPDATE ON
			R.intRiskID = RD_UPDATE.iRiskID
			AND RD_UPDATE.sFieldCode = 'R_AXAPPPPOLNO'
			AND FS.AXAPolicyNo <> RD_UPDATE.sFieldValue

END 








