ALTER PROC [SKI_INT].[AXA_ExportGroup] (@TableName varchar(50))
AS
--========================================================================================================================
--*** GROUP LOAD FILE ***
--exec [SKI_INT].[AXA_ExportGroup] 'TEST'
--========================================================================================================================
--============================================================
--Group File Header
--============================================================
DECLARE @SQLCommand varchar(MAX)

SET @SQLCommand =
	(
	   'DECLARE 
			@iCount INT

		SET @iCount = 2
		SELECT
			@iCount = @iCount + COUNT(*)
		FROM Customer c WITH (NOLOCK)
		LEFT JOIN Address a WITH (NOLOCK) ON a.intAddressID = c.rPhysAddressID
		JOIN Policy p WITH (NOLOCK) ON p.iCustomerID = c.iCustomerID AND p.iPolicyStatus < 2
		JOIN policydetails pd WITH (NOLOCK) ON pd.sFieldCode = ''P_AXAGROUNUMB'' AND pd.iPolicyID = p.iPolicyID
		JOIN Risks r WITH (NOLOCK) ON r.intpolicyid = p.iPolicyID AND r.intRiskTypeID = 1005
		--WHERE 
		--(
		--	CAST(p.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) 
		--	OR CAST(p.dDateModified AS DATE) = CAST(GETDATE() AS DATE)
		--	OR CAST(c.dDateCreated AS DATE) = CAST(GETDATE() AS DATE)
		--	OR CAST(c.dLastUpdated AS DATE) = CAST(GETDATE() AS DATE)
		--)

		INSERT INTO ' + @TableName + '
		SELECT 
			''0''
			+ (REPLICATE(''0'', 4) + ''4'')													--Extract Count
			+ (FORMAT(GETDATE(), ''ddMMyyyy''))
			+ REPLACE(CONVERT(char(5), GETDATE(), 108), '':'', '''')

		UNION ALL

		--============================================================
		--Group File Detail
		--============================================================
		SELECT 
			--a.*, 
			''1''																			--Record ID				1	Alphanumeric	Mandatory	Must be 1
			+ CASE 
				WHEN (FORMAT(p.dDateCreated, ''ddMMyyyy'') = FORMAT(GETDATE(), ''ddMMyyyy'') Or FORMAT(c.dDateCreated, ''ddMMyyyy'') = FORMAT(GETDATE(), ''ddMMyyyy'')) 
				THEN ''NB'' 
				ELSE ''UP'' 
			END																			--Type of Update		2 	Alphanumeric	Mandatory	NB� = new business, �UP� =update 
			+ CONCAT(ISNULL(pd.sFieldValue, ''''),REPLICATE('' '', 5 - LEN(ISNULL(pd.sFieldValue, ''''))))		--AXA PPP Group No		5	Alphanumeric	Mandatory	(CREST Group number allocated for the Third party)
			+ (CONCAT('''',REPLICATE('' '', 6)))											--3rd Party Group No	6	Alphanumeric	Mandatory	Third party�s Group No
			+ CASE 
				WHEN LEN(c.sCompanyName) > 36 
				THEN LEFT(c.sCompanyName, 36) 
				ELSE CONCAT(c.sCompanyName,REPLICATE('' '', 36 - LEN(c.sCompanyName))) 
			  END																		--Group Name			36	Alphanumeric	Mandatory	Third party Group Name
			+ ''S''																		--Group Type			1	Alphabetic		Mandatory	Will always be �S� = SME 
			+ ''M''																		--Payment Frequency		1	Alphabetic		Mandatory	�A� = annual, �Q� = quarterly, M = �monthly�. Will always be �M�.
			+ '' ''																		--Payment Method		1	Alphanumeric	Blank		Blank,  CREST will set up dummy values 
			+ '' ''																		--Group Status			1	Numeric			Blank		CREST will set status as �frozen� Group 
			+ (FORMAT(p.dRenewalDate, ''ddMM''))											--Group''s Renewal Date	4	Alphanumeric	Mandatory	This must be of the format "ddmm". i.e. �0104�
			+ CONCAT('''',REPLICATE('' '', 5))												--Agent Reference No	5	Alphanumeric	Blank		Not applicable, no commission paid by AXA PPP
			+ CASE 
				WHEN LEN(RTRIM(c.sContactName)) > 30 
				THEN LEFT(RTRIM(c.sContactName), 30) 
				ELSE CONCAT(RTRIM(c.sContactName),REPLICATE('' '', 30 - LEN(RTRIM(c.sContactName)))) 
			END																			--Address Line1			30	Alphanumeric	Mandatory	Group contact name (SKi Key Contact)
			+ CASE 
				WHEN LEN(RTRIM(a.strAddressLine1)) > 36 
				THEN LEFT(RTRIM(a.strAddressLine1), 36) 
				ELSE CONCAT(RTRIM(a.strAddressLine1),REPLICATE('' '', 36 - LEN(RTRIM(a.strAddressLine1)))) 
			END																			--Address Line2			36	Alphanumeric	Mandatory	Group address (SKi Address Line 1)
			+ CASE 
				WHEN LEN(RTRIM(a.strAddressLine2)) > 36 
				THEN LEFT(RTRIM(a.strAddressLine2), 36) 
				ELSE CONCAT(RTRIM(a.strAddressLine2),REPLICATE('' '', 36 - LEN(RTRIM(a.strAddressLine2)))) 
			END																			--Address Line3			36	Alphanumeric	Optional	Group address
			+ CASE 
				WHEN LEN(RTRIM(a.strSuburb)) > 36 
				THEN LEFT(RTRIM(a.strSuburb), 36) 
				ELSE CONCAT(RTRIM(a.strSuburb),REPLICATE('' '', 36 - LEN(RTRIM(a.strSuburb)))) 
			END																			--Address Line4			36	Alphanumeric	Optional	Group address (SKi Suburb)
			+ CASE 
				WHEN LEN(RTRIM(a.strPostalCode)) > 8 
				THEN LEFT(RTRIM(a.strPostalCode), 8) 
				ELSE CONCAT(a.strPostalCode,REPLICATE('' '', 8 - LEN(a.strPostalCode))) 
			END																			--Post Code				8	Alphanumeric	Mandatory	Post code of Group (SKi Postal Code)
			+ CONCAT('''',REPLICATE('' '', 12))												--Telephone Number		12	Alphanumeric	Blank		Not applicable
			+ '' ''																		--LCD Marker			1	Alphanumeric	Blank		Low Claims Discount not applicable for this arrangement
			+ CASE 
				WHEN p.iPaymentStatus = 0 
				THEN ''N'' 
				ELSE ''Y'' 
			END																			--Non Payment Flag		1	Alphanumeric	Mandatory	�Y� = yes, Group has not paid or  �N�
		FROM Customer c WITH (NOLOCK)
		LEFT JOIN Address a WITH (NOLOCK) ON a.intAddressID = c.rPhysAddressID
		JOIN Policy p WITH (NOLOCK) ON p.iCustomerID = c.iCustomerID AND p.iPolicyStatus < 2
		JOIN policydetails pd WITH (NOLOCK) ON pd.sFieldCode = ''P_AXAGROUNUMB'' AND pd.iPolicyID = p.iPolicyID
		JOIN Risks r WITH (NOLOCK) ON r.intpolicyid = p.iPolicyID AND r.intRiskTypeID = 1005
		--WHERE 
		--(
		--	CAST(p.dDateCreated AS DATE) = CAST(GETDATE() AS DATE) 
		--	OR CAST(p.dDateModified AS DATE) = CAST(GETDATE() AS DATE)
		--	OR CAST(c.dDateCreated AS DATE) = CAST(GETDATE() AS DATE)
		--	OR CAST(c.dLastUpdated AS DATE) = CAST(GETDATE() AS DATE)
		--)

		UNION ALL

		--============================================================
		--Group File Trailer
		--============================================================
		SELECT 
			''2''
			+ (REPLICATE(''0'', 4) + ''4'')													--Extract Count
			+ (FORMAT(GETDATE(), ''ddMMyyyy''))
			+ REPLACE(CONVERT(char(5), GETDATE(), 108), '':'', '''')
			+ REPLICATE(''0'', 5 - LEN(@iCount)) + CONVERT(VARCHAR, @iCount)
		'
	)

	Print @SQLCommand