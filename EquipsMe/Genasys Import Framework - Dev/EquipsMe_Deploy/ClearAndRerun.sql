exec Equipsme_Website_UAT.dbo.Populate_Staging_From_SalesApp

select PD_QuoteID, PD_ParentQuoteID,CD_RegistrationNumber, PH_PolicyNumber,* 
from ski_int.ESME_Staging_CustomerPolicy 
where CD_RegistrationNumber = '09296740' 

select RH_QuoteID, RH_ParentQuoteID,BP_EmpPack_Employee_Total_Premium, SME_EmpPack_Employee_Ref_Number, SME_EmpPack_Employee_Policy_Number, * 
from ski_int.ESME_Staging_Risk 
where CH_RegistrationNumber = '09296740' 

select RH_QuoteID, RH_ParentQuoteID,CH_RegistrationNumber,SME_EmpPack_Employee_Ref_Number,Dental_AXA_Margin_IPT,SME_EmpPack_AXA_Margin_IPT,Dental_AXA_Margin, MC_2_Premium, * 
from ski_int.ESME_Staging_BuyUp_Risk 
where CH_RegistrationNumber = '09296740'

select * from Policy where spolicyno = 'EQ000036000'
select * from Policy where spolicyno = 'EQ000036004'
select * from PolicyDetails where iPolicyID = 619
select * from PolicyDetails where iPolicyID = 620
select * from Risks where intPolicyID = 619
select * from Risks where intPolicyID = 620

select * from RiskDetails where iRiskID = 3915
select * from RiskDetails where iRiskID = 3862

select * from PolicyDetails where sFieldCode = 'P_QuoteID'
and sFieldValue = 615

select * from ski_int.ESME_Staging_Member where PH_PolicyNumber = 'EQ000033000'


delete from ski_int.ESME_Staging_BuyUp_Risk --where PH_PolicyNumber <> 'EQ000026000'
delete from ski_int.ESME_Staging_Risk --where PH_PolicyNumber <> 'EQ000026000'
delete from ski_int.ESME_Staging_Member-- where PH_PolicyNumber <> 'EQ000026000'
delete from ski_int.ESME_Staging_CustomerPolicy-- where PH_PolicyNumber <> 'EQ000026000'

truncate table importrecord

update CustomerDetails set sFieldValue = '' where sFieldCode = 'C_COMPANNUMBER'
update policy set spolicyno = replace(spolicyno,'EQ','Q') where len(sPolicyNo) > 10
Update RiskDetails set sFieldValue = '' where sFieldCode = 'R_EMPLOPOLINUMB' 
Update RiskDetails set sFieldValue = '' where sFieldCode = 'R_EMPLOREFENUMB'



select iID,* from [ski_int].[ESME_Staging_Risk] where iimportrecno = -1 and convert(money,BP_EmpPack_Employee_Total_Premium) <> 0 order by [ski_int].[ESME_Staging_Risk].iID desc

update [ski_int].[ESME_Staging_CustomerPolicy] set iimportrecno = -1
update [ski_int].[ESME_Staging_Risk] set iimportrecno = -1 where convert(money,BP_EmpPack_Employee_Total_Premium) <> 0
update [ski_int].ESME_Staging_BuyUp_Risk set iimportrecno = -1 where convert(money,BP_EmpPack_Employee_Total_Premium) <> 0
update ski_int.ESME_Staging_Member set iimportrecno = -1

select * from policy where sPolicyNo = 'EQ000043000'
select * from PolicyDetails where iPolicyID = 624
select * from risks where intPolicyID = 624
select * from PolicyDetails where sfieldvalue = 615

select * from Equipsme_Website_UAT.dbo.Quote where Reference like 'EQ000036%'
update Equipsme_Website_UAT.dbo.Quote
set ETLStatus = 0
where id in (320)

update Equipsme_Website_UAT.dbo.Quote
set ETLStatus = 0
where ParentQuoteId in (320)

SELECT q.*
FROM
    Equipsme_Website_UAT.dbo.Quote q
    JOIN Equipsme_Website_UAT.dbo.QuoteStatusHistory qsh ON q.Id = qsh.QuoteId
    JOIN (
        SELECT QuoteId, MAX(Id) Id
        FROM Equipsme_Website_UAT.dbo.QuoteStatusHistory
        GROUP BY QuoteId
        ) qshl ON qsh.QuoteId = qshl.QuoteId AND qsh.Id = qshl.Id 
    WHERE 
        qsh.[Status] = 3
        AND q.ETLStatus = 0
		and InitialQuoteDate > '2018-02-19 10:44:07.6002079'
		order by q.id desc

		select * from QuoteDetail order by CoverStart
		update QuoteDetail set CoverStart = '19 Feb 2018' where CoverStart = '0001-01-01 00:00:00.0000000'