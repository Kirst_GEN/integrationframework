CREATE TABLE [Address] (
    [Id] int NOT NULL IDENTITY,
    [AddressLine1] nvarchar(max) NULL,
    [AddressLine2] nvarchar(max) NULL,
    [AddressLine3] nvarchar(max) NULL,
    [ETLStatus] bit NOT NULL,
    [PostalCode] nvarchar(max) NULL,
    [Suburb] nvarchar(max) NULL,
    [Town] nvarchar(max) NULL,
    [Type] int NOT NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoles] (
    [Id] nvarchar(450) NOT NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] nvarchar(450) NOT NULL,
    [AccessFailedCount] int NOT NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [Email] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [LockoutEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [PasswordHash] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [UserName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Cover] (
    [Id] int NOT NULL IDENTITY,
    [DependantCount] int NOT NULL,
    [ETLStatus] bit NOT NULL,
    [Type] int NOT NULL,
    CONSTRAINT [PK_Cover] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [User] (
    [Id] int NOT NULL IDENTITY,
    [ETLStatus] bit NOT NULL,
    [SKiUserId] int NOT NULL,
    [Status] int NOT NULL,
    [UserName] nvarchar(max) NULL,
    [UserType] int NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoleClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] nvarchar(450) NOT NULL,
    [ProviderKey] nvarchar(450) NOT NULL,
    [ProviderDisplayName] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
    CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserRoles] (
    [UserId] nvarchar(450) NOT NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserTokens] (
    [UserId] nvarchar(450) NOT NULL,
    [LoginProvider] nvarchar(450) NOT NULL,
    [Name] nvarchar(450) NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
    CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Person] (
    [Id] int NOT NULL IDENTITY,
    [AddressId] int NOT NULL,
    [ContactNumber] nvarchar(max) NULL,
    [DateOfBirth] datetime2 NOT NULL,
    [ETLStatus] bit NOT NULL,
    [EmailAddress] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [Gender] int NOT NULL,
    [Initials] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [Position] int NOT NULL,
    [Title] int NOT NULL,
    [Type] int NOT NULL,
    [UserId] int NOT NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Person_Address_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [Address] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Person_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [QuoteStatusHistory] (
    [Id] int NOT NULL IDENTITY,
    [DateUpdated] datetime2 NOT NULL,
    [ETLStatus] bit NOT NULL,
    [Status] int NOT NULL,
    [UserUpdatedId] int NOT NULL,
    CONSTRAINT [PK_QuoteStatusHistory] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_QuoteStatusHistory_User_UserUpdatedId] FOREIGN KEY ([UserUpdatedId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Company] (
    [Id] int NOT NULL IDENTITY,
    [AddressId] int NOT NULL,
    [ETLStatus] bit NOT NULL,
    [HrPersonId] int NOT NULL,
    [KeyContactPersonId] int NOT NULL,
    [MethodOfCommunication] int NOT NULL,
    [MethodOfCommunicationShort] int NOT NULL,
    [RegistrationNumber] nvarchar(max) NOT NULL,
    [SICCode] int NOT NULL,
    [SKiCustomerId] int NOT NULL,
    [Type] int NOT NULL,
    [UserId] int NOT NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Company_Address_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [Address] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Company_Person_HrPersonId] FOREIGN KEY ([HrPersonId]) REFERENCES [Person] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Company_Person_KeyContactPersonId] FOREIGN KEY ([KeyContactPersonId]) REFERENCES [Person] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Company_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Member] (
    [Id] int NOT NULL IDENTITY,
    [CoverId] int NOT NULL,
    [PersonId] int NOT NULL,
    CONSTRAINT [PK_Member] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Member_Person_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [Person] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Quote] (
    [Id] int NOT NULL IDENTITY,
    [AcceptedDate] datetime2 NOT NULL,
    [AcceptedUserId] int NOT NULL,
    [BankingCustomerId] nvarchar(max) NULL,
    [BankingDetailsId] nvarchar(max) NULL,
    [BankingMandateId] nvarchar(max) NULL,
    [CompanyId] int NOT NULL,
    [ETLStatus] bit NOT NULL,
    [InitialQuoteDate] datetime2 NOT NULL,
    [InitialUserId] int NOT NULL,
    [MemberId] int NOT NULL,
    [QuoteStatusId] int NOT NULL,
    [QuoteType] int NOT NULL,
    [Reference] nvarchar(max) NULL,
    [SKiPolicyId] int NOT NULL,
    [UpdateDate] datetime2 NOT NULL,
    [UpdatedUserId] int NOT NULL,
    CONSTRAINT [PK_Quote] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Quote_User_AcceptedUserId] FOREIGN KEY ([AcceptedUserId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Quote_Company_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [Company] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Quote_User_InitialUserId] FOREIGN KEY ([InitialUserId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Quote_Member_MemberId] FOREIGN KEY ([MemberId]) REFERENCES [Member] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Quote_QuoteStatusHistory_QuoteStatusId] FOREIGN KEY ([QuoteStatusId]) REFERENCES [QuoteStatusHistory] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Quote_User_UpdatedUserId] FOREIGN KEY ([UpdatedUserId]) REFERENCES [User] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Dependent] (
    [Id] int NOT NULL IDENTITY,
    [DateAdded] datetime2 NOT NULL,
    [ETLStatus] bit NOT NULL,
    [MemberId] int NULL,
    [PersonId] int NOT NULL,
    [QuoteId] int NOT NULL,
    [Status] int NOT NULL,
    CONSTRAINT [PK_Dependent] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Dependent_Member_MemberId] FOREIGN KEY ([MemberId]) REFERENCES [Member] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Dependent_Person_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [Person] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Dependent_Quote_QuoteId] FOREIGN KEY ([QuoteId]) REFERENCES [Quote] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [QuoteDetail] (
    [Id] int NOT NULL IDENTITY,
    [CoverId] int NOT NULL,
    [CoverStart] datetime2 NOT NULL,
    [DentalOption] bit NOT NULL,
    [ETLStatus] bit NOT NULL,
    [EmployeeCount] int NOT NULL,
    [Lev1Count] int NOT NULL,
    [Lev2count] int NOT NULL,
    [Lev3Count] int NOT NULL,
    [Lev4Count] int NOT NULL,
    [QuoteId] int NOT NULL,
    [SupportOption] bit NOT NULL,
    CONSTRAINT [PK_QuoteDetail] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_QuoteDetail_Cover_CoverId] FOREIGN KEY ([CoverId]) REFERENCES [Cover] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_QuoteDetail_Quote_QuoteId] FOREIGN KEY ([QuoteId]) REFERENCES [Quote] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);

GO

CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;

GO

CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);

GO

CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);

GO

CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;

GO

CREATE INDEX [IX_Company_AddressId] ON [Company] ([AddressId]);

GO

CREATE INDEX [IX_Company_HrPersonId] ON [Company] ([HrPersonId]);

GO

CREATE INDEX [IX_Company_KeyContactPersonId] ON [Company] ([KeyContactPersonId]);

GO

CREATE INDEX [IX_Company_UserId] ON [Company] ([UserId]);

GO

CREATE INDEX [IX_Dependent_MemberId] ON [Dependent] ([MemberId]);

GO

CREATE INDEX [IX_Dependent_PersonId] ON [Dependent] ([PersonId]);

GO

CREATE INDEX [IX_Dependent_QuoteId] ON [Dependent] ([QuoteId]);

GO

CREATE INDEX [IX_Member_PersonId] ON [Member] ([PersonId]);

GO

CREATE INDEX [IX_Person_AddressId] ON [Person] ([AddressId]);

GO

CREATE INDEX [IX_Person_UserId] ON [Person] ([UserId]);

GO

CREATE INDEX [IX_Quote_AcceptedUserId] ON [Quote] ([AcceptedUserId]);

GO

CREATE INDEX [IX_Quote_CompanyId] ON [Quote] ([CompanyId]);

GO

CREATE INDEX [IX_Quote_InitialUserId] ON [Quote] ([InitialUserId]);

GO

CREATE INDEX [IX_Quote_MemberId] ON [Quote] ([MemberId]);

GO

CREATE INDEX [IX_Quote_QuoteStatusId] ON [Quote] ([QuoteStatusId]);

GO

CREATE INDEX [IX_Quote_UpdatedUserId] ON [Quote] ([UpdatedUserId]);

GO

CREATE INDEX [IX_QuoteDetail_CoverId] ON [QuoteDetail] ([CoverId]);

GO

CREATE INDEX [IX_QuoteDetail_QuoteId] ON [QuoteDetail] ([QuoteId]);

GO

CREATE INDEX [IX_QuoteStatusHistory_UserUpdatedId] ON [QuoteStatusHistory] ([UserUpdatedId]);

GO