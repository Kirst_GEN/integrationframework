select	'2' 
        +left(dbo.StringPadRight((c.sFirstName + ' ' + c.sLastName),20,' '),20)
        +dbo.StringPadRight(p.sPolicyNo,20,' ')
        +CASE
			when pr.sInsurer = 'SBABGH' then dbo.StringPadRight((p.sAccountNo),16,' ')
		  END
        + CASE --iCurrency(1) = USD, iCurrency(13) = GH
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11706 then IIF(P.iCurrency = 1,dbo.StringPadRight('8045104007100',16,' '),dbo.StringPadRight('8045104007000',16,' ')) --HLP 
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11701 then IIF(P.iCurrency = 1,dbo.StringPadRight('8045104014100',16,' '),dbo.StringPadRight('8045104014000',16,' ')) --VAF 
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11708 then IIF(P.iCurrency = 1,dbo.StringPadRight('8045104009100',16,' '),dbo.StringPadRight('8045104009000',16,' ')) --HOC 
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11700 then dbo.StringPadRight('8045104002000',16,' ') -- Funeral
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11703 then dbo.StringPadRight('8045104005000',16,' ') -- PLP
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11707 then dbo.StringPadRight('8045104008000',16,' ') -- STANSURE
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11704 then dbo.StringPadRight('8045104006000',16,' ') -- BLP
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11714 then dbo.StringPadRight('8045104004000',16,' ') -- Group Life
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11715 then dbo.StringPadRight('8045104003000',16,' ') -- EDU Plan
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11721 then dbo.StringPadRight('8045104009000',16,' ') -- Commercial Insurance
			when pr.sInsurer = 'SBABGH' and pr.iProductID = 11709 then dbo.StringPadRight('8045104009000',16,' ') -- Stock Cover
		  END
        --+CASE WHEN min(pt.sReference) like '%_ME_%' 
        --   THEN convert(char(8),min(pt.dTransaction),112)
        --     ELSE convert(char(8),GetDate(),112)
        --         END
		+ convert(char(8),GetDate(),112)
        +dbo.StringPadLeft('0' + cast(Round((sum(pt.cPremium+pt.cAdminFee+pt.cBrokerFee+pt.cPolicyFee)*-1),4) as money),18,'0')+'00'
        +dbo.StringPadRight(replace(p.sBranchCode,'-',''),8,' ') --Branch SOLID
        +CASE 
			when pr.sInsurer = 'SBABGH' then dbo.StringPadLeft(isnull(nullif(rtrim(ltrim(pd.sFieldValue)),''),'1'),10,'0')  
		 END --Serial No
		 + '0000000000000001'
        	+'  ' --Reason Code
        	+dbo.StringPadRight(ISNULL(cd.sFieldValue,''),50,' ') as 'sLine',GetDate(),'Integration' ,0			
			
			from customer c with (nolock)
				inner join Policy p with (nolock) on p.iCustomerID = c.iCustomerId
				left join PolicyDetails pd with (nolock) on pd.ipolicyid = p.ipolicyid and pd.sfieldcode = 'P_SERIALNUMBER'
				inner join Products pr with (nolock) on pr.iProductid = p.iProductid and pr.sProductName like 'GH%' 
				inner join PolicyTransactions pt with (nolock) on pt.iPolicyId = p.iPolicyID 
				inner join PolicyCommStruct pcs with (nolock) on pcs.iPolicyID = p.iPolicyID and pcs.iVersionNo = pt.iAgencyId
				inner join CommEntities ce with (nolock) on ce.iCommEntityID = pcs.iLev2EntityID
				left join CustomerDetails cd with (nolock) on cd.iCustomerID = c.iCustomerID and cd.sFieldcode = 'C_CUSTOMNUMBER'
			where (pt.iTrantype = 2)
				and (left(pt.sReference,len(pt.sReference)-2) in (SELECT sReference FROM SBAB_src.StandingOrderReference WHERE bActiveIndicator = 1 and sCountry = 'GH' and (dtDateProcessed IS NULL OR CONVERT(DATE,dtDateProcessed) < CONVERT(DATE,GETDATE()) and dtExpiryDate >= CONVERT(DATE,GETDATE()))/*and sCountry = @CountryVar "add this in when it needs to be per country"*/))
				and exists (select null from [SBAB_src].[PaidUnpaids] scrunp (nolock) where scrunp.sPolicy_Number = p.sPolicyNo and sStatus_Flag = 'U' 
				and scrunp.dtPay_Date = convert(date,(getdate() - 3)) ) 
			group by c.sFirstName,c.sLastName,p.sPolicyNo, p.sAccountNo,p.dNextPaymentDate,p.sBranchCode,p.iProductId,cd.sFieldValue,ce.sCode,  pt.dTransaction, pr.sInsurer,pr.sProductType ,pd.sFieldValue,pr.iProductID,p.iCurrency