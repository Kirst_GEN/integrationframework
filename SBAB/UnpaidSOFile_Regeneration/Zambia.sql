 SELECT	'2' 
			+left(dbo.StringPadRight((c.sFirstName + ' ' + c.sLastName),20,' '),20)
			+dbo.StringPadRight(p.sPolicyNo,20,' ')
			+CASE
				when pr.sInsurer = 'SBABKE' then dbo.StringPadRight(('0' + p.sAccountNo),16,' ')
				when pr.sInsurer = 'SBABSW' then dbo.StringPadRight((p.sAccountNo),16,' ')
				when pr.sInsurer = 'SBABZW' then dbo.StringPadRight((p.sAccountNo),16,' ')
				when pr.sInsurer = 'SBABZM' then dbo.StringPadRight((p.sAccountNo),16,' ')
			  END
			+ CASE
				when pr.sInsurer = 'SBABKE' then dbo.StringPadRight('KES1757200010001',16,' ') --LIVE KES1100100990001
				when pr.sInsurer = 'SBABSW' and p.iProductID = 11100  then dbo.StringPadRight('111000SZL5104002',16,' ')
				when pr.sInsurer = 'SBABSW' and p.iProductID <> 11100 and pr.sProductType = 'DOM' then dbo.StringPadRight('8115104015000',16,' ')
				when pr.sInsurer = 'SBABSW' and p.iProductID <> 11100 and pr.sProductType = 'COMM' then dbo.StringPadRight('8115104016000',16,' ')
				when pr.sInsurer = 'SBABZW' and p.iProductID = 11400  then dbo.StringPadRight('141000USD5104002',16,' ')
				when pr.sInsurer = 'SBABZM' and p.iProductID = 11500 then dbo.StringPadRight('131000ZMW5104002',16,' ')
				when pr.sInsurer = 'SBABZM' and p.iProductID = 11505 and p.iCurrency = 12  then dbo.StringPadRight('131000ZMW5104008',16,' ')
				when pr.sInsurer = 'SBABZM' and p.iProductID = 11505 and p.iCurrency = 1  then dbo.StringPadRight('131000USD5104008',16,' ')
			  END
			+CASE WHEN pt.sReference like '%_ME_%' 
			   THEN convert(char(8),pt.dTransaction,112)
				 ELSE convert(char(8),GetDate(),112)
					 END
			+dbo.StringPadLeft('0' + cast(Round((sum(pt.cPremium+pt.cAdminFee+pt.cBrokerFee+pt.cPolicyFee)*-1),4) as money),18,'0')+'00'
			+dbo.StringPadRight(replace(p.sBranchCode,'-',''),8,' ') --Branch SOLID
			+CASE 
				when pr.sInsurer = 'SBABKE' then dbo.StringPadRight(substring(pt.sReference,8,7)+substring(pt.sReference,19,3),10,' ')
				when pr.sInsurer = 'SBABSW' then dbo.StringPadLeft(isnull(nullif(rtrim(ltrim(pd.sFieldValue)),''),'1'),10,'0')  
				when pr.sInsurer = 'SBABZW' then dbo.StringPadLeft(isnull(nullif(rtrim(ltrim(pd.sFieldValue)),''),'1'),10,'0')  
				when pr.sInsurer = 'SBABZM' then dbo.StringPadLeft(isnull(nullif(rtrim(ltrim(pd.sFieldValue)),''),'1'),10,'0')  
			 END --Serial No
			+ CASE
						--Keya 
        				when p.iProductId =    31 then '000000000000COMG'
        				when p.iProductId = 11001 then '00000000000000MC'
        				when p.iProductId = 11005 then '0000000000000VLP'
        				when p.iProductId = 11007 then '000000000000STNP'
        				when p.iProductId = 11009 then '0000000000000MAR'
        				when p.iProductId = 11010 then '000000000000SMES'
        				when p.iProductId = 11014 then '00000000000000EB'
        				when p.iProductId = 11015 then '0000000000000ILP'
        				when p.iProductId = 11016 then '000000000000SMEL'
						when p.iProductId = 11017 then '000000000000STNB'
        				when p.iProductId = 11019 then '0000000000000HOC'
        				when p.iProductId = 11020 then '00000000000000SP'
						--Swaziland
						when p.iProductId = 11100 then '0000000000000001'
        		else '0000000000000001'
        		END
        		+'  ' --Reason Code
        		+dbo.StringPadRight(ISNULL(cd.sFieldValue,''),50,' ') as 'sLine',GetDate(),'Integration' ,0			
					
	from customer c with (nolock)
		inner join Policy p with (nolock) on p.iCustomerID = c.iCustomerId
		left join PolicyDetails pd with (nolock) on pd.ipolicyid = p.ipolicyid and pd.sfieldcode = 'P_SERIALNUMBER'
		inner join Products pr with (nolock) on pr.iProductid = p.iProductid and pr.sProductName like 'ZAM%'
		inner join PolicyTransactions pt with (nolock) on pt.iPolicyId = p.iPolicyID 
		inner join PolicyCommStruct pcs with (nolock) on pcs.iPolicyID = p.iPolicyID and pcs.iVersionNo = pt.iAgencyId
		inner join CommEntities ce with (nolock) on ce.iCommEntityID = pcs.iLev2EntityID
		left join CustomerDetails cd with (nolock) on cd.iCustomerID = c.iCustomerID and cd.sFieldcode = 'C_CUSTOMNUMBER'
	where (pt.iTrantype = 2)
		and (left(pt.sReference,len(pt.sReference)-2) in (SELECT sReference FROM SBAB_src.StandingOrderReference WHERE bActiveIndicator = 1 and sCountry = 'ZM' and (dtDateProcessed IS NULL OR CONVERT(DATE,dtDateProcessed) < CONVERT(DATE,GETDATE()) and dtExpiryDate >= CONVERT(DATE,GETDATE()))/*and sCountry = @CountryVar "add this in when it needs to be per country"*/))
		and exists (select null from [SBAB_src].[PaidUnpaids] scrunp (nolock) where scrunp.sPolicy_Number = p.sPolicyNo and sStatus_Flag = 'U' and sCountry = 'ZM' 
		and scrunp.dtPay_Date = convert(date,(getdate() - 3)) )
	group by c.sFirstName,c.sLastName,p.sPolicyNo, p.sAccountNo,p.dNextPaymentDate,p.sBranchCode,p.iProductId,cd.sFieldValue,ce.sCode, substring(pt.sReference,8,7)+substring(pt.sReference,19,3), pt.dTransaction, pt.sReference,pr.sInsurer,pr.sProductType ,pd.sFieldValue,p.iCurrency



