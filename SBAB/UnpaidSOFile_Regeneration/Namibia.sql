select	'2' 
        +left(dbo.StringPadRight((c.sFirstName + ' ' + c.sLastName),20,' '),20)
        +dbo.StringPadRight(p.sPolicyNo,20,' ')
        +CASE
			when pr.sInsurer = 'SBABNA' then dbo.StringPadRight((p.sAccountNo),16,' ')
		  END
        + CASE
			when pr.sInsurer = 'SBABNA' and pr.iProductID in (11200, 11213, 11232, 11214, 11205, 11231, 11230)  then dbo.StringPadRight('043259510',16,' ')
			when pr.sInsurer = 'SBABNA' and pr.iProductID = 11203 then dbo.StringPadRight('082372NAD5104005',16,' ')
			when pr.sInsurer = 'SBABNA' and pr.iProductID = 11204 then dbo.StringPadRight('082372NAD5104006',16,' ')
			when pr.sInsurer = 'SBABNA' and pr.iProductID = 11206 then dbo.StringPadRight('082372NAD5104007',16,' ')
			when pr.sInsurer = 'SBABNA' and pr.iProductID = 11201 then dbo.StringPadRight('082372NAD5104009',16,' ')
		  END
        --+CASE WHEN min(pt.sReference) like '%_ME_%' 
        --   THEN convert(char(8),min(pt.dTransaction),112)
        --     ELSE convert(char(8),GetDate(),112)
        --         END
		+ convert(char(8),GetDate(),112)
        +dbo.StringPadLeft('0' + cast(Round((sum(pt.cPremium+pt.cAdminFee+pt.cBrokerFee+pt.cPolicyFee)*-1),4) as money),18,'0')+'00'
        +dbo.StringPadRight(replace(p.sBranchCode,'-',''),8,' ') --Branch SOLID
        +CASE 
			when pr.sInsurer = 'SBABNA' then dbo.StringPadLeft(isnull(nullif(rtrim(ltrim(pd.sFieldValue)),''),'1'),10,'0')  
		 END --Serial No
		 + '0000000000000001'
        	+'  ' --Reason Code
        	+dbo.StringPadRight(ISNULL(cd.sFieldValue,''),50,' ') as 'sLine',GetDate(),'Integration' ,0			
			from customer c with (nolock)
				inner join Policy p with (nolock) on p.iCustomerID = c.iCustomerId
				left join PolicyDetails pd with (nolock) on pd.ipolicyid = p.ipolicyid and pd.sfieldcode = 'P_SERIALNUMBER'
				inner join Products pr with (nolock) on pr.iProductid = p.iProductid and pr.sProductName like 'NAM%' 
				inner join PolicyTransactions pt with (nolock) on pt.iPolicyId = p.iPolicyID 
				inner join PolicyCommStruct pcs with (nolock) on pcs.iPolicyID = p.iPolicyID and pcs.iVersionNo = pt.iAgencyId
				inner join CommEntities ce with (nolock) on ce.iCommEntityID = pcs.iLev2EntityID
				left join CustomerDetails cd with (nolock) on cd.iCustomerID = c.iCustomerID and cd.sFieldcode = 'C_CUSTOMNUMBER'
			where (pt.iTrantype = 2)
				and (left(pt.sReference,len(pt.sReference)-2) in (SELECT sReference FROM SBAB_src.StandingOrderReference WHERE bActiveIndicator = 1 and sCountry = 'NA' and (dtDateProcessed IS NULL OR CONVERT(DATE,dtDateProcessed) < CONVERT(DATE,GETDATE()) and dtExpiryDate >= CONVERT(DATE,GETDATE()))/*and sCountry = @CountryVar "add this in when it needs to be per country"*/))
				and exists (select null from [SBAB_src].[PaidUnpaids] scrunp (nolock) where scrunp.sPolicy_Number = p.sPolicyNo and sStatus_Flag = 'U' 
				and scrunp.dtPay_Date = convert(date,(getdate() - 4)) ) 
			group by c.sFirstName,c.sLastName,p.sPolicyNo, p.sAccountNo,p.dNextPaymentDate,p.sBranchCode,p.iProductId,cd.sFieldValue,ce.sCode,  pt.dTransaction, pr.sInsurer,pr.sProductType ,pd.sFieldValue,pr.iProductID

select sStatus_Flag,count(iID) from [SBAB_src].[PaidUnpaids]
where sCountry like 'NA%'
and dtPay_Date = convert(date,(getdate() - 4))
--and sStatus_Flag = 'u'
group by sStatus_Flag