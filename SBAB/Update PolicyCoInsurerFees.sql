SELECT CD.sFieldValue, pol.CountryId, pol.sPolicyNo, pol.sOldPolicyNo, pol.dDateCreated,pcf.*
--UPDATE pCF set cAdminFeePerc =100, cBrokerFeePerc = 100, cPolicyFeePerc = 100
FROM Policy POL
JOIN PolicyCoInsurerFees PCF
	ON PCF.iPolicyID = POL.iPolicyID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = Pol.iPolicyID
where 
	cAdminFeePerc <> 100
	and pol.CountryId = 232
order by 5