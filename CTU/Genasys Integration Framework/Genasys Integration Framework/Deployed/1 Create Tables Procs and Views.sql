USE [SKi_CTU]
GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportAddress]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportAddress] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + '|'
	FROM (
		SELECT c.NAME
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Address'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Address'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @sDataRecord = ''
	SET @SQLInsertDataCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + ' + '
	FROM (
		SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Address'
			) a
		) b

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
			)
	SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Address'' ,0 FROM SKI_INT.EDW_Address'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Address'' ,0 FROM SKI_INT.EDW_Address'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimants]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimants] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + '|'
	FROM (
		SELECT c.NAME
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Claimants'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Claimants'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @sDataRecord = ''
	SET @SQLInsertDataCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + ' + '
	FROM (
		SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Claimants'
			) a
		) b

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
			)
	SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Claimants'' ,0 FROM SKI_INT.EDW_Claimants'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Claimants'' ,0 FROM SKI_INT.EDW_Claimants'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimCatastrophes]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimCatastrophes] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimCatastrophe'
		) a
        
	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimCatastrophe'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0 FROM SKI_INT.EDW_ClaimCatastrophe'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0 FROM SKI_INT.EDW_ClaimCatastrophe'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimDetails'' ,0 FROM SKI_INT.EDW_ClaimDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimDetails'' ,0 FROM SKI_INT.EDW_ClaimDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimFloatPayments]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimFloatPayments] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimFloatPayments'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimFloatPayments'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimFloatPayments'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimFloatPayments'' ,0 FROM SKI_INT.EDW_ClaimFloatPayments'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimFloatPayments'' ,0 FROM SKI_INT.EDW_ClaimFloatPayments'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaims]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaims] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Claims'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Claims'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Claims'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Claims'' ,0 FROM SKI_INT.EDW_Claims'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Claims'' ,0 FROM SKI_INT.EDW_Claims'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimTran'' ,0 FROM SKI_INT.EDW_ClaimTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimTran'' ,0 FROM SKI_INT.EDW_ClaimTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimVendors]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimVendors] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimVendors'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimVendors'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimVendors'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimVendors'' ,0 FROM SKI_INT.EDW_ClaimVendors'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimVendors'' ,0 FROM SKI_INT.EDW_ClaimVendors'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportCustomer]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportCustomer] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Customer'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Customer'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Customer'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Customer'' ,0 FROM SKI_INT.EDW_Customer'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Customer'' ,0 FROM SKI_INT.EDW_Customer'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportCustomerDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportCustomerDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_CustomerDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_CustomerDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_CustomerDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_CustomerDetails'' ,0 FROM SKI_INT.EDW_CustomerDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_CustomerDetails'' ,0 FROM SKI_INT.EDW_CustomerDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportDebitNoteBalance]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportDebitNoteBalance] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_DebitNoteBalance'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_DebitNoteBalance'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0 FROM SKI_INT.EDW_DebitNoteBalance'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0 FROM SKI_INT.EDW_DebitNoteBalance'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportFieldCodes]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportFieldCodes] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_FieldCodes'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_FieldCodes'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_FieldCodes'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_FieldCodes'' ,0 FROM SKI_INT.EDW_FieldCodes'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_FieldCodes'' ,0 FROM SKI_INT.EDW_FieldCodes'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicy]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicy] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Policy'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Policy'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Policy'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Policy'' ,0 FROM SKI_INT.EDW_Policy'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Policy'' ,0 FROM SKI_INT.EDW_Policy'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicyClaimTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicyClaimTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolicyClaimTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolicyClaimTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0 FROM SKI_INT.EDW_PolicyClaimTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0 FROM SKI_INT.EDW_PolicyClaimTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicyDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicyDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolicyDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolicyDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolicyDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolicyDetails'' ,0 FROM SKI_INT.EDW_PolicyDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolicyDetails'' ,0 FROM SKI_INT.EDW_PolicyDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTran'' ,0 FROM SKI_INT.EDW_PolTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTran'' ,0 FROM SKI_INT.EDW_PolTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTranDet]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTranDet] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTranDet'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTranDet'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|'''  
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTranDet'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTranDet'' ,0 FROM SKI_INT.EDW_PolTranDet'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTranDet'' ,0 FROM SKI_INT.EDW_PolTranDet'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTranSummary]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTranSummary] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTranSummary'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTranSummary'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTranSummary'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTranSummary'' ,0 FROM SKI_INT.EDW_PolTranSummary'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTranSummary'' ,0 FROM SKI_INT.EDW_PolTranSummary'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportProducts]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportProducts] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Products'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Products'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Products'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Products'' ,0 FROM SKI_INT.EDW_Products'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Products'' ,0 FROM SKI_INT.EDW_Products'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportRiskDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC SKI_INT.EDW_ExportRiskDetails 'SKI_INT.SKI_EDW_RiskDetails_20180115150759'

CREATE PROCEDURE [SKI_INT].[EDW_ExportRiskDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_RiskDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_RiskDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	     SELECT 
        CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
        ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
        END AS 'Name'
	    FROM (
		    SELECT CASE 
				    WHEN patindex('%[.]%', c.NAME) > 0
					    THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				    ELSE c.NAME
				    END AS 'Name'
                    , c.system_type_id
		    FROM sys.tables t
		    JOIN sys.columns c ON t.object_id = c.object_id
			    AND t.NAME = 'EDW_RiskDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_RiskDetails'' ,0 FROM SKI_INT.EDW_RiskDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_RiskDetails'' ,0 FROM SKI_INT.EDW_RiskDetails'

	exec (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportRisks]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportRisks] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Risks'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Risks'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Risks'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Risks'' ,0 FROM SKI_INT.EDW_Risks'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Risks'' ,0 FROM SKI_INT.EDW_Risks'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END



GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Address_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Address_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Address;

INSERT INTO SKI_INT.EDW_Address
SELECT *
FROM SKI_INT.EDW_Address_Delta(NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Claimants_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Claimants_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Claimants;

INSERT INTO SKI_INT.EDW_Claimants
SELECT 
    iClaimantID
    ,iClaimID
    ,sRelationCode
    ,L_sRelationCode.sDescription AS sRelationCode_Desc
    ,sClaimantName
    ,iClaimantAddressID
    ,NORM.sDescription
    ,cAmtClaimed
    ,cTotExcess
    ,cExcessRecovered
    ,cAmountPaid
    ,cRecovered
    ,sAccountHolder
    ,sAccountNumber
    ,sBranchCode
    ,sBankName
    ,sCCAccountNumber
    ,sCCExpiryDate
    ,sCCControlDigits
    ,bCreditCard
    ,sCreditCardType
    ,sAccType
    ,sVATType
    ,sVATNo
    ,sStatus
    ,bExcludeCLAmt
    ,sLinkFilename
    ,cSalvage
    ,iVendorID
    ,sFinAccount
    ,sExternalID
    ,ClaimCategory
FROM 
  SKI_INT.EDW_Claimants_Delta NORM (NOLOCK)
  LEFT JOIN Lookup L_sRelationCode WITH (NOEXPAND NOLOCK)
    ON L_sRelationCode.sGroup = 'ClaimantRelationship'
    AND L_sRelationCode.sDBValue = sRelationCode


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimCatastrophe_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimCatastrophe_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimCatastrophe;

INSERT INTO SKI_INT.EDW_ClaimCatastrophe
SELECT 
 id AS CatastropheID
 , Name
 , Description
 , StartDate
 , EndDate
 , AffectedArea
 , DateLastUpdated
 , UserLastUpdated
FROM
  cfg.Catastrophe CV (NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimDetails_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimDetails;

INSERT INTO SKI_INT.EDW_ClaimDetails
SELECT 
  iClaimID
  ,dLastUpdated
  ,CL_THICLA
  ,CL_PBEATER
  ,CL_BACKOFFINEG
  ,CL_CHK_243
  ,CL_CHK_000
  ,CL_CHK_002
  ,CL_CHK_003
  ,CL_REPUDI
  ,CL_REPUDE
  ,CL_VEHDRI
  ,CL_DATOIN
  ,CL_DATOID
  ,CL_CHK_010
  ,CL_APPDATE
  ,CL_CHK_011
  ,CL_CHK_012
  ,CL_CHK_013
  ,CL_CHK_014
  ,CL_CHK_015
  ,CL_CHK_016
  ,CL_CHK_240
  ,CL_CHK_215
  ,CL_CHK_216
  ,CL_CHK_241
  ,CL_CHK_017
  ,CL_CHK_023
  ,CL_CHK_024
  ,CL_CHK_025
  ,CL_CHK_027
  ,CL_CHK_163
  ,CL_CHK_028
  ,CL_CHK_026
  ,CL_CHK_029
  ,CL_CHK_052
  ,CL_CHK_092
  ,CL_RECCLA
  ,CL_CHK_094
  ,CL_CHK_095
  ,CL_CHK_098
  ,CL_CHK_099
  ,CL_CHK_100
  ,CL_CHK_128
  ,CL_CHK_129
  ,CL_CHK_130
  ,CL_CHK_131
  ,CL_CHK_140
  ,CL_CHK_200
  ,CL_CHK_201
  ,CL_CHK_202
  ,CL_CHK_203
  ,CL_CHK_219
  ,CL_CHK_220
  ,CL_CHK_222
  ,CL_CHK_223
  ,CL_INVAPP
  ,CL_INVNAME
  ,CL_CHK_237
  ,CL_CHK_238
  ,CL_CHK_239
  ,CL_CHK_030
  ,CL_CHK_031
  ,CL_CHK_032
  ,CL_CHK_224
  ,CL_CHK_225
  ,CL_CHK_033
  ,CL_CHK_034
  ,CL_CHK_0342
  ,CL_CHK_0343
  ,CL_CHK_0344
  ,CL_CHK_0345
  ,CL_WRTOFF
  ,CL_WOCONFDTE
  ,CL_STKNMBR
  ,CL_SALAGREE
  ,CL_SALUPLDATE
  ,CL_INVOICED
  ,CL_INVNO
  ,CL_CHK_228
  ,CL_CHK_229
  ,CL_CHK_230
  ,CL_CHK_247
  ,CL_CHK_232
  ,CL_CHK_233
  ,CL_CHK_248
  ,CL_CHK_235
  ,CL_CHK_039
  ,CL_CHK_102
  ,CL_AGTNAME
  ,CL_OSAGT
  ,CL_OSAGTREFNO
  ,CL_OSAGTAPPDATE
  ,CL_CHK_106
  ,CL_CHK_109
  ,CL_CHK_111
  ,CL_RECMER
  ,CL_CHK_115
  ,CL_CHK_116
  ,CL_TPINSURID
  ,CL_CHK_INSCONME
  ,CL_CHK_123
  ,CL_CHK_124
  ,CL_CHK_125
  ,CL_CHK_126
  ,CL_CHK_127
  ,CL_CHK_141
  ,CL_CHK_145
  ,CL_CHK_147
  ,CL_CHK_162
  ,CL_CHK_200_01
  ,CL_RECCLA_01
  ,CL_CHK_201_01
  ,CL_CHK_094_01
  ,CL_CHK_202_01
  ,CL_CHK_095_01
  ,CL_CHK_203_01
  ,CL_CHK_098_01
  ,CL_CHK_099_01
  ,CL_CHK_100_01
  ,CL_CHK_128_01
  ,CL_CHK_129_01
  ,CL_CHK_130_01
  ,CL_CHK_131_01
  ,CL_CHK_140_01
  ,CL_CHK_219_01
  ,CL_CHK_220_01
  ,CL_CHK_222_01
  ,CL_CHK_223_01
  ,CL_CHK_102_01
  ,CL_AGTNAME_01
  ,CL_OSAGT_01
  ,CL_OSAGTREFN_01
  ,CL_OSAGTAPPD_01
  ,CL_CHK_106_01
  ,CL_CHK_109_01
  ,CL_CHK_111_01
  ,CL_RECMER_01
  ,CL_CHK_115_01
  ,CL_CHK_116_01
  ,CL_TPINSURID_01
  ,CL_CHK_INSCO_01
  ,CL_CHK_123_01
  ,CL_CHK_124_01
  ,CL_CHK_125_01
  ,CL_CHK_126_01
  ,CL_CHK_127_01
  ,CL_CHK_141_01
  ,CL_CHK_145_01
  ,CL_CHK_147_01
  ,CL_CHK_162_01
  ,CL_CHK_200_02
  ,CL_RECCLA_02
  ,CL_CHK_201_02
  ,CL_CHK_094_02
  ,CL_CHK_202_02
  ,CL_CHK_095_02
  ,CL_CHK_203_02
  ,CL_CHK_098_02
  ,CL_CHK_099_02
  ,CL_CHK_100_02
  ,CL_CHK_128_02
  ,CL_CHK_129_02
  ,CL_CHK_130_02
  ,CL_CHK_131_02
  ,CL_CHK_140_02
  ,CL_CHK_219_02
  ,CL_CHK_220_02
  ,CL_CHK_222_02
  ,CL_CHK_223_02
  ,CL_CHK_102_02
  ,CL_AGTNAME_02
  ,CL_OSAGT_02
  ,CL_OSAGTREFN_02
  ,CL_OSAGTAPPD_02
  ,CL_CHK_106_02
  ,CL_CHK_109_02
  ,CL_CHK_111_02
  ,CL_RECMER_02
  ,CL_CHK_115_02
  ,CL_CHK_116_02
  ,CL_TPINSURID_02
  ,CL_CHK_INSCO_02
  ,CL_CHK_123_02
  ,CL_CHK_124_02
  ,CL_CHK_125_02
  ,CL_CHK_126_02
  ,CL_CHK_127_02
  ,CL_CHK_141_02
  ,CL_CHK_145_02
  ,CL_CHK_147_02
  ,CL_CHK_162_02
  ,CL_CHK_200_03
  ,CL_RECCLA_03
  ,CL_CHK_201_03
  ,CL_CHK_094_03
  ,CL_CHK_202_03
  ,CL_CHK_095_03
  ,CL_CHK_203_03
  ,CL_CHK_098_03
  ,CL_CHK_099_03
  ,CL_CHK_100_03
  ,CL_CHK_128_03
  ,CL_CHK_129_03
  ,CL_CHK_130_03
  ,CL_CHK_131_03
  ,CL_CHK_140_03
  ,CL_CHK_219_03
  ,CL_CHK_220_03
  ,CL_CHK_222_03
  ,CL_CHK_223_03
  ,CL_CHK_102_03
  ,CL_AGTNAME_03
  ,CL_OSAGT_03
  ,CL_OSAGTREFN_03
  ,CL_OSAGTAPPD_03
  ,CL_CHK_106_03
  ,CL_CHK_109_03
  ,CL_CHK_111_03
  ,CL_RECMER_03
  ,CL_CHK_115_03
  ,CL_CHK_116_03
  ,CL_TPINSURID_03
  ,CL_CHK_INSCO_03
  ,CL_CHK_123_03
  ,CL_CHK_124_03
  ,CL_CHK_125_03
  ,CL_CHK_126_03
  ,CL_CHK_127_03
  ,CL_CHK_141_03
  ,CL_CHK_145_03
  ,CL_CHK_147_03
  ,CL_CHK_162_03
  ,CL_CHK_200_04
  ,CL_RECCLA_04
  ,CL_CHK_201_04
  ,CL_CHK_094_04
  ,CL_CHK_202_04
  ,CL_CHK_095_04
  ,CL_CHK_203_04
  ,CL_CHK_098_04
  ,CL_CHK_099_04
  ,CL_CHK_100_04
  ,CL_CHK_128_04
  ,CL_CHK_129_04
  ,CL_CHK_130_04
  ,CL_CHK_131_04
  ,CL_CHK_140_04
  ,CL_CHK_219_04
  ,CL_CHK_220_04
  ,CL_CHK_222_04
  ,CL_CHK_223_04
  ,CL_CHK_102_04
  ,CL_AGTNAME_04
  ,CL_OSAGT_04
  ,CL_OSAGTREFN_04
  ,CL_OSAGTAPPD_04
  ,CL_CHK_106_04
  ,CL_CHK_109_04
  ,CL_CHK_111_04
  ,CL_RECMER_04
  ,CL_CHK_115_04
  ,CL_CHK_116_04
  ,CL_TPINSURID_04
  ,CL_CHK_INSCO_04
  ,CL_CHK_123_04
  ,CL_CHK_124_04
  ,CL_CHK_125_04
  ,CL_CHK_126_04
  ,CL_CHK_127_04
  ,CL_CHK_141_04
  ,CL_CHK_145_04
  ,CL_CHK_147_04
  ,CL_CHK_162_04
  ,CL_CHK_200_05
  ,CL_RECCLA_05
  ,CL_CHK_201_05
  ,CL_CHK_094_05
  ,CL_CHK_202_05
  ,CL_CHK_095_05
  ,CL_CHK_203_05
  ,CL_CHK_098_05
  ,CL_CHK_099_05
  ,CL_CHK_100_05
  ,CL_CHK_128_05
  ,CL_CHK_129_05
  ,CL_CHK_130_05
  ,CL_CHK_131_05
  ,CL_CHK_140_05
  ,CL_CHK_219_05
  ,CL_CHK_220_05
  ,CL_CHK_222_05
  ,CL_CHK_223_05
  ,CL_CHK_102_05
  ,CL_AGTNAME_05
  ,CL_OSAGT_05
  ,CL_OSAGTREFN_05
  ,CL_OSAGTAPPD_05
  ,CL_CHK_106_05
  ,CL_CHK_109_05
  ,CL_CHK_111_05
  ,CL_RECMER_05
  ,CL_CHK_115_05
  ,CL_CHK_116_05
  ,CL_TPINSURID_05
  ,CL_CHK_INSCO_05
  ,CL_CHK_123_05
  ,CL_CHK_124_05
  ,CL_CHK_125_05
  ,CL_CHK_126_05
  ,CL_CHK_127_05
  ,CL_CHK_141_05
  ,CL_CHK_145_05
  ,CL_CHK_147_05
  ,CL_CHK_162_05
  ,CL_LEGALINV
  ,CL_CHK_211
  ,CL_CHK_212
  ,CL_CHK_213
  ,CL_CHK_214
  ,L_CL_AGTNAME.sDBValue AS CL_AGTNAME_ID
  ,L_CL_AGTNAME_01.sDBValue AS CL_AGTNAME_01_ID
  ,L_CL_AGTNAME_02.sDBValue AS CL_AGTNAME_02_ID
  ,L_CL_AGTNAME_03.sDBValue AS CL_AGTNAME_03_ID
  ,L_CL_AGTNAME_04.sDBValue AS CL_AGTNAME_04_ID
  ,L_CL_AGTNAME_05.sDBValue AS CL_AGTNAME_05_ID
  ,L_CL_BACKOFFINEG.sDBValue AS CL_BACKOFFINEG_ID
  ,L_CL_CHK_000.sDBValue AS CL_CHK_000_ID, 
  L_CL_CHK_010.sDBValue AS CL_CHK_010_ID, 
  L_CL_CHK_011.sDBValue AS CL_CHK_011_ID, 
  L_CL_CHK_012.sDBValue AS CL_CHK_012_ID, 
  L_CL_CHK_013.sDBValue AS CL_CHK_013_ID, 
  L_CL_CHK_017.sDBValue AS CL_CHK_017_ID, 
  L_CL_CHK_023.sDBValue AS CL_CHK_023_ID, 
  L_CL_CHK_024.sDBValue AS CL_CHK_024_ID, 
  L_CL_CHK_027.sDBValue AS CL_CHK_027_ID, 
  L_CL_CHK_030.sDBValue AS CL_CHK_030_ID, 
  L_CL_CHK_031.sDBValue AS CL_CHK_031_ID, 
  L_CL_CHK_033.sDBValue AS CL_CHK_033_ID, 
  L_CL_CHK_034.sDBValue AS CL_CHK_034_ID, 
  L_CL_CHK_0342.sDBValue AS CL_CHK_0342_ID, 
  L_CL_CHK_0343.sDBValue AS CL_CHK_0343_ID, 
  L_CL_CHK_0344.sDBValue AS CL_CHK_0344_ID, 
  L_CL_CHK_0345.sDBValue AS CL_CHK_0345_ID, 
  L_CL_CHK_039.sDBValue AS CL_CHK_039_ID, 
  L_CL_CHK_052.sDBValue AS CL_CHK_052_ID, 
  L_CL_CHK_092.sDBValue AS CL_CHK_092_ID, 
  L_CL_CHK_094.sDBValue AS CL_CHK_094_ID, 
  L_CL_CHK_094_01.sDBValue AS CL_CHK_094_01_ID, 
  L_CL_CHK_094_02.sDBValue AS CL_CHK_094_02_ID, 
  L_CL_CHK_094_03.sDBValue AS CL_CHK_094_03_ID, 
  L_CL_CHK_094_04.sDBValue AS CL_CHK_094_04_ID, 
  L_CL_CHK_094_05.sDBValue AS CL_CHK_094_05_ID, 
  L_CL_CHK_095.sDBValue AS CL_CHK_095_ID, 
  L_CL_CHK_095_01.sDBValue AS CL_CHK_095_01_ID, 
  L_CL_CHK_095_02.sDBValue AS CL_CHK_095_02_ID, 
  L_CL_CHK_095_03.sDBValue AS CL_CHK_095_03_ID, 
  L_CL_CHK_095_04.sDBValue AS CL_CHK_095_04_ID, 
  L_CL_CHK_095_05.sDBValue AS CL_CHK_095_05_ID, 
  L_CL_CHK_102.sDBValue AS CL_CHK_102_ID, 
  L_CL_CHK_102_01.sDBValue AS CL_CHK_102_01_ID, 
  L_CL_CHK_102_02.sDBValue AS CL_CHK_102_02_ID, 
  L_CL_CHK_102_03.sDBValue AS CL_CHK_102_03_ID, 
  L_CL_CHK_102_04.sDBValue AS CL_CHK_102_04_ID, 
  L_CL_CHK_102_05.sDBValue AS CL_CHK_102_05_ID, 
  L_CL_CHK_106.sDBValue AS CL_CHK_106_ID, 
  L_CL_CHK_106_01.sDBValue AS CL_CHK_106_01_ID, 
  L_CL_CHK_106_02.sDBValue AS CL_CHK_106_02_ID, 
  L_CL_CHK_106_03.sDBValue AS CL_CHK_106_03_ID, 
  L_CL_CHK_106_04.sDBValue AS CL_CHK_106_04_ID, 
  L_CL_CHK_106_05.sDBValue AS CL_CHK_106_05_ID, 
  L_CL_CHK_109.sDBValue AS CL_CHK_109_ID, 
  L_CL_CHK_109_01.sDBValue AS CL_CHK_109_01_ID, 
  L_CL_CHK_109_02.sDBValue AS CL_CHK_109_02_ID, 
  L_CL_CHK_109_03.sDBValue AS CL_CHK_109_03_ID, 
  L_CL_CHK_109_04.sDBValue AS CL_CHK_109_04_ID, 
  L_CL_CHK_109_05.sDBValue AS CL_CHK_109_05_ID, 
  L_CL_CHK_111.sDBValue AS CL_CHK_111_ID, 
  L_CL_CHK_111_01.sDBValue AS CL_CHK_111_01_ID, 
  L_CL_CHK_111_02.sDBValue AS CL_CHK_111_02_ID, 
  L_CL_CHK_111_03.sDBValue AS CL_CHK_111_03_ID, 
  L_CL_CHK_111_04.sDBValue AS CL_CHK_111_04_ID, 
  L_CL_CHK_111_05.sDBValue AS CL_CHK_111_05_ID, 
  L_CL_CHK_116.sDBValue AS CL_CHK_116_ID, 
  L_CL_CHK_116_01.sDBValue AS CL_CHK_116_01_ID, 
  L_CL_CHK_116_02.sDBValue AS CL_CHK_116_02_ID, 
  L_CL_CHK_116_03.sDBValue AS CL_CHK_116_03_ID, 
  L_CL_CHK_116_04.sDBValue AS CL_CHK_116_04_ID, 
  L_CL_CHK_116_05.sDBValue AS CL_CHK_116_05_ID, 
  L_CL_CHK_131.sDBValue AS CL_CHK_131_ID, 
  L_CL_CHK_131_01.sDBValue AS CL_CHK_131_01_ID, 
  L_CL_CHK_131_02.sDBValue AS CL_CHK_131_02_ID, 
  L_CL_CHK_131_03.sDBValue AS CL_CHK_131_03_ID, 
  L_CL_CHK_131_04.sDBValue AS CL_CHK_131_04_ID, 
  L_CL_CHK_131_05.sDBValue AS CL_CHK_131_05_ID, 
  L_CL_CHK_141.sDBValue AS CL_CHK_141_ID, 
  L_CL_CHK_141_01.sDBValue AS CL_CHK_141_01_ID, 
  L_CL_CHK_141_02.sDBValue AS CL_CHK_141_02_ID, 
  L_CL_CHK_141_03.sDBValue AS CL_CHK_141_03_ID, 
  L_CL_CHK_141_04.sDBValue AS CL_CHK_141_04_ID, 
  L_CL_CHK_141_05.sDBValue AS CL_CHK_141_05_ID, 
  L_CL_CHK_147.sDBValue AS CL_CHK_147_ID, 
  L_CL_CHK_147_01.sDBValue AS CL_CHK_147_01_ID, 
  L_CL_CHK_147_02.sDBValue AS CL_CHK_147_02_ID, 
  L_CL_CHK_147_03.sDBValue AS CL_CHK_147_03_ID, 
  L_CL_CHK_147_04.sDBValue AS CL_CHK_147_04_ID, 
  L_CL_CHK_147_05.sDBValue AS CL_CHK_147_05_ID, 
  L_CL_CHK_162.sDBValue AS CL_CHK_162_ID, 
  L_CL_CHK_162_01.sDBValue AS CL_CHK_162_01_ID, 
  L_CL_CHK_162_02.sDBValue AS CL_CHK_162_02_ID, 
  L_CL_CHK_162_03.sDBValue AS CL_CHK_162_03_ID, 
  L_CL_CHK_162_04.sDBValue AS CL_CHK_162_04_ID, 
  L_CL_CHK_162_05.sDBValue AS CL_CHK_162_05_ID, 
  L_CL_CHK_163.sDBValue AS CL_CHK_163_ID, 
  L_CL_CHK_200.sDBValue AS CL_CHK_200_ID, 
  L_CL_CHK_200_01.sDBValue AS CL_CHK_200_01_ID, 
  L_CL_CHK_200_02.sDBValue AS CL_CHK_200_02_ID, 
  L_CL_CHK_200_03.sDBValue AS CL_CHK_200_03_ID, 
  L_CL_CHK_200_04.sDBValue AS CL_CHK_200_04_ID, 
  L_CL_CHK_200_05.sDBValue AS CL_CHK_200_05_ID, 
  L_CL_CHK_201.sDBValue AS CL_CHK_201_ID, 
  L_CL_CHK_201_01.sDBValue AS CL_CHK_201_01_ID, 
  L_CL_CHK_201_02.sDBValue AS CL_CHK_201_02_ID, 
  L_CL_CHK_201_03.sDBValue AS CL_CHK_201_03_ID, 
  L_CL_CHK_201_04.sDBValue AS CL_CHK_201_04_ID, 
  L_CL_CHK_201_05.sDBValue AS CL_CHK_201_05_ID, 
  L_CL_CHK_211.sDBValue AS CL_CHK_211_ID, 
  L_CL_CHK_212.sDBValue AS CL_CHK_212_ID, 
  L_CL_CHK_213.sDBValue AS CL_CHK_213_ID, 
  L_CL_CHK_214.sDBValue AS CL_CHK_214_ID, 
  L_CL_CHK_215.sDBValue AS CL_CHK_215_ID, 
  L_CL_CHK_219.sDBValue AS CL_CHK_219_ID, 
  L_CL_CHK_219_01.sDBValue AS CL_CHK_219_01_ID, 
  L_CL_CHK_219_02.sDBValue AS CL_CHK_219_02_ID, 
  L_CL_CHK_219_03.sDBValue AS CL_CHK_219_03_ID, 
  L_CL_CHK_219_04.sDBValue AS CL_CHK_219_04_ID, 
  L_CL_CHK_219_05.sDBValue AS CL_CHK_219_05_ID, 
  L_CL_CHK_223.sDBValue AS CL_CHK_223_ID, 
  L_CL_CHK_223_01.sDBValue AS CL_CHK_223_01_ID, 
  L_CL_CHK_223_02.sDBValue AS CL_CHK_223_02_ID, 
  L_CL_CHK_223_03.sDBValue AS CL_CHK_223_03_ID, 
  L_CL_CHK_223_04.sDBValue AS CL_CHK_223_04_ID, 
  L_CL_CHK_223_05.sDBValue AS CL_CHK_223_05_ID, 
  L_CL_CHK_224.sDBValue AS CL_CHK_224_ID, 
  L_CL_CHK_225.sDBValue AS CL_CHK_225_ID, 
  L_CL_CHK_228.sDBValue AS CL_CHK_228_ID, 
  L_CL_CHK_229.sDBValue AS CL_CHK_229_ID, 
  L_CL_CHK_232.sDBValue AS CL_CHK_232_ID, 
  L_CL_CHK_235.sDBValue AS CL_CHK_235_ID, 
  L_CL_CHK_237.sDBValue AS CL_CHK_237_ID, 
  L_CL_CHK_238.sDBValue AS CL_CHK_238_ID, 
  L_CL_CHK_240.sDBValue AS CL_CHK_240_ID, 
  L_CL_CHK_241.sDBValue AS CL_CHK_241_ID, 
  L_CL_CHK_INSCO_01.sDBValue AS CL_CHK_INSCO_01_ID, 
  L_CL_CHK_INSCO_02.sDBValue AS CL_CHK_INSCO_02_ID, 
  L_CL_CHK_INSCO_03.sDBValue AS CL_CHK_INSCO_03_ID, 
  L_CL_CHK_INSCO_04.sDBValue AS CL_CHK_INSCO_04_ID, 
  L_CL_CHK_INSCO_05.sDBValue AS CL_CHK_INSCO_05_ID, 
  L_CL_CHK_INSCONME.sDBValue AS CL_CHK_INSCONME_ID, 
  L_CL_INVAPP.sDBValue AS CL_INVAPP_ID, 
  L_CL_INVNAME.sDBValue AS CL_INVNAME_ID, 
  L_CL_INVOICED.sDBValue AS CL_INVOICED_ID, 
  L_CL_LEGALINV.sDBValue AS CL_LEGALINV_ID, 
  L_CL_OSAGT.sDBValue AS CL_OSAGT_ID, 
  L_CL_OSAGT_01.sDBValue AS CL_OSAGT_01_ID, 
  L_CL_OSAGT_02.sDBValue AS CL_OSAGT_02_ID, 
  L_CL_OSAGT_03.sDBValue AS CL_OSAGT_03_ID, 
  L_CL_OSAGT_04.sDBValue AS CL_OSAGT_04_ID, 
  L_CL_OSAGT_05.sDBValue AS CL_OSAGT_05_ID, 
  L_CL_RECCLA.sDBValue AS CL_RECCLA_ID, 
  L_CL_RECCLA_01.sDBValue AS CL_RECCLA_01_ID, 
  L_CL_RECCLA_02.sDBValue AS CL_RECCLA_02_ID, 
  L_CL_RECCLA_03.sDBValue AS CL_RECCLA_03_ID, 
  L_CL_RECCLA_04.sDBValue AS CL_RECCLA_04_ID, 
  L_CL_RECCLA_05.sDBValue AS CL_RECCLA_05_ID, 
  L_CL_RECMER.sDBValue AS CL_RECMER_ID, 
  L_CL_RECMER_01.sDBValue AS CL_RECMER_01_ID, 
  L_CL_RECMER_02.sDBValue AS CL_RECMER_02_ID, 
  L_CL_RECMER_03.sDBValue AS CL_RECMER_03_ID, 
  L_CL_RECMER_04.sDBValue AS CL_RECMER_04_ID, 
  L_CL_RECMER_05.sDBValue AS CL_RECMER_05_ID, 
  L_CL_REPUDI.sDBValue AS CL_REPUDI_ID, 
  L_CL_SALAGREE.sDBValue AS CL_SALAGREE_ID, 
  L_CL_THICLA.sDBValue AS CL_THICLA_ID, 
  L_CL_TPINSURID.sDBValue AS CL_TPINSURID_ID, 
  L_CL_TPINSURID_01.sDBValue AS CL_TPINSURID_01_ID, 
  L_CL_TPINSURID_02.sDBValue AS CL_TPINSURID_02_ID, 
  L_CL_TPINSURID_03.sDBValue AS CL_TPINSURID_03_ID, 
  L_CL_TPINSURID_04.sDBValue AS CL_TPINSURID_04_ID, 
  L_CL_TPINSURID_05.sDBValue AS CL_TPINSURID_05_ID, 
  L_CL_VEHDRI.sDBValue AS CL_VEHDRI_ID, 
  L_CL_WRTOFF.sDBValue AS CL_WRTOFF_ID
FROM SKI_INT.st_ClaimDetails_Normal NORM (NOLOCK)
  LEFT JOIN Lookup L_CL_AGTNAME WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME.sDBValue = CL_AGTNAME
  LEFT JOIN Lookup L_CL_AGTNAME_01 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_01.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_01.sDBValue = CL_AGTNAME_01
  LEFT JOIN Lookup L_CL_AGTNAME_02 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_02.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_02.sDBValue = CL_AGTNAME_02
  LEFT JOIN Lookup L_CL_AGTNAME_03 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_03.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_03.sDBValue = CL_AGTNAME_03
  LEFT JOIN Lookup L_CL_AGTNAME_04 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_04.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_04.sDBValue = CL_AGTNAME_04
  LEFT JOIN Lookup L_CL_AGTNAME_05 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_05.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_05.sDBValue = CL_AGTNAME_05
  LEFT JOIN Lookup L_CL_BACKOFFINEG WITH (NOEXPAND NOLOCK)
    ON L_CL_BACKOFFINEG.sGroup = 'Backoffineg'
    AND L_CL_BACKOFFINEG.sDescription = CL_BACKOFFINEG
  LEFT JOIN Lookup L_CL_CHK_000 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_000.sGroup = 'CLType'
 AND L_CL_CHK_000.sDescription = CL_CHK_000
LEFT JOIN Lookup L_CL_CHK_010 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_010.sGroup = 'YesNo'
 AND L_CL_CHK_010.sDescription = CL_CHK_010
LEFT JOIN Lookup L_CL_CHK_011 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_011.sGroup = 'YesNo'
 AND L_CL_CHK_011.sDescription = CL_CHK_011
LEFT JOIN Lookup L_CL_CHK_012 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_012.sGroup = 'YesNo'
 AND L_CL_CHK_012.sDescription = CL_CHK_012
LEFT JOIN Lookup L_CL_CHK_013 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_013.sGroup = 'YesNoApp'
 AND L_CL_CHK_013.sDescription = CL_CHK_013
LEFT JOIN Lookup L_CL_CHK_017 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_017.sGroup = 'WEATHER'
 AND L_CL_CHK_017.sDescription = CL_CHK_017
LEFT JOIN Lookup L_CL_CHK_023 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_023.sGroup = 'YesNoApp'
 AND L_CL_CHK_023.sDescription = CL_CHK_023
LEFT JOIN Lookup L_CL_CHK_024 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_024.sGroup = 'CHKASSNAME'
 AND L_CL_CHK_024.sDescription = CL_CHK_024
LEFT JOIN Lookup L_CL_CHK_027 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_027.sGroup = 'CHK_027'
 AND L_CL_CHK_027.sDescription = CL_CHK_027
LEFT JOIN Lookup L_CL_CHK_030 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_030.sGroup = 'YesNoApp'
 AND L_CL_CHK_030.sDescription = CL_CHK_030
LEFT JOIN Lookup L_CL_CHK_031 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_031.sGroup = 'CHKPBEATER'
 AND L_CL_CHK_031.sDescription = CL_CHK_031
LEFT JOIN Lookup L_CL_CHK_033 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_033.sGroup = 'YesNoApp'
 AND L_CL_CHK_033.sDescription = CL_CHK_033
LEFT JOIN Lookup L_CL_CHK_034 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_034.sGroup = 'PartSupplier'
 AND L_CL_CHK_034.sDescription = CL_CHK_034
LEFT JOIN Lookup L_CL_CHK_0342 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0342.sGroup = 'PartSupplier'
 AND L_CL_CHK_0342.sDescription = CL_CHK_0342
LEFT JOIN Lookup L_CL_CHK_0343 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0343.sGroup = 'PartSupplier'
 AND L_CL_CHK_0343.sDescription = CL_CHK_0343
LEFT JOIN Lookup L_CL_CHK_0344 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0344.sGroup = 'PartSupplier'
 AND L_CL_CHK_0344.sDescription = CL_CHK_0344
LEFT JOIN Lookup L_CL_CHK_0345 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0345.sGroup = 'PartSupplier'
 AND L_CL_CHK_0345.sDescription = CL_CHK_0345
LEFT JOIN Lookup L_CL_CHK_039 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_039.sGroup = 'OthPartInv'
 AND L_CL_CHK_039.sDescription = CL_CHK_039
LEFT JOIN Lookup L_CL_CHK_052 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_052.sGroup = 'Merrit'
 AND L_CL_CHK_052.sDescription = CL_CHK_052
LEFT JOIN Lookup L_CL_CHK_092 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_092.sGroup = 'YesNo'
 AND L_CL_CHK_092.sDescription = CL_CHK_092
LEFT JOIN Lookup L_CL_CHK_094 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094.sGroup = 'YesNo'
 AND L_CL_CHK_094.sDescription = CL_CHK_094
LEFT JOIN Lookup L_CL_CHK_094_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_01.sGroup = 'YesNo'
 AND L_CL_CHK_094_01.sDescription = CL_CHK_094_01
LEFT JOIN Lookup L_CL_CHK_094_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_02.sGroup = 'YesNo'
 AND L_CL_CHK_094_02.sDescription = CL_CHK_094_02
LEFT JOIN Lookup L_CL_CHK_094_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_03.sGroup = 'YesNo'
 AND L_CL_CHK_094_03.sDescription = CL_CHK_094_03
LEFT JOIN Lookup L_CL_CHK_094_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_04.sGroup = 'YesNo'
 AND L_CL_CHK_094_04.sDescription = CL_CHK_094_04
LEFT JOIN Lookup L_CL_CHK_094_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_05.sGroup = 'YesNo'
 AND L_CL_CHK_094_05.sDescription = CL_CHK_094_05
LEFT JOIN Lookup L_CL_CHK_095 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095.sGroup = 'InterNeGot'
 AND L_CL_CHK_095.sDescription = CL_CHK_095
LEFT JOIN Lookup L_CL_CHK_095_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_01.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_01.sDescription = CL_CHK_095_01
LEFT JOIN Lookup L_CL_CHK_095_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_02.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_02.sDescription = CL_CHK_095_02
LEFT JOIN Lookup L_CL_CHK_095_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_03.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_03.sDescription = CL_CHK_095_03
LEFT JOIN Lookup L_CL_CHK_095_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_04.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_04.sDescription = CL_CHK_095_04
LEFT JOIN Lookup L_CL_CHK_095_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_05.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_05.sDescription = CL_CHK_095_05
LEFT JOIN Lookup L_CL_CHK_102 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102.sGroup = 'RecType'
 AND L_CL_CHK_102.sDescription = CL_CHK_102
LEFT JOIN Lookup L_CL_CHK_102_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_01.sGroup = 'RecType'
 AND L_CL_CHK_102_01.sDescription = CL_CHK_102_01
LEFT JOIN Lookup L_CL_CHK_102_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_02.sGroup = 'RecType'
 AND L_CL_CHK_102_02.sDescription = CL_CHK_102_02
LEFT JOIN Lookup L_CL_CHK_102_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_03.sGroup = 'RecType'
 AND L_CL_CHK_102_03.sDescription = CL_CHK_102_03
LEFT JOIN Lookup L_CL_CHK_102_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_04.sGroup = 'RecType'
 AND L_CL_CHK_102_04.sDescription = CL_CHK_102_04
LEFT JOIN Lookup L_CL_CHK_102_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_05.sGroup = 'RecType'
 AND L_CL_CHK_102_05.sDescription = CL_CHK_102_05
LEFT JOIN Lookup L_CL_CHK_106 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106.sGroup = 'UnInsType'
 AND L_CL_CHK_106.sDescription = CL_CHK_106
LEFT JOIN Lookup L_CL_CHK_106_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_01.sGroup = 'UnInsType'
 AND L_CL_CHK_106_01.sDescription = CL_CHK_106_01
LEFT JOIN Lookup L_CL_CHK_106_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_02.sGroup = 'UnInsType'
 AND L_CL_CHK_106_02.sDescription = CL_CHK_106_02
LEFT JOIN Lookup L_CL_CHK_106_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_03.sGroup = 'UnInsType'
 AND L_CL_CHK_106_03.sDescription = CL_CHK_106_03
LEFT JOIN Lookup L_CL_CHK_106_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_04.sGroup = 'UnInsType'
 AND L_CL_CHK_106_04.sDescription = CL_CHK_106_04
LEFT JOIN Lookup L_CL_CHK_106_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_05.sGroup = 'UnInsType'
 AND L_CL_CHK_106_05.sDescription = CL_CHK_106_05
LEFT JOIN Lookup L_CL_CHK_109 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109.sDescription = CL_CHK_109
LEFT JOIN Lookup L_CL_CHK_109_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_01.sDescription = CL_CHK_109_01
LEFT JOIN Lookup L_CL_CHK_109_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_02.sDescription = CL_CHK_109_02
LEFT JOIN Lookup L_CL_CHK_109_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_03.sDescription = CL_CHK_109_03
LEFT JOIN Lookup L_CL_CHK_109_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_04.sDescription = CL_CHK_109_04
LEFT JOIN Lookup L_CL_CHK_109_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_05.sDescription = CL_CHK_109_05
LEFT JOIN Lookup L_CL_CHK_111 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111.sDescription = CL_CHK_111
LEFT JOIN Lookup L_CL_CHK_111_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_01.sDescription = CL_CHK_111_01
LEFT JOIN Lookup L_CL_CHK_111_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_02.sDescription = CL_CHK_111_02
LEFT JOIN Lookup L_CL_CHK_111_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_03.sDescription = CL_CHK_111_03
LEFT JOIN Lookup L_CL_CHK_111_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_04.sDescription = CL_CHK_111_04
LEFT JOIN Lookup L_CL_CHK_111_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_05.sDescription = CL_CHK_111_05
LEFT JOIN Lookup L_CL_CHK_116 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116.sGroup = 'AttType'
 AND L_CL_CHK_116.sDescription = CL_CHK_116
LEFT JOIN Lookup L_CL_CHK_116_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_01.sGroup = 'AttType'
 AND L_CL_CHK_116_01.sDescription = CL_CHK_116_01
LEFT JOIN Lookup L_CL_CHK_116_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_02.sGroup = 'AttType'
 AND L_CL_CHK_116_02.sDescription = CL_CHK_116_02
LEFT JOIN Lookup L_CL_CHK_116_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_03.sGroup = 'AttType'
 AND L_CL_CHK_116_03.sDescription = CL_CHK_116_03
LEFT JOIN Lookup L_CL_CHK_116_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_04.sGroup = 'AttType'
 AND L_CL_CHK_116_04.sDescription = CL_CHK_116_04
LEFT JOIN Lookup L_CL_CHK_116_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_05.sGroup = 'AttType'
 AND L_CL_CHK_116_05.sDescription = CL_CHK_116_05
LEFT JOIN Lookup L_CL_CHK_131 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131.sGroup = 'APPPerc'
 AND L_CL_CHK_131.sDescription = CL_CHK_131
LEFT JOIN Lookup L_CL_CHK_131_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_01.sGroup = 'APPPerc'
 AND L_CL_CHK_131_01.sDescription = CL_CHK_131_01
LEFT JOIN Lookup L_CL_CHK_131_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_02.sGroup = 'APPPerc'
 AND L_CL_CHK_131_02.sDescription = CL_CHK_131_02
LEFT JOIN Lookup L_CL_CHK_131_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_03.sGroup = 'APPPerc'
 AND L_CL_CHK_131_03.sDescription = CL_CHK_131_03
LEFT JOIN Lookup L_CL_CHK_131_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_04.sGroup = 'APPPerc'
 AND L_CL_CHK_131_04.sDescription = CL_CHK_131_04
LEFT JOIN Lookup L_CL_CHK_131_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_05.sGroup = 'APPPerc'
 AND L_CL_CHK_131_05.sDescription = CL_CHK_131_05
LEFT JOIN Lookup L_CL_CHK_141 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141.sGroup = 'RecStatus'
 AND L_CL_CHK_141.sDescription = CL_CHK_141
LEFT JOIN Lookup L_CL_CHK_141_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_01.sGroup = 'RecStatus'
 AND L_CL_CHK_141_01.sDescription = CL_CHK_141_01
LEFT JOIN Lookup L_CL_CHK_141_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_02.sGroup = 'RecStatus'
 AND L_CL_CHK_141_02.sDescription = CL_CHK_141_02
LEFT JOIN Lookup L_CL_CHK_141_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_03.sGroup = 'RecStatus'
 AND L_CL_CHK_141_03.sDescription = CL_CHK_141_03
LEFT JOIN Lookup L_CL_CHK_141_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_04.sGroup = 'RecStatus'
 AND L_CL_CHK_141_04.sDescription = CL_CHK_141_04
LEFT JOIN Lookup L_CL_CHK_141_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_05.sGroup = 'RecStatus'
 AND L_CL_CHK_141_05.sDescription = CL_CHK_141_05
LEFT JOIN Lookup L_CL_CHK_147 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147.sGroup = 'AbandReason'
 AND L_CL_CHK_147.sDescription = CL_CHK_147
LEFT JOIN Lookup L_CL_CHK_147_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_01.sGroup = 'AbandReason'
 AND L_CL_CHK_147_01.sDescription = CL_CHK_147_01
LEFT JOIN Lookup L_CL_CHK_147_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_02.sGroup = 'AbandReason'
 AND L_CL_CHK_147_02.sDescription = CL_CHK_147_02
LEFT JOIN Lookup L_CL_CHK_147_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_03.sGroup = 'AbandReason'
 AND L_CL_CHK_147_03.sDescription = CL_CHK_147_03
LEFT JOIN Lookup L_CL_CHK_147_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_04.sGroup = 'AbandReason'
 AND L_CL_CHK_147_04.sDescription = CL_CHK_147_04
LEFT JOIN Lookup L_CL_CHK_147_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_05.sGroup = 'AbandReason'
 AND L_CL_CHK_147_05.sDescription = CL_CHK_147_05
LEFT JOIN Lookup L_CL_CHK_162 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162.sGroup = 'TPNotSett'
 AND L_CL_CHK_162.sDescription = CL_CHK_162
LEFT JOIN Lookup L_CL_CHK_162_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_01.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_01.sDescription = CL_CHK_162_01
LEFT JOIN Lookup L_CL_CHK_162_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_02.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_02.sDescription = CL_CHK_162_02
LEFT JOIN Lookup L_CL_CHK_162_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_03.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_03.sDescription = CL_CHK_162_03
LEFT JOIN Lookup L_CL_CHK_162_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_04.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_04.sDescription = CL_CHK_162_04
LEFT JOIN Lookup L_CL_CHK_162_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_05.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_05.sDescription = CL_CHK_162_05
LEFT JOIN Lookup L_CL_CHK_163 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_163.sGroup = 'YesNo'
 AND L_CL_CHK_163.sDescription = CL_CHK_163
LEFT JOIN Lookup L_CL_CHK_200 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200.sGroup = 'YesNo'
 AND L_CL_CHK_200.sDescription = CL_CHK_200
LEFT JOIN Lookup L_CL_CHK_200_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_01.sGroup = 'YesNo'
 AND L_CL_CHK_200_01.sDescription = CL_CHK_200_01
LEFT JOIN Lookup L_CL_CHK_200_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_02.sGroup = 'YesNo'
 AND L_CL_CHK_200_02.sDescription = CL_CHK_200_02
LEFT JOIN Lookup L_CL_CHK_200_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_03.sGroup = 'YesNo'
 AND L_CL_CHK_200_03.sDescription = CL_CHK_200_03
LEFT JOIN Lookup L_CL_CHK_200_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_04.sGroup = 'YesNo'
 AND L_CL_CHK_200_04.sDescription = CL_CHK_200_04
LEFT JOIN Lookup L_CL_CHK_200_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_05.sGroup = 'YesNo'
 AND L_CL_CHK_200_05.sDescription = CL_CHK_200_05
LEFT JOIN Lookup L_CL_CHK_201 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201.sGroup = 'TracName'
 AND L_CL_CHK_201.sDescription = CL_CHK_201
LEFT JOIN Lookup L_CL_CHK_201_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_01.sGroup = 'TracName'
 AND L_CL_CHK_201_01.sDescription = CL_CHK_201_01
LEFT JOIN Lookup L_CL_CHK_201_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_02.sGroup = 'TracName'
 AND L_CL_CHK_201_02.sDescription = CL_CHK_201_02
LEFT JOIN Lookup L_CL_CHK_201_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_03.sGroup = 'TracName'
 AND L_CL_CHK_201_03.sDescription = CL_CHK_201_03
LEFT JOIN Lookup L_CL_CHK_201_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_04.sGroup = 'TracName'
 AND L_CL_CHK_201_04.sDescription = CL_CHK_201_04
LEFT JOIN Lookup L_CL_CHK_201_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_05.sGroup = 'TracName'
 AND L_CL_CHK_201_05.sDescription = CL_CHK_201_05
LEFT JOIN Lookup L_CL_CHK_211 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_211.sGroup = 'CHK211'
 AND L_CL_CHK_211.sDescription = CL_CHK_211
LEFT JOIN Lookup L_CL_CHK_212 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_212.sGroup = 'CHK211'
 AND L_CL_CHK_212.sDescription = CL_CHK_212
LEFT JOIN Lookup L_CL_CHK_213 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_213.sGroup = 'CHK211'
 AND L_CL_CHK_213.sDescription = CL_CHK_213
LEFT JOIN Lookup L_CL_CHK_214 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_214.sGroup = 'CHK211'
 AND L_CL_CHK_214.sDescription = CL_CHK_214
LEFT JOIN Lookup L_CL_CHK_215 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_215.sGroup = 'CHK215'
 AND L_CL_CHK_215.sDescription = CL_CHK_215
LEFT JOIN Lookup L_CL_CHK_219 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219.sGroup = 'YesNoApp'
 AND L_CL_CHK_219.sDescription = CL_CHK_219
LEFT JOIN Lookup L_CL_CHK_219_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_01.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_01.sDescription = CL_CHK_219_01
LEFT JOIN Lookup L_CL_CHK_219_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_02.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_02.sDescription = CL_CHK_219_02
LEFT JOIN Lookup L_CL_CHK_219_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_03.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_03.sDescription = CL_CHK_219_03
LEFT JOIN Lookup L_CL_CHK_219_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_04.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_04.sDescription = CL_CHK_219_04
LEFT JOIN Lookup L_CL_CHK_219_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_05.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_05.sDescription = CL_CHK_219_05
LEFT JOIN Lookup L_CL_CHK_223 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223.sGroup = 'CHK223'
 AND L_CL_CHK_223.sDescription = CL_CHK_223
LEFT JOIN Lookup L_CL_CHK_223_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_01.sGroup = 'CHK223'
 AND L_CL_CHK_223_01.sDescription = CL_CHK_223_01
LEFT JOIN Lookup L_CL_CHK_223_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_02.sGroup = 'CHK223'
 AND L_CL_CHK_223_02.sDescription = CL_CHK_223_02
LEFT JOIN Lookup L_CL_CHK_223_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_03.sGroup = 'CHK223'
 AND L_CL_CHK_223_03.sDescription = CL_CHK_223_03
LEFT JOIN Lookup L_CL_CHK_223_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_04.sGroup = 'CHK223'
 AND L_CL_CHK_223_04.sDescription = CL_CHK_223_04
LEFT JOIN Lookup L_CL_CHK_223_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_05.sGroup = 'CHK223'
 AND L_CL_CHK_223_05.sDescription = CL_CHK_223_05
LEFT JOIN Lookup L_CL_CHK_224 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_224.sGroup = 'YesNo'
 AND L_CL_CHK_224.sDescription = CL_CHK_224
LEFT JOIN Lookup L_CL_CHK_225 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_225.sGroup = 'YesNo'
 AND L_CL_CHK_225.sDescription = CL_CHK_225
LEFT JOIN Lookup L_CL_CHK_228 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_228.sGroup = 'CHK228'
 AND L_CL_CHK_228.sDescription = CL_CHK_228
LEFT JOIN Lookup L_CL_CHK_229 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_229.sGroup = 'YesNo'
 AND L_CL_CHK_229.sDescription = CL_CHK_229
LEFT JOIN Lookup L_CL_CHK_232 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_232.sGroup = 'YesNo'
 AND L_CL_CHK_232.sDescription = CL_CHK_232
LEFT JOIN Lookup L_CL_CHK_235 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_235.sGroup = 'YesNo'
 AND L_CL_CHK_235.sDescription = CL_CHK_235
LEFT JOIN Lookup L_CL_CHK_237 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_237.sGroup = 'YesNo'
 AND L_CL_CHK_237.sDescription = CL_CHK_237
LEFT JOIN Lookup L_CL_CHK_238 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_238.sGroup = 'CHK238'
 AND L_CL_CHK_238.sDescription = CL_CHK_238
LEFT JOIN Lookup L_CL_CHK_240 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_240.sGroup = 'YesNo'
 AND L_CL_CHK_240.sDescription = CL_CHK_240
LEFT JOIN Lookup L_CL_CHK_241 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_241.sGroup = 'YesNo'
 AND L_CL_CHK_241.sDescription = CL_CHK_241
LEFT JOIN Lookup L_CL_CHK_INSCO_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_01.sDescription = CL_CHK_INSCO_01
LEFT JOIN Lookup L_CL_CHK_INSCO_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_02.sDescription = CL_CHK_INSCO_02
LEFT JOIN Lookup L_CL_CHK_INSCO_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_03.sDescription = CL_CHK_INSCO_03
LEFT JOIN Lookup L_CL_CHK_INSCO_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_04.sDescription = CL_CHK_INSCO_04
LEFT JOIN Lookup L_CL_CHK_INSCO_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_05.sDescription = CL_CHK_INSCO_05
LEFT JOIN Lookup L_CL_CHK_INSCONME WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCONME.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCONME.sDescription = CL_CHK_INSCONME
LEFT JOIN Lookup L_CL_INVAPP WITH (NOEXPAND NOLOCK)
 ON L_CL_INVAPP.sGroup = 'YesNoApp'
 AND L_CL_INVAPP.sDescription = CL_INVAPP
LEFT JOIN Lookup L_CL_INVNAME WITH (NOEXPAND NOLOCK)
 ON L_CL_INVNAME.sGroup = 'Investigator'
 AND L_CL_INVNAME.sDescription = CL_INVNAME
LEFT JOIN Lookup L_CL_INVOICED WITH (NOEXPAND NOLOCK)
 ON L_CL_INVOICED.sGroup = 'YesNo'
 AND L_CL_INVOICED.sDescription = CL_INVOICED
LEFT JOIN Lookup L_CL_LEGALINV WITH (NOEXPAND NOLOCK)
 ON L_CL_LEGALINV.sGroup = 'YesNo'
 AND L_CL_LEGALINV.sDescription = CL_LEGALINV
LEFT JOIN Lookup L_CL_OSAGT WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT.sDescription = CL_OSAGT
LEFT JOIN Lookup L_CL_OSAGT_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_01.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_01.sDescription = CL_OSAGT_01
LEFT JOIN Lookup L_CL_OSAGT_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_02.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_02.sDescription = CL_OSAGT_02
LEFT JOIN Lookup L_CL_OSAGT_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_03.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_03.sDescription = CL_OSAGT_03
LEFT JOIN Lookup L_CL_OSAGT_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_04.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_04.sDescription = CL_OSAGT_04
LEFT JOIN Lookup L_CL_OSAGT_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_05.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_05.sDescription = CL_OSAGT_05
LEFT JOIN Lookup L_CL_RECCLA WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA.sGroup = 'YesNo'
 AND L_CL_RECCLA.sDescription = CL_RECCLA
LEFT JOIN Lookup L_CL_RECCLA_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_01.sGroup = 'YesNo'
 AND L_CL_RECCLA_01.sDescription = CL_RECCLA_01
LEFT JOIN Lookup L_CL_RECCLA_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_02.sGroup = 'YesNo'
 AND L_CL_RECCLA_02.sDescription = CL_RECCLA_02
LEFT JOIN Lookup L_CL_RECCLA_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_03.sGroup = 'YesNo'
 AND L_CL_RECCLA_03.sDescription = CL_RECCLA_03
LEFT JOIN Lookup L_CL_RECCLA_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_04.sGroup = 'YesNo'
 AND L_CL_RECCLA_04.sDescription = CL_RECCLA_04
LEFT JOIN Lookup L_CL_RECCLA_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_05.sGroup = 'YesNo'
 AND L_CL_RECCLA_05.sDescription = CL_RECCLA_05
LEFT JOIN Lookup L_CL_RECMER WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER.sDescription = CL_RECMER
LEFT JOIN Lookup L_CL_RECMER_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_01.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_01.sDescription = CL_RECMER_01
LEFT JOIN Lookup L_CL_RECMER_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_02.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_02.sDescription = CL_RECMER_02
LEFT JOIN Lookup L_CL_RECMER_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_03.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_03.sDescription = CL_RECMER_03
LEFT JOIN Lookup L_CL_RECMER_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_04.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_04.sDescription = CL_RECMER_04
LEFT JOIN Lookup L_CL_RECMER_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_05.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_05.sDescription = CL_RECMER_05
LEFT JOIN Lookup L_CL_REPUDI WITH (NOEXPAND NOLOCK)
 ON L_CL_REPUDI.sGroup = 'YesNo'
 AND L_CL_REPUDI.sDescription = CL_REPUDI
LEFT JOIN Lookup L_CL_SALAGREE WITH (NOEXPAND NOLOCK)
 ON L_CL_SALAGREE.sGroup = 'SalvStatus'
 AND L_CL_SALAGREE.sDescription = CL_SALAGREE
LEFT JOIN Lookup L_CL_THICLA WITH (NOEXPAND NOLOCK)
 ON L_CL_THICLA.sGroup = 'YesNo'
 AND L_CL_THICLA.sDescription = CL_THICLA
LEFT JOIN Lookup L_CL_TPINSURID WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID.sGroup = 'TPInsure'
 AND L_CL_TPINSURID.sDescription = CL_TPINSURID
LEFT JOIN Lookup L_CL_TPINSURID_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_01.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_01.sDescription = CL_TPINSURID_01
LEFT JOIN Lookup L_CL_TPINSURID_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_02.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_02.sDescription = CL_TPINSURID_02
LEFT JOIN Lookup L_CL_TPINSURID_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_03.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_03.sDescription = CL_TPINSURID_03
LEFT JOIN Lookup L_CL_TPINSURID_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_04.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_04.sDescription = CL_TPINSURID_04
LEFT JOIN Lookup L_CL_TPINSURID_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_05.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_05.sDescription = CL_TPINSURID_05
LEFT JOIN Lookup L_CL_VEHDRI WITH (NOEXPAND NOLOCK)
 ON L_CL_VEHDRI.sGroup = 'YesNo'
 AND L_CL_VEHDRI.sDescription = CL_VEHDRI
LEFT JOIN Lookup L_CL_WRTOFF WITH (NOEXPAND NOLOCK)
 ON L_CL_WRTOFF.sGroup = 'YesNo'
 AND L_CL_WRTOFF.sDescription = CL_WRTOFF
--LEFT JOIN Lookup L_R_G_TRACKTYPE WITH (NOEXPAND NOLOCK)
-- ON L_R_G_TRACKTYPE.sGroup = 'TRACKERTYPE'
-- AND L_R_G_TRACKTYPE.sDescription = R_G_TRACKTYPE


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimFloatPayments_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimFloatPayments_Staging]
AS

TRUNCATE TABLE SKI_INT.EDW_ClaimFloatPayments;
--SELECT * FROM SKI_INT.EDW_ClaimFloatPayments

INSERT INTO SKI_INT.EDW_ClaimFloatPayments
SELECT
    cl.iClaimID,
	clmt.iClaimantID, 
    ct.iTransactionID,
	l1.sDescription as 'sClaimantType',
	clmt.sClaimantName, 
	clmt.sDescription,
	CASE WHEN cft.ManualPayment = 1 THEN 'Manual' ELSE 'EFT' END AS sPaymentMethod,
	cft.Reference, 
	ct.dtTransaction AS TransactionDate, 
	ct.cValue AS CAmount,
    ct.sRequestedBy,
    cfp.AuthStatus,
	CASE cfp.AuthStatus
		WHEN 0 THEN 'Requested'
		WHEN 1 THEN  'RequestApproved'
		WHEN 2 THEN  'PaymentAuth'
	END AS 'AuthStatus_Desc',
    cfp.RequestApprBy AS 'RequestApprovedBy', 
	cfp.RequestApprDate AS 'DateRequestApproved', 
	cfp.PaymentAuthBy AS 'PaymentAuthorizedBy', 
	cfp.PaymentAuthDate AS 'DatePaymentAuthorized'
    ,cT.bReversed
    ,CT.iParentPaymentID
FROM ClaimTransactions ct (NOLOCK)
	JOIN cl.ClaimFloatTransaction cft with (NOLOCK) 
      ON ct.iTransactionID = cft.ExternalTransactionID
	JOIN cl.ClaimFloatTransactionPayment cfp with (NOLOCK) 
      ON cfp.FloatTranID = cft.ID 
    JOIN SKI_INT.Delta_ClaimFloatPaymentTransactions DELTA WITH (NOLOCK)
      ON DELTA.FloatTranID = cft.ID  
	JOIN Claimants clmt with (NOLOCK) 
      ON clmt.iClaimantID = ct.iClaimantID
	JOIN Claims cl with (NOLOCK) 
      ON cl.iClaimID = ct.iClaimID
	LEFT JOIN Lookup l1 with (NOLOCK, noexpand) 
      ON l1.sGroup = 'ClaimantRelationship' 
      and l1.sDBValue = clmt.sRelationCode


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Claims_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Claims_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Claims;
--select * from SKI_INT.EDW_Claims;

INSERT INTO SKI_INT.EDW_Claims
SELECT 
  iClaimID
  ,iClientID
  ,iRiskID
  ,iPolicyID
  ,sRefNo
  ,sCauseCode
  ,L_CauseCode.sDescription AS sCauseCode_Desc
  ,sResponsibleName
  ,sResponsibleSurname
  ,dtEvent
  ,sStatusCode
  ,L_ClaimStatus.sDescription AS sStatusCode_Desc
  ,dStatusChanged
  ,C.sDescription
  ,sComment
  ,sApprovalCode
  ,L_ApprovalCodes.sDescription AS sApprovalCode_Desc
  ,bClientResponsible
  ,cAmtFirstClaimed
  ,cAmtClaimed
  ,cAmtPaid
  ,cRecovered
  ,cBalance
  ,iIncidentAddressID
  ,cTotExcess
  ,cExcessRecovered
  ,sAssignedTo
  ,dtCreated
  ,dLastUpdated
  ,sUpdatedBy
  ,dtRegistered
  ,cFirstExcessEst
  ,cSalvage
  ,cClaimFee
  ,sSubCauseCode
  ,L_SubCauseCode.sDescription AS sSubCauseCode_Desc
  ,sFinAccount
  ,dPolicyCoverStart
  ,dPolicyCoverEnd
  ,iMasterClaimID
  ,BurningCostID
  ,CorporateEntityID
  ,CatastropheId
  ,RIArrangementID
  ,ski_int.fn_GetCompanyIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS CompanyID
  ,dbo.fn_GetCompanyAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS Company
  ,ski_int.fn_GetBrokerIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS BrokerID
  ,dbo.fn_GetBrokerAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS Broker
  ,ski_int.fn_GetAgentIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS [Sub-AgentID]
  ,dbo.fn_GetAgentAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS [Sub-Agent]
  ,dbo.GetRiskSIforClaim(C.iRiskID,C.dtEvent) AS SumInsured

FROM 
  SKI_INT.EDW_Claims_Delta(NOLOCK) C
JOIN Lookup L_CauseCode WITH (NOEXPAND NOLOCK)
  ON L_CauseCode.sDBValue = C.sCauseCode
  AND L_CauseCode.sGroup = 'ClaimCauseCode'
JOIN Lookup L_ClaimStatus WITH (NOEXPAND NOLOCK)
  ON L_ClaimStatus.sDBValue = C.sStatusCode
  AND L_ClaimStatus.sGroup = 'ClaimStatus'
JOIN Lookup L_ApprovalCodes WITH (NOEXPAND NOLOCK)
  ON L_ApprovalCodes.sDBValue = C.sApprovalCode
  AND L_ApprovalCodes.sGroup = 'ClaimApprovalCodes'
JOIN Lookup L_SubCauseCode WITH (NOEXPAND NOLOCK)
  ON L_SubCauseCode.sDBValue = C.sSubCauseCode
  AND L_SubCauseCode.sGroup = 'ClaimSubCauseCode'
order by c.iClaimID
--140620

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimTran_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimTran;
--select * from SKI_INT.EDW_ClaimTran;
INSERT INTO SKI_INT.EDW_ClaimTran
SELECT 
  iTransactionID
  ,iClaimID
  ,iClaimantID
  ,dtTransaction
  ,dtPeriodEnd
  ,sTransactionCode
  ,L_sTransactionCode.sDescription AS sTransactionCode_Desc
  ,cValue
  ,sReference
  ,cVATValue
  ,iPayMethod
  ,L_ClaimPayMethod.sDescription AS PayMethod_Desc
  ,sRequestedBy
  ,bReversed
  ,iParentPaymentID
  ,dtPeriodStart
  ,sFinAccount
FROM 
  SKI_INT.EDW_ClaimTran_Delta NORM (NOLOCK)
  LEFT JOIN Lookup L_sTransactionCode WITH (NOEXPAND NOLOCK)
    ON L_sTransactionCode.sGroup = 'ClaimTranCodes'
    AND L_sTransactionCode.sDBValue = sTransactionCode
  LEFT JOIN Lookup L_ClaimPayMethod WITH (NOEXPAND NOLOCK)
    ON L_ClaimPayMethod.sGroup = 'ClaimPayMethod'
    AND L_ClaimPayMethod.iIndex = iPayMethod


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimVendors_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimVendors_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimVendors;

INSERT INTO SKI_INT.EDW_ClaimVendors
SELECT 
 cID AS iVendorID
 , sName AS VendorName
 , sType AS VendorType_Desc
 , sVATRate AS VATRate
 , sVATNo AS VatNo
 , sAddressLine1
 , sAddressLine2
 , sAddressLine3
 , sSuburb
 , sPostalCode
 , sPhoneNo
 , sFaxNo
 , sEmail 
 , dLastUpdated
FROM
  ClaimVendors CV (NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Customer_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Customer_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Customer;

INSERT INTO SKI_INT.EDW_Customer
select 
   iCustomerID
  ,iAgentID
  ,ce.sName AS sAgent_Desc
  ,sLastName
  ,sFirstName
  ,sInitials
  ,dDateCreated
  ,c.dLastUpdated
  ,bCompanyInd
  ,c.sUpdatedBy,rPostAddressID
  ,rPhysAddressID
  ,rWorkAddressID
  ,sCompanyName
  ,sContactName
  ,iTranID
  ,sGroupRelationShip
  ,sLanguage
  ,C.sLanguage_Desc
  ,CountryId
  ,Country_Desc
FROM 
  SKI_INT.EDW_Customer_Delta C WITH (NOLOCK)
  JOIN dbo.CommstructEntityRel crr (NOLOCK) ON crr.iCommRelid = c.iAgentid
  JOIN dbo.Commentities ce (NOLOCK) ON ce.iCommEntityid = crr.iLev2Entityid


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_CustomerDetails_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_CustomerDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_CustomerDetails;

INSERT INTO SKI_INT.EDW_CustomerDetails
SELECT 
  iCustomerID
  ,dLastUpdated
  ,iTranID
  ,C_CELLNO
  ,C_CONTDETAILS
  ,C_DOB
  ,C_EMAIL
  ,C_FAXTEL
  ,C_GENDER
  ,C_HOMETEL
  ,C_IDNO
  ,L_C_TITLE.sDBValue AS C_TITLE_ID
  ,C_TITLE
  ,L_C_VIPCLIENT.sDBValue AS C_VIPCLIENT_ID
  ,C_VIPCLIENT
  ,C_WORKTEL
  ,CH_INITIALS
FROM 
  SKI_INT.st_CustomerDetails_Normal C(NOLOCK)
  LEFT JOIN Lookup L_C_TITLE WITH (NOEXPAND NOLOCK)
    ON L_C_TITLE.sGroup = 'title'
    AND L_C_TITLE.sDBValue = C_TITLE
  LEFT JOIN Lookup L_C_VIPCLIENT WITH (NOEXPAND NOLOCK)
      ON L_C_VIPCLIENT.sGroup = 'YesNo'
      AND L_C_VIPCLIENT.sDBValue = C_TITLE
  ;


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_DebitNoteBalance_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_DebitNoteBalance_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_DebitNoteBalance;

INSERT INTO SKI_INT.EDW_DebitNoteBalance
SELECT 
 iDebitNoteId
,cPremiumBal
,cCommissionBal
,cAdminfeeBal
,cSASRIABal
,dUpdated
,sUpdatedBy
,bReplicated
,cPolicyFeeBal
,iPolicyID
,cBrokerFeeBal
,iAgentID
,iSubAgentID
,iInvoiceNo
,iEndorsementID
FROM 
  SKI_INT.EDW_DebitNotes_Delta NORM (NOLOCK)
  

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Policy_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Policy_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Policy;

INSERT INTO SKI_INT.EDW_Policy
SELECT iPolicyID
  ,iCustomerID
  ,P.iProductID
  ,PR.sProductName AS sProduct_Desc
  ,sPolicyNo
  ,sOldPolicyNo
  ,iPolicyStatus
  ,lps.sDescription AS iPolicyStatus_Desc
  ,iPaymentStatus
  ,lpps.sDescription AS iPaymentStatus_Desc
  ,dRenewalDate
  ,dLastPaymentDate
  ,dNextPaymentDate
  ,dExpiryDate
  ,dStartDate
  ,dDateCreated
  ,iPaymentTerm
  ,L_PaymentTerm.sDescription AS iPaymentTerm_Desc
  ,cNextPaymentAmt
  ,cAnnualPremium
  ,iTaxPerc
  ,iDiscountPerc
  ,sComment
  ,sAccHolderName
  ,iPaymentMethod
  ,L_PaymentMethod.sDescription AS iPaymentMethod_Desc
  ,sBank
  ,sActionedBy
  ,iCommissionMethod
  ,L_CommissionMethod.sDescription AS iCommissionMethod_Desc
  ,cCommissionAmt
  ,cCommissionPaid
  ,sPolicyType
  ,dDateModified
  ,iTranID
  ,sBranchCode
  ,sCheckDigit
  ,sExpiryDate
  ,sAccountNo
  ,sAccountType
  ,sCancelReason
  ,sCreditCardType
  ,dEffectiveDate
  ,cAdminFee
  ,iCollection
  ,cAnnualSASRIA
  ,cPolicyFee
  ,P.iCurrency
  ,L_Currency.sDescription AS iCurrency_Desc
  ,cBrokerFee
  ,iAdminType
  ,CASE WHEN iAdminType = 0 THEN 'Flat Fee' ELSE 'Percentage with Min and Max Values' END AS sAdminType_Desc
  ,cAdminPerc
  ,cAdminMin
  ,cAdminMax
  ,iParentID
  ,iTreatyYear
  ,dCoverStart
  ,sFinAccount
  ,dCancelled
  ,iCoverTerm
  ,L_CoverTerm.sDescription AS iCoverTerm_Desc
  ,sEndorseCode
  ,CASE WHEN L_PolicyEndorseReason.sDescription IS NULL THEN sEndorseCode ELSE L_PolicyEndorseReason.sDescription END AS sEndorseCode_Desc
  ,eCommCalcMethod
  ,iCommPayLevel
  ,L_CommLevels.sDescription AS iCommPayLevel_Desc
  ,iIndemnityPeriod
  ,L_IndemnityPeriod.sDescription AS iIndemnityPeriod_Desc
  ,dReviewDate
  ,sLanguage
  ,L_Language.sDescription AS sLanguage_Desc
  ,dRenewalPrepDate
  ,CancelReasonCode
  ,L_CancelReason.sDescription AS CancelReasonCode_Desc
  ,CountryId
  ,COU.Name AS Country_Desc
  ,CashbackEnabled
  ,Lev1Name
  ,Lev1Perc
  ,Lev2Name
  ,Lev2Perc
  ,Lev3Name
  ,Lev3Perc
  ,Lev4Name
  ,Lev4Perc
  ,Lev5Name
  ,Lev5Perc

FROM 
SKI_INT.EDW_Policy_Delta (NOLOCK) P
JOIN Products PR (NOLOCK)
  ON PR.iProductID = P.iProductID
JOIN Lookup LPS WITH (NOEXPAND NOLOCK)
  ON LPS.iIndex = P.iPolicyStatus
  AND LPS.sGroup = 'PolicyStatus'
LEFT JOIN Lookup LPPS WITH (NOEXPAND NOLOCK)
  ON LPPS.iIndex = P.iPaymentStatus
  AND LPPS.sGroup = 'PaymentStatus'
JOIN Lookup L_PaymentTerm WITH (NOEXPAND NOLOCK)
  ON L_PaymentTerm.iIndex = P.iPaymentTerm
  AND L_PaymentTerm.sGroup = 'Payment Term'
JOIN Lookup L_CoverTerm WITH (NOEXPAND NOLOCK)
  ON L_CoverTerm.iIndex = P.iCoverTerm
  AND L_CoverTerm.sGroup = 'CoverTerm'
JOIN Lookup L_PaymentMethod WITH (NOEXPAND NOLOCK)
  ON L_PaymentMethod.iIndex = P.iPaymentMethod
  AND L_PaymentMethod.sGroup = 'PaymentMethod'
LEFT JOIN Lookup L_CommissionMethod WITH (NOEXPAND NOLOCK)
  ON L_CommissionMethod.iIndex = P.iCommissionMethod
  AND L_CommissionMethod.sGroup = 'CommissionMethod'
LEFT JOIN Lookup L_Currency WITH (NOEXPAND NOLOCK)
  ON L_Currency.iIndex = P.iCurrency
  AND L_Currency.sGroup = 'Currency'
LEFT JOIN Lookup L_PolicyEndorseReason WITH (NOEXPAND NOLOCK)
  ON L_PolicyEndorseReason.sDBValue = P.sEndorseCode
  AND L_PolicyEndorseReason.sGroup = 'PolicyEndorseReason'
LEFT JOIN Lookup L_CommLevels WITH (NOEXPAND NOLOCK)
  ON L_CommLevels.iIndex = P.iCommPayLevel
  AND L_CommLevels.sGroup = 'CommLevels'
LEFT JOIN Lookup L_IndemnityPeriod WITH (NOEXPAND NOLOCK)
  ON L_IndemnityPeriod.iIndex = P.iIndemnityPeriod
  AND L_IndemnityPeriod.sGroup = 'BIIndemnityPeriodDOM'
LEFT JOIN Lookup L_Language WITH (NOEXPAND NOLOCK)
  ON L_Language.sDBValue = P.sLanguage
  AND L_Language.sGroup = 'LANGUAGE'
LEFT JOIN Lookup L_CancelReason WITH (NOEXPAND NOLOCK)
  ON L_CancelReason.sDBValue = P.CancelReasonCode
  AND L_CancelReason.sGroup = 'PolicyCancellationRe'
LEFT JOIN CFG.Country COU (NOLOCK)
  ON COU.Id = P.CountryId



GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolicyClaimTran_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolicyClaimTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolicyClaimTran

INSERT INTO SKI_INT.EDW_PolicyClaimTran (
	[iClaimID]
	,[AMT CLAIMED]
	,[EXCESS]
	,[SALVAGE]
	,[RECOVERY]
	,[PAID]
	,[BALANCE]
	,[Summary Date]
	)
SELECT [iClaimID]
	,[AMT CLAIMED]
	,[EXCESS]
	,[SALVAGE]
	,[RECOVERY]
	,[PAID]
	,[BALANCE]
	,getdate()
FROM SKI_INT.EDW_PolicyClaimTransaction_Delta(NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolicyDetails_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolicyDetails_Staging]
AS

TRUNCATE TABLE SKI_INT.EDW_PolicyDetails
;
--select * from SKI_INT.EDW_PolicyDetails

INSERT INTO SKI_INT.EDW_PolicyDetails
SELECT 
  iPolicyID
  ,dDateModified
  ,iTranID
  ,L_ServiceProv.sDBValue AS P_ServiceProv_ID
  ,P_ServiceProv
  ,P_MARKETCODE
  ,P_CASENO
  ,L_P_MORE5VEH.sDBValue AS P_MORE5VEH_ID
  ,P_MORE5VEH
  ,L_P_BANKPROTIND.sDBValue AS P_BANKPROTIND_ID
  ,P_BANKPROTIND
  ,P_BANKPROTPNO
  ,L_P_PREMMATREAS.sDBValue AS P_PREMMATREAS_ID
  ,P_PREMMATREAS
  ,P_ADDINFO
  ,P_B_REFACCH
  ,P_B_REFACCN
  ,P_B_REFBRNC
  ,P_CWarning
  ,P_LASTRD_DOREF
  ,P_LASTRD_HEAD
  ,P_LASTRD_REASON
  ,P_ITCCOLOUR
  ,P_QUALQUEST
  ,P_UW
  ,L_P_UW.sDescription AS P_UW_Desc
  ,P_LASTRD_AMT
  ,P_EXISTDAMAMT
  ,P_BROINITFEE
  ,P_BROKERBANK
  ,P_B_REFUND
  ,P_OR_BROKERFEE
  ,P_OR_LEGALADV
  ,P_VIKELA
  ,R_G_HELIVAC
  ,P_LASTRD_DATE
  ,P_POLTYPE
  ,L_P_POLTYPE.sDescription AS P_POLTYPE_Desc
  ,P_LICRESTRIC
  ,L_P_LICRESTRIC.sDBValue AS P_LICRESTRIC_Desc
  ,P_MORE2CLAIMS
  ,L_P_MORE2CLAIMS.sDBValue AS P_MORE2CLAIMS_Desc
  ,P_CASHBACKBONUS
  ,L_P_CASHBACKBONUS.sDBValue AS P_CASHBACKBONUS_Desc
  ,P_B_REFACCT
  ,L_P_B_REFACCT.sDescription AS P_B_REFACCT_Desc
  ,P_B_REFBANK
  ,L_P_B_REFBANK.sDescription AS P_B_REFBANK_Desc
  ,P_EXISTDAM
  ,L_P_EXISTDAM.sDBValue AS P_EXISTDAM_Desc
  ,P_G_SOURCE
  ,L_P_G_SOURCE.sDescription AS P_G_SOURCE_Desc
  ,P_INSCANC
  ,L_P_INSCANC.sDBValue AS P_INSCANC_Desc
  ,P_INSDECL
  ,L_P_INSDECL.sDBValue AS P_INSDECL_Desc
  ,P_INSREFUSED
  ,L_P_INSREFUSED.sDBValue AS P_INSREFUSED_Desc
  ,P_ISAFECASHBACK
  ,L_P_ISAFECASHBACK.sDBValue AS P_ISAFECASHBACK_Desc
  
FROM 
  SKI_INT.st_PolicyDetails_Normal NORM (NOLOCK)
  LEFT JOIN Lookup L_ServiceProv WITH (NOEXPAND NOLOCK) ON L_ServiceProv.sGroup = 'YesNo'
	AND RTRIM(L_ServiceProv.sDescription) = RTRIM(NORM.P_ServiceProv)
  LEFT JOIN Lookup L_P_MORE5VEH WITH (NOEXPAND NOLOCK) ON L_P_MORE5VEH.sGroup = 'YesNo'
	AND RTRIM(L_P_MORE5VEH.sDescription) = RTRIM(NORM.P_MORE5VEH)
  LEFT JOIN Lookup L_P_BANKPROTIND WITH (NOEXPAND NOLOCK) ON L_P_BANKPROTIND.sGroup = 'YesNo'
	AND RTRIM(L_P_BANKPROTIND.sDescription) = RTRIM(NORM.P_BANKPROTIND)
  LEFT JOIN Lookup L_P_PREMMATREAS WITH (NOEXPAND NOLOCK) ON L_P_PREMMATREAS.sGroup = 'PremMatchReason'
	AND RTRIM(L_P_PREMMATREAS.sDescription) = RTRIM(NORM.P_PREMMATREAS)
  
  LEFT JOIN Lookup L_P_CASHBACKBONUS WITH (NOEXPAND NOLOCK) ON L_P_CASHBACKBONUS.sGroup = 'YesNo'
	AND RTRIM(L_P_CASHBACKBONUS.sDescription) = RTRIM(NORM.P_CASHBACKBONUS)
  LEFT JOIN Lookup L_P_EXISTDAM WITH (NOEXPAND NOLOCK) ON L_P_EXISTDAM.sGroup = 'YesNo'
	AND RTRIM(L_P_EXISTDAM.sDescription) = RTRIM(NORM.P_EXISTDAM)
  LEFT JOIN Lookup L_P_INSCANC WITH (NOEXPAND NOLOCK) ON L_P_INSCANC.sGroup = 'YesNo'
	AND RTRIM(L_P_INSCANC.sDescription) = RTRIM(NORM.P_INSCANC)
  LEFT JOIN Lookup L_P_INSDECL WITH (NOEXPAND NOLOCK) ON L_P_INSDECL.sGroup = 'YesNo'
	AND RTRIM(L_P_INSDECL.sDescription) = RTRIM(NORM.P_INSDECL)
  LEFT JOIN Lookup L_P_INSREFUSED WITH (NOEXPAND NOLOCK) ON L_P_INSREFUSED.sGroup = 'YesNo'
	AND RTRIM(L_P_INSREFUSED.sDescription) = RTRIM(NORM.P_INSREFUSED)
  LEFT JOIN Lookup L_P_ISAFECASHBACK WITH (NOEXPAND NOLOCK) ON L_P_ISAFECASHBACK.sGroup = 'YesNo'
	AND RTRIM(L_P_ISAFECASHBACK.sDescription) = RTRIM(NORM.P_ISAFECASHBACK)
  LEFT JOIN Lookup L_P_LICRESTRIC WITH (NOEXPAND NOLOCK) ON L_P_LICRESTRIC.sGroup = 'YesNo'
	AND RTRIM(L_P_LICRESTRIC.sDescription) = RTRIM(NORM.P_LICRESTRIC)
  LEFT JOIN Lookup L_P_MORE2CLAIMS WITH (NOEXPAND NOLOCK) ON L_P_MORE2CLAIMS.sGroup = 'YesNo'
	AND RTRIM(L_P_MORE2CLAIMS.sDescription) = RTRIM(NORM.P_MORE2CLAIMS)

  LEFT JOIN Lookup L_P_B_REFACCT WITH (NOEXPAND NOLOCK) ON L_P_B_REFACCT.sGroup = 'BankAccountType'
	AND RTRIM(L_P_B_REFACCT.sDescription) = RTRIM(NORM.P_B_REFACCT)
  LEFT JOIN Lookup L_P_G_SOURCE WITH (NOEXPAND NOLOCK) ON L_P_G_SOURCE.sGroup = 'PolicySource'
	AND RTRIM(L_P_G_SOURCE.sDescription) = RTRIM(NORM.P_G_SOURCE)
  LEFT JOIN Lookup L_P_B_REFBANK WITH (NOEXPAND NOLOCK) ON L_P_B_REFBANK.sGroup = 'RefBankNames'
	AND RTRIM(L_P_B_REFBANK.sDescription) = RTRIM(NORM.P_B_REFBANK)
  LEFT JOIN Lookup L_P_POLTYPE WITH (NOEXPAND NOLOCK) ON L_P_POLTYPE.sGroup = 'SATVIKTYPE'
	AND RTRIM(L_P_POLTYPE.sDescription) = RTRIM(NORM.P_POLTYPE)
  LEFT JOIN Lookup L_P_UW WITH (NOEXPAND NOLOCK) ON L_P_UW.sGroup = 'Underwriters'
	AND RTRIM(L_P_UW.sDescription) = RTRIM(NORM.P_UW)

    
    
;
/*

SELECT SFIELDCODE, ll.sDescription, iFieldType, FieldCodes.sDescription, 0, 'Policy', sLookupGroup FROM FieldCodes
JOIN LOOKUP LL
  on ll.sGroup = 'fieldtypes'
  and ll.iIndex = iFieldType
where fieldcodes.sFieldCode in 
('P_ADDINFO','P_B_REFACCH','P_B_REFACCN','P_B_REFACCT','P_B_REFBANK','P_B_REFBRNC','P_B_REFUND','P_BANKPROTIND','P_BANKPROTPNO','P_BROINITFEE','P_BROKERBANK','P_CASENO'
,'P_CASHBACKBONUS','P_CWarning','P_EXISTDAM','P_EXISTDAMAMT','P_G_ADDNOTES','P_G_CONFNOTES','P_G_SOURCE','P_INSCANC','P_INSDECL','P_INSREFUSED','P_ISAFECASHBACK','P_ITCCOLOUR'
,'P_LASTRD_AMT','P_LASTRD_DATE','P_LASTRD_DOREF','P_LASTRD_HEAD','P_LASTRD_REASON','P_LICRESTRIC','P_MARKETCODE'
,'P_MORE2CLAIMS','P_MORE5VEH','P_OR_BROKERFEE','P_OR_LEGALADV','P_POLTYPE','P_PREMMATREAS','P_QUALQUEST','P_ServiceProv','P_UW','P_VIKELA','PolicyTypeId','R_G_HELIVAC')
and exists(
  select * from ski_int.EDW_FieldCodes e
  where e.sFieldCode = fieldcodes.sFieldCode
  and e.sFieldUsage = 'Policy'
  and iSeq = 0
  )
  order by sLookupGroup
  */

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTran_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTran;

INSERT INTO SKI_INT.EDW_PolTran
SELECT 
  iTransactionId
  ,NORM.iPolicyId
  ,sReference
  ,iAgencyId
  ,ce.sName AS Agency_Desc
  ,iTrantype
  ,L_TranType.sDescription AS iTrantype_Desc
  ,cPremium
  ,cCommission
  ,NORM.cAdminfee
  ,cSASRIA
  ,ROUND((cSASRIA * dbo.SUP_GetSASRIAPerc (dTransaction)),2) AS cSasriaCommission
  ,cCommission - ROUND((cSASRIA * dbo.SUP_GetSASRIAPerc (dTransaction)),2) AS cCommExcSasriaCommission
  ,dTransaction
  ,sUser
  ,dPeriodStart
  ,dPeriodEnd
  ,iCommStatus
  ,L_CommStatus.sDescription AS CommStatus_Desc
  ,iParentTranID
  ,iDebitNoteId
  ,NORM.cPolicyFee
  ,NORM.sComment
  ,dEffective
  ,NORM.cBrokerFee
  ,sFinAccount
FROM 
  SKI_INT.EDW_PolTran_Delta NORM (NOLOCK)
  JOIN Lookup L_TranType WITH (NOEXPAND NOLOCK)
    ON L_TranType.sGroup = 'TransactionType'
    AND L_TranType.iIndex = iTrantype
  JOIN dbo.PolicyCommStruct crr (NOLOCK) 
    ON crr.iVersionNo = NORM.iAgencyId
  JOIN dbo.Commentities ce (NOLOCK) 
    ON ce.iCommEntityid = crr.iLev2Entityid
  LEFT JOIN Lookup L_CommStatus WITH (NOEXPAND NOLOCK)
    ON L_CommStatus.sGroup = 'CommStatus'
    AND L_CommStatus.iIndex = iCommStatus


GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTranDet_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTranDet_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTranDet;

INSERT INTO SKI_INT.EDW_PolTranDet
SELECT 
  iTransactionID
  ,iPolicyID
  ,iRiskID
  ,iRiskTypeID
  ,dDateActioned
  ,iTranType
  ,L_TranType.sDescription AS iTrantype_Desc
  ,cPremium
  ,cCommission
  ,cAdminFee
  ,cSASRIA
  ,sUser
FROM 
  SKI_INT.EDW_PolTranDet_Delta NORM (NOLOCK)
  JOIN Lookup L_TranType WITH (NOEXPAND NOLOCK)
    ON L_TranType.sGroup = 'TransactionType'
    AND L_TranType.iIndex = iTrantype 

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTranSummary_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTranSummary_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTranSummary;

INSERT INTO SKI_INT.EDW_PolTranSummary (
	[PolicyID]
	,[Premium Bal]
	,[Commissoin Bal]
	,[SASRIA Bal]
	,[Admin Fee Bal]
	,[Policy Fee Bal]
	,[Broker Fee Bal]
	,[Summary Date]
	)
SELECT [PolicyID]
	,[Premium Bal]
	,[Commissoin Bal]
	,[SASRIA Bal]
	,[Admin Fee Bal]
	,[Policy Fee Bal]
	,[Broker Fee Bal]
	,GETDATE()
FROM SKI_INT.EDW_PolTranSummary_Delta(NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Products_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Products_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Products;

INSERT INTO SKI_INT.EDW_Products
SELECT 
 iProductID, sProductName, sProductCode, sInsurer
FROM
  Products CV (NOLOCK)

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_RiskDetails_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_RiskDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_RiskDetails;

INSERT INTO SKI_INT.EDW_RiskDetails (
	intPolicyID
	,iRiskID
	,dDateModified
	,iTranID
	,ZIPCODE
	,R_MOTORSEATS_ID
	,R_MOTORSEATS
	,R_MOTORYOM
	,R_MOTORREG
	,R_ENGIN_NO
	,R_CHASSISNO
	,R_COVERDETAILS
	,R_G_MARKETVAL
	,R_G_PASLIABIND
	,R_G_NUOFSEATS
	,R_G_HPLEASE
	,R_G_EXTENDLOU
	,R_G_PERSACCIND
	,R_G_CREDSFALL
	,R_G_HELIVAC
	,R_G_ABSVIOCRE
	,R_G_ISAFE
	,R_G_EXCESSRO_ID
	,R_G_EXCESSRO
	,R_G_WINDSCREEN
	,R_G_BACK2INV_ID
	,R_G_BACK2INV
	,R_G_CASHBACK
	,R_G_DEALER_ID
	,R_G_DEALER
	,R_G_INTRSTNOTED
	,R_G_CREDITPRO_ID
	,R_G_CREDITPRO
	,R_G_CRACCNO
	,R_G_ASSOC_ID
	,R_G_ASSOC
	,R_G_STDENDORSE
	,R_G_ADDFAP2000
	,R_G_ADDFAP2500
	,R_G_ADDFAP5000
	,R_G_ADDFAP7500
	,R_G_ADDFAP10000
	,R_G_ADDFAP15000
	,R_G_ADDFAP20000
	,R_G_AMATEKSI
	,R_G_AMAACCNO
	,R_G_AMAIDNO
	,R_MATCHINGCT
	,R_CTPERACC
	,R_CTHPLEASE
	,R_CTPASS
	,R_CTEXTENDLOU
	,R_CTHELIVAC
	,R_CTAMACLA
	,R_CTBACK2INV
	,R_CTISAFE
	,R_CTCASHBACK
	,R_CTCREDITSF
	,R_CTEXCESSRO
	,R_TSASRIA
	,R_MATCHINGPERC
	,R_MORE5VEH
	,R_SI
	,R_G_PASSENSI
	,R_G_PASSENDT
	,R_G_HPLEASESI
	,R_G_PERACCSI
	,R_G_EXTENDLOUSI
	,R_G_HELIVACSI
	,R_G_CSFSI
	,R_G_ABSVIOCRESI
	,R_G_ISAFESI
	,R_G_EXCESSROSI
	,R_G_WINDSCRSI
	,R_G_BACK2INVSI
	,R_G_CASHBACKSI
	,R_G_AMAPREMSI
	,R_G_VEHDEF_ID
	,R_G_VEHDEF
	,R_G_TRACKCUST_ID
	,R_G_TRACKCUST
	,R_G_TVEHDEF_ID
	,R_G_TVEHDEF
	,R_G_TRAILERLOI
	,R_G_LOANPRTPR
	,R_G_DRVDEATHSI
	,R_MATCHING
	,R_G_PASDEATHSI
	,R_G_TRACKREFNO
	,R_Driver
	,R_DriveName1
	,R_DriveID1
	,R_DriveCell1
	,R_DriveName2
	,R_DriveID2
	,R_DriveCell2
	,R_DriveName3
	,R_DriveID3
	,R_DriveCell3
	,R_DriveName4
	,R_DriveID4
	,R_DriveCell4
	,R_DriveName5
	,R_DriveID5
	,R_DriveCell5
	,R_DriveName6
	,R_DriveID6
	,R_DriveCell6
	,R_G_ADDFAPG1000
	,R_G_ADDFAPG2000
	,R_G_ADDFAPG3000
	,R_G_TPOSI
	,R_G_HPLEASEDT
	,R_G_EXTENDLOUDT
	,R_G_LOSSINOPSI
	,R_G_HELIVACDT
	,R_G_ISAFEDT
	,R_G_PERACCDT
	,R_G_CSFDT
	,R_G_ABSVIOCREDT
	,R_G_EXCESSRODT
	,R_G_WINDSCRDT
	,R_G_BACK2INVDT
	,R_G_CASHBACKDT
	,R_ISFCSHBCK_CPC
	,R_LOI_TOUR_CPC
	)
SELECT intPolicyID
	,iRiskID
	,dDateModified
	,iTranID
	,ZIPCODE
	,MOTORSEATS.sDBValue AS R_MOTORSEATS_ID
	,R_MOTORSEATS
	,R_MOTORYOM
	,R_MOTORREG
	,R_ENGIN_NO
	,R_CHASSISNO
	,R_COVERDETAILS
	,R_G_MARKETVAL
	,R_G_PASLIABIND
	,R_G_NUOFSEATS
	,R_G_HPLEASE
	,R_G_EXTENDLOU
	,R_G_PERSACCIND
	,R_G_CREDSFALL
	,R_G_HELIVAC
	,R_G_ABSVIOCRE
	,R_G_ISAFE
	,EXCESSRO.sDBValue AS R_G_EXCESSRO_ID
	,R_G_EXCESSRO
	,R_G_WINDSCREEN
	,BACK2INV.sDBValue AS R_G_BACK2INV_ID
	,R_G_BACK2INV
	,R_G_CASHBACK
	,DEALER.sDBValue AS R_G_DEALER_ID
	,R_G_DEALER
	,R_G_INTRSTNOTED
	,CREDITPRO.sDBValue AS R_G_CREDITPRO_ID
	,R_G_CREDITPRO
	,R_G_CRACCNO
	,ASSOC.sDBValue AS R_G_ASSOC_ID
	,R_G_ASSOC
	,R_G_STDENDORSE
	,R_G_ADDFAP2000
	,R_G_ADDFAP2500
	,R_G_ADDFAP5000
	,R_G_ADDFAP7500
	,R_G_ADDFAP10000
	,R_G_ADDFAP15000
	,R_G_ADDFAP20000
	,R_G_AMATEKSI
	,R_G_AMAACCNO
	,R_G_AMAIDNO
	,R_MATCHINGCT
	,R_CTPERACC
	,R_CTHPLEASE
	,R_CTPASS
	,R_CTEXTENDLOU
	,R_CTHELIVAC
	,R_CTAMACLA
	,R_CTBACK2INV
	,R_CTISAFE
	,R_CTCASHBACK
	,R_CTCREDITSF
	,R_CTEXCESSRO
	,R_TSASRIA
	,R_MATCHINGPERC
	,R_MORE5VEH
	,R_SI
	,R_G_PASSENSI
	,R_G_PASSENDT
	,R_G_HPLEASESI
	,R_G_PERACCSI
	,R_G_EXTENDLOUSI
	,R_G_HELIVACSI
	,R_G_CSFSI
	,R_G_ABSVIOCRESI
	,R_G_ISAFESI
	,R_G_EXCESSROSI
	,R_G_WINDSCRSI
	,R_G_BACK2INVSI
	,R_G_CASHBACKSI
	,R_G_AMAPREMSI
	,VEHDEF.sDBValue AS R_G_VEHDEF_ID
	,R_G_VEHDEF
	,TRACKCUST.sDBValue AS R_G_TRACKCUST_ID
	,R_G_TRACKCUST
	,TVEHDEF.sDBValue AS R_G_TVEHDEF_ID
	,R_G_TVEHDEF
	,R_G_TRAILERLOI
	,R_G_LOANPRTPR
	,R_G_DRVDEATHSI
	,R_MATCHING
	,R_G_PASDEATHSI
	,R_G_TRACKREFNO
	,R_Driver
	,R_DriveName1
	,R_DriveID1
	,R_DriveCell1
	,R_DriveName2
	,R_DriveID2
	,R_DriveCell2
	,R_DriveName3
	,R_DriveID3
	,R_DriveCell3
	,R_DriveName4
	,R_DriveID4
	,R_DriveCell4
	,R_DriveName5
	,R_DriveID5
	,R_DriveCell5
	,R_DriveName6
	,R_DriveID6
	,R_DriveCell6
	,R_G_ADDFAPG1000
	,R_G_ADDFAPG2000
	,R_G_ADDFAPG3000
	,R_G_TPOSI
	,R_G_HPLEASEDT
	,R_G_EXTENDLOUDT
	,R_G_LOSSINOPSI
	,R_G_HELIVACDT
	,R_G_ISAFEDT
	,R_G_PERACCDT
	,R_G_CSFDT
	,R_G_ABSVIOCREDT
	,R_G_EXCESSRODT
	,R_G_WINDSCRDT
	,R_G_BACK2INVDT
	,R_G_CASHBACKDT
	,R_ISFCSHBCK_CPC
	,R_LOI_TOUR_CPC
FROM SKI_INT.st_RiskDetails_Normal NORM
LEFT JOIN Lookup MOTORSEATS WITH (NOEXPAND NOLOCK) ON MOTORSEATS.sGroup = 'SATSEATS'
	AND RTRIM(MOTORSEATS.sDescription) = RTRIM(NORM.R_MOTORSEATS)
LEFT JOIN Lookup EXCESSRO WITH (NOEXPAND NOLOCK) ON EXCESSRO.sGroup = 'SATERO'
	AND RTRIM(EXCESSRO.sDescription) = RTRIM(NORM.R_G_EXCESSRO)
LEFT JOIN Lookup BACK2INV WITH (NOEXPAND NOLOCK) ON BACK2INV.sGroup = 'SATBACK2INV'
	AND RTRIM(BACK2INV.sDescription) = RTRIM(NORM.R_G_BACK2INV)
LEFT JOIN Lookup DEALER WITH (NOEXPAND NOLOCK) ON DEALER.sGroup = 'SATDEALER'
	AND RTRIM(DEALER.sDescription) = RTRIM(NORM.R_G_DEALER)
LEFT JOIN Lookup CREDITPRO WITH (NOEXPAND NOLOCK) ON CREDITPRO.sGroup = 'SATCREDIT'
	AND RTRIM(CREDITPRO.sDescription) = RTRIM(NORM.R_G_CREDITPRO)
LEFT JOIN Lookup ASSOC WITH (NOEXPAND NOLOCK) ON ASSOC.sGroup = 'SATASSOC'
	AND RTRIM(ASSOC.sDescription) = RTRIM(NORM.R_G_ASSOC)
LEFT JOIN Lookup VEHDEF WITH (NOEXPAND NOLOCK) ON VEHDEF.sGroup = 'SATVEHDEF'
	AND RTRIM(VEHDEF.sDescription) = RTRIM(NORM.R_G_VEHDEF)
LEFT JOIN Lookup TRACKCUST WITH (NOEXPAND NOLOCK) ON TRACKCUST.sGroup = 'YesNo'
	AND RTRIM(TRACKCUST.sDescription) = RTRIM(NORM.R_G_TRACKCUST)
LEFT JOIN Lookup TVEHDEF WITH (NOEXPAND NOLOCK) ON TVEHDEF.sGroup = 'SATDEFVEH'
	AND RTRIM(TVEHDEF.sDescription) = RTRIM(NORM.R_G_TVEHDEF);;

GO
/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Risks_Staging]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Risks_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Risks;
--select * from SKI_INT.EDW_Risks;

INSERT INTO SKI_INT.EDW_Risks
SELECT 
  intClientID
  ,intRiskTypeID
  ,L_Risktype.sDescription AS RiskType_Desc
  ,intRiskID
  ,intPolicyID
  ,strDescription
  ,cSumInsured
  ,dDateCreated
  ,strCreatedBy
  ,intStatus
  ,L_Status.sDescription AS intStatus_Desc
  ,intCurrency
  ,L_Currency.sDescription AS intCurrency_Desc
  ,cAdminFee
  ,R.iProductID
  ,PR.sProductName AS iProductID_Desc
  ,dDateModified
  ,iTranID
  ,iPaymentTerm
  ,L_PaymentTerm.sDescription AS iPaymentTerm_Desc
  ,cPremium
  ,cSASRIA
  ,cCommission
  ,dValidTill
  ,cVat
  ,sLastUpdatedBy
  ,dEffectiveDate
  ,iAddressID
  ,iParentID
  ,ski_int.fn_GetParentRiskDescription(iParentID) AS ParentRisk_Desc
  ,cAnnualPremium
  ,cAnnualComm
  ,cAnnualSASRIA
  ,cAnnualAdminFee
  ,sDeleteReason
  ,sSASRIAGroup
  ,R.dInception
  ,cCommissionPerc
  ,sMatching
  ,cCommissionRebate
  ,dReviewDate
  ,sReviewer
  ,PremiumPerc
FROM 
  SKI_INT.EDW_Risks_Delta R(NOLOCK)
  JOIN Lookup L_Status WITH (NOEXPAND NOLOCK)
    ON L_Status.iIndex = R.intStatus
    AND L_Status.sGroup = 'RiskStatus'
  JOIN Lookup L_Currency WITH (NOEXPAND NOLOCK)
    ON L_Currency.iIndex = R.intCurrency
    AND L_Currency.sGroup = 'Currency'
  JOIN Products PR (NOLOCK)
    ON PR.iProductID = R.iProductID
  JOIN Lookup L_PaymentTerm WITH (NOEXPAND NOLOCK)
    ON L_PaymentTerm.iIndex = R.iPaymentTerm
    AND L_PaymentTerm.sGroup = 'Payment Term'
  JOIN Lookup L_Risktype WITH (NOEXPAND NOLOCK)
    ON L_Risktype.sGroup = 'RiskType'
    AND L_Risktype.iIndex = R.intRiskTypeID 

GO
/****** Object:  StoredProcedure [SKI_INT].[NormalizeClaimDetailsTable_ALL]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeClaimDetailsTable_ALL] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalClaim'
			)
	BEGIN
		DROP TABLE TempNormalClaim
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalClaim 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalClaim

	SET @SQLInsert = 'UPDATE CDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalClaim
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_ClaimDetails_Normal CDNORM (NOLOCK)
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG WITH (NOEXPAND NOLOCK) ON LG.sDBValue = CD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN CD.sFieldValue
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_ClaimDetails_Normal CDNORM (NOLOCK) 
						' + @UpdateTableJoin + ' 
						WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalClaim
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalClaim
	END
END

GO
/****** Object:  StoredProcedure [SKI_INT].[NormalizeCustomerDetailsTable_All]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeCustomerDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id CHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalCustomer'
			)
	BEGIN
		DROP TABLE TempNormalCustomer
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalCustomer 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalCustomer

	SET @SQLInsert = 'UPDATE CDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalCustomer
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_CustomerDetails_Normal CDNORM (NOLOCK)
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG WITH (NOEXPAND NOLOCK) ON LG.sDBValue = CD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN CD.sFieldValue
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_CustomerDetails_Normal CDNORM (NOLOCK)
						' + @UpdateTableJoin + ' 
						WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalCustomer
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalCustomer
	END
END

GO
/****** Object:  StoredProcedure [SKI_INT].[NormalizePolicyDetailsTable_All]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizePolicyDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
	--with encryption
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalPolicy'
			)
	BEGIN
		DROP TABLE TempNormalPolicy
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalPolicy 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalPolicy

	SET @SQLInsert = 'UPDATE PDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalPolicy
		WHERE sFieldCode = @au_id

		--SET ROWCOUNT 1
		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			IF @PolicyID > 0 --is not null
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM (NOLOCK)
					' + @TableName + ' 
					LEFT JOIN Lookup LG WITH (NOEXPAND NOLOCK) ON LG.sDBValue = PD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + ''' 
					WHERE PD.iPolicyID = ' + RTRIM(LTRIM(str(@PolicyID))) + ' GROUP By PD.iPolicyID, P.dDateModified'
			END
			ELSE
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM (NOLOCK)
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG WITH (NOEXPAND NOLOCK) ON LG.sDBValue = PD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE PD.sFieldCode = ''' + @au_id + ''';'
			END
		END
		ELSE
		BEGIN
			IF @PolicyID > 0 --is not null
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN PD.sFieldValue
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM (NOLOCK) ' + @TableName + ' WHERE PD.iPolicyID = ' + RTRIM(LTRIM(str(@PolicyID))) + ' GROUP By PD.iPolicyID, P.dDateModified'
			END
			ELSE
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN PD.sFieldValue
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM (NOLOCK)
						' + @UpdateTableJoin + ' 
						WHERE PD.sFieldCode = ''' + @au_id + ''';'
			END
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalPolicy
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalPolicy
	END
END

GO
/****** Object:  StoredProcedure [SKI_INT].[NormalizeRiskDetailsTable_All]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeRiskDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalRisk'
			)
	BEGIN
		DROP TABLE TempNormalRisk
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalRisk 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalRisk

	SET @SQLInsert = 'UPDATE RDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE
		FROM TempNormalRisk
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + 
				' =
							COALESCE(CASE WHEN RD.iFieldType = 0 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 1 THEN ltrim(rtrim(str(RD.cFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 2 THEN ltrim(rtrim(str(RD.bFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 3 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 4 THEN ltrim(rtrim(str(RD.iFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 5 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 6 THEN LG.sDescription ELSE NULL END,
								CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue LIKE ''%@%'' THEN ISNULL(CAST(ROUND(dbo.GetCommCalcPremium(RD.sFieldValue) / 12, 2) AS VARCHAR(255)), '''') ELSE NULL END,
                                CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue NOT LIKE ''%@%'' THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 8 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 9 THEN str(RD.cFieldValue) ELSE NULL END,
								CASE WHEN RD.iFieldType = 10 THEN str(RD.iFieldValue) ELSE NULL END)
						FROM SKI_INT.st_RiskDetails_Normal RDNORM (NOLOCK) 
					' 
				+ @UpdateTableJoin + '
					LEFT JOIN Lookup LG WITH (NOEXPAND NOLOCK) ON LG.sDBValue = RD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE RD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + 
				' =
							COALESCE(CASE WHEN RD.iFieldType = 0 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 1 THEN ltrim(rtrim(str(RD.cFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 2 THEN ltrim(rtrim(str(RD.bFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 3 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 4 THEN ltrim(rtrim(str(RD.iFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 5 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 6 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue LIKE ''%@%'' THEN ISNULL(CAST(ROUND(dbo.GetCommCalcPremium(RD.sFieldValue) / 12, 2) AS VARCHAR(255)), '''') ELSE NULL END,
                                CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue NOT LIKE ''%@%'' THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 8 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 9 THEN str(RD.cFieldValue) ELSE NULL END,
								CASE WHEN RD.iFieldType = 10 THEN str(RD.iFieldValue) ELSE NULL END)
						FROM SKI_INT.st_RiskDetails_Normal RDNORM (NOLOCK)
						' 
				+ @UpdateTableJoin + ' 
						WHERE RD.sFieldCode = ''' + @au_id + ''';'
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery + '
			;')
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalRisk
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalRisk
	END
END

GO
/****** Object:  StoredProcedure [SKI_INT].[SetDeltaValues]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[SetDeltaValues]
AS
----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Policy;

INSERT INTO SKI_INT.Delta_Policy
SELECT DISTINCT iCustomerID
	,iPolicyID
	,iTranID
FROM Policy(NOLOCK)
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)

UNION

SELECT DISTINCT ARCH_Policy.iCustomerID
	,ARCH_Policy.iPolicyID
	,ARCH_Policy.iTranID
FROM ARCH_Policy(NOLOCK)
INNER JOIN Policy Pol(NOLOCK) ON Pol.iPolicyID = ARCH_Policy.iPolicyID
WHERE CONVERT(CHAR(8), ARCH_Policy.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Policy'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Policy(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Customer;

INSERT INTO SKI_INT.Delta_Customer
SELECT DISTINCT Customer.iCustomerID
	,Customer.iTranID
FROM Customer(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Customer.iCustomerID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)

UNION

SELECT DISTINCT ARCH_Customer.iCustomerID
	,ARCH_Customer.iTranID
FROM ARCH_Customer(NOLOCK)
INNER JOIN Customer Cust(NOLOCK) ON Cust.iCustomerID = ARCH_Customer.iCustomerID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Customer.iCustomerID
WHERE CONVERT(CHAR(8), ARCH_Customer.dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Customer'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Customer(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Claims;

INSERT INTO SKI_INT.Delta_Claims
SELECT DISTINCT iClientID
	,Claims.iClaimID
	,Claims.iPolicyID
FROM Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)

UNION

SELECT DISTINCT ARCH_Claims.iClientID
	,ARCH_Claims.iClaimID
	,ARCH_Claims.iPolicyID
FROM ARCH_Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)
ORDER BY iClientID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Claims'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Claims(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimTransactions;

INSERT INTO SKI_INT.Delta_ClaimTransactions
SELECT DISTINCT ClaimTransactions.iClaimID
	,iTransactionID
	,iClaimantID
FROM ClaimTransactions(NOLOCK)
JOIN SKI_INT.Delta_Claims CC (NOLOCK)
  ON CC.iClaimID = ClaimTransactions.iClaimID
WHERE CONVERT(CHAR(8), dtTransaction, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_ClaimTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_PolicyTransactions;

INSERT INTO SKI_INT.Delta_PolicyTransactions
SELECT DISTINCT PolicyTransactions.iPolicyID
	,iTransactionID
FROM PolicyTransactions(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = PolicyTransactions.iPolicyId
WHERE CASE 
		WHEN dTransaction > dEffective
			THEN CONVERT(CHAR(8), dEffective, 112)
		ELSE CONVERT(CHAR(8), dTransaction, 112)
		END >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_PolicyTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_PolicyTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_PolicyTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimFloatPaymentTransactions;

INSERT INTO SKI_INT.Delta_ClaimFloatPaymentTransactions
SELECT 
  cl.ClaimFloatTransactionPayment.FloatTranID
FROM cl.ClaimFloatTransactionPayment (NOLOCK)
WHERE DateLastUpdated >= (
		                  SELECT MAX(LastRunDate)
		                  FROM SKI_INT.EDW_RunStatus (NOLOCK)
		                  WHERE TableName = 'Delta_ClaimFloatPayment'
		                  )

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimFloatPayment'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimFloatPaymentTransactions(NOLOCK)
		)
	,getdate()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Risks;

INSERT INTO SKI_INT.Delta_Risks
SELECT DISTINCT intPolicyID
	,intRiskID
	,Risks.iTranID
FROM Risks(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = Risks.intPolicyID
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

UNION

SELECT DISTINCT ARCH_Risks.intPolicyID
	,ARCH_Risks.intRiskID
	,ARCH_Risks.iTranID
FROM ARCH_Risks(NOLOCK)
INNER JOIN Risks RR(NOLOCK) ON RR.intRiskID = ARCH_Risks.intRiskID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = RR.intPolicyID
WHERE CONVERT(CHAR(8), ARCH_Risks.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Risks'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Risks(NOLOCK)
		)
	,GETDATE()
	)

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_DebitNoteBalance;

INSERT INTO SKI_INT.Delta_DebitNoteBalance
SELECT DISTINCT iDebitNoteID
FROM DebitNoteBalance (NOLOCK)
WHERE CONVERT(CHAR(8), dUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_DebitNoteBalance'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_DebitNoteBalance'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_DebitNoteBalance(NOLOCK)
		)
	,GETDATE()
	)

    

GO
/****** Object:  StoredProcedure [SKI_INT].[SetDeltaValues_ALL]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[SetDeltaValues_ALL]
AS
----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Policy;

INSERT INTO SKI_INT.Delta_Policy
SELECT DISTINCT iCustomerID
	,iPolicyID
	,iTranID
FROM Policy(NOLOCK)
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Policy'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Policy(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Customer;

INSERT INTO SKI_INT.Delta_Customer
SELECT DISTINCT Customer.iCustomerID
	,Customer.iTranID
FROM Customer(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Customer.iCustomerID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Customer'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Customer(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Claims;

INSERT INTO SKI_INT.Delta_Claims
SELECT DISTINCT iClientID
	,Claims.iClaimID
	,Claims.iPolicyID
FROM Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Claims'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Claims(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimTransactions;

INSERT INTO SKI_INT.Delta_ClaimTransactions
SELECT DISTINCT ClaimTransactions.iClaimID
	,iTransactionID
	,iClaimantID
FROM ClaimTransactions(NOLOCK)
JOIN SKI_INT.Delta_Claims CC (NOLOCK)
  ON CC.iClaimID = ClaimTransactions.iClaimID
WHERE CONVERT(CHAR(8), dtTransaction, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_ClaimTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_PolicyTransactions;

INSERT INTO SKI_INT.Delta_PolicyTransactions
SELECT DISTINCT PolicyTransactions.iPolicyID
	,iTransactionID
FROM PolicyTransactions(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = PolicyTransactions.iPolicyId
WHERE CASE 
		WHEN dTransaction > dEffective
			THEN CONVERT(CHAR(8), dEffective, 112)
		ELSE CONVERT(CHAR(8), dTransaction, 112)
		END >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_PolicyTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_PolicyTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_PolicyTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimFloatPaymentTransactions;

INSERT INTO SKI_INT.Delta_ClaimFloatPaymentTransactions
SELECT 
  cl.ClaimFloatTransactionPayment.FloatTranID
FROM cl.ClaimFloatTransactionPayment (NOLOCK)
WHERE DateLastUpdated >= (
		                  SELECT MAX(LastRunDate)
		                  FROM SKI_INT.EDW_RunStatus (NOLOCK)
		                  WHERE TableName = 'Delta_ClaimFloatPayment'
		                  )

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimFloatPayment'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimFloatPaymentTransactions(NOLOCK)
		)
	,'2017-11-01 12:04:01.717'
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Risks;

INSERT INTO SKI_INT.Delta_Risks
SELECT DISTINCT intPolicyID
	,intRiskID
	,Risks.iTranID
FROM Risks(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = Risks.intPolicyID
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Risks'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Risks(NOLOCK)
		)
	,GETDATE()
	)

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_DebitNoteBalance;

INSERT INTO SKI_INT.Delta_DebitNoteBalance
SELECT DISTINCT iDebitNoteID
FROM DebitNoteBalance (NOLOCK)
WHERE CONVERT(CHAR(8), dUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_DebitNoteBalance'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_DebitNoteBalance'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_DebitNoteBalance(NOLOCK)
		)
	,GETDATE()
	)

    

GO
/****** Object:  StoredProcedure [SKI_INT].[SetDeltaValues_Sample]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[SetDeltaValues_Sample]
AS
----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Policy;

INSERT INTO SKI_INT.Delta_Policy
SELECT DISTINCT TOP 1000 iCustomerID
	,iPolicyID
	,iTranID
FROM Policy(NOLOCK)
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)

UNION

SELECT DISTINCT TOP 1000 ARCH_Policy.iCustomerID
	,ARCH_Policy.iPolicyID
	,ARCH_Policy.iTranID
FROM ARCH_Policy(NOLOCK)
INNER JOIN Policy Pol(NOLOCK) ON Pol.iPolicyID = ARCH_Policy.iPolicyID
WHERE CONVERT(CHAR(8), ARCH_Policy.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Policy'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Policy(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Customer;

INSERT INTO SKI_INT.Delta_Customer
SELECT DISTINCT Customer.iCustomerID
	,Customer.iTranID
FROM Customer(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Customer.iCustomerID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)

UNION

SELECT DISTINCT ARCH_Customer.iCustomerID
	,ARCH_Customer.iTranID
FROM ARCH_Customer(NOLOCK)
INNER JOIN Customer Cust(NOLOCK) ON Cust.iCustomerID = ARCH_Customer.iCustomerID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Customer.iCustomerID
WHERE CONVERT(CHAR(8), ARCH_Customer.dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Customer'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Customer(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Claims;

INSERT INTO SKI_INT.Delta_Claims
SELECT DISTINCT iClientID
	,Claims.iClaimID
	,Claims.iPolicyID
FROM Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)

UNION

SELECT DISTINCT ARCH_Claims.iClientID
	,ARCH_Claims.iClaimID
	,ARCH_Claims.iPolicyID
FROM ARCH_Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)
ORDER BY iClientID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Claims'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Claims(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimTransactions;

INSERT INTO SKI_INT.Delta_ClaimTransactions
SELECT DISTINCT ClaimTransactions.iClaimID
	,iTransactionID
	,iClaimantID
FROM ClaimTransactions(NOLOCK)
JOIN SKI_INT.Delta_Claims CC (NOLOCK)
  ON CC.iClaimID = ClaimTransactions.iClaimID
WHERE CONVERT(CHAR(8), dtTransaction, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_ClaimTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_PolicyTransactions;

INSERT INTO SKI_INT.Delta_PolicyTransactions
SELECT DISTINCT PolicyTransactions.iPolicyID
	,iTransactionID
FROM PolicyTransactions(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = PolicyTransactions.iPolicyId
WHERE CASE 
		WHEN dTransaction > dEffective
			THEN CONVERT(CHAR(8), dEffective, 112)
		ELSE CONVERT(CHAR(8), dTransaction, 112)
		END >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_PolicyTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_PolicyTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_PolicyTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimFloatPaymentTransactions;

INSERT INTO SKI_INT.Delta_ClaimFloatPaymentTransactions
SELECT 
  cl.ClaimFloatTransactionPayment.FloatTranID
FROM cl.ClaimFloatTransactionPayment (NOLOCK)
WHERE DateLastUpdated >= (
		                  SELECT MAX(LastRunDate)
		                  FROM SKI_INT.EDW_RunStatus (NOLOCK)
		                  WHERE TableName = 'Delta_ClaimFloatPayment'
		                  )

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimFloatPayment'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimFloatPaymentTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Risks;

INSERT INTO SKI_INT.Delta_Risks
SELECT DISTINCT intPolicyID
	,intRiskID
	,Risks.iTranID
FROM Risks(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = Risks.intPolicyID
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

UNION

SELECT DISTINCT ARCH_Risks.intPolicyID
	,ARCH_Risks.intRiskID
	,ARCH_Risks.iTranID
FROM ARCH_Risks(NOLOCK)
INNER JOIN Risks RR(NOLOCK) ON RR.intRiskID = ARCH_Risks.intRiskID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = RR.intPolicyID
WHERE CONVERT(CHAR(8), ARCH_Risks.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Risks'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Risks(NOLOCK)
		)
	,GETDATE()
	)

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_DebitNoteBalance;

INSERT INTO SKI_INT.Delta_DebitNoteBalance
SELECT DISTINCT iDebitNoteID
FROM DebitNoteBalance (NOLOCK)
WHERE CONVERT(CHAR(8), dUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_DebitNoteBalance'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_DebitNoteBalance'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_DebitNoteBalance(NOLOCK)
		)
	,GETDATE()
	)

    

GO
/****** Object:  StoredProcedure [SKI_INT].[UspINT_SKiSTGIntegrationCreateTable]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationCreateTable] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE @SQLCommand VARCHAR(2000)

	SET @SQLCommand = 'CREATE TABLE ' + N'' + @TableName + '([iID] [INT] IDENTITY(1,1) NOT NULL, [sDataRecord] [VARCHAR](MAX) NULL, [dtDate] [DATETIME] NULL, [sFileName] [VARCHAR](250) NULL, [iProcessStatus] [INT] NULL)'

	EXEC (@SQLCommand);
END


GO
/****** Object:  StoredProcedure [SKI_INT].[UspINT_SKiSTGIntegrationDropTable]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationDropTable] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE @SQLCommand VARCHAR(2000)

	SET @SQLCommand = 'DROP TABLE ' + N'' + @TableName

	EXEC (@SQLCommand);
END


GO
/****** Object:  Table [SKI_INT].[Delta_ClaimFloatPaymentTransactions]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_ClaimFloatPaymentTransactions](
	[FloatTranID] [uniqueidentifier] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_Claims]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_Claims](
	[iClientID] [float] NULL,
	[iClaimID] [float] NOT NULL,
	[iPolicyID] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_ClaimTransactions]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_ClaimTransactions](
	[iClaimID] [float] NULL,
	[iTransactionID] [float] NOT NULL,
	[iClaimantID] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_Customer]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_Customer](
	[iCustomerID] [int] NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_DebitNoteBalance]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_DebitNoteBalance](
	[iDebitNoteID] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_Policy]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_Policy](
	[iCustomerID] [int] NULL,
	[iPolicyID] [int] NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_PolicyTransactions]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_PolicyTransactions](
	[iPolicyID] [float] NOT NULL,
	[iTransactionID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[Delta_Risks]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[Delta_Risks](
	[iPolicyID] [float] NOT NULL,
	[intRiskID] [float] NOT NULL,
	[iTranID] [float] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[EDW_Address]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Address](
	[icustomerid] [bigint] NULL,
	[intAddressID] [bigint] NULL,
	[strAddressType] [varchar](10) NULL,
	[strAddressLine1] [varchar](255) NULL,
	[strAddressLine2] [varchar](255) NULL,
	[strAddressLine3] [varchar](255) NULL,
	[strSuburb] [varchar](255) NULL,
	[strPostalCode] [varchar](10) NULL,
	[strRatingArea] [varchar](255) NULL,
	[strPhoneNo] [varchar](30) NULL,
	[strFaxNo] [varchar](30) NULL,
	[strMobileNo] [varchar](30) NULL,
	[strEmailAdd] [varchar](100) NULL,
	[dUpdated] [datetime] NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[sTown] [varchar](255) NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_Claimants]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Claimants](
	[iClaimantID] [bigint] NULL,
	[iClaimID] [bigint] NULL,
	[sRelationCode] [varchar](255) NULL,
	[sRelationCode_Desc] [varchar](100) NULL,
	[sClaimantName] [varchar](255) NULL,
	[iClaimantAddressID] [bigint] NULL,
	[sDescription] [varchar](255) NULL,
	[cAmtClaimed] [money] NULL,
	[cTotExcess] [money] NULL,
	[cExcessRecovered] [money] NULL,
	[cAmountPaid] [money] NULL,
	[cRecovered] [money] NULL,
	[sAccountHolder] [varchar](255) NULL,
	[sAccountNumber] [varchar](255) NULL,
	[sBranchCode] [varchar](20) NULL,
	[sBankName] [varchar](255) NULL,
	[sCCAccountNumber] [varchar](255) NULL,
	[sCCExpiryDate] [datetime] NULL,
	[sCCControlDigits] [varchar](255) NULL,
	[bCreditCard] [varchar](255) NULL,
	[sCreditCardType] [varchar](255) NULL,
	[sAccType] [varchar](255) NULL,
	[sVATType] [varchar](255) NULL,
	[sVATNo] [varchar](255) NULL,
	[sStatus] [varchar](255) NULL,
	[bExcludeCLAmt] [varchar](255) NULL,
	[sLinkFilename] [varchar](255) NULL,
	[cSalvage] [money] NULL,
	[iVendorID] [varchar](255) NULL,
	[sFinAccount] [varchar](255) NULL,
	[sExternalID] [bigint] NULL,
	[ClaimCategory] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_ClaimCatastrophe]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_ClaimCatastrophe](
	[CatastropheID] [uniqueidentifier] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Description] [varchar](250) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AffectedArea] [varchar](250) NOT NULL,
	[DateLastUpdated] [datetime] NOT NULL,
	[UserLastUpdated] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_ClaimDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_ClaimDetails](
	[iClaimID] [bigint] NOT NULL,
	[dLastUpdated] [datetime] NULL,
	[CL_THICLA] [varchar](255) NULL,
	[CL_PBEATER] [varchar](255) NULL,
	[CL_BACKOFFINEG] [varchar](255) NULL,
	[CL_CHK_243] [varchar](255) NULL,
	[CL_CHK_000] [varchar](255) NULL,
	[CL_CHK_002] [varchar](255) NULL,
	[CL_CHK_003] [varchar](255) NULL,
	[CL_REPUDI] [varchar](255) NULL,
	[CL_REPUDE] [varchar](255) NULL,
	[CL_VEHDRI] [varchar](255) NULL,
	[CL_DATOIN] [varchar](255) NULL,
	[CL_DATOID] [varchar](255) NULL,
	[CL_CHK_010] [varchar](255) NULL,
	[CL_APPDATE] [varchar](255) NULL,
	[CL_CHK_011] [varchar](255) NULL,
	[CL_CHK_012] [varchar](255) NULL,
	[CL_CHK_013] [varchar](255) NULL,
	[CL_CHK_014] [varchar](255) NULL,
	[CL_CHK_015] [varchar](255) NULL,
	[CL_CHK_016] [varchar](255) NULL,
	[CL_CHK_240] [varchar](255) NULL,
	[CL_CHK_215] [varchar](255) NULL,
	[CL_CHK_216] [varchar](255) NULL,
	[CL_CHK_241] [varchar](255) NULL,
	[CL_CHK_017] [varchar](255) NULL,
	[CL_CHK_023] [varchar](255) NULL,
	[CL_CHK_024] [varchar](255) NULL,
	[CL_CHK_025] [varchar](255) NULL,
	[CL_CHK_027] [varchar](255) NULL,
	[CL_CHK_163] [varchar](255) NULL,
	[CL_CHK_028] [varchar](255) NULL,
	[CL_CHK_026] [varchar](255) NULL,
	[CL_CHK_029] [varchar](255) NULL,
	[CL_CHK_052] [varchar](255) NULL,
	[CL_CHK_092] [varchar](255) NULL,
	[CL_RECCLA] [varchar](255) NULL,
	[CL_CHK_094] [varchar](255) NULL,
	[CL_CHK_095] [varchar](255) NULL,
	[CL_CHK_098] [varchar](255) NULL,
	[CL_CHK_099] [varchar](255) NULL,
	[CL_CHK_100] [varchar](255) NULL,
	[CL_CHK_128] [varchar](255) NULL,
	[CL_CHK_129] [varchar](255) NULL,
	[CL_CHK_130] [varchar](255) NULL,
	[CL_CHK_131] [varchar](255) NULL,
	[CL_CHK_140] [varchar](255) NULL,
	[CL_CHK_200] [varchar](255) NULL,
	[CL_CHK_201] [varchar](255) NULL,
	[CL_CHK_202] [varchar](255) NULL,
	[CL_CHK_203] [varchar](255) NULL,
	[CL_CHK_219] [varchar](255) NULL,
	[CL_CHK_220] [varchar](255) NULL,
	[CL_CHK_222] [varchar](255) NULL,
	[CL_CHK_223] [varchar](255) NULL,
	[CL_INVAPP] [varchar](255) NULL,
	[CL_INVNAME] [varchar](255) NULL,
	[CL_CHK_237] [varchar](255) NULL,
	[CL_CHK_238] [varchar](255) NULL,
	[CL_CHK_239] [varchar](255) NULL,
	[CL_CHK_030] [varchar](255) NULL,
	[CL_CHK_031] [varchar](255) NULL,
	[CL_CHK_032] [varchar](255) NULL,
	[CL_CHK_224] [varchar](255) NULL,
	[CL_CHK_225] [varchar](255) NULL,
	[CL_CHK_033] [varchar](255) NULL,
	[CL_CHK_034] [varchar](255) NULL,
	[CL_CHK_0342] [varchar](255) NULL,
	[CL_CHK_0343] [varchar](255) NULL,
	[CL_CHK_0344] [varchar](255) NULL,
	[CL_CHK_0345] [varchar](255) NULL,
	[CL_WRTOFF] [varchar](255) NULL,
	[CL_WOCONFDTE] [varchar](255) NULL,
	[CL_STKNMBR] [varchar](255) NULL,
	[CL_SALAGREE] [varchar](255) NULL,
	[CL_SALUPLDATE] [varchar](255) NULL,
	[CL_INVOICED] [varchar](255) NULL,
	[CL_INVNO] [varchar](255) NULL,
	[CL_CHK_228] [varchar](255) NULL,
	[CL_CHK_229] [varchar](255) NULL,
	[CL_CHK_230] [varchar](255) NULL,
	[CL_CHK_247] [varchar](255) NULL,
	[CL_CHK_232] [varchar](255) NULL,
	[CL_CHK_233] [varchar](255) NULL,
	[CL_CHK_248] [varchar](255) NULL,
	[CL_CHK_235] [varchar](255) NULL,
	[CL_CHK_039] [varchar](255) NULL,
	[CL_CHK_102] [varchar](255) NULL,
	[CL_AGTNAME] [varchar](255) NULL,
	[CL_OSAGT] [varchar](255) NULL,
	[CL_OSAGTREFNO] [varchar](255) NULL,
	[CL_OSAGTAPPDATE] [varchar](255) NULL,
	[CL_CHK_106] [varchar](255) NULL,
	[CL_CHK_109] [varchar](255) NULL,
	[CL_CHK_111] [varchar](255) NULL,
	[CL_RECMER] [varchar](255) NULL,
	[CL_CHK_115] [varchar](255) NULL,
	[CL_CHK_116] [varchar](255) NULL,
	[CL_TPINSURID] [varchar](255) NULL,
	[CL_CHK_INSCONME] [varchar](255) NULL,
	[CL_CHK_123] [varchar](255) NULL,
	[CL_CHK_124] [varchar](255) NULL,
	[CL_CHK_125] [varchar](255) NULL,
	[CL_CHK_126] [varchar](255) NULL,
	[CL_CHK_127] [varchar](255) NULL,
	[CL_CHK_141] [varchar](255) NULL,
	[CL_CHK_145] [varchar](255) NULL,
	[CL_CHK_147] [varchar](255) NULL,
	[CL_CHK_162] [varchar](255) NULL,
	[CL_CHK_200_01] [varchar](255) NULL,
	[CL_RECCLA_01] [varchar](255) NULL,
	[CL_CHK_201_01] [varchar](255) NULL,
	[CL_CHK_094_01] [varchar](255) NULL,
	[CL_CHK_202_01] [varchar](255) NULL,
	[CL_CHK_095_01] [varchar](255) NULL,
	[CL_CHK_203_01] [varchar](255) NULL,
	[CL_CHK_098_01] [varchar](255) NULL,
	[CL_CHK_099_01] [varchar](255) NULL,
	[CL_CHK_100_01] [varchar](255) NULL,
	[CL_CHK_128_01] [varchar](255) NULL,
	[CL_CHK_129_01] [varchar](255) NULL,
	[CL_CHK_130_01] [varchar](255) NULL,
	[CL_CHK_131_01] [varchar](255) NULL,
	[CL_CHK_140_01] [varchar](255) NULL,
	[CL_CHK_219_01] [varchar](255) NULL,
	[CL_CHK_220_01] [varchar](255) NULL,
	[CL_CHK_222_01] [varchar](255) NULL,
	[CL_CHK_223_01] [varchar](255) NULL,
	[CL_CHK_102_01] [varchar](255) NULL,
	[CL_AGTNAME_01] [varchar](255) NULL,
	[CL_OSAGT_01] [varchar](255) NULL,
	[CL_OSAGTREFN_01] [varchar](255) NULL,
	[CL_OSAGTAPPD_01] [varchar](255) NULL,
	[CL_CHK_106_01] [varchar](255) NULL,
	[CL_CHK_109_01] [varchar](255) NULL,
	[CL_CHK_111_01] [varchar](255) NULL,
	[CL_RECMER_01] [varchar](255) NULL,
	[CL_CHK_115_01] [varchar](255) NULL,
	[CL_CHK_116_01] [varchar](255) NULL,
	[CL_TPINSURID_01] [varchar](255) NULL,
	[CL_CHK_INSCO_01] [varchar](255) NULL,
	[CL_CHK_123_01] [varchar](255) NULL,
	[CL_CHK_124_01] [varchar](255) NULL,
	[CL_CHK_125_01] [varchar](255) NULL,
	[CL_CHK_126_01] [varchar](255) NULL,
	[CL_CHK_127_01] [varchar](255) NULL,
	[CL_CHK_141_01] [varchar](255) NULL,
	[CL_CHK_145_01] [varchar](255) NULL,
	[CL_CHK_147_01] [varchar](255) NULL,
	[CL_CHK_162_01] [varchar](255) NULL,
	[CL_CHK_200_02] [varchar](255) NULL,
	[CL_RECCLA_02] [varchar](255) NULL,
	[CL_CHK_201_02] [varchar](255) NULL,
	[CL_CHK_094_02] [varchar](255) NULL,
	[CL_CHK_202_02] [varchar](255) NULL,
	[CL_CHK_095_02] [varchar](255) NULL,
	[CL_CHK_203_02] [varchar](255) NULL,
	[CL_CHK_098_02] [varchar](255) NULL,
	[CL_CHK_099_02] [varchar](255) NULL,
	[CL_CHK_100_02] [varchar](255) NULL,
	[CL_CHK_128_02] [varchar](255) NULL,
	[CL_CHK_129_02] [varchar](255) NULL,
	[CL_CHK_130_02] [varchar](255) NULL,
	[CL_CHK_131_02] [varchar](255) NULL,
	[CL_CHK_140_02] [varchar](255) NULL,
	[CL_CHK_219_02] [varchar](255) NULL,
	[CL_CHK_220_02] [varchar](255) NULL,
	[CL_CHK_222_02] [varchar](255) NULL,
	[CL_CHK_223_02] [varchar](255) NULL,
	[CL_CHK_102_02] [varchar](255) NULL,
	[CL_AGTNAME_02] [varchar](255) NULL,
	[CL_OSAGT_02] [varchar](255) NULL,
	[CL_OSAGTREFN_02] [varchar](255) NULL,
	[CL_OSAGTAPPD_02] [varchar](255) NULL,
	[CL_CHK_106_02] [varchar](255) NULL,
	[CL_CHK_109_02] [varchar](255) NULL,
	[CL_CHK_111_02] [varchar](255) NULL,
	[CL_RECMER_02] [varchar](255) NULL,
	[CL_CHK_115_02] [varchar](255) NULL,
	[CL_CHK_116_02] [varchar](255) NULL,
	[CL_TPINSURID_02] [varchar](255) NULL,
	[CL_CHK_INSCO_02] [varchar](255) NULL,
	[CL_CHK_123_02] [varchar](255) NULL,
	[CL_CHK_124_02] [varchar](255) NULL,
	[CL_CHK_125_02] [varchar](255) NULL,
	[CL_CHK_126_02] [varchar](255) NULL,
	[CL_CHK_127_02] [varchar](255) NULL,
	[CL_CHK_141_02] [varchar](255) NULL,
	[CL_CHK_145_02] [varchar](255) NULL,
	[CL_CHK_147_02] [varchar](255) NULL,
	[CL_CHK_162_02] [varchar](255) NULL,
	[CL_CHK_200_03] [varchar](255) NULL,
	[CL_RECCLA_03] [varchar](255) NULL,
	[CL_CHK_201_03] [varchar](255) NULL,
	[CL_CHK_094_03] [varchar](255) NULL,
	[CL_CHK_202_03] [varchar](255) NULL,
	[CL_CHK_095_03] [varchar](255) NULL,
	[CL_CHK_203_03] [varchar](255) NULL,
	[CL_CHK_098_03] [varchar](255) NULL,
	[CL_CHK_099_03] [varchar](255) NULL,
	[CL_CHK_100_03] [varchar](255) NULL,
	[CL_CHK_128_03] [varchar](255) NULL,
	[CL_CHK_129_03] [varchar](255) NULL,
	[CL_CHK_130_03] [varchar](255) NULL,
	[CL_CHK_131_03] [varchar](255) NULL,
	[CL_CHK_140_03] [varchar](255) NULL,
	[CL_CHK_219_03] [varchar](255) NULL,
	[CL_CHK_220_03] [varchar](255) NULL,
	[CL_CHK_222_03] [varchar](255) NULL,
	[CL_CHK_223_03] [varchar](255) NULL,
	[CL_CHK_102_03] [varchar](255) NULL,
	[CL_AGTNAME_03] [varchar](255) NULL,
	[CL_OSAGT_03] [varchar](255) NULL,
	[CL_OSAGTREFN_03] [varchar](255) NULL,
	[CL_OSAGTAPPD_03] [varchar](255) NULL,
	[CL_CHK_106_03] [varchar](255) NULL,
	[CL_CHK_109_03] [varchar](255) NULL,
	[CL_CHK_111_03] [varchar](255) NULL,
	[CL_RECMER_03] [varchar](255) NULL,
	[CL_CHK_115_03] [varchar](255) NULL,
	[CL_CHK_116_03] [varchar](255) NULL,
	[CL_TPINSURID_03] [varchar](255) NULL,
	[CL_CHK_INSCO_03] [varchar](255) NULL,
	[CL_CHK_123_03] [varchar](255) NULL,
	[CL_CHK_124_03] [varchar](255) NULL,
	[CL_CHK_125_03] [varchar](255) NULL,
	[CL_CHK_126_03] [varchar](255) NULL,
	[CL_CHK_127_03] [varchar](255) NULL,
	[CL_CHK_141_03] [varchar](255) NULL,
	[CL_CHK_145_03] [varchar](255) NULL,
	[CL_CHK_147_03] [varchar](255) NULL,
	[CL_CHK_162_03] [varchar](255) NULL,
	[CL_CHK_200_04] [varchar](255) NULL,
	[CL_RECCLA_04] [varchar](255) NULL,
	[CL_CHK_201_04] [varchar](255) NULL,
	[CL_CHK_094_04] [varchar](255) NULL,
	[CL_CHK_202_04] [varchar](255) NULL,
	[CL_CHK_095_04] [varchar](255) NULL,
	[CL_CHK_203_04] [varchar](255) NULL,
	[CL_CHK_098_04] [varchar](255) NULL,
	[CL_CHK_099_04] [varchar](255) NULL,
	[CL_CHK_100_04] [varchar](255) NULL,
	[CL_CHK_128_04] [varchar](255) NULL,
	[CL_CHK_129_04] [varchar](255) NULL,
	[CL_CHK_130_04] [varchar](255) NULL,
	[CL_CHK_131_04] [varchar](255) NULL,
	[CL_CHK_140_04] [varchar](255) NULL,
	[CL_CHK_219_04] [varchar](255) NULL,
	[CL_CHK_220_04] [varchar](255) NULL,
	[CL_CHK_222_04] [varchar](255) NULL,
	[CL_CHK_223_04] [varchar](255) NULL,
	[CL_CHK_102_04] [varchar](255) NULL,
	[CL_AGTNAME_04] [varchar](255) NULL,
	[CL_OSAGT_04] [varchar](255) NULL,
	[CL_OSAGTREFN_04] [varchar](255) NULL,
	[CL_OSAGTAPPD_04] [varchar](255) NULL,
	[CL_CHK_106_04] [varchar](255) NULL,
	[CL_CHK_109_04] [varchar](255) NULL,
	[CL_CHK_111_04] [varchar](255) NULL,
	[CL_RECMER_04] [varchar](255) NULL,
	[CL_CHK_115_04] [varchar](255) NULL,
	[CL_CHK_116_04] [varchar](255) NULL,
	[CL_TPINSURID_04] [varchar](255) NULL,
	[CL_CHK_INSCO_04] [varchar](255) NULL,
	[CL_CHK_123_04] [varchar](255) NULL,
	[CL_CHK_124_04] [varchar](255) NULL,
	[CL_CHK_125_04] [varchar](255) NULL,
	[CL_CHK_126_04] [varchar](255) NULL,
	[CL_CHK_127_04] [varchar](255) NULL,
	[CL_CHK_141_04] [varchar](255) NULL,
	[CL_CHK_145_04] [varchar](255) NULL,
	[CL_CHK_147_04] [varchar](255) NULL,
	[CL_CHK_162_04] [varchar](255) NULL,
	[CL_CHK_200_05] [varchar](255) NULL,
	[CL_RECCLA_05] [varchar](255) NULL,
	[CL_CHK_201_05] [varchar](255) NULL,
	[CL_CHK_094_05] [varchar](255) NULL,
	[CL_CHK_202_05] [varchar](255) NULL,
	[CL_CHK_095_05] [varchar](255) NULL,
	[CL_CHK_203_05] [varchar](255) NULL,
	[CL_CHK_098_05] [varchar](255) NULL,
	[CL_CHK_099_05] [varchar](255) NULL,
	[CL_CHK_100_05] [varchar](255) NULL,
	[CL_CHK_128_05] [varchar](255) NULL,
	[CL_CHK_129_05] [varchar](255) NULL,
	[CL_CHK_130_05] [varchar](255) NULL,
	[CL_CHK_131_05] [varchar](255) NULL,
	[CL_CHK_140_05] [varchar](255) NULL,
	[CL_CHK_219_05] [varchar](255) NULL,
	[CL_CHK_220_05] [varchar](255) NULL,
	[CL_CHK_222_05] [varchar](255) NULL,
	[CL_CHK_223_05] [varchar](255) NULL,
	[CL_CHK_102_05] [varchar](255) NULL,
	[CL_AGTNAME_05] [varchar](255) NULL,
	[CL_OSAGT_05] [varchar](255) NULL,
	[CL_OSAGTREFN_05] [varchar](255) NULL,
	[CL_OSAGTAPPD_05] [varchar](255) NULL,
	[CL_CHK_106_05] [varchar](255) NULL,
	[CL_CHK_109_05] [varchar](255) NULL,
	[CL_CHK_111_05] [varchar](255) NULL,
	[CL_RECMER_05] [varchar](255) NULL,
	[CL_CHK_115_05] [varchar](255) NULL,
	[CL_CHK_116_05] [varchar](255) NULL,
	[CL_TPINSURID_05] [varchar](255) NULL,
	[CL_CHK_INSCO_05] [varchar](255) NULL,
	[CL_CHK_123_05] [varchar](255) NULL,
	[CL_CHK_124_05] [varchar](255) NULL,
	[CL_CHK_125_05] [varchar](255) NULL,
	[CL_CHK_126_05] [varchar](255) NULL,
	[CL_CHK_127_05] [varchar](255) NULL,
	[CL_CHK_141_05] [varchar](255) NULL,
	[CL_CHK_145_05] [varchar](255) NULL,
	[CL_CHK_147_05] [varchar](255) NULL,
	[CL_CHK_162_05] [varchar](255) NULL,
	[CL_LEGALINV] [varchar](255) NULL,
	[CL_CHK_211] [varchar](255) NULL,
	[CL_CHK_212] [varchar](255) NULL,
	[CL_CHK_213] [varchar](255) NULL,
	[CL_CHK_214] [varchar](255) NULL,
	[CL_AGTNAME_ID] [varchar](50) NULL,
	[CL_AGTNAME_01_ID] [varchar](50) NULL,
	[CL_AGTNAME_02_ID] [varchar](50) NULL,
	[CL_AGTNAME_03_ID] [varchar](50) NULL,
	[CL_AGTNAME_04_ID] [varchar](50) NULL,
	[CL_AGTNAME_05_ID] [varchar](50) NULL,
	[CL_BACKOFFINEG_ID] [varchar](50) NULL,
	[CL_CHK_000_ID] [varchar](50) NULL,
	[CL_CHK_010_ID] [varchar](50) NULL,
	[CL_CHK_011_ID] [varchar](50) NULL,
	[CL_CHK_012_ID] [varchar](50) NULL,
	[CL_CHK_013_ID] [varchar](50) NULL,
	[CL_CHK_017_ID] [varchar](50) NULL,
	[CL_CHK_023_ID] [varchar](50) NULL,
	[CL_CHK_024_ID] [varchar](50) NULL,
	[CL_CHK_027_ID] [varchar](50) NULL,
	[CL_CHK_030_ID] [varchar](50) NULL,
	[CL_CHK_031_ID] [varchar](50) NULL,
	[CL_CHK_033_ID] [varchar](50) NULL,
	[CL_CHK_034_ID] [varchar](50) NULL,
	[CL_CHK_0342_ID] [varchar](50) NULL,
	[CL_CHK_0343_ID] [varchar](50) NULL,
	[CL_CHK_0344_ID] [varchar](50) NULL,
	[CL_CHK_0345_ID] [varchar](50) NULL,
	[CL_CHK_039_ID] [varchar](50) NULL,
	[CL_CHK_052_ID] [varchar](50) NULL,
	[CL_CHK_092_ID] [varchar](50) NULL,
	[CL_CHK_094_ID] [varchar](50) NULL,
	[CL_CHK_094_01_ID] [varchar](50) NULL,
	[CL_CHK_094_02_ID] [varchar](50) NULL,
	[CL_CHK_094_03_ID] [varchar](50) NULL,
	[CL_CHK_094_04_ID] [varchar](50) NULL,
	[CL_CHK_094_05_ID] [varchar](50) NULL,
	[CL_CHK_095_ID] [varchar](50) NULL,
	[CL_CHK_095_01_ID] [varchar](50) NULL,
	[CL_CHK_095_02_ID] [varchar](50) NULL,
	[CL_CHK_095_03_ID] [varchar](50) NULL,
	[CL_CHK_095_04_ID] [varchar](50) NULL,
	[CL_CHK_095_05_ID] [varchar](50) NULL,
	[CL_CHK_102_ID] [varchar](50) NULL,
	[CL_CHK_102_01_ID] [varchar](50) NULL,
	[CL_CHK_102_02_ID] [varchar](50) NULL,
	[CL_CHK_102_03_ID] [varchar](50) NULL,
	[CL_CHK_102_04_ID] [varchar](50) NULL,
	[CL_CHK_102_05_ID] [varchar](50) NULL,
	[CL_CHK_106_ID] [varchar](50) NULL,
	[CL_CHK_106_01_ID] [varchar](50) NULL,
	[CL_CHK_106_02_ID] [varchar](50) NULL,
	[CL_CHK_106_03_ID] [varchar](50) NULL,
	[CL_CHK_106_04_ID] [varchar](50) NULL,
	[CL_CHK_106_05_ID] [varchar](50) NULL,
	[CL_CHK_109_ID] [varchar](50) NULL,
	[CL_CHK_109_01_ID] [varchar](50) NULL,
	[CL_CHK_109_02_ID] [varchar](50) NULL,
	[CL_CHK_109_03_ID] [varchar](50) NULL,
	[CL_CHK_109_04_ID] [varchar](50) NULL,
	[CL_CHK_109_05_ID] [varchar](50) NULL,
	[CL_CHK_111_ID] [varchar](50) NULL,
	[CL_CHK_111_01_ID] [varchar](50) NULL,
	[CL_CHK_111_02_ID] [varchar](50) NULL,
	[CL_CHK_111_03_ID] [varchar](50) NULL,
	[CL_CHK_111_04_ID] [varchar](50) NULL,
	[CL_CHK_111_05_ID] [varchar](50) NULL,
	[CL_CHK_116_ID] [varchar](50) NULL,
	[CL_CHK_116_01_ID] [varchar](50) NULL,
	[CL_CHK_116_02_ID] [varchar](50) NULL,
	[CL_CHK_116_03_ID] [varchar](50) NULL,
	[CL_CHK_116_04_ID] [varchar](50) NULL,
	[CL_CHK_116_05_ID] [varchar](50) NULL,
	[CL_CHK_131_ID] [varchar](50) NULL,
	[CL_CHK_131_01_ID] [varchar](50) NULL,
	[CL_CHK_131_02_ID] [varchar](50) NULL,
	[CL_CHK_131_03_ID] [varchar](50) NULL,
	[CL_CHK_131_04_ID] [varchar](50) NULL,
	[CL_CHK_131_05_ID] [varchar](50) NULL,
	[CL_CHK_141_ID] [varchar](50) NULL,
	[CL_CHK_141_01_ID] [varchar](50) NULL,
	[CL_CHK_141_02_ID] [varchar](50) NULL,
	[CL_CHK_141_03_ID] [varchar](50) NULL,
	[CL_CHK_141_04_ID] [varchar](50) NULL,
	[CL_CHK_141_05_ID] [varchar](50) NULL,
	[CL_CHK_147_ID] [varchar](50) NULL,
	[CL_CHK_147_01_ID] [varchar](50) NULL,
	[CL_CHK_147_02_ID] [varchar](50) NULL,
	[CL_CHK_147_03_ID] [varchar](50) NULL,
	[CL_CHK_147_04_ID] [varchar](50) NULL,
	[CL_CHK_147_05_ID] [varchar](50) NULL,
	[CL_CHK_162_ID] [varchar](50) NULL,
	[CL_CHK_162_01_ID] [varchar](50) NULL,
	[CL_CHK_162_02_ID] [varchar](50) NULL,
	[CL_CHK_162_03_ID] [varchar](50) NULL,
	[CL_CHK_162_04_ID] [varchar](50) NULL,
	[CL_CHK_162_05_ID] [varchar](50) NULL,
	[CL_CHK_163_ID] [varchar](50) NULL,
	[CL_CHK_200_ID] [varchar](50) NULL,
	[CL_CHK_200_01_ID] [varchar](50) NULL,
	[CL_CHK_200_02_ID] [varchar](50) NULL,
	[CL_CHK_200_03_ID] [varchar](50) NULL,
	[CL_CHK_200_04_ID] [varchar](50) NULL,
	[CL_CHK_200_05_ID] [varchar](50) NULL,
	[CL_CHK_201_ID] [varchar](50) NULL,
	[CL_CHK_201_01_ID] [varchar](50) NULL,
	[CL_CHK_201_02_ID] [varchar](50) NULL,
	[CL_CHK_201_03_ID] [varchar](50) NULL,
	[CL_CHK_201_04_ID] [varchar](50) NULL,
	[CL_CHK_201_05_ID] [varchar](50) NULL,
	[CL_CHK_211_ID] [varchar](50) NULL,
	[CL_CHK_212_ID] [varchar](50) NULL,
	[CL_CHK_213_ID] [varchar](50) NULL,
	[CL_CHK_214_ID] [varchar](50) NULL,
	[CL_CHK_215_ID] [varchar](50) NULL,
	[CL_CHK_219_ID] [varchar](50) NULL,
	[CL_CHK_219_01_ID] [varchar](50) NULL,
	[CL_CHK_219_02_ID] [varchar](50) NULL,
	[CL_CHK_219_03_ID] [varchar](50) NULL,
	[CL_CHK_219_04_ID] [varchar](50) NULL,
	[CL_CHK_219_05_ID] [varchar](50) NULL,
	[CL_CHK_223_ID] [varchar](50) NULL,
	[CL_CHK_223_01_ID] [varchar](50) NULL,
	[CL_CHK_223_02_ID] [varchar](50) NULL,
	[CL_CHK_223_03_ID] [varchar](50) NULL,
	[CL_CHK_223_04_ID] [varchar](50) NULL,
	[CL_CHK_223_05_ID] [varchar](50) NULL,
	[CL_CHK_224_ID] [varchar](50) NULL,
	[CL_CHK_225_ID] [varchar](50) NULL,
	[CL_CHK_228_ID] [varchar](50) NULL,
	[CL_CHK_229_ID] [varchar](50) NULL,
	[CL_CHK_232_ID] [varchar](50) NULL,
	[CL_CHK_235_ID] [varchar](50) NULL,
	[CL_CHK_237_ID] [varchar](50) NULL,
	[CL_CHK_238_ID] [varchar](50) NULL,
	[CL_CHK_240_ID] [varchar](50) NULL,
	[CL_CHK_241_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_01_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_02_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_03_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_04_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_05_ID] [varchar](50) NULL,
	[CL_CHK_INSCONME_ID] [varchar](50) NULL,
	[CL_INVAPP_ID] [varchar](50) NULL,
	[CL_INVNAME_ID] [varchar](50) NULL,
	[CL_INVOICED_ID] [varchar](50) NULL,
	[CL_LEGALINV_ID] [varchar](50) NULL,
	[CL_OSAGT_ID] [varchar](50) NULL,
	[CL_OSAGT_01_ID] [varchar](50) NULL,
	[CL_OSAGT_02_ID] [varchar](50) NULL,
	[CL_OSAGT_03_ID] [varchar](50) NULL,
	[CL_OSAGT_04_ID] [varchar](50) NULL,
	[CL_OSAGT_05_ID] [varchar](50) NULL,
	[CL_RECCLA_ID] [varchar](50) NULL,
	[CL_RECCLA_01_ID] [varchar](50) NULL,
	[CL_RECCLA_02_ID] [varchar](50) NULL,
	[CL_RECCLA_03_ID] [varchar](50) NULL,
	[CL_RECCLA_04_ID] [varchar](50) NULL,
	[CL_RECCLA_05_ID] [varchar](50) NULL,
	[CL_RECMER_ID] [varchar](50) NULL,
	[CL_RECMER_01_ID] [varchar](50) NULL,
	[CL_RECMER_02_ID] [varchar](50) NULL,
	[CL_RECMER_03_ID] [varchar](50) NULL,
	[CL_RECMER_04_ID] [varchar](50) NULL,
	[CL_RECMER_05_ID] [varchar](50) NULL,
	[CL_REPUDI_ID] [varchar](50) NULL,
	[CL_SALAGREE_ID] [varchar](50) NULL,
	[CL_THICLA_ID] [varchar](50) NULL,
	[CL_TPINSURID_ID] [varchar](50) NULL,
	[CL_TPINSURID_01_ID] [varchar](50) NULL,
	[CL_TPINSURID_02_ID] [varchar](50) NULL,
	[CL_TPINSURID_03_ID] [varchar](50) NULL,
	[CL_TPINSURID_04_ID] [varchar](50) NULL,
	[CL_TPINSURID_05_ID] [varchar](50) NULL,
	[CL_VEHDRI_ID] [varchar](50) NULL,
	[CL_WRTOFF_ID] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_ClaimFloatPayments]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_ClaimFloatPayments](
	[iClaimID] [bigint] NULL,
	[iClaimantID] [bigint] NULL,
	[iTransactionID] [bigint] NULL,
	[sClaimantType] [varchar](255) NULL,
	[sClaimantName] [varchar](255) NULL,
	[sDescription] [varchar](255) NULL,
	[sPaymentMethod] [varchar](50) NULL,
	[Reference] [varchar](255) NULL,
	[TransactionDate] [datetime] NULL,
	[cAmount] [money] NULL,
	[sRequestedBy] [varchar](50) NULL,
	[AuthStatus] [tinyint] NULL,
	[AuthStatus_Desc] [varchar](50) NULL,
	[RequestApprovedBy] [varchar](50) NULL,
	[DateRequestApproved] [datetime] NULL,
	[PaymentAuthorizedBy] [varchar](50) NULL,
	[DatePaymentAuthorized] [datetime] NULL,
	[bReversed] [bit] NULL,
	[iParentPaymentID] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_Claims]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Claims](
	[iClaimID] [bigint] NULL,
	[iClientID] [bigint] NULL,
	[iRiskID] [bigint] NULL,
	[iPolicyID] [bigint] NULL,
	[sRefNo] [varchar](255) NULL,
	[sCauseCode] [varchar](255) NULL,
	[sCauseCode_Desc] [varchar](100) NULL,
	[sResponsibleName] [varchar](255) NULL,
	[sResponsibleSurname] [varchar](255) NULL,
	[dtEvent] [datetime] NULL,
	[sStatusCode] [varchar](255) NULL,
	[sStatusCode_Desc] [varchar](100) NULL,
	[dStatusChanged] [datetime] NULL,
	[sDescription] [varchar](255) NULL,
	[sComment] [varchar](255) NULL,
	[sApprovalCode] [varchar](255) NULL,
	[sApprovalCode_Desc] [varchar](100) NULL,
	[bClientResponsible] [varchar](255) NULL,
	[cAmtFirstClaimed] [money] NULL,
	[cAmtClaimed] [money] NULL,
	[cAmtPaid] [money] NULL,
	[cRecovered] [money] NULL,
	[cBalance] [money] NULL,
	[iIncidentAddressID] [bigint] NULL,
	[cTotExcess] [money] NULL,
	[cExcessRecovered] [money] NULL,
	[sAssignedTo] [varchar](255) NULL,
	[dtCreated] [datetime] NULL,
	[dLastUpdated] [datetime] NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[dtRegistered] [datetime] NULL,
	[cFirstExcessEst] [money] NULL,
	[cSalvage] [money] NULL,
	[cClaimFee] [money] NULL,
	[sSubCauseCode] [varchar](255) NULL,
	[sSubCauseCode_Desc] [varchar](100) NULL,
	[sFinAccount] [varchar](255) NULL,
	[dPolicyCoverStart] [datetime] NULL,
	[dPolicyCoverEnd] [datetime] NULL,
	[iMasterClaimID] [bigint] NULL,
	[BurningCostID] [varchar](255) NULL,
	[CorporateEntityID] [varchar](255) NULL,
	[CatastropheId] [varchar](255) NULL,
	[RIArrangementID] [varchar](255) NULL,
	[CompanyID] [bigint] NULL,
	[Company] [varchar](200) NULL,
	[BrokerID] [bigint] NULL,
	[Broker] [varchar](200) NULL,
	[Sub-AgentID] [bigint] NULL,
	[Sub-Agent] [varchar](200) NULL,
	[SumInsured] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_ClaimTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_ClaimTran](
	[iTransactionID] [bigint] NULL,
	[iClaimID] [bigint] NULL,
	[iClaimantID] [bigint] NULL,
	[dtTransaction] [datetime] NULL,
	[dtPeriodEnd] [datetime] NULL,
	[sTransactionCode] [varchar](255) NULL,
	[sTransactionCode_Desc] [varchar](50) NULL,
	[cValue] [money] NULL,
	[sReference] [varchar](255) NULL,
	[cVATValue] [money] NULL,
	[iPayMethod] [varchar](255) NULL,
	[PayMethod_Desc] [varchar](50) NULL,
	[sRequestedBy] [varchar](255) NULL,
	[bReversed] [int] NULL,
	[iParentPaymentID] [bigint] NULL,
	[dtPeriodStart] [datetime] NULL,
	[sFinAccount] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_ClaimVendors]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_ClaimVendors](
	[iVendorID] [int] NOT NULL,
	[VendorName] [varchar](255) NOT NULL,
	[VendorType_Desc] [varchar](50) NOT NULL,
	[VATRate] [varchar](50) NULL,
	[VatNo] [varchar](50) NULL,
	[sAddressLine1] [varchar](50) NULL,
	[sAddressLine2] [varchar](50) NULL,
	[sAddressLine3] [varchar](50) NULL,
	[sSuburb] [varchar](50) NULL,
	[sPostalCode] [varchar](13) NULL,
	[sPhoneNo] [varchar](30) NULL,
	[sFaxNo] [varchar](30) NULL,
	[sEmail] [varchar](50) NULL,
	[dLastUpdated] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_Customer]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Customer](
	[iCustomerID] [bigint] NULL,
	[iAgentID] [int] NULL,
	[sAgent_Desc] [varchar](200) NULL,
	[sLastName] [varchar](255) NULL,
	[sFirstName] [varchar](255) NULL,
	[sInitials] [varchar](255) NULL,
	[dDateCreated] [datetime] NULL,
	[dLastUpdated] [datetime] NULL,
	[bCompanyInd] [varchar](20) NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[rPostAddressID] [bigint] NULL,
	[rPhysAddressID] [bigint] NULL,
	[rWorkAddressID] [bigint] NULL,
	[sCompanyName] [varchar](255) NULL,
	[sContactName] [varchar](255) NULL,
	[iTranID] [bigint] NULL,
	[sGroupRelationShip] [varchar](255) NULL,
	[sLanguage] [varchar](255) NULL,
	[sLanguage_Desc] [varchar](50) NULL,
	[CountryId] [varchar](255) NULL,
	[Country_Desc] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_CustomerDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_CustomerDetails](
	[iCustomerID] [bigint] NULL,
	[dLastUpdated] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[C_CELLNO] [varchar](255) NULL,
	[C_CONTDETAILS] [varchar](255) NULL,
	[C_DOB] [varchar](255) NULL,
	[C_EMAIL] [varchar](255) NULL,
	[C_FAXTEL] [varchar](255) NULL,
	[C_GENDER] [varchar](255) NULL,
	[C_HOMETEL] [varchar](255) NULL,
	[C_IDNO] [varchar](255) NULL,
	[C_TITLE_ID] [varchar](20) NULL,
	[C_TITLE] [varchar](255) NULL,
	[C_VIPCLIENT_ID] [varchar](50) NULL,
	[C_VIPCLIENT] [varchar](255) NULL,
	[C_WORKTEL] [varchar](255) NULL,
	[CH_INITIALS] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_DebitNoteBalance]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_DebitNoteBalance](
	[iDebitNoteId] [float] NOT NULL,
	[cPremiumBal] [money] NOT NULL,
	[cCommissionBal] [money] NOT NULL,
	[cAdminfeeBal] [money] NOT NULL,
	[cSASRIABal] [money] NOT NULL,
	[dUpdated] [datetime] NOT NULL,
	[sUpdatedBy] [varchar](50) NOT NULL,
	[bReplicated] [bit] NOT NULL,
	[cPolicyFeeBal] [float] NOT NULL,
	[iPolicyID] [float] NOT NULL,
	[cBrokerFeeBal] [money] NOT NULL,
	[iAgentID] [float] NOT NULL,
	[iSubAgentID] [float] NOT NULL,
	[iInvoiceNo] [int] NOT NULL,
	[iEndorsementID] [uniqueidentifier] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_FieldCodes]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_FieldCodes](
	[sFieldCode] [varchar](15) NULL,
	[sFieldType] [varchar](255) NULL,
	[iFieldType] [int] NULL,
	[sFieldDescription] [varchar](255) NULL,
	[iSeq] [int] NULL,
	[sFieldUsage] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_Policy]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Policy](
	[iPolicyID] [bigint] NULL,
	[iCustomerID] [bigint] NULL,
	[iProductID] [int] NULL,
	[sProduct_Desc] [varchar](50) NULL,
	[sPolicyNo] [varchar](255) NULL,
	[sOldPolicyNo] [varchar](255) NULL,
	[iPolicyStatus] [int] NULL,
	[iPolicyStatus_Desc] [varchar](255) NULL,
	[iPaymentStatus] [int] NULL,
	[iPaymentStatus_Desc] [varchar](255) NULL,
	[dRenewalDate] [datetime] NULL,
	[dLastPaymentDate] [datetime] NULL,
	[dNextPaymentDate] [datetime] NULL,
	[dExpiryDate] [datetime] NULL,
	[dStartDate] [datetime] NULL,
	[dDateCreated] [datetime] NULL,
	[iPaymentTerm] [int] NULL,
	[iPaymentTerm_Desc] [varchar](255) NULL,
	[cNextPaymentAmt] [money] NULL,
	[cAnnualPremium] [money] NULL,
	[iTaxPerc] [money] NULL,
	[iDiscountPerc] [money] NULL,
	[sComment] [varchar](255) NULL,
	[sAccHolderName] [varchar](255) NULL,
	[iPaymentMethod] [int] NULL,
	[iPaymentMethod_Desc] [varchar](255) NULL,
	[sBank] [varchar](255) NULL,
	[sActionedBy] [varchar](255) NULL,
	[iCommissionMethod] [int] NULL,
	[iCommissionMethod_Desc] [varchar](255) NULL,
	[cCommissionAmt] [money] NULL,
	[cCommissionPaid] [money] NULL,
	[sPolicyType] [varchar](50) NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[sBranchCode] [varchar](255) NULL,
	[sCheckDigit] [varchar](255) NULL,
	[sExpiryDate] [varchar](7) NULL,
	[sAccountNo] [varchar](255) NULL,
	[sAccountType] [varchar](255) NULL,
	[sCancelReason] [varchar](255) NULL,
	[sCreditCardType] [varchar](255) NULL,
	[dEffectiveDate] [datetime] NULL,
	[cAdminFee] [money] NULL,
	[iCollection] [int] NULL,
	[cAnnualSASRIA] [money] NULL,
	[cPolicyFee] [money] NULL,
	[iCurrency] [int] NULL,
	[iCurrency_Desc] [varchar](255) NULL,
	[cBrokerFee] [money] NULL,
	[iAdminType] [int] NULL,
	[sAdminType_Desc] [varchar](50) NULL,
	[cAdminPerc] [money] NULL,
	[cAdminMin] [money] NULL,
	[cAdminMax] [money] NULL,
	[iParentID] [bigint] NULL,
	[iTreatyYear] [int] NULL,
	[dCoverStart] [datetime] NULL,
	[sFinAccount] [varchar](255) NULL,
	[dCancelled] [datetime] NULL,
	[iCoverTerm] [int] NULL,
	[iCoverTerm_Desc] [varchar](255) NULL,
	[sEndorseCode] [varchar](255) NULL,
	[sEndorseCode_Desc] [varchar](255) NULL,
	[eCommCalcMethod] [int] NULL,
	[iCommPayLevel] [int] NULL,
	[iCommPayLevel_Desc] [varchar](255) NULL,
	[iIndemnityPeriod] [float] NULL,
	[iIndemnityPeriod_Desc] [varchar](255) NULL,
	[dReviewDate] [datetime] NULL,
	[sLanguage] [varchar](255) NULL,
	[sLanguage_Desc] [varchar](255) NULL,
	[dRenewalPrepDate] [datetime] NULL,
	[CancelReasonCode] [varchar](255) NULL,
	[CancelReasonCode_Desc] [varchar](255) NULL,
	[CountryId] [int] NULL,
	[Country_Desc] [varchar](60) NULL,
	[CashbackEnabled] [bit] NULL,
	[Lev1Name] [varchar](255) NULL,
	[Lev1Perc] [float] NULL,
	[Lev2Name] [varchar](255) NULL,
	[Lev2Perc] [float] NULL,
	[Lev3Name] [varchar](255) NULL,
	[Lev3Perc] [float] NULL,
	[Lev4Name] [varchar](255) NULL,
	[Lev4Perc] [float] NULL,
	[Lev5Name] [varchar](255) NULL,
	[Lev5Perc] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_PolicyClaimTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[EDW_PolicyClaimTran](
	[iClaimID] [bigint] NOT NULL,
	[AMT CLAIMED] [money] NULL,
	[EXCESS] [money] NULL,
	[SALVAGE] [money] NULL,
	[RECOVERY] [money] NULL,
	[PAID] [money] NULL,
	[BALANCE] [money] NULL,
	[Summary Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[EDW_PolicyDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_PolicyDetails](
	[iPolicyID] [bigint] NULL,
	[Date Modified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[P_ServiceProv_ID] [varchar](20) NULL,
	[P_ServiceProv] [varchar](255) NULL,
	[P_MARKETCODE] [varchar](255) NULL,
	[P_CASENO] [varchar](255) NULL,
	[P_MORE5VEH_ID] [varchar](20) NULL,
	[P_MORE5VEH] [varchar](255) NULL,
	[P_BANKPROTIND_ID] [varchar](20) NULL,
	[P_BANKPROTIND] [varchar](255) NULL,
	[P_BANKPROTPNO] [varchar](255) NULL,
	[P_PREMMATREAS_ID] [varchar](20) NULL,
	[P_PREMMATREAS] [varchar](255) NULL,
	[P_ADDINFO] [varchar](255) NULL,
	[P_B_REFACCH] [varchar](255) NULL,
	[P_B_REFACCN] [varchar](255) NULL,
	[P_B_REFBRNC] [varchar](255) NULL,
	[P_CWarning] [varchar](255) NULL,
	[P_LASTRD_DOREF] [varchar](255) NULL,
	[P_LASTRD_HEAD] [varchar](255) NULL,
	[P_LASTRD_REASON] [varchar](255) NULL,
	[P_ITCCOLOUR] [varchar](255) NULL,
	[P_QUALQUEST] [varchar](255) NULL,
	[P_UW] [varchar](255) NULL,
	[P_UW_Desc] [varchar](255) NULL,
	[P_LASTRD_AMT] [varchar](255) NULL,
	[P_EXISTDAMAMT] [varchar](255) NULL,
	[P_BROINITFEE] [varchar](255) NULL,
	[P_BROKERBANK] [varchar](255) NULL,
	[P_B_REFUND] [varchar](255) NULL,
	[P_OR_BROKERFEE] [varchar](255) NULL,
	[P_OR_LEGALADV] [varchar](255) NULL,
	[P_VIKELA] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[P_LASTRD_DATE] [varchar](255) NULL,
	[P_POLTYPE] [varchar](255) NULL,
	[P_POLTYPE_Desc] [varchar](255) NULL,
	[P_LICRESTRIC] [varchar](255) NULL,
	[P_LICRESTRIC_Desc] [varchar](255) NULL,
	[P_MORE2CLAIMS] [varchar](255) NULL,
	[P_MORE2CLAIMS_Desc] [varchar](255) NULL,
	[P_CASHBACKBONUS] [varchar](255) NULL,
	[P_CASHBACKBONUS_Desc] [varchar](255) NULL,
	[P_B_REFACCT] [varchar](255) NULL,
	[P_B_REFACCT_Desc] [varchar](255) NULL,
	[P_B_REFBANK] [varchar](255) NULL,
	[P_B_REFBANK_Desc] [varchar](255) NULL,
	[P_EXISTDAM] [varchar](255) NULL,
	[P_EXISTDAM_Desc] [varchar](255) NULL,
	[P_G_SOURCE] [varchar](255) NULL,
	[P_G_SOURCE_Desc] [varchar](255) NULL,
	[P_INSCANC] [varchar](255) NULL,
	[P_INSCANC_Desc] [varchar](255) NULL,
	[P_INSDECL] [varchar](255) NULL,
	[P_INSDECL_Desc] [varchar](255) NULL,
	[P_INSREFUSED] [varchar](255) NULL,
	[P_INSREFUSED_Desc] [varchar](255) NULL,
	[P_ISAFECASHBACK] [varchar](255) NULL,
	[P_ISAFECASHBACK_Desc] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_PolTran]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_PolTran](
	[iTransactionId] [bigint] NULL,
	[iPolicyId] [bigint] NULL,
	[sReference] [varchar](255) NULL,
	[iAgencyId] [bigint] NULL,
	[Agency_Desc] [varchar](200) NULL,
	[iTrantype] [int] NULL,
	[iTrantype_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cCommission] [money] NULL,
	[cAdminfee] [money] NULL,
	[cSASRIA] [money] NULL,
	[cSasriaCommission] [money] NULL,
	[cCommExcSasriaCommission] [money] NULL,
	[dTransaction] [datetime] NULL,
	[sUser] [varchar](255) NULL,
	[dPeriodStart] [datetime] NULL,
	[dPeriodEnd] [datetime] NULL,
	[iCommStatus] [varchar](255) NULL,
	[CommStatus_Desc] [varchar](100) NULL,
	[iParentTranID] [bigint] NULL,
	[iDebitNoteId] [bigint] NULL,
	[cPolicyFee] [money] NULL,
	[sComment] [varchar](255) NULL,
	[dEffective] [datetime] NULL,
	[cBrokerFee] [money] NULL,
	[sFinAccount] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_PolTranDet]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_PolTranDet](
	[iTransactionID] [bigint] NULL,
	[iPolicyID] [bigint] NULL,
	[iRiskID] [bigint] NULL,
	[iRiskTypeID] [int] NULL,
	[dDateActioned] [datetime] NULL,
	[iTranType] [int] NULL,
	[iTranType_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cCommission] [money] NULL,
	[cAdminFee] [money] NULL,
	[cSASRIA] [money] NULL,
	[sUser] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_PolTranSummary]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SKI_INT].[EDW_PolTranSummary](
	[PolicyID] [bigint] NULL,
	[Premium Bal] [money] NULL,
	[Commissoin Bal] [money] NULL,
	[SASRIA Bal] [money] NULL,
	[Admin Fee Bal] [money] NULL,
	[Policy Fee Bal] [money] NULL,
	[Broker Fee Bal] [money] NULL,
	[Summary Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [SKI_INT].[EDW_Products]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Products](
	[iProductID] [float] NOT NULL,
	[sProductName] [varchar](50) NOT NULL,
	[sProductCode] [varchar](10) NULL,
	[sInsurer] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_RiskDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_RiskDetails](
	[intPolicyID] [bigint] NULL,
	[iRiskID] [bigint] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[ZIPCODE] [varchar](255) NULL,
	[R_MOTORSEATS_ID] [varchar](50) NULL,
	[R_MOTORSEATS] [varchar](255) NULL,
	[R_MOTORYOM] [varchar](255) NULL,
	[R_MOTORREG] [varchar](255) NULL,
	[R_ENGIN_NO] [varchar](255) NULL,
	[R_CHASSISNO] [varchar](255) NULL,
	[R_COVERDETAILS] [varchar](255) NULL,
	[R_G_MARKETVAL] [varchar](255) NULL,
	[R_G_PASLIABIND] [varchar](255) NULL,
	[R_G_NUOFSEATS] [varchar](255) NULL,
	[R_G_HPLEASE] [varchar](255) NULL,
	[R_G_EXTENDLOU] [varchar](255) NULL,
	[R_G_PERSACCIND] [varchar](255) NULL,
	[R_G_CREDSFALL] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[R_G_ABSVIOCRE] [varchar](255) NULL,
	[R_G_ISAFE] [varchar](255) NULL,
	[R_G_EXCESSRO_ID] [varchar](50) NULL,
	[R_G_EXCESSRO] [varchar](255) NULL,
	[R_G_WINDSCREEN] [varchar](255) NULL,
	[R_G_BACK2INV_ID] [varchar](50) NULL,
	[R_G_BACK2INV] [varchar](255) NULL,
	[R_G_CASHBACK] [varchar](255) NULL,
	[R_G_DEALER_ID] [varchar](50) NULL,
	[R_G_DEALER] [varchar](255) NULL,
	[R_G_INTRSTNOTED] [varchar](255) NULL,
	[R_G_CREDITPRO_ID] [varchar](50) NULL,
	[R_G_CREDITPRO] [varchar](255) NULL,
	[R_G_CRACCNO] [varchar](255) NULL,
	[R_G_ASSOC_ID] [varchar](50) NULL,
	[R_G_ASSOC] [varchar](255) NULL,
	[R_G_STDENDORSE] [varchar](255) NULL,
	[R_G_ADDFAP2000] [varchar](255) NULL,
	[R_G_ADDFAP2500] [varchar](255) NULL,
	[R_G_ADDFAP5000] [varchar](255) NULL,
	[R_G_ADDFAP7500] [varchar](255) NULL,
	[R_G_ADDFAP10000] [varchar](255) NULL,
	[R_G_ADDFAP15000] [varchar](255) NULL,
	[R_G_ADDFAP20000] [varchar](255) NULL,
	[R_G_AMATEKSI] [varchar](255) NULL,
	[R_G_AMAACCNO] [varchar](255) NULL,
	[R_G_AMAIDNO] [varchar](255) NULL,
	[R_MATCHINGCT] [varchar](255) NULL,
	[R_CTPERACC] [varchar](255) NULL,
	[R_CTHPLEASE] [varchar](255) NULL,
	[R_CTPASS] [varchar](255) NULL,
	[R_CTEXTENDLOU] [varchar](255) NULL,
	[R_CTHELIVAC] [varchar](255) NULL,
	[R_CTAMACLA] [varchar](255) NULL,
	[R_CTBACK2INV] [varchar](255) NULL,
	[R_CTISAFE] [varchar](255) NULL,
	[R_CTCASHBACK] [varchar](255) NULL,
	[R_CTCREDITSF] [varchar](255) NULL,
	[R_CTEXCESSRO] [varchar](255) NULL,
	[R_TSASRIA] [varchar](255) NULL,
	[R_MATCHINGPERC] [varchar](255) NULL,
	[R_MORE5VEH] [varchar](255) NULL,
	[R_SI] [varchar](255) NULL,
	[R_G_PASSENSI] [varchar](255) NULL,
	[R_G_PASSENDT] [varchar](255) NULL,
	[R_G_HPLEASESI] [varchar](255) NULL,
	[R_G_PERACCSI] [varchar](255) NULL,
	[R_G_EXTENDLOUSI] [varchar](255) NULL,
	[R_G_HELIVACSI] [varchar](255) NULL,
	[R_G_CSFSI] [varchar](255) NULL,
	[R_G_ABSVIOCRESI] [varchar](255) NULL,
	[R_G_ISAFESI] [varchar](255) NULL,
	[R_G_EXCESSROSI] [varchar](255) NULL,
	[R_G_WINDSCRSI] [varchar](255) NULL,
	[R_G_BACK2INVSI] [varchar](255) NULL,
	[R_G_CASHBACKSI] [varchar](255) NULL,
	[R_G_AMAPREMSI] [varchar](255) NULL,
	[R_G_VEHDEF_ID] [varchar](50) NULL,
	[R_G_VEHDEF] [varchar](255) NULL,
	[R_G_TRACKCUST_ID] [varchar](50) NULL,
	[R_G_TRACKCUST] [varchar](255) NULL,
	[R_G_TVEHDEF_ID] [varchar](50) NULL,
	[R_G_TVEHDEF] [varchar](255) NULL,
	[R_G_TRAILERLOI] [varchar](255) NULL,
	[R_G_LOANPRTPR] [varchar](255) NULL,
	[R_G_DRVDEATHSI] [varchar](255) NULL,
	[R_MATCHING] [varchar](255) NULL,
	[R_G_PASDEATHSI] [varchar](255) NULL,
	[R_G_TRACKREFNO] [varchar](255) NULL,
	[R_Driver] [varchar](255) NULL,
	[R_DriveName1] [varchar](255) NULL,
	[R_DriveID1] [varchar](255) NULL,
	[R_DriveCell1] [varchar](255) NULL,
	[R_DriveName2] [varchar](255) NULL,
	[R_DriveID2] [varchar](255) NULL,
	[R_DriveCell2] [varchar](255) NULL,
	[R_DriveName3] [varchar](255) NULL,
	[R_DriveID3] [varchar](255) NULL,
	[R_DriveCell3] [varchar](255) NULL,
	[R_DriveName4] [varchar](255) NULL,
	[R_DriveID4] [varchar](255) NULL,
	[R_DriveCell4] [varchar](255) NULL,
	[R_DriveName5] [varchar](255) NULL,
	[R_DriveID5] [varchar](255) NULL,
	[R_DriveCell5] [varchar](255) NULL,
	[R_DriveName6] [varchar](255) NULL,
	[R_DriveID6] [varchar](255) NULL,
	[R_DriveCell6] [varchar](255) NULL,
	[R_G_ADDFAPG1000] [varchar](255) NULL,
	[R_G_ADDFAPG2000] [varchar](255) NULL,
	[R_G_ADDFAPG3000] [varchar](255) NULL,
	[R_G_TPOSI] [varchar](255) NULL,
	[R_G_HPLEASEDT] [varchar](255) NULL,
	[R_G_EXTENDLOUDT] [varchar](255) NULL,
	[R_G_LOSSINOPSI] [varchar](255) NULL,
	[R_G_HELIVACDT] [varchar](255) NULL,
	[R_G_ISAFEDT] [varchar](255) NULL,
	[R_G_PERACCDT] [varchar](255) NULL,
	[R_G_CSFDT] [varchar](255) NULL,
	[R_G_ABSVIOCREDT] [varchar](255) NULL,
	[R_G_EXCESSRODT] [varchar](255) NULL,
	[R_G_WINDSCRDT] [varchar](255) NULL,
	[R_G_BACK2INVDT] [varchar](255) NULL,
	[R_G_CASHBACKDT] [varchar](255) NULL,
	[R_ISFCSHBCK_CPC] [varchar](255) NULL,
	[R_LOI_TOUR_CPC] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_Risks]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_Risks](
	[intClientID] [bigint] NULL,
	[intRiskTypeID] [int] NULL,
	[RiskType_Desc] [varchar](50) NULL,
	[intRiskID] [bigint] NULL,
	[intPolicyID] [bigint] NULL,
	[strDescription] [varchar](255) NULL,
	[cSumInsured] [money] NULL,
	[dDateCreated] [datetime] NULL,
	[strCreatedBy] [varchar](255) NULL,
	[intStatus] [int] NULL,
	[intStatus_Desc] [varchar](30) NULL,
	[intCurrency] [varchar](255) NULL,
	[intCurrency_Desc] [varchar](30) NULL,
	[cAdminFee] [money] NULL,
	[iProductID] [int] NULL,
	[iProductID_Desc] [varchar](50) NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[iPaymentTerm] [int] NULL,
	[iPaymentTerm_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cSASRIA] [money] NULL,
	[cCommission] [money] NULL,
	[dValidTill] [datetime] NULL,
	[cVat] [money] NULL,
	[sLastUpdatedBy] [varchar](255) NULL,
	[dEffectiveDate] [datetime] NULL,
	[iAddressID] [bigint] NULL,
	[iParentID] [bigint] NULL,
	[ParentRisk_Desc] [varchar](255) NULL,
	[cAnnualPremium] [money] NULL,
	[cAnnualComm] [money] NULL,
	[cAnnualSASRIA] [money] NULL,
	[cAnnualAdminFee] [money] NULL,
	[sDeleteReason] [varchar](255) NULL,
	[sSASRIAGroup] [varchar](255) NULL,
	[dInception] [datetime] NULL,
	[cCommissionPerc] [varchar](255) NULL,
	[sMatching] [varchar](255) NULL,
	[cCommissionRebate] [varchar](255) NULL,
	[dReviewDate] [datetime] NULL,
	[sReviewer] [varchar](255) NULL,
	[PremiumPerc] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[EDW_RunStatus]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[EDW_RunStatus](
	[TableName] [varchar](30) NULL,
	[NoOfRows] [int] NULL,
	[LastRunDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[IntegrationMasterFiles]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[IntegrationMasterFiles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[iCustomerID] [int] NOT NULL,
	[sFileName] [varchar](100) NOT NULL,
	[sFileDirection] [varchar](20) NULL,
	[sFileLocation] [varchar](250) NULL,
	[sFileProcessedLocation] [varchar](250) NULL,
	[sFileArchiveLocation] [varchar](250) NULL,
	[sPackageFolderName] [varchar](100) NULL,
	[sPackageProjectName] [varchar](100) NULL,
	[sPackageName] [varchar](100) NULL,
	[iStatusID] [int] NOT NULL,
	[sCreatedBy] [varchar](50) NOT NULL,
	[dCreated] [date] NOT NULL,
 CONSTRAINT [PK_IntegrationMasterFiles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[IntegrationMasterScripts]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[IntegrationMasterScripts](
	[iScriptID] [int] IDENTITY(1,1) NOT NULL,
	[iIntegrationMasterFileID] [int] NOT NULL,
	[sScriptName] [varchar](50) NOT NULL,
	[sScriptDesc] [varchar](100) NULL,
	[iFileFormatID] [int] NOT NULL,
	[sSQLSript] [text] NULL,
	[sColumnNames] [varchar](250) NULL,
	[sColumnSizes] [varchar](250) NULL,
	[sDelimeter] [varchar](5) NULL,
	[bHasHeaderRow] [varchar](5) NULL,
	[sHeaderRowSQLScript] [varchar](250) NULL,
	[bHasFooterRow] [varchar](5) NULL,
	[sFooterRowSQLScript] [varchar](250) NULL,
	[sCreatedBy] [varchar](50) NOT NULL,
	[dCreated] [date] NOT NULL,
	[ExportSequenceNumber] [int] NULL,
 CONSTRAINT [PK_CL_IntegrationMasterScripts] PRIMARY KEY CLUSTERED 
(
	[iScriptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[st_ClaimDetails_Normal]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[st_ClaimDetails_Normal](
	[iClaimID] [float] NOT NULL,
	[dLastUpdated] [datetime] NULL,
	[CL_THICLA] [varchar](255) NULL,
	[CL_PBEATER] [varchar](255) NULL,
	[CL_BACKOFFINEG] [varchar](255) NULL,
	[CL_CHK_243] [varchar](255) NULL,
	[CL_CHK_000] [varchar](255) NULL,
	[CL_CHK_002] [varchar](255) NULL,
	[CL_CHK_003] [varchar](255) NULL,
	[CL_REPUDI] [varchar](255) NULL,
	[CL_REPUDE] [varchar](255) NULL,
	[CL_VEHDRI] [varchar](255) NULL,
	[CL_DATOIN] [varchar](255) NULL,
	[CL_DATOID] [varchar](255) NULL,
	[CL_CHK_010] [varchar](255) NULL,
	[CL_APPDATE] [varchar](255) NULL,
	[CL_CHK_011] [varchar](255) NULL,
	[CL_CHK_012] [varchar](255) NULL,
	[CL_CHK_013] [varchar](255) NULL,
	[CL_CHK_014] [varchar](255) NULL,
	[CL_CHK_015] [varchar](255) NULL,
	[CL_CHK_016] [varchar](255) NULL,
	[CL_CHK_240] [varchar](255) NULL,
	[CL_CHK_215] [varchar](255) NULL,
	[CL_CHK_216] [varchar](255) NULL,
	[CL_CHK_241] [varchar](255) NULL,
	[CL_CHK_017] [varchar](255) NULL,
	[CL_CHK_023] [varchar](255) NULL,
	[CL_CHK_024] [varchar](255) NULL,
	[CL_CHK_025] [varchar](255) NULL,
	[CL_CHK_027] [varchar](255) NULL,
	[CL_CHK_163] [varchar](255) NULL,
	[CL_CHK_028] [varchar](255) NULL,
	[CL_CHK_026] [varchar](255) NULL,
	[CL_CHK_029] [varchar](255) NULL,
	[CL_CHK_052] [varchar](255) NULL,
	[CL_CHK_092] [varchar](255) NULL,
	[CL_RECCLA] [varchar](255) NULL,
	[CL_CHK_094] [varchar](255) NULL,
	[CL_CHK_095] [varchar](255) NULL,
	[CL_CHK_098] [varchar](255) NULL,
	[CL_CHK_099] [varchar](255) NULL,
	[CL_CHK_100] [varchar](255) NULL,
	[CL_CHK_128] [varchar](255) NULL,
	[CL_CHK_129] [varchar](255) NULL,
	[CL_CHK_130] [varchar](255) NULL,
	[CL_CHK_131] [varchar](255) NULL,
	[CL_CHK_140] [varchar](255) NULL,
	[CL_CHK_200] [varchar](255) NULL,
	[CL_CHK_201] [varchar](255) NULL,
	[CL_CHK_202] [varchar](255) NULL,
	[CL_CHK_203] [varchar](255) NULL,
	[CL_CHK_219] [varchar](255) NULL,
	[CL_CHK_220] [varchar](255) NULL,
	[CL_CHK_222] [varchar](255) NULL,
	[CL_CHK_223] [varchar](255) NULL,
	[CL_INVAPP] [varchar](255) NULL,
	[CL_INVNAME] [varchar](255) NULL,
	[CL_CHK_237] [varchar](255) NULL,
	[CL_CHK_238] [varchar](255) NULL,
	[CL_CHK_239] [varchar](255) NULL,
	[CL_CHK_030] [varchar](255) NULL,
	[CL_CHK_031] [varchar](255) NULL,
	[CL_CHK_032] [varchar](255) NULL,
	[CL_CHK_224] [varchar](255) NULL,
	[CL_CHK_225] [varchar](255) NULL,
	[CL_CHK_033] [varchar](255) NULL,
	[CL_CHK_034] [varchar](255) NULL,
	[CL_CHK_0342] [varchar](255) NULL,
	[CL_CHK_0343] [varchar](255) NULL,
	[CL_CHK_0344] [varchar](255) NULL,
	[CL_CHK_0345] [varchar](255) NULL,
	[CL_WRTOFF] [varchar](255) NULL,
	[CL_WOCONFDTE] [varchar](255) NULL,
	[CL_STKNMBR] [varchar](255) NULL,
	[CL_SALAGREE] [varchar](255) NULL,
	[CL_SALUPLDATE] [varchar](255) NULL,
	[CL_INVOICED] [varchar](255) NULL,
	[CL_INVNO] [varchar](255) NULL,
	[CL_CHK_228] [varchar](255) NULL,
	[CL_CHK_229] [varchar](255) NULL,
	[CL_CHK_230] [varchar](255) NULL,
	[CL_CHK_247] [varchar](255) NULL,
	[CL_CHK_232] [varchar](255) NULL,
	[CL_CHK_233] [varchar](255) NULL,
	[CL_CHK_248] [varchar](255) NULL,
	[CL_CHK_235] [varchar](255) NULL,
	[CL_CHK_039] [varchar](255) NULL,
	[CL_CHK_102] [varchar](255) NULL,
	[CL_AGTNAME] [varchar](255) NULL,
	[CL_OSAGT] [varchar](255) NULL,
	[CL_OSAGTREFNO] [varchar](255) NULL,
	[CL_OSAGTAPPDATE] [varchar](255) NULL,
	[CL_CHK_106] [varchar](255) NULL,
	[CL_CHK_109] [varchar](255) NULL,
	[CL_CHK_111] [varchar](255) NULL,
	[CL_RECMER] [varchar](255) NULL,
	[CL_CHK_115] [varchar](255) NULL,
	[CL_CHK_116] [varchar](255) NULL,
	[CL_TPINSURID] [varchar](255) NULL,
	[CL_CHK_INSCONME] [varchar](255) NULL,
	[CL_CHK_123] [varchar](255) NULL,
	[CL_CHK_124] [varchar](255) NULL,
	[CL_CHK_125] [varchar](255) NULL,
	[CL_CHK_126] [varchar](255) NULL,
	[CL_CHK_127] [varchar](255) NULL,
	[CL_CHK_141] [varchar](255) NULL,
	[CL_CHK_145] [varchar](255) NULL,
	[CL_CHK_147] [varchar](255) NULL,
	[CL_CHK_162] [varchar](255) NULL,
	[CL_CHK_200_01] [varchar](255) NULL,
	[CL_RECCLA_01] [varchar](255) NULL,
	[CL_CHK_201_01] [varchar](255) NULL,
	[CL_CHK_094_01] [varchar](255) NULL,
	[CL_CHK_202_01] [varchar](255) NULL,
	[CL_CHK_095_01] [varchar](255) NULL,
	[CL_CHK_203_01] [varchar](255) NULL,
	[CL_CHK_098_01] [varchar](255) NULL,
	[CL_CHK_099_01] [varchar](255) NULL,
	[CL_CHK_100_01] [varchar](255) NULL,
	[CL_CHK_128_01] [varchar](255) NULL,
	[CL_CHK_129_01] [varchar](255) NULL,
	[CL_CHK_130_01] [varchar](255) NULL,
	[CL_CHK_131_01] [varchar](255) NULL,
	[CL_CHK_140_01] [varchar](255) NULL,
	[CL_CHK_219_01] [varchar](255) NULL,
	[CL_CHK_220_01] [varchar](255) NULL,
	[CL_CHK_222_01] [varchar](255) NULL,
	[CL_CHK_223_01] [varchar](255) NULL,
	[CL_CHK_102_01] [varchar](255) NULL,
	[CL_AGTNAME_01] [varchar](255) NULL,
	[CL_OSAGT_01] [varchar](255) NULL,
	[CL_OSAGTREFN_01] [varchar](255) NULL,
	[CL_OSAGTAPPD_01] [varchar](255) NULL,
	[CL_CHK_106_01] [varchar](255) NULL,
	[CL_CHK_109_01] [varchar](255) NULL,
	[CL_CHK_111_01] [varchar](255) NULL,
	[CL_RECMER_01] [varchar](255) NULL,
	[CL_CHK_115_01] [varchar](255) NULL,
	[CL_CHK_116_01] [varchar](255) NULL,
	[CL_TPINSURID_01] [varchar](255) NULL,
	[CL_CHK_INSCO_01] [varchar](255) NULL,
	[CL_CHK_123_01] [varchar](255) NULL,
	[CL_CHK_124_01] [varchar](255) NULL,
	[CL_CHK_125_01] [varchar](255) NULL,
	[CL_CHK_126_01] [varchar](255) NULL,
	[CL_CHK_127_01] [varchar](255) NULL,
	[CL_CHK_141_01] [varchar](255) NULL,
	[CL_CHK_145_01] [varchar](255) NULL,
	[CL_CHK_147_01] [varchar](255) NULL,
	[CL_CHK_162_01] [varchar](255) NULL,
	[CL_CHK_200_02] [varchar](255) NULL,
	[CL_RECCLA_02] [varchar](255) NULL,
	[CL_CHK_201_02] [varchar](255) NULL,
	[CL_CHK_094_02] [varchar](255) NULL,
	[CL_CHK_202_02] [varchar](255) NULL,
	[CL_CHK_095_02] [varchar](255) NULL,
	[CL_CHK_203_02] [varchar](255) NULL,
	[CL_CHK_098_02] [varchar](255) NULL,
	[CL_CHK_099_02] [varchar](255) NULL,
	[CL_CHK_100_02] [varchar](255) NULL,
	[CL_CHK_128_02] [varchar](255) NULL,
	[CL_CHK_129_02] [varchar](255) NULL,
	[CL_CHK_130_02] [varchar](255) NULL,
	[CL_CHK_131_02] [varchar](255) NULL,
	[CL_CHK_140_02] [varchar](255) NULL,
	[CL_CHK_219_02] [varchar](255) NULL,
	[CL_CHK_220_02] [varchar](255) NULL,
	[CL_CHK_222_02] [varchar](255) NULL,
	[CL_CHK_223_02] [varchar](255) NULL,
	[CL_CHK_102_02] [varchar](255) NULL,
	[CL_AGTNAME_02] [varchar](255) NULL,
	[CL_OSAGT_02] [varchar](255) NULL,
	[CL_OSAGTREFN_02] [varchar](255) NULL,
	[CL_OSAGTAPPD_02] [varchar](255) NULL,
	[CL_CHK_106_02] [varchar](255) NULL,
	[CL_CHK_109_02] [varchar](255) NULL,
	[CL_CHK_111_02] [varchar](255) NULL,
	[CL_RECMER_02] [varchar](255) NULL,
	[CL_CHK_115_02] [varchar](255) NULL,
	[CL_CHK_116_02] [varchar](255) NULL,
	[CL_TPINSURID_02] [varchar](255) NULL,
	[CL_CHK_INSCO_02] [varchar](255) NULL,
	[CL_CHK_123_02] [varchar](255) NULL,
	[CL_CHK_124_02] [varchar](255) NULL,
	[CL_CHK_125_02] [varchar](255) NULL,
	[CL_CHK_126_02] [varchar](255) NULL,
	[CL_CHK_127_02] [varchar](255) NULL,
	[CL_CHK_141_02] [varchar](255) NULL,
	[CL_CHK_145_02] [varchar](255) NULL,
	[CL_CHK_147_02] [varchar](255) NULL,
	[CL_CHK_162_02] [varchar](255) NULL,
	[CL_CHK_200_03] [varchar](255) NULL,
	[CL_RECCLA_03] [varchar](255) NULL,
	[CL_CHK_201_03] [varchar](255) NULL,
	[CL_CHK_094_03] [varchar](255) NULL,
	[CL_CHK_202_03] [varchar](255) NULL,
	[CL_CHK_095_03] [varchar](255) NULL,
	[CL_CHK_203_03] [varchar](255) NULL,
	[CL_CHK_098_03] [varchar](255) NULL,
	[CL_CHK_099_03] [varchar](255) NULL,
	[CL_CHK_100_03] [varchar](255) NULL,
	[CL_CHK_128_03] [varchar](255) NULL,
	[CL_CHK_129_03] [varchar](255) NULL,
	[CL_CHK_130_03] [varchar](255) NULL,
	[CL_CHK_131_03] [varchar](255) NULL,
	[CL_CHK_140_03] [varchar](255) NULL,
	[CL_CHK_219_03] [varchar](255) NULL,
	[CL_CHK_220_03] [varchar](255) NULL,
	[CL_CHK_222_03] [varchar](255) NULL,
	[CL_CHK_223_03] [varchar](255) NULL,
	[CL_CHK_102_03] [varchar](255) NULL,
	[CL_AGTNAME_03] [varchar](255) NULL,
	[CL_OSAGT_03] [varchar](255) NULL,
	[CL_OSAGTREFN_03] [varchar](255) NULL,
	[CL_OSAGTAPPD_03] [varchar](255) NULL,
	[CL_CHK_106_03] [varchar](255) NULL,
	[CL_CHK_109_03] [varchar](255) NULL,
	[CL_CHK_111_03] [varchar](255) NULL,
	[CL_RECMER_03] [varchar](255) NULL,
	[CL_CHK_115_03] [varchar](255) NULL,
	[CL_CHK_116_03] [varchar](255) NULL,
	[CL_TPINSURID_03] [varchar](255) NULL,
	[CL_CHK_INSCO_03] [varchar](255) NULL,
	[CL_CHK_123_03] [varchar](255) NULL,
	[CL_CHK_124_03] [varchar](255) NULL,
	[CL_CHK_125_03] [varchar](255) NULL,
	[CL_CHK_126_03] [varchar](255) NULL,
	[CL_CHK_127_03] [varchar](255) NULL,
	[CL_CHK_141_03] [varchar](255) NULL,
	[CL_CHK_145_03] [varchar](255) NULL,
	[CL_CHK_147_03] [varchar](255) NULL,
	[CL_CHK_162_03] [varchar](255) NULL,
	[CL_CHK_200_04] [varchar](255) NULL,
	[CL_RECCLA_04] [varchar](255) NULL,
	[CL_CHK_201_04] [varchar](255) NULL,
	[CL_CHK_094_04] [varchar](255) NULL,
	[CL_CHK_202_04] [varchar](255) NULL,
	[CL_CHK_095_04] [varchar](255) NULL,
	[CL_CHK_203_04] [varchar](255) NULL,
	[CL_CHK_098_04] [varchar](255) NULL,
	[CL_CHK_099_04] [varchar](255) NULL,
	[CL_CHK_100_04] [varchar](255) NULL,
	[CL_CHK_128_04] [varchar](255) NULL,
	[CL_CHK_129_04] [varchar](255) NULL,
	[CL_CHK_130_04] [varchar](255) NULL,
	[CL_CHK_131_04] [varchar](255) NULL,
	[CL_CHK_140_04] [varchar](255) NULL,
	[CL_CHK_219_04] [varchar](255) NULL,
	[CL_CHK_220_04] [varchar](255) NULL,
	[CL_CHK_222_04] [varchar](255) NULL,
	[CL_CHK_223_04] [varchar](255) NULL,
	[CL_CHK_102_04] [varchar](255) NULL,
	[CL_AGTNAME_04] [varchar](255) NULL,
	[CL_OSAGT_04] [varchar](255) NULL,
	[CL_OSAGTREFN_04] [varchar](255) NULL,
	[CL_OSAGTAPPD_04] [varchar](255) NULL,
	[CL_CHK_106_04] [varchar](255) NULL,
	[CL_CHK_109_04] [varchar](255) NULL,
	[CL_CHK_111_04] [varchar](255) NULL,
	[CL_RECMER_04] [varchar](255) NULL,
	[CL_CHK_115_04] [varchar](255) NULL,
	[CL_CHK_116_04] [varchar](255) NULL,
	[CL_TPINSURID_04] [varchar](255) NULL,
	[CL_CHK_INSCO_04] [varchar](255) NULL,
	[CL_CHK_123_04] [varchar](255) NULL,
	[CL_CHK_124_04] [varchar](255) NULL,
	[CL_CHK_125_04] [varchar](255) NULL,
	[CL_CHK_126_04] [varchar](255) NULL,
	[CL_CHK_127_04] [varchar](255) NULL,
	[CL_CHK_141_04] [varchar](255) NULL,
	[CL_CHK_145_04] [varchar](255) NULL,
	[CL_CHK_147_04] [varchar](255) NULL,
	[CL_CHK_162_04] [varchar](255) NULL,
	[CL_CHK_200_05] [varchar](255) NULL,
	[CL_RECCLA_05] [varchar](255) NULL,
	[CL_CHK_201_05] [varchar](255) NULL,
	[CL_CHK_094_05] [varchar](255) NULL,
	[CL_CHK_202_05] [varchar](255) NULL,
	[CL_CHK_095_05] [varchar](255) NULL,
	[CL_CHK_203_05] [varchar](255) NULL,
	[CL_CHK_098_05] [varchar](255) NULL,
	[CL_CHK_099_05] [varchar](255) NULL,
	[CL_CHK_100_05] [varchar](255) NULL,
	[CL_CHK_128_05] [varchar](255) NULL,
	[CL_CHK_129_05] [varchar](255) NULL,
	[CL_CHK_130_05] [varchar](255) NULL,
	[CL_CHK_131_05] [varchar](255) NULL,
	[CL_CHK_140_05] [varchar](255) NULL,
	[CL_CHK_219_05] [varchar](255) NULL,
	[CL_CHK_220_05] [varchar](255) NULL,
	[CL_CHK_222_05] [varchar](255) NULL,
	[CL_CHK_223_05] [varchar](255) NULL,
	[CL_CHK_102_05] [varchar](255) NULL,
	[CL_AGTNAME_05] [varchar](255) NULL,
	[CL_OSAGT_05] [varchar](255) NULL,
	[CL_OSAGTREFN_05] [varchar](255) NULL,
	[CL_OSAGTAPPD_05] [varchar](255) NULL,
	[CL_CHK_106_05] [varchar](255) NULL,
	[CL_CHK_109_05] [varchar](255) NULL,
	[CL_CHK_111_05] [varchar](255) NULL,
	[CL_RECMER_05] [varchar](255) NULL,
	[CL_CHK_115_05] [varchar](255) NULL,
	[CL_CHK_116_05] [varchar](255) NULL,
	[CL_TPINSURID_05] [varchar](255) NULL,
	[CL_CHK_INSCO_05] [varchar](255) NULL,
	[CL_CHK_123_05] [varchar](255) NULL,
	[CL_CHK_124_05] [varchar](255) NULL,
	[CL_CHK_125_05] [varchar](255) NULL,
	[CL_CHK_126_05] [varchar](255) NULL,
	[CL_CHK_127_05] [varchar](255) NULL,
	[CL_CHK_141_05] [varchar](255) NULL,
	[CL_CHK_145_05] [varchar](255) NULL,
	[CL_CHK_147_05] [varchar](255) NULL,
	[CL_CHK_162_05] [varchar](255) NULL,
	[CL_LEGALINV] [varchar](255) NULL,
	[CL_CHK_211] [varchar](255) NULL,
	[CL_CHK_212] [varchar](255) NULL,
	[CL_CHK_213] [varchar](255) NULL,
	[CL_CHK_214] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[st_CustomerDetails_Normal]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[st_CustomerDetails_Normal](
	[iCustomerID] [bigint] NULL,
	[dLastUpdated] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[C_CELLNO] [varchar](255) NULL,
	[C_CONTDETAILS] [varchar](255) NULL,
	[C_DOB] [varchar](255) NULL,
	[C_EMAIL] [varchar](255) NULL,
	[C_FAXTEL] [varchar](255) NULL,
	[C_GENDER] [varchar](255) NULL,
	[C_HOMETEL] [varchar](255) NULL,
	[C_IDNO] [varchar](255) NULL,
	[C_TITLE] [varchar](255) NULL,
	[C_VIPCLIENT] [varchar](255) NULL,
	[C_WORKTEL] [varchar](255) NULL,
	[CH_INITIALS] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[st_PolicyDetails_Normal]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[st_PolicyDetails_Normal](
	[iPolicyID] [float] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[P_ServiceProv] [varchar](255) NULL,
	[P_MARKETCODE] [varchar](255) NULL,
	[P_CASENO] [varchar](255) NULL,
	[P_MORE5VEH] [varchar](255) NULL,
	[P_BANKPROTIND] [varchar](255) NULL,
	[P_BANKPROTPNO] [varchar](255) NULL,
	[P_PREMMATREAS] [varchar](255) NULL,
	[P_ADDINFO] [varchar](255) NULL,
	[P_B_REFACCH] [varchar](255) NULL,
	[P_B_REFACCN] [varchar](255) NULL,
	[P_B_REFBRNC] [varchar](255) NULL,
	[P_CWarning] [varchar](255) NULL,
	[P_LASTRD_DOREF] [varchar](255) NULL,
	[P_LASTRD_HEAD] [varchar](255) NULL,
	[P_LASTRD_REASON] [varchar](255) NULL,
	[P_ITCCOLOUR] [varchar](255) NULL,
	[P_QUALQUEST] [varchar](255) NULL,
	[P_UW] [varchar](255) NULL,
	[P_LASTRD_AMT] [varchar](255) NULL,
	[P_EXISTDAMAMT] [varchar](255) NULL,
	[P_BROINITFEE] [varchar](255) NULL,
	[P_BROKERBANK] [varchar](255) NULL,
	[P_B_REFUND] [varchar](255) NULL,
	[P_OR_BROKERFEE] [varchar](255) NULL,
	[P_OR_LEGALADV] [varchar](255) NULL,
	[P_VIKELA] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[P_LASTRD_DATE] [varchar](255) NULL,
	[P_POLTYPE] [varchar](255) NULL,
	[P_LICRESTRIC] [varchar](255) NULL,
	[P_MORE2CLAIMS] [varchar](255) NULL,
	[P_CASHBACKBONUS] [varchar](255) NULL,
	[P_B_REFACCT] [varchar](255) NULL,
	[P_B_REFBANK] [varchar](255) NULL,
	[P_EXISTDAM] [varchar](255) NULL,
	[P_G_SOURCE] [varchar](255) NULL,
	[P_INSCANC] [varchar](255) NULL,
	[P_INSDECL] [varchar](255) NULL,
	[P_INSREFUSED] [varchar](255) NULL,
	[P_ISAFECASHBACK] [varchar](255) NULL,
	[P_G_ADDNOTES] [varchar](255) NULL,
	[P_G_CONFNOTES] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SKI_INT].[st_RiskDetails_Normal]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SKI_INT].[st_RiskDetails_Normal](
	[intPolicyID] [float] NULL,
	[iRiskID] [float] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[ZIPCODE] [varchar](255) NULL,
	[R_MOTORSEATS] [varchar](255) NULL,
	[R_MOTORYOM] [varchar](255) NULL,
	[R_MOTORREG] [varchar](255) NULL,
	[R_ENGIN_NO] [varchar](255) NULL,
	[R_CHASSISNO] [varchar](255) NULL,
	[R_COVERDETAILS] [varchar](255) NULL,
	[R_G_MARKETVAL] [varchar](255) NULL,
	[R_G_PASLIABIND] [varchar](255) NULL,
	[R_G_NUOFSEATS] [varchar](255) NULL,
	[R_G_HPLEASE] [varchar](255) NULL,
	[R_G_EXTENDLOU] [varchar](255) NULL,
	[R_G_PERSACCIND] [varchar](255) NULL,
	[R_G_CREDSFALL] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[R_G_ABSVIOCRE] [varchar](255) NULL,
	[R_G_ISAFE] [varchar](255) NULL,
	[R_G_EXCESSRO] [varchar](255) NULL,
	[R_G_WINDSCREEN] [varchar](255) NULL,
	[R_G_BACK2INV] [varchar](255) NULL,
	[R_G_CASHBACK] [varchar](255) NULL,
	[R_G_DEALER] [varchar](255) NULL,
	[R_G_INTRSTNOTED] [varchar](255) NULL,
	[R_G_CREDITPRO] [varchar](255) NULL,
	[R_G_CRACCNO] [varchar](255) NULL,
	[R_G_ASSOC] [varchar](255) NULL,
	[R_G_STDENDORSE] [varchar](255) NULL,
	[R_G_ADDFAP2000] [varchar](255) NULL,
	[R_G_ADDFAP2500] [varchar](255) NULL,
	[R_G_ADDFAP5000] [varchar](255) NULL,
	[R_G_ADDFAP7500] [varchar](255) NULL,
	[R_G_ADDFAP10000] [varchar](255) NULL,
	[R_G_ADDFAP15000] [varchar](255) NULL,
	[R_G_ADDFAP20000] [varchar](255) NULL,
	[R_G_AMATEKSI] [varchar](255) NULL,
	[R_G_AMAACCNO] [varchar](255) NULL,
	[R_G_AMAIDNO] [varchar](255) NULL,
	[R_MATCHINGCT] [varchar](255) NULL,
	[R_CTPERACC] [varchar](255) NULL,
	[R_CTHPLEASE] [varchar](255) NULL,
	[R_CTPASS] [varchar](255) NULL,
	[R_CTEXTENDLOU] [varchar](255) NULL,
	[R_CTHELIVAC] [varchar](255) NULL,
	[R_CTAMACLA] [varchar](255) NULL,
	[R_CTBACK2INV] [varchar](255) NULL,
	[R_CTISAFE] [varchar](255) NULL,
	[R_CTCASHBACK] [varchar](255) NULL,
	[R_CTCREDITSF] [varchar](255) NULL,
	[R_CTEXCESSRO] [varchar](255) NULL,
	[R_TSASRIA] [varchar](255) NULL,
	[R_MATCHINGPERC] [varchar](255) NULL,
	[R_MORE5VEH] [varchar](255) NULL,
	[R_SI] [varchar](255) NULL,
	[R_G_PASSENSI] [varchar](255) NULL,
	[R_G_PASSENDT] [varchar](255) NULL,
	[R_G_HPLEASESI] [varchar](255) NULL,
	[R_G_PERACCSI] [varchar](255) NULL,
	[R_G_EXTENDLOUSI] [varchar](255) NULL,
	[R_G_HELIVACSI] [varchar](255) NULL,
	[R_G_CSFSI] [varchar](255) NULL,
	[R_G_ABSVIOCRESI] [varchar](255) NULL,
	[R_G_ISAFESI] [varchar](255) NULL,
	[R_G_EXCESSROSI] [varchar](255) NULL,
	[R_G_WINDSCRSI] [varchar](255) NULL,
	[R_G_BACK2INVSI] [varchar](255) NULL,
	[R_G_CASHBACKSI] [varchar](255) NULL,
	[R_G_AMAPREMSI] [varchar](255) NULL,
	[R_G_VEHDEF] [varchar](255) NULL,
	[R_G_TRACKCUST] [varchar](255) NULL,
	[R_G_TVEHDEF] [varchar](255) NULL,
	[R_G_TRAILERLOI] [varchar](255) NULL,
	[R_G_LOANPRTPR] [varchar](255) NULL,
	[R_G_DRVDEATHSI] [varchar](255) NULL,
	[R_MATCHING] [varchar](255) NULL,
	[R_G_PASDEATHSI] [varchar](255) NULL,
	[R_G_TRACKREFNO] [varchar](255) NULL,
	[R_Driver] [varchar](255) NULL,
	[R_DriveName1] [varchar](255) NULL,
	[R_DriveID1] [varchar](255) NULL,
	[R_DriveCell1] [varchar](255) NULL,
	[R_DriveName2] [varchar](255) NULL,
	[R_DriveID2] [varchar](255) NULL,
	[R_DriveCell2] [varchar](255) NULL,
	[R_DriveName3] [varchar](255) NULL,
	[R_DriveID3] [varchar](255) NULL,
	[R_DriveCell3] [varchar](255) NULL,
	[R_DriveName4] [varchar](255) NULL,
	[R_DriveID4] [varchar](255) NULL,
	[R_DriveCell4] [varchar](255) NULL,
	[R_DriveName5] [varchar](255) NULL,
	[R_DriveID5] [varchar](255) NULL,
	[R_DriveCell5] [varchar](255) NULL,
	[R_DriveName6] [varchar](255) NULL,
	[R_DriveID6] [varchar](255) NULL,
	[R_DriveCell6] [varchar](255) NULL,
	[R_G_ADDFAPG1000] [varchar](255) NULL,
	[R_G_ADDFAPG2000] [varchar](255) NULL,
	[R_G_ADDFAPG3000] [varchar](255) NULL,
	[R_G_TPOSI] [varchar](255) NULL,
	[R_G_HPLEASEDT] [varchar](255) NULL,
	[R_G_EXTENDLOUDT] [varchar](255) NULL,
	[R_G_LOSSINOPSI] [varchar](255) NULL,
	[R_G_HELIVACDT] [varchar](255) NULL,
	[R_G_ISAFEDT] [varchar](255) NULL,
	[R_G_PERACCDT] [varchar](255) NULL,
	[R_G_CSFDT] [varchar](255) NULL,
	[R_G_ABSVIOCREDT] [varchar](255) NULL,
	[R_G_EXCESSRODT] [varchar](255) NULL,
	[R_G_WINDSCRDT] [varchar](255) NULL,
	[R_G_BACK2INVDT] [varchar](255) NULL,
	[R_G_CASHBACKDT] [varchar](255) NULL,
	[R_ISFCSHBCK_CPC] [varchar](255) NULL,
	[R_LOI_TOUR_CPC] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [SKI_INT].[v_Get_Delta_PolicyTransactionIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_PolicyTransactionIDs]
AS
SELECT iTransactionID
FROM Ski_int.Delta_PolicyTransactions (NOLOCK)

GO
/****** Object:  View [SKI_INT].[EDW_PolTran_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTran_Delta]
AS
SELECT DISTINCT pt.iTransactionId
	,iPolicyId
	,sReference
	,iAgencyId
	,iTrantype
	,cPremium
	,cCommission
	,cAdminfee
	,cSASRIA
	,dTransaction
	,sUser
	,dPeriodStart
	,dPeriodEnd
	,iCommStatus
	,iParentTranID
	,iDebitNoteId
	,cPolicyFee
	,sComment
	,dEffective
	,cBrokerFee
	,sFinAccount
FROM PolicyTransactions pt (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs delta (NOLOCK) ON delta.iTransactionID = pt.iTransactionId

GO
/****** Object:  View [SKI_INT].[vw_PolicyCommStruct]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[vw_PolicyCommStruct]
AS
SELECT PCS.iPolicyID [PolicyId]
	,PCS.iVersionNo [VersionId]
	,CE1.sName [Lev1Name]
	,PCS.iLev1EntityPerc [Lev1Perc]
	,CE2.sName [Lev2Name]
	,PCS.iLev2EntityPerc [Lev2Perc]
	,CE3.sName [Lev3Name]
	,PCS.iLev3EntityPerc [Lev3Perc]
	,CE4.sName [Lev4Name]
	,PCS.iLev4EntityPerc [Lev4Perc]
	,CE5.sName [Lev5Name]
	,PCS.iLev5EntityPerc [Lev5Perc]
FROM PolicyCommStruct PCS (NOLOCK)
JOIN SKI_INT.Delta_Policy dp (NOLOCK) ON PCS.iPolicyID = dp.iPolicyID
LEFT JOIN CommEntities CE1 (NOLOCK) ON CE1.iCommEntityID = PCS.iLev1EntityID
LEFT JOIN CommEntities CE2 (NOLOCK) ON CE2.iCommEntityID = PCS.iLev2EntityID
LEFT JOIN CommEntities CE3 (NOLOCK) ON CE3.iCommEntityID = PCS.iLev3EntityID
LEFT JOIN CommEntities CE4 (NOLOCK) ON CE4.iCommEntityID = PCS.iLev4EntityID
LEFT JOIN CommEntities CE5 (NOLOCK) ON CE5.iCommEntityID = PCS.iLev5EntityID

GO
/****** Object:  View [SKI_INT].[EDW_PolTranSummary_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTranSummary_Delta]
AS
SELECT DISTINCT Pol.iPolicyID AS [PolicyID]
	,SUM(PT.cPremium) AS [PREMIUM BAL]
	,SUM(PT.cCommission) AS [COMMISSOIN BAL]
	,SUM(PT.cSASRIA) AS [SASRIA BAL]
	,SUM(PT.cAdminFee) AS [ADMIN FEE BAL]
	,SUM(PT.cPolicyFee) AS [POLICY FEE BAL]
	,SUM(PT.cBrokerFee) AS [BROKER FEE BAL]
FROM Policy Pol (NOLOCK)
INNER JOIN PolicyTransactions PT (NOLOCK) ON PT.iPolicyID = Pol.iPolicyID
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs DELTA (NOLOCK) ON DELTA.iTransactionID = PT.iTransactionId
INNER JOIN SKI_INT.vw_PolicyCommStruct PCS (NOLOCK) ON PCS.PolicyId = Pol.iPolicyID
INNER JOIN Products PR (NOLOCK) ON PR.iProductID = Pol.iProductID
INNER JOIN LOOKUP LT (NOLOCK) ON LT.iIndex = PT.iTranType
	AND LT.sGroup = 'TransactionType'
GROUP BY Pol.iPolicyID

GO
/****** Object:  View [SKI_INT].[v_Get_Delta_RiskIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_RiskIDs]
AS
SELECT intRiskID
	,iTranID
FROM Ski_int.Delta_Risks (NOLOCK)

GO
/****** Object:  View [SKI_INT].[EDW_Risks_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Risks_Delta]
AS
SELECT DISTINCT intClientID
	,intRiskTypeID
	,r.intRiskID
	,intPolicyID
	,strDescription
	,cSumInsured
	,dDateCreated
	,strCreatedBy
	,intStatus
	,intCurrency
	,cAdminFee
	,iProductID
	,dDateModified
	,r.iTranID
	,iPaymentTerm
	,cPremium
	,cSASRIA
	,cCommission
	,dValidTill
	,cVat
	,sLastUpdatedBy
	,dEffectiveDate
	,iAddressID
	,iParentID
	,cAnnualPremium
	,cAnnualComm
	,cAnnualSASRIA
	,cAnnualAdminFee
	,sDeleteReason
	,sSASRIAGroup
	,dInception
	,cCommissionPerc
	,sMatching
	,cCommissionRebate
	,dReviewDate
	,sReviewer
	,PremiumPerc
FROM Risks R(NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_RiskIDs DELTA(NOLOCK) ON DELTA.intRiskID = R.intRiskID
	AND DELTA.iTranID = R.iTranID


GO
/****** Object:  View [SKI_INT].[EDW_PolTranDet_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTranDet_Delta]
AS
SELECT DISTINCT ptd.iTransactionID
	,iPolicyID
	,iRiskID
	,iRiskTypeID
	,dDateActioned
	,iTranType
	,cPremium
	,cCommission
	,cAdminFee
	,cSASRIA
	,sUser
FROM PolicyTransactionDetails ptd (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs DELTA (NOLOCK) ON DELTA.iTransactionID = ptd.iTransactionId

GO
/****** Object:  View [SKI_INT].[v_Get_Delta_ClaimIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_ClaimIDs]
AS
SELECT DISTINCT iClaimID
FROM Ski_int.Delta_Claims (NOLOCK)

GO
/****** Object:  View [SKI_INT].[V_ClaimValues]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[V_ClaimValues]
AS
SELECT aa.iClaimantID
	,aa.iClaimid
	,SUM(aa.cEstimate) cEstimate
	,SUM(aa.cExcess) cExcess
	,SUM(aa.cAmtPaid) cAmtPaid
	,SUM(aa.cRecovery) cRecovery
	,SUM(aa.cSalvage) cSalvage
	,SUM(aa.cEstimate - aa.cExcess - aa.cAmtpaid) cBalance
	,SUM(aa.cEstimate - aa.cExcess - aa.cAmtpaid) + SUM(aa.cAmtpaid) - SUM(aa.cRecovery + aa.cSalvage) cClaimCost
FROM (
	SELECT ct.iClaimid
		,ct.iClaimantid
		,SUM(CASE 
				WHEN ct.sTransactionCode IN (
						'CLEst'
						,'CLRevEst'
						)
					THEN ct.cValue
				ELSE 0
				END) cEstimate
		,SUM(CASE 
				WHEN ct.sTransactionCode IN (
						'CLExcess'
						,'CLRevExcess'
						)
					THEN - ct.cValue
				ELSE 0
				END) cExcess
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CTP'
					THEN ct.cValue
				ELSE 0
				END) cAmtPaid
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CTR'
					THEN - ct.cValue
				ELSE 0
				END) cRecovery
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CLSALVAGE'
					THEN - ct.cValue
				ELSE 0
				END) cSalvage
	FROM ClaimTransactions ct (NOLOCK)
	JOIN Claimants cla (NOLOCK) ON cla.iClaimantid = ct.iClaimantid
	WHERE ct.iClaimID = cla.iClaimID
	GROUP BY ct.iClaimid
		,ct.iClaimantid
	) aa
JOIN SKI_INT.v_Get_Delta_ClaimIDs DeltaClaims (NOLOCK) ON DeltaClaims.iClaimID = aa.iClaimID
GROUP BY aa.iClaimantID
	,aa.iClaimID

GO
/****** Object:  View [SKI_INT].[EDW_PolicyClaimTransaction_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolicyClaimTransaction_Delta]
AS
SELECT CL.iClaimID
	,SUM(VC.cClaimCost) AS [AMT CLAIMED]
	,SUM(VC.cExcess) AS [EXCESS]
	,SUM(VC.cSalvage) AS [SALVAGE]
	,SUM(VC.cRecovery) AS [RECOVERY]
	,SUM(VC.cAmtPaid) AS [PAID]
	,SUM(VC.cBalance) AS [BALANCE]
FROM Claims (NOLOCK) CL
INNER JOIN SKI_INT.v_Get_Delta_ClaimIDs Delta (NOLOCK) ON Delta.iClaimID = CL.iClaimID
INNER JOIN Claimants ca (NOLOCK) ON CA.iClaimID = CL.iClaimID
INNER JOIN SKI_INT.V_ClaimValues VC (NOLOCK) ON VC.iClaimantID = CA.iClaimantID
GROUP BY CL.iClaimID

GO
/****** Object:  View [SKI_INT].[v_Get_ALLClaimDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLClaimDetails]
AS
SELECT cd.iClaimID
	,cd.sFieldCode
	,cd.iFieldType
	,cd.sFieldValue
	,cd.cFieldValue
	,cd.bFieldValue
	,cd.dtFieldValue
	,cd.iFieldValue
	,cd.bReplicated
FROM ClaimDetails cd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_ClaimIDs vcd (NOLOCK) ON cd.iClaimID = vcd.iClaimID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON cd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT acd.iClaimID
	,acd.sFieldCode
	,acd.iFieldType
	,acd.sFieldValue
	,acd.cFieldValue
	,acd.bFieldValue
	,acd.dtFieldValue
	,acd.iFieldValue
	,acd.bReplicated
FROM Arch_ClaimDetails acd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_ClaimIDs vcd (NOLOCK) ON acd.iClaimID = vcd.iClaimID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON acd.sFieldCode = efc.sFieldCode

GO
/****** Object:  View [SKI_INT].[v_Get_ALLRiskDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLRiskDetails]
AS
SELECT rd.iRiskID
  ,rd.sFieldCode
  ,rd.iFieldType
  ,rd.sFieldValue
  ,rd.cFieldValue
  ,rd.bFieldValue
  ,rd.dtFieldValue
  ,rd.iFieldValue
  ,rd.bReplicated
  ,rd.iTranID
  ,rd.sGUID
FROM RiskDetails rd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_RiskIDs vrd (NOLOCK) ON rd.iRiskID = vrd.intRiskID
	AND rd.iTranID = vrd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON rd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT ard.iRiskID
  ,ard.sFieldCode
  ,ard.iFieldType
  ,ard.sFieldValue
  ,ard.cFieldValue
  ,ard.bFieldValue
  ,ard.dtFieldValue
  ,ard.iFieldValue
  ,ard.bReplicated
  ,ard.iTranID
  ,ard.sGUID
FROM Arch_RiskDetails ard (NOLOCK)
JOIN SKI_INT.v_Get_Delta_RiskIDs vrd (NOLOCK) ON ard.iRiskID = vrd.intRiskID
	AND ard.iTranID = vrd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON ard.sFieldCode = efc.sFieldCode


GO
/****** Object:  View [SKI_INT].[v_Get_Delta_PolicyIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_PolicyIDs]
AS
SELECT iPolicyID
	,iTranID
FROM Ski_int.Delta_Policy (NOLOCK)

GO
/****** Object:  View [SKI_INT].[v_Get_ALLPolicyDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLPolicyDetails]
AS
SELECT pd.iPolicyID
	,pd.sFieldCode
	,pd.iFieldType
	,pd.sFieldValue
	,pd.cFieldValue
	,pd.bFieldValue
	,pd.dtFieldValue
	,pd.iFieldValue
	,pd.bReplicated
	,pd.iTranID
	,pd.sGUID
FROM PolicyDetails pd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_PolicyIDs vpd (NOLOCK) ON pd.iPolicyID = vpd.iPolicyID
	AND pd.iTranID = vpd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON pd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT apd.iPolicyID
	,apd.sFieldCode
	,apd.iFieldType
	,apd.sFieldValue
	,apd.cFieldValue
	,apd.bFieldValue
	,apd.dtFieldValue
	,apd.iFieldValue
	,apd.bReplicated
	,apd.iTranID
	,apd.sGUID
FROM Arch_PolicyDetails apd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_PolicyIDs vpd (NOLOCK) ON apd.iPolicyID = vpd.iPolicyID
	AND apd.iTranID = vpd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON apd.sFieldCode = efc.sFieldCode


GO
/****** Object:  View [SKI_INT].[v_Get_Delta_CustomerIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_CustomerIDs]
AS
SELECT DISTINCT iCustomerID
	,iTranID
FROM Ski_int.Delta_Customer (NOLOCK)

GO
/****** Object:  View [SKI_INT].[v_Get_ALLCustomerDetails]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLCustomerDetails]
AS
SELECT cd.iCustomerID
	,cd.sFieldCode
	,cd.iFieldType
	,cd.sFieldValue
	,cd.cFieldValue
	,cd.bFieldValue
	,cd.dtFieldValue
	,cd.iFieldValue
	,cd.bReplicated
	,cd.iTranID
FROM CustomerDetails cd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_CustomerIDs vcd (NOLOCK) ON cd.iCustomerID = vcd.iCustomerID
	AND cd.iTranID = vcd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON cd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT acd.iCustomerID
	,acd.sFieldCode
	,acd.iFieldType
	,acd.sFieldValue
	,acd.cFieldValue
	,acd.bFieldValue
	,acd.dtFieldValue
	,acd.iFieldValue
	,acd.bReplicated
	,acd.iTranID
FROM Arch_CustomerDetails acd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_CustomerIDs vcd (NOLOCK) ON acd.iCustomerID = vcd.iCustomerID
	AND acd.iTranID = vcd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON acd.sFieldCode = efc.sFieldCode


GO
/****** Object:  View [SKI_INT].[EDW_Policy_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SKI_INT].[EDW_Policy_Delta]
AS
SELECT DISTINCT p.iPolicyID
	,iCustomerID
	,iProductID
	,sPolicyNo
	,sOldPolicyNo
	,iPolicyStatus
	,iPaymentStatus
	,dRenewalDate
	,dLastPaymentDate
	,dNextPaymentDate
	,dExpiryDate
	,dStartDate
	,dDateCreated
	,iPaymentTerm
	,cNextPaymentAmt
	,cAnnualPremium
	,iTaxPerc
	,iDiscountPerc
	,sComment
	,sAccHolderName
	,iPaymentMethod
	,sBank
	,sActionedBy
	,iCommissionMethod
	,cCommissionAmt
	,cCommissionPaid
	,sPolicyType
	,dDateModified
	,p.iTranID
	,sBranchCode
	,sCheckDigit
	,sExpiryDate
	,sAccountNo
	,sAccountType
	,sCancelReason
	,sCreditCardType
	,dEffectiveDate
	,cAdminFee
	,iCollection
	,cAnnualSASRIA
	,cPolicyFee
	,iCurrency
	,cBrokerFee
	,iAdminType
	,cAdminPerc
	,cAdminMin
	,cAdminMax
	,iParentID
	,iTreatyYear
	,dCoverStart
	,sFinAccount
	,dCancelled
	,iCoverTerm
	,sEndorseCode
	,eCommCalcMethod
	,iCommPayLevel
	,iIndemnityPeriod
	,dReviewDate
	,sLanguage
	,dRenewalPrepDate
	,CancelReasonCode
	,CountryId
	,CashbackEnabled
	,pcs.Lev1Name
	,pcs.Lev1Perc
	,pcs.Lev2Name
	,pcs.Lev2Perc
	,pcs.Lev3Name
	,pcs.Lev3Perc
	,pcs.Lev4Name
	,pcs.Lev4Perc
	,pcs.Lev5Name
	,pcs.Lev5Perc
FROM Policy p (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyIDs DELTA(NOLOCK) ON DELTA.iPolicyID = p.iPolicyID
	AND DELTA.iTranID = p.iTranID
INNER JOIN SKI_INT.vw_PolicyCommStruct pcs(NOLOCK) ON pcs.PolicyId = p.iPolicyID
	AND pcs.VersionId = p.iAgencyID

UNION ALL

SELECT DISTINCT p.iPolicyID
	,iCustomerID
	,iProductID
	,sPolicyNo
	,sOldPolicyNo
	,iPolicyStatus
	,iPaymentStatus
	,dRenewalDate
	,dLastPaymentDate
	,dNextPaymentDate
	,dExpiryDate
	,dStartDate
	,dDateCreated
	,iPaymentTerm
	,cNextPaymentAmt
	,cAnnualPremium
	,iTaxPerc
	,iDiscountPerc
	,sComment
	,sAccHolderName
	,iPaymentMethod
	,sBank
	,sActionedBy
	,iCommissionMethod
	,cCommissionAmt
	,cCommissionPaid
	,sPolicyType
	,dDateModified
	,p.iTranID
	,sBranchCode
	,sCheckDigit
	,sExpiryDate
	,sAccountNo
	,sAccountType
	,sCancelReason
	,sCreditCardType
	,dEffectiveDate
	,cAdminFee
	,iCollection
	,cAnnualSASRIA
	,cPolicyFee
	,iCurrency
	,cBrokerFee
	,iAdminType
	,cAdminPerc
	,cAdminMin
	,cAdminMax
	,iParentID
	,iTreatyYear
	,dCoverStart
	,sFinAccount
	,dCancelled
	,iCoverTerm
	,sEndorseCode
	,eCommCalcMethod
	,iCommPayLevel
	,iIndemnityPeriod
	,dReviewDate
	,sLanguage
	,dRenewalPrepDate
	,CancelReasonCode
	,CountryId
	,CashbackEnabled
	,pcs.Lev1Name
	,pcs.Lev1Perc
	,pcs.Lev2Name
	,pcs.Lev2Perc
	,pcs.Lev3Name
	,pcs.Lev3Perc
	,pcs.Lev4Name
	,pcs.Lev4Perc
	,pcs.Lev5Name
	,pcs.Lev5Perc
FROM Arch_Policy p (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyIDs DELTA(NOLOCK) ON DELTA.iPolicyID = p.iPolicyID
	AND DELTA.iTranID = p.iTranID
INNER JOIN SKI_INT.vw_PolicyCommStruct pcs(NOLOCK) ON pcs.PolicyId = p.iPolicyID
	AND pcs.VersionId = p.iAgencyID



GO
/****** Object:  View [SKI_INT].[EDW_Address_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Address_Delta]
AS
SELECT DISTINCT a.icustomerid
	,intAddressID
	,strAddressType
	,strAddressLine1
	,strAddressLine2
	,strAddressLine3
	,strSuburb
	,strPostalCode
	,strRatingArea
	,strPhoneNo
	,strFaxNo
	,strMobileNo
	,strEmailAdd
	,dUpdated
	,sUpdatedBy
	,sTown
	,a.iTranID
FROM Address(NOLOCK) a
JOIN SKI_INT.v_Get_Delta_CustomerIDs DELTA(NOLOCK) ON DELTA.iCustomerID = a.iCustomerID


GO
/****** Object:  View [SKI_INT].[V_ClaimInitialReserves]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[V_ClaimInitialReserves]
AS
SELECT a.iClaimID
	,SUM([Init Reserve]) AS [Init Reserve]
	,SUM([Init Excess]) AS [Init Excess]
FROM (
	SELECT CT.iClaimID
		,CT.iClaimantID
		,CASE CT.sTransactionCode
			WHEN 'CLEst'
				THEN SUM(cValue)
			END AS [Init Reserve]
		,CASE CT.sTransactionCode
			WHEN 'CLExcess'
				THEN SUM(cValue * - 1)
			END AS [Init Excess]
	FROM Claimtransactions (NOLOCK) CT
	INNER JOIN Claimants (NOLOCK) CA ON CA.iClaimantID = CT.iClaimantID
	INNER JOIN (
		SELECT iClaimID
			,CTMIN.sTransactionCode
			,MIN(CTMIN.dtTransaction) AS MINTRANSDATE
		FROM ClaimTransactions (NOLOCK) CTMIN
		WHERE CTMIN.sTransactionCode IN (
				'CLEst'
				,'CLExcess'
				)
		GROUP BY iClaimID
			,CTMIN.sTransactionCode
		) CTORIGEST ON CTORIGEST.iClaimID = CT.iClaimID
		AND CTORIGEST.sTransactionCode = CT.sTransactionCode
		AND CT.dtTransaction = MINTRANSDATE
	WHERE
		sClaimantName = 'Unallocated Reserve'
	GROUP BY CT.iClaimID
		,CT.iClaimantID
		,CT.sTransactionCode
	) a
JOIN [SKI_INT].[v_Get_Delta_ClaimIDs] (NOLOCK) dcl ON a.iClaimID = dcl.iClaimID
GROUP BY a.iClaimID

GO
/****** Object:  View [SKI_INT].[EDW_Claims_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SKI_INT].[EDW_Claims_Delta]
AS
SELECT DISTINCT CAST(cl.iClaimID AS VARCHAR) AS 'iClaimID'
	,iClientID AS 'iClientID'
	,iRiskID AS 'iRiskID'
	,iPolicyID AS 'iPolicyID'
	,sRefNo
	,sCauseCode
	,sResponsibleName
	,sResponsibleSurname
	,CONVERT(CHAR(10), dtEvent, 111) AS 'dtEvent'
	,sStatusCode
	,CONVERT(CHAR(10), dStatusChanged, 111) AS 'dStatusChanged'
	,REPLACE(REPLACE(sDescription, CHAR(13), ''), CHAR(10), ' ') AS 'sDescription'
	,sComment
	,sApprovalCode
	,bClientResponsible
	,cAmtFirstClaimed
	,cAmtClaimed
	,cAmtPaid
	,cRecovered
	,cBalance
	,CAST(CAST(iIncidentAddressID AS INT) AS VARCHAR) AS 'iIncidentAddressID'
	,cTotExcess
	,cExcessRecovered
	,sAssignedTo
	,CONVERT(CHAR(10), dtCreated, 111) AS 'dtCreated'
	,CONVERT(CHAR(10), dLastUpdated, 111) AS 'dLastUpdated'
	,sUpdatedBy
	,CONVERT(CHAR(10), dtRegistered, 111) AS 'dtRegistered'
	,cFirstExcessEst
	,cSalvage
	,cClaimFee
	,sSubCauseCode
	,sFinAccount
	,CONVERT(CHAR(10), dPolicyCoverStart, 111) AS 'dPolicyCoverStart'
	,CONVERT(CHAR(10), dPolicyCoverEnd, 111) AS 'dPolicyCoverEnd'
	,iMasterClaimID
	,BurningCostID
	,CorporateEntityID
	,CatastropheId
	,RIArrangementID
FROM Claims cl
INNER JOIN SKI_INT.v_Get_Delta_ClaimIDs DeltaClaims(NOLOCK) ON DeltaClaims.iClaimID = cl.iClaimID


GO
/****** Object:  View [SKI_INT].[v_Get_Delta_ClaimTransactionIDs]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_ClaimTransactionIDs]
AS
SELECT iTransactionID
	,iClaimantID
FROM Ski_int.Delta_ClaimTransactions (NOLOCK)

GO
/****** Object:  View [SKI_INT].[EDW_Claimants_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Claimants_Delta]
AS
SELECT DISTINCT cl.iClaimantID
	,iClaimID
	,sRelationCode
	,sClaimantName
	,iClaimantAddressID
	,sDescription
	,cAmtClaimed
	,cTotExcess
	,cExcessRecovered
	,cAmountPaid
	,cRecovered
	,sAccountHolder
	,sAccountNumber
	,sBranchCode
	,sBankName
	,sCCAccountNumber
	,sCCExpiryDate
	,sCCControlDigits
	,bCreditCard
	,sCreditCardType
	,sAccType
	,sVATType
	,sVATNo
	,sStatus
	,bExcludeCLAmt
	,sLinkFilename
	,cSalvage
	,iVendorID
	,sFinAccount
	,sExternalID
	,ClaimCategory
FROM Claimants cl
INNER JOIN SKI_INT.v_Get_Delta_ClaimTransactionIDs DELTA (NOLOCK) ON DELTA.iClaimantID = cl.iClaimantID


GO
/****** Object:  View [SKI_INT].[EDW_ClaimTran_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_ClaimTran_Delta]
AS
SELECT DISTINCT ct.iTransactionID
	,iClaimID
	,ct.iClaimantID
	,dtTransaction
	,dtPeriodEnd
	,sTransactionCode
	,cValue
	,sReference
	,cVATValue
	,iPayMethod
	,sRequestedBy
	,bReversed
	,iParentPaymentID
	,dtPeriodStart
	,sFinAccount
FROM ClaimTransactions ct (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_ClaimTransactionIDs Delta (NOLOCK) ON Delta.iClaimantID = ct.iClaimantID
	AND Delta.iTransactionID = CT.iTransactionID

GO
/****** Object:  View [SKI_INT].[EDW_Customer_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [SKI_INT].[EDW_Customer_Delta]
AS
SELECT c.iCustomerID
	,iAgentID
	,sLastName
	,sFirstName
	,sInitials
	,convert(char(10),dDateCreated,111) as 'dDateCreated'
	,convert(char(10),dLastUpdated,111) as 'dLastUpdated'
	,case when bCompanyInd = 0 then 'Individual' else 'Company' end as 'bCompanyInd'
	,sUpdatedBy
	,rPostAddressID
	,rPhysAddressID
	,rWorkAddressID
	,sCompanyName
	,sContactName
	,c.iTranID
	,sGroupRelationShip
	,sLanguage
    ,LL.sDescription AS 'sLanguage_Desc'
    ,C.CountryId
	,cntry.Name as 'Country_Desc'
FROM Customer c (NOLOCK)
INNER JOIN SKI_INT.Delta_Customer DELTA(NOLOCK) ON DELTA.iCustomerID = c.iCustomerID
	AND DELTA.iTranID = c.iTranID
INNER JOIN cfg.Country cntry (NOLOCK) ON cntry.Id = c.CountryId
LEFT JOIN Lookup LL WITH (NOEXPAND NOLOCK) 
  ON LL.sDBValue = sLanguage 
  AND LL.sGroup = 'DisplayLang'

UNION ALL

SELECT c.iCustomerID
	,iAgentID
	,sLastName
	,sFirstName
	,sInitials
	,convert(char(10),dDateCreated,111) as 'dDateCreated'
	,convert(char(10),dLastUpdated,111) as 'dLastUpdated'
	,case when bCompanyInd = 0 then 'Individual' else 'Company' end as 'bCompanyInd'
	,sUpdatedBy
	,rPostAddressID
	,rPhysAddressID
	,rWorkAddressID
	,sCompanyName
	,sContactName
	,c.iTranID
	,sGroupRelationShip
	,sLanguage
    ,LL.sDescription AS 'sLanguage_Desc'
    ,C.CountryId
	,cntryc.Name as 'Country_Desc'
FROM Arch_Customer c (NOLOCK)
INNER JOIN SKI_INT.Delta_Customer DELTA (NOLOCK) ON DELTA.iCustomerID = c.iCustomerID
	AND DELTA.iTranID = c.iTranID
INNER JOIN cfg.Country cntryc ON cntryc.Id = c.CountryId
LEFT JOIN Lookup LL WITH (NOEXPAND NOLOCK) 
  ON LL.sDBValue = sLanguage 
  AND LL.sGroup = 'DisplayLang'




GO
/****** Object:  View [SKI_INT].[EDW_DebitNotes_Delta]    Script Date: 25/02/2018 02:04:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SKI_INT].[EDW_DebitNotes_Delta]
AS
SELECT pt.*
FROM DebitNoteBalance pt (NOLOCK)
INNER JOIN SKI_INT.Delta_DebitNoteBalance delta (NOLOCK) ON delta.idebitNoteID = pt.iDebitNoteId


GO
