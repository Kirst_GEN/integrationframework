/**************************************************************************************
    This script creates a script to generate and SSIS Environment and its variables.
    Replace the necessary entries to create a new envrionment
    ***NOTE: variables marked as sensitive have their values masked with '<REPLACE_ME>'.
        These values will need to be replace with the actual values
**************************************************************************************/

DECLARE @ReturnCode INT=0, @folder_id bigint

/*****************************************************
    Variable declarations, make any changes here
*****************************************************/
DECLARE @folder sysname = 'Bi360' /* this is the name of the new folder you want to create */
        , @env sysname = 'UAT' /* this is the name of the new environment you want to create */
        , @PackageToExecute nvarchar(255)= N'FlatFiles'
        , @pFileName nvarchar(255)= N'SKI_EDW_%'
        , @sClientName nvarchar(255)= N'CTU'
        , @sFileType nvarchar(255)= N'.txt'
        , @SKiConfig nvarchar(255)= N'Data Source=CTUBRYSQL01\DEV;Initial Catalog=SKi_CTU_UAT;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False'
        , @varCompressionType nvarchar(255)= N'.zip'
        , @varDeltaProc nvarchar(255)= N'EXEC SKI_INT.SetDeltaValues'
        , @varProcessExecuted nvarchar(255)= N'BI360'
;
/* Starting the transaction */
BEGIN TRANSACTION
    IF NOT EXISTS (SELECT 1 FROM [SSISDB].[catalog].[folders] WHERE name = @folder)
    BEGIN
        RAISERROR('Creating folder: %s ...', 10, 1, @folder) WITH NOWAIT;
        EXEC @ReturnCode = [SSISDB].[catalog].[create_folder] @folder_name=@folder, @folder_id=@folder_id OUTPUT
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;
    END

    RAISERROR('Creating Environtment: %s', 10, 1, @env) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment] @folder_name=@folder, @environment_name=@env
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    /******************************************************
        Variable creation
    ******************************************************/
    RAISERROR('Creating variable: PackageToExecute ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'PackageToExecute'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@PackageToExecute
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: pFileName ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'pFileName'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@pFileName
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: sClientName ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'sClientName'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@sClientName
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: sFileType ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'sFileType'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@sFileType
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: SKiConfig ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'SKiConfig'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@SKiConfig
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: varCompressionType ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'varCompressionType'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@varCompressionType
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: varDeltaProc ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'varDeltaProc'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@varDeltaProc
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: varProcessExecuted ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'varProcessExecuted'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@varProcessExecuted
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

COMMIT TRANSACTION
RAISERROR(N'Complete!', 10, 1) WITH NOWAIT;
GOTO EndSave

QuitWithRollback:
IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
RAISERROR(N'Variable creation failed', 16,1) WITH NOWAIT;

EndSave:
GO
