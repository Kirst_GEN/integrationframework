IF NOT EXISTS
	(
	SELECT * FROM SYS.tables TAB
	INNER JOIN SYS.schemas SC
		ON SC.schema_id = TAB.schema_id
	WHERE SC.name = 'SKI_INT'
		AND TAB.name = 'IntegrationMasterfILES'
	)
BEGIN
	CREATE TABLE [SKI_INT].[IntegrationMasterFiles](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[iCustomerID] [int] NOT NULL,
		[sFileName] [varchar](100) NOT NULL,
		[sFileDirection] [varchar](20) NULL,
		[sFileLocation] [varchar](250) NULL,
		[sFileProcessedLocation] [varchar](250) NULL,
		[sFileArchiveLocation] [varchar](250) NULL,
		[sPackageFolderName] [varchar](100) NULL,
		[sPackageProjectName] [varchar](100) NULL,
		[sPackageName] [varchar](100) NULL,
		[iStatusID] [int] NOT NULL,
		[sCreatedBy] [varchar](50) NOT NULL,
		[dCreated] [date] NOT NULL,
	 CONSTRAINT [PK_IntegrationMasterFiles] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'TABLE SKI_INT.IntegrationMasterFiles ALREADY EXISTS'
END