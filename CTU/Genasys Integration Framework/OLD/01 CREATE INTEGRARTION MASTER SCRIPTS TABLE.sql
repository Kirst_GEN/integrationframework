IF NOT EXISTS
	(
	SELECT * FROM SYS.tables TAB
	INNER JOIN SYS.schemas SC
		ON SC.schema_id = TAB.schema_id
	WHERE SC.name = 'SKI_INT'
		AND TAB.name = 'IntegrationMasterScripts'
	)
BEGIN
	CREATE TABLE [SKI_INT].[IntegrationMasterScripts](
		[iScriptID] [int] IDENTITY(1,1) NOT NULL,
		[iIntegrationMasterFileID] [int] NOT NULL,
		[sScriptName] [varchar](50) NOT NULL,
		[sScriptDesc] [varchar](100) NULL,
		[iFileFormatID] [int] NOT NULL,
		[sSQLSript] [text] NULL,
		[sColumnNames] [varchar](250) NULL,
		[sColumnSizes] [varchar](250) NULL,
		[sDelimeter] [varchar](5) NULL,
		[bHasHeaderRow] [varchar](5) NULL,
		[sHeaderRowSQLScript] [varchar](250) NULL,
		[bHasFooterRow] [varchar](5) NULL,
		[sFooterRowSQLScript] [varchar](250) NULL,
		[sCreatedBy] [varchar](50) NOT NULL,
		[dCreated] [date] NOT NULL,
		[ExportSequenceNumber] [int] NULL,
	 CONSTRAINT [PK_CL_IntegrationMasterScripts] PRIMARY KEY CLUSTERED 
	(
		[iScriptID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'TABLE SKI_INT.IntegrationMasterScripts ALREADY EXISTS'
END