USE [SKi_CTU_DEV]
GO

/****** Object:  UserDefinedFunction [SKI_INT].[fn_GetCompanyIDAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create FUNCTION [SKI_INT].[fn_GetCompanyIDAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS  
BEGIN 
DECLARE @ReturnValue Bigint

SELECT @ReturnValue = 
(
	SELECT TOP 1
		COMP.iCommEntityID AS CompanyID
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH (NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities COMP WITH(NOLOCK) ON PC.iLev1EntityID = COMP.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND CONVERT(VARCHAR(10), @DateOfLoss, 120) BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, PT.dPeriodStart, PT.dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue

END

GO

/****** Object:  UserDefinedFunction [SKI_INT].[fn_GetAgentIDAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create FUNCTION [SKI_INT].[fn_GetAgentIDAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS  
BEGIN 
DECLARE @ReturnValue bigint
SELECT @ReturnValue = 
(
	SELECT TOP 1 SA.iCommEntityID AS SubAgentID
	FROM PolicyTransactions PT 
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH(NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities SA WITH(NOLOCK) ON PC.iLev3EntityID = SA.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND @DateOfLoss BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, dPeriodStart, dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue
END

GO

/****** Object:  UserDefinedFunction [SKI_INT].[fn_GetBrokerIDAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [SKI_INT].[fn_GetBrokerIDAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS
BEGIN 
	DECLARE @ReturnValue bigint
SELECT @ReturnValue = 
(
	SELECT TOP 1
		BROK.iCommEntityID AS [BrokerID]
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH (NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN dbo.CommEntities BROK WITH(NOLOCK) ON PC.iLev2EntityID = BROK.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND CONVERT(VARCHAR(10), @DateOfLoss, 120) BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, PT.dPeriodStart, PT.dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCompanyAtDateOfLoss_LessMinPeriodStart]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetCompanyAtDateOfLoss_LessMinPeriodStart](@ClaimID INT, @ClaimPolicyID INT) RETURNS VARCHAR(50)
AS  
BEGIN
DECLARE @ReturnValue VARCHAR(50)
SELECT @ReturnValue = 
(
	SELECT TOP 1
		COMP.sName AS Company
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH (NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities COMP WITH(NOLOCK) ON PC.iLev1EntityID = COMP.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	GROUP BY PT.iPolicyId, CONVERT(VARCHAR(10), PT.dPeriodStart, 120), PT.iTrantype, COMP.sName, CL.iClaimID
	ORDER BY PT.iPolicyID, CONVERT(VARCHAR(10), PT.dPeriodStart, 120), PT.iTrantype
)

RETURN @ReturnValue

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCompanyAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetCompanyAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS  
BEGIN 
DECLARE @ReturnValue VARCHAR(50)

SELECT @ReturnValue = 
(
	SELECT TOP 1
		COMP.sName AS Company
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH (NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities COMP WITH(NOLOCK) ON PC.iLev1EntityID = COMP.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND CONVERT(VARCHAR(10), @DateOfLoss, 120) BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, PT.dPeriodStart, PT.dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetBrokerAtDateofLoss_LessMinPeriodStart]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetBrokerAtDateofLoss_LessMinPeriodStart](@ClaimID INT, @ClaimPolicyID INT) RETURNS VARCHAR(50)

AS  

BEGIN 
DECLARE @ReturnValue VARCHAR(50)
SELECT @ReturnValue = 
(
	SELECT TOP 1
		BROK.sName AS [Broker]
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH(NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities BROK (NOLOCK) ON PC.iLev2EntityID = BROK.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	GROUP BY PT.iPolicyId, CONVERT(VARCHAR(10),PT.dPeriodStart, 120), PT.iTrantype, BROK.sName, CL.iClaimID
	ORDER BY PT.iPolicyID, CONVERT(VARCHAR(10),PT.dPeriodStart, 120), PT.iTrantype
)
	RETURN @ReturnValue
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetBrokerAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetBrokerAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS
BEGIN 
	DECLARE @ReturnValue VARCHAR(50)
SELECT @ReturnValue = 
(
	SELECT TOP 1
		BROK.sName AS [Broker]
	FROM PolicyTransactions PT WITH(NOLOCK)
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH (NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN dbo.CommEntities BROK WITH(NOLOCK) ON PC.iLev2EntityID = BROK.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND CONVERT(VARCHAR(10), @DateOfLoss, 120) BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, PT.dPeriodStart, PT.dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAgentAtDateOfLoss_LessMinPeriodStart]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetAgentAtDateOfLoss_LessMinPeriodStart](@ClaimID INT, @ClaimPolicyID INT) RETURNS VARCHAR(50)
AS  
BEGIN 
DECLARE @ReturnValue VARCHAR(50)
SELECT @ReturnValue = 
(
	SELECT TOP 1 SA.sName AS SubAgent
	FROM PolicyTransactions PT 
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH(NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities SA WITH(NOLOCK) ON PC.iLev3EntityID = SA.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	GROUP BY PT.iPolicyId, CONVERT(VARCHAR(10), PT.dPeriodStart, 120), PT.iTrantype, SA.sName, CL.iClaimID
	ORDER BY PT.iPolicyID, CONVERT(VARCHAR(10), PT.dPeriodStart, 120), PT.iTrantype
)

RETURN @ReturnValue
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAgentAtDateOfLoss]    Script Date: 16/01/2018 08:58:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GetAgentAtDateOfLoss](@ClaimID INT, @ClaimPolicyID INT, @DateOfLoss DATETIME) RETURNS VARCHAR(50)
AS  
BEGIN 
DECLARE @ReturnValue VARCHAR(50)
SELECT @ReturnValue = 
(
	SELECT TOP 1 SA.sName AS SubAgent
	FROM PolicyTransactions PT 
	LEFT JOIN PolicyCommStruct PC WITH(NOLOCK) ON PT.iAgencyId = PC.iVersionNo AND PT.iPolicyId = PC.iPolicyID
	INNER JOIN [Lookup] TTLK WITH(NOEXPAND, NOLOCK) ON TTLK.iIndex = PT.iTrantype AND TTLK.sGroup = 'TransactionType'
	INNER JOIN Claims CL WITH(NOLOCK) ON CL.iPolicyID = PT.iPolicyId
	INNER JOIN CommEntities SA WITH(NOLOCK) ON PC.iLev3EntityID = SA.iCommEntityID
	WHERE PT.iParentTranID = -1 AND PT.iTrantype IN (6, 9)
	AND PT.iPolicyId = @ClaimPolicyID 
	AND CL.iClaimID = @ClaimID
	AND CONVERT(VARCHAR(10), CL.dtEvent, 120) = @DateOfLoss
	AND @DateOfLoss BETWEEN CONVERT(VARCHAR(10), PT.dPeriodStart, 120) AND CONVERT(VARCHAR(10), PT.dPeriodEnd, 120)
	ORDER BY CL.iPolicyID, dPeriodStart, dPeriodEnd, PT.iTrantype
)

RETURN @ReturnValue
END

GO


