USE [SKi_CTU_DEV]
GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportDebitNoteBalance]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportDebitNoteBalance] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_DebitNoteBalance'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_DebitNoteBalance'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0 FROM SKI_INT.EDW_DebitNoteBalance'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_DebitNoteBalance'' ,0 FROM SKI_INT.EDW_DebitNoteBalance'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_DebitNoteBalance_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_DebitNoteBalance_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_DebitNoteBalance;

INSERT INTO SKI_INT.EDW_DebitNoteBalance
SELECT 
 *
FROM 
  SKI_INT.EDW_DebitNotes_Delta NORM (NOLOCK)
  
GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportFieldCodes]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportFieldCodes] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_FieldCodes'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_FieldCodes'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_FieldCodes'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_FieldCodes'' ,0 FROM SKI_INT.EDW_FieldCodes'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_FieldCodes'' ,0 FROM SKI_INT.EDW_FieldCodes'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportProducts]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportProducts] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Products'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Products'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Products'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Products'' ,0 FROM SKI_INT.EDW_Products'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Products'' ,0 FROM SKI_INT.EDW_Products'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Products_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Products_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Products;

INSERT INTO SKI_INT.EDW_Products
SELECT 
 iProductID, sProductName, sProductCode, sInsurer
FROM
  Products CV (NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimCatastrophe_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimCatastrophe_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimCatastrophe;

INSERT INTO SKI_INT.EDW_ClaimCatastrophe
SELECT 
 id AS CatastropheID
 , Name
 , Description
 , StartDate
 , EndDate
 , AffectedArea
 , DateLastUpdated
 , UserLastUpdated
FROM
  cfg.Catastrophe CV (NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimCatastrophes]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimCatastrophes] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimCatastrophe'
		) a
        
	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimCatastrophe'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0 FROM SKI_INT.EDW_ClaimCatastrophe'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimCatastrophe'' ,0 FROM SKI_INT.EDW_ClaimCatastrophe'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimVendors]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimVendors] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimVendors'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimVendors'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimVendors'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimVendors'' ,0 FROM SKI_INT.EDW_ClaimVendors'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimVendors'' ,0 FROM SKI_INT.EDW_ClaimVendors'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimVendors_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimVendors_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimVendors;

INSERT INTO SKI_INT.EDW_ClaimVendors
SELECT 
 cID AS iVendorID
 , sName AS VendorName
 , sType AS VendorType_Desc
 , sVATRate AS VATRate
 , sVATNo AS VatNo
 , sAddressLine1
 , sAddressLine2
 , sAddressLine3
 , sSuburb
 , sPostalCode
 , sPhoneNo
 , sFaxNo
 , sEmail 
 , dLastUpdated
FROM
  ClaimVendors CV (NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_RiskDetails_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_RiskDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_RiskDetails;

INSERT INTO SKI_INT.EDW_RiskDetails (
	intPolicyID
	,iRiskID
	,dDateModified
	,iTranID
	,ZIPCODE
	,R_MOTORSEATS_ID
	,R_MOTORSEATS
	,R_MOTORYOM
	,R_MOTORREG
	,R_ENGIN_NO
	,R_CHASSISNO
	,R_COVERDETAILS
	,R_G_MARKETVAL
	,R_G_PASLIABIND
	,R_G_NUOFSEATS
	,R_G_HPLEASE
	,R_G_EXTENDLOU
	,R_G_PERSACCIND
	,R_G_CREDSFALL
	,R_G_HELIVAC
	,R_G_ABSVIOCRE
	,R_G_ISAFE
	,R_G_EXCESSRO_ID
	,R_G_EXCESSRO
	,R_G_WINDSCREEN
	,R_G_BACK2INV_ID
	,R_G_BACK2INV
	,R_G_CASHBACK
	,R_G_DEALER_ID
	,R_G_DEALER
	,R_G_INTRSTNOTED
	,R_G_CREDITPRO_ID
	,R_G_CREDITPRO
	,R_G_CRACCNO
	,R_G_ASSOC_ID
	,R_G_ASSOC
	,R_G_STDENDORSE
	,R_G_ADDFAP2000
	,R_G_ADDFAP2500
	,R_G_ADDFAP5000
	,R_G_ADDFAP7500
	,R_G_ADDFAP10000
	,R_G_ADDFAP15000
	,R_G_ADDFAP20000
	,R_G_AMATEKSI
	,R_G_AMAACCNO
	,R_G_AMAIDNO
	,R_MATCHINGCT
	,R_CTPERACC
	,R_CTHPLEASE
	,R_CTPASS
	,R_CTEXTENDLOU
	,R_CTHELIVAC
	,R_CTAMACLA
	,R_CTBACK2INV
	,R_CTISAFE
	,R_CTCASHBACK
	,R_CTCREDITSF
	,R_CTEXCESSRO
	,R_TSASRIA
	,R_MATCHINGPERC
	,R_MORE5VEH
	,R_SI
	,R_G_PASSENSI
	,R_G_PASSENDT
	,R_G_HPLEASESI
	,R_G_PERACCSI
	,R_G_EXTENDLOUSI
	,R_G_HELIVACSI
	,R_G_CSFSI
	,R_G_ABSVIOCRESI
	,R_G_ISAFESI
	,R_G_EXCESSROSI
	,R_G_WINDSCRSI
	,R_G_BACK2INVSI
	,R_G_CASHBACKSI
	,R_G_AMAPREMSI
	,R_G_VEHDEF_ID
	,R_G_VEHDEF
	,R_G_TRACKCUST_ID
	,R_G_TRACKCUST
	,R_G_TVEHDEF_ID
	,R_G_TVEHDEF
	,R_G_TRAILERLOI
	,R_G_LOANPRTPR
	,R_G_DRVDEATHSI
	,R_MATCHING
	,R_G_PASDEATHSI
	,R_G_TRACKREFNO
	,R_Driver
	,R_DriveName1
	,R_DriveID1
	,R_DriveCell1
	,R_DriveName2
	,R_DriveID2
	,R_DriveCell2
	,R_DriveName3
	,R_DriveID3
	,R_DriveCell3
	,R_DriveName4
	,R_DriveID4
	,R_DriveCell4
	,R_DriveName5
	,R_DriveID5
	,R_DriveCell5
	,R_DriveName6
	,R_DriveID6
	,R_DriveCell6
	,R_G_ADDFAPG1000
	,R_G_ADDFAPG2000
	,R_G_ADDFAPG3000
	,R_G_TPOSI
	,R_G_HPLEASEDT
	,R_G_EXTENDLOUDT
	,R_G_LOSSINOPSI
	,R_G_HELIVACDT
	,R_G_ISAFEDT
	,R_G_PERACCDT
	,R_G_CSFDT
	,R_G_ABSVIOCREDT
	,R_G_EXCESSRODT
	,R_G_WINDSCRDT
	,R_G_BACK2INVDT
	,R_G_CASHBACKDT
	,R_ISFCSHBCK_CPC
	,R_LOI_TOUR_CPC
	)
SELECT intPolicyID
	,iRiskID
	,dDateModified
	,iTranID
	,ZIPCODE
	,MOTORSEATS.sDBValue AS R_MOTORSEATS_ID
	,R_MOTORSEATS
	,R_MOTORYOM
	,R_MOTORREG
	,R_ENGIN_NO
	,R_CHASSISNO
	,R_COVERDETAILS
	,R_G_MARKETVAL
	,R_G_PASLIABIND
	,R_G_NUOFSEATS
	,R_G_HPLEASE
	,R_G_EXTENDLOU
	,R_G_PERSACCIND
	,R_G_CREDSFALL
	,R_G_HELIVAC
	,R_G_ABSVIOCRE
	,R_G_ISAFE
	,EXCESSRO.sDBValue AS R_G_EXCESSRO_ID
	,R_G_EXCESSRO
	,R_G_WINDSCREEN
	,BACK2INV.sDBValue AS R_G_BACK2INV_ID
	,R_G_BACK2INV
	,R_G_CASHBACK
	,DEALER.sDBValue AS R_G_DEALER_ID
	,R_G_DEALER
	,R_G_INTRSTNOTED
	,CREDITPRO.sDBValue AS R_G_CREDITPRO_ID
	,R_G_CREDITPRO
	,R_G_CRACCNO
	,ASSOC.sDBValue AS R_G_ASSOC_ID
	,R_G_ASSOC
	,R_G_STDENDORSE
	,R_G_ADDFAP2000
	,R_G_ADDFAP2500
	,R_G_ADDFAP5000
	,R_G_ADDFAP7500
	,R_G_ADDFAP10000
	,R_G_ADDFAP15000
	,R_G_ADDFAP20000
	,R_G_AMATEKSI
	,R_G_AMAACCNO
	,R_G_AMAIDNO
	,R_MATCHINGCT
	,R_CTPERACC
	,R_CTHPLEASE
	,R_CTPASS
	,R_CTEXTENDLOU
	,R_CTHELIVAC
	,R_CTAMACLA
	,R_CTBACK2INV
	,R_CTISAFE
	,R_CTCASHBACK
	,R_CTCREDITSF
	,R_CTEXCESSRO
	,R_TSASRIA
	,R_MATCHINGPERC
	,R_MORE5VEH
	,R_SI
	,R_G_PASSENSI
	,R_G_PASSENDT
	,R_G_HPLEASESI
	,R_G_PERACCSI
	,R_G_EXTENDLOUSI
	,R_G_HELIVACSI
	,R_G_CSFSI
	,R_G_ABSVIOCRESI
	,R_G_ISAFESI
	,R_G_EXCESSROSI
	,R_G_WINDSCRSI
	,R_G_BACK2INVSI
	,R_G_CASHBACKSI
	,R_G_AMAPREMSI
	,VEHDEF.sDBValue AS R_G_VEHDEF_ID
	,R_G_VEHDEF
	,TRACKCUST.sDBValue AS R_G_TRACKCUST_ID
	,R_G_TRACKCUST
	,TVEHDEF.sDBValue AS R_G_TVEHDEF_ID
	,R_G_TVEHDEF
	,R_G_TRAILERLOI
	,R_G_LOANPRTPR
	,R_G_DRVDEATHSI
	,R_MATCHING
	,R_G_PASDEATHSI
	,R_G_TRACKREFNO
	,R_Driver
	,R_DriveName1
	,R_DriveID1
	,R_DriveCell1
	,R_DriveName2
	,R_DriveID2
	,R_DriveCell2
	,R_DriveName3
	,R_DriveID3
	,R_DriveCell3
	,R_DriveName4
	,R_DriveID4
	,R_DriveCell4
	,R_DriveName5
	,R_DriveID5
	,R_DriveCell5
	,R_DriveName6
	,R_DriveID6
	,R_DriveCell6
	,R_G_ADDFAPG1000
	,R_G_ADDFAPG2000
	,R_G_ADDFAPG3000
	,R_G_TPOSI
	,R_G_HPLEASEDT
	,R_G_EXTENDLOUDT
	,R_G_LOSSINOPSI
	,R_G_HELIVACDT
	,R_G_ISAFEDT
	,R_G_PERACCDT
	,R_G_CSFDT
	,R_G_ABSVIOCREDT
	,R_G_EXCESSRODT
	,R_G_WINDSCRDT
	,R_G_BACK2INVDT
	,R_G_CASHBACKDT
	,R_ISFCSHBCK_CPC
	,R_LOI_TOUR_CPC
FROM SKI_INT.st_RiskDetails_Normal NORM
LEFT JOIN Lookup MOTORSEATS ON MOTORSEATS.sGroup = 'SATSEATS'
	AND RTRIM(MOTORSEATS.sDescription) = RTRIM(NORM.R_MOTORSEATS)
LEFT JOIN Lookup EXCESSRO ON EXCESSRO.sGroup = 'SATERO'
	AND RTRIM(EXCESSRO.sDescription) = RTRIM(NORM.R_G_EXCESSRO)
LEFT JOIN Lookup BACK2INV ON BACK2INV.sGroup = 'SATBACK2INV'
	AND RTRIM(BACK2INV.sDescription) = RTRIM(NORM.R_G_BACK2INV)
LEFT JOIN Lookup DEALER ON DEALER.sGroup = 'SATDEALER'
	AND RTRIM(DEALER.sDescription) = RTRIM(NORM.R_G_DEALER)
LEFT JOIN Lookup CREDITPRO ON CREDITPRO.sGroup = 'SATCREDIT'
	AND RTRIM(CREDITPRO.sDescription) = RTRIM(NORM.R_G_CREDITPRO)
LEFT JOIN Lookup ASSOC ON ASSOC.sGroup = 'SATASSOC'
	AND RTRIM(ASSOC.sDescription) = RTRIM(NORM.R_G_ASSOC)
LEFT JOIN Lookup VEHDEF ON VEHDEF.sGroup = 'SATVEHDEF'
	AND RTRIM(VEHDEF.sDescription) = RTRIM(NORM.R_G_VEHDEF)
LEFT JOIN Lookup TRACKCUST ON TRACKCUST.sGroup = 'YesNo'
	AND RTRIM(TRACKCUST.sDescription) = RTRIM(NORM.R_G_TRACKCUST)
LEFT JOIN Lookup TVEHDEF ON TVEHDEF.sGroup = 'SATDEFVEH'
	AND RTRIM(TVEHDEF.sDescription) = RTRIM(NORM.R_G_TVEHDEF);;
GO

/****** Object:  StoredProcedure [SKI_INT].[UspINT_SKiSTGIntegrationDropTable]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationDropTable] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE @SQLCommand VARCHAR(2000)

	SET @SQLCommand = 'DROP TABLE ' + N'' + @TableName

	EXEC (@SQLCommand);
END

GO

/****** Object:  StoredProcedure [SKI_INT].[UspINT_SKiSTGIntegrationCreateTable]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[UspINT_SKiSTGIntegrationCreateTable] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE @SQLCommand VARCHAR(2000)

	SET @SQLCommand = 'CREATE TABLE ' + N'' + @TableName + '([iID] [INT] IDENTITY(1,1) NOT NULL, [sDataRecord] [VARCHAR](MAX) NULL, [dtDate] [DATETIME] NULL, [sFileName] [VARCHAR](250) NULL, [iProcessStatus] [INT] NULL)'

	EXEC (@SQLCommand);
END

GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportRisks]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportRisks] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Risks'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Risks'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Risks'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Risks'' ,0 FROM SKI_INT.EDW_Risks'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Risks'' ,0 FROM SKI_INT.EDW_Risks'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportRiskDetails]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--EXEC SKI_INT.EDW_ExportRiskDetails 'SKI_INT.SKI_EDW_RiskDetails_20180115150759'

CREATE PROCEDURE [SKI_INT].[EDW_ExportRiskDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_RiskDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_RiskDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	     SELECT 
        CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
        ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
        END AS 'Name'
	    FROM (
		    SELECT CASE 
				    WHEN patindex('%[.]%', c.NAME) > 0
					    THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				    ELSE c.NAME
				    END AS 'Name'
                    , c.system_type_id
		    FROM sys.tables t
		    JOIN sys.columns c ON t.object_id = c.object_id
			    AND t.NAME = 'EDW_RiskDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_RiskDetails'' ,0 FROM SKI_INT.EDW_RiskDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_RiskDetails'' ,0 FROM SKI_INT.EDW_RiskDetails'

	exec (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTranSummary]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTranSummary] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTranSummary'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTranSummary'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTranSummary'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTranSummary'' ,0 FROM SKI_INT.EDW_PolTranSummary'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTranSummary'' ,0 FROM SKI_INT.EDW_PolTranSummary'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTranDet]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTranDet] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTranDet'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTranDet'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|'''  
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTranDet'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTranDet'' ,0 FROM SKI_INT.EDW_PolTranDet'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTranDet'' ,0 FROM SKI_INT.EDW_PolTranDet'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolTran]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolTran'' ,0 FROM SKI_INT.EDW_PolTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolTran'' ,0 FROM SKI_INT.EDW_PolTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicyDetails]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicyDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolicyDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolicyDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolicyDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolicyDetails'' ,0 FROM SKI_INT.EDW_PolicyDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolicyDetails'' ,0 FROM SKI_INT.EDW_PolicyDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicyClaimTran]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicyClaimTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_PolicyClaimTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_PolicyClaimTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0 FROM SKI_INT.EDW_PolicyClaimTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_PolicyClaimTran'' ,0 FROM SKI_INT.EDW_PolicyClaimTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportPolicy]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportPolicy] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Policy'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Policy'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Policy'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Policy'' ,0 FROM SKI_INT.EDW_Policy'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Policy'' ,0 FROM SKI_INT.EDW_Policy'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportCustomerDetails]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportCustomerDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_CustomerDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_CustomerDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_CustomerDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_CustomerDetails'' ,0 FROM SKI_INT.EDW_CustomerDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_CustomerDetails'' ,0 FROM SKI_INT.EDW_CustomerDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportCustomer]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportCustomer] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Customer'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Customer'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Customer'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Customer'' ,0 FROM SKI_INT.EDW_Customer'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Customer'' ,0 FROM SKI_INT.EDW_Customer'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END

GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimTran]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimTran] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimTran'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimTran'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimTran'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimTran'' ,0 FROM SKI_INT.EDW_ClaimTran'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimTran'' ,0 FROM SKI_INT.EDW_ClaimTran'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaims]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaims] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Claims'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Claims'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Claims'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Claims'' ,0 FROM SKI_INT.EDW_Claims'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Claims'' ,0 FROM SKI_INT.EDW_Claims'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimDetails]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimDetails] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_ClaimDetails'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_ClaimDetails'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	    SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_ClaimDetails'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_ClaimDetails'' ,0 FROM SKI_INT.EDW_ClaimDetails'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_ClaimDetails'' ,0 FROM SKI_INT.EDW_ClaimDetails'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportClaimants]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportClaimants] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + '|'
	FROM (
		SELECT c.NAME
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Claimants'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Claimants'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @sDataRecord = ''
	SET @SQLInsertDataCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + ' + '
	FROM (
		SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Claimants'
			) a
		) b

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
			)
	SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Claimants'' ,0 FROM SKI_INT.EDW_Claimants'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Claimants'' ,0 FROM SKI_INT.EDW_Claimants'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END

GO

/****** Object:  StoredProcedure [SKI_INT].[EDW_ExportAddress]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[EDW_ExportAddress] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(2000)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + '|'
	FROM (
		SELECT c.NAME
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'EDW_Address'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''SKi_EDW_Address'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @sDataRecord = ''
	SET @SQLInsertDataCommand = ''

	SELECT @sDataRecord = @sDataRecord + NAME + ' + '
	FROM (
		SELECT 
            CASE WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
            ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'EDW_Address'
			) a
		) b

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
			)
	SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''SKi_EDW_Address'' ,0 FROM SKI_INT.EDW_Address'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''SKi_EDW_Address'' ,0 FROM SKI_INT.EDW_Address'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolicyDetails_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolicyDetails_Staging]
AS

TRUNCATE TABLE SKI_INT.EDW_PolicyDetails
;

INSERT INTO SKI_INT.EDW_PolicyDetails
SELECT 
  iPolicyID
  ,dDateModified
  ,iTranID
  ,L_ServiceProv.sDBValue AS P_ServiceProv_ID
  ,P_ServiceProv
  ,P_MARKETCODE
  ,P_CASENO
  ,L_P_MORE5VEH.sDBValue AS P_MORE5VEH_ID
  ,P_MORE5VEH
  ,L_P_BANKPROTIND.sDBValue AS P_BANKPROTIND_ID
  ,P_BANKPROTIND
  ,P_BANKPROTPNO
  ,L_P_PREMMATREAS.sDBValue AS P_PREMMATREAS_ID
  ,P_PREMMATREAS
FROM 
  SKI_INT.st_PolicyDetails_Normal NORM (NOLOCK)
  LEFT JOIN Lookup L_ServiceProv ON L_ServiceProv.sGroup = 'YesNo'
	AND RTRIM(L_ServiceProv.sDescription) = RTRIM(NORM.P_ServiceProv)
  LEFT JOIN Lookup L_P_MORE5VEH ON L_P_MORE5VEH.sGroup = 'YesNo'
	AND RTRIM(L_P_MORE5VEH.sDescription) = RTRIM(NORM.P_MORE5VEH)
  LEFT JOIN Lookup L_P_BANKPROTIND ON L_P_BANKPROTIND.sGroup = 'YesNo'
	AND RTRIM(L_P_BANKPROTIND.sDescription) = RTRIM(NORM.P_BANKPROTIND)
  LEFT JOIN Lookup L_P_PREMMATREAS ON L_P_PREMMATREAS.sGroup = 'PremMatchReason'
	AND RTRIM(L_P_PREMMATREAS.sDescription) = RTRIM(NORM.P_PREMMATREAS);
;

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimDetails_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimDetails;

INSERT INTO SKI_INT.EDW_ClaimDetails
SELECT 
  iClaimID
  ,dLastUpdated
  ,CL_THICLA
  ,CL_PBEATER
  ,CL_BACKOFFINEG
  ,CL_CHK_243
  ,CL_CHK_000
  ,CL_CHK_002
  ,CL_CHK_003
  ,CL_REPUDI
  ,CL_REPUDE
  ,CL_VEHDRI
  ,CL_DATOIN
  ,CL_DATOID
  ,CL_CHK_010
  ,CL_APPDATE
  ,CL_CHK_011
  ,CL_CHK_012
  ,CL_CHK_013
  ,CL_CHK_014
  ,CL_CHK_015
  ,CL_CHK_016
  ,CL_CHK_240
  ,CL_CHK_215
  ,CL_CHK_216
  ,CL_CHK_241
  ,CL_CHK_017
  ,CL_CHK_023
  ,CL_CHK_024
  ,CL_CHK_025
  ,CL_CHK_027
  ,CL_CHK_163
  ,CL_CHK_028
  ,CL_CHK_026
  ,CL_CHK_029
  ,CL_CHK_052
  ,CL_CHK_092
  ,CL_RECCLA
  ,CL_CHK_094
  ,CL_CHK_095
  ,CL_CHK_098
  ,CL_CHK_099
  ,CL_CHK_100
  ,CL_CHK_128
  ,CL_CHK_129
  ,CL_CHK_130
  ,CL_CHK_131
  ,CL_CHK_140
  ,CL_CHK_200
  ,CL_CHK_201
  ,CL_CHK_202
  ,CL_CHK_203
  ,CL_CHK_219
  ,CL_CHK_220
  ,CL_CHK_222
  ,CL_CHK_223
  ,CL_INVAPP
  ,CL_INVNAME
  ,CL_CHK_237
  ,CL_CHK_238
  ,CL_CHK_239
  ,CL_CHK_030
  ,CL_CHK_031
  ,CL_CHK_032
  ,CL_CHK_224
  ,CL_CHK_225
  ,CL_CHK_033
  ,CL_CHK_034
  ,CL_CHK_0342
  ,CL_CHK_0343
  ,CL_CHK_0344
  ,CL_CHK_0345
  ,CL_WRTOFF
  ,CL_WOCONFDTE
  ,CL_STKNMBR
  ,CL_SALAGREE
  ,CL_SALUPLDATE
  ,CL_INVOICED
  ,CL_INVNO
  ,CL_CHK_228
  ,CL_CHK_229
  ,CL_CHK_230
  ,CL_CHK_247
  ,CL_CHK_232
  ,CL_CHK_233
  ,CL_CHK_248
  ,CL_CHK_235
  ,CL_CHK_039
  ,CL_CHK_102
  ,CL_AGTNAME
  ,CL_OSAGT
  ,CL_OSAGTREFNO
  ,CL_OSAGTAPPDATE
  ,CL_CHK_106
  ,CL_CHK_109
  ,CL_CHK_111
  ,CL_RECMER
  ,CL_CHK_115
  ,CL_CHK_116
  ,CL_TPINSURID
  ,CL_CHK_INSCONME
  ,CL_CHK_123
  ,CL_CHK_124
  ,CL_CHK_125
  ,CL_CHK_126
  ,CL_CHK_127
  ,CL_CHK_141
  ,CL_CHK_145
  ,CL_CHK_147
  ,CL_CHK_162
  ,CL_CHK_200_01
  ,CL_RECCLA_01
  ,CL_CHK_201_01
  ,CL_CHK_094_01
  ,CL_CHK_202_01
  ,CL_CHK_095_01
  ,CL_CHK_203_01
  ,CL_CHK_098_01
  ,CL_CHK_099_01
  ,CL_CHK_100_01
  ,CL_CHK_128_01
  ,CL_CHK_129_01
  ,CL_CHK_130_01
  ,CL_CHK_131_01
  ,CL_CHK_140_01
  ,CL_CHK_219_01
  ,CL_CHK_220_01
  ,CL_CHK_222_01
  ,CL_CHK_223_01
  ,CL_CHK_102_01
  ,CL_AGTNAME_01
  ,CL_OSAGT_01
  ,CL_OSAGTREFN_01
  ,CL_OSAGTAPPD_01
  ,CL_CHK_106_01
  ,CL_CHK_109_01
  ,CL_CHK_111_01
  ,CL_RECMER_01
  ,CL_CHK_115_01
  ,CL_CHK_116_01
  ,CL_TPINSURID_01
  ,CL_CHK_INSCO_01
  ,CL_CHK_123_01
  ,CL_CHK_124_01
  ,CL_CHK_125_01
  ,CL_CHK_126_01
  ,CL_CHK_127_01
  ,CL_CHK_141_01
  ,CL_CHK_145_01
  ,CL_CHK_147_01
  ,CL_CHK_162_01
  ,CL_CHK_200_02
  ,CL_RECCLA_02
  ,CL_CHK_201_02
  ,CL_CHK_094_02
  ,CL_CHK_202_02
  ,CL_CHK_095_02
  ,CL_CHK_203_02
  ,CL_CHK_098_02
  ,CL_CHK_099_02
  ,CL_CHK_100_02
  ,CL_CHK_128_02
  ,CL_CHK_129_02
  ,CL_CHK_130_02
  ,CL_CHK_131_02
  ,CL_CHK_140_02
  ,CL_CHK_219_02
  ,CL_CHK_220_02
  ,CL_CHK_222_02
  ,CL_CHK_223_02
  ,CL_CHK_102_02
  ,CL_AGTNAME_02
  ,CL_OSAGT_02
  ,CL_OSAGTREFN_02
  ,CL_OSAGTAPPD_02
  ,CL_CHK_106_02
  ,CL_CHK_109_02
  ,CL_CHK_111_02
  ,CL_RECMER_02
  ,CL_CHK_115_02
  ,CL_CHK_116_02
  ,CL_TPINSURID_02
  ,CL_CHK_INSCO_02
  ,CL_CHK_123_02
  ,CL_CHK_124_02
  ,CL_CHK_125_02
  ,CL_CHK_126_02
  ,CL_CHK_127_02
  ,CL_CHK_141_02
  ,CL_CHK_145_02
  ,CL_CHK_147_02
  ,CL_CHK_162_02
  ,CL_CHK_200_03
  ,CL_RECCLA_03
  ,CL_CHK_201_03
  ,CL_CHK_094_03
  ,CL_CHK_202_03
  ,CL_CHK_095_03
  ,CL_CHK_203_03
  ,CL_CHK_098_03
  ,CL_CHK_099_03
  ,CL_CHK_100_03
  ,CL_CHK_128_03
  ,CL_CHK_129_03
  ,CL_CHK_130_03
  ,CL_CHK_131_03
  ,CL_CHK_140_03
  ,CL_CHK_219_03
  ,CL_CHK_220_03
  ,CL_CHK_222_03
  ,CL_CHK_223_03
  ,CL_CHK_102_03
  ,CL_AGTNAME_03
  ,CL_OSAGT_03
  ,CL_OSAGTREFN_03
  ,CL_OSAGTAPPD_03
  ,CL_CHK_106_03
  ,CL_CHK_109_03
  ,CL_CHK_111_03
  ,CL_RECMER_03
  ,CL_CHK_115_03
  ,CL_CHK_116_03
  ,CL_TPINSURID_03
  ,CL_CHK_INSCO_03
  ,CL_CHK_123_03
  ,CL_CHK_124_03
  ,CL_CHK_125_03
  ,CL_CHK_126_03
  ,CL_CHK_127_03
  ,CL_CHK_141_03
  ,CL_CHK_145_03
  ,CL_CHK_147_03
  ,CL_CHK_162_03
  ,CL_CHK_200_04
  ,CL_RECCLA_04
  ,CL_CHK_201_04
  ,CL_CHK_094_04
  ,CL_CHK_202_04
  ,CL_CHK_095_04
  ,CL_CHK_203_04
  ,CL_CHK_098_04
  ,CL_CHK_099_04
  ,CL_CHK_100_04
  ,CL_CHK_128_04
  ,CL_CHK_129_04
  ,CL_CHK_130_04
  ,CL_CHK_131_04
  ,CL_CHK_140_04
  ,CL_CHK_219_04
  ,CL_CHK_220_04
  ,CL_CHK_222_04
  ,CL_CHK_223_04
  ,CL_CHK_102_04
  ,CL_AGTNAME_04
  ,CL_OSAGT_04
  ,CL_OSAGTREFN_04
  ,CL_OSAGTAPPD_04
  ,CL_CHK_106_04
  ,CL_CHK_109_04
  ,CL_CHK_111_04
  ,CL_RECMER_04
  ,CL_CHK_115_04
  ,CL_CHK_116_04
  ,CL_TPINSURID_04
  ,CL_CHK_INSCO_04
  ,CL_CHK_123_04
  ,CL_CHK_124_04
  ,CL_CHK_125_04
  ,CL_CHK_126_04
  ,CL_CHK_127_04
  ,CL_CHK_141_04
  ,CL_CHK_145_04
  ,CL_CHK_147_04
  ,CL_CHK_162_04
  ,CL_CHK_200_05
  ,CL_RECCLA_05
  ,CL_CHK_201_05
  ,CL_CHK_094_05
  ,CL_CHK_202_05
  ,CL_CHK_095_05
  ,CL_CHK_203_05
  ,CL_CHK_098_05
  ,CL_CHK_099_05
  ,CL_CHK_100_05
  ,CL_CHK_128_05
  ,CL_CHK_129_05
  ,CL_CHK_130_05
  ,CL_CHK_131_05
  ,CL_CHK_140_05
  ,CL_CHK_219_05
  ,CL_CHK_220_05
  ,CL_CHK_222_05
  ,CL_CHK_223_05
  ,CL_CHK_102_05
  ,CL_AGTNAME_05
  ,CL_OSAGT_05
  ,CL_OSAGTREFN_05
  ,CL_OSAGTAPPD_05
  ,CL_CHK_106_05
  ,CL_CHK_109_05
  ,CL_CHK_111_05
  ,CL_RECMER_05
  ,CL_CHK_115_05
  ,CL_CHK_116_05
  ,CL_TPINSURID_05
  ,CL_CHK_INSCO_05
  ,CL_CHK_123_05
  ,CL_CHK_124_05
  ,CL_CHK_125_05
  ,CL_CHK_126_05
  ,CL_CHK_127_05
  ,CL_CHK_141_05
  ,CL_CHK_145_05
  ,CL_CHK_147_05
  ,CL_CHK_162_05
  ,CL_LEGALINV
  ,CL_CHK_211
  ,CL_CHK_212
  ,CL_CHK_213
  ,CL_CHK_214
  ,L_CL_AGTNAME.sDBValue AS CL_AGTNAME_ID
  ,L_CL_AGTNAME_01.sDBValue AS CL_AGTNAME_01_ID
  ,L_CL_AGTNAME_02.sDBValue AS CL_AGTNAME_02_ID
  ,L_CL_AGTNAME_03.sDBValue AS CL_AGTNAME_03_ID
  ,L_CL_AGTNAME_04.sDBValue AS CL_AGTNAME_04_ID
  ,L_CL_AGTNAME_05.sDBValue AS CL_AGTNAME_05_ID
  ,L_CL_BACKOFFINEG.sDBValue AS CL_BACKOFFINEG_ID
  ,L_CL_CHK_000.sDBValue AS CL_CHK_000_ID, 
  L_CL_CHK_010.sDBValue AS CL_CHK_010_ID, 
  L_CL_CHK_011.sDBValue AS CL_CHK_011_ID, 
  L_CL_CHK_012.sDBValue AS CL_CHK_012_ID, 
  L_CL_CHK_013.sDBValue AS CL_CHK_013_ID, 
  L_CL_CHK_017.sDBValue AS CL_CHK_017_ID, 
  L_CL_CHK_023.sDBValue AS CL_CHK_023_ID, 
  L_CL_CHK_024.sDBValue AS CL_CHK_024_ID, 
  L_CL_CHK_027.sDBValue AS CL_CHK_027_ID, 
  L_CL_CHK_030.sDBValue AS CL_CHK_030_ID, 
  L_CL_CHK_031.sDBValue AS CL_CHK_031_ID, 
  L_CL_CHK_033.sDBValue AS CL_CHK_033_ID, 
  L_CL_CHK_034.sDBValue AS CL_CHK_034_ID, 
  L_CL_CHK_0342.sDBValue AS CL_CHK_0342_ID, 
  L_CL_CHK_0343.sDBValue AS CL_CHK_0343_ID, 
  L_CL_CHK_0344.sDBValue AS CL_CHK_0344_ID, 
  L_CL_CHK_0345.sDBValue AS CL_CHK_0345_ID, 
  L_CL_CHK_039.sDBValue AS CL_CHK_039_ID, 
  L_CL_CHK_052.sDBValue AS CL_CHK_052_ID, 
  L_CL_CHK_092.sDBValue AS CL_CHK_092_ID, 
  L_CL_CHK_094.sDBValue AS CL_CHK_094_ID, 
  L_CL_CHK_094_01.sDBValue AS CL_CHK_094_01_ID, 
  L_CL_CHK_094_02.sDBValue AS CL_CHK_094_02_ID, 
  L_CL_CHK_094_03.sDBValue AS CL_CHK_094_03_ID, 
  L_CL_CHK_094_04.sDBValue AS CL_CHK_094_04_ID, 
  L_CL_CHK_094_05.sDBValue AS CL_CHK_094_05_ID, 
  L_CL_CHK_095.sDBValue AS CL_CHK_095_ID, 
  L_CL_CHK_095_01.sDBValue AS CL_CHK_095_01_ID, 
  L_CL_CHK_095_02.sDBValue AS CL_CHK_095_02_ID, 
  L_CL_CHK_095_03.sDBValue AS CL_CHK_095_03_ID, 
  L_CL_CHK_095_04.sDBValue AS CL_CHK_095_04_ID, 
  L_CL_CHK_095_05.sDBValue AS CL_CHK_095_05_ID, 
  L_CL_CHK_102.sDBValue AS CL_CHK_102_ID, 
  L_CL_CHK_102_01.sDBValue AS CL_CHK_102_01_ID, 
  L_CL_CHK_102_02.sDBValue AS CL_CHK_102_02_ID, 
  L_CL_CHK_102_03.sDBValue AS CL_CHK_102_03_ID, 
  L_CL_CHK_102_04.sDBValue AS CL_CHK_102_04_ID, 
  L_CL_CHK_102_05.sDBValue AS CL_CHK_102_05_ID, 
  L_CL_CHK_106.sDBValue AS CL_CHK_106_ID, 
  L_CL_CHK_106_01.sDBValue AS CL_CHK_106_01_ID, 
  L_CL_CHK_106_02.sDBValue AS CL_CHK_106_02_ID, 
  L_CL_CHK_106_03.sDBValue AS CL_CHK_106_03_ID, 
  L_CL_CHK_106_04.sDBValue AS CL_CHK_106_04_ID, 
  L_CL_CHK_106_05.sDBValue AS CL_CHK_106_05_ID, 
  L_CL_CHK_109.sDBValue AS CL_CHK_109_ID, 
  L_CL_CHK_109_01.sDBValue AS CL_CHK_109_01_ID, 
  L_CL_CHK_109_02.sDBValue AS CL_CHK_109_02_ID, 
  L_CL_CHK_109_03.sDBValue AS CL_CHK_109_03_ID, 
  L_CL_CHK_109_04.sDBValue AS CL_CHK_109_04_ID, 
  L_CL_CHK_109_05.sDBValue AS CL_CHK_109_05_ID, 
  L_CL_CHK_111.sDBValue AS CL_CHK_111_ID, 
  L_CL_CHK_111_01.sDBValue AS CL_CHK_111_01_ID, 
  L_CL_CHK_111_02.sDBValue AS CL_CHK_111_02_ID, 
  L_CL_CHK_111_03.sDBValue AS CL_CHK_111_03_ID, 
  L_CL_CHK_111_04.sDBValue AS CL_CHK_111_04_ID, 
  L_CL_CHK_111_05.sDBValue AS CL_CHK_111_05_ID, 
  L_CL_CHK_116.sDBValue AS CL_CHK_116_ID, 
  L_CL_CHK_116_01.sDBValue AS CL_CHK_116_01_ID, 
  L_CL_CHK_116_02.sDBValue AS CL_CHK_116_02_ID, 
  L_CL_CHK_116_03.sDBValue AS CL_CHK_116_03_ID, 
  L_CL_CHK_116_04.sDBValue AS CL_CHK_116_04_ID, 
  L_CL_CHK_116_05.sDBValue AS CL_CHK_116_05_ID, 
  L_CL_CHK_131.sDBValue AS CL_CHK_131_ID, 
  L_CL_CHK_131_01.sDBValue AS CL_CHK_131_01_ID, 
  L_CL_CHK_131_02.sDBValue AS CL_CHK_131_02_ID, 
  L_CL_CHK_131_03.sDBValue AS CL_CHK_131_03_ID, 
  L_CL_CHK_131_04.sDBValue AS CL_CHK_131_04_ID, 
  L_CL_CHK_131_05.sDBValue AS CL_CHK_131_05_ID, 
  L_CL_CHK_141.sDBValue AS CL_CHK_141_ID, 
  L_CL_CHK_141_01.sDBValue AS CL_CHK_141_01_ID, 
  L_CL_CHK_141_02.sDBValue AS CL_CHK_141_02_ID, 
  L_CL_CHK_141_03.sDBValue AS CL_CHK_141_03_ID, 
  L_CL_CHK_141_04.sDBValue AS CL_CHK_141_04_ID, 
  L_CL_CHK_141_05.sDBValue AS CL_CHK_141_05_ID, 
  L_CL_CHK_147.sDBValue AS CL_CHK_147_ID, 
  L_CL_CHK_147_01.sDBValue AS CL_CHK_147_01_ID, 
  L_CL_CHK_147_02.sDBValue AS CL_CHK_147_02_ID, 
  L_CL_CHK_147_03.sDBValue AS CL_CHK_147_03_ID, 
  L_CL_CHK_147_04.sDBValue AS CL_CHK_147_04_ID, 
  L_CL_CHK_147_05.sDBValue AS CL_CHK_147_05_ID, 
  L_CL_CHK_162.sDBValue AS CL_CHK_162_ID, 
  L_CL_CHK_162_01.sDBValue AS CL_CHK_162_01_ID, 
  L_CL_CHK_162_02.sDBValue AS CL_CHK_162_02_ID, 
  L_CL_CHK_162_03.sDBValue AS CL_CHK_162_03_ID, 
  L_CL_CHK_162_04.sDBValue AS CL_CHK_162_04_ID, 
  L_CL_CHK_162_05.sDBValue AS CL_CHK_162_05_ID, 
  L_CL_CHK_163.sDBValue AS CL_CHK_163_ID, 
  L_CL_CHK_200.sDBValue AS CL_CHK_200_ID, 
  L_CL_CHK_200_01.sDBValue AS CL_CHK_200_01_ID, 
  L_CL_CHK_200_02.sDBValue AS CL_CHK_200_02_ID, 
  L_CL_CHK_200_03.sDBValue AS CL_CHK_200_03_ID, 
  L_CL_CHK_200_04.sDBValue AS CL_CHK_200_04_ID, 
  L_CL_CHK_200_05.sDBValue AS CL_CHK_200_05_ID, 
  L_CL_CHK_201.sDBValue AS CL_CHK_201_ID, 
  L_CL_CHK_201_01.sDBValue AS CL_CHK_201_01_ID, 
  L_CL_CHK_201_02.sDBValue AS CL_CHK_201_02_ID, 
  L_CL_CHK_201_03.sDBValue AS CL_CHK_201_03_ID, 
  L_CL_CHK_201_04.sDBValue AS CL_CHK_201_04_ID, 
  L_CL_CHK_201_05.sDBValue AS CL_CHK_201_05_ID, 
  L_CL_CHK_211.sDBValue AS CL_CHK_211_ID, 
  L_CL_CHK_212.sDBValue AS CL_CHK_212_ID, 
  L_CL_CHK_213.sDBValue AS CL_CHK_213_ID, 
  L_CL_CHK_214.sDBValue AS CL_CHK_214_ID, 
  L_CL_CHK_215.sDBValue AS CL_CHK_215_ID, 
  L_CL_CHK_219.sDBValue AS CL_CHK_219_ID, 
  L_CL_CHK_219_01.sDBValue AS CL_CHK_219_01_ID, 
  L_CL_CHK_219_02.sDBValue AS CL_CHK_219_02_ID, 
  L_CL_CHK_219_03.sDBValue AS CL_CHK_219_03_ID, 
  L_CL_CHK_219_04.sDBValue AS CL_CHK_219_04_ID, 
  L_CL_CHK_219_05.sDBValue AS CL_CHK_219_05_ID, 
  L_CL_CHK_223.sDBValue AS CL_CHK_223_ID, 
  L_CL_CHK_223_01.sDBValue AS CL_CHK_223_01_ID, 
  L_CL_CHK_223_02.sDBValue AS CL_CHK_223_02_ID, 
  L_CL_CHK_223_03.sDBValue AS CL_CHK_223_03_ID, 
  L_CL_CHK_223_04.sDBValue AS CL_CHK_223_04_ID, 
  L_CL_CHK_223_05.sDBValue AS CL_CHK_223_05_ID, 
  L_CL_CHK_224.sDBValue AS CL_CHK_224_ID, 
  L_CL_CHK_225.sDBValue AS CL_CHK_225_ID, 
  L_CL_CHK_228.sDBValue AS CL_CHK_228_ID, 
  L_CL_CHK_229.sDBValue AS CL_CHK_229_ID, 
  L_CL_CHK_232.sDBValue AS CL_CHK_232_ID, 
  L_CL_CHK_235.sDBValue AS CL_CHK_235_ID, 
  L_CL_CHK_237.sDBValue AS CL_CHK_237_ID, 
  L_CL_CHK_238.sDBValue AS CL_CHK_238_ID, 
  L_CL_CHK_240.sDBValue AS CL_CHK_240_ID, 
  L_CL_CHK_241.sDBValue AS CL_CHK_241_ID, 
  L_CL_CHK_INSCO_01.sDBValue AS CL_CHK_INSCO_01_ID, 
  L_CL_CHK_INSCO_02.sDBValue AS CL_CHK_INSCO_02_ID, 
  L_CL_CHK_INSCO_03.sDBValue AS CL_CHK_INSCO_03_ID, 
  L_CL_CHK_INSCO_04.sDBValue AS CL_CHK_INSCO_04_ID, 
  L_CL_CHK_INSCO_05.sDBValue AS CL_CHK_INSCO_05_ID, 
  L_CL_CHK_INSCONME.sDBValue AS CL_CHK_INSCONME_ID, 
  L_CL_INVAPP.sDBValue AS CL_INVAPP_ID, 
  L_CL_INVNAME.sDBValue AS CL_INVNAME_ID, 
  L_CL_INVOICED.sDBValue AS CL_INVOICED_ID, 
  L_CL_LEGALINV.sDBValue AS CL_LEGALINV_ID, 
  L_CL_OSAGT.sDBValue AS CL_OSAGT_ID, 
  L_CL_OSAGT_01.sDBValue AS CL_OSAGT_01_ID, 
  L_CL_OSAGT_02.sDBValue AS CL_OSAGT_02_ID, 
  L_CL_OSAGT_03.sDBValue AS CL_OSAGT_03_ID, 
  L_CL_OSAGT_04.sDBValue AS CL_OSAGT_04_ID, 
  L_CL_OSAGT_05.sDBValue AS CL_OSAGT_05_ID, 
  L_CL_RECCLA.sDBValue AS CL_RECCLA_ID, 
  L_CL_RECCLA_01.sDBValue AS CL_RECCLA_01_ID, 
  L_CL_RECCLA_02.sDBValue AS CL_RECCLA_02_ID, 
  L_CL_RECCLA_03.sDBValue AS CL_RECCLA_03_ID, 
  L_CL_RECCLA_04.sDBValue AS CL_RECCLA_04_ID, 
  L_CL_RECCLA_05.sDBValue AS CL_RECCLA_05_ID, 
  L_CL_RECMER.sDBValue AS CL_RECMER_ID, 
  L_CL_RECMER_01.sDBValue AS CL_RECMER_01_ID, 
  L_CL_RECMER_02.sDBValue AS CL_RECMER_02_ID, 
  L_CL_RECMER_03.sDBValue AS CL_RECMER_03_ID, 
  L_CL_RECMER_04.sDBValue AS CL_RECMER_04_ID, 
  L_CL_RECMER_05.sDBValue AS CL_RECMER_05_ID, 
  L_CL_REPUDI.sDBValue AS CL_REPUDI_ID, 
  L_CL_SALAGREE.sDBValue AS CL_SALAGREE_ID, 
  L_CL_THICLA.sDBValue AS CL_THICLA_ID, 
  L_CL_TPINSURID.sDBValue AS CL_TPINSURID_ID, 
  L_CL_TPINSURID_01.sDBValue AS CL_TPINSURID_01_ID, 
  L_CL_TPINSURID_02.sDBValue AS CL_TPINSURID_02_ID, 
  L_CL_TPINSURID_03.sDBValue AS CL_TPINSURID_03_ID, 
  L_CL_TPINSURID_04.sDBValue AS CL_TPINSURID_04_ID, 
  L_CL_TPINSURID_05.sDBValue AS CL_TPINSURID_05_ID, 
  L_CL_VEHDRI.sDBValue AS CL_VEHDRI_ID, 
  L_CL_WRTOFF.sDBValue AS CL_WRTOFF_ID
FROM SKI_INT.st_ClaimDetails_Normal NORM (NOLOCK)
  LEFT JOIN Lookup L_CL_AGTNAME WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME.sDBValue = CL_AGTNAME
  LEFT JOIN Lookup L_CL_AGTNAME_01 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_01.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_01.sDBValue = CL_AGTNAME_01
  LEFT JOIN Lookup L_CL_AGTNAME_02 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_02.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_02.sDBValue = CL_AGTNAME_02
  LEFT JOIN Lookup L_CL_AGTNAME_03 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_03.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_03.sDBValue = CL_AGTNAME_03
  LEFT JOIN Lookup L_CL_AGTNAME_04 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_04.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_04.sDBValue = CL_AGTNAME_04
  LEFT JOIN Lookup L_CL_AGTNAME_05 WITH (NOEXPAND NOLOCK)
    ON L_CL_AGTNAME_05.sGroup = 'AGENTNAME'
    AND L_CL_AGTNAME_05.sDBValue = CL_AGTNAME_05
  LEFT JOIN Lookup L_CL_BACKOFFINEG WITH (NOEXPAND NOLOCK)
    ON L_CL_BACKOFFINEG.sGroup = 'Backoffineg'
    AND L_CL_BACKOFFINEG.sDescription = CL_BACKOFFINEG
  LEFT JOIN Lookup L_CL_CHK_000 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_000.sGroup = 'CLType'
 AND L_CL_CHK_000.sDescription = CL_CHK_000
LEFT JOIN Lookup L_CL_CHK_010 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_010.sGroup = 'YesNo'
 AND L_CL_CHK_010.sDescription = CL_CHK_010
LEFT JOIN Lookup L_CL_CHK_011 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_011.sGroup = 'YesNo'
 AND L_CL_CHK_011.sDescription = CL_CHK_011
LEFT JOIN Lookup L_CL_CHK_012 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_012.sGroup = 'YesNo'
 AND L_CL_CHK_012.sDescription = CL_CHK_012
LEFT JOIN Lookup L_CL_CHK_013 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_013.sGroup = 'YesNoApp'
 AND L_CL_CHK_013.sDescription = CL_CHK_013
LEFT JOIN Lookup L_CL_CHK_017 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_017.sGroup = 'WEATHER'
 AND L_CL_CHK_017.sDescription = CL_CHK_017
LEFT JOIN Lookup L_CL_CHK_023 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_023.sGroup = 'YesNoApp'
 AND L_CL_CHK_023.sDescription = CL_CHK_023
LEFT JOIN Lookup L_CL_CHK_024 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_024.sGroup = 'CHKASSNAME'
 AND L_CL_CHK_024.sDescription = CL_CHK_024
LEFT JOIN Lookup L_CL_CHK_027 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_027.sGroup = 'CHK_027'
 AND L_CL_CHK_027.sDescription = CL_CHK_027
LEFT JOIN Lookup L_CL_CHK_030 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_030.sGroup = 'YesNoApp'
 AND L_CL_CHK_030.sDescription = CL_CHK_030
LEFT JOIN Lookup L_CL_CHK_031 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_031.sGroup = 'CHKPBEATER'
 AND L_CL_CHK_031.sDescription = CL_CHK_031
LEFT JOIN Lookup L_CL_CHK_033 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_033.sGroup = 'YesNoApp'
 AND L_CL_CHK_033.sDescription = CL_CHK_033
LEFT JOIN Lookup L_CL_CHK_034 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_034.sGroup = 'PartSupplier'
 AND L_CL_CHK_034.sDescription = CL_CHK_034
LEFT JOIN Lookup L_CL_CHK_0342 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0342.sGroup = 'PartSupplier'
 AND L_CL_CHK_0342.sDescription = CL_CHK_0342
LEFT JOIN Lookup L_CL_CHK_0343 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0343.sGroup = 'PartSupplier'
 AND L_CL_CHK_0343.sDescription = CL_CHK_0343
LEFT JOIN Lookup L_CL_CHK_0344 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0344.sGroup = 'PartSupplier'
 AND L_CL_CHK_0344.sDescription = CL_CHK_0344
LEFT JOIN Lookup L_CL_CHK_0345 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_0345.sGroup = 'PartSupplier'
 AND L_CL_CHK_0345.sDescription = CL_CHK_0345
LEFT JOIN Lookup L_CL_CHK_039 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_039.sGroup = 'OthPartInv'
 AND L_CL_CHK_039.sDescription = CL_CHK_039
LEFT JOIN Lookup L_CL_CHK_052 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_052.sGroup = 'Merrit'
 AND L_CL_CHK_052.sDescription = CL_CHK_052
LEFT JOIN Lookup L_CL_CHK_092 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_092.sGroup = 'YesNo'
 AND L_CL_CHK_092.sDescription = CL_CHK_092
LEFT JOIN Lookup L_CL_CHK_094 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094.sGroup = 'YesNo'
 AND L_CL_CHK_094.sDescription = CL_CHK_094
LEFT JOIN Lookup L_CL_CHK_094_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_01.sGroup = 'YesNo'
 AND L_CL_CHK_094_01.sDescription = CL_CHK_094_01
LEFT JOIN Lookup L_CL_CHK_094_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_02.sGroup = 'YesNo'
 AND L_CL_CHK_094_02.sDescription = CL_CHK_094_02
LEFT JOIN Lookup L_CL_CHK_094_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_03.sGroup = 'YesNo'
 AND L_CL_CHK_094_03.sDescription = CL_CHK_094_03
LEFT JOIN Lookup L_CL_CHK_094_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_04.sGroup = 'YesNo'
 AND L_CL_CHK_094_04.sDescription = CL_CHK_094_04
LEFT JOIN Lookup L_CL_CHK_094_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_094_05.sGroup = 'YesNo'
 AND L_CL_CHK_094_05.sDescription = CL_CHK_094_05
LEFT JOIN Lookup L_CL_CHK_095 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095.sGroup = 'InterNeGot'
 AND L_CL_CHK_095.sDescription = CL_CHK_095
LEFT JOIN Lookup L_CL_CHK_095_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_01.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_01.sDescription = CL_CHK_095_01
LEFT JOIN Lookup L_CL_CHK_095_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_02.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_02.sDescription = CL_CHK_095_02
LEFT JOIN Lookup L_CL_CHK_095_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_03.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_03.sDescription = CL_CHK_095_03
LEFT JOIN Lookup L_CL_CHK_095_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_04.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_04.sDescription = CL_CHK_095_04
LEFT JOIN Lookup L_CL_CHK_095_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_095_05.sGroup = 'InterNeGot'
 AND L_CL_CHK_095_05.sDescription = CL_CHK_095_05
LEFT JOIN Lookup L_CL_CHK_102 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102.sGroup = 'RecType'
 AND L_CL_CHK_102.sDescription = CL_CHK_102
LEFT JOIN Lookup L_CL_CHK_102_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_01.sGroup = 'RecType'
 AND L_CL_CHK_102_01.sDescription = CL_CHK_102_01
LEFT JOIN Lookup L_CL_CHK_102_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_02.sGroup = 'RecType'
 AND L_CL_CHK_102_02.sDescription = CL_CHK_102_02
LEFT JOIN Lookup L_CL_CHK_102_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_03.sGroup = 'RecType'
 AND L_CL_CHK_102_03.sDescription = CL_CHK_102_03
LEFT JOIN Lookup L_CL_CHK_102_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_04.sGroup = 'RecType'
 AND L_CL_CHK_102_04.sDescription = CL_CHK_102_04
LEFT JOIN Lookup L_CL_CHK_102_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_102_05.sGroup = 'RecType'
 AND L_CL_CHK_102_05.sDescription = CL_CHK_102_05
LEFT JOIN Lookup L_CL_CHK_106 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106.sGroup = 'UnInsType'
 AND L_CL_CHK_106.sDescription = CL_CHK_106
LEFT JOIN Lookup L_CL_CHK_106_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_01.sGroup = 'UnInsType'
 AND L_CL_CHK_106_01.sDescription = CL_CHK_106_01
LEFT JOIN Lookup L_CL_CHK_106_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_02.sGroup = 'UnInsType'
 AND L_CL_CHK_106_02.sDescription = CL_CHK_106_02
LEFT JOIN Lookup L_CL_CHK_106_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_03.sGroup = 'UnInsType'
 AND L_CL_CHK_106_03.sDescription = CL_CHK_106_03
LEFT JOIN Lookup L_CL_CHK_106_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_04.sGroup = 'UnInsType'
 AND L_CL_CHK_106_04.sDescription = CL_CHK_106_04
LEFT JOIN Lookup L_CL_CHK_106_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_106_05.sGroup = 'UnInsType'
 AND L_CL_CHK_106_05.sDescription = CL_CHK_106_05
LEFT JOIN Lookup L_CL_CHK_109 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109.sDescription = CL_CHK_109
LEFT JOIN Lookup L_CL_CHK_109_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_01.sDescription = CL_CHK_109_01
LEFT JOIN Lookup L_CL_CHK_109_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_02.sDescription = CL_CHK_109_02
LEFT JOIN Lookup L_CL_CHK_109_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_03.sDescription = CL_CHK_109_03
LEFT JOIN Lookup L_CL_CHK_109_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_04.sDescription = CL_CHK_109_04
LEFT JOIN Lookup L_CL_CHK_109_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_109_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_109_05.sDescription = CL_CHK_109_05
LEFT JOIN Lookup L_CL_CHK_111 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111.sDescription = CL_CHK_111
LEFT JOIN Lookup L_CL_CHK_111_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_01.sDescription = CL_CHK_111_01
LEFT JOIN Lookup L_CL_CHK_111_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_02.sDescription = CL_CHK_111_02
LEFT JOIN Lookup L_CL_CHK_111_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_03.sDescription = CL_CHK_111_03
LEFT JOIN Lookup L_CL_CHK_111_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_04.sDescription = CL_CHK_111_04
LEFT JOIN Lookup L_CL_CHK_111_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_111_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_111_05.sDescription = CL_CHK_111_05
LEFT JOIN Lookup L_CL_CHK_116 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116.sGroup = 'AttType'
 AND L_CL_CHK_116.sDescription = CL_CHK_116
LEFT JOIN Lookup L_CL_CHK_116_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_01.sGroup = 'AttType'
 AND L_CL_CHK_116_01.sDescription = CL_CHK_116_01
LEFT JOIN Lookup L_CL_CHK_116_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_02.sGroup = 'AttType'
 AND L_CL_CHK_116_02.sDescription = CL_CHK_116_02
LEFT JOIN Lookup L_CL_CHK_116_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_03.sGroup = 'AttType'
 AND L_CL_CHK_116_03.sDescription = CL_CHK_116_03
LEFT JOIN Lookup L_CL_CHK_116_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_04.sGroup = 'AttType'
 AND L_CL_CHK_116_04.sDescription = CL_CHK_116_04
LEFT JOIN Lookup L_CL_CHK_116_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_116_05.sGroup = 'AttType'
 AND L_CL_CHK_116_05.sDescription = CL_CHK_116_05
LEFT JOIN Lookup L_CL_CHK_131 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131.sGroup = 'APPPerc'
 AND L_CL_CHK_131.sDescription = CL_CHK_131
LEFT JOIN Lookup L_CL_CHK_131_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_01.sGroup = 'APPPerc'
 AND L_CL_CHK_131_01.sDescription = CL_CHK_131_01
LEFT JOIN Lookup L_CL_CHK_131_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_02.sGroup = 'APPPerc'
 AND L_CL_CHK_131_02.sDescription = CL_CHK_131_02
LEFT JOIN Lookup L_CL_CHK_131_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_03.sGroup = 'APPPerc'
 AND L_CL_CHK_131_03.sDescription = CL_CHK_131_03
LEFT JOIN Lookup L_CL_CHK_131_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_04.sGroup = 'APPPerc'
 AND L_CL_CHK_131_04.sDescription = CL_CHK_131_04
LEFT JOIN Lookup L_CL_CHK_131_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_131_05.sGroup = 'APPPerc'
 AND L_CL_CHK_131_05.sDescription = CL_CHK_131_05
LEFT JOIN Lookup L_CL_CHK_141 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141.sGroup = 'RecStatus'
 AND L_CL_CHK_141.sDescription = CL_CHK_141
LEFT JOIN Lookup L_CL_CHK_141_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_01.sGroup = 'RecStatus'
 AND L_CL_CHK_141_01.sDescription = CL_CHK_141_01
LEFT JOIN Lookup L_CL_CHK_141_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_02.sGroup = 'RecStatus'
 AND L_CL_CHK_141_02.sDescription = CL_CHK_141_02
LEFT JOIN Lookup L_CL_CHK_141_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_03.sGroup = 'RecStatus'
 AND L_CL_CHK_141_03.sDescription = CL_CHK_141_03
LEFT JOIN Lookup L_CL_CHK_141_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_04.sGroup = 'RecStatus'
 AND L_CL_CHK_141_04.sDescription = CL_CHK_141_04
LEFT JOIN Lookup L_CL_CHK_141_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_141_05.sGroup = 'RecStatus'
 AND L_CL_CHK_141_05.sDescription = CL_CHK_141_05
LEFT JOIN Lookup L_CL_CHK_147 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147.sGroup = 'AbandReason'
 AND L_CL_CHK_147.sDescription = CL_CHK_147
LEFT JOIN Lookup L_CL_CHK_147_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_01.sGroup = 'AbandReason'
 AND L_CL_CHK_147_01.sDescription = CL_CHK_147_01
LEFT JOIN Lookup L_CL_CHK_147_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_02.sGroup = 'AbandReason'
 AND L_CL_CHK_147_02.sDescription = CL_CHK_147_02
LEFT JOIN Lookup L_CL_CHK_147_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_03.sGroup = 'AbandReason'
 AND L_CL_CHK_147_03.sDescription = CL_CHK_147_03
LEFT JOIN Lookup L_CL_CHK_147_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_04.sGroup = 'AbandReason'
 AND L_CL_CHK_147_04.sDescription = CL_CHK_147_04
LEFT JOIN Lookup L_CL_CHK_147_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_147_05.sGroup = 'AbandReason'
 AND L_CL_CHK_147_05.sDescription = CL_CHK_147_05
LEFT JOIN Lookup L_CL_CHK_162 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162.sGroup = 'TPNotSett'
 AND L_CL_CHK_162.sDescription = CL_CHK_162
LEFT JOIN Lookup L_CL_CHK_162_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_01.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_01.sDescription = CL_CHK_162_01
LEFT JOIN Lookup L_CL_CHK_162_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_02.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_02.sDescription = CL_CHK_162_02
LEFT JOIN Lookup L_CL_CHK_162_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_03.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_03.sDescription = CL_CHK_162_03
LEFT JOIN Lookup L_CL_CHK_162_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_04.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_04.sDescription = CL_CHK_162_04
LEFT JOIN Lookup L_CL_CHK_162_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_162_05.sGroup = 'TPNotSett'
 AND L_CL_CHK_162_05.sDescription = CL_CHK_162_05
LEFT JOIN Lookup L_CL_CHK_163 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_163.sGroup = 'YesNo'
 AND L_CL_CHK_163.sDescription = CL_CHK_163
LEFT JOIN Lookup L_CL_CHK_200 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200.sGroup = 'YesNo'
 AND L_CL_CHK_200.sDescription = CL_CHK_200
LEFT JOIN Lookup L_CL_CHK_200_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_01.sGroup = 'YesNo'
 AND L_CL_CHK_200_01.sDescription = CL_CHK_200_01
LEFT JOIN Lookup L_CL_CHK_200_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_02.sGroup = 'YesNo'
 AND L_CL_CHK_200_02.sDescription = CL_CHK_200_02
LEFT JOIN Lookup L_CL_CHK_200_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_03.sGroup = 'YesNo'
 AND L_CL_CHK_200_03.sDescription = CL_CHK_200_03
LEFT JOIN Lookup L_CL_CHK_200_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_04.sGroup = 'YesNo'
 AND L_CL_CHK_200_04.sDescription = CL_CHK_200_04
LEFT JOIN Lookup L_CL_CHK_200_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_200_05.sGroup = 'YesNo'
 AND L_CL_CHK_200_05.sDescription = CL_CHK_200_05
LEFT JOIN Lookup L_CL_CHK_201 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201.sGroup = 'TracName'
 AND L_CL_CHK_201.sDescription = CL_CHK_201
LEFT JOIN Lookup L_CL_CHK_201_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_01.sGroup = 'TracName'
 AND L_CL_CHK_201_01.sDescription = CL_CHK_201_01
LEFT JOIN Lookup L_CL_CHK_201_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_02.sGroup = 'TracName'
 AND L_CL_CHK_201_02.sDescription = CL_CHK_201_02
LEFT JOIN Lookup L_CL_CHK_201_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_03.sGroup = 'TracName'
 AND L_CL_CHK_201_03.sDescription = CL_CHK_201_03
LEFT JOIN Lookup L_CL_CHK_201_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_04.sGroup = 'TracName'
 AND L_CL_CHK_201_04.sDescription = CL_CHK_201_04
LEFT JOIN Lookup L_CL_CHK_201_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_201_05.sGroup = 'TracName'
 AND L_CL_CHK_201_05.sDescription = CL_CHK_201_05
LEFT JOIN Lookup L_CL_CHK_211 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_211.sGroup = 'CHK211'
 AND L_CL_CHK_211.sDescription = CL_CHK_211
LEFT JOIN Lookup L_CL_CHK_212 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_212.sGroup = 'CHK211'
 AND L_CL_CHK_212.sDescription = CL_CHK_212
LEFT JOIN Lookup L_CL_CHK_213 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_213.sGroup = 'CHK211'
 AND L_CL_CHK_213.sDescription = CL_CHK_213
LEFT JOIN Lookup L_CL_CHK_214 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_214.sGroup = 'CHK211'
 AND L_CL_CHK_214.sDescription = CL_CHK_214
LEFT JOIN Lookup L_CL_CHK_215 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_215.sGroup = 'CHK215'
 AND L_CL_CHK_215.sDescription = CL_CHK_215
LEFT JOIN Lookup L_CL_CHK_219 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219.sGroup = 'YesNoApp'
 AND L_CL_CHK_219.sDescription = CL_CHK_219
LEFT JOIN Lookup L_CL_CHK_219_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_01.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_01.sDescription = CL_CHK_219_01
LEFT JOIN Lookup L_CL_CHK_219_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_02.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_02.sDescription = CL_CHK_219_02
LEFT JOIN Lookup L_CL_CHK_219_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_03.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_03.sDescription = CL_CHK_219_03
LEFT JOIN Lookup L_CL_CHK_219_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_04.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_04.sDescription = CL_CHK_219_04
LEFT JOIN Lookup L_CL_CHK_219_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_219_05.sGroup = 'YesNoApp'
 AND L_CL_CHK_219_05.sDescription = CL_CHK_219_05
LEFT JOIN Lookup L_CL_CHK_223 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223.sGroup = 'CHK223'
 AND L_CL_CHK_223.sDescription = CL_CHK_223
LEFT JOIN Lookup L_CL_CHK_223_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_01.sGroup = 'CHK223'
 AND L_CL_CHK_223_01.sDescription = CL_CHK_223_01
LEFT JOIN Lookup L_CL_CHK_223_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_02.sGroup = 'CHK223'
 AND L_CL_CHK_223_02.sDescription = CL_CHK_223_02
LEFT JOIN Lookup L_CL_CHK_223_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_03.sGroup = 'CHK223'
 AND L_CL_CHK_223_03.sDescription = CL_CHK_223_03
LEFT JOIN Lookup L_CL_CHK_223_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_04.sGroup = 'CHK223'
 AND L_CL_CHK_223_04.sDescription = CL_CHK_223_04
LEFT JOIN Lookup L_CL_CHK_223_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_223_05.sGroup = 'CHK223'
 AND L_CL_CHK_223_05.sDescription = CL_CHK_223_05
LEFT JOIN Lookup L_CL_CHK_224 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_224.sGroup = 'YesNo'
 AND L_CL_CHK_224.sDescription = CL_CHK_224
LEFT JOIN Lookup L_CL_CHK_225 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_225.sGroup = 'YesNo'
 AND L_CL_CHK_225.sDescription = CL_CHK_225
LEFT JOIN Lookup L_CL_CHK_228 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_228.sGroup = 'CHK228'
 AND L_CL_CHK_228.sDescription = CL_CHK_228
LEFT JOIN Lookup L_CL_CHK_229 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_229.sGroup = 'YesNo'
 AND L_CL_CHK_229.sDescription = CL_CHK_229
LEFT JOIN Lookup L_CL_CHK_232 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_232.sGroup = 'YesNo'
 AND L_CL_CHK_232.sDescription = CL_CHK_232
LEFT JOIN Lookup L_CL_CHK_235 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_235.sGroup = 'YesNo'
 AND L_CL_CHK_235.sDescription = CL_CHK_235
LEFT JOIN Lookup L_CL_CHK_237 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_237.sGroup = 'YesNo'
 AND L_CL_CHK_237.sDescription = CL_CHK_237
LEFT JOIN Lookup L_CL_CHK_238 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_238.sGroup = 'CHK238'
 AND L_CL_CHK_238.sDescription = CL_CHK_238
LEFT JOIN Lookup L_CL_CHK_240 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_240.sGroup = 'YesNo'
 AND L_CL_CHK_240.sDescription = CL_CHK_240
LEFT JOIN Lookup L_CL_CHK_241 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_241.sGroup = 'YesNo'
 AND L_CL_CHK_241.sDescription = CL_CHK_241
LEFT JOIN Lookup L_CL_CHK_INSCO_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_01.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_01.sDescription = CL_CHK_INSCO_01
LEFT JOIN Lookup L_CL_CHK_INSCO_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_02.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_02.sDescription = CL_CHK_INSCO_02
LEFT JOIN Lookup L_CL_CHK_INSCO_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_03.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_03.sDescription = CL_CHK_INSCO_03
LEFT JOIN Lookup L_CL_CHK_INSCO_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_04.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_04.sDescription = CL_CHK_INSCO_04
LEFT JOIN Lookup L_CL_CHK_INSCO_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCO_05.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCO_05.sDescription = CL_CHK_INSCO_05
LEFT JOIN Lookup L_CL_CHK_INSCONME WITH (NOEXPAND NOLOCK)
 ON L_CL_CHK_INSCONME.sGroup = 'CHKINSNAME'
 AND L_CL_CHK_INSCONME.sDescription = CL_CHK_INSCONME
LEFT JOIN Lookup L_CL_INVAPP WITH (NOEXPAND NOLOCK)
 ON L_CL_INVAPP.sGroup = 'YesNoApp'
 AND L_CL_INVAPP.sDescription = CL_INVAPP
LEFT JOIN Lookup L_CL_INVNAME WITH (NOEXPAND NOLOCK)
 ON L_CL_INVNAME.sGroup = 'Investigator'
 AND L_CL_INVNAME.sDescription = CL_INVNAME
LEFT JOIN Lookup L_CL_INVOICED WITH (NOEXPAND NOLOCK)
 ON L_CL_INVOICED.sGroup = 'YesNo'
 AND L_CL_INVOICED.sDescription = CL_INVOICED
LEFT JOIN Lookup L_CL_LEGALINV WITH (NOEXPAND NOLOCK)
 ON L_CL_LEGALINV.sGroup = 'YesNo'
 AND L_CL_LEGALINV.sDescription = CL_LEGALINV
LEFT JOIN Lookup L_CL_OSAGT WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT.sDescription = CL_OSAGT
LEFT JOIN Lookup L_CL_OSAGT_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_01.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_01.sDescription = CL_OSAGT_01
LEFT JOIN Lookup L_CL_OSAGT_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_02.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_02.sDescription = CL_OSAGT_02
LEFT JOIN Lookup L_CL_OSAGT_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_03.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_03.sDescription = CL_OSAGT_03
LEFT JOIN Lookup L_CL_OSAGT_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_04.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_04.sDescription = CL_OSAGT_04
LEFT JOIN Lookup L_CL_OSAGT_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_OSAGT_05.sGroup = 'OUTSOURCINGAGENT'
 AND L_CL_OSAGT_05.sDescription = CL_OSAGT_05
LEFT JOIN Lookup L_CL_RECCLA WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA.sGroup = 'YesNo'
 AND L_CL_RECCLA.sDescription = CL_RECCLA
LEFT JOIN Lookup L_CL_RECCLA_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_01.sGroup = 'YesNo'
 AND L_CL_RECCLA_01.sDescription = CL_RECCLA_01
LEFT JOIN Lookup L_CL_RECCLA_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_02.sGroup = 'YesNo'
 AND L_CL_RECCLA_02.sDescription = CL_RECCLA_02
LEFT JOIN Lookup L_CL_RECCLA_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_03.sGroup = 'YesNo'
 AND L_CL_RECCLA_03.sDescription = CL_RECCLA_03
LEFT JOIN Lookup L_CL_RECCLA_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_04.sGroup = 'YesNo'
 AND L_CL_RECCLA_04.sDescription = CL_RECCLA_04
LEFT JOIN Lookup L_CL_RECCLA_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECCLA_05.sGroup = 'YesNo'
 AND L_CL_RECCLA_05.sDescription = CL_RECCLA_05
LEFT JOIN Lookup L_CL_RECMER WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER.sDescription = CL_RECMER
LEFT JOIN Lookup L_CL_RECMER_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_01.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_01.sDescription = CL_RECMER_01
LEFT JOIN Lookup L_CL_RECMER_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_02.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_02.sDescription = CL_RECMER_02
LEFT JOIN Lookup L_CL_RECMER_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_03.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_03.sDescription = CL_RECMER_03
LEFT JOIN Lookup L_CL_RECMER_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_04.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_04.sDescription = CL_RECMER_04
LEFT JOIN Lookup L_CL_RECMER_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_RECMER_05.sGroup = 'CHKATTNAME'
 AND L_CL_RECMER_05.sDescription = CL_RECMER_05
LEFT JOIN Lookup L_CL_REPUDI WITH (NOEXPAND NOLOCK)
 ON L_CL_REPUDI.sGroup = 'YesNo'
 AND L_CL_REPUDI.sDescription = CL_REPUDI
LEFT JOIN Lookup L_CL_SALAGREE WITH (NOEXPAND NOLOCK)
 ON L_CL_SALAGREE.sGroup = 'SalvStatus'
 AND L_CL_SALAGREE.sDescription = CL_SALAGREE
LEFT JOIN Lookup L_CL_THICLA WITH (NOEXPAND NOLOCK)
 ON L_CL_THICLA.sGroup = 'YesNo'
 AND L_CL_THICLA.sDescription = CL_THICLA
LEFT JOIN Lookup L_CL_TPINSURID WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID.sGroup = 'TPInsure'
 AND L_CL_TPINSURID.sDescription = CL_TPINSURID
LEFT JOIN Lookup L_CL_TPINSURID_01 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_01.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_01.sDescription = CL_TPINSURID_01
LEFT JOIN Lookup L_CL_TPINSURID_02 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_02.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_02.sDescription = CL_TPINSURID_02
LEFT JOIN Lookup L_CL_TPINSURID_03 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_03.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_03.sDescription = CL_TPINSURID_03
LEFT JOIN Lookup L_CL_TPINSURID_04 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_04.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_04.sDescription = CL_TPINSURID_04
LEFT JOIN Lookup L_CL_TPINSURID_05 WITH (NOEXPAND NOLOCK)
 ON L_CL_TPINSURID_05.sGroup = 'TPInsure'
 AND L_CL_TPINSURID_05.sDescription = CL_TPINSURID_05
LEFT JOIN Lookup L_CL_VEHDRI WITH (NOEXPAND NOLOCK)
 ON L_CL_VEHDRI.sGroup = 'YesNo'
 AND L_CL_VEHDRI.sDescription = CL_VEHDRI
LEFT JOIN Lookup L_CL_WRTOFF WITH (NOEXPAND NOLOCK)
 ON L_CL_WRTOFF.sGroup = 'YesNo'
 AND L_CL_WRTOFF.sDescription = CL_WRTOFF
--LEFT JOIN Lookup L_R_G_TRACKTYPE WITH (NOEXPAND NOLOCK)
-- ON L_R_G_TRACKTYPE.sGroup = 'TRACKERTYPE'
-- AND L_R_G_TRACKTYPE.sDescription = R_G_TRACKTYPE

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_CustomerDetails_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_CustomerDetails_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_CustomerDetails;

INSERT INTO SKI_INT.EDW_CustomerDetails
SELECT 
  iCustomerID
  ,dLastUpdated
  ,iTranID
  ,C_CELLNO
  ,C_CONTDETAILS
  ,C_DOB
  ,C_EMAIL
  ,C_FAXTEL
  ,C_GENDER
  ,C_HOMETEL
  ,C_IDNO
  ,L_C_TITLE.sDBValue AS C_TITLE_ID
  ,C_TITLE
  ,L_C_VIPCLIENT.sDBValue AS C_VIPCLIENT_ID
  ,C_VIPCLIENT
  ,C_WORKTEL
  ,CH_INITIALS
FROM 
  SKI_INT.st_CustomerDetails_Normal C(NOLOCK)
  LEFT JOIN Lookup L_C_TITLE WITH (NOEXPAND NOLOCK)
    ON L_C_TITLE.sGroup = 'title'
    AND L_C_TITLE.sDBValue = C_TITLE
  LEFT JOIN Lookup L_C_VIPCLIENT WITH (NOEXPAND NOLOCK)
      ON L_C_VIPCLIENT.sGroup = 'YesNo'
      AND L_C_VIPCLIENT.sDBValue = C_TITLE
  ;

GO

/****** Object:  StoredProcedure [SKI_INT].[NormalizeCustomerDetailsTable_All]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeCustomerDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id CHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalCustomer'
			)
	BEGIN
		DROP TABLE TempNormalCustomer
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalCustomer 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalCustomer

	SET @SQLInsert = 'UPDATE CDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalCustomer
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_CustomerDetails_Normal CDNORM 
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG ON LG.sDBValue = CD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN CD.sFieldValue
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_CustomerDetails_Normal CDNORM 
						' + @UpdateTableJoin + ' 
						WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalCustomer
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalCustomer
	END
END
GO

/****** Object:  StoredProcedure [SKI_INT].[NormalizePolicyDetailsTable_All]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizePolicyDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
	--with encryption
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalPolicy'
			)
	BEGIN
		DROP TABLE TempNormalPolicy
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalPolicy 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalPolicy

	SET @SQLInsert = 'UPDATE PDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalPolicy
		WHERE sFieldCode = @au_id

		--SET ROWCOUNT 1
		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			IF @PolicyID > 0 --is not null
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM 
					' + @TableName + ' 
					LEFT JOIN Lookup LG (NOLOCK) ON LG.sDBValue = PD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + ''' 
					WHERE PD.iPolicyID = ' + RTRIM(LTRIM(str(@PolicyID))) + ' GROUP By PD.iPolicyID, P.dDateModified'
			END
			ELSE
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM 
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG (NOLOCK) ON LG.sDBValue = PD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE PD.sFieldCode = ''' + @au_id + ''''
			END
		END
		ELSE
		BEGIN
			IF @PolicyID > 0 --is not null
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN PD.sFieldValue
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM ' + @TableName + ' WHERE PD.iPolicyID = ' + RTRIM(LTRIM(str(@PolicyID))) + ' GROUP By PD.iPolicyID, P.dDateModified'
			END
			ELSE
			BEGIN
				SET @SQLQuery = @au_id + ' =
                                CASE PD.iFieldType
								WHEN 0 THEN PD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(PD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(PD.bFieldValue)))
								WHEN 3 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(PD.iFieldValue)))
								WHEN 5 THEN convert(char(10),PD.dtFieldValue,111)
								WHEN 6 THEN PD.sFieldValue
								WHEN 7 THEN PD.sFieldValue
								WHEN 8 THEN PD.sFieldValue
								WHEN 9 THEN str(PD.cFieldValue)
								WHEN 10 THEN str(PD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_PolicyDetails_Normal PDNORM 
						' + @UpdateTableJoin + ' 
						WHERE PD.sFieldCode = ''' + @au_id + ''''
			END
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalPolicy
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalPolicy
	END
END
GO

/****** Object:  StoredProcedure [SKI_INT].[NormalizeRiskDetailsTable_All]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeRiskDetailsTable_All] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalRisk'
			)
	BEGIN
		DROP TABLE TempNormalRisk
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalRisk 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalRisk

	SET @SQLInsert = 'UPDATE RDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE
		FROM TempNormalRisk
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + 
				' =
							COALESCE(CASE WHEN RD.iFieldType = 0 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 1 THEN ltrim(rtrim(str(RD.cFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 2 THEN ltrim(rtrim(str(RD.bFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 3 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 4 THEN ltrim(rtrim(str(RD.iFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 5 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 6 THEN LG.sDescription ELSE NULL END,
								CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue LIKE ''%@%'' THEN ISNULL(CAST(ROUND(dbo.GetCommCalcPremium(RD.sFieldValue) / 12, 2) AS VARCHAR(255)), '''') ELSE NULL END,
                                CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue NOT LIKE ''%@%'' THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 8 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 9 THEN str(RD.cFieldValue) ELSE NULL END,
								CASE WHEN RD.iFieldType = 10 THEN str(RD.iFieldValue) ELSE NULL END)
						FROM SKI_INT.st_RiskDetails_Normal RDNORM 
					' 
				+ @UpdateTableJoin + '
					LEFT JOIN Lookup LG ON LG.sDBValue = RD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE RD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + 
				' =
							COALESCE(CASE WHEN RD.iFieldType = 0 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 1 THEN ltrim(rtrim(str(RD.cFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 2 THEN ltrim(rtrim(str(RD.bFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 3 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 4 THEN ltrim(rtrim(str(RD.iFieldValue))) ELSE NULL END,
								CASE WHEN RD.iFieldType = 5 THEN convert(char(10),RD.dtFieldValue,111) ELSE NULL END,
								CASE WHEN RD.iFieldType = 6 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue LIKE ''%@%'' THEN ISNULL(CAST(ROUND(dbo.GetCommCalcPremium(RD.sFieldValue) / 12, 2) AS VARCHAR(255)), '''') ELSE NULL END,
                                CASE WHEN RD.iFieldType = 7 AND RD.sFieldValue NOT LIKE ''%@%'' THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 8 THEN RD.sFieldValue ELSE NULL END,
								CASE WHEN RD.iFieldType = 9 THEN str(RD.cFieldValue) ELSE NULL END,
								CASE WHEN RD.iFieldType = 10 THEN str(RD.iFieldValue) ELSE NULL END)
						FROM SKI_INT.st_RiskDetails_Normal RDNORM 
						' 
				+ @UpdateTableJoin + ' 
						WHERE RD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery + '
			;')
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalRisk
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalRisk
	END
END
GO

/****** Object:  StoredProcedure [SKI_INT].[NormalizeClaimDetailsTable_ALL]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SKI_INT].[NormalizeClaimDetailsTable_ALL] (
	@TableName VARCHAR(MAX)
	,@PolicyID INT = NULL
	,@iRiskTypeID VARCHAR(10)
	,@FieldCodeList VARCHAR(MAX)
	,@bCreateNew BIT
	,@UpdateTableJoin VARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @au_id VARCHAR(15)
		,@SQLQuery VARCHAR(Max)
		,@SQLInsertIDs VARCHAR(Max)
		,@SQLInsert VARCHAR(Max)
		,@LengthOfString INT
		,@LookupJoin VARCHAR(Max)
		,@LookupGroup VARCHAR(100)
		,@FieldType INT

	SET ROWCOUNT 0

	IF EXISTS (
			SELECT *
			FROM SYS.objects
			WHERE NAME = 'TempNormalClaim'
			)
	BEGIN
		DROP TABLE TempNormalClaim
	END

	EXEC (
			'SELECT DISTINCT RTRIM(LTRIM(t.sFieldCode)) as sFieldCode, FC.sLookupGroup AS sLookupGroup, FC.iFieldType INTO TempNormalClaim 
		   FROM ' + @FieldCodeList + ' 
		   ORDER BY RTRIM(LTRIM(t.sFieldCode)) desc'
			)

	SET ROWCOUNT 1

	SELECT @au_id = RTRIM(LTRIM(sFieldCode))
		,@LookupGroup = sLookupGroup
		,@FieldType = iFieldType
	FROM TempNormalClaim

	SET @SQLInsert = 'UPDATE CDNORM SET
					'
	SET @LengthOfString = 0

	WHILE @@rowcount <> 0
	BEGIN
		SET ROWCOUNT 0

		DELETE TempNormalClaim
		WHERE sFieldCode = @au_id

		IF @LookupGroup <> ''
			AND @FieldType = 6
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN LG.sDescription
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_ClaimDetails_Normal CDNORM 
					' + @UpdateTableJoin + '
					LEFT JOIN Lookup LG ON LG.sDBValue = CD.sFieldValue AND LG.sGroup = ''' + @LookupGroup + '''  
					WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END
		ELSE
		BEGIN
			SET @SQLQuery = @au_id + ' =
							CASE CD.iFieldType
								WHEN 0 THEN CD.sFieldValue
								WHEN 1 THEN ltrim(rtrim(str(CD.cFieldValue)))
								WHEN 2 THEN ltrim(rtrim(str(CD.bFieldValue)))
								WHEN 3 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 4 THEN ltrim(rtrim(str(CD.iFieldValue)))
								WHEN 5 THEN convert(char(10),CD.dtFieldValue,111)
								WHEN 6 THEN CD.sFieldValue
								WHEN 7 THEN CD.sFieldValue
								WHEN 8 THEN CD.sFieldValue
								WHEN 9 THEN str(CD.cFieldValue)
								WHEN 10 THEN str(CD.iFieldValue)
								ELSE NULL
							END
						FROM SKI_INT.st_ClaimDetails_Normal CDNORM 
						' + @UpdateTableJoin + ' 
						WHERE CD.sFieldCode = ''' + @au_id + ''''
			SET @LengthOfString = len(@SQLQuery)
		END

		IF @SQLQuery <> ''
		BEGIN
			EXEC (@SQLInsert + @SQLQuery)
		END

		SELECT @au_id = RTRIM(LTRIM(sFieldCode))
			,@LookupGroup = sLookupGroup
			,@FieldType = iFieldType
		FROM TempNormalClaim
	END

	SET ROWCOUNT 0

	BEGIN
		DROP TABLE TempNormalClaim
	END
END
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolicyClaimTran_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolicyClaimTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolicyClaimTran

INSERT INTO SKI_INT.EDW_PolicyClaimTran (
	[iClaimID]
	,[AMT CLAIMED]
	,[EXCESS]
	,[SALVAGE]
	,[RECOVERY]
	,[PAID]
	,[BALANCE]
	,[Summary Date]
	)
SELECT [iClaimID]
	,[AMT CLAIMED]
	,[EXCESS]
	,[SALVAGE]
	,[RECOVERY]
	,[PAID]
	,[BALANCE]
	,getdate()
FROM SKI_INT.EDW_PolicyClaimTransaction_Delta(NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTranDet_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTranDet_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTranDet;

INSERT INTO SKI_INT.EDW_PolTranDet
SELECT 
  iTransactionID
  ,iPolicyID
  ,iRiskID
  ,iRiskTypeID
  ,dDateActioned
  ,iTranType
  ,L_TranType.sDescription AS iTrantype_Desc
  ,cPremium
  ,cCommission
  ,cAdminFee
  ,cSASRIA
  ,sUser
FROM 
  SKI_INT.EDW_PolTranDet_Delta NORM (NOLOCK)
  JOIN Lookup L_TranType WITH (NOEXPAND NOLOCK)
    ON L_TranType.sGroup = 'TransactionType'
    AND L_TranType.iIndex = iTrantype 
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Risks_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Risks_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Risks;
--select * from SKI_INT.EDW_Risks;

INSERT INTO SKI_INT.EDW_Risks
SELECT 
  intClientID
  ,intRiskTypeID
  ,L_Risktype.sDescription AS RiskType_Desc
  ,intRiskID
  ,intPolicyID
  ,strDescription
  ,cSumInsured
  ,dDateCreated
  ,strCreatedBy
  ,intStatus
  ,L_Status.sDescription AS intStatus_Desc
  ,intCurrency
  ,L_Currency.sDescription AS intCurrency_Desc
  ,cAdminFee
  ,R.iProductID
  ,PR.sProductName AS iProductID_Desc
  ,dDateModified
  ,iTranID
  ,iPaymentTerm
  ,L_PaymentTerm.sDescription AS iPaymentTerm_Desc
  ,cPremium
  ,cSASRIA
  ,cCommission
  ,dValidTill
  ,cVat
  ,sLastUpdatedBy
  ,dEffectiveDate
  ,iAddressID
  ,iParentID
  ,ski_int.fn_GetParentRiskDescription(iParentID) AS ParentRisk_Desc
  ,cAnnualPremium
  ,cAnnualComm
  ,cAnnualSASRIA
  ,cAnnualAdminFee
  ,sDeleteReason
  ,sSASRIAGroup
  ,R.dInception
  ,cCommissionPerc
  ,sMatching
  ,cCommissionRebate
  ,dReviewDate
  ,sReviewer
  ,PremiumPerc
FROM 
  SKI_INT.EDW_Risks_Delta R(NOLOCK)
  JOIN Lookup L_Status WITH (NOEXPAND NOLOCK)
    ON L_Status.iIndex = R.intStatus
    AND L_Status.sGroup = 'RiskStatus'
  JOIN Lookup L_Currency WITH (NOEXPAND NOLOCK)
    ON L_Currency.iIndex = R.intCurrency
    AND L_Currency.sGroup = 'Currency'
  JOIN Products PR (NOLOCK)
    ON PR.iProductID = R.iProductID
  JOIN Lookup L_PaymentTerm WITH (NOEXPAND NOLOCK)
    ON L_PaymentTerm.iIndex = R.iPaymentTerm
    AND L_PaymentTerm.sGroup = 'Payment Term'
  JOIN Lookup L_Risktype WITH (NOEXPAND NOLOCK)
    ON L_Risktype.sGroup = 'RiskType'
    AND L_Risktype.iIndex = R.intRiskTypeID 
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTranSummary_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTranSummary_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTranSummary;

INSERT INTO SKI_INT.EDW_PolTranSummary (
	[PolicyID]
	,[Premium Bal]
	,[Commissoin Bal]
	,[SASRIA Bal]
	,[Admin Fee Bal]
	,[Policy Fee Bal]
	,[Broker Fee Bal]
	,[Summary Date]
	)
SELECT [PolicyID]
	,[Premium Bal]
	,[Commissoin Bal]
	,[SASRIA Bal]
	,[Admin Fee Bal]
	,[Policy Fee Bal]
	,[Broker Fee Bal]
	,GETDATE()
FROM SKI_INT.EDW_PolTranSummary_Delta(NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_PolTran_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_PolTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_PolTran;

INSERT INTO SKI_INT.EDW_PolTran
SELECT 
  iTransactionId
  ,iPolicyId
  ,sReference
  ,iAgencyId
  ,ce.sName AS Agency_Desc
  ,iTrantype
  ,L_TranType.sDescription AS iTrantype_Desc
  ,cPremium
  ,cCommission
  ,NORM.cAdminfee
  ,cSASRIA
  ,dTransaction
  ,sUser
  ,dPeriodStart
  ,dPeriodEnd
  ,iCommStatus
  ,L_CommStatus.sDescription AS CommStatus_Desc
  ,iParentTranID
  ,iDebitNoteId
  ,NORM.cPolicyFee
  ,NORM.sComment
  ,dEffective
  ,NORM.cBrokerFee
  ,sFinAccount
FROM 
  SKI_INT.EDW_PolTran_Delta NORM (NOLOCK)
  JOIN Lookup L_TranType WITH (NOEXPAND NOLOCK)
    ON L_TranType.sGroup = 'TransactionType'
    AND L_TranType.iIndex = iTrantype
  JOIN dbo.CommstructEntityRel crr (NOLOCK) ON crr.iCommRelid = NORM.iAgencyId
  JOIN dbo.Commentities ce (NOLOCK) ON ce.iCommEntityid = crr.iLev2Entityid
  LEFT JOIN Lookup L_CommStatus WITH (NOEXPAND NOLOCK)
    ON L_CommStatus.sGroup = 'CommStatus'
    AND L_CommStatus.iIndex = iCommStatus

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_ClaimTran_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_ClaimTran_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_ClaimTran;
--select * from SKI_INT.EDW_ClaimTran;
INSERT INTO SKI_INT.EDW_ClaimTran
SELECT 
  iTransactionID
  ,iClaimID
  ,iClaimantID
  ,dtTransaction
  ,dtPeriodEnd
  ,sTransactionCode
  ,L_sTransactionCode.sDescription AS sTransactionCode_Desc
  ,cValue
  ,sReference
  ,cVATValue
  ,iPayMethod
  ,L_ClaimPayMethod.sDescription AS PayMethod_Desc
  ,sRequestedBy
  ,bReversed
  ,iParentPaymentID
  ,dtPeriodStart
  ,sFinAccount
FROM 
  SKI_INT.EDW_ClaimTran_Delta NORM (NOLOCK)
  LEFT JOIN Lookup L_sTransactionCode WITH (NOEXPAND NOLOCK)
    ON L_sTransactionCode.sGroup = 'ClaimTranCodes'
    AND L_sTransactionCode.sDBValue = sTransactionCode
  LEFT JOIN Lookup L_ClaimPayMethod WITH (NOEXPAND NOLOCK)
    ON L_ClaimPayMethod.sGroup = 'ClaimPayMethod'
    AND L_ClaimPayMethod.iIndex = iPayMethod

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Claimants_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Claimants_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Claimants;

INSERT INTO SKI_INT.EDW_Claimants
SELECT 
    iClaimantID
    ,iClaimID
    ,sRelationCode
    ,L_sRelationCode.sDescription AS sRelationCode_Desc
    ,sClaimantName
    ,iClaimantAddressID
    ,NORM.sDescription
    ,cAmtClaimed
    ,cTotExcess
    ,cExcessRecovered
    ,cAmountPaid
    ,cRecovered
    ,sAccountHolder
    ,sAccountNumber
    ,sBranchCode
    ,sBankName
    ,sCCAccountNumber
    ,sCCExpiryDate
    ,sCCControlDigits
    ,bCreditCard
    ,sCreditCardType
    ,sAccType
    ,sVATType
    ,sVATNo
    ,sStatus
    ,bExcludeCLAmt
    ,sLinkFilename
    ,cSalvage
    ,iVendorID
    ,sFinAccount
    ,sExternalID
    ,ClaimCategory
FROM 
  SKI_INT.EDW_Claimants_Delta NORM (NOLOCK)
  LEFT JOIN Lookup L_sRelationCode WITH (NOEXPAND NOLOCK)
    ON L_sRelationCode.sGroup = 'ClaimantRelationship'
    AND L_sRelationCode.sDBValue = sRelationCode

GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Claims_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Claims_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Claims;
--select * from SKI_INT.EDW_Claims;

INSERT INTO SKI_INT.EDW_Claims
SELECT 
  iClaimID
  ,iClientID
  ,iRiskID
  ,iPolicyID
  ,sRefNo
  ,sCauseCode
  ,L_CauseCode.sDescription AS sCauseCode_Desc
  ,sResponsibleName
  ,sResponsibleSurname
  ,dtEvent
  ,sStatusCode
  ,L_ClaimStatus.sDescription AS sStatusCode_Desc
  ,dStatusChanged
  ,C.sDescription
  ,sComment
  ,sApprovalCode
  ,L_ApprovalCodes.sDescription AS sApprovalCode_Desc
  ,bClientResponsible
  ,cAmtFirstClaimed
  ,cAmtClaimed
  ,cAmtPaid
  ,cRecovered
  ,cBalance
  ,iIncidentAddressID
  ,cTotExcess
  ,cExcessRecovered
  ,sAssignedTo
  ,dtCreated
  ,dLastUpdated
  ,sUpdatedBy
  ,dtRegistered
  ,cFirstExcessEst
  ,cSalvage
  ,cClaimFee
  ,sSubCauseCode
  ,L_SubCauseCode.sDescription AS sSubCauseCode_Desc
  ,sFinAccount
  ,dPolicyCoverStart
  ,dPolicyCoverEnd
  ,iMasterClaimID
  ,BurningCostID
  ,CorporateEntityID
  ,CatastropheId
  ,RIArrangementID
  ,ski_int.fn_GetCompanyIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS CompanyID
  ,dbo.fn_GetCompanyAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS Company
  ,ski_int.fn_GetBrokerIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS BrokerID
  ,dbo.fn_GetBrokerAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS Broker
  ,ski_int.fn_GetAgentIDAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS [Sub-AgentID]
  ,dbo.fn_GetAgentAtDateOfLoss(C.iClaimID,C.iPolicyID, C.dtEvent) AS [Sub-Agent]
  ,dbo.GetRiskSIforClaim(C.iRiskID,C.dtEvent) AS SumInsured

FROM 
  SKI_INT.EDW_Claims_Delta(NOLOCK) C
JOIN Lookup L_CauseCode WITH (NOEXPAND NOLOCK)
  ON L_CauseCode.sDBValue = C.sCauseCode
  AND L_CauseCode.sGroup = 'ClaimCauseCode'
JOIN Lookup L_ClaimStatus WITH (NOEXPAND NOLOCK)
  ON L_ClaimStatus.sDBValue = C.sStatusCode
  AND L_ClaimStatus.sGroup = 'ClaimStatus'
JOIN Lookup L_ApprovalCodes WITH (NOEXPAND NOLOCK)
  ON L_ApprovalCodes.sDBValue = C.sApprovalCode
  AND L_ApprovalCodes.sGroup = 'ClaimApprovalCodes'
JOIN Lookup L_SubCauseCode WITH (NOEXPAND NOLOCK)
  ON L_SubCauseCode.sDBValue = C.sSubCauseCode
  AND L_SubCauseCode.sGroup = 'ClaimSubCauseCode'
order by c.iClaimID
--140620
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Address_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Address_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Address;

INSERT INTO SKI_INT.EDW_Address
SELECT *
FROM SKI_INT.EDW_Address_Delta(NOLOCK)
GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Policy_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Policy_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Policy;

INSERT INTO SKI_INT.EDW_Policy
SELECT iPolicyID
  ,iCustomerID
  ,P.iProductID
  ,PR.sProductName AS sProduct_Desc
  ,sPolicyNo
  ,sOldPolicyNo
  ,iPolicyStatus
  ,lps.sDescription AS iPolicyStatus_Desc
  ,iPaymentStatus
  ,lpps.sDescription AS iPaymentStatus_Desc
  ,dRenewalDate
  ,dLastPaymentDate
  ,dNextPaymentDate
  ,dExpiryDate
  ,dStartDate
  ,dDateCreated
  ,iPaymentTerm
  ,L_PaymentTerm.sDescription AS iPaymentTerm_Desc
  ,cNextPaymentAmt
  ,cAnnualPremium
  ,iTaxPerc
  ,iDiscountPerc
  ,sComment
  ,sAccHolderName
  ,iPaymentMethod
  ,L_PaymentMethod.sDescription AS iPaymentMethod_Desc
  ,sBank
  ,sActionedBy
  ,iCommissionMethod
  ,L_CommissionMethod.sDescription AS iCommissionMethod_Desc
  ,cCommissionAmt
  ,cCommissionPaid
  ,sPolicyType
  ,dDateModified
  ,iTranID
  ,sBranchCode
  ,sCheckDigit
  ,sExpiryDate
  ,sAccountNo
  ,sAccountType
  ,sCancelReason
  ,sCreditCardType
  ,dEffectiveDate
  ,cAdminFee
  ,iCollection
  ,cAnnualSASRIA
  ,cPolicyFee
  ,P.iCurrency
  ,L_Currency.sDescription AS iCurrency_Desc
  ,cBrokerFee
  ,iAdminType
  ,CASE WHEN iAdminType = 0 THEN 'Flat Fee' ELSE 'Percentage with Min and Max Values' END AS sAdminType_Desc
  ,cAdminPerc
  ,cAdminMin
  ,cAdminMax
  ,iParentID
  ,iTreatyYear
  ,dCoverStart
  ,sFinAccount
  ,dCancelled
  ,iCoverTerm
  ,L_CoverTerm.sDescription AS iCoverTerm_Desc
  ,sEndorseCode
  ,CASE WHEN L_PolicyEndorseReason.sDescription IS NULL THEN sEndorseCode ELSE L_PolicyEndorseReason.sDescription END AS sEndorseCode_Desc
  ,eCommCalcMethod
  ,iCommPayLevel
  ,L_CommLevels.sDescription AS iCommPayLevel_Desc
  ,iIndemnityPeriod
  ,L_IndemnityPeriod.sDescription AS iIndemnityPeriod_Desc
  ,dReviewDate
  ,sLanguage
  ,L_Language.sDescription AS sLanguage_Desc
  ,dRenewalPrepDate
  ,CancelReasonCode
  ,L_CancelReason.sDescription AS CancelReasonCode_Desc
  ,CountryId
  ,COU.Name AS Country_Desc
  ,CashbackEnabled
  ,Lev1Name
  ,Lev1Perc
  ,Lev2Name
  ,Lev2Perc
  ,Lev3Name
  ,Lev3Perc
  ,Lev4Name
  ,Lev4Perc
  ,Lev5Name
  ,Lev5Perc

FROM 
SKI_INT.EDW_Policy_Delta (NOLOCK) P
JOIN Products PR (NOLOCK)
  ON PR.iProductID = P.iProductID
JOIN Lookup LPS WITH (NOEXPAND NOLOCK)
  ON LPS.iIndex = P.iPolicyStatus
  AND LPS.sGroup = 'PolicyStatus'
LEFT JOIN Lookup LPPS WITH (NOEXPAND NOLOCK)
  ON LPPS.iIndex = P.iPaymentStatus
  AND LPPS.sGroup = 'PaymentStatus'
JOIN Lookup L_PaymentTerm WITH (NOEXPAND NOLOCK)
  ON L_PaymentTerm.iIndex = P.iPaymentTerm
  AND L_PaymentTerm.sGroup = 'Payment Term'
JOIN Lookup L_CoverTerm WITH (NOEXPAND NOLOCK)
  ON L_CoverTerm.iIndex = P.iCoverTerm
  AND L_CoverTerm.sGroup = 'CoverTerm'
JOIN Lookup L_PaymentMethod WITH (NOEXPAND NOLOCK)
  ON L_PaymentMethod.iIndex = P.iPaymentMethod
  AND L_PaymentMethod.sGroup = 'PaymentMethod'
LEFT JOIN Lookup L_CommissionMethod WITH (NOEXPAND NOLOCK)
  ON L_CommissionMethod.iIndex = P.iCommissionMethod
  AND L_CommissionMethod.sGroup = 'CommissionMethod'
LEFT JOIN Lookup L_Currency WITH (NOEXPAND NOLOCK)
  ON L_Currency.iIndex = P.iCurrency
  AND L_Currency.sGroup = 'Currency'
LEFT JOIN Lookup L_PolicyEndorseReason WITH (NOEXPAND NOLOCK)
  ON L_PolicyEndorseReason.sDBValue = P.sEndorseCode
  AND L_PolicyEndorseReason.sGroup = 'PolicyEndorseReason'
LEFT JOIN Lookup L_CommLevels WITH (NOEXPAND NOLOCK)
  ON L_CommLevels.iIndex = P.iCommPayLevel
  AND L_CommLevels.sGroup = 'CommLevels'
LEFT JOIN Lookup L_IndemnityPeriod WITH (NOEXPAND NOLOCK)
  ON L_IndemnityPeriod.iIndex = P.iIndemnityPeriod
  AND L_IndemnityPeriod.sGroup = 'BIIndemnityPeriodDOM'
LEFT JOIN Lookup L_Language WITH (NOEXPAND NOLOCK)
  ON L_Language.sDBValue = P.sLanguage
  AND L_Language.sGroup = 'LANGUAGE'
LEFT JOIN Lookup L_CancelReason WITH (NOEXPAND NOLOCK)
  ON L_CancelReason.sDBValue = P.CancelReasonCode
  AND L_CancelReason.sGroup = 'PolicyCancellationRe'
LEFT JOIN CFG.Country COU (NOLOCK)
  ON COU.Id = P.CountryId


GO

/****** Object:  StoredProcedure [SKI_INT].[Insert_EDW_Customer_Staging]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[Insert_EDW_Customer_Staging]
AS
TRUNCATE TABLE SKI_INT.EDW_Customer;

INSERT INTO SKI_INT.EDW_Customer
select 
   iCustomerID
  ,iAgentID
  ,ce.sName AS sAgent_Desc
  ,sLastName
  ,sFirstName
  ,sInitials
  ,dDateCreated
  ,c.dLastUpdated
  ,bCompanyInd
  ,c.sUpdatedBy,rPostAddressID
  ,rPhysAddressID
  ,rWorkAddressID
  ,sCompanyName
  ,sContactName
  ,iTranID
  ,sGroupRelationShip
  ,sLanguage
  ,C.sLanguage_Desc
  ,CountryId
  ,Country_Desc
FROM 
  SKI_INT.EDW_Customer_Delta C WITH (NOLOCK)
  JOIN dbo.CommstructEntityRel crr (NOLOCK) ON crr.iCommRelid = c.iAgentid
  JOIN dbo.Commentities ce (NOLOCK) ON ce.iCommEntityid = crr.iLev2Entityid

GO

/****** Object:  StoredProcedure [SKI_INT].[SetDeltaValues]    Script Date: 16/01/2018 01:36:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [SKI_INT].[SetDeltaValues]
AS
----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Policy;

INSERT INTO SKI_INT.Delta_Policy
SELECT DISTINCT TOP 1000 iCustomerID
	,iPolicyID
	,iTranID
FROM Policy(NOLOCK)
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)

UNION

SELECT DISTINCT TOP 1000 ARCH_Policy.iCustomerID
	,ARCH_Policy.iPolicyID
	,ARCH_Policy.iTranID
FROM ARCH_Policy(NOLOCK)
INNER JOIN Policy Pol(NOLOCK) ON Pol.iPolicyID = ARCH_Policy.iPolicyID
WHERE CONVERT(CHAR(8), ARCH_Policy.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Policy'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Policy'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Policy(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Customer;

INSERT INTO SKI_INT.Delta_Customer
SELECT DISTINCT Customer.iCustomerID
	,Customer.iTranID
FROM Customer(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Customer.iCustomerID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)

UNION

SELECT DISTINCT ARCH_Customer.iCustomerID
	,ARCH_Customer.iTranID
FROM ARCH_Customer(NOLOCK)
INNER JOIN Customer Cust(NOLOCK) ON Cust.iCustomerID = ARCH_Customer.iCustomerID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Customer.iCustomerID
WHERE CONVERT(CHAR(8), ARCH_Customer.dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Customer'
		)
ORDER BY iCustomerID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Customer'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Customer(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Claims;

INSERT INTO SKI_INT.Delta_Claims
SELECT DISTINCT iClientID
	,Claims.iClaimID
	,Claims.iPolicyID
FROM Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)

UNION

SELECT DISTINCT ARCH_Claims.iClientID
	,ARCH_Claims.iClaimID
	,ARCH_Claims.iPolicyID
FROM ARCH_Claims(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iCustomerID = ARCH_Claims.iClientID
WHERE CONVERT(CHAR(8), dLastUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Claims'
		)
ORDER BY iClientID desc

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Claims'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Claims(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_ClaimTransactions;

INSERT INTO SKI_INT.Delta_ClaimTransactions
SELECT DISTINCT ClaimTransactions.iClaimID
	,iTransactionID
	,iClaimantID
FROM ClaimTransactions(NOLOCK)
JOIN SKI_INT.Delta_Claims CC (NOLOCK)
  ON CC.iClaimID = ClaimTransactions.iClaimID
WHERE CONVERT(CHAR(8), dtTransaction, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_ClaimTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_ClaimTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_ClaimTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_PolicyTransactions;

INSERT INTO SKI_INT.Delta_PolicyTransactions
SELECT DISTINCT PolicyTransactions.iPolicyID
	,iTransactionID
FROM PolicyTransactions(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = PolicyTransactions.iPolicyId
WHERE CASE 
		WHEN dTransaction > dEffective
			THEN CONVERT(CHAR(8), dEffective, 112)
		ELSE CONVERT(CHAR(8), dTransaction, 112)
		END >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_PolicyTransactions'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_PolicyTransactions'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_PolicyTransactions(NOLOCK)
		)
	,GETDATE()
	);

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_Risks;

INSERT INTO SKI_INT.Delta_Risks
SELECT DISTINCT intPolicyID
	,intRiskID
	,Risks.iTranID
FROM Risks(NOLOCK)
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = Risks.intPolicyID
WHERE CONVERT(CHAR(8), dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

UNION

SELECT DISTINCT ARCH_Risks.intPolicyID
	,ARCH_Risks.intRiskID
	,ARCH_Risks.iTranID
FROM ARCH_Risks(NOLOCK)
INNER JOIN Risks RR(NOLOCK) ON RR.intRiskID = ARCH_Risks.intRiskID
JOIN SKI_INT.Delta_Policy CC (NOLOCK)
  ON CC.iPolicyID = RR.intPolicyID
WHERE CONVERT(CHAR(8), ARCH_Risks.dDateModified, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_Risks'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_Risks'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_Risks(NOLOCK)
		)
	,GETDATE()
	)

----------------------------------------------------------------------
TRUNCATE TABLE SKI_INT.Delta_DebitNoteBalance;

INSERT INTO SKI_INT.Delta_DebitNoteBalance
SELECT DISTINCT iDebitNoteID
FROM DebitNoteBalance (NOLOCK)
WHERE CONVERT(CHAR(8), dUpdated, 112) >= (
		SELECT MAX(LastRunDate)
		FROM SKI_INT.EDW_RunStatus (NOLOCK)
		WHERE TableName = 'Delta_DebitNoteBalance'
		)

INSERT INTO SKI_INT.EDW_RunStatus
VALUES (
	'Delta_DebitNoteBalance'
	,(
		SELECT COUNT(*)
		FROM SKI_INT.Delta_DebitNoteBalance(NOLOCK)
		)
	,GETDATE()
	)

    
GO


