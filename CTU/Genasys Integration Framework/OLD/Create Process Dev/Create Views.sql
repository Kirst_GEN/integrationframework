USE [SKi_CTU_DEV]
GO

/****** Object:  View [SKI_INT].[v_Get_Delta_PolicyTransactionIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_PolicyTransactionIDs]
AS
SELECT iTransactionID
FROM Ski_int.Delta_PolicyTransactions (NOLOCK)
GO

/****** Object:  View [SKI_INT].[EDW_PolTran_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTran_Delta]
AS
SELECT DISTINCT pt.iTransactionId
	,iPolicyId
	,sReference
	,iAgencyId
	,iTrantype
	,cPremium
	,cCommission
	,cAdminfee
	,cSASRIA
	,dTransaction
	,sUser
	,dPeriodStart
	,dPeriodEnd
	,iCommStatus
	,iParentTranID
	,iDebitNoteId
	,cPolicyFee
	,sComment
	,dEffective
	,cBrokerFee
	,sFinAccount
FROM PolicyTransactions pt (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs delta (NOLOCK) ON delta.iTransactionID = pt.iTransactionId
GO

/****** Object:  View [SKI_INT].[vw_PolicyCommStruct]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[vw_PolicyCommStruct]
AS
SELECT PCS.iPolicyID [PolicyId]
	,PCS.iVersionNo [VersionId]
	,CE1.sName [Lev1Name]
	,PCS.iLev1EntityPerc [Lev1Perc]
	,CE2.sName [Lev2Name]
	,PCS.iLev2EntityPerc [Lev2Perc]
	,CE3.sName [Lev3Name]
	,PCS.iLev3EntityPerc [Lev3Perc]
	,CE4.sName [Lev4Name]
	,PCS.iLev4EntityPerc [Lev4Perc]
	,CE5.sName [Lev5Name]
	,PCS.iLev5EntityPerc [Lev5Perc]
FROM PolicyCommStruct PCS (NOLOCK)
JOIN SKI_INT.Delta_Policy dp (NOLOCK) ON PCS.iPolicyID = dp.iPolicyID
LEFT JOIN CommEntities CE1 (NOLOCK) ON CE1.iCommEntityID = PCS.iLev1EntityID
LEFT JOIN CommEntities CE2 (NOLOCK) ON CE2.iCommEntityID = PCS.iLev2EntityID
LEFT JOIN CommEntities CE3 (NOLOCK) ON CE3.iCommEntityID = PCS.iLev3EntityID
LEFT JOIN CommEntities CE4 (NOLOCK) ON CE4.iCommEntityID = PCS.iLev4EntityID
LEFT JOIN CommEntities CE5 (NOLOCK) ON CE5.iCommEntityID = PCS.iLev5EntityID
GO

/****** Object:  View [SKI_INT].[EDW_PolTranSummary_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTranSummary_Delta]
AS
SELECT DISTINCT Pol.iPolicyID AS [PolicyID]
	,SUM(PT.cPremium) AS [PREMIUM BAL]
	,SUM(PT.cCommission) AS [COMMISSOIN BAL]
	,SUM(PT.cSASRIA) AS [SASRIA BAL]
	,SUM(PT.cAdminFee) AS [ADMIN FEE BAL]
	,SUM(PT.cPolicyFee) AS [POLICY FEE BAL]
	,SUM(PT.cBrokerFee) AS [BROKER FEE BAL]
FROM Policy Pol (NOLOCK)
INNER JOIN PolicyTransactions PT (NOLOCK) ON PT.iPolicyID = Pol.iPolicyID
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs DELTA (NOLOCK) ON DELTA.iTransactionID = PT.iTransactionId
INNER JOIN SKI_INT.vw_PolicyCommStruct PCS (NOLOCK) ON PCS.PolicyId = Pol.iPolicyID
INNER JOIN Products PR (NOLOCK) ON PR.iProductID = Pol.iProductID
INNER JOIN LOOKUP LT (NOLOCK) ON LT.iIndex = PT.iTranType
	AND LT.sGroup = 'TransactionType'
GROUP BY Pol.iPolicyID
GO

/****** Object:  View [SKI_INT].[v_Get_Delta_RiskIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_RiskIDs]
AS
SELECT intRiskID
	,iTranID
FROM Ski_int.Delta_Risks (NOLOCK)
GO

/****** Object:  View [SKI_INT].[EDW_Risks_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Risks_Delta]
AS
SELECT DISTINCT intClientID
	,intRiskTypeID
	,r.intRiskID
	,intPolicyID
	,strDescription
	,cSumInsured
	,dDateCreated
	,strCreatedBy
	,intStatus
	,intCurrency
	,cAdminFee
	,iProductID
	,dDateModified
	,r.iTranID
	,iPaymentTerm
	,cPremium
	,cSASRIA
	,cCommission
	,dValidTill
	,cVat
	,sLastUpdatedBy
	,dEffectiveDate
	,iAddressID
	,iParentID
	,cAnnualPremium
	,cAnnualComm
	,cAnnualSASRIA
	,cAnnualAdminFee
	,sDeleteReason
	,sSASRIAGroup
	,dInception
	,cCommissionPerc
	,sMatching
	,cCommissionRebate
	,dReviewDate
	,sReviewer
	,PremiumPerc
FROM Risks R(NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_RiskIDs DELTA(NOLOCK) ON DELTA.intRiskID = R.intRiskID
	AND DELTA.iTranID = R.iTranID

GO

/****** Object:  View [SKI_INT].[EDW_PolTranDet_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolTranDet_Delta]
AS
SELECT DISTINCT ptd.iTransactionID
	,iPolicyID
	,iRiskID
	,iRiskTypeID
	,dDateActioned
	,iTranType
	,cPremium
	,cCommission
	,cAdminFee
	,cSASRIA
	,sUser
FROM PolicyTransactionDetails ptd (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyTransactionIDs DELTA (NOLOCK) ON DELTA.iTransactionID = ptd.iTransactionId
GO

/****** Object:  View [SKI_INT].[v_Get_Delta_ClaimIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_ClaimIDs]
AS
SELECT DISTINCT iClaimID
FROM Ski_int.Delta_Claims (NOLOCK)
GO

/****** Object:  View [SKI_INT].[V_ClaimValues]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[V_ClaimValues]
AS
SELECT aa.iClaimantID
	,aa.iClaimid
	,SUM(aa.cEstimate) cEstimate
	,SUM(aa.cExcess) cExcess
	,SUM(aa.cAmtPaid) cAmtPaid
	,SUM(aa.cRecovery) cRecovery
	,SUM(aa.cSalvage) cSalvage
	,SUM(aa.cEstimate - aa.cExcess - aa.cAmtpaid) cBalance
	,SUM(aa.cEstimate - aa.cExcess - aa.cAmtpaid) + SUM(aa.cAmtpaid) - SUM(aa.cRecovery + aa.cSalvage) cClaimCost
FROM (
	SELECT ct.iClaimid
		,ct.iClaimantid
		,SUM(CASE 
				WHEN ct.sTransactionCode IN (
						'CLEst'
						,'CLRevEst'
						)
					THEN ct.cValue
				ELSE 0
				END) cEstimate
		,SUM(CASE 
				WHEN ct.sTransactionCode IN (
						'CLExcess'
						,'CLRevExcess'
						)
					THEN - ct.cValue
				ELSE 0
				END) cExcess
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CTP'
					THEN ct.cValue
				ELSE 0
				END) cAmtPaid
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CTR'
					THEN - ct.cValue
				ELSE 0
				END) cRecovery
		,SUM(CASE 
				WHEN ct.sTransactionCode = 'CLSALVAGE'
					THEN - ct.cValue
				ELSE 0
				END) cSalvage
	FROM ClaimTransactions ct (NOLOCK)
	JOIN Claimants cla (NOLOCK) ON cla.iClaimantid = ct.iClaimantid
	WHERE ct.iClaimID = cla.iClaimID
	GROUP BY ct.iClaimid
		,ct.iClaimantid
	) aa
JOIN SKI_INT.v_Get_Delta_ClaimIDs DeltaClaims (NOLOCK) ON DeltaClaims.iClaimID = aa.iClaimID
GROUP BY aa.iClaimantID
	,aa.iClaimID
GO

/****** Object:  View [SKI_INT].[EDW_PolicyClaimTransaction_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_PolicyClaimTransaction_Delta]
AS
SELECT CL.iClaimID
	,SUM(VC.cClaimCost) AS [AMT CLAIMED]
	,SUM(VC.cExcess) AS [EXCESS]
	,SUM(VC.cSalvage) AS [SALVAGE]
	,SUM(VC.cRecovery) AS [RECOVERY]
	,SUM(VC.cAmtPaid) AS [PAID]
	,SUM(VC.cBalance) AS [BALANCE]
FROM Claims (NOLOCK) CL
INNER JOIN SKI_INT.v_Get_Delta_ClaimIDs Delta (NOLOCK) ON Delta.iClaimID = CL.iClaimID
INNER JOIN Claimants ca (NOLOCK) ON CA.iClaimID = CL.iClaimID
INNER JOIN SKI_INT.V_ClaimValues VC (NOLOCK) ON VC.iClaimantID = CA.iClaimantID
GROUP BY CL.iClaimID
GO

/****** Object:  View [SKI_INT].[v_Get_ALLClaimDetails]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLClaimDetails]
AS
SELECT cd.iClaimID
	,cd.sFieldCode
	,cd.iFieldType
	,cd.sFieldValue
	,cd.cFieldValue
	,cd.bFieldValue
	,cd.dtFieldValue
	,cd.iFieldValue
	,cd.bReplicated
FROM ClaimDetails cd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_ClaimIDs vcd (NOLOCK) ON cd.iClaimID = vcd.iClaimID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON cd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT acd.iClaimID
	,acd.sFieldCode
	,acd.iFieldType
	,acd.sFieldValue
	,acd.cFieldValue
	,acd.bFieldValue
	,acd.dtFieldValue
	,acd.iFieldValue
	,acd.bReplicated
FROM Arch_ClaimDetails acd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_ClaimIDs vcd (NOLOCK) ON acd.iClaimID = vcd.iClaimID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON acd.sFieldCode = efc.sFieldCode
GO

/****** Object:  View [SKI_INT].[v_Get_ALLRiskDetails]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLRiskDetails]
AS
SELECT rd.iRiskID
  ,rd.sFieldCode
  ,rd.iFieldType
  ,rd.sFieldValue
  ,rd.cFieldValue
  ,rd.bFieldValue
  ,rd.dtFieldValue
  ,rd.iFieldValue
  ,rd.bReplicated
  ,rd.iTranID
  ,rd.sGUID
FROM RiskDetails rd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_RiskIDs vrd (NOLOCK) ON rd.iRiskID = vrd.intRiskID
	AND rd.iTranID = vrd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON rd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT ard.iRiskID
  ,ard.sFieldCode
  ,ard.iFieldType
  ,ard.sFieldValue
  ,ard.cFieldValue
  ,ard.bFieldValue
  ,ard.dtFieldValue
  ,ard.iFieldValue
  ,ard.bReplicated
  ,ard.iTranID
  ,ard.sGUID
FROM Arch_RiskDetails ard (NOLOCK)
JOIN SKI_INT.v_Get_Delta_RiskIDs vrd (NOLOCK) ON ard.iRiskID = vrd.intRiskID
	AND ard.iTranID = vrd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON ard.sFieldCode = efc.sFieldCode

GO

/****** Object:  View [SKI_INT].[v_Get_Delta_PolicyIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_PolicyIDs]
AS
SELECT iPolicyID
	,iTranID
FROM Ski_int.Delta_Policy (NOLOCK)
GO

/****** Object:  View [SKI_INT].[v_Get_ALLPolicyDetails]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLPolicyDetails]
AS
SELECT pd.iPolicyID
	,pd.sFieldCode
	,pd.iFieldType
	,pd.sFieldValue
	,pd.cFieldValue
	,pd.bFieldValue
	,pd.dtFieldValue
	,pd.iFieldValue
	,pd.bReplicated
	,pd.iTranID
	,pd.sGUID
FROM PolicyDetails pd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_PolicyIDs vpd (NOLOCK) ON pd.iPolicyID = vpd.iPolicyID
	AND pd.iTranID = vpd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON pd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT apd.iPolicyID
	,apd.sFieldCode
	,apd.iFieldType
	,apd.sFieldValue
	,apd.cFieldValue
	,apd.bFieldValue
	,apd.dtFieldValue
	,apd.iFieldValue
	,apd.bReplicated
	,apd.iTranID
	,apd.sGUID
FROM Arch_PolicyDetails apd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_PolicyIDs vpd (NOLOCK) ON apd.iPolicyID = vpd.iPolicyID
	AND apd.iTranID = vpd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON apd.sFieldCode = efc.sFieldCode

GO

/****** Object:  View [SKI_INT].[v_Get_Delta_CustomerIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_CustomerIDs]
AS
SELECT DISTINCT iCustomerID
	,iTranID
FROM Ski_int.Delta_Customer (NOLOCK)
GO

/****** Object:  View [SKI_INT].[v_Get_ALLCustomerDetails]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_ALLCustomerDetails]
AS
SELECT cd.iCustomerID
	,cd.sFieldCode
	,cd.iFieldType
	,cd.sFieldValue
	,cd.cFieldValue
	,cd.bFieldValue
	,cd.dtFieldValue
	,cd.iFieldValue
	,cd.bReplicated
	,cd.iTranID
FROM CustomerDetails cd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_CustomerIDs vcd (NOLOCK) ON cd.iCustomerID = vcd.iCustomerID
	AND cd.iTranID = vcd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON cd.sFieldCode = efc.sFieldCode

UNION ALL

SELECT acd.iCustomerID
	,acd.sFieldCode
	,acd.iFieldType
	,acd.sFieldValue
	,acd.cFieldValue
	,acd.bFieldValue
	,acd.dtFieldValue
	,acd.iFieldValue
	,acd.bReplicated
	,acd.iTranID
FROM Arch_CustomerDetails acd (NOLOCK)
JOIN SKI_INT.v_Get_Delta_CustomerIDs vcd (NOLOCK) ON acd.iCustomerID = vcd.iCustomerID
	AND acd.iTranID = vcd.iTranID
JOIN SKI_INT.EDW_FieldCodes efc (NOLOCK) ON acd.sFieldCode = efc.sFieldCode

GO

/****** Object:  View [SKI_INT].[EDW_Policy_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Policy_Delta]
AS
SELECT DISTINCT p.iPolicyID
	,iCustomerID
	,iProductID
	,sPolicyNo
	,sOldPolicyNo
	,iPolicyStatus
	,iPaymentStatus
	,dRenewalDate
	,dLastPaymentDate
	,dNextPaymentDate
	,dExpiryDate
	,dStartDate
	,dDateCreated
	,iPaymentTerm
	,cNextPaymentAmt
	,cAnnualPremium
	,iTaxPerc
	,iDiscountPerc
	,sComment
	,sAccHolderName
	,iPaymentMethod
	,sBank
	,sActionedBy
	,iCommissionMethod
	,cCommissionAmt
	,cCommissionPaid
	,sPolicyType
	,dDateModified
	,p.iTranID
	,sBranchCode
	,sCheckDigit
	,sExpiryDate
	,sAccountNo
	,sAccountType
	,sCancelReason
	,sCreditCardType
	,dEffectiveDate
	,cAdminFee
	,iCollection
	,cAnnualSASRIA
	,cPolicyFee
	,iCurrency
	,cBrokerFee
	,iAdminType
	,cAdminPerc
	,cAdminMin
	,cAdminMax
	,iParentID
	,iTreatyYear
	,dCoverStart
	,sFinAccount
	,dCancelled
	,iCoverTerm
	,sEndorseCode
	,eCommCalcMethod
	,iCommPayLevel
	,iIndemnityPeriod
	,dReviewDate
	,sLanguage
	,dRenewalPrepDate
	,CancelReasonCode
	,CountryId
	,CashbackEnabled
	,pcs.Lev1Name
	,pcs.Lev1Perc
	,pcs.Lev2Name
	,pcs.Lev2Perc
	,pcs.Lev3Name
	,pcs.Lev3Perc
	,pcs.Lev4Name
	,pcs.Lev4Perc
	,pcs.Lev5Name
	,pcs.Lev5Perc
FROM Policy p (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyIDs DELTA(NOLOCK) ON DELTA.iPolicyID = p.iPolicyID
	AND DELTA.iTranID = p.iTranID
INNER JOIN SKI_INT.vw_PolicyCommStruct pcs(NOLOCK) ON pcs.PolicyId = p.iPolicyID
	AND pcs.VersionId = p.iAgencyID

UNION ALL

SELECT DISTINCT p.iPolicyID
	,iCustomerID
	,iProductID
	,sPolicyNo
	,sOldPolicyNo
	,iPolicyStatus
	,iPaymentStatus
	,dRenewalDate
	,dLastPaymentDate
	,dNextPaymentDate
	,dExpiryDate
	,dStartDate
	,dDateCreated
	,iPaymentTerm
	,cNextPaymentAmt
	,cAnnualPremium
	,iTaxPerc
	,iDiscountPerc
	,sComment
	,sAccHolderName
	,iPaymentMethod
	,sBank
	,sActionedBy
	,iCommissionMethod
	,cCommissionAmt
	,cCommissionPaid
	,sPolicyType
	,dDateModified
	,p.iTranID
	,sBranchCode
	,sCheckDigit
	,sExpiryDate
	,sAccountNo
	,sAccountType
	,sCancelReason
	,sCreditCardType
	,dEffectiveDate
	,cAdminFee
	,iCollection
	,cAnnualSASRIA
	,cPolicyFee
	,iCurrency
	,cBrokerFee
	,iAdminType
	,cAdminPerc
	,cAdminMin
	,cAdminMax
	,iParentID
	,iTreatyYear
	,dCoverStart
	,sFinAccount
	,dCancelled
	,iCoverTerm
	,sEndorseCode
	,eCommCalcMethod
	,iCommPayLevel
	,iIndemnityPeriod
	,dReviewDate
	,sLanguage
	,dRenewalPrepDate
	,CancelReasonCode
	,CountryId
	,CashbackEnabled
	,pcs.Lev1Name
	,pcs.Lev1Perc
	,pcs.Lev2Name
	,pcs.Lev2Perc
	,pcs.Lev3Name
	,pcs.Lev3Perc
	,pcs.Lev4Name
	,pcs.Lev4Perc
	,pcs.Lev5Name
	,pcs.Lev5Perc
FROM Arch_Policy p (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_PolicyIDs DELTA(NOLOCK) ON DELTA.iPolicyID = p.iPolicyID
	AND DELTA.iTranID = p.iTranID
INNER JOIN SKI_INT.vw_PolicyCommStruct pcs(NOLOCK) ON pcs.PolicyId = p.iPolicyID
	AND pcs.VersionId = p.iAgencyID

GO

/****** Object:  View [SKI_INT].[EDW_Address_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Address_Delta]
AS
SELECT DISTINCT a.icustomerid
	,intAddressID
	,strAddressType
	,strAddressLine1
	,strAddressLine2
	,strAddressLine3
	,strSuburb
	,strPostalCode
	,strRatingArea
	,strPhoneNo
	,strFaxNo
	,strMobileNo
	,strEmailAdd
	,dUpdated
	,sUpdatedBy
	,sTown
	,a.iTranID
FROM Address(NOLOCK) a
JOIN SKI_INT.v_Get_Delta_CustomerIDs DELTA(NOLOCK) ON DELTA.iCustomerID = a.iCustomerID

GO

/****** Object:  View [SKI_INT].[V_ClaimInitialReserves]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[V_ClaimInitialReserves]
AS
SELECT a.iClaimID
	,SUM([Init Reserve]) AS [Init Reserve]
	,SUM([Init Excess]) AS [Init Excess]
FROM (
	SELECT CT.iClaimID
		,CT.iClaimantID
		,CASE CT.sTransactionCode
			WHEN 'CLEst'
				THEN SUM(cValue)
			END AS [Init Reserve]
		,CASE CT.sTransactionCode
			WHEN 'CLExcess'
				THEN SUM(cValue * - 1)
			END AS [Init Excess]
	FROM Claimtransactions (NOLOCK) CT
	INNER JOIN Claimants (NOLOCK) CA ON CA.iClaimantID = CT.iClaimantID
	INNER JOIN (
		SELECT iClaimID
			,CTMIN.sTransactionCode
			,MIN(CTMIN.dtTransaction) AS MINTRANSDATE
		FROM ClaimTransactions (NOLOCK) CTMIN
		WHERE CTMIN.sTransactionCode IN (
				'CLEst'
				,'CLExcess'
				)
		GROUP BY iClaimID
			,CTMIN.sTransactionCode
		) CTORIGEST ON CTORIGEST.iClaimID = CT.iClaimID
		AND CTORIGEST.sTransactionCode = CT.sTransactionCode
		AND CT.dtTransaction = MINTRANSDATE
	WHERE
		sClaimantName = 'Unallocated Reserve'
	GROUP BY CT.iClaimID
		,CT.iClaimantID
		,CT.sTransactionCode
	) a
JOIN [SKI_INT].[v_Get_Delta_ClaimIDs] (NOLOCK) dcl ON a.iClaimID = dcl.iClaimID
GROUP BY a.iClaimID
GO

/****** Object:  View [SKI_INT].[EDW_Claims_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SKI_INT].[EDW_Claims_Delta]
AS
SELECT DISTINCT CAST(cl.iClaimID AS VARCHAR) AS 'iClaimID'
	,iClientID AS 'iClientID'
	,iRiskID AS 'iRiskID'
	,iPolicyID AS 'iPolicyID'
	,sRefNo
	,sCauseCode
	,sResponsibleName
	,sResponsibleSurname
	,CONVERT(CHAR(10), dtEvent, 111) AS 'dtEvent'
	,sStatusCode
	,CONVERT(CHAR(10), dStatusChanged, 111) AS 'dStatusChanged'
	,REPLACE(REPLACE(sDescription, CHAR(13), ''), CHAR(10), ' ') AS 'sDescription'
	,sComment
	,sApprovalCode
	,bClientResponsible
	,cAmtFirstClaimed
	,cAmtClaimed
	,cAmtPaid
	,cRecovered
	,cBalance
	,CAST(CAST(iIncidentAddressID AS INT) AS VARCHAR) AS 'iIncidentAddressID'
	,cTotExcess
	,cExcessRecovered
	,sAssignedTo
	,CONVERT(CHAR(10), dtCreated, 111) AS 'dtCreated'
	,CONVERT(CHAR(10), dLastUpdated, 111) AS 'dLastUpdated'
	,sUpdatedBy
	,CONVERT(CHAR(10), dtRegistered, 111) AS 'dtRegistered'
	,cFirstExcessEst
	,cSalvage
	,cClaimFee
	,sSubCauseCode
	,sFinAccount
	,CONVERT(CHAR(10), dPolicyCoverStart, 111) AS 'dPolicyCoverStart'
	,CONVERT(CHAR(10), dPolicyCoverEnd, 111) AS 'dPolicyCoverEnd'
	,iMasterClaimID
	,BurningCostID
	,CorporateEntityID
	,CatastropheId
	,RIArrangementID
FROM Claims cl
INNER JOIN SKI_INT.v_Get_Delta_ClaimIDs DeltaClaims(NOLOCK) ON DeltaClaims.iClaimID = cl.iClaimID

GO

/****** Object:  View [SKI_INT].[v_Get_Delta_ClaimTransactionIDs]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[v_Get_Delta_ClaimTransactionIDs]
AS
SELECT iTransactionID
	,iClaimantID
FROM Ski_int.Delta_ClaimTransactions (NOLOCK)
GO

/****** Object:  View [SKI_INT].[EDW_Claimants_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_Claimants_Delta]
AS
SELECT DISTINCT cl.iClaimantID
	,iClaimID
	,sRelationCode
	,sClaimantName
	,iClaimantAddressID
	,sDescription
	,cAmtClaimed
	,cTotExcess
	,cExcessRecovered
	,cAmountPaid
	,cRecovered
	,sAccountHolder
	,sAccountNumber
	,sBranchCode
	,sBankName
	,sCCAccountNumber
	,sCCExpiryDate
	,sCCControlDigits
	,bCreditCard
	,sCreditCardType
	,sAccType
	,sVATType
	,sVATNo
	,sStatus
	,bExcludeCLAmt
	,sLinkFilename
	,cSalvage
	,iVendorID
	,sFinAccount
	,sExternalID
	,ClaimCategory
FROM Claimants cl
INNER JOIN SKI_INT.v_Get_Delta_ClaimTransactionIDs DELTA (NOLOCK) ON DELTA.iClaimantID = cl.iClaimantID

GO

/****** Object:  View [SKI_INT].[EDW_ClaimTran_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [SKI_INT].[EDW_ClaimTran_Delta]
AS
SELECT DISTINCT ct.iTransactionID
	,iClaimID
	,ct.iClaimantID
	,dtTransaction
	,dtPeriodEnd
	,sTransactionCode
	,cValue
	,sReference
	,cVATValue
	,iPayMethod
	,sRequestedBy
	,bReversed
	,iParentPaymentID
	,dtPeriodStart
	,sFinAccount
FROM ClaimTransactions ct (NOLOCK)
INNER JOIN SKI_INT.v_Get_Delta_ClaimTransactionIDs Delta (NOLOCK) ON Delta.iClaimantID = ct.iClaimantID
	AND Delta.iTransactionID = CT.iTransactionID
GO

/****** Object:  View [SKI_INT].[EDW_DebitNotes_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SKI_INT].[EDW_DebitNotes_Delta]
AS
SELECT pt.*
FROM DebitNoteBalance pt (NOLOCK)
INNER JOIN SKI_INT.Delta_DebitNoteBalance delta (NOLOCK) ON delta.idebitNoteID = pt.iDebitNoteId

GO

/****** Object:  View [SKI_INT].[EDW_Customer_Delta]    Script Date: 16/01/2018 01:37:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [SKI_INT].[EDW_Customer_Delta]
AS
SELECT c.iCustomerID
	,iAgentID
	,sLastName
	,sFirstName
	,sInitials
	,convert(char(10),dDateCreated,111) as 'dDateCreated'
	,convert(char(10),dLastUpdated,111) as 'dLastUpdated'
	,case when bCompanyInd = 0 then 'Individual' else 'Company' end as 'bCompanyInd'
	,sUpdatedBy
	,rPostAddressID
	,rPhysAddressID
	,rWorkAddressID
	,sCompanyName
	,sContactName
	,c.iTranID
	,sGroupRelationShip
	,sLanguage
    ,LL.sDescription AS 'sLanguage_Desc'
    ,C.CountryId
	,cntry.Name as 'Country_Desc'
FROM Customer c (NOLOCK)
INNER JOIN SKI_INT.Delta_Customer DELTA(NOLOCK) ON DELTA.iCustomerID = c.iCustomerID
	AND DELTA.iTranID = c.iTranID
INNER JOIN cfg.Country cntry (NOLOCK) ON cntry.Id = c.CountryId
LEFT JOIN Lookup LL WITH (NOEXPAND NOLOCK) 
  ON LL.sDBValue = sLanguage 
  AND LL.sGroup = 'DisplayLang'

UNION ALL

SELECT c.iCustomerID
	,iAgentID
	,sLastName
	,sFirstName
	,sInitials
	,convert(char(10),dDateCreated,111) as 'dDateCreated'
	,convert(char(10),dLastUpdated,111) as 'dLastUpdated'
	,case when bCompanyInd = 0 then 'Individual' else 'Company' end as 'bCompanyInd'
	,sUpdatedBy
	,rPostAddressID
	,rPhysAddressID
	,rWorkAddressID
	,sCompanyName
	,sContactName
	,c.iTranID
	,sGroupRelationShip
	,sLanguage
    ,LL.sDescription AS 'sLanguage_Desc'
    ,C.CountryId
	,cntryc.Name as 'Country_Desc'
FROM Arch_Customer c (NOLOCK)
INNER JOIN SKI_INT.Delta_Customer DELTA (NOLOCK) ON DELTA.iCustomerID = c.iCustomerID
	AND DELTA.iTranID = c.iTranID
INNER JOIN cfg.Country cntryc ON cntryc.Id = c.CountryId
LEFT JOIN Lookup LL WITH (NOEXPAND NOLOCK) 
  ON LL.sDBValue = sLanguage 
  AND LL.sGroup = 'DisplayLang'



GO


