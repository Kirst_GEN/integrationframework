USE [SKi_CTU_DEV]
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_CELLNO', N'Special Lookup', 7, N'Cellphone Number', 1, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_CONTDETAILS', N'String', 0, N'Contact Details', 2, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_DOB', N'Date', 3, N'Client Date Of Birth', 3, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_EMAIL', N'Special Lookup', 7, N'Preferred Email Address', 4, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_FAXTEL', N'Special Lookup', 7, N'Fax Number', 5, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_GENDER', N'Lookup', 6, N'Gender', 6, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_HOMETEL', N'Special Lookup', 7, N'Home Phone Number', 7, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_IDNO', N'Special Lookup', 7, N'ID Number', 8, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_TITLE', N'Lookup', 6, N'Title', 9, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_VIPCLIENT', N'Lookup', 6, N'Is this a VIP client?', 10, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'C_WORKTEL', N'Special Lookup', 7, N'Daytime/Work Phone Number', 11, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CH_INITIALS', N'String', 0, N'Initials', 12, N'Customer')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME', N'Lookup', 6, N'Agent Name', 13, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME_01', N'Lookup', 6, N'Agent Name', 14, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME_02', N'Lookup', 6, N'Agent Name', 15, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME_03', N'Lookup', 6, N'Agent Name', 16, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME_04', N'Lookup', 6, N'Agent Name', 17, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_AGTNAME_05', N'Lookup', 6, N'Agent Name', 18, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_APPDATE', N'Date', 3, N'Approval Date', 19, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_BACKOFFINEG', N'Lookup', 6, N'Back Office Negotiator', 20, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_000', N'Lookup', 6, N'Claim Type', 21, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_002', N'Date', 3, N'Cover and premium confirmation date', 22, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_003', N'Time', 5, N'Time of incident (00:00 format)', 23, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_010', N'Lookup', 6, N'eNatis check done', 24, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_011', N'Lookup', 6, N'Driver tested for Alcohol', 25, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_012', N'Lookup', 6, N'Blood test taken', 26, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_013', N'Lookup', 6, N'Incident reported to SAP', 27, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_014', N'String', 0, N'SAP station name reported to', 28, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_015', N'String', 0, N'SAP reference number', 29, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_016', N'Date', 3, N'Date reported to SAP', 30, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_017', N'Lookup', 6, N'Weather conditions', 31, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_023', N'Lookup', 6, N'Assessor appointed', 32, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_024', N'Lookup', 6, N'Assessor name', 33, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_025', N'Date', 3, N'Assessor Appointed date', 34, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_026', N'Date', 3, N'Assessment date', 35, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_027', N'Lookup', 6, N'Where will vehicle be assessed', 36, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_028', N'Date', 3, N'Assessor report received on (dd/mm/yyyy)', 37, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_029', N'Date', 3, N'Estimated date of repair completion', 38, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_030', N'Lookup', 6, N'Authorisation given to panel beater', 39, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_031', N'Lookup', 6, N'Panel Beater name', 40, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_032', N'Date', 3, N'Date authorisation given to Panel Beater', 41, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_033', N'Lookup', 6, N'Has parts been ordered', 42, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_034', N'Lookup', 6, N'Parts supplier name 1', 43, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_0342', N'Lookup', 6, N'Parts supplier name 2', 44, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_0343', N'Lookup', 6, N'Parts supplier name 3', 45, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_0344', N'Lookup', 6, N'Parts supplier name 4', 46, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_0345', N'Lookup', 6, N'Parts supplier name 5', 47, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_039', N'Lookup', 6, N'Other party involved', 48, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_052', N'Lookup', 6, N'Merit', 49, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_092', N'Lookup', 6, N'Own damage quantum/authorisation confirmed', 50, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094', N'Lookup', 6, N'TP liability claim', 51, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094_01', N'Lookup', 6, N'TP liability claim', 52, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094_02', N'Lookup', 6, N'TP liability claim', 53, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094_03', N'Lookup', 6, N'TP liability claim', 54, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094_04', N'Lookup', 6, N'TP liability claim', 55, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_094_05', N'Lookup', 6, N'TP liability claim', 56, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095', N'Lookup', 6, N'Internal negotiator', 57, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095_01', N'Lookup', 6, N'Internal negotiator', 58, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095_02', N'Lookup', 6, N'Internal negotiator', 59, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095_03', N'Lookup', 6, N'Internal negotiator', 60, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095_04', N'Lookup', 6, N'Internal negotiator', 61, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_095_05', N'Lookup', 6, N'Internal negotiator', 62, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098', N'String', 0, N'TP ID number', 63, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098_01', N'String', 0, N'TP ID number', 64, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098_02', N'String', 0, N'TP ID number', 65, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098_03', N'String', 0, N'TP ID number', 66, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098_04', N'String', 0, N'TP ID number', 67, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_098_05', N'String', 0, N'TP ID number', 68, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099', N'String', 0, N'TP reference number', 69, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099_01', N'String', 0, N'TP reference number', 70, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099_02', N'String', 0, N'TP reference number', 71, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099_03', N'String', 0, N'TP reference number', 72, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099_04', N'String', 0, N'TP reference number', 73, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_099_05', N'String', 0, N'TP reference number', 74, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100', N'String', 0, N'TP Registration number', 75, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100_01', N'String', 0, N'TP Registration number', 76, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100_02', N'String', 0, N'TP Registration number', 77, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100_03', N'String', 0, N'TP Registration number', 78, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100_04', N'String', 0, N'TP Registration number', 79, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_100_05', N'String', 0, N'TP Registration number', 80, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102', N'Lookup', 6, N'Recovery/TP Type', 81, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102_01', N'Lookup', 6, N'Recovery/TP Type', 82, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102_02', N'Lookup', 6, N'Recovery/TP Type', 83, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102_03', N'Lookup', 6, N'Recovery/TP Type', 84, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102_04', N'Lookup', 6, N'Recovery/TP Type', 85, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_102_05', N'Lookup', 6, N'Recovery/TP Type', 86, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106', N'Lookup', 6, N'Uninsured type', 87, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106_01', N'Lookup', 6, N'Uninsured type', 88, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106_02', N'Lookup', 6, N'Uninsured type', 89, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106_03', N'Lookup', 6, N'Uninsured type', 90, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106_04', N'Lookup', 6, N'Uninsured type', 91, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_106_05', N'Lookup', 6, N'Uninsured type', 92, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109', N'Lookup', 6, N'Insurance company name', 93, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109_01', N'Lookup', 6, N'Insurance company name', 94, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109_02', N'Lookup', 6, N'Insurance company name', 95, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109_03', N'Lookup', 6, N'Insurance company name', 96, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109_04', N'Lookup', 6, N'Insurance company name', 97, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_109_05', N'Lookup', 6, N'Insurance company name', 98, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111', N'Lookup', 6, N'Insurance company name', 99, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111_01', N'Lookup', 6, N'Insurance company name', 100, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111_02', N'Lookup', 6, N'Insurance company name', 101, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111_03', N'Lookup', 6, N'Insurance company name', 102, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111_04', N'Lookup', 6, N'Insurance company name', 103, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_111_05', N'Lookup', 6, N'Insurance company name', 104, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115', N'Date', 3, N'Date appointed', 105, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115_01', N'Date', 3, N'Date appointed', 106, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115_02', N'Date', 3, N'Date appointed', 107, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115_03', N'Date', 3, N'Date appointed', 108, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115_04', N'Date', 3, N'Date appointed', 109, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_115_05', N'Date', 3, N'Date appointed', 110, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116', N'Lookup', 6, N'Attorney type', 111, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116_01', N'Lookup', 6, N'Attorney type', 112, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116_02', N'Lookup', 6, N'Attorney type', 113, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116_03', N'Lookup', 6, N'Attorney type', 114, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116_04', N'Lookup', 6, N'Attorney type', 115, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_116_05', N'Lookup', 6, N'Attorney type', 116, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123', N'String', 0, N'Attorney reference number', 117, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123_01', N'String', 0, N'Attorney reference number', 118, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123_02', N'String', 0, N'Attorney reference number', 119, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123_03', N'String', 0, N'Attorney reference number', 120, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123_04', N'String', 0, N'Attorney reference number', 121, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_123_05', N'String', 0, N'Attorney reference number', 122, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124', N'Date', 3, N'Date of summons', 123, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124_01', N'Date', 3, N'Date of summons', 124, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124_02', N'Date', 3, N'Date of summons', 125, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124_03', N'Date', 3, N'Date of summons', 126, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124_04', N'Date', 3, N'Date of summons', 127, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_124_05', N'Date', 3, N'Date of summons', 128, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125', N'Currency', 1, N'Monthy installment amount', 129, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125_01', N'Currency', 1, N'Monthy installment amount', 130, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125_02', N'Currency', 1, N'Monthy installment amount', 131, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125_03', N'Currency', 1, N'Monthy installment amount', 132, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125_04', N'Currency', 1, N'Monthy installment amount', 133, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_125_05', N'Currency', 1, N'Monthy installment amount', 134, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126', N'Currency', 1, N'Full AOD amount', 135, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126_01', N'Currency', 1, N'Full AOD amount', 136, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126_02', N'Currency', 1, N'Full AOD amount', 137, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126_03', N'Currency', 1, N'Full AOD amount', 138, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126_04', N'Currency', 1, N'Full AOD amount', 139, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_126_05', N'Currency', 1, N'Full AOD amount', 140, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127', N'Date', 3, N'Date AOD Signed', 141, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127_01', N'Date', 3, N'Date AOD Signed', 142, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127_02', N'Date', 3, N'Date AOD Signed', 143, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127_03', N'Date', 3, N'Date AOD Signed', 144, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127_04', N'Date', 3, N'Date AOD Signed', 145, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_127_05', N'Date', 3, N'Date AOD Signed', 146, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128', N'Date', 3, N'Date approach send/received', 147, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128_01', N'Date', 3, N'Date approach send/received', 148, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128_02', N'Date', 3, N'Date approach send/received', 149, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128_03', N'Date', 3, N'Date approach send/received', 150, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128_04', N'Date', 3, N'Date approach send/received', 151, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_128_05', N'Date', 3, N'Date approach send/received', 152, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129', N'Currency', 1, N'Claimed amount', 153, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129_01', N'Currency', 1, N'Claimed amount', 154, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129_02', N'Currency', 1, N'Claimed amount', 155, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129_03', N'Currency', 1, N'Claimed amount', 156, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129_04', N'Currency', 1, N'Claimed amount', 157, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_129_05', N'Currency', 1, N'Claimed amount', 158, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130', N'Currency', 1, N'Release/settled amount', 159, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130_01', N'Currency', 1, N'Release/settled amount', 160, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130_02', N'Currency', 1, N'Release/settled amount', 161, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130_03', N'Currency', 1, N'Release/settled amount', 162, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130_04', N'Currency', 1, N'Release/settled amount', 163, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_130_05', N'Currency', 1, N'Release/settled amount', 164, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131', N'Lookup', 6, N'Apportionment', 165, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131_01', N'Lookup', 6, N'Apportionment', 166, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131_02', N'Lookup', 6, N'Apportionment', 167, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131_03', N'Lookup', 6, N'Apportionment', 168, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131_04', N'Lookup', 6, N'Apportionment', 169, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_131_05', N'Lookup', 6, N'Apportionment', 170, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140', N'String', 0, N'Apportionment %', 171, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140_01', N'String', 0, N'Apportionment %', 172, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140_02', N'String', 0, N'Apportionment %', 173, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140_03', N'String', 0, N'Apportionment %', 174, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140_04', N'String', 0, N'Apportionment %', 175, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_140_05', N'String', 0, N'Apportionment %', 176, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141', N'Lookup', 6, N'Recovery/TP status', 177, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141_01', N'Lookup', 6, N'Recovery/TP status', 178, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141_02', N'Lookup', 6, N'Recovery/TP status', 179, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141_03', N'Lookup', 6, N'Recovery/TP status', 180, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141_04', N'Lookup', 6, N'Recovery/TP status', 181, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_141_05', N'Lookup', 6, N'Recovery/TP status', 182, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145', N'Date', 3, N'File close date', 183, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145_01', N'Date', 3, N'File close date', 184, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145_02', N'Date', 3, N'File close date', 185, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145_03', N'Date', 3, N'File close date', 186, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145_04', N'Date', 3, N'File close date', 187, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_145_05', N'Date', 3, N'File close date', 188, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147', N'Lookup', 6, N'Abandonment reasons', 189, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147_01', N'Lookup', 6, N'Abandonment reasons', 190, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147_02', N'Lookup', 6, N'Abandonment reasons', 191, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147_03', N'Lookup', 6, N'Abandonment reasons', 192, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147_04', N'Lookup', 6, N'Abandonment reasons', 193, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_147_05', N'Lookup', 6, N'Abandonment reasons', 194, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162', N'Lookup', 6, N'Reason', 195, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162_01', N'Lookup', 6, N'Reason', 196, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162_02', N'Lookup', 6, N'Reason', 197, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162_03', N'Lookup', 6, N'Reason', 198, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162_04', N'Lookup', 6, N'Reason', 199, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_162_05', N'Lookup', 6, N'Reason', 200, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_163', N'Lookup', 6, N'Assessor report received', 201, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200', N'Lookup', 6, N'Tracer Appointed', 202, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200_01', N'Lookup', 6, N'Tracer Appointed', 203, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200_02', N'Lookup', 6, N'Tracer Appointed', 204, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200_03', N'Lookup', 6, N'Tracer Appointed', 205, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200_04', N'Lookup', 6, N'Tracer Appointed', 206, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_200_05', N'Lookup', 6, N'Tracer Appointed', 207, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201', N'Lookup', 6, N'Tracer Name', 208, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201_01', N'Lookup', 6, N'Tracer Name', 209, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201_02', N'Lookup', 6, N'Tracer Name', 210, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201_03', N'Lookup', 6, N'Tracer Name', 211, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201_04', N'Lookup', 6, N'Tracer Name', 212, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_201_05', N'Lookup', 6, N'Tracer Name', 213, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202', N'Date', 3, N'Date Appointed', 214, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202_01', N'Date', 3, N'Date Appointed', 215, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202_02', N'Date', 3, N'Date Appointed', 216, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202_03', N'Date', 3, N'Date Appointed', 217, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202_04', N'Date', 3, N'Date Appointed', 218, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_202_05', N'Date', 3, N'Date Appointed', 219, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203', N'Date', 3, N'Report Received Date', 220, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203_01', N'Date', 3, N'Report Received Date', 221, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203_02', N'Date', 3, N'Report Received Date', 222, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203_03', N'Date', 3, N'Report Received Date', 223, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203_04', N'Date', 3, N'Report Received Date', 224, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_203_05', N'Date', 3, N'Report Received Date', 225, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_211', N'Lookup', 6, N'Glass to be replaced', 226, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_212', N'Lookup', 6, N'Glass to be replaced', 227, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_213', N'Lookup', 6, N'Glass to be replaced', 228, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_214', N'Lookup', 6, N'Glass to be replaced', 229, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_215', N'Lookup', 6, N'Has own damage claim been accepted', 230, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_216', N'String', 0, N'Own damage claim number', 231, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219', N'Lookup', 6, N'LVA required', 232, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219_01', N'Lookup', 6, N'LVA required', 233, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219_02', N'Lookup', 6, N'LVA required', 234, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219_03', N'Lookup', 6, N'LVA required', 235, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219_04', N'Lookup', 6, N'LVA required', 236, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_219_05', N'Lookup', 6, N'LVA required', 237, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220', N'Date', 3, N'Date instruction given', 238, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220_01', N'Date', 3, N'Date instruction given', 239, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220_02', N'Date', 3, N'Date instruction given', 240, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220_03', N'Date', 3, N'Date instruction given', 241, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220_04', N'Date', 3, N'Date instruction given', 242, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_220_05', N'Date', 3, N'Date instruction given', 243, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222', N'Date', 3, N'LVA report received date', 244, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222_01', N'Date', 3, N'LVA report received date', 245, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222_02', N'Date', 3, N'LVA report received date', 246, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222_03', N'Date', 3, N'LVA report received date', 247, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222_04', N'Date', 3, N'LVA report received date', 248, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_222_05', N'Date', 3, N'LVA report received date', 249, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223', N'Lookup', 6, N'LVA results', 250, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223_01', N'Lookup', 6, N'LVA results', 251, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223_02', N'Lookup', 6, N'LVA results', 252, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223_03', N'Lookup', 6, N'LVA results', 253, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223_04', N'Lookup', 6, N'LVA results', 254, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_223_05', N'Lookup', 6, N'LVA results', 255, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_224', N'Lookup', 6, N'Preferred panel beater', 256, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_225', N'Lookup', 6, N'Panel beater invoice received', 257, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_228', N'Lookup', 6, N'Vehicle code', 258, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_229', N'Lookup', 6, N'AOL sent', 259, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_230', N'Date', 3, N'AOL sent date', 260, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_232', N'Lookup', 6, N'Cash in lieu sent', 261, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_233', N'Date', 3, N'Cash in lieu sent date', 262, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_235', N'Lookup', 6, N'Total loss notification sent to underwriter', 263, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_237', N'Lookup', 6, N'Finance Applicable', 264, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_238', N'Lookup', 6, N'Finance house name', 265, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_239', N'String', 0, N'Finance house reference number', 266, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_240', N'Lookup', 6, N'Keys Received', 267, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_241', N'Lookup', 6, N'Vehicle Recovered', 268, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_243', N'Date', 3, N'Claim notification date', 269, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_247', N'Date', 3, N'Date signed AOL received', 270, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_248', N'Date', 3, N'Date signed CASH IN LIEU received', 271, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCO_01', N'Lookup', 6, N'Insurance Company Name', 272, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCO_02', N'Lookup', 6, N'Insurance Company Name', 273, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCO_03', N'Lookup', 6, N'Insurance Company Name', 274, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCO_04', N'Lookup', 6, N'Insurance Company Name', 275, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCO_05', N'Lookup', 6, N'Insurance Company Name', 276, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_CHK_INSCONME', N'Lookup', 6, N'Insurance Company Name', 277, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_DATOID', N'String', 0, N'Driver ID number at time of accident', 278, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_DATOIN', N'String', 0, N'Driver name and surname at time of accident', 279, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_INVAPP', N'Lookup', 6, N'Investigator Appointed', 280, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_INVNAME', N'Lookup', 6, N'Investigator Name', 281, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_INVNO', N'String', 0, N'Invoice Number', 282, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_INVOICED', N'Lookup', 6, N'Invoiced', 283, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_iSafeIncDate', N'Date', 3, N'iSafe Inception Date', 284, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_LEGALINV', N'Lookup', 6, N'Legal Involved', 285, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT', N'Lookup', 6, N'Outsourcing Agent', 286, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT_01', N'Lookup', 6, N'Outsourcing Agent', 287, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT_02', N'Lookup', 6, N'Outsourcing Agent', 288, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT_03', N'Lookup', 6, N'Outsourcing Agent', 289, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT_04', N'Lookup', 6, N'Outsourcing Agent', 290, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGT_05', N'Lookup', 6, N'Outsourcing Agent', 291, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPD_01', N'Date', 3, N'Outsourcing Agent Appointment Date', 292, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPD_02', N'Date', 3, N'Outsourcing Agent Appointment Date', 293, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPD_03', N'Date', 3, N'Outsourcing Agent Appointment Date', 294, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPD_04', N'Date', 3, N'Outsourcing Agent Appointment Date', 295, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPD_05', N'Date', 3, N'Outsourcing Agent Appointment Date', 296, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTAPPDATE', N'Date', 3, N'Outsourcing Agent Appointment Date', 297, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFN_01', N'String', 0, N'Outsourcing Agent Reference Number', 298, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFN_02', N'String', 0, N'Outsourcing Agent Reference Number', 299, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFN_03', N'String', 0, N'Outsourcing Agent Reference Number', 300, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFN_04', N'String', 0, N'Outsourcing Agent Reference Number', 301, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFN_05', N'String', 0, N'Outsourcing Agent Reference Number', 302, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_OSAGTREFNO', N'String', 0, N'Outsourcing Agent Reference Number', 303, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_PBEATER', N'String', 0, N'Panel Beater Name', 304, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA', N'Lookup', 6, N'Recovery claim', 305, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA_01', N'Lookup', 6, N'Recovery claim', 306, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA_02', N'Lookup', 6, N'Recovery claim', 307, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA_03', N'Lookup', 6, N'Recovery claim', 308, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA_04', N'Lookup', 6, N'Recovery claim', 309, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECCLA_05', N'Lookup', 6, N'Recovery claim', 310, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER', N'Lookup', 6, N'Attorney name', 311, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER_01', N'Lookup', 6, N'Attorney name', 312, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER_02', N'Lookup', 6, N'Attorney name', 313, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER_03', N'Lookup', 6, N'Attorney name', 314, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER_04', N'Lookup', 6, N'Attorney name', 315, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_RECMER_05', N'Lookup', 6, N'Attorney name', 316, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_REPUDE', N'String', 0, N'Repudiation reason', 317, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_REPUDI', N'Lookup', 6, N'Repudiation', 318, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_SALAGREE', N'Lookup', 6, N'Salvage Agreement', 319, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_SALUPLDATE', N'Date', 3, N'Salvage Upliftment Date', 320, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_STKNMBR', N'String', 0, N'Stock Number', 321, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_THICLA', N'Lookup', 6, N'Third Party Claim', 322, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID', N'Lookup', 6, N'TP Insurance Identification', 323, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID_01', N'Lookup', 6, N'TP Insurance Identification', 324, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID_02', N'Lookup', 6, N'TP Insurance Identification', 325, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID_03', N'Lookup', 6, N'TP Insurance Identification', 326, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID_04', N'Lookup', 6, N'TP Insurance Identification', 327, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TPINSURID_05', N'Lookup', 6, N'TP Insurance Identification', 328, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_TrackerRef', N'String', 0, N'Tracker Reference Number', 329, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_VEHDRI', N'Lookup', 6, N'Vehicle drivable', 330, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_VT', N'String', 0, N'Vehicle Type', 331, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_WOCONFDTE', N'Date', 3, N'Write Off Confirmation Date', 332, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'CL_WRTOFF', N'Lookup', 6, N'Write off', 333, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_B_REFACCN', N'String', 0, N'Account Number', 334, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_BANKPROTIND', N'Lookup', 6, N'Bank Protector Policy', 335, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_BANKPROTPNO', N'String', 0, N'Bank Protector Policy number', 336, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_CASENO', N'String', 0, N'Case Number', 337, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_MARKETCODE', N'String', 0, N'Marketers Code', 338, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_MORE5VEH', N'Lookup', 6, N'More than 5 vehicles insured', 339, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_PREMMATREAS', N'Lookup', 6, N'Premium Match Reason', 340, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'P_ServiceProv', N'Lookup', 6, N'Service Provider Policy', 341, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'PH_ACCOUNTNO', N'String', 0, N'Account Number', 342, N'Policy')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CHASSISNO', N'String', 0, N'Chassis number', 343, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_COVERDETAILS', N'String', 0, N'Cover Details', 344, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTAMACLA', N'Special Lookup', 7, N'AMA-Teksi Credit Life ', 345, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTBACK2INV', N'Special Lookup', 7, N'Back to Invoice', 346, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTCASHBACK', N'Special Lookup', 7, N'Cash Back', 347, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTCREDITSF', N'Special Lookup', 7, N'Credit Shortfall', 348, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTEXCESSRO', N'Special Lookup', 7, N'Excess Reducer', 349, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTEXTENDLOU', N'Special Lookup', 7, N'Loss of income Extended ', 350, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTHELIVAC', N'Special Lookup', 7, N'Helivac', 351, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTHELIVAC', N'Special Lookup', 7, N'Helivac', 352, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTHPLEASE', N'Special Lookup', 7, N'Loss of income', 353, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTISAFE', N'Special Lookup', 7, N'iSAFE', 354, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTPASS', N'Special Lookup', 7, N'Passenger Liability', 355, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_CTPERACC', N'Special Lookup', 7, N'Personal Accident', 356, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell1', N'Integer', 4, N'Driver 1 Cell Number', 357, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell2', N'Integer', 4, N'Driver 2 Cell Number', 358, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell3', N'Integer', 4, N'Driver 3 Cell Number', 359, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell4', N'Integer', 4, N'Driver 4 Cell Number', 360, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell5', N'Integer', 4, N'Driver 5 Cell Number', 361, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveCell6', N'Integer', 4, N'Driver 6 Cell Number', 362, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID1', N'String', 0, N'Driver 1 ID Number', 363, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID2', N'String', 0, N'Driver 2 ID Number', 364, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID3', N'String', 0, N'Driver 3 ID Number', 365, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID4', N'String', 0, N'Driver 4 ID Number', 366, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID5', N'String', 0, N'Driver 5 ID Number', 367, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveID6', N'String', 0, N'Driver 6 ID Number', 368, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName1', N'String', 0, N'Driver 1 Name and Surname', 369, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName2', N'String', 0, N'Driver 2 Name and Surname', 370, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName3', N'String', 0, N'Driver 3 Name and Surname', 371, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName4', N'String', 0, N'Driver 4 Name and Surname', 372, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName5', N'String', 0, N'Driver 5 Name and Surname', 373, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_DriveName6', N'String', 0, N'Driver 6 Name and Surname', 374, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_Driver', N'Boolean', 2, N'Driver Details', 375, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_ENGIN_NO', N'String', 0, N'Engine Number', 376, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ABSVIOCRE', N'Boolean', 2, N'Abscond/violation/credit shortfall', 377, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ABSVIOCREDT', N'Date', 3, N'Abscond/Violation Credit Shortfall - Date', 378, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ABSVIOCRESI', N'Special Lookup', 7, N'Abscond/Violation Credit Shortfall - Sum Insured', 379, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ABSVIOSI', N'Special Lookup', 7, N'Sum Insured', 380, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP10000', N'Boolean', 2, N'Additional compulsory FAP R10000', 381, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP15000', N'Boolean', 2, N'Additional compulsory FAP R15000', 382, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP2000', N'Boolean', 2, N'Additional compulsory FAP R2000', 383, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP20000', N'Boolean', 2, N'Additional compulsory FAP R20000', 384, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP2500', N'Boolean', 2, N'Additional compulsory FAP R2500', 385, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP5000', N'Boolean', 2, N'Additional compulsory FAP R5000', 386, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAP7500', N'Boolean', 2, N'Additional compulsory FAP R7500', 387, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAPG1000', N'Boolean', 2, N'Additional compulsory excess FAP R1000 on Glass', 388, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAPG2000', N'Boolean', 2, N'Additional compulsory excess FAP R2000 on Glass', 389, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDFAPG3000', N'Boolean', 2, N'Additional compulsory excess FAP R3000 on Glass', 390, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX10000', N'Boolean', 2, N'Additional compulsory FAP w/ex R10000', 391, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX15000', N'Boolean', 2, N'Additional compulsory FAP w/ex R15000', 392, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX2000', N'Boolean', 2, N'Additional compulsory FAP w/ex R2000', 393, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX20000', N'Boolean', 2, N'Additional compulsory FAP w/ex R20000', 394, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX5000', N'Boolean', 2, N'Additional compulsory FAP w/ex R5000', 395, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ADDWEX7500', N'Boolean', 2, N'Additional compulsory FAP w/ex R7500', 396, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_AMAACCNO', N'String', 0, N'Account Number', 397, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_AMAIDNO', N'String', 0, N'ID Number', 398, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_AMAPREMSI', N'Special Lookup', 7, N'AMA-Teksi Credit Life - Premium', 399, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_AMATEKSI', N'Boolean', 2, N'AMA-TEKSI Credit life', 400, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ASSOC', N'Lookup', 6, N'Taxi Association', 401, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_BACK2INV', N'Lookup', 6, N'Back to Invoice', 402, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_BACK2INVDT', N'Date', 3, N'Back to Invoice - Date', 403, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_BACK2INVSI', N'Special Lookup', 7, N'Back to Invoice - Sum Insured', 404, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CASHBACK', N'Boolean', 2, N'Cash Back', 405, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CASHBACKDT', N'Date', 3, N'Cash Back - Date', 406, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CASHBACKSI', N'Special Lookup', 7, N'Cash Back - Sum Insured', 407, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CRACCNO', N'String', 0, N'Account number', 408, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CREDITPRO', N'Lookup', 6, N'Credit provider', 409, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CREDSFALL', N'Boolean', 2, N'Credit Shortfall', 410, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CSFDT', N'Date', 3, N'Credit Shortfall - Date', 411, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_CSFSI', N'Special Lookup', 7, N'Credit Shortfall - Sum Insured', 412, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_DEALER', N'Lookup', 6, N'Dealer name', 413, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_DRVDEATHSI', N'Special Lookup', 7, N'Sum Insured', 414, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXCESSRO', N'Lookup', 6, N'Excess reducer options', 415, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXCESSRODT', N'Date', 3, N'Excess Reducer - Date', 416, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXCESSROSI', N'Special Lookup', 7, N'Excess Reducer - Sum Insured', 417, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXTENDLOU', N'Boolean', 2, N'Loss of income Extended ', 418, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXTENDLOUDT', N'Date', 3, N'Loss of income Extended  - Date', 419, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_EXTENDLOUSI', N'Special Lookup', 7, N'Loss of income Extended  SI', 420, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HELIVAC', N'Boolean', 2, N'Helivac', 421, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HELIVAC', N'Boolean', 2, N'Helivac', 422, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HELIVACDT', N'Date', 3, N'Helivac - Date', 423, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HELIVACSI', N'Special Lookup', 7, N'Helivac - Sum Insured', 424, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HPLEASE', N'Boolean', 2, N'Loss of income', 425, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HPLEASEDT', N'Date', 3, N'Hire Purchase Lease - Date', 426, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_HPLEASESI', N'Special Lookup', 7, N'Hire Purchase Lease - Sum Insured', 427, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_INTRSTNOTED', N'Boolean', 2, N'Interest Noted', 428, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ISAFE', N'Boolean', 2, N'iSAFE', 429, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ISAFEDT', N'Date', 3, N'iSAFE inception date', 430, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_ISAFESI', N'Special Lookup', 7, N'iSAFE - Sum Insured', 431, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_LOANPRTPR', N'Special Lookup', 7, N'Loan Protector Premium ', 432, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_LOSSINOPSI', N'Special Lookup', 7, N'Loss Of Income Options SI', 433, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_MARKETVAL', N'Number', 10, N'Sum Insured', 434, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_NUOFSEATS', N'Integer', 4, N'Number Of Seats', 435, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PASDEATHSI', N'Special Lookup', 7, N'Sum Insured', 436, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PASLIABIND', N'Boolean', 2, N'Passenger Liability', 437, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PASSENDT', N'Date', 3, N'Passenger Liability - Date', 438, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PASSENSI', N'Special Lookup', 7, N'Passenger Liability - Sum Insured', 439, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PERACCDT', N'Date', 3, N'Personal Accident - Date', 440, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PERACCSI', N'Special Lookup', 7, N'Personal Accident - Sum Insured', 441, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_PERSACCIND', N'Boolean', 2, N'Personal Accident', 442, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_STDENDORSE', N'Boolean', 2, N'Standard Endorsements', 443, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TPOSI', N'Special Lookup', 7, N'Sum Insured', 444, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TRACKCUST', N'Lookup', 6, N'Should tracker contact the customer?', 445, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TRACKREFNO', N'String', 0, N'Tracker Reference Number', 446, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TRACKTYPE', N'Lookup', 6, N'Tracker Type', 447, N'Claims')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TRAILERLOI', N'Special Lookup', 7, N'Premium', 448, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_TVEHDEF', N'Lookup', 6, N'Vehicle definition', 449, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_VEHDEF', N'Lookup', 6, N'Vehicle Type', 450, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_WINDSCRDT', N'Date', 3, N'Windscreen Cover - Date', 451, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_WINDSCREEN', N'Boolean', 2, N'Windscreen Cover', 452, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_G_WINDSCRSI', N'Special Lookup', 7, N'Windscreen Cover - Sum Insured', 453, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_ISFCSHBCK_CPC', N'Special Lookup', 7, N'iSAFE Super Cashback Sum Insured', 454, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_LOI_TOUR_CPC', N'Special Lookup', 7, N'Loss of income for tourism SI', 455, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MATCHING', N'Special Lookup', 7, N'Discount / Loading', 456, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MATCHINGCT', N'Special Lookup', 7, N'Discount / Loading', 457, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MATCHINGPERC', N'String', 0, N'Discount / Loading Perc', 458, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MORE5VEH', N'Boolean', 2, N'More than 5 Vehicles Insured', 459, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MOTORREG', N'String', 0, N'Registration', 460, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MOTORSEATS', N'Lookup', 6, N'Number Of Seats', 461, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_MOTORYOM', N'Integer', 4, N'Year Of Manufacture', 462, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_SI', N'Special Lookup', 7, N'Premium', 463, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'R_TSASRIA', N'Special Lookup', 7, N'Term Sasria Specified', 464, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'RH_SUMINSURED', N'Currency', 1, N'Sum Insured', 465, N'Risk')
GO
INSERT [SKI_INT].[EDW_FieldCodes] ([sFieldCode], [sFieldType], [iFieldType], [sFieldDescription], [iSeq], [sFieldUsage]) VALUES (N'ZIPCODE', N'String', 0, N'Area Code', 466, N'Risk')
GO
SET IDENTITY_INSERT [SKI_INT].[IntegrationMasterFiles] ON 

GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (1, 1, N'SKI_EDW_Customer', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-14' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (2, 1, N'SKI_EDW_Policy', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (3, 1, N'SKI_EDW_Risks', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (4, 1, N'SKI_EDW_Address', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (5, 1, N'SKI_EDW_PolTran', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (6, 1, N'SKI_EDW_PolTranSummary', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (7, 1, N'SKI_EDW_Claimants', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (8, 1, N'SKI_EDW_ClaimTran', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (9, 1, N'SKI_EDW_CustomerDetails', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (10, 1, N'SKI_EDW_PolicyDetails', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (11, 1, N'SKI_EDW_RiskDetails', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (12, 1, N'SKI_EDW_ClaimDetails', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (13, 1, N'SKI_EDW_PolTranDet', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (14, 1, N'SKI_EDW_Claims', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (15, 1, N'SKI_EDW_PolClaimTran', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) VALUES (16, 1, N'SKI_EDW_ClaimVendors', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) 
VALUES (17, 1, N'SKI_EDW_ClaimCatastrophe', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) 
VALUES (18, 1, N'SKI_EDW_Products', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) 
VALUES (19, 1, N'SKI_EDW_FieldCodes', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
INSERT [SKI_INT].[IntegrationMasterFiles] ([ID], [iCustomerID], [sFileName], [sFileDirection], [sFileLocation], [sFileProcessedLocation], [sFileArchiveLocation], [sPackageFolderName], [sPackageProjectName], [sPackageName], [iStatusID], [sCreatedBy], [dCreated]) 
VALUES (20, 1, N'SKI_EDW_DebitNoteBalance', N'EXPORT', N'D:\GenasysExportFramework\Pickup\', N'', N'D:\GenasysExportFramework\Archive\', N'BI360', N'SkiXtract', N'FlatFiles', 1, N'Louwrens Grobler', CAST(N'2017-11-16' AS Date))
GO
SET IDENTITY_INSERT [SKI_INT].[IntegrationMasterFiles] OFF
GO
SET IDENTITY_INSERT [SKI_INT].[IntegrationMasterScripts] ON 

GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (1, 1, N'EDW_Customer_Export', N'Export Customers to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportCustomer]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-14' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (4, 2, N'EDW_Policy_Export', N'Export Policies to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolicy]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (5, 3, N'EDW_Risks_Export', N'Export Risks to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportRisks]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (6, 4, N'EDW_Address_Export', N'Export Addresses to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportAddress]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (7, 5, N'EDW_PolTran_Export', N'Export Policy Transactions to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolTran]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (8, 6, N'EDW_PolTranSummary_Export', N'Export Policy Transactions Summary to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolTranSummary]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (9, 7, N'EDW_Claimants_Export', N'Export Claimants to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaimants]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (10, 8, N'EDW_Claimtran_Export', N'Export Claim Transactions to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaimTran]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (11, 9, N'EDW_CustomerDetails_Export', N'Export Customer Details to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportCustomerDetails]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (12, 10, N'EDW_PolicyDetails_Export', N'Export Policy Details to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolicyDetails]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (13, 11, N'EDW_RiskDetails_Export', N'Export Risk Details to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportRiskDetails]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (14, 12, N'EDW_ClaimDetails_Export', N'Export Claim Details to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaimDetails]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (15, 13, N'EDW_PolTranDet_Export', N'Export Policy Transaction Details to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolTranDet]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (16, 14, N'EDW_Claims_Export', N'Export Claims to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaims]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) VALUES (17, 15, N'EDW_PolClaimTran_Export', N'Export Policy Claim Transactions to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportPolicyClaimTran]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) 
VALUES (18, 16, N'EDW_CaimVendors_Export', N'Export Claim Vendors to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaimVendors]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) 
VALUES (19, 17, N'EDW_ClaimCatastrophe_Export', N'Export Claim Catastrophe to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportClaimCatastrophes]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) 
VALUES (20, 18, N'EDW_Products_Export', N'Export Products to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportProducts]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO
INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) 
VALUES (21, 19, N'EDW_FieldCodes_Export', N'Export FieldCodes to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportFieldCodes]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO

INSERT [SKI_INT].[IntegrationMasterScripts] ([iScriptID], [iIntegrationMasterFileID], [sScriptName], [sScriptDesc], [iFileFormatID], [sSQLSript], [sColumnNames], [sColumnSizes], [sDelimeter], [bHasHeaderRow], [sHeaderRowSQLScript], [bHasFooterRow], [sFooterRowSQLScript], [sCreatedBy], [dCreated], [ExportSequenceNumber]) 
VALUES (22, 20, N'EDW_DebitNoteBalance_Export', N'Export DebitNoteBalance to EDW', 1, N'EXEC [SKI_INT].[EDW_ExportDebitNoteBalance]', N'', N'', N'|', N'N', N'', N'N', N'', N'Louwrens Grobler', CAST(N'2017-11-16' AS Date), 1)
GO

SET IDENTITY_INSERT [SKI_INT].[IntegrationMasterScripts] OFF
GO
