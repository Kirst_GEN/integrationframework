USE [SKi_CTU_DEV]
GO

/****** Object:  Table [SKI_INT].[EDW_PolicyDetails]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_PolicyDetails](
	[iPolicyID] [bigint] NULL,
	[Date Modified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[P_ServiceProv_ID] [varchar](20) NULL,
	[P_ServiceProv] [varchar](255) NULL,
	[P_MARKETCODE] [varchar](255) NULL,
	[P_CASENO] [varchar](255) NULL,
	[P_MORE5VEH_ID] [varchar](20) NULL,
	[P_MORE5VEH] [varchar](255) NULL,
	[P_BANKPROTIND_ID] [varchar](20) NULL,
	[P_BANKPROTIND] [varchar](255) NULL,
	[P_BANKPROTPNO] [varchar](255) NULL,
	[P_PREMMATREAS_ID] [varchar](20) NULL,
	[P_PREMMATREAS] [varchar](255) NULL,
	[P_ADDINFO] [varchar](255) NULL,
	[P_B_REFACCH] [varchar](255) NULL,
	[P_B_REFACCN] [varchar](255) NULL,
	[P_B_REFBRNC] [varchar](255) NULL,
	[P_CWarning] [varchar](255) NULL,
	[P_LASTRD_DOREF] [varchar](255) NULL,
	[P_LASTRD_HEAD] [varchar](255) NULL,
	[P_LASTRD_REASON] [varchar](255) NULL,
	[P_ITCCOLOUR] [varchar](255) NULL,
	[P_QUALQUEST] [varchar](255) NULL,
	[P_UW] [varchar](255) NULL,
	[P_UW_Desc] [varchar](255) NULL,
	[P_LASTRD_AMT] [varchar](255) NULL,
	[P_EXISTDAMAMT] [varchar](255) NULL,
	[P_BROINITFEE] [varchar](255) NULL,
	[P_BROKERBANK] [varchar](255) NULL,
	[P_B_REFUND] [varchar](255) NULL,
	[P_OR_BROKERFEE] [varchar](255) NULL,
	[P_OR_LEGALADV] [varchar](255) NULL,
	[P_VIKELA] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[P_LASTRD_DATE] [varchar](255) NULL,
	[P_POLTYPE] [varchar](255) NULL,
	[P_POLTYPE_Desc] [varchar](255) NULL,
	[P_LICRESTRIC] [varchar](255) NULL,
	[P_LICRESTRIC_Desc] [varchar](255) NULL,
	[P_MORE2CLAIMS] [varchar](255) NULL,
	[P_MORE2CLAIMS_Desc] [varchar](255) NULL,
	[P_CASHBACKBONUS] [varchar](255) NULL,
	[P_CASHBACKBONUS_Desc] [varchar](255) NULL,
	[P_B_REFACCT] [varchar](255) NULL,
	[P_B_REFACCT_Desc] [varchar](255) NULL,
	[P_B_REFBANK] [varchar](255) NULL,
	[P_B_REFBANK_Desc] [varchar](255) NULL,
	[P_EXISTDAM] [varchar](255) NULL,
	[P_EXISTDAM_Desc] [varchar](255) NULL,
	[P_G_SOURCE] [varchar](255) NULL,
	[P_G_SOURCE_Desc] [varchar](255) NULL,
	[P_INSCANC] [varchar](255) NULL,
	[P_INSCANC_Desc] [varchar](255) NULL,
	[P_INSDECL] [varchar](255) NULL,
	[P_INSDECL_Desc] [varchar](255) NULL,
	[P_INSREFUSED] [varchar](255) NULL,
	[P_INSREFUSED_Desc] [varchar](255) NULL,
	[P_ISAFECASHBACK] [varchar](255) NULL,
	[P_ISAFECASHBACK_Desc] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Risks]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Risks](
	[intClientID] [bigint] NULL,
	[intRiskTypeID] [int] NULL,
	[RiskType_Desc] [varchar](50) NULL,
	[intRiskID] [bigint] NULL,
	[intPolicyID] [bigint] NULL,
	[strDescription] [varchar](255) NULL,
	[cSumInsured] [money] NULL,
	[dDateCreated] [datetime] NULL,
	[strCreatedBy] [varchar](255) NULL,
	[intStatus] [int] NULL,
	[intStatus_Desc] [varchar](30) NULL,
	[intCurrency] [varchar](255) NULL,
	[intCurrency_Desc] [varchar](30) NULL,
	[cAdminFee] [money] NULL,
	[iProductID] [int] NULL,
	[iProductID_Desc] [varchar](50) NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[iPaymentTerm] [int] NULL,
	[iPaymentTerm_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cSASRIA] [money] NULL,
	[cCommission] [money] NULL,
	[dValidTill] [datetime] NULL,
	[cVat] [money] NULL,
	[sLastUpdatedBy] [varchar](255) NULL,
	[dEffectiveDate] [datetime] NULL,
	[iAddressID] [bigint] NULL,
	[iParentID] [bigint] NULL,
	[ParentRisk_Desc] [varchar](255) NULL,
	[cAnnualPremium] [money] NULL,
	[cAnnualComm] [money] NULL,
	[cAnnualSASRIA] [money] NULL,
	[cAnnualAdminFee] [money] NULL,
	[sDeleteReason] [varchar](255) NULL,
	[sSASRIAGroup] [varchar](255) NULL,
	[dInception] [datetime] NULL,
	[cCommissionPerc] [varchar](255) NULL,
	[sMatching] [varchar](255) NULL,
	[cCommissionRebate] [varchar](255) NULL,
	[dReviewDate] [datetime] NULL,
	[sReviewer] [varchar](255) NULL,
	[PremiumPerc] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_DebitNoteBalance]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_DebitNoteBalance](
	[iDebitNoteId] [float] NOT NULL,
	[cPremiumBal] [money] NOT NULL,
	[cCommissionBal] [money] NOT NULL,
	[cAdminfeeBal] [money] NOT NULL,
	[cSASRIABal] [money] NOT NULL,
	[dUpdated] [datetime] NOT NULL,
	[sUpdatedBy] [varchar](50) NOT NULL,
	[bReplicated] [bit] NOT NULL,
	[cPolicyFeeBal] [float] NOT NULL,
	[iPolicyID] [float] NOT NULL,
	[cBrokerFeeBal] [money] NOT NULL,
	[iAgentID] [float] NOT NULL,
	[iSubAgentID] [float] NOT NULL,
	[iInvoiceNo] [int] NOT NULL,
	[iEndorsementID] [uniqueidentifier] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[Delta_DebitNoteBalance]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_DebitNoteBalance](
	[iDebitNoteID] [bigint] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[EDW_PolTran]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_PolTran](
	[iTransactionId] [bigint] NULL,
	[iPolicyId] [bigint] NULL,
	[sReference] [varchar](255) NULL,
	[iAgencyId] [bigint] NULL,
	[Agency_Desc] [varchar](200) NULL,
	[iTrantype] [int] NULL,
	[iTrantype_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cCommission] [money] NULL,
	[cAdminfee] [money] NULL,
	[cSASRIA] [money] NULL,
	[dTransaction] [datetime] NULL,
	[sUser] [varchar](255) NULL,
	[dPeriodStart] [datetime] NULL,
	[dPeriodEnd] [datetime] NULL,
	[iCommStatus] [varchar](255) NULL,
	[CommStatus_Desc] [varchar](100) NULL,
	[iParentTranID] [bigint] NULL,
	[iDebitNoteId] [bigint] NULL,
	[cPolicyFee] [money] NULL,
	[sComment] [varchar](255) NULL,
	[dEffective] [datetime] NULL,
	[cBrokerFee] [money] NULL,
	[sFinAccount] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Customer]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Customer](
	[iCustomerID] [bigint] NULL,
	[iAgentID] [int] NULL,
	[sAgent_Desc] [varchar](200) NULL,
	[sLastName] [varchar](255) NULL,
	[sFirstName] [varchar](255) NULL,
	[sInitials] [varchar](255) NULL,
	[dDateCreated] [datetime] NULL,
	[dLastUpdated] [datetime] NULL,
	[bCompanyInd] [varchar](20) NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[rPostAddressID] [bigint] NULL,
	[rPhysAddressID] [bigint] NULL,
	[rWorkAddressID] [bigint] NULL,
	[sCompanyName] [varchar](255) NULL,
	[sContactName] [varchar](255) NULL,
	[iTranID] [bigint] NULL,
	[sGroupRelationShip] [varchar](255) NULL,
	[sLanguage] [varchar](255) NULL,
	[sLanguage_Desc] [varchar](50) NULL,
	[CountryId] [varchar](255) NULL,
	[Country_Desc] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Policy]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Policy](
	[iPolicyID] [bigint] NULL,
	[iCustomerID] [bigint] NULL,
	[iProductID] [int] NULL,
	[sProduct_Desc] [varchar](50) NULL,
	[sPolicyNo] [varchar](255) NULL,
	[sOldPolicyNo] [varchar](255) NULL,
	[iPolicyStatus] [int] NULL,
	[iPolicyStatus_Desc] [varchar](50) NULL,
	[iPaymentStatus] [int] NULL,
	[iPaymentStatus_Desc] [varchar](50) NULL,
	[dRenewalDate] [datetime] NULL,
	[dLastPaymentDate] [datetime] NULL,
	[dNextPaymentDate] [datetime] NULL,
	[dExpiryDate] [datetime] NULL,
	[dStartDate] [datetime] NULL,
	[dDateCreated] [datetime] NULL,
	[iPaymentTerm] [int] NULL,
	[iPaymentTerm_Desc] [varchar](50) NULL,
	[cNextPaymentAmt] [money] NULL,
	[cAnnualPremium] [money] NULL,
	[iTaxPerc] [varchar](255) NULL,
	[iDiscountPerc] [varchar](255) NULL,
	[sComment] [varchar](255) NULL,
	[sAccHolderName] [varchar](255) NULL,
	[iPaymentMethod] [int] NULL,
	[iPaymentMethod_Desc] [varchar](50) NULL,
	[sBank] [varchar](255) NULL,
	[sActionedBy] [varchar](255) NULL,
	[iCommissionMethod] [varchar](255) NULL,
	[iCommissionMethod_Desc] [varchar](50) NULL,
	[cCommissionAmt] [money] NULL,
	[cCommissionPaid] [money] NULL,
	[sPolicyType] [varchar](50) NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[sBranchCode] [varchar](255) NULL,
	[sCheckDigit] [varchar](255) NULL,
	[sExpiryDate] [datetime] NULL,
	[sAccountNo] [varchar](255) NULL,
	[sAccountType] [varchar](255) NULL,
	[sCancelReason] [varchar](255) NULL,
	[sCreditCardType] [varchar](255) NULL,
	[dEffectiveDate] [datetime] NULL,
	[cAdminFee] [money] NULL,
	[iCollection] [int] NULL,
	[cAnnualSASRIA] [money] NULL,
	[cPolicyFee] [money] NULL,
	[iCurrency] [varchar](255) NULL,
	[iCurrency_Desc] [varchar](50) NULL,
	[cBrokerFee] [money] NULL,
	[iAdminType] [varchar](255) NULL,
	[sAdminType_Desc] [varchar](50) NULL,
	[cAdminPerc] [money] NULL,
	[cAdminMin] [money] NULL,
	[cAdminMax] [money] NULL,
	[iParentID] [bigint] NULL,
	[iTreatyYear] [varchar](255) NULL,
	[dCoverStart] [datetime] NULL,
	[sFinAccount] [varchar](255) NULL,
	[dCancelled] [datetime] NULL,
	[iCoverTerm] [int] NULL,
	[iCoverTerm_Desc] [varchar](50) NULL,
	[sEndorseCode] [varchar](255) NULL,
	[sEndorseCode_Desc] [varchar](50) NULL,
	[eCommCalcMethod] [varchar](255) NULL,
	[iCommPayLevel] [varchar](255) NULL,
	[iCommPayLevel_Desc] [varchar](50) NULL,
	[iIndemnityPeriod] [varchar](255) NULL,
	[iIndemnityPeriod_Desc] [varchar](50) NULL,
	[dReviewDate] [datetime] NULL,
	[sLanguage] [varchar](255) NULL,
	[sLanguage_Desc] [varchar](50) NULL,
	[dRenewalPrepDate] [datetime] NULL,
	[CancelReasonCode] [varchar](255) NULL,
	[CancelReasonCode_Desc] [varchar](50) NULL,
	[CountryId] [varchar](255) NULL,
	[Country_Desc] [varchar](50) NULL,
	[CashbackEnabled] [varchar](255) NULL,
	[Lev1Name] [varchar](255) NULL,
	[Lev1Perc] [varchar](255) NULL,
	[Lev2Name] [varchar](255) NULL,
	[Lev2Perc] [varchar](255) NULL,
	[Lev3Name] [varchar](255) NULL,
	[Lev3Perc] [varchar](255) NULL,
	[Lev4Name] [varchar](255) NULL,
	[Lev4Perc] [varchar](255) NULL,
	[Lev5Name] [varchar](255) NULL,
	[Lev5Perc] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Products]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Products](
	[iProductID] [float] NOT NULL,
	[sProductName] [varchar](50) NOT NULL,
	[sProductCode] [varchar](10) NULL,
	[sInsurer] [varchar](15) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_ClaimTran]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_ClaimTran](
	[iTransactionID] [bigint] NULL,
	[iClaimID] [bigint] NULL,
	[iClaimantID] [bigint] NULL,
	[dtTransaction] [datetime] NULL,
	[dtPeriodEnd] [datetime] NULL,
	[sTransactionCode] [varchar](255) NULL,
	[sTransactionCode_Desc] [varchar](50) NULL,
	[cValue] [money] NULL,
	[sReference] [varchar](255) NULL,
	[cVATValue] [money] NULL,
	[iPayMethod] [varchar](255) NULL,
	[PayMethod_Desc] [varchar](50) NULL,
	[sRequestedBy] [varchar](255) NULL,
	[bReversed] [int] NULL,
	[iParentPaymentID] [bigint] NULL,
	[dtPeriodStart] [datetime] NULL,
	[sFinAccount] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Claims]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Claims](
	[iClaimID] [bigint] NULL,
	[iClientID] [bigint] NULL,
	[iRiskID] [bigint] NULL,
	[iPolicyID] [bigint] NULL,
	[sRefNo] [varchar](255) NULL,
	[sCauseCode] [varchar](255) NULL,
	[sCauseCode_Desc] [varchar](100) NULL,
	[sResponsibleName] [varchar](255) NULL,
	[sResponsibleSurname] [varchar](255) NULL,
	[dtEvent] [datetime] NULL,
	[sStatusCode] [varchar](255) NULL,
	[sStatusCode_Desc] [varchar](100) NULL,
	[dStatusChanged] [datetime] NULL,
	[sDescription] [varchar](255) NULL,
	[sComment] [varchar](255) NULL,
	[sApprovalCode] [varchar](255) NULL,
	[sApprovalCode_Desc] [varchar](100) NULL,
	[bClientResponsible] [varchar](255) NULL,
	[cAmtFirstClaimed] [money] NULL,
	[cAmtClaimed] [money] NULL,
	[cAmtPaid] [money] NULL,
	[cRecovered] [money] NULL,
	[cBalance] [money] NULL,
	[iIncidentAddressID] [bigint] NULL,
	[cTotExcess] [money] NULL,
	[cExcessRecovered] [money] NULL,
	[sAssignedTo] [varchar](255) NULL,
	[dtCreated] [datetime] NULL,
	[dLastUpdated] [datetime] NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[dtRegistered] [datetime] NULL,
	[cFirstExcessEst] [money] NULL,
	[cSalvage] [money] NULL,
	[cClaimFee] [money] NULL,
	[sSubCauseCode] [varchar](255) NULL,
	[sSubCauseCode_Desc] [varchar](100) NULL,
	[sFinAccount] [varchar](255) NULL,
	[dPolicyCoverStart] [datetime] NULL,
	[dPolicyCoverEnd] [datetime] NULL,
	[iMasterClaimID] [bigint] NULL,
	[BurningCostID] [varchar](255) NULL,
	[CorporateEntityID] [varchar](255) NULL,
	[CatastropheId] [varchar](255) NULL,
	[RIArrangementID] [varchar](255) NULL,
	[CompanyID] [bigint] NULL,
	[Company] [varchar](200) NULL,
	[BrokerID] [bigint] NULL,
	[Broker] [varchar](200) NULL,
	[Sub-AgentID] [bigint] NULL,
	[Sub-Agent] [varchar](200) NULL,
	[SumInsured] [money] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_ClaimCatastrophe]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_ClaimCatastrophe](
	[CatastropheID] [uniqueidentifier] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Description] [varchar](250) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AffectedArea] [varchar](250) NOT NULL,
	[DateLastUpdated] [datetime] NOT NULL,
	[UserLastUpdated] [varchar](100) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_ClaimVendors]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_ClaimVendors](
	[iVendorID] [int] NOT NULL,
	[VendorName] [varchar](255) NOT NULL,
	[VendorType_Desc] [varchar](50) NOT NULL,
	[VATRate] [varchar](50) NULL,
	[VatNo] [varchar](50) NULL,
	[sAddressLine1] [varchar](50) NULL,
	[sAddressLine2] [varchar](50) NULL,
	[sAddressLine3] [varchar](50) NULL,
	[sSuburb] [varchar](50) NULL,
	[sPostalCode] [varchar](13) NULL,
	[sPhoneNo] [varchar](30) NULL,
	[sFaxNo] [varchar](30) NULL,
	[sEmail] [varchar](50) NULL,
	[dLastUpdated] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_PolicyClaimTran]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[EDW_PolicyClaimTran](
	[iClaimID] [bigint] NOT NULL,
	[AMT CLAIMED] [money] NULL,
	[EXCESS] [money] NULL,
	[SALVAGE] [money] NULL,
	[RECOVERY] [money] NULL,
	[PAID] [money] NULL,
	[BALANCE] [money] NULL,
	[Summary Date] [datetime] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[EDW_Claimants]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Claimants](
	[iClaimantID] [bigint] NULL,
	[iClaimID] [bigint] NULL,
	[sRelationCode] [varchar](255) NULL,
	[sRelationCode_Desc] [varchar](100) NULL,
	[sClaimantName] [varchar](255) NULL,
	[iClaimantAddressID] [bigint] NULL,
	[sDescription] [varchar](255) NULL,
	[cAmtClaimed] [money] NULL,
	[cTotExcess] [money] NULL,
	[cExcessRecovered] [money] NULL,
	[cAmountPaid] [money] NULL,
	[cRecovered] [money] NULL,
	[sAccountHolder] [varchar](255) NULL,
	[sAccountNumber] [varchar](255) NULL,
	[sBranchCode] [varchar](20) NULL,
	[sBankName] [varchar](255) NULL,
	[sCCAccountNumber] [varchar](255) NULL,
	[sCCExpiryDate] [datetime] NULL,
	[sCCControlDigits] [varchar](255) NULL,
	[bCreditCard] [varchar](255) NULL,
	[sCreditCardType] [varchar](255) NULL,
	[sAccType] [varchar](255) NULL,
	[sVATType] [varchar](255) NULL,
	[sVATNo] [varchar](255) NULL,
	[sStatus] [varchar](255) NULL,
	[bExcludeCLAmt] [varchar](255) NULL,
	[sLinkFilename] [varchar](255) NULL,
	[cSalvage] [money] NULL,
	[iVendorID] [varchar](255) NULL,
	[sFinAccount] [varchar](255) NULL,
	[sExternalID] [bigint] NULL,
	[ClaimCategory] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_Address]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_Address](
	[icustomerid] [bigint] NULL,
	[intAddressID] [bigint] NULL,
	[strAddressType] [varchar](10) NULL,
	[strAddressLine1] [varchar](255) NULL,
	[strAddressLine2] [varchar](255) NULL,
	[strAddressLine3] [varchar](255) NULL,
	[strSuburb] [varchar](255) NULL,
	[strPostalCode] [varchar](10) NULL,
	[strRatingArea] [varchar](255) NULL,
	[strPhoneNo] [varchar](30) NULL,
	[strFaxNo] [varchar](30) NULL,
	[strMobileNo] [varchar](30) NULL,
	[strEmailAdd] [varchar](100) NULL,
	[dUpdated] [datetime] NULL,
	[sUpdatedBy] [varchar](255) NULL,
	[sTown] [varchar](255) NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_RiskDetails]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_RiskDetails](
	[intPolicyID] [bigint] NULL,
	[iRiskID] [bigint] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[ZIPCODE] [varchar](255) NULL,
	[R_MOTORSEATS_ID] [varchar](50) NULL,
	[R_MOTORSEATS] [varchar](255) NULL,
	[R_MOTORYOM] [varchar](255) NULL,
	[R_MOTORREG] [varchar](255) NULL,
	[R_ENGIN_NO] [varchar](255) NULL,
	[R_CHASSISNO] [varchar](255) NULL,
	[R_COVERDETAILS] [varchar](255) NULL,
	[R_G_MARKETVAL] [varchar](255) NULL,
	[R_G_PASLIABIND] [varchar](255) NULL,
	[R_G_NUOFSEATS] [varchar](255) NULL,
	[R_G_HPLEASE] [varchar](255) NULL,
	[R_G_EXTENDLOU] [varchar](255) NULL,
	[R_G_PERSACCIND] [varchar](255) NULL,
	[R_G_CREDSFALL] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[R_G_ABSVIOCRE] [varchar](255) NULL,
	[R_G_ISAFE] [varchar](255) NULL,
	[R_G_EXCESSRO_ID] [varchar](50) NULL,
	[R_G_EXCESSRO] [varchar](255) NULL,
	[R_G_WINDSCREEN] [varchar](255) NULL,
	[R_G_BACK2INV_ID] [varchar](50) NULL,
	[R_G_BACK2INV] [varchar](255) NULL,
	[R_G_CASHBACK] [varchar](255) NULL,
	[R_G_DEALER_ID] [varchar](50) NULL,
	[R_G_DEALER] [varchar](255) NULL,
	[R_G_INTRSTNOTED] [varchar](255) NULL,
	[R_G_CREDITPRO_ID] [varchar](50) NULL,
	[R_G_CREDITPRO] [varchar](255) NULL,
	[R_G_CRACCNO] [varchar](255) NULL,
	[R_G_ASSOC_ID] [varchar](50) NULL,
	[R_G_ASSOC] [varchar](255) NULL,
	[R_G_STDENDORSE] [varchar](255) NULL,
	[R_G_ADDFAP2000] [varchar](255) NULL,
	[R_G_ADDFAP2500] [varchar](255) NULL,
	[R_G_ADDFAP5000] [varchar](255) NULL,
	[R_G_ADDFAP7500] [varchar](255) NULL,
	[R_G_ADDFAP10000] [varchar](255) NULL,
	[R_G_ADDFAP15000] [varchar](255) NULL,
	[R_G_ADDFAP20000] [varchar](255) NULL,
	[R_G_AMATEKSI] [varchar](255) NULL,
	[R_G_AMAACCNO] [varchar](255) NULL,
	[R_G_AMAIDNO] [varchar](255) NULL,
	[R_MATCHINGCT] [varchar](255) NULL,
	[R_CTPERACC] [varchar](255) NULL,
	[R_CTHPLEASE] [varchar](255) NULL,
	[R_CTPASS] [varchar](255) NULL,
	[R_CTEXTENDLOU] [varchar](255) NULL,
	[R_CTHELIVAC] [varchar](255) NULL,
	[R_CTAMACLA] [varchar](255) NULL,
	[R_CTBACK2INV] [varchar](255) NULL,
	[R_CTISAFE] [varchar](255) NULL,
	[R_CTCASHBACK] [varchar](255) NULL,
	[R_CTCREDITSF] [varchar](255) NULL,
	[R_CTEXCESSRO] [varchar](255) NULL,
	[R_TSASRIA] [varchar](255) NULL,
	[R_MATCHINGPERC] [varchar](255) NULL,
	[R_MORE5VEH] [varchar](255) NULL,
	[R_SI] [varchar](255) NULL,
	[R_G_PASSENSI] [varchar](255) NULL,
	[R_G_PASSENDT] [varchar](255) NULL,
	[R_G_HPLEASESI] [varchar](255) NULL,
	[R_G_PERACCSI] [varchar](255) NULL,
	[R_G_EXTENDLOUSI] [varchar](255) NULL,
	[R_G_HELIVACSI] [varchar](255) NULL,
	[R_G_CSFSI] [varchar](255) NULL,
	[R_G_ABSVIOCRESI] [varchar](255) NULL,
	[R_G_ISAFESI] [varchar](255) NULL,
	[R_G_EXCESSROSI] [varchar](255) NULL,
	[R_G_WINDSCRSI] [varchar](255) NULL,
	[R_G_BACK2INVSI] [varchar](255) NULL,
	[R_G_CASHBACKSI] [varchar](255) NULL,
	[R_G_AMAPREMSI] [varchar](255) NULL,
	[R_G_VEHDEF_ID] [varchar](50) NULL,
	[R_G_VEHDEF] [varchar](255) NULL,
	[R_G_TRACKCUST_ID] [varchar](50) NULL,
	[R_G_TRACKCUST] [varchar](255) NULL,
	[R_G_TVEHDEF_ID] [varchar](50) NULL,
	[R_G_TVEHDEF] [varchar](255) NULL,
	[R_G_TRAILERLOI] [varchar](255) NULL,
	[R_G_LOANPRTPR] [varchar](255) NULL,
	[R_G_DRVDEATHSI] [varchar](255) NULL,
	[R_MATCHING] [varchar](255) NULL,
	[R_G_PASDEATHSI] [varchar](255) NULL,
	[R_G_TRACKREFNO] [varchar](255) NULL,
	[R_Driver] [varchar](255) NULL,
	[R_DriveName1] [varchar](255) NULL,
	[R_DriveID1] [varchar](255) NULL,
	[R_DriveCell1] [varchar](255) NULL,
	[R_DriveName2] [varchar](255) NULL,
	[R_DriveID2] [varchar](255) NULL,
	[R_DriveCell2] [varchar](255) NULL,
	[R_DriveName3] [varchar](255) NULL,
	[R_DriveID3] [varchar](255) NULL,
	[R_DriveCell3] [varchar](255) NULL,
	[R_DriveName4] [varchar](255) NULL,
	[R_DriveID4] [varchar](255) NULL,
	[R_DriveCell4] [varchar](255) NULL,
	[R_DriveName5] [varchar](255) NULL,
	[R_DriveID5] [varchar](255) NULL,
	[R_DriveCell5] [varchar](255) NULL,
	[R_DriveName6] [varchar](255) NULL,
	[R_DriveID6] [varchar](255) NULL,
	[R_DriveCell6] [varchar](255) NULL,
	[R_G_ADDFAPG1000] [varchar](255) NULL,
	[R_G_ADDFAPG2000] [varchar](255) NULL,
	[R_G_ADDFAPG3000] [varchar](255) NULL,
	[R_G_TPOSI] [varchar](255) NULL,
	[R_G_HPLEASEDT] [varchar](255) NULL,
	[R_G_EXTENDLOUDT] [varchar](255) NULL,
	[R_G_LOSSINOPSI] [varchar](255) NULL,
	[R_G_HELIVACDT] [varchar](255) NULL,
	[R_G_ISAFEDT] [varchar](255) NULL,
	[R_G_PERACCDT] [varchar](255) NULL,
	[R_G_CSFDT] [varchar](255) NULL,
	[R_G_ABSVIOCREDT] [varchar](255) NULL,
	[R_G_EXCESSRODT] [varchar](255) NULL,
	[R_G_WINDSCRDT] [varchar](255) NULL,
	[R_G_BACK2INVDT] [varchar](255) NULL,
	[R_G_CASHBACKDT] [varchar](255) NULL,
	[R_ISFCSHBCK_CPC] [varchar](255) NULL,
	[R_LOI_TOUR_CPC] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_PolTranDet]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_PolTranDet](
	[iTransactionID] [bigint] NULL,
	[iPolicyID] [bigint] NULL,
	[iRiskID] [bigint] NULL,
	[iRiskTypeID] [int] NULL,
	[dDateActioned] [datetime] NULL,
	[iTranType] [int] NULL,
	[iTranType_Desc] [varchar](50) NULL,
	[cPremium] [money] NULL,
	[cCommission] [money] NULL,
	[cAdminFee] [money] NULL,
	[cSASRIA] [money] NULL,
	[sUser] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_CustomerDetails]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_CustomerDetails](
	[iCustomerID] [bigint] NULL,
	[dLastUpdated] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[C_CELLNO] [varchar](255) NULL,
	[C_CONTDETAILS] [varchar](255) NULL,
	[C_DOB] [varchar](255) NULL,
	[C_EMAIL] [varchar](255) NULL,
	[C_FAXTEL] [varchar](255) NULL,
	[C_GENDER] [varchar](255) NULL,
	[C_HOMETEL] [varchar](255) NULL,
	[C_IDNO] [varchar](255) NULL,
	[C_TITLE_ID] [varchar](20) NULL,
	[C_TITLE] [varchar](255) NULL,
	[C_VIPCLIENT_ID] [varchar](50) NULL,
	[C_VIPCLIENT] [varchar](255) NULL,
	[C_WORKTEL] [varchar](255) NULL,
	[CH_INITIALS] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_ClaimDetails]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_ClaimDetails](
	[iClaimID] [bigint] NOT NULL,
	[dLastUpdated] [datetime] NULL,
	[CL_THICLA] [varchar](255) NULL,
	[CL_PBEATER] [varchar](255) NULL,
	[CL_BACKOFFINEG] [varchar](255) NULL,
	[CL_CHK_243] [varchar](255) NULL,
	[CL_CHK_000] [varchar](255) NULL,
	[CL_CHK_002] [varchar](255) NULL,
	[CL_CHK_003] [varchar](255) NULL,
	[CL_REPUDI] [varchar](255) NULL,
	[CL_REPUDE] [varchar](255) NULL,
	[CL_VEHDRI] [varchar](255) NULL,
	[CL_DATOIN] [varchar](255) NULL,
	[CL_DATOID] [varchar](255) NULL,
	[CL_CHK_010] [varchar](255) NULL,
	[CL_APPDATE] [varchar](255) NULL,
	[CL_CHK_011] [varchar](255) NULL,
	[CL_CHK_012] [varchar](255) NULL,
	[CL_CHK_013] [varchar](255) NULL,
	[CL_CHK_014] [varchar](255) NULL,
	[CL_CHK_015] [varchar](255) NULL,
	[CL_CHK_016] [varchar](255) NULL,
	[CL_CHK_240] [varchar](255) NULL,
	[CL_CHK_215] [varchar](255) NULL,
	[CL_CHK_216] [varchar](255) NULL,
	[CL_CHK_241] [varchar](255) NULL,
	[CL_CHK_017] [varchar](255) NULL,
	[CL_CHK_023] [varchar](255) NULL,
	[CL_CHK_024] [varchar](255) NULL,
	[CL_CHK_025] [varchar](255) NULL,
	[CL_CHK_027] [varchar](255) NULL,
	[CL_CHK_163] [varchar](255) NULL,
	[CL_CHK_028] [varchar](255) NULL,
	[CL_CHK_026] [varchar](255) NULL,
	[CL_CHK_029] [varchar](255) NULL,
	[CL_CHK_052] [varchar](255) NULL,
	[CL_CHK_092] [varchar](255) NULL,
	[CL_RECCLA] [varchar](255) NULL,
	[CL_CHK_094] [varchar](255) NULL,
	[CL_CHK_095] [varchar](255) NULL,
	[CL_CHK_098] [varchar](255) NULL,
	[CL_CHK_099] [varchar](255) NULL,
	[CL_CHK_100] [varchar](255) NULL,
	[CL_CHK_128] [varchar](255) NULL,
	[CL_CHK_129] [varchar](255) NULL,
	[CL_CHK_130] [varchar](255) NULL,
	[CL_CHK_131] [varchar](255) NULL,
	[CL_CHK_140] [varchar](255) NULL,
	[CL_CHK_200] [varchar](255) NULL,
	[CL_CHK_201] [varchar](255) NULL,
	[CL_CHK_202] [varchar](255) NULL,
	[CL_CHK_203] [varchar](255) NULL,
	[CL_CHK_219] [varchar](255) NULL,
	[CL_CHK_220] [varchar](255) NULL,
	[CL_CHK_222] [varchar](255) NULL,
	[CL_CHK_223] [varchar](255) NULL,
	[CL_INVAPP] [varchar](255) NULL,
	[CL_INVNAME] [varchar](255) NULL,
	[CL_CHK_237] [varchar](255) NULL,
	[CL_CHK_238] [varchar](255) NULL,
	[CL_CHK_239] [varchar](255) NULL,
	[CL_CHK_030] [varchar](255) NULL,
	[CL_CHK_031] [varchar](255) NULL,
	[CL_CHK_032] [varchar](255) NULL,
	[CL_CHK_224] [varchar](255) NULL,
	[CL_CHK_225] [varchar](255) NULL,
	[CL_CHK_033] [varchar](255) NULL,
	[CL_CHK_034] [varchar](255) NULL,
	[CL_CHK_0342] [varchar](255) NULL,
	[CL_CHK_0343] [varchar](255) NULL,
	[CL_CHK_0344] [varchar](255) NULL,
	[CL_CHK_0345] [varchar](255) NULL,
	[CL_WRTOFF] [varchar](255) NULL,
	[CL_WOCONFDTE] [varchar](255) NULL,
	[CL_STKNMBR] [varchar](255) NULL,
	[CL_SALAGREE] [varchar](255) NULL,
	[CL_SALUPLDATE] [varchar](255) NULL,
	[CL_INVOICED] [varchar](255) NULL,
	[CL_INVNO] [varchar](255) NULL,
	[CL_CHK_228] [varchar](255) NULL,
	[CL_CHK_229] [varchar](255) NULL,
	[CL_CHK_230] [varchar](255) NULL,
	[CL_CHK_247] [varchar](255) NULL,
	[CL_CHK_232] [varchar](255) NULL,
	[CL_CHK_233] [varchar](255) NULL,
	[CL_CHK_248] [varchar](255) NULL,
	[CL_CHK_235] [varchar](255) NULL,
	[CL_CHK_039] [varchar](255) NULL,
	[CL_CHK_102] [varchar](255) NULL,
	[CL_AGTNAME] [varchar](255) NULL,
	[CL_OSAGT] [varchar](255) NULL,
	[CL_OSAGTREFNO] [varchar](255) NULL,
	[CL_OSAGTAPPDATE] [varchar](255) NULL,
	[CL_CHK_106] [varchar](255) NULL,
	[CL_CHK_109] [varchar](255) NULL,
	[CL_CHK_111] [varchar](255) NULL,
	[CL_RECMER] [varchar](255) NULL,
	[CL_CHK_115] [varchar](255) NULL,
	[CL_CHK_116] [varchar](255) NULL,
	[CL_TPINSURID] [varchar](255) NULL,
	[CL_CHK_INSCONME] [varchar](255) NULL,
	[CL_CHK_123] [varchar](255) NULL,
	[CL_CHK_124] [varchar](255) NULL,
	[CL_CHK_125] [varchar](255) NULL,
	[CL_CHK_126] [varchar](255) NULL,
	[CL_CHK_127] [varchar](255) NULL,
	[CL_CHK_141] [varchar](255) NULL,
	[CL_CHK_145] [varchar](255) NULL,
	[CL_CHK_147] [varchar](255) NULL,
	[CL_CHK_162] [varchar](255) NULL,
	[CL_CHK_200_01] [varchar](255) NULL,
	[CL_RECCLA_01] [varchar](255) NULL,
	[CL_CHK_201_01] [varchar](255) NULL,
	[CL_CHK_094_01] [varchar](255) NULL,
	[CL_CHK_202_01] [varchar](255) NULL,
	[CL_CHK_095_01] [varchar](255) NULL,
	[CL_CHK_203_01] [varchar](255) NULL,
	[CL_CHK_098_01] [varchar](255) NULL,
	[CL_CHK_099_01] [varchar](255) NULL,
	[CL_CHK_100_01] [varchar](255) NULL,
	[CL_CHK_128_01] [varchar](255) NULL,
	[CL_CHK_129_01] [varchar](255) NULL,
	[CL_CHK_130_01] [varchar](255) NULL,
	[CL_CHK_131_01] [varchar](255) NULL,
	[CL_CHK_140_01] [varchar](255) NULL,
	[CL_CHK_219_01] [varchar](255) NULL,
	[CL_CHK_220_01] [varchar](255) NULL,
	[CL_CHK_222_01] [varchar](255) NULL,
	[CL_CHK_223_01] [varchar](255) NULL,
	[CL_CHK_102_01] [varchar](255) NULL,
	[CL_AGTNAME_01] [varchar](255) NULL,
	[CL_OSAGT_01] [varchar](255) NULL,
	[CL_OSAGTREFN_01] [varchar](255) NULL,
	[CL_OSAGTAPPD_01] [varchar](255) NULL,
	[CL_CHK_106_01] [varchar](255) NULL,
	[CL_CHK_109_01] [varchar](255) NULL,
	[CL_CHK_111_01] [varchar](255) NULL,
	[CL_RECMER_01] [varchar](255) NULL,
	[CL_CHK_115_01] [varchar](255) NULL,
	[CL_CHK_116_01] [varchar](255) NULL,
	[CL_TPINSURID_01] [varchar](255) NULL,
	[CL_CHK_INSCO_01] [varchar](255) NULL,
	[CL_CHK_123_01] [varchar](255) NULL,
	[CL_CHK_124_01] [varchar](255) NULL,
	[CL_CHK_125_01] [varchar](255) NULL,
	[CL_CHK_126_01] [varchar](255) NULL,
	[CL_CHK_127_01] [varchar](255) NULL,
	[CL_CHK_141_01] [varchar](255) NULL,
	[CL_CHK_145_01] [varchar](255) NULL,
	[CL_CHK_147_01] [varchar](255) NULL,
	[CL_CHK_162_01] [varchar](255) NULL,
	[CL_CHK_200_02] [varchar](255) NULL,
	[CL_RECCLA_02] [varchar](255) NULL,
	[CL_CHK_201_02] [varchar](255) NULL,
	[CL_CHK_094_02] [varchar](255) NULL,
	[CL_CHK_202_02] [varchar](255) NULL,
	[CL_CHK_095_02] [varchar](255) NULL,
	[CL_CHK_203_02] [varchar](255) NULL,
	[CL_CHK_098_02] [varchar](255) NULL,
	[CL_CHK_099_02] [varchar](255) NULL,
	[CL_CHK_100_02] [varchar](255) NULL,
	[CL_CHK_128_02] [varchar](255) NULL,
	[CL_CHK_129_02] [varchar](255) NULL,
	[CL_CHK_130_02] [varchar](255) NULL,
	[CL_CHK_131_02] [varchar](255) NULL,
	[CL_CHK_140_02] [varchar](255) NULL,
	[CL_CHK_219_02] [varchar](255) NULL,
	[CL_CHK_220_02] [varchar](255) NULL,
	[CL_CHK_222_02] [varchar](255) NULL,
	[CL_CHK_223_02] [varchar](255) NULL,
	[CL_CHK_102_02] [varchar](255) NULL,
	[CL_AGTNAME_02] [varchar](255) NULL,
	[CL_OSAGT_02] [varchar](255) NULL,
	[CL_OSAGTREFN_02] [varchar](255) NULL,
	[CL_OSAGTAPPD_02] [varchar](255) NULL,
	[CL_CHK_106_02] [varchar](255) NULL,
	[CL_CHK_109_02] [varchar](255) NULL,
	[CL_CHK_111_02] [varchar](255) NULL,
	[CL_RECMER_02] [varchar](255) NULL,
	[CL_CHK_115_02] [varchar](255) NULL,
	[CL_CHK_116_02] [varchar](255) NULL,
	[CL_TPINSURID_02] [varchar](255) NULL,
	[CL_CHK_INSCO_02] [varchar](255) NULL,
	[CL_CHK_123_02] [varchar](255) NULL,
	[CL_CHK_124_02] [varchar](255) NULL,
	[CL_CHK_125_02] [varchar](255) NULL,
	[CL_CHK_126_02] [varchar](255) NULL,
	[CL_CHK_127_02] [varchar](255) NULL,
	[CL_CHK_141_02] [varchar](255) NULL,
	[CL_CHK_145_02] [varchar](255) NULL,
	[CL_CHK_147_02] [varchar](255) NULL,
	[CL_CHK_162_02] [varchar](255) NULL,
	[CL_CHK_200_03] [varchar](255) NULL,
	[CL_RECCLA_03] [varchar](255) NULL,
	[CL_CHK_201_03] [varchar](255) NULL,
	[CL_CHK_094_03] [varchar](255) NULL,
	[CL_CHK_202_03] [varchar](255) NULL,
	[CL_CHK_095_03] [varchar](255) NULL,
	[CL_CHK_203_03] [varchar](255) NULL,
	[CL_CHK_098_03] [varchar](255) NULL,
	[CL_CHK_099_03] [varchar](255) NULL,
	[CL_CHK_100_03] [varchar](255) NULL,
	[CL_CHK_128_03] [varchar](255) NULL,
	[CL_CHK_129_03] [varchar](255) NULL,
	[CL_CHK_130_03] [varchar](255) NULL,
	[CL_CHK_131_03] [varchar](255) NULL,
	[CL_CHK_140_03] [varchar](255) NULL,
	[CL_CHK_219_03] [varchar](255) NULL,
	[CL_CHK_220_03] [varchar](255) NULL,
	[CL_CHK_222_03] [varchar](255) NULL,
	[CL_CHK_223_03] [varchar](255) NULL,
	[CL_CHK_102_03] [varchar](255) NULL,
	[CL_AGTNAME_03] [varchar](255) NULL,
	[CL_OSAGT_03] [varchar](255) NULL,
	[CL_OSAGTREFN_03] [varchar](255) NULL,
	[CL_OSAGTAPPD_03] [varchar](255) NULL,
	[CL_CHK_106_03] [varchar](255) NULL,
	[CL_CHK_109_03] [varchar](255) NULL,
	[CL_CHK_111_03] [varchar](255) NULL,
	[CL_RECMER_03] [varchar](255) NULL,
	[CL_CHK_115_03] [varchar](255) NULL,
	[CL_CHK_116_03] [varchar](255) NULL,
	[CL_TPINSURID_03] [varchar](255) NULL,
	[CL_CHK_INSCO_03] [varchar](255) NULL,
	[CL_CHK_123_03] [varchar](255) NULL,
	[CL_CHK_124_03] [varchar](255) NULL,
	[CL_CHK_125_03] [varchar](255) NULL,
	[CL_CHK_126_03] [varchar](255) NULL,
	[CL_CHK_127_03] [varchar](255) NULL,
	[CL_CHK_141_03] [varchar](255) NULL,
	[CL_CHK_145_03] [varchar](255) NULL,
	[CL_CHK_147_03] [varchar](255) NULL,
	[CL_CHK_162_03] [varchar](255) NULL,
	[CL_CHK_200_04] [varchar](255) NULL,
	[CL_RECCLA_04] [varchar](255) NULL,
	[CL_CHK_201_04] [varchar](255) NULL,
	[CL_CHK_094_04] [varchar](255) NULL,
	[CL_CHK_202_04] [varchar](255) NULL,
	[CL_CHK_095_04] [varchar](255) NULL,
	[CL_CHK_203_04] [varchar](255) NULL,
	[CL_CHK_098_04] [varchar](255) NULL,
	[CL_CHK_099_04] [varchar](255) NULL,
	[CL_CHK_100_04] [varchar](255) NULL,
	[CL_CHK_128_04] [varchar](255) NULL,
	[CL_CHK_129_04] [varchar](255) NULL,
	[CL_CHK_130_04] [varchar](255) NULL,
	[CL_CHK_131_04] [varchar](255) NULL,
	[CL_CHK_140_04] [varchar](255) NULL,
	[CL_CHK_219_04] [varchar](255) NULL,
	[CL_CHK_220_04] [varchar](255) NULL,
	[CL_CHK_222_04] [varchar](255) NULL,
	[CL_CHK_223_04] [varchar](255) NULL,
	[CL_CHK_102_04] [varchar](255) NULL,
	[CL_AGTNAME_04] [varchar](255) NULL,
	[CL_OSAGT_04] [varchar](255) NULL,
	[CL_OSAGTREFN_04] [varchar](255) NULL,
	[CL_OSAGTAPPD_04] [varchar](255) NULL,
	[CL_CHK_106_04] [varchar](255) NULL,
	[CL_CHK_109_04] [varchar](255) NULL,
	[CL_CHK_111_04] [varchar](255) NULL,
	[CL_RECMER_04] [varchar](255) NULL,
	[CL_CHK_115_04] [varchar](255) NULL,
	[CL_CHK_116_04] [varchar](255) NULL,
	[CL_TPINSURID_04] [varchar](255) NULL,
	[CL_CHK_INSCO_04] [varchar](255) NULL,
	[CL_CHK_123_04] [varchar](255) NULL,
	[CL_CHK_124_04] [varchar](255) NULL,
	[CL_CHK_125_04] [varchar](255) NULL,
	[CL_CHK_126_04] [varchar](255) NULL,
	[CL_CHK_127_04] [varchar](255) NULL,
	[CL_CHK_141_04] [varchar](255) NULL,
	[CL_CHK_145_04] [varchar](255) NULL,
	[CL_CHK_147_04] [varchar](255) NULL,
	[CL_CHK_162_04] [varchar](255) NULL,
	[CL_CHK_200_05] [varchar](255) NULL,
	[CL_RECCLA_05] [varchar](255) NULL,
	[CL_CHK_201_05] [varchar](255) NULL,
	[CL_CHK_094_05] [varchar](255) NULL,
	[CL_CHK_202_05] [varchar](255) NULL,
	[CL_CHK_095_05] [varchar](255) NULL,
	[CL_CHK_203_05] [varchar](255) NULL,
	[CL_CHK_098_05] [varchar](255) NULL,
	[CL_CHK_099_05] [varchar](255) NULL,
	[CL_CHK_100_05] [varchar](255) NULL,
	[CL_CHK_128_05] [varchar](255) NULL,
	[CL_CHK_129_05] [varchar](255) NULL,
	[CL_CHK_130_05] [varchar](255) NULL,
	[CL_CHK_131_05] [varchar](255) NULL,
	[CL_CHK_140_05] [varchar](255) NULL,
	[CL_CHK_219_05] [varchar](255) NULL,
	[CL_CHK_220_05] [varchar](255) NULL,
	[CL_CHK_222_05] [varchar](255) NULL,
	[CL_CHK_223_05] [varchar](255) NULL,
	[CL_CHK_102_05] [varchar](255) NULL,
	[CL_AGTNAME_05] [varchar](255) NULL,
	[CL_OSAGT_05] [varchar](255) NULL,
	[CL_OSAGTREFN_05] [varchar](255) NULL,
	[CL_OSAGTAPPD_05] [varchar](255) NULL,
	[CL_CHK_106_05] [varchar](255) NULL,
	[CL_CHK_109_05] [varchar](255) NULL,
	[CL_CHK_111_05] [varchar](255) NULL,
	[CL_RECMER_05] [varchar](255) NULL,
	[CL_CHK_115_05] [varchar](255) NULL,
	[CL_CHK_116_05] [varchar](255) NULL,
	[CL_TPINSURID_05] [varchar](255) NULL,
	[CL_CHK_INSCO_05] [varchar](255) NULL,
	[CL_CHK_123_05] [varchar](255) NULL,
	[CL_CHK_124_05] [varchar](255) NULL,
	[CL_CHK_125_05] [varchar](255) NULL,
	[CL_CHK_126_05] [varchar](255) NULL,
	[CL_CHK_127_05] [varchar](255) NULL,
	[CL_CHK_141_05] [varchar](255) NULL,
	[CL_CHK_145_05] [varchar](255) NULL,
	[CL_CHK_147_05] [varchar](255) NULL,
	[CL_CHK_162_05] [varchar](255) NULL,
	[CL_LEGALINV] [varchar](255) NULL,
	[CL_CHK_211] [varchar](255) NULL,
	[CL_CHK_212] [varchar](255) NULL,
	[CL_CHK_213] [varchar](255) NULL,
	[CL_CHK_214] [varchar](255) NULL,
	[CL_AGTNAME_ID] [varchar](50) NULL,
	[CL_AGTNAME_01_ID] [varchar](50) NULL,
	[CL_AGTNAME_02_ID] [varchar](50) NULL,
	[CL_AGTNAME_03_ID] [varchar](50) NULL,
	[CL_AGTNAME_04_ID] [varchar](50) NULL,
	[CL_AGTNAME_05_ID] [varchar](50) NULL,
	[CL_BACKOFFINEG_ID] [varchar](50) NULL,
	[CL_CHK_000_ID] [varchar](50) NULL,
	[CL_CHK_010_ID] [varchar](50) NULL,
	[CL_CHK_011_ID] [varchar](50) NULL,
	[CL_CHK_012_ID] [varchar](50) NULL,
	[CL_CHK_013_ID] [varchar](50) NULL,
	[CL_CHK_017_ID] [varchar](50) NULL,
	[CL_CHK_023_ID] [varchar](50) NULL,
	[CL_CHK_024_ID] [varchar](50) NULL,
	[CL_CHK_027_ID] [varchar](50) NULL,
	[CL_CHK_030_ID] [varchar](50) NULL,
	[CL_CHK_031_ID] [varchar](50) NULL,
	[CL_CHK_033_ID] [varchar](50) NULL,
	[CL_CHK_034_ID] [varchar](50) NULL,
	[CL_CHK_0342_ID] [varchar](50) NULL,
	[CL_CHK_0343_ID] [varchar](50) NULL,
	[CL_CHK_0344_ID] [varchar](50) NULL,
	[CL_CHK_0345_ID] [varchar](50) NULL,
	[CL_CHK_039_ID] [varchar](50) NULL,
	[CL_CHK_052_ID] [varchar](50) NULL,
	[CL_CHK_092_ID] [varchar](50) NULL,
	[CL_CHK_094_ID] [varchar](50) NULL,
	[CL_CHK_094_01_ID] [varchar](50) NULL,
	[CL_CHK_094_02_ID] [varchar](50) NULL,
	[CL_CHK_094_03_ID] [varchar](50) NULL,
	[CL_CHK_094_04_ID] [varchar](50) NULL,
	[CL_CHK_094_05_ID] [varchar](50) NULL,
	[CL_CHK_095_ID] [varchar](50) NULL,
	[CL_CHK_095_01_ID] [varchar](50) NULL,
	[CL_CHK_095_02_ID] [varchar](50) NULL,
	[CL_CHK_095_03_ID] [varchar](50) NULL,
	[CL_CHK_095_04_ID] [varchar](50) NULL,
	[CL_CHK_095_05_ID] [varchar](50) NULL,
	[CL_CHK_102_ID] [varchar](50) NULL,
	[CL_CHK_102_01_ID] [varchar](50) NULL,
	[CL_CHK_102_02_ID] [varchar](50) NULL,
	[CL_CHK_102_03_ID] [varchar](50) NULL,
	[CL_CHK_102_04_ID] [varchar](50) NULL,
	[CL_CHK_102_05_ID] [varchar](50) NULL,
	[CL_CHK_106_ID] [varchar](50) NULL,
	[CL_CHK_106_01_ID] [varchar](50) NULL,
	[CL_CHK_106_02_ID] [varchar](50) NULL,
	[CL_CHK_106_03_ID] [varchar](50) NULL,
	[CL_CHK_106_04_ID] [varchar](50) NULL,
	[CL_CHK_106_05_ID] [varchar](50) NULL,
	[CL_CHK_109_ID] [varchar](50) NULL,
	[CL_CHK_109_01_ID] [varchar](50) NULL,
	[CL_CHK_109_02_ID] [varchar](50) NULL,
	[CL_CHK_109_03_ID] [varchar](50) NULL,
	[CL_CHK_109_04_ID] [varchar](50) NULL,
	[CL_CHK_109_05_ID] [varchar](50) NULL,
	[CL_CHK_111_ID] [varchar](50) NULL,
	[CL_CHK_111_01_ID] [varchar](50) NULL,
	[CL_CHK_111_02_ID] [varchar](50) NULL,
	[CL_CHK_111_03_ID] [varchar](50) NULL,
	[CL_CHK_111_04_ID] [varchar](50) NULL,
	[CL_CHK_111_05_ID] [varchar](50) NULL,
	[CL_CHK_116_ID] [varchar](50) NULL,
	[CL_CHK_116_01_ID] [varchar](50) NULL,
	[CL_CHK_116_02_ID] [varchar](50) NULL,
	[CL_CHK_116_03_ID] [varchar](50) NULL,
	[CL_CHK_116_04_ID] [varchar](50) NULL,
	[CL_CHK_116_05_ID] [varchar](50) NULL,
	[CL_CHK_131_ID] [varchar](50) NULL,
	[CL_CHK_131_01_ID] [varchar](50) NULL,
	[CL_CHK_131_02_ID] [varchar](50) NULL,
	[CL_CHK_131_03_ID] [varchar](50) NULL,
	[CL_CHK_131_04_ID] [varchar](50) NULL,
	[CL_CHK_131_05_ID] [varchar](50) NULL,
	[CL_CHK_141_ID] [varchar](50) NULL,
	[CL_CHK_141_01_ID] [varchar](50) NULL,
	[CL_CHK_141_02_ID] [varchar](50) NULL,
	[CL_CHK_141_03_ID] [varchar](50) NULL,
	[CL_CHK_141_04_ID] [varchar](50) NULL,
	[CL_CHK_141_05_ID] [varchar](50) NULL,
	[CL_CHK_147_ID] [varchar](50) NULL,
	[CL_CHK_147_01_ID] [varchar](50) NULL,
	[CL_CHK_147_02_ID] [varchar](50) NULL,
	[CL_CHK_147_03_ID] [varchar](50) NULL,
	[CL_CHK_147_04_ID] [varchar](50) NULL,
	[CL_CHK_147_05_ID] [varchar](50) NULL,
	[CL_CHK_162_ID] [varchar](50) NULL,
	[CL_CHK_162_01_ID] [varchar](50) NULL,
	[CL_CHK_162_02_ID] [varchar](50) NULL,
	[CL_CHK_162_03_ID] [varchar](50) NULL,
	[CL_CHK_162_04_ID] [varchar](50) NULL,
	[CL_CHK_162_05_ID] [varchar](50) NULL,
	[CL_CHK_163_ID] [varchar](50) NULL,
	[CL_CHK_200_ID] [varchar](50) NULL,
	[CL_CHK_200_01_ID] [varchar](50) NULL,
	[CL_CHK_200_02_ID] [varchar](50) NULL,
	[CL_CHK_200_03_ID] [varchar](50) NULL,
	[CL_CHK_200_04_ID] [varchar](50) NULL,
	[CL_CHK_200_05_ID] [varchar](50) NULL,
	[CL_CHK_201_ID] [varchar](50) NULL,
	[CL_CHK_201_01_ID] [varchar](50) NULL,
	[CL_CHK_201_02_ID] [varchar](50) NULL,
	[CL_CHK_201_03_ID] [varchar](50) NULL,
	[CL_CHK_201_04_ID] [varchar](50) NULL,
	[CL_CHK_201_05_ID] [varchar](50) NULL,
	[CL_CHK_211_ID] [varchar](50) NULL,
	[CL_CHK_212_ID] [varchar](50) NULL,
	[CL_CHK_213_ID] [varchar](50) NULL,
	[CL_CHK_214_ID] [varchar](50) NULL,
	[CL_CHK_215_ID] [varchar](50) NULL,
	[CL_CHK_219_ID] [varchar](50) NULL,
	[CL_CHK_219_01_ID] [varchar](50) NULL,
	[CL_CHK_219_02_ID] [varchar](50) NULL,
	[CL_CHK_219_03_ID] [varchar](50) NULL,
	[CL_CHK_219_04_ID] [varchar](50) NULL,
	[CL_CHK_219_05_ID] [varchar](50) NULL,
	[CL_CHK_223_ID] [varchar](50) NULL,
	[CL_CHK_223_01_ID] [varchar](50) NULL,
	[CL_CHK_223_02_ID] [varchar](50) NULL,
	[CL_CHK_223_03_ID] [varchar](50) NULL,
	[CL_CHK_223_04_ID] [varchar](50) NULL,
	[CL_CHK_223_05_ID] [varchar](50) NULL,
	[CL_CHK_224_ID] [varchar](50) NULL,
	[CL_CHK_225_ID] [varchar](50) NULL,
	[CL_CHK_228_ID] [varchar](50) NULL,
	[CL_CHK_229_ID] [varchar](50) NULL,
	[CL_CHK_232_ID] [varchar](50) NULL,
	[CL_CHK_235_ID] [varchar](50) NULL,
	[CL_CHK_237_ID] [varchar](50) NULL,
	[CL_CHK_238_ID] [varchar](50) NULL,
	[CL_CHK_240_ID] [varchar](50) NULL,
	[CL_CHK_241_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_01_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_02_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_03_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_04_ID] [varchar](50) NULL,
	[CL_CHK_INSCO_05_ID] [varchar](50) NULL,
	[CL_CHK_INSCONME_ID] [varchar](50) NULL,
	[CL_INVAPP_ID] [varchar](50) NULL,
	[CL_INVNAME_ID] [varchar](50) NULL,
	[CL_INVOICED_ID] [varchar](50) NULL,
	[CL_LEGALINV_ID] [varchar](50) NULL,
	[CL_OSAGT_ID] [varchar](50) NULL,
	[CL_OSAGT_01_ID] [varchar](50) NULL,
	[CL_OSAGT_02_ID] [varchar](50) NULL,
	[CL_OSAGT_03_ID] [varchar](50) NULL,
	[CL_OSAGT_04_ID] [varchar](50) NULL,
	[CL_OSAGT_05_ID] [varchar](50) NULL,
	[CL_RECCLA_ID] [varchar](50) NULL,
	[CL_RECCLA_01_ID] [varchar](50) NULL,
	[CL_RECCLA_02_ID] [varchar](50) NULL,
	[CL_RECCLA_03_ID] [varchar](50) NULL,
	[CL_RECCLA_04_ID] [varchar](50) NULL,
	[CL_RECCLA_05_ID] [varchar](50) NULL,
	[CL_RECMER_ID] [varchar](50) NULL,
	[CL_RECMER_01_ID] [varchar](50) NULL,
	[CL_RECMER_02_ID] [varchar](50) NULL,
	[CL_RECMER_03_ID] [varchar](50) NULL,
	[CL_RECMER_04_ID] [varchar](50) NULL,
	[CL_RECMER_05_ID] [varchar](50) NULL,
	[CL_REPUDI_ID] [varchar](50) NULL,
	[CL_SALAGREE_ID] [varchar](50) NULL,
	[CL_THICLA_ID] [varchar](50) NULL,
	[CL_TPINSURID_ID] [varchar](50) NULL,
	[CL_TPINSURID_01_ID] [varchar](50) NULL,
	[CL_TPINSURID_02_ID] [varchar](50) NULL,
	[CL_TPINSURID_03_ID] [varchar](50) NULL,
	[CL_TPINSURID_04_ID] [varchar](50) NULL,
	[CL_TPINSURID_05_ID] [varchar](50) NULL,
	[CL_VEHDRI_ID] [varchar](50) NULL,
	[CL_WRTOFF_ID] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[IntegrationMasterFiles]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[IntegrationMasterFiles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[iCustomerID] [int] NOT NULL,
	[sFileName] [varchar](100) NOT NULL,
	[sFileDirection] [varchar](20) NULL,
	[sFileLocation] [varchar](250) NULL,
	[sFileProcessedLocation] [varchar](250) NULL,
	[sFileArchiveLocation] [varchar](250) NULL,
	[sPackageFolderName] [varchar](100) NULL,
	[sPackageProjectName] [varchar](100) NULL,
	[sPackageName] [varchar](100) NULL,
	[iStatusID] [int] NOT NULL,
	[sCreatedBy] [varchar](50) NOT NULL,
	[dCreated] [date] NOT NULL,
 CONSTRAINT [PK_IntegrationMasterFiles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[st_RiskDetails_Normal]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[st_RiskDetails_Normal](
	[intPolicyID] [float] NULL,
	[iRiskID] [float] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[ZIPCODE] [varchar](255) NULL,
	[R_MOTORSEATS] [varchar](255) NULL,
	[R_MOTORYOM] [varchar](255) NULL,
	[R_MOTORREG] [varchar](255) NULL,
	[R_ENGIN_NO] [varchar](255) NULL,
	[R_CHASSISNO] [varchar](255) NULL,
	[R_COVERDETAILS] [varchar](255) NULL,
	[R_G_MARKETVAL] [varchar](255) NULL,
	[R_G_PASLIABIND] [varchar](255) NULL,
	[R_G_NUOFSEATS] [varchar](255) NULL,
	[R_G_HPLEASE] [varchar](255) NULL,
	[R_G_EXTENDLOU] [varchar](255) NULL,
	[R_G_PERSACCIND] [varchar](255) NULL,
	[R_G_CREDSFALL] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[R_G_ABSVIOCRE] [varchar](255) NULL,
	[R_G_ISAFE] [varchar](255) NULL,
	[R_G_EXCESSRO] [varchar](255) NULL,
	[R_G_WINDSCREEN] [varchar](255) NULL,
	[R_G_BACK2INV] [varchar](255) NULL,
	[R_G_CASHBACK] [varchar](255) NULL,
	[R_G_DEALER] [varchar](255) NULL,
	[R_G_INTRSTNOTED] [varchar](255) NULL,
	[R_G_CREDITPRO] [varchar](255) NULL,
	[R_G_CRACCNO] [varchar](255) NULL,
	[R_G_ASSOC] [varchar](255) NULL,
	[R_G_STDENDORSE] [varchar](255) NULL,
	[R_G_ADDFAP2000] [varchar](255) NULL,
	[R_G_ADDFAP2500] [varchar](255) NULL,
	[R_G_ADDFAP5000] [varchar](255) NULL,
	[R_G_ADDFAP7500] [varchar](255) NULL,
	[R_G_ADDFAP10000] [varchar](255) NULL,
	[R_G_ADDFAP15000] [varchar](255) NULL,
	[R_G_ADDFAP20000] [varchar](255) NULL,
	[R_G_AMATEKSI] [varchar](255) NULL,
	[R_G_AMAACCNO] [varchar](255) NULL,
	[R_G_AMAIDNO] [varchar](255) NULL,
	[R_MATCHINGCT] [varchar](255) NULL,
	[R_CTPERACC] [varchar](255) NULL,
	[R_CTHPLEASE] [varchar](255) NULL,
	[R_CTPASS] [varchar](255) NULL,
	[R_CTEXTENDLOU] [varchar](255) NULL,
	[R_CTHELIVAC] [varchar](255) NULL,
	[R_CTAMACLA] [varchar](255) NULL,
	[R_CTBACK2INV] [varchar](255) NULL,
	[R_CTISAFE] [varchar](255) NULL,
	[R_CTCASHBACK] [varchar](255) NULL,
	[R_CTCREDITSF] [varchar](255) NULL,
	[R_CTEXCESSRO] [varchar](255) NULL,
	[R_TSASRIA] [varchar](255) NULL,
	[R_MATCHINGPERC] [varchar](255) NULL,
	[R_MORE5VEH] [varchar](255) NULL,
	[R_SI] [varchar](255) NULL,
	[R_G_PASSENSI] [varchar](255) NULL,
	[R_G_PASSENDT] [varchar](255) NULL,
	[R_G_HPLEASESI] [varchar](255) NULL,
	[R_G_PERACCSI] [varchar](255) NULL,
	[R_G_EXTENDLOUSI] [varchar](255) NULL,
	[R_G_HELIVACSI] [varchar](255) NULL,
	[R_G_CSFSI] [varchar](255) NULL,
	[R_G_ABSVIOCRESI] [varchar](255) NULL,
	[R_G_ISAFESI] [varchar](255) NULL,
	[R_G_EXCESSROSI] [varchar](255) NULL,
	[R_G_WINDSCRSI] [varchar](255) NULL,
	[R_G_BACK2INVSI] [varchar](255) NULL,
	[R_G_CASHBACKSI] [varchar](255) NULL,
	[R_G_AMAPREMSI] [varchar](255) NULL,
	[R_G_VEHDEF] [varchar](255) NULL,
	[R_G_TRACKCUST] [varchar](255) NULL,
	[R_G_TVEHDEF] [varchar](255) NULL,
	[R_G_TRAILERLOI] [varchar](255) NULL,
	[R_G_LOANPRTPR] [varchar](255) NULL,
	[R_G_DRVDEATHSI] [varchar](255) NULL,
	[R_MATCHING] [varchar](255) NULL,
	[R_G_PASDEATHSI] [varchar](255) NULL,
	[R_G_TRACKREFNO] [varchar](255) NULL,
	[R_Driver] [varchar](255) NULL,
	[R_DriveName1] [varchar](255) NULL,
	[R_DriveID1] [varchar](255) NULL,
	[R_DriveCell1] [varchar](255) NULL,
	[R_DriveName2] [varchar](255) NULL,
	[R_DriveID2] [varchar](255) NULL,
	[R_DriveCell2] [varchar](255) NULL,
	[R_DriveName3] [varchar](255) NULL,
	[R_DriveID3] [varchar](255) NULL,
	[R_DriveCell3] [varchar](255) NULL,
	[R_DriveName4] [varchar](255) NULL,
	[R_DriveID4] [varchar](255) NULL,
	[R_DriveCell4] [varchar](255) NULL,
	[R_DriveName5] [varchar](255) NULL,
	[R_DriveID5] [varchar](255) NULL,
	[R_DriveCell5] [varchar](255) NULL,
	[R_DriveName6] [varchar](255) NULL,
	[R_DriveID6] [varchar](255) NULL,
	[R_DriveCell6] [varchar](255) NULL,
	[R_G_ADDFAPG1000] [varchar](255) NULL,
	[R_G_ADDFAPG2000] [varchar](255) NULL,
	[R_G_ADDFAPG3000] [varchar](255) NULL,
	[R_G_TPOSI] [varchar](255) NULL,
	[R_G_HPLEASEDT] [varchar](255) NULL,
	[R_G_EXTENDLOUDT] [varchar](255) NULL,
	[R_G_LOSSINOPSI] [varchar](255) NULL,
	[R_G_HELIVACDT] [varchar](255) NULL,
	[R_G_ISAFEDT] [varchar](255) NULL,
	[R_G_PERACCDT] [varchar](255) NULL,
	[R_G_CSFDT] [varchar](255) NULL,
	[R_G_ABSVIOCREDT] [varchar](255) NULL,
	[R_G_EXCESSRODT] [varchar](255) NULL,
	[R_G_WINDSCRDT] [varchar](255) NULL,
	[R_G_BACK2INVDT] [varchar](255) NULL,
	[R_G_CASHBACKDT] [varchar](255) NULL,
	[R_ISFCSHBCK_CPC] [varchar](255) NULL,
	[R_LOI_TOUR_CPC] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[IntegrationMasterScripts]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[IntegrationMasterScripts](
	[iScriptID] [int] IDENTITY(1,1) NOT NULL,
	[iIntegrationMasterFileID] [int] NOT NULL,
	[sScriptName] [varchar](50) NOT NULL,
	[sScriptDesc] [varchar](100) NULL,
	[iFileFormatID] [int] NOT NULL,
	[sSQLSript] [text] NULL,
	[sColumnNames] [varchar](250) NULL,
	[sColumnSizes] [varchar](250) NULL,
	[sDelimeter] [varchar](5) NULL,
	[bHasHeaderRow] [varchar](5) NULL,
	[sHeaderRowSQLScript] [varchar](250) NULL,
	[bHasFooterRow] [varchar](5) NULL,
	[sFooterRowSQLScript] [varchar](250) NULL,
	[sCreatedBy] [varchar](50) NOT NULL,
	[dCreated] [date] NOT NULL,
	[ExportSequenceNumber] [int] NULL,
 CONSTRAINT [PK_CL_IntegrationMasterScripts] PRIMARY KEY CLUSTERED 
(
	[iScriptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[st_CustomerDetails_Normal]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[st_CustomerDetails_Normal](
	[iCustomerID] [bigint] NULL,
	[dLastUpdated] [datetime] NULL,
	[iTranID] [bigint] NULL,
	[C_CELLNO] [varchar](255) NULL,
	[C_CONTDETAILS] [varchar](255) NULL,
	[C_DOB] [varchar](255) NULL,
	[C_EMAIL] [varchar](255) NULL,
	[C_FAXTEL] [varchar](255) NULL,
	[C_GENDER] [varchar](255) NULL,
	[C_HOMETEL] [varchar](255) NULL,
	[C_IDNO] [varchar](255) NULL,
	[C_TITLE] [varchar](255) NULL,
	[C_VIPCLIENT] [varchar](255) NULL,
	[C_WORKTEL] [varchar](255) NULL,
	[CH_INITIALS] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[st_PolicyDetails_Normal]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[st_PolicyDetails_Normal](
	[iPolicyID] [float] NOT NULL,
	[dDateModified] [datetime] NULL,
	[iTranID] [int] NULL,
	[P_ServiceProv] [varchar](255) NULL,
	[P_MARKETCODE] [varchar](255) NULL,
	[P_CASENO] [varchar](255) NULL,
	[P_MORE5VEH] [varchar](255) NULL,
	[P_BANKPROTIND] [varchar](255) NULL,
	[P_BANKPROTPNO] [varchar](255) NULL,
	[P_PREMMATREAS] [varchar](255) NULL,
	[P_ADDINFO] [varchar](255) NULL,
	[P_B_REFACCH] [varchar](255) NULL,
	[P_B_REFACCN] [varchar](255) NULL,
	[P_B_REFBRNC] [varchar](255) NULL,
	[P_CWarning] [varchar](255) NULL,
	[P_LASTRD_DOREF] [varchar](255) NULL,
	[P_LASTRD_HEAD] [varchar](255) NULL,
	[P_LASTRD_REASON] [varchar](255) NULL,
	[P_ITCCOLOUR] [varchar](255) NULL,
	[P_QUALQUEST] [varchar](255) NULL,
	[P_UW] [varchar](255) NULL,
	[P_LASTRD_AMT] [varchar](255) NULL,
	[P_EXISTDAMAMT] [varchar](255) NULL,
	[P_BROINITFEE] [varchar](255) NULL,
	[P_BROKERBANK] [varchar](255) NULL,
	[P_B_REFUND] [varchar](255) NULL,
	[P_OR_BROKERFEE] [varchar](255) NULL,
	[P_OR_LEGALADV] [varchar](255) NULL,
	[P_VIKELA] [varchar](255) NULL,
	[R_G_HELIVAC] [varchar](255) NULL,
	[P_LASTRD_DATE] [varchar](255) NULL,
	[P_POLTYPE] [varchar](255) NULL,
	[P_LICRESTRIC] [varchar](255) NULL,
	[P_MORE2CLAIMS] [varchar](255) NULL,
	[P_CASHBACKBONUS] [varchar](255) NULL,
	[P_B_REFACCT] [varchar](255) NULL,
	[P_B_REFBANK] [varchar](255) NULL,
	[P_EXISTDAM] [varchar](255) NULL,
	[P_G_SOURCE] [varchar](255) NULL,
	[P_INSCANC] [varchar](255) NULL,
	[P_INSDECL] [varchar](255) NULL,
	[P_INSREFUSED] [varchar](255) NULL,
	[P_ISAFECASHBACK] [varchar](255) NULL,
	[P_G_ADDNOTES] [varchar](255) NULL,
	[P_G_CONFNOTES] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[st_ClaimDetails_Normal]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[st_ClaimDetails_Normal](
	[iClaimID] [float] NOT NULL,
	[dLastUpdated] [datetime] NULL,
	[CL_THICLA] [varchar](255) NULL,
	[CL_PBEATER] [varchar](255) NULL,
	[CL_BACKOFFINEG] [varchar](255) NULL,
	[CL_CHK_243] [varchar](255) NULL,
	[CL_CHK_000] [varchar](255) NULL,
	[CL_CHK_002] [varchar](255) NULL,
	[CL_CHK_003] [varchar](255) NULL,
	[CL_REPUDI] [varchar](255) NULL,
	[CL_REPUDE] [varchar](255) NULL,
	[CL_VEHDRI] [varchar](255) NULL,
	[CL_DATOIN] [varchar](255) NULL,
	[CL_DATOID] [varchar](255) NULL,
	[CL_CHK_010] [varchar](255) NULL,
	[CL_APPDATE] [varchar](255) NULL,
	[CL_CHK_011] [varchar](255) NULL,
	[CL_CHK_012] [varchar](255) NULL,
	[CL_CHK_013] [varchar](255) NULL,
	[CL_CHK_014] [varchar](255) NULL,
	[CL_CHK_015] [varchar](255) NULL,
	[CL_CHK_016] [varchar](255) NULL,
	[CL_CHK_240] [varchar](255) NULL,
	[CL_CHK_215] [varchar](255) NULL,
	[CL_CHK_216] [varchar](255) NULL,
	[CL_CHK_241] [varchar](255) NULL,
	[CL_CHK_017] [varchar](255) NULL,
	[CL_CHK_023] [varchar](255) NULL,
	[CL_CHK_024] [varchar](255) NULL,
	[CL_CHK_025] [varchar](255) NULL,
	[CL_CHK_027] [varchar](255) NULL,
	[CL_CHK_163] [varchar](255) NULL,
	[CL_CHK_028] [varchar](255) NULL,
	[CL_CHK_026] [varchar](255) NULL,
	[CL_CHK_029] [varchar](255) NULL,
	[CL_CHK_052] [varchar](255) NULL,
	[CL_CHK_092] [varchar](255) NULL,
	[CL_RECCLA] [varchar](255) NULL,
	[CL_CHK_094] [varchar](255) NULL,
	[CL_CHK_095] [varchar](255) NULL,
	[CL_CHK_098] [varchar](255) NULL,
	[CL_CHK_099] [varchar](255) NULL,
	[CL_CHK_100] [varchar](255) NULL,
	[CL_CHK_128] [varchar](255) NULL,
	[CL_CHK_129] [varchar](255) NULL,
	[CL_CHK_130] [varchar](255) NULL,
	[CL_CHK_131] [varchar](255) NULL,
	[CL_CHK_140] [varchar](255) NULL,
	[CL_CHK_200] [varchar](255) NULL,
	[CL_CHK_201] [varchar](255) NULL,
	[CL_CHK_202] [varchar](255) NULL,
	[CL_CHK_203] [varchar](255) NULL,
	[CL_CHK_219] [varchar](255) NULL,
	[CL_CHK_220] [varchar](255) NULL,
	[CL_CHK_222] [varchar](255) NULL,
	[CL_CHK_223] [varchar](255) NULL,
	[CL_INVAPP] [varchar](255) NULL,
	[CL_INVNAME] [varchar](255) NULL,
	[CL_CHK_237] [varchar](255) NULL,
	[CL_CHK_238] [varchar](255) NULL,
	[CL_CHK_239] [varchar](255) NULL,
	[CL_CHK_030] [varchar](255) NULL,
	[CL_CHK_031] [varchar](255) NULL,
	[CL_CHK_032] [varchar](255) NULL,
	[CL_CHK_224] [varchar](255) NULL,
	[CL_CHK_225] [varchar](255) NULL,
	[CL_CHK_033] [varchar](255) NULL,
	[CL_CHK_034] [varchar](255) NULL,
	[CL_CHK_0342] [varchar](255) NULL,
	[CL_CHK_0343] [varchar](255) NULL,
	[CL_CHK_0344] [varchar](255) NULL,
	[CL_CHK_0345] [varchar](255) NULL,
	[CL_WRTOFF] [varchar](255) NULL,
	[CL_WOCONFDTE] [varchar](255) NULL,
	[CL_STKNMBR] [varchar](255) NULL,
	[CL_SALAGREE] [varchar](255) NULL,
	[CL_SALUPLDATE] [varchar](255) NULL,
	[CL_INVOICED] [varchar](255) NULL,
	[CL_INVNO] [varchar](255) NULL,
	[CL_CHK_228] [varchar](255) NULL,
	[CL_CHK_229] [varchar](255) NULL,
	[CL_CHK_230] [varchar](255) NULL,
	[CL_CHK_247] [varchar](255) NULL,
	[CL_CHK_232] [varchar](255) NULL,
	[CL_CHK_233] [varchar](255) NULL,
	[CL_CHK_248] [varchar](255) NULL,
	[CL_CHK_235] [varchar](255) NULL,
	[CL_CHK_039] [varchar](255) NULL,
	[CL_CHK_102] [varchar](255) NULL,
	[CL_AGTNAME] [varchar](255) NULL,
	[CL_OSAGT] [varchar](255) NULL,
	[CL_OSAGTREFNO] [varchar](255) NULL,
	[CL_OSAGTAPPDATE] [varchar](255) NULL,
	[CL_CHK_106] [varchar](255) NULL,
	[CL_CHK_109] [varchar](255) NULL,
	[CL_CHK_111] [varchar](255) NULL,
	[CL_RECMER] [varchar](255) NULL,
	[CL_CHK_115] [varchar](255) NULL,
	[CL_CHK_116] [varchar](255) NULL,
	[CL_TPINSURID] [varchar](255) NULL,
	[CL_CHK_INSCONME] [varchar](255) NULL,
	[CL_CHK_123] [varchar](255) NULL,
	[CL_CHK_124] [varchar](255) NULL,
	[CL_CHK_125] [varchar](255) NULL,
	[CL_CHK_126] [varchar](255) NULL,
	[CL_CHK_127] [varchar](255) NULL,
	[CL_CHK_141] [varchar](255) NULL,
	[CL_CHK_145] [varchar](255) NULL,
	[CL_CHK_147] [varchar](255) NULL,
	[CL_CHK_162] [varchar](255) NULL,
	[CL_CHK_200_01] [varchar](255) NULL,
	[CL_RECCLA_01] [varchar](255) NULL,
	[CL_CHK_201_01] [varchar](255) NULL,
	[CL_CHK_094_01] [varchar](255) NULL,
	[CL_CHK_202_01] [varchar](255) NULL,
	[CL_CHK_095_01] [varchar](255) NULL,
	[CL_CHK_203_01] [varchar](255) NULL,
	[CL_CHK_098_01] [varchar](255) NULL,
	[CL_CHK_099_01] [varchar](255) NULL,
	[CL_CHK_100_01] [varchar](255) NULL,
	[CL_CHK_128_01] [varchar](255) NULL,
	[CL_CHK_129_01] [varchar](255) NULL,
	[CL_CHK_130_01] [varchar](255) NULL,
	[CL_CHK_131_01] [varchar](255) NULL,
	[CL_CHK_140_01] [varchar](255) NULL,
	[CL_CHK_219_01] [varchar](255) NULL,
	[CL_CHK_220_01] [varchar](255) NULL,
	[CL_CHK_222_01] [varchar](255) NULL,
	[CL_CHK_223_01] [varchar](255) NULL,
	[CL_CHK_102_01] [varchar](255) NULL,
	[CL_AGTNAME_01] [varchar](255) NULL,
	[CL_OSAGT_01] [varchar](255) NULL,
	[CL_OSAGTREFN_01] [varchar](255) NULL,
	[CL_OSAGTAPPD_01] [varchar](255) NULL,
	[CL_CHK_106_01] [varchar](255) NULL,
	[CL_CHK_109_01] [varchar](255) NULL,
	[CL_CHK_111_01] [varchar](255) NULL,
	[CL_RECMER_01] [varchar](255) NULL,
	[CL_CHK_115_01] [varchar](255) NULL,
	[CL_CHK_116_01] [varchar](255) NULL,
	[CL_TPINSURID_01] [varchar](255) NULL,
	[CL_CHK_INSCO_01] [varchar](255) NULL,
	[CL_CHK_123_01] [varchar](255) NULL,
	[CL_CHK_124_01] [varchar](255) NULL,
	[CL_CHK_125_01] [varchar](255) NULL,
	[CL_CHK_126_01] [varchar](255) NULL,
	[CL_CHK_127_01] [varchar](255) NULL,
	[CL_CHK_141_01] [varchar](255) NULL,
	[CL_CHK_145_01] [varchar](255) NULL,
	[CL_CHK_147_01] [varchar](255) NULL,
	[CL_CHK_162_01] [varchar](255) NULL,
	[CL_CHK_200_02] [varchar](255) NULL,
	[CL_RECCLA_02] [varchar](255) NULL,
	[CL_CHK_201_02] [varchar](255) NULL,
	[CL_CHK_094_02] [varchar](255) NULL,
	[CL_CHK_202_02] [varchar](255) NULL,
	[CL_CHK_095_02] [varchar](255) NULL,
	[CL_CHK_203_02] [varchar](255) NULL,
	[CL_CHK_098_02] [varchar](255) NULL,
	[CL_CHK_099_02] [varchar](255) NULL,
	[CL_CHK_100_02] [varchar](255) NULL,
	[CL_CHK_128_02] [varchar](255) NULL,
	[CL_CHK_129_02] [varchar](255) NULL,
	[CL_CHK_130_02] [varchar](255) NULL,
	[CL_CHK_131_02] [varchar](255) NULL,
	[CL_CHK_140_02] [varchar](255) NULL,
	[CL_CHK_219_02] [varchar](255) NULL,
	[CL_CHK_220_02] [varchar](255) NULL,
	[CL_CHK_222_02] [varchar](255) NULL,
	[CL_CHK_223_02] [varchar](255) NULL,
	[CL_CHK_102_02] [varchar](255) NULL,
	[CL_AGTNAME_02] [varchar](255) NULL,
	[CL_OSAGT_02] [varchar](255) NULL,
	[CL_OSAGTREFN_02] [varchar](255) NULL,
	[CL_OSAGTAPPD_02] [varchar](255) NULL,
	[CL_CHK_106_02] [varchar](255) NULL,
	[CL_CHK_109_02] [varchar](255) NULL,
	[CL_CHK_111_02] [varchar](255) NULL,
	[CL_RECMER_02] [varchar](255) NULL,
	[CL_CHK_115_02] [varchar](255) NULL,
	[CL_CHK_116_02] [varchar](255) NULL,
	[CL_TPINSURID_02] [varchar](255) NULL,
	[CL_CHK_INSCO_02] [varchar](255) NULL,
	[CL_CHK_123_02] [varchar](255) NULL,
	[CL_CHK_124_02] [varchar](255) NULL,
	[CL_CHK_125_02] [varchar](255) NULL,
	[CL_CHK_126_02] [varchar](255) NULL,
	[CL_CHK_127_02] [varchar](255) NULL,
	[CL_CHK_141_02] [varchar](255) NULL,
	[CL_CHK_145_02] [varchar](255) NULL,
	[CL_CHK_147_02] [varchar](255) NULL,
	[CL_CHK_162_02] [varchar](255) NULL,
	[CL_CHK_200_03] [varchar](255) NULL,
	[CL_RECCLA_03] [varchar](255) NULL,
	[CL_CHK_201_03] [varchar](255) NULL,
	[CL_CHK_094_03] [varchar](255) NULL,
	[CL_CHK_202_03] [varchar](255) NULL,
	[CL_CHK_095_03] [varchar](255) NULL,
	[CL_CHK_203_03] [varchar](255) NULL,
	[CL_CHK_098_03] [varchar](255) NULL,
	[CL_CHK_099_03] [varchar](255) NULL,
	[CL_CHK_100_03] [varchar](255) NULL,
	[CL_CHK_128_03] [varchar](255) NULL,
	[CL_CHK_129_03] [varchar](255) NULL,
	[CL_CHK_130_03] [varchar](255) NULL,
	[CL_CHK_131_03] [varchar](255) NULL,
	[CL_CHK_140_03] [varchar](255) NULL,
	[CL_CHK_219_03] [varchar](255) NULL,
	[CL_CHK_220_03] [varchar](255) NULL,
	[CL_CHK_222_03] [varchar](255) NULL,
	[CL_CHK_223_03] [varchar](255) NULL,
	[CL_CHK_102_03] [varchar](255) NULL,
	[CL_AGTNAME_03] [varchar](255) NULL,
	[CL_OSAGT_03] [varchar](255) NULL,
	[CL_OSAGTREFN_03] [varchar](255) NULL,
	[CL_OSAGTAPPD_03] [varchar](255) NULL,
	[CL_CHK_106_03] [varchar](255) NULL,
	[CL_CHK_109_03] [varchar](255) NULL,
	[CL_CHK_111_03] [varchar](255) NULL,
	[CL_RECMER_03] [varchar](255) NULL,
	[CL_CHK_115_03] [varchar](255) NULL,
	[CL_CHK_116_03] [varchar](255) NULL,
	[CL_TPINSURID_03] [varchar](255) NULL,
	[CL_CHK_INSCO_03] [varchar](255) NULL,
	[CL_CHK_123_03] [varchar](255) NULL,
	[CL_CHK_124_03] [varchar](255) NULL,
	[CL_CHK_125_03] [varchar](255) NULL,
	[CL_CHK_126_03] [varchar](255) NULL,
	[CL_CHK_127_03] [varchar](255) NULL,
	[CL_CHK_141_03] [varchar](255) NULL,
	[CL_CHK_145_03] [varchar](255) NULL,
	[CL_CHK_147_03] [varchar](255) NULL,
	[CL_CHK_162_03] [varchar](255) NULL,
	[CL_CHK_200_04] [varchar](255) NULL,
	[CL_RECCLA_04] [varchar](255) NULL,
	[CL_CHK_201_04] [varchar](255) NULL,
	[CL_CHK_094_04] [varchar](255) NULL,
	[CL_CHK_202_04] [varchar](255) NULL,
	[CL_CHK_095_04] [varchar](255) NULL,
	[CL_CHK_203_04] [varchar](255) NULL,
	[CL_CHK_098_04] [varchar](255) NULL,
	[CL_CHK_099_04] [varchar](255) NULL,
	[CL_CHK_100_04] [varchar](255) NULL,
	[CL_CHK_128_04] [varchar](255) NULL,
	[CL_CHK_129_04] [varchar](255) NULL,
	[CL_CHK_130_04] [varchar](255) NULL,
	[CL_CHK_131_04] [varchar](255) NULL,
	[CL_CHK_140_04] [varchar](255) NULL,
	[CL_CHK_219_04] [varchar](255) NULL,
	[CL_CHK_220_04] [varchar](255) NULL,
	[CL_CHK_222_04] [varchar](255) NULL,
	[CL_CHK_223_04] [varchar](255) NULL,
	[CL_CHK_102_04] [varchar](255) NULL,
	[CL_AGTNAME_04] [varchar](255) NULL,
	[CL_OSAGT_04] [varchar](255) NULL,
	[CL_OSAGTREFN_04] [varchar](255) NULL,
	[CL_OSAGTAPPD_04] [varchar](255) NULL,
	[CL_CHK_106_04] [varchar](255) NULL,
	[CL_CHK_109_04] [varchar](255) NULL,
	[CL_CHK_111_04] [varchar](255) NULL,
	[CL_RECMER_04] [varchar](255) NULL,
	[CL_CHK_115_04] [varchar](255) NULL,
	[CL_CHK_116_04] [varchar](255) NULL,
	[CL_TPINSURID_04] [varchar](255) NULL,
	[CL_CHK_INSCO_04] [varchar](255) NULL,
	[CL_CHK_123_04] [varchar](255) NULL,
	[CL_CHK_124_04] [varchar](255) NULL,
	[CL_CHK_125_04] [varchar](255) NULL,
	[CL_CHK_126_04] [varchar](255) NULL,
	[CL_CHK_127_04] [varchar](255) NULL,
	[CL_CHK_141_04] [varchar](255) NULL,
	[CL_CHK_145_04] [varchar](255) NULL,
	[CL_CHK_147_04] [varchar](255) NULL,
	[CL_CHK_162_04] [varchar](255) NULL,
	[CL_CHK_200_05] [varchar](255) NULL,
	[CL_RECCLA_05] [varchar](255) NULL,
	[CL_CHK_201_05] [varchar](255) NULL,
	[CL_CHK_094_05] [varchar](255) NULL,
	[CL_CHK_202_05] [varchar](255) NULL,
	[CL_CHK_095_05] [varchar](255) NULL,
	[CL_CHK_203_05] [varchar](255) NULL,
	[CL_CHK_098_05] [varchar](255) NULL,
	[CL_CHK_099_05] [varchar](255) NULL,
	[CL_CHK_100_05] [varchar](255) NULL,
	[CL_CHK_128_05] [varchar](255) NULL,
	[CL_CHK_129_05] [varchar](255) NULL,
	[CL_CHK_130_05] [varchar](255) NULL,
	[CL_CHK_131_05] [varchar](255) NULL,
	[CL_CHK_140_05] [varchar](255) NULL,
	[CL_CHK_219_05] [varchar](255) NULL,
	[CL_CHK_220_05] [varchar](255) NULL,
	[CL_CHK_222_05] [varchar](255) NULL,
	[CL_CHK_223_05] [varchar](255) NULL,
	[CL_CHK_102_05] [varchar](255) NULL,
	[CL_AGTNAME_05] [varchar](255) NULL,
	[CL_OSAGT_05] [varchar](255) NULL,
	[CL_OSAGTREFN_05] [varchar](255) NULL,
	[CL_OSAGTAPPD_05] [varchar](255) NULL,
	[CL_CHK_106_05] [varchar](255) NULL,
	[CL_CHK_109_05] [varchar](255) NULL,
	[CL_CHK_111_05] [varchar](255) NULL,
	[CL_RECMER_05] [varchar](255) NULL,
	[CL_CHK_115_05] [varchar](255) NULL,
	[CL_CHK_116_05] [varchar](255) NULL,
	[CL_TPINSURID_05] [varchar](255) NULL,
	[CL_CHK_INSCO_05] [varchar](255) NULL,
	[CL_CHK_123_05] [varchar](255) NULL,
	[CL_CHK_124_05] [varchar](255) NULL,
	[CL_CHK_125_05] [varchar](255) NULL,
	[CL_CHK_126_05] [varchar](255) NULL,
	[CL_CHK_127_05] [varchar](255) NULL,
	[CL_CHK_141_05] [varchar](255) NULL,
	[CL_CHK_145_05] [varchar](255) NULL,
	[CL_CHK_147_05] [varchar](255) NULL,
	[CL_CHK_162_05] [varchar](255) NULL,
	[CL_LEGALINV] [varchar](255) NULL,
	[CL_CHK_211] [varchar](255) NULL,
	[CL_CHK_212] [varchar](255) NULL,
	[CL_CHK_213] [varchar](255) NULL,
	[CL_CHK_214] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_PolTranSummary]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[EDW_PolTranSummary](
	[PolicyID] [bigint] NULL,
	[Premium Bal] [money] NULL,
	[Commissoin Bal] [money] NULL,
	[SASRIA Bal] [money] NULL,
	[Admin Fee Bal] [money] NULL,
	[Policy Fee Bal] [money] NULL,
	[Broker Fee Bal] [money] NULL,
	[Summary Date] [datetime] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_Risks]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_Risks](
	[iPolicyID] [float] NOT NULL,
	[intRiskID] [float] NOT NULL,
	[iTranID] [float] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_PolicyTransactions]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_PolicyTransactions](
	[iPolicyID] [float] NOT NULL,
	[iTransactionID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_ClaimTransactions]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_ClaimTransactions](
	[iClaimID] [float] NULL,
	[iTransactionID] [float] NOT NULL,
	[iClaimantID] [float] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_Claims]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_Claims](
	[iClientID] [float] NULL,
	[iClaimID] [float] NOT NULL,
	[iPolicyID] [float] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_Policy]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_Policy](
	[iCustomerID] [int] NULL,
	[iPolicyID] [int] NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[Delta_Customer]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[Delta_Customer](
	[iCustomerID] [int] NULL,
	[iTranID] [int] NULL
) ON [PRIMARY]

GO

/****** Object:  Table [SKI_INT].[EDW_RunStatus]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_RunStatus](
	[TableName] [varchar](30) NULL,
	[NoOfRows] [int] NULL,
	[LastRunDate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [SKI_INT].[EDW_FieldCodes]    Script Date: 16/01/2018 02:29:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [SKI_INT].[EDW_FieldCodes](
	[sFieldCode] [varchar](15) NULL,
	[sFieldType] [varchar](255) NULL,
	[iFieldType] [int] NULL,
	[sFieldDescription] [varchar](255) NULL,
	[iSeq] [int] NULL,
	[sFieldUsage] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


