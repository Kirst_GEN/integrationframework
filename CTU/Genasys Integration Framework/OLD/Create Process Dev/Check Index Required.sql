CREATE NONCLUSTERED INDEX [NCI_EDW_RiskDetails_sFieldCode]
ON [dbo].[RiskDetails] ([sFieldCode])
INCLUDE ([iRiskID],[iFieldType],[sFieldValue],[cFieldValue],[bFieldValue],[dtFieldValue],[iFieldValue],[bReplicated],[iTranID],[sGUID])
GO

CREATE NONCLUSTERED INDEX [NCI_EDW_RiskDetails_iRiskID_iTranID]
ON [dbo].[RiskDetails] ([iRiskID],[iTranID])
INCLUDE ([sFieldCode],[iFieldType],[sFieldValue],[cFieldValue],[bFieldValue],[dtFieldValue],[iFieldValue],[bReplicated],[sGUID])
GO

CREATE NONCLUSTERED INDEX [NCI_EDW_Arch_RiskDetails_iRiskID_iTranID]
ON [dbo].[Arch_RiskDetails] ([iRIskID],[iTranID])
INCLUDE ([sFieldCode],[iFieldType],[sFieldValue],[cFieldValue],[bFieldValue],[dtFieldValue],[iFieldValue],[bReplicated],[sGUID])
GO
