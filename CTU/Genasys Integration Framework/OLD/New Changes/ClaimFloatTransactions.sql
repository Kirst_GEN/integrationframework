select * from cl.ClaimFloat
select * from cl.ClaimFloatTransaction WHERE ManualPayment = 0
select * from cl.ClaimFloatTransactionPayment
SELECT * FROM ClaimPaymentHistory ORDER BY dUpdated DESC


SELECT 
  CFT.TranType
  ,CFT.FloatType
  ,CFT.Amount
  ,CFT.Reference
  ,CFT.RequestedBy
  ,CFT.TransactionDate
  ,CFT.UserLastUpdated
  ,CFT.DateLastUpdated
  ,CFT.ExternalTransactionID
  ,CFT.ManualPayment
  ,CFTP.PayTransactionID
  ,CFTP.AutoAuth
  ,CFTP.AuthStatus
  ,CFTP.RequestApprBy
  ,CFTP.RequestApprDate
  ,CFTP.PaymentAuthBy
  ,CFTP.PaymentAuthDate
  ,CFTP.Paid
  ,CFTP.PaidBy
  ,CFTP.PaidDate
  ,CFTP.UserLastUpdated
  ,CFTP.DateLastUpdated
FROM
  CL.ClaimFloatTransaction CFT
  LEFT JOIN CL.ClaimFloatTransactionPayment CFTP
    ON CFTP.FloatTranID = CFT.ID