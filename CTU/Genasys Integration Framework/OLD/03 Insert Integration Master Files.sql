--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENRTY FOR A NEW EXPORT FILE TYPE
--DATE			:	2017-02-13	
--AUTHOR		:	KIRSTEN HARRIS
--CLIENT		:	AALL
--==============================================================================================================

--==================================================================================================================
--ADD RECORD FOR FILENAME TO BE EXECUTED IN MASTER FILE TABLE
--==================================================================================================================
Declare @CustomerID INT,
		@sFileName VARCHAR(100),
		@sFileDirection VARCHAR(20),
		@sFileLocation VARCHAR(255),
		@sFileProcessedLocation VARCHAR(255),
		@sFileArchiveLocation VARCHAR(255),
		@sPackageFolderName VARCHAR(100),
		@sPackageProjectName VARCHAR(100),
		@sPackageName VARCHAR(50),
		@iStatusID INT,
		@sCreatedBy VARCHAR(50),
		@dCreated DateTime

--==============================================================================================================
--ASSIGN VALUES TO PARAMETERS
--==============================================================================================================
SET @CustomerID = 1								--AON
SET @sFileName = 'Genasys_GWP_Data'				
SET @sFileDirection = 'EXPORT'
SET @sFileLocation = '\\Kirsten-Laptop\Ski\GWP_DATA\Pickup\'
SET @sFileProcessedLocation = ''
SET @sFileArchiveLocation = '\\kirsten-Laptop\Ski\GWP_DATA\Archive\'
SET @sPackageFolderName = 'GenasysGWP'
SET @sPackageProjectName = 'SkiXtract'
SET @sPackageName = 'FlatFiles'
SET @iStatusID = 1
SET @sCreatedBy = 'Kirsten Harris'
SET @dCreated = GETDATE()

--==============================================================================================================
--CREATE ENTRY
--==============================================================================================================
INSERT INTO SKI_INT.IntegrationMasterFiles
SELECT 
	@CustomerID, 
	@sFileName,
	@sFileDirection,
	@sFileLocation,
	@sFileProcessedLocation,
	@sFileArchiveLocation,
	@sPackageFolderName,
	@sPackageProjectName,
	@sPackageName,
	@iStatusID,
	@sCreatedBy,
	@dCreated 
WHERE NOT EXISTS(
	SELECT * FROM 
		SKI_INT.IntegrationMasterFiles SS 
	WHERE 
		SS.sFileName = @sFileName
	)