TRUNCATE TABLE [ski_int].[SSISFileType]
GO
SET IDENTITY_INSERT [ski_int].[SSISFileType] ON 
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'.dat', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'.csv', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'.txt', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISFileType] OFF

DELETE FROM [ski_int].[SSISBatchUser] WHERE USERNAME = 'SSISINTEGRATION'
GO
INSERT [ski_int].[SSISBatchUser] ([uID], [UserName], [Password]) VALUES (NULL, N'SSISINTEGRATION', 0x010000007EC0A02033FF482964AB3815051CA7BC88B5DE3038589334BBAE3FA24F26988F)

TRUNCATE TABLE [ski_int].[SSISCommunicationType]
GO
SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] ON 
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'ProcessFailure', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'ProcessErrors', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Progress', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Exceptions', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] OFF

TRUNCATE TABLE [ski_int].[SSISProcessType]
GO
SET IDENTITY_INSERT [ski_int].[SSISProcessType] ON 
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'Import', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'Export', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Update', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Raising', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'Receipting', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'DocumentMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ClaimMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (8, N'UnderwritingMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (9, N'CancelPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (10, N'ReinstatePolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (11, N'SuspendPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (12, N'CashImport', NULL, 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISProcessType] OFF

TRUNCATE TABLE [ski_int].[SSISScriptType]
GO
SET IDENTITY_INSERT [ski_int].[SSISScriptType] ON 
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'SQLCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.900' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'SKiImporterTable', 1, N'Integration', CAST(N'2017-09-28 15:30:25.907' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'ASQueryName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.910' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'FlashImporterTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.917' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'SKiImporterSPPName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.923' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'ExceptionsCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.927' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ExceptionsTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.930' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (8, N'SQLStagingCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.930' AS DateTime))
SET IDENTITY_INSERT [ski_int].[SSISScriptType] OFF

--===========================================================================================================================================================================================
--END RUN ONCE ONLY WHEN IMPLEMENTED
--===========================================================================================================================================================================================

