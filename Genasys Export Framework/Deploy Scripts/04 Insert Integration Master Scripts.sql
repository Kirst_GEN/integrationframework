--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENRTY FOR A NEW EXPORT FILE TYPE
--DATE			:	2017-02-13	
--AUTHOR		:	KIRSTEN HARRIS
--CLIENT		:	AALL
--==============================================================================================================

--==============================================================================================================
--DECLARATION
--==============================================================================================================

--select * from [ski_int].[IntegrationMasterScripts] ORDER BY 1 DESC

Declare 
	@iScriptID INT,
	@iIntegrationMasterFileID INT,
	@sScriptName VARCHAR(50),
	@sScriptDesc VARCHAR(100),
	@iFileFormatID INT,
	@sColumnNames VARCHAR(250),
	@sColumnSizes VARCHAR(250),
	@sDelimeter VARCHAR(5),
	@bHasHeaderRow VARCHAR(5),
	@sHeaderRowSQLScript VARCHAR(250),
	@bHasFooterRow VARCHAR(5),
	@sFooterRowSQLScript VARCHAR(250),
	@sCreatedBy VARCHAR(30),
	@dCreated DATE,
	@ExportSequenceNumber INT

--==============================================================================================================
--ASSIGN VALUES TO PARAMETERS
--==============================================================================================================
SET @iScriptID = ISNULL((SELECT MAX(iScriptID) FROM SKI_INT.IntegrationMasterScripts),0)+1
SET @iIntegrationMasterFileID = (SELECT ID FROM SKI_INT.IntegrationMasterFiles WHERE sPackageFolderName = 'GenasysGWP')
SET @sScriptName = 'ExportClientGWP'
SET @sScriptDesc = 'Export Client GWP Data'
SET @iFileFormatID = 1
SET @sColumnNames = ''
SET @sColumnSizes = ''
SET @sDelimeter = '|'
SET @bHasHeaderRow = 'N'
SET @sHeaderRowSQLScript = ''
SET @bHasFooterRow = 'N'
SET @sFooterRowSQLScript = ''
SET @sCreatedBy = 'Kirsten Harris'
SET @dCreated = GETDATE()
SET @ExportSequenceNumber = '1'

--==============================================================================================================
--CREATE ENTRY
--==============================================================================================================
INSERT INTO SKI_INT.IntegrationMasterScripts
SELECT 
	@iIntegrationMasterFileID, 
	@sScriptName, 
	@sScriptDesc, 
	@iFileFormatID, 
	'EXEC [SKI_INT].[EXPORT_GWP]', 
	@sColumnNames, 
	@sColumnSizes, 
	@sDelimeter, 
	@bHasHeaderRow, 
	@sHeaderRowSQLScript,
	@bHasFooterRow, 
	@sFooterRowSQLScript, 
	@sCreatedBy, 
	@dCreated,
	@ExportSequenceNumber
WHERE NOT EXISTS(
	SELECT * FROM 
		SKI_INT.IntegrationMasterScripts SS 
	WHERE 
		SS.iIntegrationMasterFileID = @iIntegrationMasterFileID
	)

