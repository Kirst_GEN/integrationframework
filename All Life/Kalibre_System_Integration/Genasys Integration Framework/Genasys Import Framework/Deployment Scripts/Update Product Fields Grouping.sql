--SELECT * FROM ProductRiskTypes WHERE iProductID = 251

SELECT 
	iProductID,sFieldCode,sDescription,iSeq,sCustomGroup,
	FieldType,  bReadOnly, bRequired, bOutputIfEmpty
FROM (
	SELECT iProductID AS iProductID, fc.sFieldCode,fc.sDescription,iSeq,sCustomGroup,
		 L.sDescription AS FieldType,  bReadOnly, bRequired, bOutputIfEmpty
	FROM ProductFieldRel rfr
		INNER JOIN FieldCodes fc on fc.sFieldCode = rfr.sFieldCode
		INNER JOIN Lookup L 
			ON L.iIndex = Fc.iFieldType AND sGroup = 'FieldTypes'
	WHERE iProductID = 251
) a
WHERE sDescription like '%height%'
order by 3

select * from FieldCodes where sfieldcode like '%P_PREFDEBITDAY%' order by 1
select * from Lookup where sGroup = 'CollectionDays'

/*
UPDATE ProductFieldRel SET sCustomGroup = 'Medical Questions'
WHERE iSeq between 51 and 100

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details'
WHERE iSeq between 105 AND 135

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 1'
WHERE iSeq between 106 AND 108

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 2'
WHERE iSeq between 109 AND 111

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 3'
WHERE iSeq between 112 AND 114

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 4'
WHERE iSeq between 115 AND 117

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 5'
WHERE iSeq between 118 AND 120

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 6'
WHERE iSeq between 121 AND 123

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 7'
WHERE iSeq between 124 AND 126

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 8'
WHERE iSeq between 127 AND 129

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 9'
WHERE iSeq between 130 AND 132

UPDATE ProductFieldRel SET sCustomGroup = 'Beneficiary Details \ Beneficiary 10'
WHERE iSeq between 133 AND 135

UPDATE ProductFieldRel SET sCustomGroup = 'General'
WHERE iSeq IN (1,2,3,4,5,6,7,8,31,136,137,138,79)

UPDATE ProductFieldRel SET sCustomGroup = 'Cession Details'
WHERE iSeq IN (9,10,11,12,13,14,15,16,17,18,19,20)

UPDATE ProductFieldRel SET sCustomGroup = 'Life Insured Details'
WHERE iSeq IN (21,22,23,24,25,26,27,28,29,30,34,46,35)

UPDATE ProductFieldRel SET sCustomGroup = 'Payer Details'
WHERE iSeq IN (36,37,38,39,40,41,42,43,44,45)


UPDATE ProductFieldRel SET sCustomGroup = 'Medical Details'
WHERE iSeq IN (47,48,49,50,101,102,103,104,141,80,79)

UPDATE ProductFieldRel SET sCustomGroup = 'Spouse Details'
WHERE iSeq IN (142,143,144,145)
*/