/*===========================================================================================================================================================================================
--RUN ONCE ONLY WHEN IMPLEMENTED
CREATE TABLE [ski_int].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
GO
===========================================================================================================================================================================================*/

SET IDENTITY_INSERT [ski_int].[SSISFileType] ON 
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'.dat', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'.csv', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISFileType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'.txt', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISFileType] OFF

INSERT [ski_int].[BatchUser] ([uID], [UserName], [Password]) VALUES (NULL, N'MonthEnd', 0x01000000A922098B7E7385AC2707DC05817E51C45BC837CE02A9D9EEBF6AD664EB6B7C71)
INSERT [ski_int].[SSISBatchUser] ([uID], [UserName], [Password]) VALUES (NULL, N'MonthEnd', 0x010000007EC0A02033FF482964AB3815051CA7BC88B5DE3038589334BBAE3FA24F26988F)

SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] ON 
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'ProcessFailure', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'ProcessErrors', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Progress', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISCommunicationType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Exceptions', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISCommunicationType] OFF

--truncate table [ski_int].[SSISFileLocations]
SET IDENTITY_INSERT [ski_int].[SSISProcessType] ON 
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'Import', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'Export', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'Update', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'Raising', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'Receipting', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'DocumentMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ClaimMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (8, N'UnderwritingMigration', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (9, N'CancelPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (10, N'ReinstatePolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (11, N'SuspendPolicy', NULL, 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISProcessType] ([Id], [Description], [ScriptId], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (12, N'CashImport', NULL, 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISProcessType] OFF

SET IDENTITY_INSERT [ski_int].[SSISScriptType] ON 
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (1, N'SQLCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.900' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (2, N'SKiImporterTable', 1, N'Integration', CAST(N'2017-09-28 15:30:25.907' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (3, N'ASQueryName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.910' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (4, N'FlashImporterTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.917' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (5, N'SKiImporterSPPName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.923' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (6, N'ExceptionsCommand', 1, N'Integration', CAST(N'2017-09-28 15:30:25.927' AS DateTime))
INSERT [ski_int].[SSISScriptType] ([Id], [Description], [Enabled], [UserLastUpdated], [DateLastUpdated]) VALUES (7, N'ExceptionsTableName', 1, N'Integration', CAST(N'2017-09-28 15:30:25.930' AS DateTime))
SET IDENTITY_INSERT [ski_int].[SSISScriptType] OFF

--===========================================================================================================================================================================================
--END RUN ONCE ONLY WHEN IMPLEMENTED
--===========================================================================================================================================================================================


--===========================================================================================================================================================================================
--START CONFIG
--===========================================================================================================================================================================================
--truncate table [ski_int].[SSISProcess]
SET IDENTITY_INSERT [ski_int].[SSISProcess] ON 
INSERT [ski_int].[SSISProcess] ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
	VALUES (1, N'1', N'Import Kalibre Policies', 1, 1, 0, 1, N'SKI_Automation', N'GenasysIntegrationFramework', N'NewBusinessKalibreImport', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISProcess] OFF

--truncate table [ski_int].[SSISScript]

SET IDENTITY_INSERT [ski_int].[SSISScript] ON 
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (1, 1, 1, N'[SKI_INT].[SSIS_Normalize_Kalibre_Import]', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISScript] OFF

--truncate table [ski_int].[SSISFileLocations]
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] ON 
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] OFF

--===========================================================================================================================================================================================
--END CONFIG
--===========================================================================================================================================================================================

--===========================================================================================================================================================================================
--CHECK CONFIG
--===========================================================================================================================================================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Kalibre Policies',''
