CREATE FUNCTION [ski_int].[fn_GetFileNameFromPath](@FilePath varchar(255)) 
RETURNS VARCHAR(255)
AS
BEGIN
   RETURN 
		(
			SELECT
				RIGHT(@FilePath, CHARINDEX('\', REVERSE(@FilePath)) -1)  [file_name] 
				--, LEFT(@FilePath,LEN(@FilePath) - charindex('\',reverse(@FilePath),1) + 1) [path]
				  
		)
END
GO

CREATE FUNCTION [ski_int].[fn_GetFilePath](@FilePath varchar(255)) 
RETURNS VARCHAR(255)
AS
BEGIN
   RETURN 
		(
			SELECT
				LEFT(@FilePath,LEN(@FilePath) - charindex('\',reverse(@FilePath),1) + 1) [path]
				  
		)
END

