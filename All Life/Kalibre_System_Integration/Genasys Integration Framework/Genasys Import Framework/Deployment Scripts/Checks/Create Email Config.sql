ALTER PROCEDURE [ski_int].[SSIS_AddEmailConfig]
	
    @MailServer VARCHAR(8000),
    @MailPort INT = 0,
    @MailUserName VARCHAR(100) = NULL,
    @MailPassword VARCHAR(8000) 
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISEmailConfig];

	INSERT INTO [ski_int].[SSISEmailConfig]
	(
		MailServer,
		MailPort,
		MailUserName,
		MailPassword
	)
	VALUES
	(
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailServer)),
		@MailPort,
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailUserName)),
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailPassword))
	);
		

END
GO

truncate table [ski_int].[SSISEmailConfig]
EXEC [ski_int].[SSIS_AddEmailConfig] 'Smtp.gmail.com',465,'KirstenHarris.KLH@Gmail.com','Ethyn@123'
EXEC [ski_int].[SSIS_AddEmailConfig] 'Smtp.gmail.com',587,'KirstenHarris.KLH@Gmail.com','Ethyn@123'

SELECT 
	CAST(DECRYPTBYPASSPHRASE('**********',MailServer) AS VARCHAR(8000)),
	MailPort,
	CAST(DECRYPTBYPASSPHRASE('**********',MailUserName) AS VARCHAR(8000)),
	CAST(DECRYPTBYPASSPHRASE('**********',MailPassword) AS VARCHAR(8000))
FROM 
	[ski_int].[SSISEmailConfig]
WHERE 
	MailPort <> 25
	