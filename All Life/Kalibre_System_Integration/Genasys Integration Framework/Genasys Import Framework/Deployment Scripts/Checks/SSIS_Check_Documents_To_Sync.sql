--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS A COUNT OF DOCUMENTS TO WHICH HAVE NOT BEEN SYNCED UP YET
--DATE			:	2018-04-01	
--AUTHOR		:	KIRSTEN HARRIS
--==============================================================================================================
ALTER PROCEDURE [SKI_INT].[SSIS_Check_Documents_To_Sync]

AS

BEGIN

	SELECT 
		  COUNT(*) AS NoOfDocuments
	INTO
		#TEMPCOUNT
	FROM 
		dbo.Policy P (NOLOCK)
		JOIN PolicyDetails PD (NOLOCK)
			ON PD.iPolicyID = P.iPolicyID
			AND PD.sFieldCode = 'P_KALIBREID'
		JOIN Customer C (NOLOCK)
			ON C.iCustomerID = P.iCustomerID
		JOIN KalibreSA_UAT.dbo.IncomingXMLstore x (NOLOCK) 
			ON x.[policy-reference] = PD.sFieldValue
	WHERE
		NOT EXISTS	(
					SELECT * FROM DocMetaData DM (NOLOCK)
					WHERE DM.iCustomerID = C.iCustomerID
					AND DM.sFileName = [ski_int].[fn_GetFileNameFromPath] (x.KeyingSummary_PDF)
					AND iDocType = 1
					)
		;
SELECT * FROM #TEMPCOUNT;
END 








