EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily Kalibre','Kalibre_Daily'


INSERT INTO 
	[ski_int].[SSIS_Src_RawData_Log] 
SELECT 
	* 
FROM 
	[ski_int].[SSIS_Src_RawData];


SELECT * FROM [ski_int].[SSIS_Src_RawData];
truncate table [ski_int].[SSIS_Src_RawData];

select * from ski_int.SSISFileLocations
update ski_int.SSISFileLocations
set 
	PickupLocation = replace(PickupLocation,'\\ALLLIfe_JHB_Dev\SKi\IntegrationFramework\Kalibre','\\alllife_jhb\Ski\Kalibre'),
	ArchiveLocation = replace(ArchiveLocation,'\\ALLLIfe_JHB_Dev\SKi\IntegrationFramework\Kalibre','\\alllife_jhb\Ski\Kalibre'),
	LogFileLocation = replace(LogFileLocation,'\\ALLLIfe_JHB_Dev\SKi\IntegrationFramework\Kalibre','\\alllife_jhb\Ski\Kalibre'),
	ProcessedLocation = replace(ProcessedLocation,'\\ALLLIfe_JHB_Dev\SKi\IntegrationFramework\Kalibre','\\alllife_jhb\Ski\Kalibre')

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [Id]
      ,[XMLData]
      ,[Importdate]
      ,[FileName]
      ,[Outcome]
      ,[policy-reference]
  FROM [AllLife].[dbo].[IncomingXMLstore]

INSERT INTO [dbo].[IncomingXMLstore](XMLData, Importdate, FileName, OutCome, [policy-reference])
SELECT CONVERT(XML, BulkColumn) AS BulkColumn, GETDATE(),'Kalibre_Daily2.xml','','' 
FROM OPENROWSET(BULK 'c:\Ski\Kalibre_Daily2.xml', SINGLE_BLOB) AS x;


SELECT * FROM IncomingXMLstore
EXEC [ski_int].[Int_Kalibre_Import]
EXEC [dbo].[usp_Insert_DailyStaging_ETL_XML]


SELECT * FROM IncomingXMLstore
select * from [ski_int].[DailyStaging_ETL_XML]

select * from CoInsurers

select * from products order by iproductid desc

alter table [ski_int].[DailyStaging_ETL_XML]
add [iID] [bigint] IDENTITY(1,1) NOT NULL

alter table [ski_int].[DailyStaging_ETL_XML]
add [iImportRecNo] [bigint] NULL

alter table [ski_int].[DailyStaging_ETL_XML]
add [dtProcessDate] [datetime] NULL

select * from cfg.Country

select * from customer
order by iCustomerID desc

update ski_int.DailyStaging_ETL_XML
set iImportRecNo = -1

exec sp_executesql N'SELECT c.iCustomerID FROM Customer c WITH (NOLOCK) 
INNER JOIN CustomerDetails d0 WITH (NOLOCK) ON (d0.iCustomerID=c.iCustomerID) AND (d0.sFieldCode=@P1) 
WHERE (d0.sFieldValue=LTrim(RTrim(@P2)))',N'@P1 varchar(6),@P2 nvarchar(6)','C_IDNO',N'AD4545'