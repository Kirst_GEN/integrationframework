
/****** Object:  StoredProcedure [SKI_INT].[BatchUserLogin]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[BatchUserLogin]
    @pLoginName NVARCHAR(254),
    @pPassword NVARCHAR(50),
    @responseMessage NVARCHAR(250)='' OUTPUT
AS
BEGIN

    SET NOCOUNT ON

    DECLARE @userID INT

    IF EXISTS (SELECT TOP 1 uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName)
    BEGIN
        SET @userID=(SELECT uID FROM [ski_int].[BatchUser] WHERE UserName = @pLoginName AND Password = HASHBYTES('SHA2_512', @pPassword))

       IF(@userID IS NULL)
           SET @responseMessage='Incorrect password'
       ELSE 
           SET @responseMessage='User successfully logged in'
    END
    ELSE
       SET @responseMessage='Invalid login'

	   select @responseMessage

END



GO

/****** Object:  StoredProcedure [SKI_INT].[Execute_SSIS_Packages]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [SKI_INT].[Execute_SSIS_Packages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;







GO

/****** Object:  StoredProcedure [SKI_INT].[PostUpdate]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create Proc [SKI_INT].[PostUpdate]
AS
SELECT NULL
GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_AddEmailConfig]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_AddEmailConfig]
	
    @MailServer VARCHAR(8000),
    @MailPort INT = 0,
    @MailUserName VARCHAR(100) = NULL,
    @MailPassword VARCHAR(8000) 
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISEmailConfig];

	INSERT INTO [ski_int].[SSISEmailConfig]
	(
		MailServer,
		MailPort,
		MailUserName,
		MailPassword
	)
	VALUES
	(
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailServer)),
		@MailPort,
		@MailUserName,
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailPassword))
	);
		

END

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_AddSSISUser]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_AddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISBatchUser];

	INSERT INTO [ski_int].[SSISBatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_CanContinue]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONTINUE NEXT STEP INDICATOR FOR THE PROCESS
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_CanContinue]
(
		@ProcessName VARCHAR(100)
		,@PackageName VARCHAR(100)
)
AS

BEGIN

	--==============================================================================================================
	--GET PACKAGE ID
	--==============================================================================================================
	DECLARE @PackageLogID INT;
	
	SELECT TOP 1 
		@PackageLogID = PackageLogID
	FROM
		[ski_int].[SSISPackageLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
	ORDER BY
		EndTime DESC;
		
	--==============================================================================================================
	--GET CONTINUE NEXT STEP INDICATOR
	--==============================================================================================================
	SELECT TOP 1 
		ContinueNextStep
	FROM
		[ski_int].[SSISProgressLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
		AND ProgressLogID = @PackageLogID
	ORDER BY
		EndTime DESC;

END 

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_DataErrorLog]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_DataErrorLog]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorSource NVARCHAR(255)
	, @SourceRecord NVARCHAR(255)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISDataErrorLog(
		PackageLogID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorSource
		,SourceRecord
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorSource
	, @SourceRecord
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndExtractLog]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndExtractLog]
  @ExtractLogID INT
, @ExtractCount INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastExtractDateTime DATETIME, @SQL NVARCHAR(255)
	SELECT @SQL = N'SELECT @LastExtractDateTime = ISNULL(MAX(ModifiedDate), ''1900-01-01'') FROM ski_int.imp_' + TableName FROM ski_int.SSISExtractLog WHERE ExtractLogID = @ExtractLogID
	EXEC sp_executeSQL @SQL, N'@LastExtractDateTime DATETIME OUTPUT', @LastExtractDateTime OUTPUT
	
	UPDATE ski_int.SSISExtractLog
	SET
	  EndTime = GetDate()
	, ExtractCount = @ExtractCount
	, LastExtractDateTime = @LastExtractDateTime
	, Success = 1
	WHERE ExtractLogID = @ExtractLogID

	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndJob]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES THE [ski_int].[SSIS_EndJob] TALE ON COMPLETION OF THE JOB
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [SKI_INT].[SSIS_EndJob]
	@SSISJobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN

	--==============================================================================================================
	--UPDATE JOB
	--==============================================================================================================
	UPDATE 
		[ski_int].[SSISJob] 
	SET
		EndTime = GetDate()
		,JobReference = @JobReference 
		,Success = @Success
	WHERE 
		SSISJobID = @SSISJobID;
	

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndPackageLog]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndPackageLog]
	@JobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN


	UPDATE 
		ski_int.SSISPackageLog 
	SET
		EndTime = GetDate()
		,Success = @Success
		,JobReference = @JobReference
	WHERE 
		JobID = @JobID;

	UPDATE 
		ski_int.SSISProgressLog 
	SET
		EndTime = GetDate()
		,Success = @Success
	WHERE 
		JobID = @JobID;
	

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndProgressLog]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndProgressLog]
  @PackageLogID INT
  ,@Success INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ski_int.SSISProgressLog SET
	  EndTime = GetDate()
	, Success = @Success
	WHERE ProgresslogID = @PackageLogID

	SET NOCOUNT OFF;
END

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ErrorLog]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_ErrorLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100) 
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISErrorLog(
		JobID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ExecutePackages]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [SKI_INT].[SSIS_ExecutePackages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;







GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetAllDataForSKiImporter]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetAllDataForSKiImporter] 
(
	@TableName SYSNAME
)
AS

BEGIN

	--=====================================================
	--GET ALL DATA WHERE IMPORTRECNO IS 999999
	--=====================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'SELECT * FROM ' + N'' + @TableName + ' WHERE   iImportRecNo = 999999';

	EXEC(@SQLCommand);
	
END 

	

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetDataForSKiImporter]    Script Date: 03/04/2018 05:59:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT GET ALL RECORDS FROM IMPORT RECORD TABLE TO BE IMPORTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetDataForSKiImporter] 

AS

BEGIN

	--=====================================================
	--GET ALL RECORDS
	--=====================================================
	SELECT 
		* 
	FROM 
		dbo.ImportRecord IR 
	WHERE
		IR.eProcStatus = 8
	ORDER BY 
		IR.iImportRecNo
	
END 





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetJobSchedulerResults]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_GetJobSchedulerResults]

AS

BEGIN

	--==============================================================================================================
	--SELECT
	--==============================================================================================================
	SELECT 
	[sJOB].[name] AS [JobName]
    , CASE 
        WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
        ELSE CAST(
                CAST([sJOBH].[run_date] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [LastRunDateTime]
    , CASE [sJOBH].[run_status]
        WHEN 0 THEN 'Failed'
        WHEN 1 THEN 'Succeeded'
        WHEN 2 THEN 'Retry'
        WHEN 3 THEN 'Canceled'
        WHEN 4 THEN 'Running' -- In Progress
      END AS [LastRunStatus]
    , STUFF(
            STUFF(RIGHT('000000' + CAST([sJOBH].[run_duration] AS VARCHAR(6)),  6)
                , 3, 0, ':')
            , 6, 0, ':') 
        AS [LastRunDuration (HH:MM:SS)]
    , [sJOBH].[message] AS [LastRunStatusMessage]
    , CASE [sJOBSCH].[NextRunDate]
        WHEN 0 THEN NULL
        ELSE CAST(
                CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [NextRunDateTime]
	FROM 
		[msdb].[dbo].[sysjobs] AS [sJOB]
		LEFT JOIN (
					SELECT
						[job_id]
						, MIN([next_run_date]) AS [NextRunDate]
						, MIN([next_run_time]) AS [NextRunTime]
					FROM [msdb].[dbo].[sysjobschedules]
					GROUP BY [job_id]
				) AS [sJOBSCH]
			ON [sJOB].[job_id] = [sJOBSCH].[job_id]
		LEFT JOIN (
					SELECT 
						[job_id]
						, [run_date]
						, [run_time]
						, [run_status]
						, [run_duration]
						, [message]
						, ROW_NUMBER() OVER (
												PARTITION BY [job_id] 
												ORDER BY [run_date] DESC, [run_time] DESC
						  ) AS RowNumber
					FROM [msdb].[dbo].[sysjobhistory]
					WHERE [step_id] = 0
				) AS [sJOBH]
			ON [sJOB].[job_id] = [sJOBH].[job_id]
			AND [sJOBH].[RowNumber] = 1
	ORDER BY [JobName]

END 








GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcess]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcess]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcessConfig]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'Import Kalibre Policies'			@ProcessToExecute
	--FL.FileName					= ''	@FileName

	--EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Kalibre Policies',''
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		LEFT JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
			AND FL.FileName = @FileName
		LEFT JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 








GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcessToExecute]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE FILE NAME THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcessToExecute] 
(
		@sFileDirection VARCHAR(50)
		,@sPackageNameToExecute VARCHAR(50)
		,@sFileName VARCHAR(255)
)
AS

BEGIN

	--==============================================================================================================
	--GET RELEVANT INFO OF PACKAGE TO EXECUTE
	--==============================================================================================================
	SELECT 
		MF.sPackageName
		,MF.sFileLocation
		,CASE WHEN MF.sPackageName = 'ClaimsMigration' THEN MF.sFileName ELSE MF.sFileName END AS 'sFileName'
		,MF.sFileProcessedLocation  
		,MF.sPackageFolderName
		,MF.sPackageProjectName
		,MS.iFileFormatID
	FROM 
		[ski_int].[IntegrationMasterFiles] MF (NOLOCK)
		INNER JOIN [ski_int].[IntegrationMasterScripts] MS (NOLOCK) ON 
			MF.ID = MS.iIntegrationMasterFileID
	WHERE 
		MF.sFileDirection = @sFileDirection 
		AND MF.sPackageName = @sPackageNameToExecute
		AND MF.sFileName = @sFileName
		AND MF.iStatusID = 1 


END 






GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ImportLog_Insert]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_ImportLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @FileName NVARCHAR(255)
	, @FileRecordCount INT
	, @InsertCount INT
	, @UpdateCount INT
	, @CancelCount INT
	, @RejectCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISImportLog (
		JobID
		,ProcessName
		,JobReference
		,FileName
		,FileRecordCount
		,InsertCount
		,UpdateCount
		,CancelCount
		,RejectCount
		,StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @FileName
	, @FileRecordCount
	, @InsertCount
	, @UpdateCount
	, @CancelCount
	, @RejectCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ProgressLog]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_ProgressLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ContinueNextStep INT
	, @Message NVARCHAR(255)
	,@Success INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISProgressLog (
	  JobID
	, ProcessName
	, JobReference
	, PackageName
	, TaskName
	, ContinueNextStep
	, Message
	, StartTime
	, Success
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ContinueNextStep
	, @Message
	, GetDate()
	, @Success
	)
		
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartExtractLog]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_StartExtractLog]
  @PackageLogID INT
, @ProcessName NVARCHAR(255)
, @TableName NVARCHAR(100)
AS
BEGIN
	DECLARE @LastExtractDateTime	DATETIME
	SET NOCOUNT ON;
	
	SELECT @LastExtractDateTime = ISNULL(MAX(LastExtractDateTime), '1900-01-01')
	FROM ski_int.SSISExtractLog
	WHERE TableName = @TableName
		
	INSERT INTO ski_int.SSISExtractLog (
	  PackageLogID
	, ProcessName
	, TableName
	, StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @TableName
	, GetDate()
	)

	SELECT 
	  CAST(Scope_Identity() AS INT) ExtractLogID
	, CONVERT(NVARCHAR(23), @LastExtractDateTime, 121) LastExtractDateTimeString
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartJob]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENTRY [ski_int].[SSISJob] EVERY TIME A NEW SSIS JOB IS EXECUTED
--DATE			:	2016-01-18	
--AUTHOR		:	KIRSTEN HARRIS	
--==============================================================================================================

CREATE PROCEDURE [SKI_INT].[SSIS_StartJob]
	@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)

AS

BEGIN
	--==============================================================================================================
	--SAMPLE EXECUTE
	--==============================================================================================================
	--EXEC [ski_int].[SSIS_StartJob] 'Import Daily LTI File - BW','REFERENCE1'

	--==============================================================================================================
	--INSERT JOB
	--==============================================================================================================
	INSERT INTO [ski_int].[SSISJob]
	(
		ProcessName
		,JobReference
		,StartTime
		,EndTime
		,DateCreated
		,Success
	)
	VALUES 
	(
		@ProcessName
		,@JobReference
		,GetDate()
		,NULL
		,GetDate()
		,0
	);

	--==============================================================================================================
	--RETURN LATEST JOB ID
	--==============================================================================================================
	SELECT CAST(Scope_Identity() AS INT) SSISJobID;

END







GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartPackageLog]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_StartPackageLog] 
	@JobID INT
	,@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO ski_int.SSISPackageLog 
	(
		JobID,
		ProcessName,
		JobReference,
		PackageName,
		StartTime
	)
	VALUES 
	(
		@JobID,
		@ProcessName,
		@JobReference,
		@PackageName,
		GetDate()
	);

	SELECT MAX(PackageLogID) FROM ski_int.SSISPackageLog;

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_SuspendLog_Insert]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_SuspendLog_Insert]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @SuspendQueryCount INT
	, @SuspendedCount INT
	, @NotSuspendedCount INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISSuspendLog (
		PackageLogID
		,ProcessName
		,JobReference
		,SuspendQueryCount
		,SuspendedCount
		,NotSuspendedCount
		,ExecutionDate
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @SuspendQueryCount
	, @SuspendedCount
	, @NotSuspendedCount
	, GetDate()
	)
		
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[uspAddBatchUser]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[uspAddBatchUser]
    @pLogin NVARCHAR(50), 
    @pPassword NVARCHAR(50), 
    @responseMessage NVARCHAR(250) OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO [ski_int].[BatchUser] (UserName, Password)
        VALUES(@pLogin, HASHBYTES('SHA2_512', @pPassword))

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END



GO

/****** Object:  StoredProcedure [SKI_INT].[uspAddSSISUser]    Script Date: 03/04/2018 05:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[uspAddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[BatchUser];

	INSERT INTO [ski_int].[BatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END



GO



CREATE PROCEDURE [SKI_INT].[SSIS_SyncDocuments]
(
	  @iDocType int
	, @iContextID int
	, @sPath VARCHAR(520) 
	, @sFileName VARCHAR(300)
	, @bArchived int = 0
	, @iCustomerID int
	, @iContext int = 2
	, @sVirtualPath varchar(1000)
	, @iDocumentID int = NULL
	, @iSensitivity int = NULL
	, @sComment VARCHAR(255)
	, @sDescription VARCHAR(255)
	, @iEndorsementID UNIQUEIDENTIFIER
)

AS

BEGIN

INSERT INTO 
	DocMetaData
VALUES
	('SSISINTEGRATION', GETDATE(), @iDocType, @iContextID, @sPath, @sFileName
	 ,@bArchived, @iCustomerID, @iContext, @sVirtualPath, @iDocumentID, @iSensitivity, @sComment, @sDescription, @iEndorsementID)

END
