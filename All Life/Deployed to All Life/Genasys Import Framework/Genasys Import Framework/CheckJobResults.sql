--====================================================================================================
--DATA QUERIES
--====================================================================================================
DECLARE @JobReference VARCHAR(100);

--==============================
--GET REFERENCE NUMBER
--==============================
SELECT TOP 1 * FROM [ski_int].[SSISJob] ORDER BY SSISJobID DESC
SET @JobReference = (SELECT TOP 1 JobReference FROM [ski_int].[SSISJob] ORDER BY SSISJobID DESC)

--==============================
--PROGRESS
--==============================
SELECT * FROM [ski_int].[SSISProgressLog] WHERE JobReference = @JobReference

--==============================
--ERRORS
--==============================
SELECT * FROM [ski_int].[SSISErrorLog] WHERE JobReference = @JobReference
