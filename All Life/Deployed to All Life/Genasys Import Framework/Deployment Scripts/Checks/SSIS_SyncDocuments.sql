--select top 10 * from DocMetaData order by iContextID desc

ALTER PROCEDURE [SKI_INT].[SSIS_SyncDocuments]
(
	  @iDocType int
	, @iContextID int
	, @sPath VARCHAR(520) 
	, @sFileName VARCHAR(300)
	, @bArchived int = 0
	, @iCustomerID int
	, @iContext int = 2
	, @sVirtualPath varchar(1000)
	, @iDocumentID int = NULL
	, @iSensitivity int = NULL
	, @sComment VARCHAR(255)
	, @sDescription VARCHAR(255)
	, @iEndorsementID UNIQUEIDENTIFIER
)

AS

BEGIN

INSERT INTO 
	DocMetaData
VALUES
	('SSISINTEGRATION', GETDATE(), @iDocType, @iContextID, @sPath, @sFileName
	 ,@bArchived, @iCustomerID, @iContext, @sVirtualPath, @iDocumentID, @iSensitivity, @sComment, @sDescription, @iEndorsementID)

END
