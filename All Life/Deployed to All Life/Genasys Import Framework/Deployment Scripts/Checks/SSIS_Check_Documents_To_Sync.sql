--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS A COUNT OF DOCUMENTS TO WHICH HAVE NOT BEEN SYNCED UP YET
--DATE			:	2018-04-01	
--AUTHOR		:	KIRSTEN HARRIS
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_Check_Documents_To_Sync]

AS

--EXEC SKI_INT.SSIS_Check_Documents_To_Sync
BEGIN

	SELECT 
		  COUNT(*)
	FROM 
		dbo.Policy P
		JOIN Customer C
			ON C.iCustomerID = P.iCustomerID
		JOIN KalibreSA_UAT.dbo.IncomingXMLstore x 
			ON x.[policy-reference] = P.sOldPolicyNo
	WHERE
		NOT EXISTS	(
					SELECT * FROM DocMetaData DM
					WHERE DM.iCustomerID = C.iCustomerID
					AND DM.sFileName = [ski_int].[fn_GetFileNameFromPath] (x.KeyingSummary_PDF)
					AND iDocType = 1
					)
		;


END 








