--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE LIST OF DOCUMENTS TO BE ATTACHED TO POLICIES IMPORTED
--DATE			:	2018-04-01	
--AUTHOR		:	KIRSTEN HARRIS
--==============================================================================================================
ALTER PROCEDURE [SKI_INT].[SSIS_Get_List_of_Documents]

AS
--EXEC SKI_INT.SSIS_Get_List_of_Documents
BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--EXEC [ski_int].[SKI_INT.SSIS_Get_List_of_Documents]
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT 
		  CAST(p.iPolicyID As INT) AS iPolicyID
		, P.sPolicyNo
		--, P.sOldPolicyNo
		, x.[policy-reference] AS Ski_Policy_Reference
		, [ski_int].[fn_GetFilePath] (x.KeyingSummary_PDF) AS Source_Document_Path
		, [ski_int].[fn_GetFileNameFromPath] (x.KeyingSummary_PDF) AS Source_FileName
		--, x.FileSource As File_Source
		, '%ClientFolder%\GeneratedDocs\' + C.sInitials + '\' + C.sLastName + ' ' + C.sFirstName + '_' + LTRIM(RTRIM(STR(C.iCustomerID))) + '\Policies\' + P.sPolicyNo + '\Miscellaneous\' AS Destination_Path
		, 'Policies\' + P.sPolicyNo + '\Miscellaneous' AS Destination_VirtualPath
		, CAST(P.iEndorsementID AS VARCHAR(255)) AS iEndorsementID
		, CAST(C.iCustomerID AS INT) AS iCustomerID
	INTO
		#Documents
	FROM 
		dbo.Policy P
		JOIN Customer C
			ON C.iCustomerID = P.iCustomerID
		JOIN KalibreSA_UAT.dbo.IncomingXMLstore x 
			ON x.[policy-reference] = P.sOldPolicyNo
	WHERE
		NOT EXISTS	(
					SELECT * FROM DocMetaData DM
					WHERE DM.iCustomerID = C.iCustomerID
					AND DM.sFileName = [ski_int].[fn_GetFileNameFromPath] (x.KeyingSummary_PDF)
					AND iDocType = 1
					)
		;

SELECT * FROM #Documents

END 








