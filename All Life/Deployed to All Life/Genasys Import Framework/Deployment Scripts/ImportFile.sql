
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.3156)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

ALTER PROCEDURE [ski_int].[Int_Kalibre_Import]
AS
BEGIN

	--========================================================================================================================
	--CLEAR PROCESSED DATA TABLES
	--========================================================================================================================
	TRUNCATE TABLE [dbo].[IncomingXMLstore]

	--========================================================================================================================
	--INSERT DATA RAW DATA FROM XML
	--========================================================================================================================
	INSERT INTO [dbo].[IncomingXMLstore](XMLData, Importdate, FileName, OutCome, [policy-reference])
	SELECT CONVERT(XML, BulkColumn) AS BulkColumn, GETDATE(),'Kalibre_Daily2.xml','','' 
	FROM OPENROWSET(BULK 'c:\Ski\Kalibre_Daily2.xml', SINGLE_BLOB) AS x;
	
	--========================================================================================================================
	--INSERT DATA RAW DATA INTO RAW DATA LOG TABLE
	--========================================================================================================================
	INSERT INTO [SKI_INT].[IncomingXMLstore_LOG]
	SELECT * FROM [dbo].[IncomingXMLstore];

	--========================================================================================================================
	--INSERT DATA INTO PROCESS TABLE FROM RAW DATA
	--========================================================================================================================
	TRUNCATE TABLE [ski_int].[DailyStaging_ETL_XML]
	EXEC [dbo].[usp_Insert_DailyStaging_ETL_XML];

SET NOCOUNT ON;

END


