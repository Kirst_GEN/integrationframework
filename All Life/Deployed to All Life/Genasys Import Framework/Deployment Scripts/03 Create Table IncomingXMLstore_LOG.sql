
/****** Object:  Table [dbo].[IncomingXMLstore]    Script Date: 15/12/2017 2:55:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SKI_INT].[IncomingXMLstore_LOG](
	[Id] [int] NOT NULL,
	[XMLData] [xml] NULL,
	[Importdate] [datetime] NULL,
	[FileName] [varchar](50) NULL,
	[Outcome] [varchar](50) NULL,
	[policy-reference] [varchar](30) NULL,
) ON [PRIMARY]
GO


