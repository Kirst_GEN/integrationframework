
truncate table SKI_Policy_Fields
select * from SKI_Risk_Detail_Fields_WholeLife

insert into SKI_Policy_Fields
(P_KalibreID,	iPolicyTypeID,	iProductID,	sPolicyNo,	sOldPolicyNo,	iTariffVersion,	iPolicyStatus,	iPaymentStatus
,	dStartDate,	iPaymentTerm,	iTaxPerc,	iPaymentMethod,	sPolicyType,	iCollection
,	iCurrency,	dCoverStart,	sFinAccount,	CountryId)
SELECT P_KalibreID,	251,	251,	'',	P_KalibreID,	1,	0,	0
,	getdate(),	3,	1.0,	0,	'New Advantage',	1
,	0,	getdate(),	'DEFAULT',	206
from SKI_Customer_Fields

insert into SKI_Risk_Fields
(P_KalibreID,intRiskTypeID,strDescription,intStatus,intCurrency,iProductID)
select P_KalibreID,153,'Whole Life',2,0,251
from SKI_Customer_Fields

select * from SKI_Risk_Fields