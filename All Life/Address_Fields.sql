--USE [KalibreSA_SkiImport]
--GO

/****** Object:  Table [dbo].[SKI_Address_Fields]    Script Date: 11/15/2017 12:51:21 PM ******/


CREATE TABLE [dbo].[SKI_Address_Fields](
	[intAddressID] [float] NULL,
	[strAddressGroup] [varchar](30) NULL,
	[strAddressType] [varchar](30) NULL,
	[strAddressLine1] [varchar](50) NULL,
	[strAddressLine2] [varchar](50) NULL,
	[strAddressLine3] [varchar](50) NULL,
	[strSuburb] [varchar](50) NULL,
	[strPostalCode] [varchar](13) NULL,
	[strRatingArea] [varchar](30) NULL,
	[strPhoneNo] [varchar](30) NULL,
	[strFaxNo] [varchar](30) NULL,
	[strMobileNo] [varchar](30) NULL,
	[strEmailAdd] [varchar](50) NULL,
	[bReplicated] [int] NULL,
	[iTownID] [float] NULL,
	[dUpdated] [datetime] NULL,
	[sUpdatedBy] [varchar](20) NULL,
	[sTown] [varchar](50) NULL,
	[iTranID] [numeric](18, 0) NULL,
	[icustomerid] [varchar](30) NULL,
	[bPrefered] [varchar](30) NULL,
	[iLatitude] [float] NULL,
	[iLongitude] [float] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


