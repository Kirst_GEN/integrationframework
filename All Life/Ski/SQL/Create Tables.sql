USE [AllLife]
GO

/****** Object:  Table [ski_int].[SSIS_Daily_NewBusiness]    Script Date: 12/03/2018 9:07:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSIS_Daily_NewBusiness](
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NULL,
	[dtProcessDate] [datetime] NULL,
	[P_KalibreID] [varchar](50) NULL,
	[CH_LastName] [varchar](30) NULL,
	[CH_FirstName] [varchar](30) NULL,
	[CH_Initials] [varchar](8) NULL,
	[CH_CountryID] [int] NOT NULL,
	[C_TITLE] [varchar](15) NULL,
	[C_IDTYPE] [int] NOT NULL,
	[C_IDNO] [varchar](max) NULL,
	[C_DOB] [date] NULL,
	[C_GENDER] [int] NOT NULL,
	[C_AGE] [int] NULL,
	[C_ECOMMSALLOWED] [varchar](255) NULL,
	[C_EMAILDOCS] [int] NOT NULL,
	[C_DONOTPOST] [int] NOT NULL,
	[C_CELLNO] [varchar](max) NULL,
	[C_EMAIL] [varchar](max) NULL,
	[C_HIVINFORMATIO] [varchar](max) NULL,
	[C_VIRALLOADAPPL] [int] NOT NULL,
	[C_DIABETICSINFO] [varchar](max) NULL,
	[C_A1_APPLICABLE] [int] NULL,
	[C_A1CCHANGEREAS] [varchar](100) NULL,
	[C_A1_ADHERENCE] [money] NULL,
	[C_A1_ADHEREDATE] [datetime] NULL,
	[C_A1CLAB] [float] NULL,
	[C_A1CCOMMENT] [varchar](255) NULL,
	[C_A1CPREVDATE] [datetime] NULL,
	[C_A1CLABPREV] [float] NULL,
	[C_A1CCOMMPREV] [varchar](255) NULL,
	[C_A1CHANGHIST] [varchar](100) NULL,
	[C_LSTADHMSGSENT] [varchar](50) NULL,
	[C_LASTSMSMSGDT] [datetime] NULL,
	[C_NEXTSMSMSGDT] [datetime] NULL,
	[C_NEXTSMSTYPE] [varchar](50) NULL,
	[C_LASTADHLETDTR] [datetime] NULL,
	[C_LASTADHLETTER] [varchar](255) NULL,
	[C_CTRLSTATUS] [varchar](100) NULL,
	[C_WHLIFECONVERT] [int] NULL,
	[C_WHLIFEOPTOUT] [int] NULL,
	[C_WHLIFETERMLET] [int] NULL,
	[AR_AddressType] [varchar](30) NULL,
	[A_AddressLine1] [varchar](50) NULL,
	[AR_AddressLine2] [varchar](50) NULL,
	[AR_AddressLine3] [varchar](50) NULL,
	[AR_PostalCode] [varchar](13) NULL,
	[AP_AddressType] [varchar](30) NULL,
	[AP_AddressLine1] [varchar](50) NULL,
	[AP_AddressLine2] [varchar](50) NULL,
	[AP_AddressLine3] [varchar](50) NULL,
	[AP_PostalCode] [varchar](13) NULL,
	[P_Bank] [varchar](50) NULL,
	[P_AccHolderName] [varchar](50) NULL,
	[P_AccountNumber] [varchar](16) NULL,
	[P_AccountType] [varchar](15) NULL,
	[P_BranchCode] [varchar](10) NULL,
	[P_PaymentMethod] [int] NOT NULL,
	[P_sOldPolicyNo] [varchar](50) NULL,
	[P_PolicyStatus] [int] NULL,
	[P_CoverTerm] [int] NULL,
	[P_PaymentTerm] [int] NULL,
	[P_PaymentStatus] [int] NULL,
	[P_CoverStartDate] [datetime] NULL,
	[P_PolicyType] [varchar](30) NULL,
	[P_iCollection] [int] NULL,
	[P_Currency] [int] NULL,
	[P_sFinAccount] [varchar](15) NULL,
	[P_CountryID] [int] NULL,
	[P_EDUCATIONESL] [varchar](max) NULL,
	[PD_P_EDUCATIONESL_DESC] [varchar](30) NULL,
	[PD_P_LIPOSITION] [varchar](max) NULL,
	[PD_P_PAYERGROSSINC] [varchar](max) NULL,
	[PD_P_EXISTCOVER] [int] NOT NULL,
	[PD_P_BENEFID0] [varchar](max) NULL,
	[PD_P_BENEFID1] [varchar](max) NULL,
	[PD_P_BENEFID2] [varchar](max) NULL,
	[PD_P_BENEFID3] [varchar](max) NULL,
	[PD_P_BENEFID4] [varchar](max) NULL,
	[PD_P_BENEFID5] [varchar](max) NULL,
	[PD_P_BENEFID6] [varchar](max) NULL,
	[PD_P_BENEFID7] [varchar](max) NULL,
	[PD_P_BENEFID8] [varchar](max) NULL,
	[PD_P_BENEFID9] [varchar](max) NULL,
	[PD_P_BENEFSHARE0] [varchar](max) NULL,
	[PD_P_BENEFSHARE1] [varchar](max) NULL,
	[PD_P_BENEFSHARE2] [varchar](max) NULL,
	[PD_P_BENEFSHARE3] [varchar](max) NULL,
	[PD_P_BENEFSHARE4] [varchar](max) NULL,
	[PD_P_BENEFSHARE5] [varchar](max) NULL,
	[PD_P_BENEFSHARE6] [varchar](max) NULL,
	[PD_P_BENEFSHARE7] [varchar](max) NULL,
	[PD_P_BENEFSHARE8] [varchar](max) NULL,
	[PD_P_BENEFSHARE9] [varchar](max) NULL,
	[PD_P_BMI] [varchar](max) NULL,
	[PD_P_DEBITORDER] [int] NOT NULL,
	[PD_P_DOCLOCATION] [varchar](max) NULL,
	[PD_P_DOCTOR] [varchar](max) NULL,
	[PD_P_INSURCOMPANY1] [varchar](max) NULL,
	[PD_P_INSURCOMPANY2] [varchar](max) NULL,
	[PD_P_INSURCOMPANY3] [varchar](max) NULL,
	[PD_P_LIEMPLOYER] [varchar](max) NULL,
	[PD_P_M2_Q10] [int] NOT NULL,
	[PD_P_M2_Q10HOSP] [int] NOT NULL,
	[PD_P_M2_Q12] [int] NOT NULL,
	[PD_P_M2_Q12DET1] [varchar](max) NULL,
	[PD_P_M2_Q12DET21] [varchar](max) NULL,
	[PD_P_M2_Q13] [int] NOT NULL,
	[PD_P_M2_Q14] [int] NOT NULL,
	[PD_P_M2_Q15] [int] NOT NULL,
	[PD_P_M2_Q16] [int] NOT NULL,
	[PD_P_M2_Q17] [int] NOT NULL,
	[PD_P_M2_Q17DET] [varchar](max) NULL,
	[PD_P_PAYERCONTACT] [varchar](max) NULL,
	[PD_P_PAYEREMAIL] [varchar](max) NULL,
	[PD_P_PAYERSURNAME] [varchar](max) NULL,
	[PD_P_POCELLNO] [varchar](max) NULL,
	[PD_P_POFULLNAME] [varchar](max) NULL,
	[RD_R_AGEATQUOTE] [varchar](max) NULL,
	[RD_R_AGERATED] [int] NOT NULL,
	[RD_R_DATEOFBIRTH] [varchar](max) NULL,
	[RDW_R_DIABETESTYPE] [int] NULL,
	[RD_R_DIAGNOSISYEAR] [varchar](max) NULL,
	[RD_R_DISABILITY] [int] NOT NULL,
	[RD_R_ELCOVERAMOUNT] [varchar](max) NULL,
	[RDW_R_ELGENDER] [int] NULL,
	[RD_R_PREMIUM] [varchar](max) NULL,
	[RD_R_QUOTEDATE] [varchar](max) NULL,
	[RD_R_SMOKERSTATUS] [int] NOT NULL,
	[RD_R_TYPEOFDISABIL] [varchar](255) NULL,
	[RD_R_UWHEIGHT] [varchar](max) NULL,
	[RD_R_UWUPDATE] [int] NOT NULL,
	[RD_R_UWWEIGHT] [varchar](max) NULL,
	[RD_R_VIF] [varchar](max) NULL,
	[RD_R_VIRALLOADATQU] [varchar](max) NULL,
	[RD_R_INSVALUE] [float] NULL,
 CONSTRAINT [PK_SSIS_Daily_NewBusiness] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[IncomingXMLstore_LOG]    Script Date: 12/03/2018 9:07:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[IncomingXMLstore_LOG](
	[Id] [int] NOT NULL,
	[XMLData] [xml] NULL,
	[Importdate] [datetime] NULL,
	[FileName] [varchar](50) NULL,
	[Outcome] [varchar](50) NULL,
	[policy-reference] [varchar](30) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[DailyStaging_ETL_XML]    Script Date: 12/03/2018 9:07:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[DailyStaging_ETL_XML](
	[policy-reference] [varchar](50) NULL,
	[sent_timestamp] [datetime] NULL,
	[version] [int] NULL,
	[date_of_birth] [nvarchar](max) NULL,
	[gender] [nvarchar](max) NULL,
	[li-gross-monthly-income] [nvarchar](max) NULL,
	[level-education] [nvarchar](max) NULL,
	[m-cm] [int] NULL,
	[kg] [int] NULL,
	[smk-apply] [nvarchar](max) NULL,
	[alcohol-amount] [nvarchar](max) NULL,
	[hiv-status] [nvarchar](max) NULL,
	[diabetes-status] [nvarchar](max) NULL,
	[bp-high] [nvarchar](max) NULL,
	[chol-high] [nvarchar](max) NULL,
	[first-diagnosed] [nvarchar](max) NULL,
	[diabetic-diagnosis] [nvarchar](max) NULL,
	[basic-life-outcome] [nvarchar](max) NULL,
	[pricing-basic-life-quote-id] [nvarchar](max) NULL,
	[pricing-basic-life-accept] [nvarchar](max) NULL,
	[pricing-basic-bmi] [nvarchar](max) NULL,
	[pricing-basic-life-base-premium-no-fee] [nvarchar](max) NULL,
	[pricing-basic-life-final-premium-no-fee] [nvarchar](max) NULL,
	[pricing-basic-life-cover] [nvarchar](max) NULL,
	[pricing-basic-life-base-premium] [nvarchar](max) NULL,
	[pricing-basic-life-decision-premium] [nvarchar](max) NULL,
	[pricing-basic-life-final-premium] [nvarchar](max) NULL,
	[pricing-final-decision-wording] [nvarchar](max) NULL,
	[eligibility_dementia] [nvarchar](max) NULL,
	[eligibility_schiz] [nvarchar](max) NULL,
	[eligibility_drugs] [nvarchar](max) NULL,
	[eligibility_stroke] [nvarchar](max) NULL,
	[eligibility_limbs] [nvarchar](max) NULL,
	[eligibility_kidney] [nvarchar](max) NULL,
	[eligibility_heart_attack] [nvarchar](max) NULL,
	[hiv-conditions] [nvarchar](max) NULL,
	[hiv-conditions-specify] [nvarchar](max) NULL,
	[cancer-conditions] [nvarchar](max) NULL,
	[cancer-conditions-specify] [nvarchar](max) NULL,
	[heart-conditions] [nvarchar](max) NULL,
	[heart-conditions-specify] [nvarchar](max) NULL,
	[form-of-stroke] [nvarchar](max) NULL,
	[form-of-stroke-specify] [nvarchar](max) NULL,
	[sick-leave] [nvarchar](max) NULL,
	[sick-leave-specify] [nvarchar](max) NULL,
	[current-pregnancy] [nvarchar](max) NULL,
	[last-five-years] [nvarchar](max) NULL,
	[non-art-medication] [nvarchar](max) NULL,
	[non-art-medication-specify] [nvarchar](max) NULL,
	[drugs-alcohol] [nvarchar](max) NULL,
	[drugs-alcohol-specify] [nvarchar](max) NULL,
	[hospitalisation-surgeries] [nvarchar](max) NULL,
	[other-disclosures] [nvarchar](max) NULL,
	[other-issues-tests] [nvarchar](max) NULL,
	[other-issues-tests-specify] [nvarchar](max) NULL,
	[travels] [nvarchar](max) NULL,
	[pursuits-flying] [nvarchar](max) NULL,
	[pursuits-motorcar] [nvarchar](max) NULL,
	[pursuits-cave] [nvarchar](max) NULL,
	[pursuits-sailing] [nvarchar](max) NULL,
	[pursuits-mountain] [nvarchar](max) NULL,
	[pursuits-extremesports] [nvarchar](max) NULL,
	[pursuits-none-of-above] [nvarchar](max) NULL,
	[occ-underwater] [nvarchar](max) NULL,
	[occ-underground] [nvarchar](max) NULL,
	[occ-explosives] [nvarchar](max) NULL,
	[occ-heights] [nvarchar](max) NULL,
	[occ-firearm] [nvarchar](max) NULL,
	[occ-manual] [nvarchar](max) NULL,
	[occ-none-of-above] [nvarchar](max) NULL,
	[job-title] [nvarchar](max) NULL,
	[life-cover-amount-dq] [nvarchar](max) NULL,
	[life-premium-amount-dq] [nvarchar](max) NULL,
	[disability-conf-dq] [nvarchar](max) NULL,
	[plan-fee] [nvarchar](max) NULL,
	[pricing-decision-life-final-premium-no-fee] [nvarchar](max) NULL,
	[pricing-decision-life-cover] [nvarchar](max) NULL,
	[pricing-decision-life-base-premium] [nvarchar](max) NULL,
	[pricing-decision-life-decision-premium] [nvarchar](max) NULL,
	[pricing-decision-life-final-premium] [nvarchar](max) NULL,
	[decision-life-outcome] [nvarchar](max) NULL,
	[pricing-decision-life-quote-id] [nvarchar](max) NULL,
	[pricing-decision-life-accept] [nvarchar](max) NULL,
	[life-term-age-string] [nvarchar](max) NULL,
	[pricing-decision-bmi] [nvarchar](max) NULL,
	[life-calculated-term-years] [nvarchar](max) NULL,
	[first-name] [nvarchar](max) NULL,
	[surname] [nvarchar](max) NULL,
	[title] [nvarchar](max) NULL,
	[id-number] [nvarchar](max) NULL,
	[passport-number] [nvarchar](max) NULL,
	[type-of-identification] [nvarchar](max) NULL,
	[res-address-one] [nvarchar](max) NULL,
	[res-address-two] [nvarchar](max) NULL,
	[res-postal-code] [nvarchar](max) NULL,
	[cell-number] [nvarchar](max) NULL,
	[res-address-three] [nvarchar](max) NULL,
	[email-address] [nvarchar](max) NULL,
	[preferred-channel] [nvarchar](max) NULL,
	[email-address-check] [nvarchar](max) NULL,
	[has-other-cover] [nvarchar](max) NULL,
	[cover-amount-one] [nvarchar](max) NULL,
	[cover-amount-two] [nvarchar](max) NULL,
	[art-conf] [nvarchar](max) NULL,
	[art-start-date] [nvarchar](max) NULL,
	[art-regime] [nvarchar](max) NULL,
	[primary-doctor-name] [nvarchar](max) NULL,
	[primary-doctor-phone-number] [nvarchar](max) NULL,
	[primary-doctor-city] [nvarchar](max) NULL,
	[bene-optional] [nvarchar](max) NULL,
	[fullname-insert] [nvarchar](max) NULL,
	[dob-insert] [nvarchar](max) NULL,
	[id-number-insert] [nvarchar](max) NULL,
	[percentage-insert] [nvarchar](max) NULL,
	[existing-beneficiary-list] [nvarchar](max) NULL,
	[P_POLICYBENEF1] [varchar](50) NULL,
	[P_BENFDOB1] [varchar](50) NULL,
	[P_BENEFID1] [varchar](50) NULL,
	[P_BENEFSHARE1] [varchar](50) NULL,
	[P_POLICYBENEF2] [varchar](50) NULL,
	[P_BENFDOB2] [varchar](50) NULL,
	[P_BENEFID2] [varchar](50) NULL,
	[P_BENEFSHARE2] [varchar](50) NULL,
	[P_POLICYBENEF3] [varchar](50) NULL,
	[P_BENFDOB3] [varchar](50) NULL,
	[P_BENEFID3] [varchar](50) NULL,
	[P_BENEFSHARE3] [varchar](50) NULL,
	[P_POLICYBENEF4] [varchar](50) NULL,
	[P_BENFDOB4] [varchar](50) NULL,
	[P_BENEFID4] [varchar](50) NULL,
	[P_BENEFSHARE4] [varchar](50) NULL,
	[P_POLICYBENEF5] [varchar](50) NULL,
	[P_BENFDOB5] [varchar](50) NULL,
	[P_BENEFID5] [varchar](50) NULL,
	[P_BENEFSHARE5] [varchar](50) NULL,
	[P_POLICYBENEF6] [varchar](50) NULL,
	[P_BENFDOB6] [varchar](50) NULL,
	[P_BENEFID6] [varchar](50) NULL,
	[P_BENEFSHARE6] [varchar](50) NULL,
	[P_POLICYBENEF7] [varchar](50) NULL,
	[P_BENFDOB7] [varchar](50) NULL,
	[P_BENEFID7] [varchar](50) NULL,
	[P_BENEFSHARE7] [varchar](50) NULL,
	[P_POLICYBENEF8] [varchar](50) NULL,
	[P_BENFDOB8] [varchar](50) NULL,
	[P_BENEFID8] [varchar](50) NULL,
	[P_BENEFSHARE8] [varchar](50) NULL,
	[P_POLICYBENEF9] [varchar](50) NULL,
	[P_BENFDOB9] [varchar](50) NULL,
	[P_BENEFID9] [varchar](50) NULL,
	[P_BENEFSHARE9] [varchar](50) NULL,
	[P_POLICYBENEF10] [varchar](50) NULL,
	[P_BENFDOB10] [varchar](50) NULL,
	[P_BENEFID10] [varchar](50) NULL,
	[P_BENEFSHARE10] [varchar](50) NULL,
	[payer-intro] [nvarchar](max) NULL,
	[bank-name] [nvarchar](max) NULL,
	[account-holder-name] [nvarchar](max) NULL,
	[account-branch-code] [nvarchar](max) NULL,
	[account-number] [nvarchar](max) NULL,
	[account-type] [nvarchar](max) NULL,
	[first-debit-date] [nvarchar](max) NULL,
	[debit-order-day] [nvarchar](max) NULL,
	[consent-to-debit] [nvarchar](max) NULL,
	[ts-and-cs-agree] [nvarchar](max) NULL,
	[iID] [bigint] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NULL,
	[dtProcessDate] [datetime] NULL,
 CONSTRAINT [PK_DailyStaging_ETL_XML] PRIMARY KEY CLUSTERED 
(
	[iID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISSuspendLog]    Script Date: 12/03/2018 9:07:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISSuspendLog](
	[SuspendLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[SuspendQueryCount] [int] NOT NULL,
	[SuspendedCount] [int] NOT NULL,
	[NotSuspendedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISReceiptingLog]    Script Date: 12/03/2018 9:07:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISReceiptingLog](
	[ReceiptingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[ReceiptingQueryCount] [int] NOT NULL,
	[ReceiptingCount] [int] NOT NULL,
	[NotReceiptedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISRaisingLog]    Script Date: 12/03/2018 9:07:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISRaisingLog](
	[RaisingLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[RaisingQueryCount] [int] NOT NULL,
	[RaisedCount] [int] NOT NULL,
	[NotRaisedCount] [int] NOT NULL,
	[ExecutionDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISProgressLog]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISProgressLog](
	[ProgressLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ContinueNextStep] [bit] NULL,
	[Message] [nvarchar](255) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NOT NULL,
 CONSTRAINT [PK_SSISProgressLog] PRIMARY KEY CLUSTERED 
(
	[ProgressLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISPackageLog]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISPackageLog](
	[PackageLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISPackageLog] PRIMARY KEY CLUSTERED 
(
	[PackageLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISImportLogTest]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISImportLogTest](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISImportLog]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISImportLog](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISExtractLog]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISExtractLog](
	[ExtractLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[ExtractCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[LastExtractDateTime] [datetime] NULL,
	[Success] [bit] NOT NULL,
 CONSTRAINT [PK_SSISExtractLog] PRIMARY KEY CLUSTERED 
(
	[ExtractLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISErrorLog]    Script Date: 12/03/2018 9:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISDataErrorLog]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISDataErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorSource] [nvarchar](255) NOT NULL,
	[SourceRecord] [nvarchar](255) NOT NULL,
	[ErrorNumber] [nvarchar](100) NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISScriptType]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISScriptType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISScript]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISScript](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScriptId] [int] NOT NULL,
	[ScriptTypeId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISProcessType]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISProcessType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ScriptId] [int] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISProcess]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISProcess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](400) NULL,
	[ProcessTypeId] [int] NULL,
	[CommunicationId] [int] NULL,
	[FileLocationId] [int] NULL,
	[ScriptId] [int] NULL,
	[SSISDBCatalogFolderName] [varchar](100) NULL,
	[SSISDBCatalogProjectName] [varchar](100) NULL,
	[SSISDBCatalogPackageName] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISMailsToBeSent]    Script Date: 12/03/2018 9:07:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISMailsToBeSent](
	[MailID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[MailFrom] [varchar](200) NULL,
	[MailTO] [varchar](400) NULL,
	[MailCC] [varchar](200) NULL,
	[MailBCC] [varchar](200) NULL,
	[MailSubject] [varchar](200) NULL,
	[MailBody] [varchar](max) NULL,
	[DateToSend] [date] NULL,
	[Sent] [bit] NULL,
	[DateSent] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISJob]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISJob](
	[SSISJobID] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISJob] PRIMARY KEY CLUSTERED 
(
	[SSISJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISIntegrationLookupMap]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISIntegrationLookupMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[SourceGroup] [varchar](50) NOT NULL,
	[SourceDBValue] [varchar](255) NOT NULL,
	[TargetGroup] [varchar](255) NOT NULL,
	[TargetDBValue] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CL_LookupMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISFileType]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISFileType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](10) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISFileLocations]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISFileLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [varchar](100) NULL,
	[FileName] [varchar](100) NULL,
	[FileType] [int] NULL,
	[FileHasHeaderRow] [bit] NULL,
	[FileHasFooterRow] [bit] NULL,
	[PickupLocation] [varchar](255) NULL,
	[ProcessedLocation] [varchar](255) NULL,
	[ArchiveLocation] [varchar](255) NULL,
	[LogFileLocation] [varchar](255) NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISEmailConfig]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISEmailConfig](
	[uID] [int] NULL,
	[MailServer] [varbinary](100) NOT NULL,
	[MailPort] [int] NOT NULL,
	[MailUserName] [varchar](100) NOT NULL,
	[MailPassword] [varbinary](100) NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISCommunicationType]    Script Date: 12/03/2018 9:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISCommunicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISCommunication]    Script Date: 12/03/2018 9:07:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationId] [int] NOT NULL,
	[CommunicationTypeId] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[InAttachment] [bit] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSISBatchUser]    Script Date: 12/03/2018 9:07:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSISBatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSIS_Src_RawData_Log]    Script Date: 12/03/2018 9:07:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSIS_Src_RawData_Log](
	[ID] [int] NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[SSIS_Src_RawData]    Script Date: 12/03/2018 9:07:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSIS_Src_RawData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NOT NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL,
 CONSTRAINT [PK_SSIS_Src_RawData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[DynamicSQLStatements]    Script Date: 12/03/2018 9:07:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[DynamicSQLStatements](
	[ExecuteOrder] [int] NULL,
	[sMainTableName] [varchar](255) NULL,
	[sSQLStatement] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[DailyImportExceptions]    Script Date: 12/03/2018 9:07:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[DailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [ski_int].[BatchUser]    Script Date: 12/03/2018 9:07:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
GO

ALTER TABLE [ski_int].[SSISProgressLog] ADD  CONSTRAINT [DF_ProgressLog_Success]  DEFAULT ((0)) FOR [Success]
GO

ALTER TABLE [ski_int].[SSISExtractLog] ADD  CONSTRAINT [DF_ExtractLog_ExtractCount]  DEFAULT ((0)) FOR [ExtractCount]
GO

ALTER TABLE [ski_int].[SSISExtractLog] ADD  CONSTRAINT [DF_ExtractLog_Success]  DEFAULT ((0)) FOR [Success]
GO

ALTER TABLE [ski_int].[SSISDataErrorLog] ADD  CONSTRAINT [DF_DataErrorLog_ErrorTime]  DEFAULT (getdate()) FOR [ErrorTime]
GO

ALTER TABLE [ski_int].[SSISJob] ADD  CONSTRAINT [DF_SSISJobID_Success]  DEFAULT ((0)) FOR [Success]
GO

ALTER TABLE [ski_int].[DailyImportExceptions] ADD  CONSTRAINT [Imported_D_0]  DEFAULT ((0)) FOR [Imported]
GO


