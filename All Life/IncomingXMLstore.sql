--USE [KalibreSA_SkiImport ]
--GO


CREATE TABLE [dbo].[IncomingXMLstore](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[XMLData] [xml] NULL,
	[Importdate] [datetime] NULL,
	[FileName] [varchar](50) NULL,
	[Outcome] [varchar](50) NULL,
	[policy-reference] [varchar] (30)
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


