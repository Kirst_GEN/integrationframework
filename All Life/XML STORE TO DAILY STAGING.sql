--USE [KalibreSA_SkiImport ]
--GO

/****** Object:  StoredProcedure [dbo].[usp_Insert_DailyStaging_ETL_XML]    Script Date: 11/22/2017 9:27:07 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

ALTER Procedure [dbo].[usp_Insert_DailyStaging_ETL_XML]
as begin

insert into  ski_int.DailyStaging_ETL_XML
--select * into [DailyStagingXML] from (
select 
[policy-reference]                           = case when XMLData.value('(/buy_message/forms//item[@name="agreement-reference"])[1]', 'nvarchar(max)') is null 
                                                      then XMLData.value('(buy_message/message/quote-reference/text())[1]','Nvarchar(max)')
													  else XMLData.value('(/buy_message/forms//item[@name="agreement-reference"])[1]', 'nvarchar(max)') end
,[sent_timestamp]                             = XMLData.value('(buy_message/message/sent_timestamp/text())[1]'                                     ,      'datetime')
,[version]                                    = XMLData.value('(buy_message/message/version/text())[1]'                                            ,           'Int')
,[date_of_birth]                              = XMLData.value('(/buy_message/forms//item[@name="date-of-birth"])[1]'                               , 'nvarchar(max)')
,[gender]                                     = XMLData.value('(/buy_message/forms//item[@name="gender"])[1]'                                      , 'nvarchar(max)')
,[li-gross-monthly-income]                    = XMLData.value('(/buy_message/forms//item[@name="li-gross-monthly-income"])[1]'                     , 'nvarchar(max)')
,[level-education]                            = XMLData.value('(/buy_message/forms//item[@name="level-education"])[1]'                             , 'nvarchar(max)')

/*<section name="lifestyle"><group name="lifestyle">*/

,[m-cm]                                       = XMLData.value('(/buy_message/forms//item[@name="m-cm"])[1]'                                        ,           'Int')
,[kg]                                         = XMLData.value('(/buy_message/forms//item[@name="kg"])[1]'                                          ,           'Int')
,[smk-apply]                                  = XMLData.value('(/buy_message/forms//item[@name="smk-apply"])[1]'                                   , 'nvarchar(max)')
,[alcohol-amount]                             = XMLData.value('(/buy_message/forms//item[@name="alcohol-amount"])[1]'                              , 'nvarchar(max)')
,[hiv-status]                                 = XMLData.value('(/buy_message/forms//item[@name="hiv-status"])[1]'                                  , 'nvarchar(max)')
,[diabetes-status]                            = XMLData.value('(/buy_message/forms//item[@name="diabetes-status"])[1]'                             , 'nvarchar(max)')
,[bp-high]                                    = XMLData.value('(/buy_message/forms//item[@name="bp-high"])[1]'                                     , 'nvarchar(max)')
,[chol-high]                                  = XMLData.value('(/buy_message/forms//item[@name="chol-high"])[1]'                                   , 'nvarchar(max)')


/*<section name="lifestyle"><group name="Diabetes">*/

,[first-diagnosed]                            =	XMLData.value('(/buy_message/forms//item[@name="first-diagnosed"])[1]'                             , 'nvarchar(max)')
,[diabetic-diagnosis]                         =	XMLData.value('(/buy_message/forms//item[@name="diabetic-diagnosis"])[1]'                          , 'nvarchar(max)')


/*<section name="cover"><group name="pricing">*/

,[basic-life-outcome]                         = XMLData.value('(/buy_message/forms//item[@name="basic-life-outcome"])[1]'                          , 'nvarchar(max)')
,[pricing-basic-life-quote-id]                = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-quote-id"])[1]'                 , 'nvarchar(max)')
,[pricing-basic-life-accept]                  = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-accept"])[1]'                   , 'nvarchar(max)')
--,[life-term-age-string]                       = XMLData.value('(/buy_message/forms//item[@name="life-term-age-string"])[1]'                        , 'nvarchar(max)')
--,[plan-fee]                                   = XMLData.value('(/buy_message/forms//item[@name="plan-fee"])[1]'                                    , 'nvarchar(max)')
,[pricing-basic-bmi]                          = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-bmi"])[1]'                           , 'nvarchar(max)')
,[pricing-basic-life-base-premium-no-fee]     = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-base-premium-no-fee"])[1]'      , 'nvarchar(max)')
,[pricing-basic-life-final-premium-no-fee]    = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-final-premium-no-fee"])[1]'     , 'nvarchar(max)')
,[pricing-basic-life-cover]                   = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-cover"])[1]'                    , 'nvarchar(max)')
,[pricing-basic-life-base-premium]            = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-base-premium"])[1]'             , 'nvarchar(max)')
,[pricing-basic-life-decision-premium]        = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-decision-premium"])[1]'         , 'nvarchar(max)')
,[pricing-basic-life-final-premium]           = XMLData.value('(/buy_message/forms//item[@name="pricing-basic-life-final-premium"])[1]'            , 'nvarchar(max)')
--,[life-calculated-term-years]                 = XMLData.value('(/buy_message/forms//item[@name="life-calculated-term-years"])[1]'                  ,           'Int')
,[pricing-final-decision-wording]             = XMLData.value('(/buy_message/forms//item[@name="pricing-final-decision-wording"])[1]'                       , 'nvarchar(max)')


/*<form name="Apply"><section name="eligibility"><group name="eligibility">*/

,[eligibility_dementia]                       = XMLData.value('(/buy_message/forms//item[@name="eligibility_dementia"])[1]'                        , 'nvarchar(max)')
,[eligibility_schiz]                          = XMLData.value('(/buy_message/forms//item[@name="eligibility_schiz"])[1]'                           , 'nvarchar(max)')
,[eligibility_drugs]                          = XMLData.value('(/buy_message/forms//item[@name="eligibility_drugs"])[1]'                           , 'nvarchar(max)')
,[eligibility_stroke]                         = XMLData.value('(/buy_message/forms//item[@name="eligibility_stroke"])[1]'                          , 'nvarchar(max)')
,[eligibility_limbs]                          = XMLData.value('(/buy_message/forms//item[@name="eligibility_limbs"])[1]'                           , 'nvarchar(max)')
,[eligibility_kidney]                         = XMLData.value('(/buy_message/forms//item[@name="eligibility_kidney"])[1]'                          , 'nvarchar(max)')
,[eligibility_heart_attack]                   = XMLData.value('(/buy_message/forms//item[@name="eligibility_heart_attack"])[1]'                    , 'nvarchar(max)')


/*<section name="medical-history"><group name="medical-history">*/

,[hiv-conditions]                             =	XMLData.value('(/buy_message/forms//item[@name="hiv-conditions"])[1]'                              , 'nvarchar(max)')
,[hiv-conditions-specify]                     =	XMLData.value('(/buy_message/forms//item[@name="hiv-conditions-specify"])[1]'                      , 'nvarchar(max)')
,[cancer-conditions]                          =	XMLData.value('(/buy_message/forms//item[@name="cancer-conditions"])[1]'                           , 'nvarchar(max)')
,[cancer-conditions-specify]                  =	XMLData.value('(/buy_message/forms//item[@name="cancer-conditions-specify"])[1]'                   , 'nvarchar(max)')
,[heart-conditions]                           =	XMLData.value('(/buy_message/forms//item[@name="heart-conditions"])[1]'                            , 'nvarchar(max)')
,[heart-conditions-specify]                   =	XMLData.value('(/buy_message/forms//item[@name="heart-conditions-specify"])[1]'                    , 'nvarchar(max)')
,[form-of-stroke]                             =	XMLData.value('(/buy_message/forms//item[@name="form-of-stroke"])[1]'                              , 'nvarchar(max)')
,[form-of-stroke-specify]                     =	XMLData.value('(/buy_message/forms//item[@name="form-of-stroke-specify"])[1]'                      , 'nvarchar(max)')
,[sick-leave]                                 =	XMLData.value('(/buy_message/forms//item[@name="sick-leave"])[1]'                                  , 'nvarchar(max)')
,[sick-leave-specify]                         =	XMLData.value('(/buy_message/forms//item[@name="sick-leave-specify"])[1]'                          , 'nvarchar(max)')
,[current-pregnancy]                          =	XMLData.value('(/buy_message/forms//item[@name="current-pregnancy"])[1]'                           , 'nvarchar(max)')
,[last-five-years]                            =	XMLData.value('(/buy_message/forms//item[@name="last-five-years"])[1]'                             , 'nvarchar(max)')
,[non-art-medication]                         =	XMLData.value('(/buy_message/forms//item[@name="non-art-medication"])[1]'                          , 'nvarchar(max)')
,[non-art-medication-specify]                 =	XMLData.value('(/buy_message/forms//item[@name="non-art-medication-specify"])[1]'                  , 'nvarchar(max)')
,[drugs-alcohol]                              =	XMLData.value('(/buy_message/forms//item[@name="drugs-alcohol"])[1]'                               , 'nvarchar(max)')
,[drugs-alcohol-specify]                      =	XMLData.value('(/buy_message/forms//item[@name="drugs-alcohol-specify"])[1]'                       , 'nvarchar(max)')
,[hospitalisation-surgeries]                  =	XMLData.value('(/buy_message/forms//item[@name="hospitalisation-surgeries"])[1]'                   , 'nvarchar(max)')
,[other-disclosures]                          =	XMLData.value('(/buy_message/forms//item[@name="other-disclosures"])[1]'                           , 'nvarchar(max)')
,[other-issues-tests]                         =	XMLData.value('(/buy_message/forms//item[@name="other-issues-tests"])[1]'                          , 'nvarchar(max)')
,[other-issues-tests-specify]                 =	XMLData.value('(/buy_message/forms//item[@name="other-issues-tests-specify"])[1]'                  , 'nvarchar(max)')


/*<section name="occupation-and-pursuits"><group name="pursuits-checkboxes">*/

,[travels]                                    = XMLData.value('(/buy_message/forms//item[@name="travels"])[1]'                                     , 'nvarchar(max)')
,[pursuits-flying]                            =	XMLData.value('(/buy_message/forms//item[@name="pursuits-flying"])[1]'                             , 'nvarchar(max)')
,[pursuits-motorcar]                          =	XMLData.value('(/buy_message/forms//item[@name="pursuits-motorcar"])[1]'                           , 'nvarchar(max)')
,[pursuits-cave]                              =	XMLData.value('(/buy_message/forms//item[@name="pursuits-cave"])[1]'                               , 'nvarchar(max)')
,[pursuits-sailing]                           =	XMLData.value('(/buy_message/forms//item[@name="pursuits-sailing"])[1]'                            , 'nvarchar(max)')
,[pursuits-mountain]                          =	XMLData.value('(/buy_message/forms//item[@name="pursuits-mountain"])[1]'                           , 'nvarchar(max)')
,[pursuits-extremesports]                     =	XMLData.value('(/buy_message/forms//item[@name="pursuits-extremesports"])[1]'                      , 'nvarchar(max)')
,[pursuits-none-of-above]                     =	XMLData.value('(/buy_message/forms//item[@name="pursuits-none-of-above"])[1]'                      , 'nvarchar(max)')


/*<group name="occ-checkboxes">*/

,[occ-underwater]                             =	XMLData.value('(/buy_message/forms//item[@name="occ-underwater"])[1]'                              , 'nvarchar(max)')
,[occ-underground]                            =	XMLData.value('(/buy_message/forms//item[@name="occ-underground"])[1]'                             , 'nvarchar(max)')
,[occ-explosives]                             =	XMLData.value('(/buy_message/forms//item[@name="occ-explosives"])[1]'                              , 'nvarchar(max)')
,[occ-heights]                                =	XMLData.value('(/buy_message/forms//item[@name="occ-heights"])[1]'                                 , 'nvarchar(max)')
,[occ-firearm]                                =	XMLData.value('(/buy_message/forms//item[@name="occ-firearm"])[1]'                                 , 'nvarchar(max)')
,[occ-manual]                                 =	XMLData.value('(/buy_message/forms//item[@name="occ-manual"])[1]'                                  , 'nvarchar(max)')
,[occ-none-of-above]                          =	XMLData.value('(/buy_message/forms//item[@name="occ-none-of-above"])[1]'                           , 'nvarchar(max)')


/*<section name="occupation-and-pursuits">*/

,[job-title]                                  = XMLData.value('(/buy_message/forms//item[@name="job-title"])[1]'                                   , 'nvarchar(max)')

/*<section name="final-cover"><group name="recover">*/

,[life-cover-amount-dq]                       = XMLData.value('(/buy_message/forms//item[@name="life-cover-amount-dq"])[1]'                        , 'nvarchar(max)')
,[life-premium-amount-dq]                     = XMLData.value('(/buy_message/forms//item[@name="life-premium-amount-dq"])[1]'                      , 'nvarchar(max)')
,[disability-conf-dq]                         = XMLData.value('(/buy_message/forms//item[@name="disability-conf-dq"])[1]'                          , 'nvarchar(max)')

/*<section name="final-cover"><group name="pricing">*/

,[plan-fee]                                   = XMLData.value('(/buy_message/forms//item[@name="plan-fee"])[1]'                                    , 'nvarchar(max)')
,[pricing-decision-life-final-premium-no-fee] = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-final-premium-no-fee"])[1]'  , 'nvarchar(max)')
,[pricing-decision-life-cover]                = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-cover"])[1]'                 , 'nvarchar(max)')
,[pricing-decision-life-base-premium]         = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-base-premium"])[1]'          , 'nvarchar(max)')
,[pricing-decision-life-decision-premium]     = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-decision-premium"])[1]'      , 'nvarchar(max)')
,[pricing-decision-life-final-premium]        = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-final-premium"])[1]'         , 'nvarchar(max)')
,[decision-life-outcome]                      = XMLData.value('(/buy_message/forms//item[@name="decision-life-outcome"])[1]'                       , 'nvarchar(max)')
,[pricing-decision-life-quote-id]             = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-quote-id"])[1]'              , 'nvarchar(max)')
,[pricing-decision-life-accept]               = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-life-accept"])[1]'                , 'nvarchar(max)')
,[life-term-age-string]                       = XMLData.value('(/buy_message/forms//item[@name="life-term-age-string"])[1]'                        , 'nvarchar(max)')
,[pricing-decision-bmi]                       = XMLData.value('(/buy_message/forms//item[@name="pricing-decision-bmi"])[1]'                        , 'nvarchar(max)')
,[life-calculated-term-years]                 = XMLData.value('(/buy_message/forms//item[@name="life-calculated-term-years"])[1]'                  , 'nvarchar(max)')

/*<form name="Buy"><section name="personal-details"><group name="personal-details">*/

,[first-name]                                 = XMLData.value('(/buy_message/forms//item[@name="first-name"])[1]'                                  , 'nvarchar(max)')
,[surname]                                    = XMLData.value('(/buy_message/forms//item[@name="surname"])[1]'                                     , 'nvarchar(max)')
,[title]                                      = XMLData.value('(/buy_message/forms//item[@name="title"])[1]'                                       , 'nvarchar(max)')
,[id-number]                                  = XMLData.value('(/buy_message/forms//item[@name="id-number"])[1]'                                   , 'nvarchar(max)')
,[passport-number]                            = XMLData.value('(/buy_message/forms//item[@name="passport-number"])[1]'                       , 'nvarchar(max)')
,[type-of-identification]                     = XMLData.value('(/buy_message/forms//item[@name="type-of-identification"])[1]'                      , 'nvarchar(max)')


/*<section name="contact-details"><group name="contact-details">*/

,[res-address-one]                            = XMLData.value('(/buy_message/forms//item[@name="res-address-one"])[1]'                             , 'nvarchar(max)')
,[res-address-two]                            = XMLData.value('(/buy_message/forms//item[@name="res-address-two"])[1]'                             , 'nvarchar(max)')
,[res-postal-code]                            = XMLData.value('(/buy_message/forms//item[@name="res-postal-code"])[1]'                             , 'nvarchar(max)')
,[cell-number]                                = XMLData.value('(/buy_message/forms//item[@name="cell-number"])[1]'                                 , 'nvarchar(max)')
,[res-address-three]                          = XMLData.value('(/buy_message/forms//item[@name="res-address-three"])[1]'                           , 'nvarchar(max)')
,[email-address]                              = XMLData.value('(/buy_message/forms//item[@name="email-address"])[1]'                               , 'nvarchar(max)')
,[preferred-channel]                          = XMLData.value('(/buy_message/forms//item[@name="preferred-channel"])[1]'                           , 'nvarchar(max)')
,[email-address-check]                        = XMLData.value('(/buy_message/forms//item[@name="email-address-check"])[1]'                         , 'nvarchar(max)')


/*<section name="existing-cover"><group name="existing-cover">*/

,[has-other-cover]                            = XMLData.value('(/buy_message/forms//item[@name="has-other-cover"])[1]'                             , 'nvarchar(max)')
,[cover-amount-one]                            = XMLData.value('(/buy_message/forms//item[@name="cover-amount-one"])[1]'                           , 'nvarchar(max)')
,[cover-amount-two]                            = XMLData.value('(/buy_message/forms//item[@name="cover-amount-two"])[1]'                           , 'nvarchar(max)')

/*<section name="healthcare"><group name="healthcare">*/

,[art-conf]                                   = XMLData.value('(/buy_message/forms//item[@name="art-conf"])[1]'                                    , 'nvarchar(max)')
,[art-start-date]                             = XMLData.value('(/buy_message/forms//item[@name="art-start-date"])[1]'                              , 'nvarchar(max)')
,[art-regime]                                 = XMLData.value('(/buy_message/forms//item[@name="art-regime"])[1]'                                  , 'nvarchar(max)')
,[primary-doctor-name]                        = XMLData.value('(/buy_message/forms//item[@name="primary-doctor-name"])[1]'                         , 'nvarchar(max)')
,[primary-doctor-phone-number]                = XMLData.value('(/buy_message/forms//item[@name="primary-doctor-phone-number"])[1]'                 , 'nvarchar(max)')
,[primary-doctor-city]                        = XMLData.value('(/buy_message/forms//item[@name="primary-doctor-city"])[1]'                         , 'nvarchar(max)')

/*<section name="beneficiaries"><group name="beneficiaries">*/

,[bene-optional]                              = XMLData.value('(/buy_message/forms//item[@name="bene-optional"])[1]'                               , 'nvarchar(max)')
,[fullname-insert]                            = XMLData.value('(/buy_message/forms//item[@name="fullname-insert"])[1]'                             , 'nvarchar(max)')
,[dob-insert]                                 = XMLData.value('(/buy_message/forms//item[@name="dob-insert"])[1]'                                  , 'nvarchar(max)')
,[id-number-insert]                           = XMLData.value('(/buy_message/forms//item[@name="id-number-insert"])[1]'                            , 'nvarchar(max)')
,[percentage-insert]                          = XMLData.value('(/buy_message/forms//item[@name="percentage-insert"])[1]'                           , 'nvarchar(max)')
,[existing-beneficiary-list]                  = XMLData.value('(/buy_message/forms//item[@name="existing-beneficiary-list"])[1]'                   , 'nvarchar(max)')
,P_POLICYBENEF1   = Null
,P_BENFDOB1       = Null
,P_BENEFID1       = Null
,P_BENEFSHARE1    = Null
,P_POLICYBENEF2   = Null
,P_BENFDOB2       = Null
,P_BENEFID2       = Null
,P_BENEFSHARE2    = Null
,P_POLICYBENEF3   = Null
,P_BENFDOB3       = Null
,P_BENEFID3       = Null
,P_BENEFSHARE3    = Null
,P_POLICYBENEF4   = Null
,P_BENFDOB4       = Null
,P_BENEFID4       = Null
,P_BENEFSHARE4    = Null
,P_POLICYBENEF5   = Null
,P_BENFDOB5       = Null
,P_BENEFID5       = Null
,P_BENEFSHARE5    = Null
,P_POLICYBENEF6   = Null
,P_BENFDOB6       = Null
,P_BENEFID6       = Null
,P_BENEFSHARE6    = Null
,P_POLICYBENEF7   = Null
,P_BENFDOB7       = Null
,P_BENEFID7       = Null
,P_BENEFSHARE7    = Null
,P_POLICYBENEF8   = Null
,P_BENFDOB8       = Null
,P_BENEFID8       = Null
,P_BENEFSHARE8    = Null
,P_POLICYBENEF9   = Null
,P_BENFDOB9       = Null
,P_BENEFID9       = Null
,P_BENEFSHARE9    = Null
,P_POLICYBENEF10  = Null
,P_BENFDOB10      = Null
,P_BENEFID10      = Null
,P_BENEFSHARE10   = Null

/*section name="payment-details"><group name="payment-details">*/

,[payer-intro]                                = XMLData.value('(/buy_message/forms//item[@name="payer-intro"])[1]'                                 , 'nvarchar(max)')
,[bank-name]                                  = XMLData.value('(/buy_message/forms//item[@name="bank-name"])[1]'                                   , 'nvarchar(max)')
,[account-holder-name]                        = XMLData.value('(/buy_message/forms//item[@name="account-holder-name"])[1]'                         , 'nvarchar(max)')
,[account-branch-code]                        = XMLData.value('(/buy_message/forms//item[@name="account-branch-code"])[1]'                         , 'nvarchar(max)')
,[account-number]                             = XMLData.value('(/buy_message/forms//item[@name="account-number"])[1]'                              , 'nvarchar(max)')
,[account-type]                               = XMLData.value('(/buy_message/forms//item[@name="account-type"])[1]'                                , 'nvarchar(max)')
,[first-debit-date]                          = XMLData.value('(/buy_message/forms//item[@name="first-debit-date"])[1]'                              , 'nvarchar(max)')
,[debit-order-day]                            = XMLData.value('(/buy_message/forms//item[@name="debit-order-day"])[1]'                             , 'nvarchar(max)')
,[consent-to-debit]                           = XMLData.value('(/buy_message/forms//item[@name="consent-to-debit"])[1]'                            , 'nvarchar(max)')

/*<section name="terms-and-conditions"><group name="terms-and-conditions">*/

,[ts-and-cs-agree]                            = XMLData.value('(/buy_message/forms//item[@name="ts-and-cs-agree"])[1]'                             , 'nvarchar(max)')
,- 1 iImportRecNo
,getdate() as Processdate
from [IncomingXMLstore]
where  [FileName]  not in (select [policy-reference] from [DailyStaging_ETL_XML])
and [Outcome] <> 'Refer'

end
GO


