--USE [KalibreSA_SkiImport ]
--GO



CREATE TABLE [dbo].[SKI_Customer_Fields](
	[iCustomerID] [float] NULL,
	[iAgentID] [float] NULL,
	[sLastName] [varchar](30) NULL,
	[sFirstName] [varchar](30) NULL,
	[sInitials] [varchar](8) NULL,
	[dDateCreated] [datetime] NULL,
	[dLastUpdated] [datetime] NULL,
	[bReplicated] [int] NULL,
	[bCompanyInd] [bit] NULL,
	[sUpdatedBy] [varchar](20) NULL,
	[rPostAddressID] [float] NULL,
	[rPhysAddressID] [float] NULL,
	[rWorkAddressID] [float] NULL,
	[sCompanyName] [varchar](255) NULL,
	[sContactName] [varchar](80) NULL,
	[sUserName] [varchar](100) NULL,
	[sPassword] [varchar](64) NULL,
	[iTranID] [numeric](18, 0) NULL,
	[dValidTill] [datetime] NULL,
	[sDocDir] [varchar](255) NULL,
	[iGroupID] [float] NULL,
	[sGroupRelationShip] [varchar](15) NULL,
	[sLanguage] [varchar](15) NULL,
	[bITCCheckAuthorized] [bit] NULL,
	[AdminUser] [varchar](20) NULL,
	[CorporateID] [uniqueidentifier] NULL,
	[CountryId] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


