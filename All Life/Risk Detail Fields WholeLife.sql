--USE [KalibreSA_SkiImport ]
--GO



CREATE TABLE [dbo].[SKI_Risk_Detail_Fields_WholeLife](
    [R_ClientID]	float not null,
    [R_INSVALUE] [varchar](150) NULL,
	[R_ANNUPREMESCAL] [varchar](150) NULL,
	[R_UWUPDATE] [varchar](150) NULL,
	[R_UWREASON] [varchar](150) NULL,
	[R_UWDECISION] [varchar](150) NULL,
	[R_UWDCISIONDATE] [varchar](150) NULL,
	[R_INITUWDECSION] [varchar](150) NULL,
	[R_INITUWDATE] [varchar](150) NULL,
	[R_AVTG_BMILOAD] [varchar](150) NULL,
	[R_AVTG_OCCLOAD] [varchar](150) NULL,
	[R_PREMIUM] [varchar](150) NULL,
	[R_INSUREDVALUE] [varchar](150) NULL,
	[R_CD4COUNT] [varchar](150) NULL,
	[R_VIRALLOADATQU] [varchar](150) NULL,
	[R_ELCOVERAMOUNT] [varchar](150) NULL,
	[R_CD4QUOTEDATE] [varchar](150) NULL,
	[R_QUOTEDATE] [varchar](150) NULL,
	[R_AGERATED] [varchar](150) NULL,
	[R_ELESCFACTOR] [varchar](150) NULL,
	[R_CLASS1LIFE] [varchar](150) NULL,
	[R_DATEOFBIRTH] [varchar](150) NULL,
	[R_ELCOVERESC] [varchar](150) NULL,
	[R_INCEPTIONDATE] [varchar](150) NULL,
	[R_CTDEFAULT] [varchar](150) NULL,
	[R_AVTG_LIFEDIBE] [varchar](150) NULL,
	[R_DISABILITY] [varchar](150) NULL,
	[R_TYPEOFDISABIL] [varchar](150) NULL,
	[R_BROKERCOMM] [varchar](150) NULL,
	[R_DIABETESTYPE] [varchar](150) NULL,
	[R_DIAGNOSISYEAR] [varchar](150) NULL,
	[R_SMOKERSTATUS] [varchar](150) NULL,
	[R_ELLIFECLASS] [varchar](150) NULL,
	[R_ELGENDER] [varchar](150) NULL,
	[R_ELLOADING] [varchar](150) NULL,
	[R_OCCLOADING] [varchar](150) NULL,
	[R_ORCOVERAMOUNT] [varchar](150) NULL,
	[R_PREMESCALATIO] [varchar](150) NULL,
	[R_DM_LIFECLASS] [varchar](150) NULL,
	[R_PREM_ESCAL] [varchar](150) NULL,
	[R_START_CTRLBND] [varchar](150) NULL,
	[R_UW_CTRLBAND] [varchar](150) NULL,
	[R_COMM] [varchar](150) NULL,
	[R_EFFECTIVECVR] [varchar](150) NULL,
	[R_VIF] [varchar](150) NULL,
	[R_PWSID] [varchar](150) NULL,
	[R_AGEATQUOTE] [varchar](150) NULL,
	[R_UWHEIGHT] [varchar](150) NULL,
	[R_UWWEIGHT] [varchar](150) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


