--USE [KalibreSA_SkiImport ]
--GO


CREATE TABLE [dbo].[SKI_Bank_Acct_Fields](
	[iBankingDetailsID] [Varchar](30) NULL,
	[sBank] [varchar](50) NULL,
	[sAccHolderName] [varchar](50) NULL,
	[sAccountNumber] [varchar](16) NULL,
	[sAccountType] [varchar](15) NULL,
	[sBranchCode] [varchar](10) NULL,
	[iPaymentMethod] [varchar](30) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


