Create Proc SKI_INT.PostUpdate
AS

UPDATE 
	Risks 
SET 
	cPremium = 0.01, cAnnualPremium = 0.01 
WHERE 
	cPremium = 0;

UPDATE 
	Risks 
SET cAnnualPremium = 0.01 
WHERE 
	cAnnualPremium = 0 
	and cPremium <> 0;

UPDATE 
	Risks 
SET 
	cAnnualComm = cCommission * 12;

UPDATE p
	SET
	 p.cNextPaymentAmt = ISNULL(r.cPremium,0)
	,p.cAnnualPremium  = ISNULL(r.cAnnualPremium,0)
	,p.cCommissionAmt  = ISNULL(r.cCommissionAmt,0)
	,p.cAnnualSasria   = ISNULL(r.cAnnualSasria,0)
	--select P.iPolicyID, p.cAnnualPremium, r.cAnnualPremium, r.cpremium, r.cAnnualSasria, p.cannualpremium, p.cNextPaymentAmt,p.cAnnualSasria,r.cCommissionAmt,p.cCommissionAmt, *
	FROM dbo.Policy p LEFT JOIN (
								SELECT intClientid,intPolicyid,iProductid--, intRiskID
								,SUM(cPremium) cPremium
								,SUM(cAnnualPremium) cAnnualPremium
								,SUM(cAnnualSasria)  cAnnualSasria
								,SUM(cAnnualComm)    cCommissionAmt
								FROM dbo.Risks
								WHERE intStatus = 2
								--WHERE intPolicyID = 717969
								GROUP BY  intClientid,intPolicyid,iProductid--, intRiskID
							) r ON r.intPolicyid = p.iPolicyid ;
	--WHERE P.sPolicyNo = 'EQ000006000'

update 
	PolicyCoInsurerFees 
SET 
	cAdminFeePerc = 0
	, cPolicyFeePerc = 0
	, cBrokerFeePerc = 0 
WHERE  
	sCoInsurer <> 'TC001';
--SELECT 
--	P.cAdminFee, R.cAdminFee, R.cAnnualAdminFee
UPDATE
	P SET cAdminFee = R.cAnnualAdminFee
FROM
	Policy P
	JOIN (
		SELECT intPolicyID, SUM(cAnnualAdminFee) as cAnnualAdminFee
		FROM Risks 
		GROUP BY intPolicyID) R ON R.intPolicyID = P.iPolicyID
where 
	(P.cAdminMin <> R.cAnnualAdminFee
	AND P.cAdminFee = 0);