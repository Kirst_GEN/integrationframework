USE Equipsme_Website_UAT
GO

DECLARE		@SP_Definition		NVARCHAR (MAX) 
DECLARE		@SP					VARCHAR (100) 	
DECLARE		@SP_Bak				VARCHAR (100)
DECLARE		@DatabaseName		VARCHAR (100)='Equipsme_Website_UAT' 
DECLARE		@SchemaName			VARCHAR (100)='dbo' 
DECLARE		@PROCName			VARCHAR (100)='Populate_Staging_From_SalesApp'

DECLARE		BackupStoredProcedure CURSOR FOR
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT		CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',pr.name,']') AS 'SP',
			CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',pr.name, '_', CONVERT(VARCHAR(10),modify_date,112), '_', REPLACE(CONVERT(VARCHAR(10),modify_date,8),':',''),']') AS 'SP_Bak',
			REPLACE(
						mod.definition,
						CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',pr.name,']'),
						CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',pr.name, '_', CONVERT(VARCHAR(10),modify_date,112), '_', REPLACE(CONVERT(VARCHAR(10),modify_date,8),':',''),']')
					) AS 'SP_Definition'
FROM		sys.procedures		pr 
INNER JOIN	sys.sql_modules		mod ON pr.object_id = mod.object_id 
WHERE		pr.Is_MS_Shipped = 0 
			AND pr.name IN (@PROCName)
			AND pr.schema_id IN (SCHEMA_ID(@SchemaName))
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
OPEN		BackupStoredProcedure 
FETCH		NEXT 
FROM		BackupStoredProcedure 
INTO		@SP, @SP_Bak, @SP_Definition
-----------------------------------------------------------------------
WHILE		@@FETCH_STATUS = 0  
BEGIN 
			SET		@SP_Definition = REPLACE(@SP_Definition,'''','''''') 
			SET		@SP_Definition = 'USE [' + @DatabaseName + ']; EXEC(''' + @SP_Definition + ''')' 
 	 
			IF OBJECT_ID(@sp_bak) IS NULL
			BEGIN
				EXEC	(@SP_Definition)

				IF OBJECT_ID(@sp_bak) IS NULL
				BEGIN
					PRINT CONCAT('ERROR! Backup of Stored Procedure ',@SP,' has not completed successfully. DO NOT PROCESS ANY UPDATES!')
				END
				ELSE
				BEGIN
					PRINT CONCAT('SUCCESS! Backup of Stored Procedure ',@SP,' completed as: ', @SP_Bak)
				END
			END
			ELSE
			BEGIN
				PRINT CONCAT('NO CHANGE! Stored Procedure ', @SP,' has not been modifed since previous backup and is currently saved as: ', @SP_Bak)
			END
 
			FETCH NEXT FROM BackupStoredProcedure INTO @SP, @SP_Bak, @SP_Definition
END              
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CLOSE		BackupStoredProcedure 
DEALLOCATE	BackupStoredProcedure 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

