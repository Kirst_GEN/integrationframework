USE Equipsme_Website_UAT
GO
---------------------------------------------------------------------------------------------------------------------------
--IN ORDER TO EXECUTE A ROLLBACK, THE FOLLOWING VARIABLE NEEDS TO BE SET TO 1
--THIS IS TO PREVENT ACCIDENTAL EXECUTION OF A ROLLBACK
---------------------------------------------------------------------------------------------------------------------------
DECLARE		@ROLLBACK_REQUIRED  INT
SET			@ROLLBACK_REQUIRED  = 0
---------------------------------------------------------------------------------------------------------------------------

IF @ROLLBACK_REQUIRED = 1
BEGIN
	DECLARE		@SP_Definition		NVARCHAR (MAX) 
	DECLARE		@SP					VARCHAR (100) 	
	DECLARE		@SP_Bak				VARCHAR (100)
	DECLARE		@DatabaseName		VARCHAR (100)='Equipsme_Website_UAT' 
	DECLARE		@SchemaName			VARCHAR (100)='dbo' 
	DECLARE		@PROCName			VARCHAR (100)='Populate_Staging_From_SalesApp'
	DECLARE		@BackupEntityName	VARCHAR (50)

	SET @BackupEntityName = (
							SELECT TOP 1 
								name 
							FROM 
								Sys.procedures 
							WHERE 
								Name Like @PROCName + '_%'
							ORDER BY 
								create_date DESC
							)

	DECLARE		BackupStoredProcedure CURSOR FOR
	---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT		CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',@BackupEntityName,']') AS 'SP',
				CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',@PROCName,']') AS 'SP_Bak',
				REPLACE(
							REPLACE(mod.definition,'CREATE PROC','ALTER PROC'),
								CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',@BackupEntityName,']'),
								CONCAT('[',SCHEMA_NAME(pr.schema_id),'].[',@PROCName,']')
						) AS 'SP_Definition'
	FROM		sys.procedures		pr 
	INNER JOIN	sys.sql_modules		mod ON pr.object_id = mod.object_id 
	WHERE		pr.Is_MS_Shipped = 0 
				AND pr.name IN (@BackupEntityName)
				AND pr.schema_id IN (SCHEMA_ID(@SchemaName))
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
	OPEN		BackupStoredProcedure 
	FETCH		NEXT 
	FROM		BackupStoredProcedure 
	INTO		@SP, @SP_Bak, @SP_Definition
	-----------------------------------------------------------------------
	WHILE		@@FETCH_STATUS = 0  
	BEGIN 
				SET		@SP_Definition = REPLACE(@SP_Definition,'''','''''') 
				SET		@SP_Definition = 'USE [' + @DatabaseName + ']; EXEC(''' + @SP_Definition + ''')' 
 	 
				IF OBJECT_ID(@sp_bak) IS NOT NULL
				BEGIN
					EXEC	(@SP_Definition)

					IF OBJECT_ID(@sp_bak) IS NULL
					BEGIN
						PRINT CONCAT('ERROR! ROLLBACK of Stored Procedure ',@SP,' has not completed successfully. PLEASE CONTACT GENASYS!')
					END
					ELSE
					BEGIN
						PRINT CONCAT('SUCCESS! ROLLBACK of Stored Procedure ',@SP,' completed as: ', @SP_Bak)
					END
				END
				ELSE
				BEGIN
					PRINT CONCAT('NO CHANGE! Stored Procedure ', @SP,' has not been rolled back and is currently saved as: ', @SP_Bak, 'PLEASE CONATCT GENASYS!')
				END
 
				FETCH NEXT FROM BackupStoredProcedure INTO @SP, @SP_Bak, @SP_Definition
	END              
	-----------------------------------------------------------------------
	CLOSE		BackupStoredProcedure 
	DEALLOCATE	BackupStoredProcedure 
	---------------------------------------------------------------------------------------------------------------------------------------------------------------------
END
ELSE
BEGIN
	PRINT CONCAT('WARNING! ROLLBACK NOT PERFORMED AS THE @ROLLBACK_REQUIRED INDICATOR WAS SET TO FALSE!', CHAR(10),'*************************************************************************************', CHAR(10), 'PLEASE REVIEW IMPLEMENTATION PLAN IN ORDER TO EXECUTE THE ROLLBACK')
END