
--select iProductID, * from policy where spolicyno = 'EQ000027000'

select 
	intRiskID, intRiskTypeID, L.sDescription, 
	strDescription, L.sDescription, cPremium, cAdminFee
	, DBO.GetCommCalcPremium(RD_R_EEQUIPSMARGIN.sFieldValue) / 12 AS [Esme Margin INCL]
	, (DBO.GetCommCalcPremium(RD_R_EEQUIPSMARGIN.sFieldValue) / 12) - (DBO.GetCommCalcPremium(RD_R_EEQUIPSMARGIN.sFieldValue) / 12) * .15 AS [Esme Margin]
	, L_R_SMEDEPECOVE.sDescription AS EMPLOYEE_COVER
	, L_R_SMELEVEL.sDescription AS EMPLOYEE_LEVEL
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL ONE)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	, RD_R_LEVEONENUMEMP.iFieldValue AS LEVEL1_NO_OF_EMPLOYEES
	, ((DBO.GetCommCalcPremium(RD_R_LEVELONEPREM.sFieldValue) / 12) / RD_R_LEVEONENUMEMP.iFieldValue) - ((DBO.GetCommCalcPremium(RD_R_LEVELONEPREM.sFieldValue) / 12) / RD_R_LEVEONENUMEMP.iFieldValue) * .15 AS [ONE Axa Ins + Margin in �s]
	, ((DBO.GetCommCalcPremium(RD_R_LEVELONEPREM.sFieldValue) / 12) / RD_R_LEVEONENUMEMP.iFieldValue) * .15 AS [ONE Axa Ins + Margin in �s COMM]
	, (DBO.GetCommCalcPremium(RD_R_LEVELONEPREM.sFieldValue) / 12) / RD_R_LEVEONENUMEMP.iFieldValue AS [ONE Axa Ins + Margin in �s INCL]
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL TWO)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	, RD_R_LEVETWONUMEMP.iFieldValue AS LEVEL2_NO_OF_EMPLOYEES
	, ((DBO.GetCommCalcPremium(RD_R_LEVELTWOPREM.sFieldValue) / 12) / RD_R_LEVETWONUMEMP.iFieldValue) - ((DBO.GetCommCalcPremium(RD_R_LEVELTWOPREM.sFieldValue) / 12) / RD_R_LEVETWONUMEMP.iFieldValue) * .15 AS [TWO Axa Ins + Margin in �s]
	, ((DBO.GetCommCalcPremium(RD_R_LEVELTWOPREM.sFieldValue) / 12) / RD_R_LEVETWONUMEMP.iFieldValue) * .15 AS [TWO Axa Ins + Margin in �s COMM]
	, (DBO.GetCommCalcPremium(RD_R_LEVELTWOPREM.sFieldValue) / 12) / RD_R_LEVETWONUMEMP.iFieldValue AS [TWO Axa Ins + Margin in �s INCL]
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL THREE)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	, RD_R_LEVETHRNUMEMP.iFieldValue AS LEVEL3_NO_OF_EMPLOYEES
	, ((DBO.GetCommCalcPremium(RD_R_LEVELTHREPREM.sFieldValue) / 12) / RD_R_LEVETHRNUMEMP.iFieldValue) - ((DBO.GetCommCalcPremium(RD_R_LEVELTHREPREM.sFieldValue) / 12) / RD_R_LEVETHRNUMEMP.iFieldValue) * .15 AS [THREE Axa Ins + Margin in �s]
	, ((DBO.GetCommCalcPremium(RD_R_LEVELTHREPREM.sFieldValue) / 12) / RD_R_LEVETHRNUMEMP.iFieldValue) * .15 AS [THREE Axa Ins + Margin in �s COMM]
	, (DBO.GetCommCalcPremium(RD_R_LEVELTHREPREM.sFieldValue) / 12) / RD_R_LEVETHRNUMEMP.iFieldValue AS [THREE Axa Ins + Margin in �s INCL]
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL GP+)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	, RD_R_LEVEFOUNUMEMP.iFieldValue AS LEVEL4_NO_OF_EMPLOYEES
	, ((DBO.GetCommCalcPremium(RD_R_LEVELFOURPREM.sFieldValue) / 12) / RD_R_LEVEFOUNUMEMP.iFieldValue) - ((DBO.GetCommCalcPremium(RD_R_LEVELFOURPREM.sFieldValue) / 12) / RD_R_LEVEFOUNUMEMP.iFieldValue) * .15 AS [FOUR Axa Ins + Margin in �s]
	, ((DBO.GetCommCalcPremium(RD_R_LEVELFOURPREM.sFieldValue) / 12) / RD_R_LEVEFOUNUMEMP.iFieldValue) * .15 AS [FOUR Axa Ins + Margin in �s COMM]
	, (DBO.GetCommCalcPremium(RD_R_LEVELFOURPREM.sFieldValue) / 12) / RD_R_LEVETHRNUMEMP.iFieldValue AS [FOUR Axa Ins + Margin in �s INCL]
	, * 
from 
	Risks R
	JOIN Lookup L WITH (NOEXPAND NOLOCK) ON L.iindex = intRiskTypeID and L.sGroup = 'RiskType'
	LEFT JOIN RiskDetails RD_R_EEQUIPSMARGIN (NOLOCK) ON RD_R_EEQUIPSMARGIN.iRiskID = R.intRiskID AND RD_R_EEQUIPSMARGIN.sFieldCode = 'R_EEQUIPSMARGIN'
	LEFT JOIN RiskDetails RD_R_SMEDEPECOVE (NOLOCK) ON RD_R_SMEDEPECOVE.iRiskID = R.intRiskID AND RD_R_SMEDEPECOVE.sFieldCode = 'R_SMEDEPECOVE'
	LEFT join Lookup L_R_SMEDEPECOVE WITH (NOEXPAND NOLOCK) ON L_R_SMEDEPECOVE.sDBValue = RD_R_SMEDEPECOVE.sFieldValue AND L_R_SMEDEPECOVE.SGROUP = 'EquipsmeCoverStatus'
	LEFT JOIN RiskDetails RD_R_SMELEVEL (NOLOCK) ON RD_R_SMELEVEL.iRiskID = R.intRiskID AND RD_R_SMELEVEL.sFieldCode = 'R_SMELEVEL'
	LEFT join Lookup L_R_SMELEVEL WITH (NOEXPAND NOLOCK) ON L_R_SMELEVEL.sDBValue = RD_R_SMELEVEL.sFieldValue AND L_R_SMELEVEL.SGROUP = 'EquipsmeLevel'
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL ONE)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	LEFT JOIN RiskDetails RD_R_LEVEONENUMEMP (NOLOCK) ON RD_R_LEVEONENUMEMP.iRiskID = R.intRiskID AND RD_R_LEVEONENUMEMP.sFieldCode = 'R_LEVEONENUMEMP'
	LEFT JOIN RiskDetails RD_R_LEVELONEPREM (NOLOCK) ON RD_R_LEVELONEPREM.iRiskID = R.intRiskID AND RD_R_LEVELONEPREM.sFieldCode = 'R_LEVELONEPREM'
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL TWO)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	LEFT JOIN RiskDetails RD_R_LEVETWONUMEMP (NOLOCK) ON RD_R_LEVETWONUMEMP.iRiskID = R.intRiskID AND RD_R_LEVETWONUMEMP.sFieldCode = 'R_LEVETWONUMEMP'
	LEFT JOIN RiskDetails RD_R_LEVELTWOPREM (NOLOCK) ON RD_R_LEVELTWOPREM.iRiskID = R.intRiskID AND RD_R_LEVELTWOPREM.sFieldCode = 'R_LEVELTWOPREM'
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (LEVEL THREE)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	LEFT JOIN RiskDetails RD_R_LEVETHRNUMEMP (NOLOCK) ON RD_R_LEVETHRNUMEMP.iRiskID = R.intRiskID AND RD_R_LEVETHRNUMEMP.sFieldCode = 'R_LEVETHRNUMEMP'
	LEFT JOIN RiskDetails RD_R_LEVELTHREPREM (NOLOCK) ON RD_R_LEVELTHREPREM.iRiskID = R.intRiskID AND RD_R_LEVELTHREPREM.sFieldCode = 'R_LEVELTHREPREM'
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MAIN COVER RISKTYPE (GP+)
	-----------------------------------------------------------------------------------------------------------------------------------------------
	LEFT JOIN RiskDetails RD_R_LEVEFOUNUMEMP (NOLOCK) ON RD_R_LEVEFOUNUMEMP.iRiskID = R.intRiskID AND RD_R_LEVEFOUNUMEMP.sFieldCode = 'R_LEVEFOUNUMEMP'
	LEFT JOIN RiskDetails RD_R_LEVELFOURPREM (NOLOCK) ON RD_R_LEVELFOURPREM.iRiskID = R.intRiskID AND RD_R_LEVELFOURPREM.sFieldCode = 'R_LEVELFOURPREM'
	-----------------------------------------------------------------------------------------------------------------------------------------------
	--MEDICAL SOLUTIONS
	-----------------------------------------------------------------------------------------------------------------------------------------------
	LEFT JOIN RiskDetails RD_R_MEDICSOLUPREM (NOLOCK) ON RD_R_LEVEFOUNUMEMP.iRiskID = R.intRiskID AND RD_R_LEVEFOUNUMEMP.sFieldCode = 'R_MEDICSOLUPREM'
	
WHERE 
	intPolicyID = 352

