
/****** Object:  Table [SKI_INT].[BatchUser]    Script Date: 03/04/2018 05:53:59 PM ******/
CREATE TABLE [SKI_INT].[BatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]
GO

/****** Object:  Table [SKI_INT].[DailyImportExceptions]    Script Date: 03/04/2018 05:53:59 PM ******/
CREATE TABLE [SKI_INT].[DailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]
GO

CREATE TABLE [SKI_INT].[SSIS_Src_RawData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NOT NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL,
 CONSTRAINT [PK_SSIS_Src_RawData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSIS_Src_RawData_Log](
	[ID] [int] NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISBatchUser](
	[uID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](100) NULL
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationId] [int] NOT NULL,
	[CommunicationTypeId] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[InAttachment] [bit] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISCommunicationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISDailyImportExceptions](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISDailyImportExceptions_Log](
	[ImportRecordiID] [bigint] NULL,
	[ImportFileName] [varchar](255) NULL,
	[ImportRecordLineNumber] [bigint] NULL,
	[Country] [varchar](10) NULL,
	[DateImported] [datetime] NULL,
	[SourceRecord] [char](4000) NULL,
	[ExceptionField] [varchar](255) NULL,
	[ExceptionFieldValue] [varchar](255) NULL,
	[ExceptionReason] [varchar](255) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISDataErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorSource] [nvarchar](255) NOT NULL,
	[SourceRecord] [nvarchar](255) NOT NULL,
	[ErrorNumber] [nvarchar](100) NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISEmailConfig](
	[uID] [int] NULL,
	[MailServer] [varbinary](100) NOT NULL,
	[MailPort] [int] NOT NULL,
	[MailUserName] [varchar](100) NOT NULL,
	[MailPassword] [varbinary](100) NOT NULL
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISFileLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [varchar](100) NULL,
	[FileName] [varchar](100) NULL,
	[FileType] [int] NULL,
	[FileHasHeaderRow] [bit] NULL,
	[FileHasFooterRow] [bit] NULL,
	[PickupLocation] [varchar](255) NULL,
	[ProcessedLocation] [varchar](255) NULL,
	[ArchiveLocation] [varchar](255) NULL,
	[LogFileLocation] [varchar](255) NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISFileType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](10) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISImportLog](
	[ImportLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileRecordCount] [int] NOT NULL,
	[InsertCount] [int] NOT NULL,
	[UpdateCount] [int] NOT NULL,
	[CancelCount] [int] NOT NULL,
	[RejectCount] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISIntegrationLookupMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[SourceGroup] [varchar](50) NOT NULL,
	[SourceDBValue] [varchar](255) NOT NULL,
	[TargetGroup] [varchar](255) NOT NULL,
	[TargetDBValue] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CL_SSISLookupMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISJob](
	[SSISJobID] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [varchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISJob] PRIMARY KEY CLUSTERED 
(
	[SSISJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISMailsToBeSent](
	[MailID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLogID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[MailFrom] [varchar](200) NULL,
	[MailTO] [varchar](400) NULL,
	[MailCC] [varchar](200) NULL,
	[MailBCC] [varchar](200) NULL,
	[MailSubject] [varchar](200) NULL,
	[MailBody] [varchar](max) NULL,
	[DateToSend] [date] NULL,
	[Sent] [bit] NULL,
	[DateSent] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISPackageLog](
	[PackageLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NULL,
 CONSTRAINT [PK_SSISPackageLog] PRIMARY KEY CLUSTERED 
(
	[PackageLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISProcess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](400) NULL,
	[ProcessTypeId] [int] NULL,
	[CommunicationId] [int] NULL,
	[FileLocationId] [int] NULL,
	[ScriptId] [int] NULL,
	[SSISDBCatalogFolderName] [varchar](100) NULL,
	[SSISDBCatalogProjectName] [varchar](100) NULL,
	[SSISDBCatalogPackageName] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [SKI_INT].[SSISProcessType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ScriptId] [int] NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISProgressLog](
	[ProgressLogID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NOT NULL,
	[ProcessName] [nvarchar](255) NOT NULL,
	[JobReference] [nvarchar](100) NOT NULL,
	[PackageName] [nvarchar](100) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[ContinueNextStep] [bit] NULL,
	[Message] [nvarchar](255) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Success] [bit] NOT NULL,
 CONSTRAINT [PK_SSISProgressLog] PRIMARY KEY CLUSTERED 
(
	[ProgressLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISScript](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScriptId] [int] NOT NULL,
	[ScriptTypeId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [SKI_INT].[SSISScriptType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[UserLastUpdated] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [SKI_INT].[DailyImportExceptions] ADD  CONSTRAINT [Imported_D_0]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [SKI_INT].[SSISDailyImportExceptions] ADD  CONSTRAINT [Imported_D_SSISDailyImportExceptions]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [SKI_INT].[SSISJob] ADD  CONSTRAINT [DF_SSISJobID_Success]  DEFAULT ((0)) FOR [Success]
GO

ALTER TABLE [SKI_INT].[SSISProgressLog] ADD  CONSTRAINT [DF_ProgressLog_Success]  DEFAULT ((0)) FOR [Success]
GO


