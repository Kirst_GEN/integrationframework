
ALTER FUNCTION [ski_int].[RemoveSpecialChars](@StringToValidate AS VARCHAR(4000))
RETURNS VARCHAR(4000)
AS
BEGIN 

/*########################################################################################################################
THE FOLLOWING sql WILL ALLOW YOU TO IDENTIFY SPECIAL CHARACTERS TO REMOVE OR VALIDATE
SET THE @STRING VARIABLE TO BE THE TEXT YOU WANT TO TEST
##########################################################################################################################
			DECLARE @String_Position int, @String varchar (max)
			-- Initialize the current position and the string variables.
			SET @String_Position = 1

			SET @String = (Select strSuburb from Address where intAddressID = 354263)
			SET @String = ''

			WHILE @String_Position <= DATALENGTH(@String)
			   BEGIN
			   SELECT ASCII(SUBSTRING(@String, @String_Position, 1)), 
				  CHAR(ASCII(SUBSTRING(@String, @String_Position, 1)))
			   SET @String_Position = @String_Position + 1
			   END
			GO
########################################################################################################################*/

		RETURN 
			(
			replace(replace(replace(@StringToValidate, char(131),''),char(160),' '), char(194),'')
			)

END
