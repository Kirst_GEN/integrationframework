/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID]    Script Date: 28/09/2017 6:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [ski_int].[SBAB_GetPolicyID](@CIFNumber VARCHAR(30), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
		
				SELECT 
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					TOP 1 P.iPolicyID
				FROM 
					Customer c
				INNER JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
				INNER JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = 'C_CUSTOMNUMBER'
				
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumber
					AND P.iProductID = @ProductID
			
		)
END




SELECT 
	P.iPolicyID
FROM 
	Customer c
JOIN 
	policy p 
		ON p.iCustomerID = c.iCustomerID
JOIN 
	CustomerDetails CD 
		ON CD.iCustomerID = C.iCustomerID 
		AND CD.sFieldCode = 'C_CUSTOMNUMBER'
JOIN CustomerDetails CDF 
	ON CDF.iCustomerID = P.iCustomerID
	AND CDF.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_SRC.STIPremium PR
	ON 
		sOldPolicyNo = PR.sNewLoanAccNo 
		AND cd.sFieldValue = PR.sCIF_Number
		AND P.iProductID = PR.PH_ProductID
		AND CDF.sFieldValue = PR.sCountry_Cif
JOIN PolicyDetails PD
	ON P.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue



