--------------------------------------------------------------------------------------------------
--Clear Data before Updating Config
--------------------------------------------------------------------------------------------------
DELETE FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'CoInsurer_STI'

--------------------------------------------------------------------------------------------------
--Insert config
--------------------------------------------------------------------------------------------------
INSERT INTO [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','CoInsurer_STI','CoInsurer','NA','CoInsurer_NAM','HOLLARDNAM')
		
Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_STI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

/*
SELECT * FROM [ski_int].[IntegrationLookupMap]
WHERE Type = 'CoInsurer_STI'
ORDER BY 7
*/