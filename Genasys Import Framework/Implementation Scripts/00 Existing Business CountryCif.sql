IF Object_ID('dbo.CTOPROJ_76_K','U') is null
Begin
Create table dbo.CTOPROJ_76_K
(
 UpdateCustID Bigint not null,
 UpdateFielV varchar(250),
 BackupFielV varchar(250),
)
END

go

DISABLE TRIGGER [dbo].[Cust_insert] ON [dbo].[CustomerDetails]
go

Update CD
	set CD.sFieldValue = CONCAT(CE.sCode,'_',CD2.sFieldValue)
--Select 
--	C.iCustomerID
--	,CNT.sRelName
--	,CE.sCode
--	,CD.sFieldValue
--	,CD2.sFieldValue
--	,CONCAT(CE.sCode,'_',CD2.sFieldValue)
Output 
	Deleted.iCustomerID,
	Deleted.sFieldValue,
	Inserted.sFieldValue
into 
	dbo.CTOPROJ_76_K
from 
	Customer C
	join CustomerDetails CD on CD.iCustomerID = C.iCustomerID and CD.sFieldCode = 'C_COUNTRYCIF' 
	join CustomerDetails CD2 on CD2.iCustomerID = C.iCustomerID and CD2.sFieldCode = 'C_CUSTOMNUMBER'
    join (CommStructEntityRel CNT 
		 Join CommEntities CE on CE.iCommEntityID = CNT.iLev1EntityID and CE.slevelType = 'Lev1'
		 ) on CNT.iCommRelID = C.iAgentID
		 AND ISNULL(sRelName,'') <> ''
where 
	CONCAT(CE.sCode,'_',CD2.sFieldValue) <> CD2.sFieldValue
go

Enable TRIGGER [dbo].[Cust_insert] ON [dbo].[CustomerDetails]
go




