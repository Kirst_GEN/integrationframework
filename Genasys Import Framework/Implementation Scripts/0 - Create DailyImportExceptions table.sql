--drop table SKI_INT.DailyImportExceptions

CREATE Table SKI_INT.DailyImportExceptions
	(
	ImportRecordiID BIGINT,
	ImportFileName VARCHAR(255),
	ImportRecordLineNumber BIGINT,
	Country VARCHAR(10),
	DateImported DateTime,
	SourceRecord CHAR(4000),
	ExceptionField VARCHAR(255),
	ExceptionFieldValue VARCHAR(255),
	ExceptionReason VARCHAR(255)
)

