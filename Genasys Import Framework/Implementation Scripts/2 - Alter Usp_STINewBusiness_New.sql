/****** Object:  StoredProcedure [SBAB_src].[Usp_STINewBusiness]    Script Date: 02/10/2017 8:34:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [SBAB_src].[Usp_STINewBusiness]
AS
BEGIN

	EXEC [SBAB_src].[Usp_RawDataExceptions_STI];

	------------------------------------------------------------------------------------------------------
	--alter Variable @Date to restirct only the latest records
	------------------------------------------------------------------------------------------------------
	SET DATEFORMAT YMD;
	
	DECLARE @Date DATE = (SELECT DATEADD(DD, 0, CONVERT(DATE, GETDATE())))
	
	-------------------------------------------------------------------
	--Insert the data for the latest records
	-------------------------------------------------------------------
	INSERT INTO [SBAB_src].[DailySTINewBusiness]
	SELECT 
		RTRIM(LTRIM(SUBSTRING(SourceRecord, 1, 1))) Record_Id
		,RTRIM(LTRIM(CONVERT(DATE, SUBSTRING(SourceRecord, 2, 8)))) Transaction_Date
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) = ' ', NULL, RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2)))) Reason_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) CIF_Number
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 62, 30))) Customer_Title
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 92, 30))) Customer_First_Name
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 122, 30))) = ' ', '.', RTRIM(LTRIM(SUBSTRING(SourceRecord, 122, 30)))) Customer_Surname
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1))) = ' ', 'O', RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1)))) Gender
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))))) Date_of_Birth
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20))) ID_Passport_Number
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30))) Postal_Address_1
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30))) Postal_Address_2
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30))) Postal_Address_3
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10))) = ' ', '0000', RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10)))) Postal_Code
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 281, 25))) Tel_No
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 306, 50))) Employer_Name
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 356, 25))) Employer_Contact_Tel_No
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 381, 5))) Marital_Status
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) Loan_Account_No
		,CASE WHEN Country = 'KE' 
				THEN
					CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8))) 
						WHEN '31019' THEN 10155
						WHEN '31002' THEN 10149
						WHEN '31003' THEN 10159
						WHEN '31004' THEN 10154
						WHEN '31005' THEN 10151
						WHEN '31006' THEN 10150
						WHEN '31007' THEN 10147
						WHEN '31008' THEN 10152
						WHEN '31009' THEN 10175
						WHEN '31010' THEN 10153
						WHEN '31011' THEN 10161
						WHEN '31012' THEN 10170 
						WHEN '31013' THEN 10168
						WHEN '31014' THEN 10162
						WHEN '31015' THEN 10164
						WHEN '31016' THEN 10172
						WHEN '31017' THEN 10163
						WHEN '31018' THEN 10165
						WHEN '31020' THEN 10157
						WHEN '31021' THEN 10171
						WHEN '31022' THEN 10166
						WHEN '31023' THEN 10148
						WHEN '31024' THEN 10167
						WHEN '31025' THEN 10156
						WHEN '31026' THEN 10173
						WHEN '31027' THEN 10160
						WHEN '31028' THEN 10158
						WHEN '31200' THEN 10169
						WHEN '31200' THEN 10174
						WHEN '31201' THEN 10176
									 ELSE 10177
				   END
	 ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
	 END SOL_ID
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))) = ' ', NULL, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))))) Loan_Amount
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 30))) Phys_Add1
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 460, 30))) Phys_Add2
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 490, 30))) Phys_Add3
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8))) = '',@DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord, 520, 8)))) AS Loan_Value_Date
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = ' ', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))) Premium
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = ' ', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))) Property_value
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 568, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 568, 8))))) Repayment_Date
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 576, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 576, 8))))) Policy_Expiry_Date
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))) = ' ', NULL, REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O')) Scheme_code
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 586, 3))) = ' ', NULL, ISNULL(LMPCurrencyCode.TargetDBValue,NULL)) Currency_Code
	 ,ISNULL(LMPConstructType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 4)))) Construction_Type
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 593, 1))) Cover_Note
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1))) = ' ', NULL, CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1)))
	 		WHEN 'M' THEN '1'
	 		WHEN 'A' THEN '2'
	 		ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 594, 1)))
	 		END) Premium_Payment_Frequency
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 595, 50))) Insurer_Own_insurance
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 645, 15))) Policy_ID_Own_insurance
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 660, 20))) = ' ', NULL, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 660, 20))))) Amount_Own_insurance
	 ,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 680, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 680, 8))))) Expiry_Date_Own_Insurance
	 ,RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 16))) Loan_Account_No_Own_Insurance
	 ,REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 704, 5))), 'D', 'O') Product_Scheme_Code
	 ,CASE WHEN Country = 'KE' THEN
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))) 
		WHEN '001' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('301','302''303','304','310','330','331','332','333','392','393','422')
				THEN '004'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('321','394','400','401','402','403','404','405','406','407','408','409','410','411','412','413','414','415','416','417','421','423','424','425','426','501','502','503')
				THEN '005'
				ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
			END
		WHEN '002' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('000','100','101','102','125','499')
				THEN '001'
			WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('300','301','302')
				THEN '004'  
			WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('103','104','400','401','402','403','404','405','406','407','408','409','410')
				THEN '005'
			ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))
			END 
		WHEN '009' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))	
				WHEN '999' THEN '005'
			ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))
			END
		END
	ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3)))
	END segment,
		ISNULL(
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))) 
			WHEN '001' THEN
			CASE 
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('100','122','147')
				THEN '106'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('125','131','140','146','149','151','221','222','223','224','225','226')
				THEN '105'  
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('145','159','190','230') 
				THEN '102'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('148','200','201','202','203','227') 
				THEN '103'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('301','302','330','331','393') 
				THEN '203'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('303','332','333','392','422') 
				THEN '201'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('304') 
				THEN '202'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('310') 
				THEN '204'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('400','401','402','403','410','411','412') 
				THEN '301'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('421','423','424','425','426','407','408','409','413','414','415') 
				THEN '302'	 
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('394','404','405','406','416','417') 
				THEN '303'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('321') 
				THEN '309'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('503') 
				THEN '67'										 
			END
		WHEN '002' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('000','100','101','102','125','499') 
				THEN '106'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('300','301') 
				THEN '201'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('302') 
				THEN '203'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('402','403') 
				THEN '303'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('404') 
				THEN '301'
			END
		WHEN '009' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))	
			WHEN '999' THEN 'UNK'
			END
	ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
	END, RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))) Sub_segment
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 715, 5))) Serial_Number
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 720, 100))) Risk_Address
		,RTRIM(LTRIM(SUBSTRING(SourceRecord, 820, 20))) Underwriter_Name
		,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 840, 10))) = ' ', '0', RTRIM(LTRIM(SUBSTRING(SourceRecord, 840, 10)))) PIN_number
		,- 1 iImportRecNo
		,convert(datetime,substring(FileName,19,6)) Processdate
		,t.ID SourceFileID
		,CONCAT (
			RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16)))
			,REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O')
			) NewLoanAccNo
		,Country
		,CASE WHEN Country = 'KE' THEN
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))) 
		WHEN '001' THEN
		CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3))) IN ('101','102','103','104','105','106','107','108','110','111','112','120','121','123','124','130','141','132','142','143','144','150','152','153','154','156','155','157','161','191','210','220','300','390','391','151') 
		THEN '2'
		ELSE	
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
				WHEN  '100' THEN '1'
				WHEN  '148' THEN '10'
				WHEN  '149' THEN '11'
				WHEN  '151' THEN '12'
				WHEN  '159' THEN '13'
				WHEN  '190' THEN '14'
				WHEN  '199' THEN '15'
				WHEN  '200' THEN '16'
				WHEN  '201' THEN '17'
				WHEN  '202' THEN '18'
				WHEN  '203' THEN '19'
				WHEN  '221' THEN '20'
				WHEN  '222' THEN '21'
				WHEN  '223' THEN '22'
				WHEN  '224' THEN '23'
				WHEN  '225' THEN '24'
				WHEN  '226' THEN '25'
				WHEN  '227' THEN '26'
				WHEN  '230' THEN '27'
				WHEN  '301' THEN '28'
				WHEN  '302' THEN '29'
				WHEN  '122' THEN '3'
				WHEN  '303' THEN '30'
				WHEN  '304' THEN '31'
				WHEN  '310' THEN '32'
				WHEN  '321' THEN '33'
				WHEN  '330' THEN '34'
				WHEN  '331' THEN '35'
				WHEN  '332' THEN '36'
				WHEN  '333' THEN '37'
				WHEN  '392' THEN '38'
				WHEN  '393' THEN '39'
				WHEN  '125' THEN '4'
				WHEN  '394' THEN '40'
				WHEN  '400' THEN '41'
				WHEN  '401' THEN '42'
				WHEN  '402' THEN '43'
				WHEN  '403' THEN '43'
				WHEN  '404' THEN '44'
				WHEN  '405' THEN '45'
				WHEN  '406' THEN '46'
				WHEN  '407' THEN '47'
				WHEN  '408' THEN '48'
				WHEN  '409' THEN '49'
				WHEN  '131' THEN '5'
				WHEN  '410' THEN '50'
				WHEN  '411' THEN '51'
				WHEN  '412' THEN '52'
				WHEN  '413' THEN '53'
				WHEN  '414' THEN '54'
				WHEN  '415' THEN '55'
				WHEN  '416' THEN '56'
				WHEN  '417' THEN '57'
				WHEN  '421' THEN '58'
				WHEN  '422' THEN '59'
				WHEN  '140' THEN '6'
				WHEN  '423' THEN '60'
				WHEN  '424' THEN '61'
				WHEN  '425' THEN '62'
				WHEN  '426' THEN '63'
				WHEN  '500' THEN '64'
				WHEN  '501' THEN '65'
				WHEN  '502' THEN '66'
				WHEN  '503' THEN '67'
				WHEN  '145' THEN '7'
				WHEN  '146' THEN '8'
				WHEN  '147' THEN '9'
			END
		END
		WHEN '002' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 712, 3)))
			WHEN '000' THEN '68'
			WHEN '100' THEN '69'
			WHEN '102' THEN '71'
			WHEN '103' THEN '72'
			WHEN '104' THEN '73'
			WHEN '125' THEN  '4'
			WHEN '300' THEN '74'
			WHEN '301' THEN '30'
			WHEN '302' THEN '75'
			WHEN '401' THEN '42'
			WHEN '402' THEN '76'
			WHEN '403' THEN '47'
			WHEN '404' THEN '77'
			WHEN '405' THEN '53'
			WHEN '406' THEN '54'
			WHEN '407' THEN '55'
			WHEN '408' THEN '56'
			WHEN '409' THEN '78'
			WHEN '410' THEN '61'
			WHEN '499' THEN '80'
			WHEN '101' THEN '70'
			END
		END
	ELSE ''
	END sCountryClassification,
	CONCAT(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50)))) Country_Cif
	,COU.Id
	,LMPProductID.TargetDBValue
	,LMPCoInsurer.TargetDBValue
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),3) AS 'Nasria'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),4) AS 'StampDuty'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),5) as 'Namfisa'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),6) as 'NamfisaComm'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),7) as 'RiskPremCPC'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),8) as 'RiskNasriaCPC'
	,dbo.GetNAMHOCSplits(IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 548, 20))))),IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))) = '', 0, CONVERT(NUMERIC(18, 2), RTRIM(LTRIM(SUBSTRING(SourceRecord, 528, 20))))),9) as 'RiskComm'
	,t.FileName
	FROM [ski_int].[SSIS_Src_RawData] t
	LEFT JOIN Policy p ON LTRIM(RTRIM(SUBSTRING(t.SourceRecord, 386, 16))) = p.sOldPolicyNo
	LEFT JOIN CFG.Country COU on COU.Alpha2Code = t.Country
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O') +'_' + t.Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
		ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 586, 3)))
		AND LMPCurrencyCode.Type = 'Currency_Code'
		AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPConstructType
		ON LMPConstructType.SourceDBValue = Concat(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))), 'D', 'O'),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 4))))
		AND LMPConstructType.Type = 'Construct_Type_STI'
		AND LMPConstructType.SourceGroup = 'Construction_Type'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCoInsurer  
		ON LMPCoInsurer.SourceDBValue = t.Country
		AND LMPCoInsurer.Type = 'CoInsurer_STI'
		AND LMPCoInsurer.SourceGroup = 'CoInsurer'
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = '2'
		AND ProcessStatus = 0
		AND CONVERT(DATE, DateImported) = @Date;

	UPDATE 
		[SBAB_src].[DailySTINewBusiness] 
	SET 
		sCustomer_First_Name = '.' 
	WHERE 
		sSegment = '001' and sCustomer_First_Name = ''

	UPDATE 
		[SBAB_src].[DailySTINewBusiness] 
	SET
		dtLoan_Value_Date = DATEADD(M,-1,dtPolicy_Expiry_Date)
	WHERE
		dtLoan_Value_Date > dtPolicy_Expiry_Date

--SELECT dtLoan_Value_Date, dtPolicy_Expiry_Date,DATEADD(M,-1,dtPolicy_Expiry_Date),* FROM 
--[SBAB_src].[DailySTINewBusiness]
--WHERE
--		dtLoan_Value_Date > dtPolicy_Expiry_Date

	---------------------------------------------------------------------------------------------------------------------------
	--Update the source record to processed to eliminate processing duplicates
	---------------------------------------------------------------------------------------------------------------------------
	--Validated Records
	------------------------------------------------------------------
	UPDATE SSF
		SET ProcessStatus = 1
	FROM 
		[ski_int].[SSIS_Src_RawData] SSF
	WHERE 
		SSF.FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, SSF.DateImported) = @Date;
	
	--and CONVERT(DATE,dtProcessDate) = @Date;
	------------------------------------------------------------------
	--Invalid date records
	------------------------------------------------------------------
	--UPDATE SSF
	--SET iProcessStatus = 2
	--FROM SBAB_src.StagingSourceFiles SSF
	--JOIN ##TempInvalidDate TMP ON TMP.iID = SSF.iID
	--WHERE SSF.sFileName LIKE '%sti.newbus%'
	--	AND CONVERT(DATE, SSF.dtDate) = @Date;
	
	------------------------------------------------------------------
	--Invalid numeric records
	------------------------------------------------------------------
	--UPDATE SSF
	--		SET iProcessStatus = 3
	--	FROM SBAB_src.StagingSourceFiles SSF
	--					JOIN ##TempInvalidNumeric TMP
	--					on TMP.iID = SSF.iID
	--WHERE SSF.sFileName like '%sti.newbus%'
	--		and CONVERT(DATE,SSF.dtDate) = @Date;
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	INSERT INTO SBAB_Src.StagingFileProcessAudit
	SELECT 
		SUBSTRING(SourceRecord, 1, 1) Record_ID
		,SUBSTRING(SourceRecord, 2, 8) Count
        ,filename FileName
        ,DateImported ProcessDate
        ,Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData]
	WHERE 
		SUBSTRING(SourceRecord, 1, 1) = 9
		AND FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, DateImported) = @Date
	
	EXCEPT
	
	SELECT Record_ID
		,Count
		,sFileName
		,dtDate
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, dtDate) = @Date
	
	UNION
	
	-------------------------------------------------------------------------------------------------------------------------------------------------
	--Create a record for audit table with count of records for the specific file,country and date
	-------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT 0 AS Record_ID
		,COUNT(*) Count
		,SSF.FileName FileName
		,DateImported ProcessDate
		,SSF.Country Country
		,NULL cTotalPremiumReceived
	FROM 
		[ski_int].[SSIS_Src_RawData]  SSF
		JOIN [SBAB_src].[DailySTINewBusiness] TMP ON TMP.iSourceFileID = SSF.ID
	WHERE 
		SSF.FileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, DateImported) = @Date
	GROUP BY 
		SSF.FileName
		,DateImported
		,SSF.Country
	
	EXCEPT
	
	SELECT Record_ID
		,Count
		,sFileName
		,dtDate
		,sCountry
		,cTotalPremiumReceived
	FROM 
		SBAB_Src.StagingFileProcessAudit
	WHERE 
		sFileName LIKE '%sti.newbus%'
		AND CONVERT(DATE, dtDate) = @Date
	
	TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

	SET NOCOUNT ON;
	

END