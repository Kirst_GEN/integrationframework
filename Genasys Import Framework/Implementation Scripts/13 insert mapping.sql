truncate table [ski_int].[SSISIntegrationLookupMap]
go

SET IDENTITY_INSERT [ski_int].[SSISIntegrationLookupMap] ON 

GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (31, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031019', N'SOL_ID', N'010155')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (32, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031002', N'SOL_ID', N'010149')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (33, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031003', N'SOL_ID', N'010159')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (34, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031004', N'SOL_ID', N'010154')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (35, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031005', N'SOL_ID', N'010151')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (36, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031006', N'SOL_ID', N'010150')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (37, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031007', N'SOL_ID', N'010147')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (38, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031008', N'SOL_ID', N'010152')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (39, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031009', N'SOL_ID', N'010175')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (40, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031010', N'SOL_ID', N'010153')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (41, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031011', N'SOL_ID', N'010161')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (42, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031012', N'SOL_ID', N'010170')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (43, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031013', N'SOL_ID', N'010168')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (44, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031014', N'SOL_ID', N'010162')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (45, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031015', N'SOL_ID', N'010164')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (46, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031016', N'SOL_ID', N'010172')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (47, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031017', N'SOL_ID', N'010163')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (48, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031018', N'SOL_ID', N'010165')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (49, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031020', N'SOL_ID', N'010157')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (50, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031021', N'SOL_ID', N'010171')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (51, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031022', N'SOL_ID', N'010166')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (52, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031023', N'SOL_ID', N'010148')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (53, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031024', N'SOL_ID', N'010167')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (54, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031025', N'SOL_ID', N'010156')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (55, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031026', N'SOL_ID', N'010173')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (56, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031027', N'SOL_ID', N'010160')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (57, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031028', N'SOL_ID', N'010158')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (58, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031200', N'SOL_ID', N'010169')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (59, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031200', N'SOL_ID', N'010174')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (60, N'SBAB', N'SOLID_KE_LTI', N'SOL_ID', N'031201', N'SOL_ID', N'010176')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (82, N'SBAB', N'CoInsurer_LTI', N'CoInsurer', N'NA', N'CoInsurer_NAM', N'LIBERTYNAM')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (267, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_BW', N'iProductID', N'11604')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (268, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_BW', N'iProductID', N'11606')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (269, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_BW', N'iProductID', N'11608')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (270, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_BW', N'iProductID', N'11603')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (271, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_BW', N'iProductID', N'11605')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (272, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_GH', N'iProductID', N'11704')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (273, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_GH', N'iProductID', N'11706')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (274, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_GH', N'iProductID', N'11708')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (275, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_GH', N'iProductID', N'11703')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (276, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_GH', N'iProductID', N'11709')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (277, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_KE', N'iProductID', N'11004')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (278, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_KE', N'iProductID', N'11006')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (279, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_KE', N'iProductID', N'11008')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (280, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_KE', N'iProductID', N'11003')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (281, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_KE', N'iProductID', N'11005')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (282, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_LS', N'iProductID', N'11304')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (283, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_LS', N'iProductID', N'11306')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (284, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_LS', N'iProductID', N'11308')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (285, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_LS', N'iProductID', N'11303')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (286, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_MW', N'iProductID', N'11904')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (287, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_MW', N'iProductID', N'11906')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (288, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_MW', N'iProductID', N'11908')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (289, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_MW', N'iProductID', N'11903')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (290, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_MW', N'iProductID', N'11905')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (291, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_NA', N'iProductID', N'11204')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (292, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_NA', N'iProductID', N'11206')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (293, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_NA', N'iProductID', N'11201')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (294, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_NA', N'iProductID', N'11203')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (295, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_NA', N'iProductID', N'11205')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (296, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_SW', N'iProductID', N'11104')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (297, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_SW', N'iProductID', N'11106')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (298, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_SW', N'iProductID', N'11108')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (299, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_SW', N'iProductID', N'11103')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (300, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_SW', N'iProductID', N'11105')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (301, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_UG', N'iProductID', N'11806')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (302, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_UG', N'iProductID', N'11808')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (303, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_UG', N'iProductID', N'11803')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (304, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_UG', N'iProductID', N'11805')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (305, N'SBAB', N'Product_Code', N'Scheme_Code', N'C_ZM', N'iProductID', N'11504')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (306, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_ZM', N'iProductID', N'11506')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (307, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_ZM', N'iProductID', N'11508')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (308, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_ZM', N'iProductID', N'11503')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (309, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_ZM', N'iProductID', N'11505')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (310, N'SBAB', N'Product_Code', N'Scheme_Code', N'B_ZW', N'iProductID', N'11406')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (311, N'SBAB', N'Product_Code', N'Scheme_Code', N'O_ZW', N'iProductID', N'11408')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (312, N'SBAB', N'Product_Code', N'Scheme_Code', N'A_ZW', N'iProductID', N'11403')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (313, N'SBAB', N'Product_Code', N'Scheme_Code', N'D_ZW', N'iProductID', N'11405')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (314, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_KE_1', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (315, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_KE_2', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (316, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_KE_3', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (317, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_KE_4', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (318, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_KE_1', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (319, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_KE_2', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (320, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_KE_3', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (321, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_KE_4', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (322, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_KE_1', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (323, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_KE_2', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (324, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_KE_3', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (325, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_KE_4', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (326, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_KE_1', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (327, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_KE_2', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (328, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_KE_3', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (329, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_KE_4', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (330, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_SW_A', N'Death + Total and Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (331, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_SW_A', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (332, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_SW_B', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (333, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_SW_C', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (334, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_SW_D', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (335, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_SW_A', N'Death + Total and Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (336, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_SW_A', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (337, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_SW_B', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (338, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_SW_C', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (339, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_SW_D', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (340, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_A', N'Death', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (341, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_A', N'A: Death + Permanent and Total Disability and Critical Illnes', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (342, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_B', N'Total and Permanent Disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (343, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_B', N'B: Death + Permanent and Total Disability + Critical Illness and Retrenchment', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (344, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_C', N'Death + Total and Permanent Disability', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (345, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_C', N'C: Death + Permanent and Total Disability + Critical Illness and Last Expense', N'C')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (346, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_SW_D', N'D: Death + Permanent and Total Disability + Critical Illness + Retrenchment and Last Expense', N'D')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (347, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_SW_A', N'Death Benefit', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (348, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_SW_B', N'Death + Total and Permanent Disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (349, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_NA_A', N'PLP A:  Death, Permanent Disability, Temporary Disability and Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (350, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_NA_B', N'PLP B:  Death, Permanent Disability, Temporary Disability, Retrenchment and Dread Disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (351, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_NA_C', N'PLP C:  Death, Temporary Disability and Retrenchment', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (352, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_NA_A', N'BLP A:  Death, Permanent Disability, Temporary Disability and Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (353, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_NA_B', N'BLP B:  Death, Permanent Disability, Temporary Disability, Retrenchment and Dread Disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (354, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_NA_C', N'BLP C:  Death, Temporary Disability and Retrenchment', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (355, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_A', N'HLP A:  Death, Permanent Disability, Temporary Disability and Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (356, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_B', N'HLP B:  Death, Permanent Disability, Temporary Disability, Retrenchment and Dread Disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (357, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_C', N'HLP C:  Death Temporary Disability and Retrenchment', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (358, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_D', N'HLP D:  Death Only', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (359, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_E', N'HLP E:   Death and Temporary Disability', N'5')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (360, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_NA_F', N'HLP F:   Death and Retrenchment', N'6')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (361, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_LS_A', N'Death, Temporary Disability & Permanent total disability & Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (362, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_LS_B', N'Death, Disability, Cash Back & Funeral', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (363, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_LS_C', N'Death, Disability, Retrenchment, Cash Back & Funeral', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (364, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_LS_A', N'Death, Temporary Disability & Permanent total disability & Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (365, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_LS_B', N'Death, Disability, Cash Back & Funeral', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (366, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_LS_C', N'Death, Disability, Retrenchment, Cash Back & Funeral', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (367, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_LS_A', N'Death, Temporary Disability & Permanent total disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (368, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_ZW_A', N'Death + Temporary and Permanent Total Disability + Retrenchment + Abscondance', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (369, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_ZW_A', N'Death + Temporary and Permanent Total Disability + Retrenchment + Abscondance', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (370, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_ZW_A', N'Death + Temporary and Permanent Total Disability + Retrenchment + Abscondance', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (371, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_ZM_A', N'Death', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (372, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_ZM_B', N'Death+ Temporary Disability + Permanent total disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (373, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_ZM_A', N'Death', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (374, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_ZM_B', N'Death+ Temporary Disability + Permanent total disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (375, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_ZM_A', N'Plan A: Death, temporary disability and permanent disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (376, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_ZM_B', N'Plan B: Death, temporary disability, permanent disability and dread disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (377, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_ZM_C', N'Plan C: Death cover only', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (378, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_ZM_A', N'Death, Permanent total disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (379, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_BW_A', N'PLP A:  Death and Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (380, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_BW_B', N'PLP C:  Death, Temporary and Permanent Disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (381, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_BW_C', N'PLP PLAN TYPE 97: Death, Permanent Disability and Desertion', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (382, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_BW_A', N'BLP A:  Death and Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (383, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_BW_B', N'BLP B:  Death Only', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (384, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_BW_A', N'Plan A: Death, Total and Permanent Disability, Temporary Disability and Sickness', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (385, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_BW_B', N'Plan B: Death, Total and Permanent Disability, Temporary Disability and Sickness and Dread Disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (386, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_BW_C', N'Plan C: Death Only', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (387, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_BW_A', N'HLP A:  (Death and Total Disability)', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (388, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'B_BW_B', N'HLP 98: (Death, Total  & Permanent Disability, Temporary Disability and Sickness)', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (389, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_GH_V', N'PLP V: Death and Temporary and Permanent Disability', N'V')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (390, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_GH_A', N'PLP A: Death, Temporary and Total Permanent Disability, Retrenchment and Funeral', N'A')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (391, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_GH_B', N'PLP B: Death, Temporary and Total Permanent Disability and Funeral', N'B')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (392, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_GH_A', N'Plan A:  Death, Temporary & Total Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (393, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_UG_A', N'Death', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (394, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_UG_B', N'Death and Total Disability, Critical Illness & Retrenchment', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (395, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_UG_A', N'Plan A - Maximum (No Medical)', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (396, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_UG_B', N'Plan B - Medical Required', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (397, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_MW_A', N'PLP A:  Death, Disability and retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (398, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'A_MW_B', N'PLP B:  Death and Disability', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (399, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'C_MW_A', N'Death,  Total & Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (400, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_MW_A', N'Death, Total & Permanent Disability', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (401, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_A', N'ASB A:  Death, Temporary Disability, Permanent Disability and Retrenchment', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (402, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_B', N'ASB B:  Death, Temporary Disability, Permanent Disability, Retrenchment and Dread Disease', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (403, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_C', N'ASB C:  Death Only', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (404, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_D', N'VAFPP � Option 1 (Death only)', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (405, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_E', N'VAFPP � Option 2 (Death, Permanent Disability, Total Disability)', N'5')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (406, N'SBAB', N'Plan_Type_LTI', N'Plan_Type', N'D_NA_F', N'VAFPP � Option 3 (Death, Permanent Disability, Total Disability, Dread Disease, Income Loss)', N'6')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (423, N'SBAB', N'Currency_Code', N'Currency_Code', N'BWP', N'Currency_Code', N'8')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (424, N'SBAB', N'Currency_Code', N'Currency_Code', N'ZAR', N'Currency_Code', N'0')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (425, N'SBAB', N'Currency_Code', N'Currency_Code', N'KES', N'Currency_Code', N'6')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (426, N'SBAB', N'Currency_Code', N'Currency_Code', N'USD', N'Currency_Code', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (427, N'SBAB', N'Currency_Code', N'Currency_Code', N'EUR', N'Currency_Code', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (428, N'SBAB', N'Currency_Code', N'Currency_Code', N'GBP', N'Currency_Code', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (429, N'SBAB', N'Currency_Code', N'Currency_Code', N'SZL', N'Currency_Code', N'9')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (430, N'SBAB', N'Currency_Code', N'Currency_Code', N'ZMW', N'Currency_Code', N'12')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (431, N'SBAB', N'Currency_Code', N'Currency_Code', N'GHS', N'Currency_Code', N'13')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (432, N'SBAB', N'Currency_Code', N'Currency_Code', N'UGX', N'Currency_Code', N'14')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (433, N'SBAB', N'Currency_Code', N'Currency_Code', N'LSL', N'Currency_Code', N'11')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (434, N'SBAB', N'Currency_Code', N'Currency_Code', N'MWK', N'Currency_Code', N'15')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (435, N'SBAB', N'Currency_Code', N'Currency_Code', N'MT', N'Currency_Code', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (436, N'SBAB', N'Currency_Code', N'Currency_Code', N'NAD', N'Currency_Code', N'10')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (437, N'SBAB', N'Currency_Code', N'Currency_Code', N'SSP', N'Currency_Code', N'7')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (438, N'SBAB', N'Currency_Code', N'Currency_Code', N'TS', N'Currency_Code', N'5')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (439, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_BW_STDR', N'Standard Construction', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (440, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_BW_STDC', N'Commercial Construction', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (441, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_BW_THCH', N'Thatch', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (442, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_BW_SLST', N'Subsidence and Landslip: Standard Construction', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (443, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_BW_SLNS', N'Subsidence and Landslip: Non Standard Construction', N'5')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (444, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_GH_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (445, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_LS_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (446, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_LS_2', N'Non-Standard', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (447, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_LS_3', N'High', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (448, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_LS_4', N'Thatch', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (449, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_MW_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (450, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_NHS', N'NHS - Not A Holiday Home Standard Construction', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (451, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_NHT', N'NHT - Not A Holiday Home Thatch Construction', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (452, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_NHNS', N'NHNS - Not A Holiday Home Non-Standard Construction', N'3')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (453, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_HS', N'HS - Holiday Home Standard Construction', N'4')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (454, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_HT', N'HT - Holiday Home Thatch Construction', N'5')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (455, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_NA_HNS', N'HNS - Holiday Home Non-Standard Construction', N'6')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (456, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_SW_BCOM', N'Business Combined', N'BCOM')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (457, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_SW_CMFP', N'Commercial Fire Policy', N'CMFP')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (458, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_SW_STND', N'Standard', N'STND')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (459, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_SW_THCH', N'Thatch', N'THCH')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (460, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_UG_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (461, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_UG_2', N'Non-Standard', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (462, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_ZM_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (463, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_ZW_STND', N'Standard', N'1')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (464, N'SBAB', N'Construct_Type_STI', N'Construction_Type', N'O_ZW_2', N'Standard Staff Rate', N'2')
GO
INSERT [ski_int].[SSISIntegrationLookupMap] ([Id], [ClientID], [Type], [SourceGroup], [SourceDBValue], [TargetGroup], [TargetDBValue]) VALUES (465, N'SBAB', N'CoInsurer_STI', N'CoInsurer', N'NA', N'CoInsurer_NAM', N'HOLLARDNAM')
GO
SET IDENTITY_INSERT [ski_int].[SSISIntegrationLookupMap] OFF
GO
