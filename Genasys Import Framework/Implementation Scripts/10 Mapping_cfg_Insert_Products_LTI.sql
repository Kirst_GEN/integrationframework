--------------------------------------------------------------------------------------------------
--Clear Data before Updating Config
--------------------------------------------------------------------------------------------------
DELETE FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'Product_Code'

--------------------------------------------------------------------------------------------------
--Insert config
--------------------------------------------------------------------------------------------------
insert into [ski_int].[SSISIntegrationLookupMap]
Select 
    'SBAB',
    'Product_Code',
    'Scheme_Code',
    case when sProductCode = 'HOC' 
            then Concat('O_',Right(sInsurer,2))
        when sProductCode = 'PLP' 
            then Concat('A_',Right(sInsurer,2))
        when sProductCode = 'HLP' 
            then Concat('B_',Right(sInsurer,2))
        when sProductCode = 'VLP' 
            then Concat('D_',Right(sInsurer,2))
		when sProductCode = 'VFP' 
            then Concat('D_',Right(sInsurer,2))
        when sProductCode = 'BLP' 
            then Concat('C_',Right(sInsurer,2))
    end,
    'iProductID',
    iProductID
from 
    Products 
where 
    sProductCode in ('HOC','PLP','HLP','VLP','BLP','VFP')
order by 
    sProductName


UPDATE N Set PH_ProductID = TargetDBValue
FROM SBAB_src.DailyLTINewBusiness N
JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = sScheme_Code +'_' + sCountry
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code' 	
	WHERE --sCountry = 'NA' AND 
	PH_ProductID IS NULL

/*
SELECT * FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'Product_Code'
ORDER BY 7
*/

UPDATE N Set PH_ProductID = TargetDBValue
--SELECT sScheme_Code +'_' + sCountry,*
FROM SBAB_src.LTIPremium N
JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = RTRIM(sScheme_Code) +'_' + sCountry
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code' 	
	WHERE PH_ProductID IS NULL

UPDATE N Set PH_ProductID = TargetDBValue
--SELECT sScheme_Code +'_' + sCountry,*
FROM SBAB_src.STIPremium N
JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = RTRIM(sScheme_Code) +'_' + sCountry
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code' 	
	WHERE PH_ProductID IS NULL
		
--select ski_int.SBAB_GetPolicyID (sCIF_Number, sNewLoanAccNo, PH_ProductID)
UPDATE sbab_src.LTIPremium
SET PH_PolicyID = ski_int.SBAB_GetPolicyID (sCIF_Number, sNewLoanAccNo, PH_ProductID)
from sbab_src.LTIPremium

UPDATE sbab_src.STIPremium
SET PH_PolicyID = ski_int.SBAB_GetPolicyID (sCIF_Number, sNewLoanAccNo, PH_ProductID)
--SELECT ski_int.SBAB_GetPolicyID (sCIF_Number, sNewLoanAccNo, PH_ProductID), sCIF_Number, sNewLoanAccNo, PH_ProductID,*
from sbab_src.STIPremium

--select Convert(char(10),dtProcessDate,111), SUM(cPremium_Received) AS cPremium_Received, SUM(cOutstandingBalance) AS cOutstandingBalance, count(*) 
--from sbab_src.LTIPremium where sCountry = 'NA' AND PH_PolicyID IS NULL 
--group by Convert(char(10),dtProcessDate,111)
--order by Convert(char(10),dtProcessDate,111) desc

--select Convert(char(10),dtProcessDate,111), *
--from sbab_src.LTIPremium where sCountry = 'NA' AND PH_PolicyID IS NULL 
--AND DATEPART(MONTH,dtProcessDate) = 7 
--AND DATEPART(DAY,dtProcessDate) IN (18,19,27,29,31)
--order by Convert(char(10),dtProcessDate,111) desc

--select Convert(char(10),dtProcessDate,111), SUM(cPremium_Received) AS cPremium_Received, count(*) 
--from sbab_src.STIPremium where sCountry = 'NA' AND PH_PolicyID IS NULL 
--AND DATEPART(MONTH,dtProcessDate) = 7 
--group by Convert(char(10),dtProcessDate,111)
--order by Convert(char(10),dtProcessDate,111) desc

--select Convert(char(10),dtProcessDate,111), *
--from sbab_src.STIPremium where sCountry = 'NA' AND PH_PolicyID IS NULL 
--AND DATEPART(MONTH,dtProcessDate) = 7 
--AND DATEPART(DAY,dtProcessDate) IN (18,19,27,29,31)
--order by Convert(char(10),dtProcessDate,111) desc