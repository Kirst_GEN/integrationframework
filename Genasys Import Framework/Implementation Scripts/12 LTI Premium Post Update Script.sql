CREATE PROCEDURE [SBAB_src].[Usp_LTIPremium_PostUpdate]
AS
BEGIN


DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, 0, GETDATE())))

-----------------------------------------------------------------------------------------------------------------------
--PREMIUM ADJUSTMENTS FROM LTIPREMIUM
-----------------------------------------------------------------------------------------------------------------------
UPDATE RSK
	SET cPremium = src.cPremium
	, cSumInsured = ABS(cOutstandingBalance)
	, cAnnualPremium = CASE RSK.iPaymentTerm 
							WHEN 3 THEN SRC.cPremium * 12 
							WHEN 0 THEN SRC.cPremium 
							WHEN 1 THEN SRC.cPremium * 2 
							WHEN 2 THEN SRC.cPremium * 4
							WHEN 4 THEN SRC.cPremium 
							END
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN (
	SELECT CONCAT(sLoan_Account_No,sScheme_Code) sNewLoanAccountNo
		,cPremium_Received cPremium
		,cOutstandingBalance 
		,dtProcessDate
		,iCountryID
		,sCountry_Cif
		,PH_ProductID
		--,ROW_NUMBER() OVER(PARTITION BY CONCAT(sLoan_Account_No,sScheme_Code) ORDER BY dtprocessdate DESC) RowNumber 
	FROM SBAB_src.LTIPremium
	) SRC 
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
AND CONVERT(DATE,SRC.dtProcessDate) = @Date;


-----------------------------------------------------------------------------------------------------------------------
--HEADER POLICY PREMIUM UPDATE
-----------------------------------------------------------------------------------------------------------------------
UPDATE P
SET
	 P.cNextPaymentAmt = R.cPremium
	,P.cAnnualPremium  = R.cAnnualPremium
	,P.cCommissionAmt  = R.cCommissionAmt
	,P.cAnnualSasria   = R.cAnnualSasria
--select r.cAnnualPremium, r.cpremium, r.cAnnualSasria, p.cannualpremium, p.cNextPaymentAmt,p.cAnnualSasria,r.cCommissionAmt,p.cCommissionAmt, *
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNo = sNewLoanAccNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
WHERE CONVERT(DATE,dtProcessDate) = @Date;

--------------------------------------------------------------------------------------------
--Policy Commision Premium Update - Kenya
--------------------------------------------------------------------------------------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 8/100) 
	, cCommissionPaid = 0.08
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision Premium Update - Botswana - HLP Product
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 13.5/100) 
	,cCommissionPaid = 0.135
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND sScheme_Code in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision Premium Update - Zim
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 20/100) 
	,cCommissionPaid = 0.20
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision Premium Update - NAM
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 20/100) 
	, cCommissionPaid = 0.20
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'NA'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision Premium Update - Zambia
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 35/100) 
	, cCommissionPaid = 0.35
--select CONVERT(FLOAT,POL.cAnnualPremium* 8/100) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision Premium Update- Kenya
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 8/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision Premium Update- Zim
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 20/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision Premium Update- Zambia
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 35/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision Premium Update- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 13.5/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND sScheme_Code in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision Premium Update- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 20/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND sScheme_Code not in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Admin Fee Premium Update- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(RSK.cPremium * 12/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Insurance Levy Amount - Zambia
-----------------
UPDATE PD
SET cFieldValue = ROUND(SRC.cPremium_Due * 3/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_INSURLEVYAMOU'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Amount - UGANDA
-----------------
UPDATE PD
SET cFieldValue = ROUND(SRC.cPremium_Due * 0.5/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYAMOU'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Percent - UGANDA
-----------------
UPDATE PD
SET cFieldValue = 0.5
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYPERC'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- IRA Levy Amount - UGANDA
-----------------
UPDATE PD
SET cFieldValue = ROUND(SRC.cPremium_Due * 1.5/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYAMOU'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- IRA Levy Percent - UGANDA
-----------------
UPDATE PD
SET cFieldValue = 1.5
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = Pol.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYPERC'
JOIN SBAB_src.LTIPremium SRC
	ON sOldPolicyNO = SRC.sNewLoanAccNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Policy Details Admin Fee Premium Update- Swaziland
-----------------
UPDATE PD
SET cFieldValue = ROUND(RSK.cPremium * 10/100,2)*(12/fValue)
--select ROUND(RSK.cPremium * 10/100,2),RSK.cPremium ,LTI.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Policy Admin Fee Premium Update- Swaziland
-----------------
UPDATE POL
SET cAdminFee = ROUND(RSK.cPremium * 10/100,2)*(12/fValue)
--select ROUND((RSK.cPremium * 10/100)/fValue,2),RSK.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Admin Fee Premium Update- Swaziland
-----------------
UPDATE RSK
SET cAdminFee = ROUND(LTI.cPremium_Received * 10/100,2)
--select ROUND((RSK.cPremium * 10/100)/fValue,2),RSK.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk premium less Policy Admin Fee Premium Update - Swaziland
-----------------
UPDATE RSK
SET RSK.cPremium = LTI.cPremium_Received - ROUND(LTI.cPremium_Received * 10/100,2),
	cAnnualPremium = (LTI.cPremium_Received - ROUND(LTI.cPremium_Received * 10/100,2))*(12/fvalue)
--select RSK.cPremium - ROUND(RSK.cPremium * 10/100,2),cAnnualPremium = (RSK.cPremium - ROUND(RSK.cPremium * 10/100,2))*(12/fvalue),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Policy premium Premium Update
-----------------
UPDATE P
SET  P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
--select r.cAnnualPremium, r.cpremium, r.cAnnualSasria, p.cannualpremium, p.cNextPaymentAmt,p.cAnnualSasria,r.cCommissionAmt,p.cCommissionAmt,cadminfee,src.cpremium, *
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND P.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND P.iProductID = LTI.PH_ProductID
WHERE CONVERT(DATE,dtProcessDate) = @Date
	AND sCountry = 'SZ';

-----------------
--Policy Commision New Business - Swaziland
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,LTI.cPremium * 12.5/100 * 12/fvalue) 
	, cCommissionPaid = 0.125
--select CONVERT(FLOAT,POL.cAnnualPremium* 12.5/100) ,cCommissionPaid = 0.125,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Policy Commision Premium Update - Swaziland
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,LTI.cPremium_Received * 12.5/100 * 12/fvalue) ,cCommissionPaid = 0.125
--select CONVERT(FLOAT,POL.cAnnualPremium* 8/100) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Commision New Business- Swaziland
-----------------
UPDATE RSK
SET cCommission = ROUND(LTI.cPremium * 12.5/100,2)
-- select ROUND(RSK.cPremium * 12.5/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision Premium Update- Swaziland
-----------------
UPDATE RSK
SET cCommission = ROUND(LTI.cPremium_Received * 12.5/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision Premium Update - Ghana
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,LTI.cPremium_Received * 40/100 * 12/fvalue) ,cCommissionPaid = 0.125
--select CONVERT(FLOAT,POL.cAnnualPremium* 8/100) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision Premium Update - Ghana
-----------------
UPDATE RSK
SET cCommission = ROUND(LTI.cPremium_Received * 40/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Withholding Tax Premium Update- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND((RSK.cPremium* 8/100)*10/100,2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Withholding Tax Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND((cCommissionAmt* 10/100),2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Withholding Tax Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = 10
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND	PD.sFieldCode = 'P_WITHHTAXPERC'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--VAT Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND((cCommissionAmt* 12/100),2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--VAT Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = 10
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Excise tax Premium Update - Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND((RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,2)
--select ROUND((RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = LTI.sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Excise tax to be removed Premium Update- Kenya
-----------------
UPDATE PD
SET sFieldValue = (RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100
--select (RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU0'
JOIN SBAB_src.LTIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

END