
--sCountry_Cif
--Add Column
	
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness1','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness2','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness3','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness4','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness1','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness2','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness3','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness4','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness_Arch','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness_Arch','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusinessException','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusinessException','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'StandaloneCrossSell','sCountry_Cif','varchar(50)';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness1','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness2','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness3','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness4','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness1','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness2','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness3','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness4','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness_Arch','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusinessException','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusinessException','iCountryID','INT';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness','RH_CoInsurer','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','RH_CoInsurer','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness','PH_ProductID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','PH_ProductID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness_ARCH','PH_ProductID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness_ARCH','PH_ProductID','INT';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','PH_ProductID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'STIPremium','PH_ProductID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','PH_PolicyID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'STIPremium','PH_PolicyID','INT';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','sCountry_Cif','varchar(50)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','sCountry_Cif','varchar(50)';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','iCountryID','INT';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','iCountryID','INT';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'StagingFileProcessAudit','sFileName','varchar(255)';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','NA_StampDuty','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','NA_Namfisa','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','NA_NamfisaComm','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','RiskPremCPC','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','NA_RiskNasriaCPC','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','RiskComm','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','NA_Nasria','money';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'LTIPremium','cProperty_Value','money';

EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailySTINewBusiness','FileName','VARCHAR(255)';
EXEC SKI_INT.AlterTablesAddColumns 'SBAB_src', 'DailyLTINewBusiness','FileName','VARCHAR(255)';

	--Update
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness_Arch D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness_Arch D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusinessException D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusinessException D

	Update D set iCountryID = C.ID
	from SBAB_src.LTIPremium D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.STIPremium D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness1 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness2 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness3 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness4 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness1 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness2 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness3 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness4 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness_Arch D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness_Arch D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailyLTINewBusinessException D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusinessException D
	join cfg.Country C on C.Alpha2Code = D.sCountry

--Add Column 

	
--Update
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness1 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness2 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness3 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness4 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness1 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness2 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness3 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness4 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness_Arch D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness_Arch D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusinessException D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusinessException D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.LTIPremium D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.STIPremium D
Update C set C.sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.StandaloneCrossSell C

	Update L
	set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.LTIPremium L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.STIPremium L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_LTI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

	Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_STI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

ALTER TABLE SBAB_src.DailySTINewBusiness
ALTER COLUMN sSub_segment VARCHAR(10)
 
ALTER TABLE SBAB_src.DailyLTINewBusiness
ALTER COLUMN sSub_segment VARCHAR(10) 


Alter Table SKI_INT.DailyImportExceptions
	add Imported bit 

GO
Update SKI_INT.DailyImportExceptions
	set Imported = 0 

GO

Alter Table SKI_INT.DailyImportExceptions
	add Constraint Imported_D_0 default 0 FOR Imported  



