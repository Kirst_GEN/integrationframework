-----------------------------------------------------------------------------
--INSERT MISSING COINSURERS FOR NAMIBIA
-----------------------------------------------------------------------------
INSERT INTO CoInsurersRiskRel
Select 
	R.intRiskID,
	CASE WHEN P.iProductID = 11201 THEN 'HOLLARDNAM' ELSE 'LIBERTYNAM' END,  
	100,1,NULL,NULL,
	CASE WHEN P.iProductID = 11201 THEN 11000006 ELSE 11000007 END
FROM 
	Risks R
JOIN Policy P 
	ON P.ipolicyid = R.intPolicyID 
	AND P.iProductID IN(11206,11203,11204,11205) 
WHERE NOT EXISTS
	(
	SELECT * FROM CoInsurersRiskRel C where C.iRiskID = R.intRiskID
	)

-----------------------------------------------------------------------------
--REMOVE COINSURERS WHICH HAVE NO RISKS ATTACHED TO THEM
-----------------------------------------------------------------------------
DELETE 
FROM CoInsurersRiskRel 
WHERE NOT EXISTS(
	SELECT * FROM RISKS R 
	WHERE R.intRiskID = CoInsurersRiskRel.iRiskID
	)


