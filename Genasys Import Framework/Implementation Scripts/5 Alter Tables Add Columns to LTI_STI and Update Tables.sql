
--sCountry_Cif
--Add Column

	Alter table SBAB_src.DailyLTINewBusiness 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness1 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness2 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness3 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness4
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness1 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness2 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness3 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness4
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness_Arch
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusiness_Arch
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailyLTINewBusinessException
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.DailySTINewBusinessException
		add sCountry_Cif varchar(50) null

	--Update
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness_Arch D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness_Arch D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusinessException D
	Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusinessException D

--Add Country ID Column

	Alter table SBAB_src.DailyLTINewBusiness 
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness1 
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness2 
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness3 
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness4
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness 
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness1 
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness2 
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness3 
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness4
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness_Arch
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusiness_Arch
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusinessException
		add iCountryID int null

	Alter table SBAB_src.DailySTINewBusinessException
		add iCountryID int null

	Alter table SBAB_src.DailyLTINewBusiness 
		add RH_CoInsurer varchar(50) null
	
	Alter table SBAB_src.DailySTINewBusiness 
		add RH_CoInsurer varchar(50) null

	Alter table SBAB_src.DailyLTINewBusiness 
		add PH_ProductID int

	Alter table SBAB_src.DailySTINewBusiness 
		add PH_ProductID int

	Alter table SBAB_src.LTIPremium 
		add PH_ProductID int

	Alter table SBAB_src.STIPremium 
		add PH_ProductID int
	
	Alter table SBAB_src.LTIPremium 
		add PH_PolicyID int

	Alter table SBAB_src.STIPremium 
		add PH_PolicyID int
	
	Alter table SBAB_src.LTIPremium 
		add PH_ProductID int

	Alter table SBAB_src.STIPremium 
		add PH_ProductID int
		
	Alter table SBAB_src.LTIPremium 
		add PH_PolicyID int

	Alter table SBAB_src.STIPremium 
		add PH_PolicyID int

	Alter table SBAB_src.LTIPremium 
		add sCountry_Cif varchar(50) null

	Alter table SBAB_src.STIPremium 
		add sCountry_Cif varchar(50) null
	
	Alter table SBAB_src.LTIPremium 
		add iCountryID int null

	Alter table SBAB_src.STIPremium 
		add iCountryID int null

	--Update
	Update D set iCountryID = C.ID
	from SBAB_src.LTIPremium D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.STIPremium D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness1 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness2 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness3 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness4 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness1 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness2 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusiness3 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness4 D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailyLTINewBusiness_Arch D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailySTINewBusiness_Arch D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID 
	from SBAB_src.DailyLTINewBusinessException D
	join cfg.Country C on C.Alpha2Code = D.sCountry
	Update D set iCountryID = C.ID
	from SBAB_src.DailySTINewBusinessException D
	join cfg.Country C on C.Alpha2Code = D.sCountry

--Add Column 

	Alter table SBAB_src.DailyLTINewBusiness 
		add PH_ProductID int

	Alter table SBAB_src.DailySTINewBusiness 
		add PH_ProductID int

	Alter table SBAB_src.DailyLTINewBusiness_Arch
		add PH_ProductID int

	Alter table SBAB_src.DailySTINewBusiness_Arch 
		add PH_ProductID int

--Update
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness1 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness2 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness3 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness4 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness1 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness2 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness3 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness4 D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusiness_Arch D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusiness_Arch D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailyLTINewBusinessException D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.DailySTINewBusinessException D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.LTIPremium D
Update D set sCountry_Cif = Concat(sCountry,'_',sCIF_Number) from SBAB_src.STIPremium D

	Update L
	set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness_Arch L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.LTIPremium L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.PH_ProductID =  M.TargetDBValue 
	from SBAB_src.STIPremium L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'Product_Code' and SourceDBValue = Concat(L.sScheme_Code,'_',L.sCountry)

	Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_LTI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

	Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailySTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_STI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

		ALTER TABLE SBAB_Src.StagingFileProcessAudit
		ALTER COLUMN sFileName VARCHAR(255)
