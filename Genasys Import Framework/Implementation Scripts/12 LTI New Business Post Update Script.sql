CREATE PROCEDURE [SBAB_src].[Usp_LTINewBusiness_PostUpdate]
AS
BEGIN


DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, 0, GETDATE())))

------------------------------------------------------------------------------------------------------------------
--RISK PREMIUM UPDATE ON RISK
------------------------------------------------------------------------------------------------------------------
UPDATE RSK
SET cPremium = src.cPremium
	, cSumInsured = cLoan_Amount
	, cAnnualPremium = CASE RSK.iPaymentTerm 
						WHEN 3 THEN SRC.cPremium * 12 
						WHEN 0 THEN SRC.cPremium 
						WHEN 1 THEN SRC.cPremium * 2 
						WHEN 2 THEN SRC.cPremium * 4
						WHEN 4 THEN SRC.cPremium 
						END
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = iPolicyID
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE CONVERT(DATE,dtProcessDate) = @Date;

-----------------------------------------------------------------------------------------------------------------------
--HEADER POLICY PREMIUM UPDATE
-----------------------------------------------------------------------------------------------------------------------
UPDATE P
	SET
		P.cNextPaymentAmt = R.cPremium
		,P.cAnnualPremium  = R.cAnnualPremium
		,P.cCommissionAmt  = R.cCommissionAmt
		,P.cAnnualSasria   = R.cAnnualSasria
--select r.cAnnualPremium, r.cpremium, r.cAnnualSasria, p.cannualpremium, p.cNextPaymentAmt,p.cAnnualSasria,r.cCommissionAmt,p.cCommissionAmt, *
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
WHERE CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Comm Structs
-----------------
UPDATE 
	PC
SET 
	PC.iLev2EntityID = CE.iCommEntityID,
	PC.iCommEntityRelID = CSER.iCommRelID
FROM 
	Policy Pol
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = POL.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	JOIN SBAB_src.DailyLTINewBusiness DL (NOLOCK) 
		ON DL.sNewLoanAccountNo = Pol.sOldPolicyNo
		AND POL.CountryId = DL.iCountryID
		AND CD.sFieldValue = DL.sCountry_Cif
		AND POL.iProductID = DL.PH_ProductID
	JOIN PolicyCommStruct PC (NOLOCK) ON PC.iVersionNo = Pol.iAgencyID
	JOIN CommEntities CE (NOLOCK) 
		ON CE.sCode = case 
							when len(DL.sSOL_ID) = 5 then '0'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 4 then '00'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 3 then '000'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 2 then '0000'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 1 then '00000'+ (DL.sSOL_ID)
							else DL.sSOL_ID
						end
	JOIN CommStructEntityRel CSER (NOLOCK) 
		ON CSER.iLev1EntityID = PC.iLev1EntityID 
		AND CSER.iLev2EntityID = CE.iCommEntityID 
		AND CSER.iCommStructID = PC.iCommStructID
		AND CSER.iLev3EntityID = PC.iLev3EntityID 
		AND CSER.iLev4EntityID = PC.iLev4EntityID 
		AND CSER.iLev5EntityID = PC.iLev5EntityID
WHERE 
	PC.iLev2EntityID <> CE.iCommEntityID
	AND CONVERT(DATE,dtProcessDate) = @Date;

------------------------------------------------------------------------------------------------------------------
--UPDATE CONTACT NAME FROM COMPANY NAME FOR COMPANIES
------------------------------------------------------------------------------------------------------------------
UPDATE CUST
	SET sContactName = CUST.sCompanyName
FROM 
	Customer CUST
JOIN Policy POL
	ON CUST.iCustomerID = POL.iCustomerID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = SRC.sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	bCompanyInd = 1
	AND ISNULL(sContactName,'') = ''
	AND CONVERT(DATE,dtProcessDate) = @Date;

------------------------------------------------------------------------------------------------------------------
--CANCEL POLICY WHERE THE REASON CODE IS C OR S
------------------------------------------------------------------------------------------------------------------
UPDATE POL
	SET iPolicyStatus = 2, sComment = 'Cancelled due to LTI Update', dDateModified = getdate(), dEffectiveDate = getdate()
FROM 
	Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sReason_code in ('C','S')
	AND CONVERT(DATE,dtProcessDate) = @Date
	AND iParentID = -1;

------------------------------------------------------------------------------------------------------------------
--Policy Vat
------------------------------------------------------------------------------------------------------------------
UPDATE POL
	SET iTaxPerc = 1.00
FROM 
	Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = SRC.sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	ISNULL(iTaxPerc,0.00) <> 1.00
	AND CONVERT(DATE,dtProcessDate) = @Date;
	
-----------------
--Risk review status
-----------------
UPDATE RSK
	SET eReviewStatus = 0
FROM 
	Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = iPolicyID
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = SRC.sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	eReviewStatus = 1
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Payment Method
-----------------
UPDATE POL
SET iPaymentMethod = 2
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	iPaymentMethod <> 2
	AND	CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Endorsement Code
-----------------
UPDATE POL
SET sEndorseCode = 17, sComment = 'Imported Policy'
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	(sEndorseCode <> 17 AND sComment <> 'Imported Policy')
	AND CONVERT(DATE,dtProcessDate) = @Date;

	-----------------
--Policy Renewal Date
-----------------
UPDATE POL
SET dRenewalDate = dateadd(dd,1,dExpiryDate)
FROM 
	Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = SRC.sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	dRenewalDate <> dateadd(dd,1,dExpiryDate)
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business- Kenya
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 8/100) 
	,cCommissionPaid = 0.08
--select CONVERT(FLOAT,POL.cAnnualPremium* 8/100) ,cCommissionPaid = 0.08,*
FROM 
	policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	LTI.sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business- Botswana - HLP%BLP Product
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 13.5/100) 
	,cCommissionPaid = 0.135
FROM 
	policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE sCountry = 'BW'
	AND sScheme_Code in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business- Botswana -  Other Products
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 20/100) 
	, cCommissionPaid = 0.2
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND sScheme_Code not in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business- Zim
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 20/100) 
	, cCommissionPaid = 0.20
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business- Zim
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 35/100) 
	, cCommissionPaid = 0.35
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------------------------------------------------------------------------------------------------------------
--Policy Commision Premium Update - Botswana - Other Products
-----------------------------------------------------------------------------------------------------------------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,POL.cAnnualPremium* 20/100) 
	, cCommissionPaid = 0.20
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND sScheme_Code Not in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision New Business- Kenya
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 8/100,2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision New Business- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 13.5/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE sCountry = 'BW'
	AND sScheme_Code in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision New Business- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 20/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE sCountry = 'BW'
	AND sScheme_Code not in ('B','C')
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision New Business- Zim
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 20/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision New Business- Nam
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 20/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'NA'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Risk Commision New Business- Zambia
-----------------
UPDATE RSK
SET cCommission = ROUND(RSK.cPremium* 35/100,2)
-- select ROUND(RSK.cPremium* 8/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Admin Fee New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(RSK.cPremium * 12/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Insurance Levy Amount- Zambia
-----------------
UPDATE PD
SET cFieldValue = ROUND(LTI.cPremium * 3/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_INSURLEVYAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Policy Details Admin Fee New Business- Swaziland
-----------------
UPDATE PD
SET cFieldValue = ROUND(RSK.cPremium * 10/100,2)*(12/fValue)
--select ROUND(RSK.cPremium * 10/100,2),RSK.cPremium ,LTI.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy Admin Fee New Business- Swaziland
-----------------
UPDATE POL
SET cAdminFee = ROUND(RSK.cPremium * 10/100,2)*(12/fValue)
--select ROUND((RSK.cPremium * 10/100)/fValue,2),RSK.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Amount- UGANDA
-----------------
UPDATE PD
SET cFieldValue = ROUND(LTI.cPremium * 0.5/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;

	
-----------------
-- Training Levy Percent- UGANDA
-----------------
UPDATE PD
SET cFieldValue = 0.5
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYPERC'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- IRA Levy Amount- UGANDA
-----------------
UPDATE PD
SET cFieldValue = ROUND(LTI.cPremium * 1.5/100,2)
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- IRA Levy Percent- UGANDA
-----------------
UPDATE PD
SET cFieldValue = 1.5
--select ROUND(RSK.cPremium * 12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYPERC'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Admin Fee New Business- Swaziland
-----------------
UPDATE RSK
SET cAdminFee = ROUND(LTI.cPremium * 10/100,2)
--select ROUND((RSK.cPremium * 10/100)/fValue,2),RSK.cPremium,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP 
	ON iIndex = POL.iPaymentTerm
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk premium less Policy Admin Fee New Business- Swaziland
-----------------
UPDATE RSK
SET RSK.cPremium = LTI.cPremium - ROUND(LTI.cPremium * 10/100,2),
	cAnnualPremium = (LTI.cPremium - ROUND(LTI.cPremium * 10/100,2))*(12/fvalue)
--select RSK.cPremium - ROUND(RSK.cPremium * 10/100,2),cAnnualPremium = (RSK.cPremium - ROUND(RSK.cPremium * 10/100,2))*(12/fvalue),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Policy premium New Bus
-----------------
UPDATE P
SET  P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
--select r.cAnnualPremium, r.cpremium, r.cAnnualSasria, p.cannualpremium, p.cNextPaymentAmt,p.cAnnualSasria,r.cCommissionAmt,p.cCommissionAmt,cadminfee,src.cpremium, *
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND P.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND P.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'SZ'
	AND	CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy Commision New Business - Ghana
-----------------
UPDATE POL
SET cCommissionAmt = CONVERT(FLOAT,LTI.cPremium * 40/100 * 12/fvalue) ,cCommissionPaid = 0.125
--select CONVERT(FLOAT,POL.cAnnualPremium* 12.5/100) ,cCommissionPaid = 0.125,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN LOOKUP LKP
	ON POL.iPaymentTerm = iIndex
	and sGroup = 'Payment Term'
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commision New Business - Ghana
-----------------
UPDATE RSK
SET cCommission = ROUND(LTI.cPremium * 40/100,2)
-- select ROUND(RSK.cPremium * 12.5/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Withholding Tax New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND((RSK.cPremium* 8/100)*10/100,2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--PLP Policy Fee Percentage- Ghana
-----------------
UPDATE PD
SET cFieldValue = 0.05
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND pd.sFieldCode = 'P_POLICFEEPERC'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
	AND sScheme_Code = 'A'
WHERE 
	sCountry = 'GH'
	AND PD.cFieldValue is null
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--PLP Policy Fee Amonut- Ghana
-----------------
UPDATE PD
SET cFieldValue = (cLoan_Amount*0.05)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_POLICFEEAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
	AND sScheme_Code = 'A'
WHERE 
	sCountry = 'GH'
	AND PD.cFieldValue is null
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--BLP Policy Fee Percentage- Ghana
-----------------
UPDATE PD
SET cFieldValue = 0.01
--select pd.*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_POLICFEEPERC'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
	AND sScheme_Code = 'C'
WHERE 
	sCountry = 'GH'
	AND PD.cFieldValue is null
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--BLP Policy Fee Amonut- Ghana
-----------------
UPDATE PD
SET cFieldValue = (cLoan_Amount*0.01)
--select PD.*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_POLICFEEAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
	AND sScheme_Code = 'C'
WHERE 
	sCountry = 'GH'
--	AND cFieldValue is null
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Withholding Tax New Business- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND((cCommissionAmt* 10/100),2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Withholding Tax New Business- Botswana
-----------------
UPDATE PD
SET cFieldValue = 10
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_WITHHTAXPERC'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--VAT- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND((cCommissionAmt* 12/100),2)
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--VAT- Botswana
-----------------
UPDATE PD
SET cFieldValue = 12
--select ROUND((RSK.cPremium* 8/100)*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Excise tax New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND((RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,2)
--select ROUND((RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Excise tax to be removed New Business- Kenya
-----------------
UPDATE PD
SET sFieldValue = (RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100
--select (RSK.cPremium* 8/100+(RSK.cPremium * 12/100))*10/100,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU0'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Loan repayment date
-----------------
UPDATE A
SET sFieldValue = DATEPART(d,dtRepayment_Date)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN RiskDetailS A 
	ON A.iRiskID = RSK.intRiskID
	AND A.sFieldCode = 'R_LOANREPADATE'
JOIN SBAB_src.DailyLTINewBusiness LTI
	ON sOldPolicyNO = LTI.sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
WHERE 
	A.sFieldValue IS NULL
	AND CONVERT(DATE,dtProcessDate) = @Date;



-------------------------------------------------------------
--Risks inception date - STI
-------------------------------------------------------------
UPDATE R
SET R.dInception = dCoverStart
FROM Risks R
JOIN Policy POL
	ON intPolicyID = iPolicyID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailyLTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	CONVERT(DATE,R.dInception) <> CONVERT(DATE,dCoverStart);

END