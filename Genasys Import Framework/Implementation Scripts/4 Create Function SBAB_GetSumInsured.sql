/****** Object:  UserDefinedFunction [ski_int].[SBAB_GetPolicyID]    Script Date: 28/09/2017 6:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [ski_int].[SBAB_GetSumInsured](@CIFNumber VARCHAR(30), @NewLoanAccountNumber VARCHAR(30), @ProductID INT) 
RETURNS BIGINT
AS
BEGIN

RETURN (
		
				SELECT 
					--c.icustomerid, p.ipolicyid, p.sPolicyNo, P.sOldPolicyNo, CD.sFieldValue
					TOP 1 R.cSumInsured
				FROM 
					Customer c
				INNER JOIN 
					policy p 
						ON p.iCustomerID = c.iCustomerID
				INNER JOIN 
					CustomerDetails CD 
						ON CD.iCustomerID = C.iCustomerID 
						AND CD.sFieldCode = 'C_CUSTOMNUMBER'
				INNER JOIN Risks R
					ON R.intPolicyID = P.iPolicyID
				WHERE 
					sOldPolicyNo = @NewLoanAccountNumber 
					AND cd.sFieldValue = @CIFNumber
					AND P.iProductID = @ProductID
			
		)
END

