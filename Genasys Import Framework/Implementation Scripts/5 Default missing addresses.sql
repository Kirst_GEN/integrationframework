/*
    find any Namibia customers that have no address entries and insert blanks
*/

DECLARE @AddID INT = (SELECT MAX(intAddressID) FROM dbo.Address)

INSERT INTO dbo.Address
SELECT 
(ROW_NUMBER() OVER(ORDER BY c.iCustomerID) +@AddID) intAddressID,
'Client' strAddressGroup,	
'Postal' strAddressType,
'' strAddressLine1,
'' strAddressLine2,
'' strAddressLine3,	
'' strSuburb,
'' strPostalCode,	
'' strRatingArea,	
'' strPhoneNo,	
'' strFaxNo,
'' strMobileNo,
'' strEmailAdd,
0 bReplicated,	
0 iTownID,
GETDATE() dUpdated,
'GENADMIN_FIX' sUpdatedBy,	
'' sTown,
c.iCustomerID iTranID,
c.iCustomerID icustomerid,	
0 bPrefered,	
NULL iLatitude,
NULL iLongitude
FROM 
	dbo.CommStructEntityRel cse
	JOIN dbo.Customer c ON cse.iCommRelID = c.iAgentID and cse.iLev1EntityID = 10203 
WHERE 
	(
		c.rPhysAddressID < 0
		AND c.rPostAddressID < 0
		AND c.rWorkAddressID < 0
	)
GO

--set address id on customer
UPDATE c 
SET c.rPostAddressID = a.intAddressID
FROM 
	dbo.CommStructEntityRel cse WITH (NOLOCK)
	JOIN dbo.Customer c ON cse.iCommRelID = c.iAgentID 
		and cse.iLev1EntityID = 10203 
		AND c.rPostAddressID < 0 
	JOIN dbo.Address a WITH (NOLOCK) on c.iCustomerID = a.iCustomerID 
		AND a.strAddressGroup = 'Client' 
		AND a.strAddressType = 'Postal'
GO

/*
    Set missing required fields on addresses to default
*/
UPDATE aPost
SET --only replace blank fields with defaults.
	aPost.strAddressLine1 = 
		CASE WHEN ISNULL(aPost.strAddressLine1,'') = '' THEN COUNTRY.NAME ELSE aPost.strAddressLine1 END,
	aPost.strSuburb = 
		CASE WHEN ISNULL(aPost.strSuburb,'') = '' THEN COUNTRY.NAME ELSE aPost.strSuburb END,
	aPost.strPostalCode = 
		CASE WHEN ISNULL(aPost.strPostalCode,'') = '' THEN '9000' ELSE aPost.strPostalCode END
FROM 
	dbo.CommStructEntityRel cse
	JOIN dbo.Customer c ON cse.iCommRelID = c.iAgentID and cse.iLev1EntityID = 10203 --only for Namibia customers
	JOIN dbo.Address aPost ON c.rPostAddressID = aPost.intAddressID
	JOIN dbo.Address aPhys ON c.rPhysAddressID = aPhys.intAddressID
	JOIN cfg.country COUNTRY ON COUNTRY.ID = C.CountryId
WHERE (--if both postal and physical addresses are complete, then set the defauls on the postal address. 
		(--if either of these fields are blank, the address is incomplete.
			ISNULL(aPhys.strAddressLine1, '') = ''
			OR ISNULL(aPhys.strSuburb, '') = '' 
			OR ISNULL(aPhys.strPostalCode, '') = ''
		)
		AND
		(
			ISNULL(aPost.strAddressLine1, '') = ''
			OR ISNULL(aPost.strSuburb, '') = '' 
			OR ISNULL(aPost.strPostalCode, '') = ''
		)
	)
GO

