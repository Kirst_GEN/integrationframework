CREATE TABLE [ski_int].[SSISIntegrationLookupMap](
       [Id] [int] IDENTITY(1,1) NOT NULL,
       [ClientID] [varchar](50) NOT NULL,
       [Type] [varchar](50) NOT NULL,
       [SourceGroup] [varchar](50) NOT NULL,
       [SourceDBValue] [varchar](255) NOT NULL,
       [TargetGroup] [varchar](255) NOT NULL,
       [TargetDBValue] [varchar](50) NOT NULL,
CONSTRAINT [PK_CLL_LookupMap] PRIMARY KEY CLUSTERED 
(
       [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

