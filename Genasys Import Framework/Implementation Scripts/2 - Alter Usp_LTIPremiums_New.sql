USE [SBAB_082017]
GO
/****** Object:  StoredProcedure [SBAB_src].[Usp_LTIPremiums_New]    Script Date: 02/10/2017 12:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [SBAB_src].[Usp_LTIPremiums] 
	
AS
BEGIN
-------------------------------------------------------------------------------
--Delete exising temp tables 
-------------------------------------------------------------------------------
--IF OBJECT_ID('tempdb..##Temp') IS NOT NULL
--DROP TABLE ##Temp;
--IF OBJECT_ID('tempdb..##TempInvalidDate') IS NOT NULL
--DROP TABLE ##TempInvalidDate;
--IF OBJECT_ID('tempdb..##TempInvalidNumeric') IS NOT NULL
--DROP TABLE ##TempInvalidNumeric;

------------------------------------------------------------------------------------------------------
--Create Variable @Maxdate to restirct only the latest records
------------------------------------------------------------------------------------------------------
--truncate table sbab_src.ltipremium

DECLARE @Date date = (SELECT CONVERT(DATE,DATEADD(DD,0,GETDATE())))

-------------------------------------------------------------------
--Get all validated records
-------------------------------------------------------------------
--SELECT *
--INTO ##Temp
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and ISDATE(SUBSTRING(SourceRecord,2,8)) = 1
--		and ISDATE(SUBSTRING(SourceRecord,233,8)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,76,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,97,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 1
--		and CONVERT(DATE,dtDate) = @Date

-------------------------------------------------------------------
--Get all invalid date records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidDate
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and (ISDATE(SUBSTRING(SourceRecord,2,8)) = 0
--		   or ISDATE(SUBSTRING(SourceRecord,233,8)) = 0)
--		and CONVERT(DATE,dtDate) = @Date
-------------------------------------------------------------------
--Get all invalid numeric records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidNumeric
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE FileName like '%lti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and ProcessStatus = 0
--		and (ISNUMERIC(SUBSTRING(SourceRecord,76,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,97,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 0)
--		and CONVERT(DATE,dtDate) = @Date
-------------------------------------------------------------------
--Insert the data for the latest records
-------------------------------------------------------------------
INSERT INTO [SBAB_src].[LTIPremium]
SELECT 
					RTRIM(LTRIM(SUBSTRING(SourceRecord,1,1))) Record_Id,
					CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,2,8)))) Transaction_Date ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))) Loan_Account_No	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,26,50))) CIF_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,76,20))) = ' ', NULL,CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,76,20))) Premium_Received ,
					SUBSTRING(SourceRecord,96,1) Balance_Sign ,
					CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,97,20)) OutstandingBalance ,
					IIF(SUBSTRING(SourceRecord,117,20) = ' ' ,NULL,CONVERT(NUMERIC(18,2),rtrim(ltrim(SUBSTRING(SourceRecord,117,20)))))  Premium_due ,
					IIF(SUBSTRING(SourceRecord,137,2) = ' ' ,NULL,SUBSTRING(SourceRecord,137,2)) Scheme_code ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,139,3))) = ' ' ,NULL,CASE RTRIM(LTRIM(SUBSTRING(SourceRecord,139,3))) 
																					WHEN 'BWP' THEN '8'
																					WHEN 'KES' THEN '6'
																					WHEN 'USD' THEN '1'
																					WHEN 'EUR' THEN '3'
																					WHEN 'GBP' THEN '2'
																					WHEN 'SZL' THEN '9'
																					WHEN 'GHS' THEN '13'
																					WHEN 'ZMW' THEN '12'
																					WHEN 'ZAR' THEN '0'
																					WHEN 'UGX' THEN '14'
																					WHEN 'LSL' THEN '11'
																					WHEN 'MWK' THEN '15'
																					WHEN 'NAD' THEN '10'
																				  ELSE NULL 
																				END) Currency_Code,
					IIF(SUBSTRING(SourceRecord,142,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,142,30)))) Customer_Title,	
					IIF(SUBSTRING(SourceRecord,172,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,172,30)))) Customer_First_Name,
					IIF(SUBSTRING(SourceRecord,202,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,202,30)))) Customer_Surname ,
					SUBSTRING(SourceRecord,232,1) Gender,
					IIF(SUBSTRING(SourceRecord,233,8) = ' ' ,NULL,CONVERT(DATE,SUBSTRING(SourceRecord,233,8))) Date_of_Birth,
					IIF(SUBSTRING(SourceRecord,241,20)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,241,20)))) ID_Passport_Number	,
					IIF(SUBSTRING(SourceRecord,261,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,261,30)))) Postal_Address_1,	
					IIF(SUBSTRING(SourceRecord,291,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,291,30)))) Postal_Address_2,	
					IIF(SUBSTRING(SourceRecord,321,30)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,321,30)))) Postal_Address_3,	
					IIF(SUBSTRING(SourceRecord,351,10)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,351,10)))) Postal_Code	,
					IIF(SUBSTRING(SourceRecord,361,25)= ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,361,25)))) Tel_No ,
					IIF(SUBSTRING(SourceRecord,386,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,386,5)))) Marital_Status,
					IIF(SUBSTRING(SourceRecord,391,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,391,30)))) Phys_Add1,
					IIF(SUBSTRING(SourceRecord,421,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,421,30)))) Phys_Add2,	
					IIF(SUBSTRING(SourceRecord,451,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,451,30)))) Phys_Add3,	
					IIF(SUBSTRING(SourceRecord,481,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,481,30)))) Second_Life_Insured_First_Name,
					IIF(SUBSTRING(SourceRecord,511,30) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,511,30)))) Second_Life_Insured_Surname,
					IIF(SUBSTRING(SourceRecord,541,20) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,541,20)))) Second_Life_Insured_ID_Passport_No,
					IIF(SUBSTRING(SourceRecord,561,8) = ' ' ,NULL,SUBSTRING(SourceRecord,561,8)) Second_Life_DOB	,
					IIF(SUBSTRING(SourceRecord,569,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,569,5)))) Product_Scheme_Code ,
					IIF(SUBSTRING(SourceRecord,574,1) = ' ' ,NULL,SUBSTRING(SourceRecord,574,1)) Status_Flag ,
					IIF(SUBSTRING(SourceRecord,575,1) = ' ' ,NULL,SUBSTRING(SourceRecord,575,1)) Exception	,
					IIF(SUBSTRING(SourceRecord,576,2) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,576,2)))) Plan_Type,
					IIF(SUBSTRING(SourceRecord,578,5) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,578,5)))) Serial_Number,
					IIF(SUBSTRING(SourceRecord,583,20) = ' ',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,583,20)))) Underwriter_Name,
					IIF(SUBSTRING(SourceRecord,603,10) = ' ',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,603,10)))) PIN_number,
					-1 iImportRecNo,
					convert(datetime,substring(FileName,21,6)) as ProcessDate,
					[ski_int].[SSIS_Src_RawData].ID iSourceFileID,
					CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))) sNewLoanAccNo
					,Country
					,LMPProductID.TargetDBValue
					,ski_int.SBAB_GetPolicyID (RTRIM(LTRIM(SUBSTRING(SourceRecord,26,50))), CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))), LMPProductID.TargetDBValue)
					,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 26, 50)))) Country_Cif
					,COU.Id
					,ISNULL([ski_int].[SBAB_GetSumInsured] (RTRIM(LTRIM(SUBSTRING(SourceRecord,26,50))), CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,2)))), LMPProductID.TargetDBValue),0)
FROM [ski_int].[SSIS_Src_RawData]
LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 137, 2))) +'_' + Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
LEFT JOIN CFG.Country COU on COU.Alpha2Code = [ski_int].[SSIS_Src_RawData].Country
WHERE FileName like '%lti.premium%'
		and SUBSTRING(SourceRecord,1,1) = 2
		and ProcessStatus = 0
		and CONVERT(DATE,DateImported) = @Date
---------------------------------------------------------------------------------------------------------------------------
--Update the source record to processed to eliminate processing duplicates
---------------------------------------------------------------------------------------------------------------------------
--Validated Records
------------------------------------------------------------------
UPDATE SSF
		SET ProcessStatus = 1
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [SBAB_src].[LTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE SSF.FileName like '%lti.premium%'
		and CONVERT(DATE,SSF.DateImported) = @Date 
------------------------------------------------------------------
--Invalid date records
------------------------------------------------------------------
--UPDATE SSF
--		SET ProcessStatus = 2
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidDate TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.FileName like '%lti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date 
------------------------------------------------------------------
--Invalid numeric records
------------------------------------------------------------------
--UPDATE SSF
--		SET ProcessStatus = 3
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidNumeric TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.FileName like '%lti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--IF ( SELECT TOP 1 FileName
--			FROM SBAB_Src.StagingFileProcessAudit
--		 WHERE FileName like '%lti.premiums%'
--				and CONVERT(DATE,dtDate) = @Date) IS NOT NULL

--BEGIN

--	DELETE FROM SBAB_Src.StagingFileProcessAudit
--					WHERE FileName like '%lti.premiums%'
--							and CONVERT(DATE,dtDate) = @Date
--END

INSERT INTO SBAB_Src.StagingFileProcessAudit
SELECT  SUBSTRING(SourceRecord,1,1) Record_ID,
					iif(RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12))) = ' ',0,RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12)))) Count
					,FileName FileName
					,DateImported ProcessDate
					,Country
					,CONVERT(NUMERIC(18,2),SUBSTRING(SourceRecord,2,20)) TotalPremiumsReceived				
	FROM [ski_int].[SSIS_Src_RawData]
 WHERE SUBSTRING(SourceRecord,1,1) = 9
		and FileName like '%lti.premium%'
		and CONVERT(DATE,DateImported) = @Date

 EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%lti.premium%'
		AND CONVERT(DATE,dtDate) = @Date

 UNION
-------------------------------------------------------------------------------------------------------------------------------------------------
--Create a record for audit table with count of records for the specific file,country and date
-------------------------------------------------------------------------------------------------------------------------------------------------
SELECT	0 AS Record_ID,
					COUNT(*) Count,
					FileName FileName,
					DateImported ProcessDate,
					SSF.Country Country,
					SUM(cPremium_Received) TotalPremiumsReceived
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [SBAB_src].[LTIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE FileName like '%lti.premium%'
		AND CONVERT(DATE,DateImported) = @Date
GROUP BY FileName, DateImported,SSF.Country

EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
WHERE sFileName like '%lti.premium%'
		AND CONVERT(DATE,dtDate) = @Date


TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

SET NOCOUNT ON;
END

