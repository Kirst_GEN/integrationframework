CREATE PROCEDURE [SBAB_src].[Usp_STIPremium_PostUpdate]
AS
BEGIN


DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, 0, GETDATE())))

-----------------------------------------------------------------------------------------------------------------------
--PREMIUM ADJUSTMENTS FROM STIPREMIUM
-----------------------------------------------------------------------------------------------------------------------
-----------------
--Premium Adjustments
-----------------
UPDATE RSK
SET cPremium = src.cPremium
	,cSumInsured = ABS(cProperty_Value)
	,cAnnualPremium = CASE RSK.iPaymentTerm 
							WHEN 3 THEN SRC.cPremium * 12 
							WHEN 0 THEN SRC.cPremium 
							WHEN 1 THEN SRC.cPremium * 2 
							WHEN 2 THEN SRC.cPremium * 4
							WHEN 4 THEN SRC.cPremium 
						END
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON iPolicyID = intPolicyID
JOIN (
	SELECT sNewLoanAccNo sNewLoanAccountNo
		,cPremium_Received cPremium
		,cProperty_Value 
		,dtProcessDate
		,sSerial_Number
		,iCountryID
		,sCountry_Cif
		,PH_ProductID
		--,ROW_NUMBER() OVER(PARTITION BY CONCAT(sLoan_Account_No,sScheme_Code) ORDER BY dtprocessdate DESC) RowNumber 
FROM SBAB_src.STIPremium
	) SRC ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy premium
-----------------
UPDATE P
SET
 P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.STIPremium SRC
	ON sOldPolicyNo = sNewLoanAccNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON P.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE
	CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy commission - Kenya
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;



-----------------
--Policy commission - Botswana
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium* 20/100)),2) ,cCommissionPaid = 0.2
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy commission - Zim
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*20/100),2) ,cCommissionPaid = 0.20
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy commission - Zambia
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*35/100),2) ,cCommissionPaid = 0.35
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;



-----------------
--Policy commission - Ghana
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*15/100),2) ,cCommissionPaid = 0.15
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Commission Premium Update- Kenya
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Commission Premium Update- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium )*20/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Commission Premium Update- Zim
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*20/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND pd.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = pd.sFieldValue
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;



-----------------
--Risk Commission Premium Update- Zambia
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*35/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Risk Commission Premium Update- Ghana
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*15/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- ITET(Training Levy) Premium Update - Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_ITETTRALEVFLA'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--PHCF (Policy holder compensation fund) Premium Update - Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.25/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.25/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_PHCFPOLHOLCO0'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Amount - Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.5/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Percentage - Uganda
-----------------
UPDATE PD
SET cFieldValue = 0.5
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYPERC'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
-- Training Levy Amount - Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 1.5/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- IRA Levy Percentage - Uganda
-----------------
UPDATE PD
SET cFieldValue = 1.5
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_IRALEVYPERC'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
-- IRA Levy Percentage - Uganda
-----------------
UPDATE PD
SET cFieldValue = 1.5
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_IRALEVYPERC'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
-- Policy admin fee Premium Update- Kenya
-----------------
UPDATE POL
SET cAdminFee = PD.cFieldValue *(12/fValue)
--select cFieldValue *(12/fValue),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_ITETTRALEVFLA'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Policy Fee Premium Update- Kenya
-----------------
UPDATE POL
SET cPolicyfee = PD.cFieldValue *(12/fValue)
--select cFieldValue *12,*(12/fValue)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_PHCFPOLHOLCO0'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Admin Fee Premium Update - Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100,2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Withholding tax Premium Update- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Withholding tax Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(cCommissionAmt*10/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Withholding tax Premium Update- Botswana
-----------------
UPDATE PD
SET cFieldValue = 10
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXPERC'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Vat- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(cCommissionAmt*12/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Vat- Botswana
-----------------
UPDATE PD
SET cFieldValue = 12
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Excise Tax Premium Update- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(((CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100)+
						(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100)) *10/100,2)--/(12/fValue)
--select ROUND(((CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100)+
						--(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100)) *10/100,2)--/(12/fValue)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Risk Premium less ITET and PHCF Premium Update - Kenya
-----------------
UPDATE RSK
SET  cPremium = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Vat- Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,((RSK.cPremium-case when icurrency = 14 then 35000 else 11 end/12/fValue)*18/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Vat- Uganda
-----------------
UPDATE PD
SET cFieldValue = 18
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Stamp Duty Premium Update- Swaziland
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
--select ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_STAMPDUTYAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Insurance Levy Amount - Zambia
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(lti.cPremium_due*3/100)),2)
--select ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_INSURLEVYAMOU'
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Risk Premium less Admin fee Premium Update - Uganda
-----------------
UPDATE RSK
SET  cPremium = ROUND(CONVERT(FLOAT,(LTI.cPremium_Received-case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
--select rsk.cPremium,ROUND(CONVERT(FLOAT,(RSK.cPremium - iif(RSK.cPremium * 10 / 100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Risk Admin fee Premium Update - Uganda
-----------------
UPDATE RSK
SET  cAdminFee = ROUND(CONVERT(FLOAT,(case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
--select rsk.cPremium,ROUND(CONVERT(FLOAT,(RSK.cPremium - iif(RSK.cPremium * 10 / 100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Policy premium Premium Update
-----------------
UPDATE P
SET
 P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF' 
JOIN SBAB_src.STIPremium SRC
	ON sOldPolicyNo = sNewLoanAccNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Policy commission - Swaziland
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100)),2) ,cCommissionPaid = 0.175
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date
;


-----------------
--Risk commission - Swaziland
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100)),2) ,cCommissionPerc= 0.175
--select cPremium,ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100)),2) ,cCommissionPerc= 0.175,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.STIPremium LTI
	ON sOldPolicyNO = sNewLoanAccNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;


END