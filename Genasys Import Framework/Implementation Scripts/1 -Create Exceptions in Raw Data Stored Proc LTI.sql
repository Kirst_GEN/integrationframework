
/*
CREATE Table SKI_INT.DailyImportExceptions(
ImportRecordiID BIGINT,
ImportFileName VARCHAR(100),
ImportRecordLineNumber BIGINT,
Country VARCHAR(10),
DateImported DateTime,
SourceRecord CHAR(4000),
ExceptionField VARCHAR(255),
ExceptionFieldValue VARCHAR(255),
ExceptionReason VARCHAR(255)
)
select * from [ski_int].[SSIS_Src_RawData]
TRUNCATE TABLE SBAB_src.StagingSourceFiles;
*/

	
ALTER PROCEDURE [SBAB_src].[Usp_RawDataExceptions_LTI]
AS			

--INSERT INTO	
--	[ski_int].[SSIS_Src_RawData_Log]
--SELECT 
--	*
--FROM
--	SBAB_src.StagingSourceFiles;
--SELECT * FROM [ski_int].[SSIS_Src_RawData_Log]	

------------------------------------------------------------------------------------------------------------------------------------------------
--remove special characters first before validating any further
------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE
	ski_int.SSIS_Src_RawData
SET
	SourceRecord = ski_int.RemoveSpecialChars(SourceRecord);

------------------------------------------------------------------------------------------------------------------------------------------------
--##File Format Incorrect
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Record Id',
	SUBSTRING(SourceRecord,1,1),
	'Invalid Record Id in position 1'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	FileName like '%lti.newbus%'
	AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1
------------------------------------------------------------------------------------------------------------------------------------------------
--##File Format Incorrect
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Record Id',
	SUBSTRING(SourceRecord,1,1),
	'Invalid Record Id - Invalid Character in position 1'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	FileName like '%lti.newbus%'
	AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 1
	AND SUBSTRING(SourceRecord,1,1) NOT IN ('1','2','9')
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1
------------------------------------------------------------------------------------------------------------------------------------------------
--##Invalid Loan Amount
--select * from SKI_INT.DailyImportExceptions
--TRUNCATE TABLE SKI_INT.DailyImportExceptions
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Loan Amount',
	REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','') AS [Loan Amount],
	'Invalid Numeric Value for Loan Amount in position 410'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord,1,1) = '2'
	AND FileName like '%lti.newbus%'
	AND ProcessStatus = 0
	AND ISNUMERIC(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','')) = 0
	--AND CONVERT(NUMERIC,REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','')) <= 0 -- [Loan Amount]
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##Loan Amount Less than or Equal to zero
------------------------------------------------------------------------------------------------------------------------------------------------
------------INSERT INTO SKI_INT.DailyImportExceptions
------------SELECT 
------------	ID,
------------	FileName,
------------	ID,
------------	Country,
------------	DateImported,
------------	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
------------	'Loan Amount',
------------	REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','') AS [Loan Amount],
------------	'Loan Amount Less than or Equal to zero'
------------FROM 
------------	[ski_int].[SSIS_Src_RawData]
------------WHERE 
------------	SUBSTRING(SourceRecord,1,1) = '2'
------------	AND FileName like '%lti.newbus%'
------------	AND ProcessStatus = 0
------------	AND ISNUMERIC(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','')) = 1
------------	AND CONVERT(NUMERIC,REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','')) <= 0 -- [Loan Amount]
------------	AND NOT EXISTS(
------------				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
------------				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
------------				   )
------------order by 
------------	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##File Format Incorrect
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Fixed Width format',
	LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
	'File Format Fixed Width Length Incorrect - Expecting 730'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%lti.newbus%'
	AND (
		LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')) < 729
		)
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##Date of Application Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Date of Application',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))),
	'Date of Application Invalid in position 2'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%lti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##Date Of Birth Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Date Of Birth',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))),
	'Date Of Birth Invalid in position 153'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%lti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##Date Of Birth Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Repayment Date',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8))),
	'Repayment Date Invalid in position 463'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%lti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##LTI Expiry Date Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
--INSERT INTO SKI_INT.DailyImportExceptions
--SELECT 
--	ID,
--	FileName,
--	ID,
--	Country,
--	DateImported,
--	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
--	'LTI Expiry Date',
--	RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))),
--	'LTI Expiry Date Invalid'
--FROM 
--	[ski_int].[SSIS_Src_RawData]
--WHERE 
--	SUBSTRING(SourceRecord, 1, 1) = '2'
--	AND FileName like '%lti.newbus%'
--	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))) = 0
--	AND NOT EXISTS(
--				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
--				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
--				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
--				   )
--order by 
--	1

------------------------------------------------------------------------------------------------------------------------------------------------
--##CrossSell Exceptions
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID-1,
	Country,
	DateImported,
	LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
	'Fixed Width format',
	LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
	'File Format Fixed Width Lenth Incorrect expecting 440'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	FileName like '%cross%'
	AND (
		LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')) < 440
		)
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1


------------------------------------------------------------------------------------------------------------------------------------------------
--##Remove Exception Records from Source table
------------------------------------------------------------------------------------------------------------------------------------------------
DELETE 
--SELECT * 
	FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE EXISTS(
			SELECT 
				* 
			FROM 
				SKI_INT.DailyImportExceptions EXC
			WHERE
				EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
			)

