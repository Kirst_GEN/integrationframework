
--EXEC SKI_INT.AlterTablesAddColumns 'ski_int', 'test','TestColumn6','Varchar(30)';

ALTER PROC SKI_INT.AlterTablesAddColumns 
	(
	@SchemaName VARCHAR(10), 
	@TableName VARCHAR(100), 
	@ColumnName Varchar(100),
	@ColumnType Varchar(100)
	)
AS
BEGIN
DECLARE @AlterString VARCHAR(2000)
SET @AlterString = 'Alter Table ' + @SchemaName + '.' + @TableName + ' ADD ' + @ColumnName + ' ' + @ColumnType + ' NULL'

	IF NOT EXISTS 
	(
		SELECT
	        *
        FROM
            INFORMATION_SCHEMA.COLUMNS
        WHERE
			TABLE_SCHEMA = @SchemaName
            AND TABLE_NAME = @TableName
            AND COLUMN_NAME = @ColumnName
	)
		BEGIN
			EXEC (@AlterString)
			--PRINT '=======================================================================================================';
			PRINT char(13);
			PRINT 'Table was altered, ' + @ColumnName +' column added successfully to table ' + @TableName;
			PRINT '=======================================================================================================';
		END
	ELSE
		BEGIN
			--PRINT '#######################################################################################################';
			PRINT char(13);
			PRINT 'Table ' + @TableName + ' was not altered, as column ' + @ColumnName + ' already exists on the table.';
			PRINT '#######################################################################################################';
		END
END