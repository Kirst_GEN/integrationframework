--------------------------------------------------------------------------------------------------
--Clear Data before Updating Config
--------------------------------------------------------------------------------------------------
DELETE FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'SOLID_KE_LTI'

--------------------------------------------------------------------------------------------------
--Insert config
--------------------------------------------------------------------------------------------------
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031019','SOL_ID','010155')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031002','SOL_ID','010149')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031003','SOL_ID','010159')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031004','SOL_ID','010154')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031005','SOL_ID','010151')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031006','SOL_ID','010150')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031007','SOL_ID','010147')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031008','SOL_ID','010152')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031009','SOL_ID','010175')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031010','SOL_ID','010153')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031011','SOL_ID','010161')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031012','SOL_ID','010170')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031013','SOL_ID','010168')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031014','SOL_ID','010162')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031015','SOL_ID','010164')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031016','SOL_ID','010172')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031017','SOL_ID','010163')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031018','SOL_ID','010165')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031020','SOL_ID','010157')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031021','SOL_ID','010171')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031022','SOL_ID','010166')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031023','SOL_ID','010148')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031024','SOL_ID','010167')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031025','SOL_ID','010156')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031026','SOL_ID','010173')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031027','SOL_ID','010160')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031028','SOL_ID','010158')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031200','SOL_ID','010169')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031200','SOL_ID','010174')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','SOLID_KE_LTI','SOL_ID','031201','SOL_ID','010176')