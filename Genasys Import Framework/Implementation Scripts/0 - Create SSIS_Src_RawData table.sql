
--drop table [ski_int].[SSIS_Src_RawData]
CREATE TABLE [ski_int].[SSIS_Src_RawData](
	[ID] [int] identity (1,1) NOT NULL,
	[SourceRecord] [varchar](8000) NULL,
	[DateImported] [datetime] NULL,
	[FileName] [varchar](255) NULL,
	[Country] [varchar](25) NULL,
	[ProcessStatus] [int] NULL
) ON [PRIMARY]

GO

