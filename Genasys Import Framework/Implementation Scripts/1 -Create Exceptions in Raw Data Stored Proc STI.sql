/****** Object:  StoredProcedure [SBAB_src].[Usp_RawDataExceptions_STI]    Script Date: 30/09/2017 7:01:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
CREATE Table SKI_INT.DailyImportExceptions(
ImportRecordiID BIGINT,
ImportFileName VARCHAR(100),
ImportRecordLineNumber BIGINT,
Country VARCHAR(10),
DateImported DateTime,
SourceRecord CHAR(4000),
ExceptionField VARCHAR(255),
ExceptionFieldValue VARCHAR(255),
ExceptionReason VARCHAR(255)
)
select * from [ski_int].[SSIS_Src_RawData]
TRUNCATE TABLE SBAB_src.StagingSourceFiles;
*/

	
alter PROCEDURE [SBAB_src].[Usp_RawDataExceptions_STI]
AS			

--INSERT INTO	
--	[ski_int].[SSIS_Src_RawData_Log]
--SELECT 
--	*
--FROM
--	SBAB_src.StagingSourceFiles;
--SELECT * FROM [ski_int].[SSIS_Src_RawData_Log]	



------------------------------------------------------------------------------------------------------------------------------------------------
--remove special characters first before validating any further
------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE
	ski_int.SSIS_Src_RawData
SET
	SourceRecord = ski_int.RemoveSpecialChars(SourceRecord);

------------------------------------------------------------------------------------------------------------------------------------------------
--##File Format Incorrect
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Fixed Width format',
	LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')),
	'File Format Fixed Width Lenth Incorrect'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%sti.newbus%'
	AND (
		LEN(REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),'')) < 719
		)
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;


------------------------------------------------------------------------------------------------------------------------------------------------
--##File Format Incorrect
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Record Id',
	SUBSTRING(SourceRecord,1,1),
	'Invalid Record Id'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	FileName like '%Sti.newbus%'
	AND ISNUMERIC(SUBSTRING(SourceRecord,1,1)) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;
------------------------------------------------------------------------------------------------------------------------------------------------
--##Invalid Loan Amount
--select * from SKI_INT.DailyImportExceptions
--TRUNCATE TABLE SKI_INT.DailyImportExceptions
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Loan Amount',
	REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','') AS [Loan Amount],
	'Invalid Numeric Value for Loan Amount'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord,1,1) = '2'
	AND FileName like '%sti.newbus%'
	AND ProcessStatus = 0
	AND ISNUMERIC(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','')) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1
	;
------------------------------------------------------------------------------------------------------------------------------------------------
--##Scheme Code was not supplied
------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Scheme Code',
	REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','') AS [Loan Amount],
	'Scheme Code blank/invalid'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord,1,1) = '2'
	AND FileName like '%sti.newbus%'
	AND ProcessStatus = 0
	AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 584, 2))) = ' '
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;

------------------------------------------------------------------------------------------------------------------------------------------------
--##Segment was not supplied
------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Segement Code',
	REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ','') AS [Loan Amount],
	'Segment Code blank/invalid'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord,1,1) = '2'
	AND FileName like '%sti.newbus%'
	AND ProcessStatus = 0
	AND RTRIM(LTRIM(SUBSTRING(SourceRecord, 709, 3))) = ' '
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;


------------------------------------------------------------------------------------------------------------------------------------------------
--##Date of Application Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Date of Application',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8))),
	'Date of Application Invalid'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%sti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;

------------------------------------------------------------------------------------------------------------------------------------------------
--##Date Of Birth Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Date Of Birth',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))),
	'Date Of Birth Invalid'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%sti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;

------------------------------------------------------------------------------------------------------------------------------------------------
--##Date Of Birth Invalid
------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO SKI_INT.DailyImportExceptions
SELECT 
	ID,
	FileName,
	ID,
	Country,
	DateImported,
	REPLACE(REPLACE(SourceRecord, CHAR(10),''), CHAR(13),''),
	'Repayment Date',
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8))),
	'Repayment Date Invalid'
FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND FileName like '%sti.newbus%'
	AND ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 568, 8)))) = 0
	AND NOT EXISTS(
				   SELECT * FROM SKI_INT.DailyImportExceptions EXC
				   WHERE EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				   AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
				   )
order by 
	1;

------------------------------------------------------------------------------------------------------------------------------------------------
--##Remove Exception Records from Source table
------------------------------------------------------------------------------------------------------------------------------------------------
DELETE 
--SELECT * 
	FROM 
	[ski_int].[SSIS_Src_RawData]
WHERE EXISTS(
			SELECT 
				* 
			FROM 
				SKI_INT.DailyImportExceptions EXC
			WHERE
				EXC.ImportRecordiID = [ski_int].[SSIS_Src_RawData].ID
				AND EXC.ImportFileName = [ski_int].[SSIS_Src_RawData].FileName
			);

