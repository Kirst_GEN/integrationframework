--------------------------------------------------------------------------------------------------
--Clear Data before Updating Config
--------------------------------------------------------------------------------------------------
DELETE FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'Currency_Code'

--------------------------------------------------------------------------------------------------
--Insert config
--------------------------------------------------------------------------------------------------
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','BWP','Currency_Code','8')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','ZAR','Currency_Code','0')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','KES','Currency_Code','6')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','USD','Currency_Code','1')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','EUR','Currency_Code','3')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','GBP','Currency_Code','2')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','SZL','Currency_Code','9')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','ZMW','Currency_Code','12')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','GHS','Currency_Code','13')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','UGX','Currency_Code','14')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','LSL','Currency_Code','11')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','MWK','Currency_Code','15')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','MT','Currency_Code','4')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','NAD','Currency_Code','10')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','SSP','Currency_Code','7')
insert into [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','Currency_Code','Currency_Code','TS','Currency_Code','5')


/*
SELECT * FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'Currency_Code'
ORDER BY 7
*/