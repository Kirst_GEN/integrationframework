
/****** Object:  UserDefinedFunction [dbo].[GetNAMHOCSplits]    Script Date: 13/09/2017 2:20:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetNAMHOCSplits](@FSI float, @FPremium float, @ID Int)
RETURNS Float
AS
BEGIN 
		DECLARE @RtnValue Float
		DECLARE @SI as Float
		DECLARE @FilePremium as Float
		DECLARE @Ratio as Float
		DECLARE @Nasria as Float
		DECLARE @PolicyFee as Float
		DECLARE @StampDuty as Float
		DECLARE @Namfisa as Float
		DECLARE @NamfisaComm as Float

		SET @SI = @FSI
		SET @FilePremium = @FPremium--515.35

		SET @Ratio = 116.9941
		SET @PolicyFee = 10 
		SET @Nasria = @SI * 0.00001 * 1.15
		SET @StampDuty = case when (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio > 20.83 then 20.83 else (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio end
		SET @Namfisa = (@FilePremium - (@SI * 0.00001 * 1.15) - @PolicyFee) / @Ratio
		SET @NamfisaComm = @SI * 0.00001 * 0.01
		SET @RtnValue = 0
   
		select @RtnValue = Case 

							when @Id = 1 then --SI
								cast(round(@SI,2)  as money)
							when @Id = 2 then --File Premium
								cast(round(@FilePremium,2)  as money)
							when @Id = 3 then --Nasria
								cast(round(@Nasria,2) as money)
							when @Id = 4 then --StampDuty
								cast(round(@StampDuty,2)  as money)
							when @Id = 5 then --Namfisa
								cast(round(@Namfisa,2)  as money)
							when @Id = 6 then --NamfisaComm
								cast(round(@SI * 0.00001 * 0.01,2) as money)
							when @Id = 7 then --RiskCPCPremium
								cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm - @Nasria)*12,2)  as money)
							when @Id = 8 then --RiskNasriaCPC
								cast(round(@Nasria*12,2) as money)
							when @Id = 9 then --RiskHeaderComm
								cast(round((@FilePremium - @StampDuty - @Namfisa - @NamfisaComm)* 0.2,2)  as money)
						   End


       RETURN @RtnValue
END
