--------------------------------------------------------------------------------------------------
--Clear Data before Updating Config
--------------------------------------------------------------------------------------------------
DELETE FROM [ski_int].[SSISIntegrationLookupMap]
WHERE Type = 'CoInsurer_LTI'

--------------------------------------------------------------------------------------------------
--Insert config
--------------------------------------------------------------------------------------------------
INSERT INTO [ski_int].[SSISIntegrationLookupMap]
VALUES ('SBAB','CoInsurer_LTI','CoInsurer','NA','CoInsurer_NAM','LIBERTYNAM')

		
Update L
		set L.RH_COInsurer =  M.TargetDBValue 
	from SBAB_src.DailyLTINewBusiness L 
	join SKI_int.SSISIntegrationLookupMap M on M.Type = 'CoInsurer_STI' 
		and SourceGroup = 'CoInsurer' 
		and SourceDBValue = sCountry

/*
SELECT * FROM [ski_int].[IntegrationLookupMap]
WHERE Type = 'CoInsurer_LTI'
ORDER BY 7
*/