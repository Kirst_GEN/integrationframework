USE [SBAB_082017]
GO
/****** Object:  StoredProcedure [SBAB_src].[Usp_STIPremiums_New]    Script Date: 02/10/2017 12:18:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [SBAB_src].[Usp_STIPremiums] 
	
AS
BEGIN
-------------------------------------------------------------------------------
--Delete exising temp tables 
-------------------------------------------------------------------------------
--IF OBJECT_ID('tempdb..##Temp') IS NOT NULL
--DROP TABLE ##Temp;
--IF OBJECT_ID('tempdb..##TempInvalidDate') IS NOT NULL
--DROP TABLE ##TempInvalidDate;
--IF OBJECT_ID('tempdb..##TempInvalidNumeric') IS NOT NULL
--DROP TABLE ##TempInvalidNumeric;
------------------------------------------------------------------------------------------------------
--Create Variable @Dateto restirct only todays records
------------------------------------------------------------------------------------------------------

DECLARE @Date date = (SELECT CONVERT(DATE,DATEADD(DD,0,GETDATE())))

-------------------------------------------------------------------
--Get all validated records
-------------------------------------------------------------------
--SELECT *
--INTO ##Temp
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and ISDATE(SUBSTRING(SourceRecord,2,8)) = 1
--		and ISDATE(SUBSTRING(SourceRecord,253,8)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,46,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 1
--		and ISNUMERIC(SUBSTRING(SourceRecord,137,20)) = 1
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Get all invalid date records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidDate
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and (ISDATE(SUBSTRING(SourceRecord,2,8)) = 0
--		or ISDATE(SUBSTRING(SourceRecord,253,8)) = 0)
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Get all invalid numeric records
-------------------------------------------------------------------
--SELECT *
--INTO ##TempInvalidNumeric
--FROM [SBAB_src].[StagingSourceFiles]
--WHERE sFileName like '%sti.premium%'
--		and SUBSTRING(SourceRecord,1,1) = 2
--		and iProcessStatus = 0
--		and (ISNUMERIC(SUBSTRING(SourceRecord,46,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,117,20)) = 0
--		or ISNUMERIC(SUBSTRING(SourceRecord,137,20)) = 0)
--		and CONVERT(DATE,dtDate) = @Date		
-------------------------------------------------------------------
--Insert the data for the latest records
-------------------------------------------------------------------

INSERT INTO [SBAB_src].[STIPremium]
SELECT 
					SUBSTRING(SourceRecord,1,1) Record_Id,
					CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,2,8)))) Transaction_Date ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))) Loan_Account_No	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,26,20))) ID_Number_Business_Reg_Number,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,46,20))) = '',0,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,46,20))))) Premium_Received ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,66,50))) CIF_Number	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,116,1))) Balance_Sign,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,117,20))) = ' ' ,NULL,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,117,20))))) Property_Value ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,137,20))) = '',0,CONVERT(NUMERIC(18,2),RTRIM(LTRIM(SUBSTRING(SourceRecord,137,20))))) Premium_due ,
					Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O') Scheme_code ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,159,3))) = ' ' ,NULL,CASE RTRIM(LTRIM(SUBSTRING(SourceRecord,159,3))) 
																					WHEN 'BWP' THEN '8'
																					WHEN 'KES' THEN '6'
																					WHEN 'USD' THEN '1'
																					WHEN 'EUR' THEN '3'
																					WHEN 'GBP' THEN '2'
																					WHEN 'SZL' THEN '9'
																					WHEN 'GHS' THEN '13'
																					WHEN 'ZMW' THEN '12'
																					WHEN 'ZAR' THEN '0'
																					WHEN 'UGX' THEN '14'
																					WHEN 'LSL' THEN '11'
																					WHEN 'MWK' THEN '15'
																					WHEN 'NAD' THEN '10'
																				  ELSE NULL 
																				END) Currency_Code,
					IIF(SUBSTRING(SourceRecord,162,30) = ' ' ,NULL,SUBSTRING(SourceRecord,162,30)) Customer_Title	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,192,30))) Customer_First_Name	,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,222,30))) Customer_Surname ,
					RTRIM(LTRIM(SUBSTRING(SourceRecord,252,1))) Gender	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,253,8))) = ' ' ,NULL,CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,253,8))))) Date_of_Birth ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,261,20))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,261,20)))) ID_Passport_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,281,30))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,281,30)))) Postal_Address_1,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,311,30))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,311,30)))) Postal_Address_2,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,341,30))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,341,30)))) Postal_Address_3,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,371,10))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,371,10)))) Postal_Code,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,381,25))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,381,25)))) Tel_No,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,406,5))) = ' ',NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,406,5)))) Marital_Status ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,411,30))) = ' ' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,411,30)))) Phys_Add1,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,441,30))) = ' ' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,441,30)))) Phys_Add2,	
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,471,30))) = ' ' , NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,471,30)))) Phys_Add3,	
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,501,5))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,501,5)))) Product_Scheme_Code ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,506,1))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,506,1)))) Status_Flag ,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,507,1))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,507,1)))) Exception,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,508,2))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,508,2)))) Plan_Type,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,510,5)))) Serial_Number	,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,515,20))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,515,20)))) Underwriter_Name,
					IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,535,10))) = ' ' ,NULL,RTRIM(LTRIM(SUBSTRING(SourceRecord,535,10))))	PIN_number	,
					-1 iImportRecNo,
					convert(datetime,substring(FileName,21,6)) ProcessDate,
					[ski_int].[SSIS_Src_RawData].ID iSourceFileID,
					CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O')) sNewLoanAccNo,
					Country
					,LMPProductID.TargetDBValue
					,ski_int.SBAB_GetPolicyID (RTRIM(LTRIM(SUBSTRING(SourceRecord,66,50))), CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord,10,16))),RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2)))), LMPProductID.TargetDBValue)
					,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 66, 50)))) Country_Cif
					,COU.Id
	FROM [ski_int].[SSIS_Src_RawData]
		LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
			ON LMPProductID.SourceDBValue = Replace(RTRIM(LTRIM(SUBSTRING(SourceRecord,157,2))),'D','O') +'_' + Country
			AND LMPProductID.Type = 'Product_Code'
			AND LMPProductID.SourceGroup = 'Scheme_Code'
		LEFT JOIN CFG.Country COU on COU.Alpha2Code = [ski_int].[SSIS_Src_RawData].Country
WHERE FileName like '%sti.premium%'
		and SUBSTRING(SourceRecord,1,1) = 2
		and ProcessStatus = 0
		and CONVERT(DATE,DateImported) = @Date		

---------------------------------------------------------------------------------------------------------------------------
--Update the source record to processed to eliminate processing duplicates
---------------------------------------------------------------------------------------------------------------------------
--Validated Records
------------------------------------------------------------------
UPDATE SSF
		SET ProcessStatus = 1
	FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [SBAB_src].[STIPremium] TMP
					on TMP.iSourceFileID = SSF.ID
WHERE SSF.FileName like '%sti.premium%'
		and CONVERT(DATE,SSF.DateImported) = @Date;
------------------------------------------------------------------
--Invalid date records
------------------------------------------------------------------
--UPDATE SSF
--		SET iProcessStatus = 2
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidDate TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.sFileName like '%sti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date;
------------------------------------------------------------------
--Invalid numeric records
------------------------------------------------------------------
--UPDATE SSF
--		SET iProcessStatus = 3
--	FROM SBAB_src.StagingSourceFiles SSF
--					JOIN ##TempInvalidNumeric TMP
--					on TMP.iID = SSF.iID
--WHERE SSF.sFileName like '%sti.premium%'
--		and CONVERT(DATE,SSF.dtDate) = @Date;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--IF ( SELECT TOP 1 sFileName
--			FROM SBAB_Src.StagingFileProcessAudit
--		 WHERE sFileName like '%sti.premiums%'
--				and CONVERT(DATE,dtDate) = @Date) IS NOT NULL

--BEGIN

--	DELETE FROM SBAB_Src.StagingFileProcessAudit
--					WHERE sFileName like '%sti.premiums%'
--							and CONVERT(DATE,dtDate) = @Date
--END

INSERT INTO SBAB_Src.StagingFileProcessAudit
SELECT  SUBSTRING(SourceRecord,1,1) Record_ID,
					iif(RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12))) = ' ',0,RTRIM(LTRIM(SUBSTRING(SourceRecord,21,12)))) Count
					,filename FileName
					,DateImported ProcessDate
					,Country
					,CONVERT(FLOAT,SUBSTRING(SourceRecord,2,20)) TotalPremiumsReceived				
FROM [ski_int].[SSIS_Src_RawData]
 WHERE SUBSTRING(SourceRecord,1,1) = 9
 and FileName like '%sti.premium%'
 and CONVERT(DATE,DateImported) = @Date

EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%sti.premium%'
		and CONVERT(DATE,dtDate) = @Date
 UNION
-------------------------------------------------------------------------------------------------------------------------------------------------
--Create a record for audit table with count of records for the specific file,country and date
-------------------------------------------------------------------------------------------------------------------------------------------------
SELECT	0 AS Record_ID,
					COUNT(*) Count,
					FileName FileName,
					DateImported ProcessDate,
					SSF.Country Country,
					SUM(cPremium_Received) TotalPremiumsReceived
FROM [ski_int].[SSIS_Src_RawData] SSF
					JOIN [SBAB_src].[STIPremium] TMP
					on TMP.iSourceFileID = SSF.ID 
WHERE FileName like '%sti.premium%'
		and CONVERT(DATE,DateImported) = @Date
 GROUP BY FileName,DateImported,SSF.Country

 EXCEPT

 SELECT		Record_ID,
						Count,
						sFileName,
						dtDate,
						sCountry,
						cTotalPremiumReceived
 FROM SBAB_Src.StagingFileProcessAudit
 WHERE sFileName like '%sti.premium%'
		and CONVERT(DATE,dtDate) = @Date
 

TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];

SET NOCOUNT ON;
END


