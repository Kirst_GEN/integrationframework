ALTER PROCEDURE [SBAB_src].[Usp_STINewBusiness_PostUpdate]
AS
BEGIN


DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, 0, GETDATE())))


--------------------------------------------------------------------------------------------
----STI New Business
--------------------------------------------------------------------------------------------
-- Risk Premium
-----------------
UPDATE RSK
SET  cPremium = src.cPremium, cSumInsured = ABS(cProperty_Value)
	, cAnnualPremium = CASE RSK.iPaymentTerm 
							WHEN 3 THEN SRC.cPremium * 12 
							WHEN 0 THEN SRC.cPremium 
							WHEN 1 THEN SRC.cPremium * 2 
							WHEN 2 THEN SRC.cPremium * 4
							WHEN 4 THEN SRC.cPremium 
						END
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = iPolicyID
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Comm Structs
-----------------
UPDATE 
	PC
SET 
	PC.iLev2EntityID = CE.iCommEntityID,
	PC.iCommEntityRelID = CSER.iCommRelID
FROM 
	Policy Pol
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = POL.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	JOIN SBAB_src.DailySTINewBusiness DL 
		ON DL.sNewLoanAccountNo = POL.sOldPolicyNo
		AND POL.CountryId = DL.iCountryID
		AND CD.sFieldValue = DL.sCountry_Cif
		AND POL.iProductID = DL.PH_ProductID
	JOIN PolicyDetails PD 
		ON POL.iPolicyID = PD.iPolicyID 
		AND PD.sFieldCode = 'P_SERIALNUMBER' 
		AND sSerial_Number = PD.sFieldValue
	JOIN PolicyCommStruct PC (NOLOCK) 
		ON PC.iVersionNo = Pol.iAgencyID
	JOIN CommEntities CE (NOLOCK) 
		ON CE.sCode = case 
							when len(DL.sSOL_ID) = 5 then '0'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 4 then '00'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 3 then '000'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 2 then '0000'+ (DL.sSOL_ID)
							when len(DL.sSOL_ID) = 1 then '00000'+ (DL.sSOL_ID)
							else DL.sSOL_ID
						end
	JOIN CommStructEntityRel CSER (NOLOCK) 
		on CSER.iLev1EntityID = PC.iLev1EntityID and CSER.iLev2EntityID = CE.iCommEntityID and CSER.iCommStructID = PC.iCommStructID
											  and CSER.iLev3EntityID = PC.iLev3EntityID and CSER.iLev4EntityID = PC.iLev4EntityID and CSER.iLev5EntityID = PC.iLev5EntityID
WHERE 
	PC.iLev2EntityID != CE.iCommEntityID
	AND 
	CONVERT(DATE,dtProcessDate) = @Date;



-----------------
--Company Name
-----------------
UPDATE CUST
SET sContactName = sCompanyName
FROM Customer CUST
JOIN Policy POL
	ON CUST.iCustomerID = POL.iCustomerID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = iPolicyID
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	bCompanyInd = 1
	AND sContactName = ''
	AND CONVERT(DATE,dtProcessDate) = @Date;

----------------------------------------------------------------------------------------------------------------------------------------
--CANCEL POLICY WHERE THE REASON CODE IS C OR S
----------------------------------------------------------------------------------------------------------------------------------------
UPDATE POL
SET iPolicyStatus = 2, sComment = 'Cancelled due to STI Update', dDateModified = getdate(), dEffectiveDate = getdate()
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE sReason_code in ('C','S')
	AND CONVERT(DATE,dtProcessDate) = @Date
	AND iParentID = -1;
-----------------
--Vat rate
-----------------
UPDATE POL
SET iTaxPerc = 1.00
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE
	sCountry <> 'NA'
	AND ISNULL(iTaxPerc,0.00) <> 1.00
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Vat rate UGANDA
-----------------
UPDATE POL
SET iTaxPerc = 1.18
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'UG'
	AND ISNULL(iTaxPerc,0.00) <> 1.18
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Vat rate NAMIBIA
-----------------
UPDATE POL
SET iTaxPerc = 1.15
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
WHERE 
	sCountry = 'NA'
	AND ISNULL(iTaxPerc,0.00) <> 1.15
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk review status
-----------------
UPDATE RSK
SET eReviewStatus = 0
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN Risks RSK
	ON intPolicyID  = iPolicyID
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE eReviewStatus = 1
	AND CONVERT(DATE,dtProcessDate) = @Date;


-----------------
--Payment method
-----------------
UPDATE POL
SET iPaymentMethod = 2
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Endorsement code AND REASON
-----------------
UPDATE POL
SET sEndorseCode = 17, sComment = 'Imported Policy'
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy renewal date
-----------------
UPDATE POL
SET dRenewalDate = dateadd(dd,1,dExpiryDate)
--select *
FROM Policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;
-----------------
--Policy commission New Business- Kenya
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy commission New Business- Botswana
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium *20/100)),2) ,cCommissionPaid = 0.2
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy commission New Business- Zim
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*20/100),2) ,cCommissionPaid = 0.20
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy commission New Business- Zambia
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*35/100),2) ,cCommissionPaid = 0.35
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy commission New Business- Ghana
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium)*15/100),2) ,cCommissionPaid = 0.15
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2) ,cCommissionPaid = 0.08,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Kenya
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Botswana
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium )*20/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Namibia
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium )*20/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'NA'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Zim
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*20/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZW'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Zambia
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*35/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Risk Commission New Business- Ghana
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium)*15/100),2)
-- select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)*8/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE
	sCountry = 'GH'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- ITET(Training Levy) New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_ITETTRALEVFLA'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--PHCF (Policy holder compensation fund) New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.25/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.25/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_PHCFPOLHOLCO0'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Training Levy Amount - Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 0.5/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
-- Training Levy Percentage - Uganda
-----------------
UPDATE PD
SET cFieldValue = 0.5
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_TRAINLEVYPERC'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-----------------
-- Training Levy Amount - Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,RSK.cPremium* 1.5/100),2)
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_IRALEVYAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE 
	sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date;
-----------------
-- IRA Levy Percentage - Uganda
-----------------
UPDATE PD
SET cFieldValue = 1.5
--select ROUND(CONVERT(FLOAT,RSK.cPremium* 0.2/100),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_IRALEVYPERC'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
-- Policy admin fee New Business - Kenya
-----------------
UPDATE POL
SET cAdminFee = PD.cFieldValue *(12/fValue)
--select cFieldValue *(12/fValue),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_ITETTRALEVFLA'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
-- Policy admin fee New Business - Uganda
-----------------
UPDATE POL
SET cAdminFee = case when iCurrency = 14 then 35000 else 11 end/fValue
--select cFieldValue *(12/fValue),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Policy Fee New Business- Kenya
-----------------
UPDATE POL
SET cPolicyfee = PD.cFieldValue *(12/fValue)
--select cFieldValue *12,*(12/fValue)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_PHCFPOLHOLCO0'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Admin Fee New Business - Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100,2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_ADMFLAT1'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Withholding tax New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Withholding tax New Business- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(cCommissionAmt*10/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Withholding tax New Business- Botswana
-----------------
UPDATE PD
SET cFieldValue = 10
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_WITHHTAXPERC'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Vat- Botswana
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(cCommissionAmt*12/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Vat- Botswana
-----------------
UPDATE PD
SET cFieldValue = 12
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'BW'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Excise Tax New Business- Kenya
-----------------
UPDATE PD
SET cFieldValue = ROUND(((CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100)+
						(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100)) *10/100,2)--/(12/fValue)
--select ROUND(((CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100)+
						--(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*12/100)) *10/100,2)--/(12/fValue)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_EXCISTAXAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Risk Premium less ITET and PHCF New Business - Kenya
-----------------
UPDATE RSK
SET  cPremium = ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100)),2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'KE'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Vat- Uganda
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,((RSK.cPremium-case when icurrency = 14 then 35000 else 11 end/12/fValue)*18/100)),2)
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATAMOUNT'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Vat- Uganda
-----------------
UPDATE PD
SET cFieldValue = 18
--select ROUND(CONVERT(FLOAT,(RSK.cPremium - RSK.cPremium* 0.2/100 - RSK.cPremium* 0.25/100))*8/100*10/100,2),*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_VATPERCEN'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Stamp Duty New Business- Swaziland
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
--select ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_STAMPDUTYAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Insurance Levy Amount - Zambia
-----------------
UPDATE PD
SET cFieldValue = ROUND(CONVERT(FLOAT,(lti.cPremium*3/100)),2)
--select ROUND(CONVERT(FLOAT,(iif(RSK.cPremium*10/100 > 60,60,RSK.cPremium*10/100))),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN PolicyDetails PD
	ON PD.iPolicyID = POL.iPolicyID
	AND PD.sFieldCode = 'P_INSURLEVYAMOU'
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'ZM'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Risk Premium less Admin Fee New Business - Uganda
-----------------
UPDATE RSK
SET  cPremium = ROUND(CONVERT(FLOAT,(LTI.cPremium-case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
--select rsk.cPremium,ROUND(CONVERT(FLOAT,(LTI.cPremium-case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Risk Admin Fee New Business - Uganda
-----------------
UPDATE RSK
SET  cAdminFee = ROUND(CONVERT(FLOAT,(case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
--select rsk.cPremium,ROUND(CONVERT(FLOAT,(case when icurrency = 14 then 35000 else 11 end/12/fValue)),2)
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
JOIN Lookup
	ON RSK.iPaymentTerm = iIndex
	AND sGroup = 'Payment Term'
WHERE sCountry = 'UG'
	AND CONVERT(DATE,dtProcessDate) = @Date
;

-----------------
--Policy premium New Business
-----------------
UPDATE P
SET
 P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF' 
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;

-----------------
--Policy commission New Business- Swaziland
-----------------
UPDATE POL
SET cCommissionAmt = ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100 )),2) ,cCommissionPaid = 0.175
--select RSK.cPremium,ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100 )),2) ,cCommissionPaid = 0.175,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date
;
-----------------
--Risk commission New Business- Swaziland
-----------------
UPDATE RSK
SET cCommission = ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100 )),2) ,cCommissionPerc = 0.175
--select RSK.cPremium,ROUND(CONVERT(FLOAT,(RSK.cPremium * 17.5/100 )),2) ,cCommissionPaid = 0.175,*
FROM policy POL
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN risks RSK
	ON iPolicyID = intPolicyID
JOIN SBAB_src.DailySTINewBusiness LTI
	ON sOldPolicyNO = sNewLoanAccountNo
	AND POL.CountryId = LTI.iCountryID
	AND CD.sFieldValue = LTI.sCountry_Cif
	AND POL.iProductID = LTI.PH_ProductID
JOIN PolicyDetails PDSERNo
	ON POL.iPolicyID = PDSERNo.iPolicyID
	AND PDSERNo.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PDSERNo.sFieldValue
WHERE sCountry = 'SZ'
	AND CONVERT(DATE,dtProcessDate) = @Date;

-------------------------------------------------------------
--Risks inception date - LTI
-------------------------------------------------------------
UPDATE R
SET R.dInception = dCoverStart
FROM Risks R
JOIN Policy POL
	ON intPolicyID = iPolicyID
JOIN CustomerDetails CD 
	ON CD.iCustomerID = POL.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND POL.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND POL.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON POL.iPolicyID = PD.iPolicyID
	AND PD.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,R.dInception) <> CONVERT(DATE,dCoverStart);

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update RiskDetails
------------------------------------------------------------------------------------------------------------------------------------------------------------------

--select p.spolicyno,rd.sFieldCode,rd.sFieldValue,FileSI,
--	case when rd.sfieldcode = 'R_LIABISUMINSU' then ltrim(rtrim(str(FileSI,25,2) + ' @ 0.000 @ 100.00000 [' + cast(RiskPremCPC as varchar) + ']'))
--		 when rd.sfieldcode = 'R_TSASRIA' then  ltrim(rtrim(str(FileSI,25,2) + ' @ 0.000 @ 100.00000 [' + cast(RiskNasriaCPC as varchar) + ']'))
--	end as 'New'

update rd
set rd.sfieldvalue = case when rd.sfieldcode = 'R_LIABISUMINSU' then ltrim(rtrim(str(cProperty_value,25,2) + ' @ 0.000 @ 100.00000 [' + cast(RiskPremCPC as varchar) + ']'))
						  when rd.sfieldcode = 'R_TSASRIA' then  ltrim(rtrim(str(cProperty_value,25,2) + ' @ 0.000 @ 100.00000 [' + cast(RiskNasriaCPC as varchar) + ']'))
					 end 
from Policy p (nolock)
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = P.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	join Risks r (nolock) 
		on r.intPolicyID = p.ipolicyid
	join RiskDetails rd (nolock) 
		on rd.iriskID = r.intRiskID
	JOIN SBAB_src.DailySTINewBusiness SRC
		ON sOldPolicyNo = sNewLoanAccountNo
		AND p.CountryId = SRC.iCountryID
		AND CD.sFieldValue = SRC.sCountry_Cif
		AND p.iProductID = SRC.PH_ProductID
	JOIN PolicyDetails PD
		ON p.iPolicyID = PD.iPolicyID
		AND PD.sFieldCode = 'P_SERIALNUMBER'
		AND sSerial_Number = PD.sFieldValue
	--join Tmp_HOCUpdates aa (nolock) on aa.ipolicyid = p.iPolicyID	
where rd.sFieldCode in ('R_LIABISUMINSU','R_TSASRIA')
	and r.intstatus = 2
	AND P.IPRODUCTID IN (11201) --NA - HOC
	AND CONVERT(DATE,dtProcessDate) = @Date;


------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update Risk Header
------------------------------------------------------------------------------------------------------------------------------------------------------------------

--select r.cSumInsured,SRC.cProperty_value
--	   ,r.cPremium, round((RiskPremCPC + RiskNasriaCPC) /12,2)
--	   ,r.cSASRIA, round(RiskNasriaCPC /12,2)
--	   ,r.cCommission,round(RiskComm,2)
--	   ,r.cAnnualPremium,round(RiskPremCPC + RiskNasriaCPC,2)
--	   ,r.cAnnualSASRIA, round(RiskNasriaCPC,2)
--	   ,r.cAnnualComm, round(RiskComm * 12,2)
--	   ,*
update r
set r.cSumInsured = SRC.cProperty_value
	   ,r.cPremium = round((RiskPremCPC + RiskNasriaCPC) /12,2)
	   ,r.cSASRIA = round(RiskNasriaCPC /12,2)
	   ,r.cCommission = round(RiskComm,2)
	   ,r.cAnnualPremium =round(RiskPremCPC + RiskNasriaCPC,2)
	   ,r.cAnnualSASRIA = round(RiskNasriaCPC,2)
	   ,r.cAnnualComm = round(RiskComm * 12,2)
from Policy p (nolock)
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = P.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	JOIN SBAB_src.DailySTINewBusiness SRC
		ON sOldPolicyNo = sNewLoanAccountNo
		AND p.CountryId = SRC.iCountryID
		AND CD.sFieldValue = SRC.sCountry_Cif
		AND p.iProductID = SRC.PH_ProductID
	--join Tmp_HOCUpdates aa (nolock) on aa.ipolicyid = p.iPolicyID
	join Risks r (nolock) on r.intPolicyID = p.ipolicyid
	JOIN PolicyDetails PD
		ON p.iPolicyID = PD.iPolicyID
		AND PD.sFieldCode = 'P_SERIALNUMBER'
		AND sSerial_Number = PD.sFieldValue
where 
	r.intstatus = 2
	AND P.IPRODUCTID IN (11201) --NA - HOC
	AND CONVERT(DATE,dtProcessDate) = @Date;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Add Policy Details FOR MISSING FIELD CODES
------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into PolicyDetails
select iPolicyID
,sFieldCode
,iFieldType
,''
,0
,0
,'1900-01-01'
,0
,0
,iPolicyID
,cfg.NewCombGUID()
from policy p
CROSS JOIN FieldCodes fc
WHERE p.iProductID = 11201
	AND sFieldCode in ('P_STAMPDUTYAMOU','P_NAMFILEVYAMOU','P_NASRINAMFLEVY','P_NASRIACOMMIS')
	AND p.iPolicyStatus < 2
	AND NOT EXISTS (select null from PolicyDetails pd where pd.iPolicyID = p.iPolicyID and pd.sFieldCode = fc.sfieldcode)
	AND P.IPRODUCTID IN (11201) --NA - HOC

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update Policy Details
------------------------------------------------------------------------------------------------------------------------------------------------------------------

--select p.iPolicyID,pd.sFieldCode,pd.ifieldtype,pd.cfieldvalue,
--	case when pd.sFieldCode = 'P_STAMPDUTYAMOU' then SRC.StampDuty
--		 when pd.sFieldCode = 'P_NAMFILEVYAMOU' then SRC.Namfisa
--		 when pd.sFieldCode = 'P_NASRINAMFLEVY' then SRC.NamfisaComm
--		 when pd.sFieldCode = 'P_NASRIACOMMIS' then SRC.Nasria * 0.2
--	end 
update pd
set pd.cFieldValue = case when pd.sFieldCode = 'P_STAMPDUTYAMOU' then SRC.StampDuty
						  when pd.sFieldCode = 'P_NAMFILEVYAMOU' then SRC.Namfisa
						  when pd.sFieldCode = 'P_NASRINAMFLEVY' then SRC.NamfisaComm
						  when pd.sFieldCode = 'P_NASRIACOMMIS' then SRC.Nasria * 0.2
					  end
from Policy p (nolock)
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = P.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	JOIN SBAB_src.DailySTINewBusiness SRC
		ON sOldPolicyNo = sNewLoanAccountNo
		AND p.CountryId = SRC.iCountryID
		AND CD.sFieldValue = SRC.sCountry_Cif
		AND p.iProductID = SRC.PH_ProductID
	--join Tmp_HOCUpdates aa (nolock) on aa.ipolicyid = p.iPolicyID
	JOIN PolicyDetails pd (nolock) on pd.iPolicyID = p.iPolicyID
	JOIN PolicyDetails PDS
		ON p.iPolicyID = PDS.iPolicyID
		AND PDS.sFieldCode = 'P_SERIALNUMBER'
		AND sSerial_Number = PDS.sFieldValue 
where 
	pd.sFieldCode in ('P_STAMPDUTYAMOU','P_NAMFILEVYAMOU','P_NASRINAMFLEVY','P_NASRIACOMMIS')
	AND P.IPRODUCTID IN (11201) --NA - HOC
	AND CONVERT(DATE,dtProcessDate) = @Date;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update Policy Header
------------------------------------------------------------------------------------------------------------------------------------------------------------------

--select distinct p.iPolicyID,p.cBrokerFee,p.cPolicyFee,
--	aa.StampDuty,aa.Namfisa + aa.NamfisaComm
update p
set p.cBrokerFee = SRC.StampDuty * 12
	,p.cPolicyFee = (SRC.Namfisa + SRC.NamfisaComm) * 12
from Policy p (nolock)
	JOIN CustomerDetails CD 
		ON CD.iCustomerID = P.iCustomerID
		AND CD.sFieldCode = 'C_COUNTRYCIF'
	--join Tmp_HOCUpdates aa (nolock) on aa.ipolicyid = p.iPolicyID
	JOIN PolicyDetails pd (nolock) on pd.iPolicyID = p.iPolicyID 
	JOIN SBAB_src.DailySTINewBusiness SRC
		ON sOldPolicyNo = sNewLoanAccountNo
		AND p.CountryId = SRC.iCountryID
		AND CD.sFieldValue = SRC.sCountry_Cif
		AND p.iProductID = SRC.PH_ProductID
	JOIN PolicyDetails PDS
		ON p.iPolicyID = PDS.iPolicyID
		AND PDS.sFieldCode = 'P_SERIALNUMBER'
		AND sSerial_Number = PDS.sFieldValue 
WHERE 
	P.IPRODUCTID IN (11201) --NA - HOC
	AND CONVERT(DATE,dtProcessDate) = @Date;
--where p.sPolicyNo = 'HOC1074575'



------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update Policy Header
------------------------------------------------------------------------------------------------------------------------------------------------------------------

--UPDATE p
--SET p.cNextPaymentAmt = aa.cNextPaymentAmt
--,p.cAnnualPremium     = aa.cAnnualPremium
--,p.cCommissionAmt     = aa.cCommissionAmt
--,p.cAnnualSasria    = aa.cAnnualSasria
--FROM dbo.Policy p 
--join Tmp_HOCUpdates bb (nolock) on bb.ipolicyid = p.iPolicyID
--JOIN (
--                                          SELECT r.intClientID
--                                          ,r.intPolicyid
--                                          ,r.iProductID
--                                          ,SUM(r.cPremium) cNextPaymentAmt
--                                          ,SUM(r.cAnnualPremium) cAnnualPremium
--                                          ,SUM(r.cAnnualComm) cCommissionAmt
--                                          ,SUM(r.cAnnualsasria) cAnnualSasria
--                                          FROM dbo.Risks r
--                                          WHERE r.intStatus IN (2,3)
--                                          GROUP BY r.intClientID, r.intPolicyid, r.iProductID
--                                    ) aa ON aa.intPolicyid = p.iPolicyid 
--WHERE aa.intClientID = p.iCustomerid 
--  AND aa.iProductid  = p.iProductid 
--  --and p.sPolicyNo = 'HOC1074575'

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Update Policy Header
------------------------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Policy premium
------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE P
SET
 P.cNextPaymentAmt = R.cPremium
,P.cAnnualPremium  = R.cAnnualPremium
,P.cCommissionAmt  = R.cCommissionAmt
,P.cAnnualSasria   = R.cAnnualSasria
FROM dbo.Policy P JOIN (
                        SELECT intClientid,intPolicyid,iProductid
                        ,SUM(cPremium) cPremium
                        ,SUM(cAnnualPremium) cAnnualPremium
                        ,SUM(cAnnualSasria)  cAnnualSasria
                        ,SUM(cAnnualComm)    cCommissionAmt
                        FROM dbo.Risks
                        GROUP BY  intClientid,intPolicyid,iProductid
        ) R 
	ON R.intPolicyid = P.iPolicyid 
JOIN CustomerDetails CD 
	ON CD.iCustomerID = P.iCustomerID
	AND CD.sFieldCode = 'C_COUNTRYCIF'
JOIN SBAB_src.DailySTINewBusiness SRC
	ON sOldPolicyNo = sNewLoanAccountNo
	AND P.CountryId = SRC.iCountryID
	AND CD.sFieldValue = SRC.sCountry_Cif
	AND P.iProductID = SRC.PH_ProductID
JOIN PolicyDetails PD
	ON P.iPolicyID = PD.iPolicyID
	AND pd.sFieldCode = 'P_SERIALNUMBER'
	AND sSerial_Number = PD.sFieldValue
WHERE 
	CONVERT(DATE,dtProcessDate) = @Date;

END