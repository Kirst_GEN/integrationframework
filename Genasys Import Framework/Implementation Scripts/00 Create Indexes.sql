CREATE NONCLUSTERED INDEX [NCI_ImportRecord_iImportID_eProcStatus]
ON [dbo].[ImportRecord] ([iImportID],[eProcStatus])
INCLUDE ([iImportRecNo])
GO


CREATE NONCLUSTERED INDEX [NCI_DailyLTINewBusiness_iImportRecNo]
ON [SBAB_src].[DailyLTINewBusiness] ([iImportRecNo])
INCLUDE ([sRecord_Id],[dtTransaction_Date],[sReason_Code],[sCIF_Number],[sCustomer_Title],[sCustomer_First_Name],[sCustomer_Surname],[sGender],[dtDate_of_Birth],[sID_Passport_Number],[sPostal_Address_1],[sPostal_Address_2],[sPostal_Address_3],[sPostal_Code],[sTel_No],[sEmployer_Name],[sEmployer_Contact_Tel_No],[sMarital_Status],[sLoan_Account_No],[sSOL_ID],[cLoan_Amount],[sLoan_Term],[sPlan_Type],[dtLoan_Value_Date],[cPremium],[dtRepayment_Date],[sSecond_Life_Insured_Title],[sSecond_Life_Insured_First_Name],[sSecond_Life_Insured_Surname],[sSecond_Life_Insured_ID_Passport_No],[sSecond_Life_DOB],[sLTI_Flag],[dtLTI_Expiry],[cOutstanding_Balance],[sScheme_Code],[sCurrency_Code],[sInsurer_Own_insurance],[sPolicy_ID_Own_insurance],[cAmount_Own_insurance],[dtExpiry_Date_Own_Insurance],[sProduct_Scheme_Code],[sSegment],[sSub_segment],[sSerial_Number],[sUnderwriter_Name],[sPIN_number],[iID],[dtProcessDate],[iSourceFileID],[sNewLoanAccountNo],[sCountry],[sCountryClassification],[sCountry_Cif],[iCountryID],[RH_CoInsurer],[PH_ProductID])




