
/****** Object:  StoredProcedure [SBAB_src].[Usp_LTINewBusiness_New]    Script Date: 29/09/2017 9:59:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [SBAB_src].[Usp_LTINewBusiness]
AS
BEGIN
------------------------------------------------------------------------------------------------------
--alter Variable @Date to restirct only the latest records
------------------------------------------------------------------------------------------------------

EXEC  [SBAB_src].[Usp_RawDataExceptions_LTI];

DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, 0, GETDATE())))
-------------------------------------------------------------------
--Insert the data for the latest records
--select * from [SBAB_src].[DailyLTINewBusiness]
-------------------------------------------------------------------

INSERT INTO [SBAB_src].[DailyLTINewBusiness]
SELECT 
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 1, 1))) Record_Id
	,CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 2, 8)))) Transaction_Date
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 10, 2))) Reason_Code
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50))) CIF_Number
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 62, 30))) Customer_Title
	,IIF(RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 92, 30))) = ' ', '.', RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 92, 30)))) Customer_First_Name
	,IIF(RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 122, 30))) = ' ', '.', RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 122, 30)))) Customer_Surname
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1))) = ' ', 'O', RTRIM(LTRIM(SUBSTRING(SourceRecord, 152, 1)))) Gender
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 153, 8))))) Date_of_Birth
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20))) = ' ', NULL, RTRIM(LTRIM(SUBSTRING(SourceRecord, 161, 20)))) ID_Passport_Number
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30))) = ' ', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 181, 30)))) Postal_Address_1
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30))) = ' ', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 211, 30)))) Postal_Address_2
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30))) = ' ', ' ', RTRIM(LTRIM(SUBSTRING(SourceRecord, 241, 30)))) Postal_Address_3
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10))) = ' ', '0000', RTRIM(LTRIM(SUBSTRING(SourceRecord, 271, 10)))) Postal_Code
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 281, 25))) Tel_No
	,RTRIM(LTRIM(SUBSTRING(replace(SourceRecord, char(131),' '), 306, 50))) Employer_Name
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 356, 25))) Employer_Contact_Tel_No
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 381, 5))) Marital_Status
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16))) Loan_Account_No
	,CASE WHEN Country = 'KE' THEN
		ISNULL(LMSOLID.TargetDBValue,10177)
	 ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
	 END SOL_ID
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))) = ' ', 0, CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 410, 20))),' ',''))) Loan_Amount
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 430, 3))) Loan_Term
	,ISNULL(LMPlanType.TargetDBValue,RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2)))) Plan_Type
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8))) = ' ',ISNULL(CONVERT(DATE,RTRIM(LTRIM(SUBSTRING(SourceRecord,435,8)))),@Date),@DATE) AS Loan_Value_Date
	,IIF(ISNUMERIC(REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 443, 20))),' ','')) = 1,CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 443, 20))),' ','')),CONVERT(NUMERIC(18, 2),0)) AS Premium
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8))) = ' ', ' ', CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 463, 8))))) Repayment_Date
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 471, 30))) Second_Life_Insured_Title
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 501, 30))) Second_Life_Insured_First_Name
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 531, 30))) Second_Life_Insured_Surname
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 561, 20))) Second_Life_Insured_ID_Passport_No
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 581, 8))) = ' ', NULL, CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 581, 8))))) Second_Life_DOB
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 589, 1))) = ' ', NULL, CASE WHEN SUBSTRING(SourceRecord, 589, 1) =  'M' THEN 1 WHEN SUBSTRING(SourceRecord, 589, 1) = 'A' THEN '2' WHEN SUBSTRING(SourceRecord, 589, 1) = 'T' THEN '6' ELSE  0 END)  LTI_Flag
	--,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))) = ' ', EOMONTH(GETDATE()), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))))) LTI_Expiry
	,CASE WHEN ISDATE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))) = 0 THEN
		'20791231'
	ELSE
		IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8))) = ' ', EOMONTH(GETDATE()), CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 590, 8)))))
	END AS LTI_Expiry
	--,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))) = ' ', NULL, ABS(CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))),' ','')))) Outstanding_Balance
	,CASE WHEN isnumeric(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20)))) = 0 THEN
		0
	ELSE
		IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))) = ' ', NULL, ABS(CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 598, 20))),' ',''))))
	END AS Outstanding_Balance
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) Scheme_Code
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 620, 3))) = ' ', NULL, ISNULL(LMPCurrencyCode.TargetDBValue,NULL)) Currency_Code
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 623, 50))) Insurer_Own_insurance
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 673, 15))) Policy_ID_Own_insurance
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 20))) = ' ', 0, CONVERT(NUMERIC(18, 2),REPLACE(RTRIM(LTRIM(SUBSTRING(SourceRecord, 688, 20))),' ',''))) Amount_Own_insurance
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 708, 8))) = ' ', ' ', CONVERT(DATE, RTRIM(LTRIM(SUBSTRING(SourceRecord, 708, 8))))) Expiry_Date_Own_Insurance
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 716, 5))) Product_Scheme_Code
	,CASE WHEN Country = 'KE' THEN
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))) 
		WHEN '001' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('301','302''303','304','310','330','331','332','333','392','393','422')
				THEN '004'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('321','394','400','401','402','403','404','405','406','407','408','409','410','411','412','413','414','415','416','417','421','423','424','425','426','501','502','503')
				THEN '005'
				ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))
			END
		WHEN '002' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('000','100','101','102','125','499')
				THEN '001'
			WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('300','301','302')
				THEN '004'  
			WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('103','104','400','401','402','403','404','405','406','407','408','409','410')
				THEN '005'
			ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))
			END 
		WHEN '009' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))	
				WHEN '999' THEN '005'
			ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))
			END
		END
	ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3)))
	END segment,           
	ISNULL(
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))) 
			WHEN '001' THEN
			CASE 
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('100','122','147')
				THEN '106'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('125','131','140','146','149','151','221','222','223','224','225','226')
				THEN '105'  
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('145','159','190','230') 
				THEN '102'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('148','200','201','202','203','227') 
				THEN '103'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('301','302','330','331','393') 
				THEN '203'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('303','332','333','392','422') 
				THEN '201'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('304') 
				THEN '202'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('310') 
				THEN '204'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('400','401','402','403','410','411','412') 
				THEN '301'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('421','423','424','425','426','407','408','409','413','414','415') 
				THEN '302'	 
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('394','404','405','406','416','417') 
				THEN '303'	
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('321') 
				THEN '309'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('503') 
				THEN '67'										 
			END
		WHEN '002' THEN
			CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('000','100','101','102','125','499') 
				THEN '106'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('300','301') 
				THEN '201'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('302') 
				THEN '203'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('402','403') 
				THEN '303'
				WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('404') 
				THEN '301'
			END
		WHEN '009' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))	
			WHEN '999' THEN 'UNK'
			END
	ELSE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))
	END, RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))) Sub_segment
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 727, 5))) Serial_Number
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 732, 20))) Underwriter_Name
	,IIF(RTRIM(LTRIM(SUBSTRING(SourceRecord, 752, 10))) = ' ', '0', RTRIM(LTRIM(SUBSTRING(SourceRecord, 752, 10)))) PIN_number
	,- 1 iImportRecNo
	,convert(datetime,substring(FileName,19,6)) AS ProcessDate
	,t.ID AS SourceFileID
	,CONCAT (
	RTRIM(LTRIM(SUBSTRING(SourceRecord, 386, 16)))
	,RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2)))
	) NewLoanAccNo
	,Country
	,CASE WHEN Country = 'KE' THEN
		CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 721, 3))) 
		WHEN '001' THEN
		CASE WHEN RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3))) IN ('101','102','103','104','105','106','107','108','110','111','112','120','121','123','124','130','141','132','142','143','144','150','152','153','154','156','155','157','161','191','210','220','300','390','391','151') 
		THEN '2'
		ELSE	
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))
				WHEN  '100' THEN '1'
				WHEN  '148' THEN '10'
				WHEN  '149' THEN '11'
				WHEN  '151' THEN '12'
				WHEN  '159' THEN '13'
				WHEN  '190' THEN '14'
				WHEN  '199' THEN '15'
				WHEN  '200' THEN '16'
				WHEN  '201' THEN '17'
				WHEN  '202' THEN '18'
				WHEN  '203' THEN '19'
				WHEN  '221' THEN '20'
				WHEN  '222' THEN '21'
				WHEN  '223' THEN '22'
				WHEN  '224' THEN '23'
				WHEN  '225' THEN '24'
				WHEN  '226' THEN '25'
				WHEN  '227' THEN '26'
				WHEN  '230' THEN '27'
				WHEN  '301' THEN '28'
				WHEN  '302' THEN '29'
				WHEN  '122' THEN '3'
				WHEN  '303' THEN '30'
				WHEN  '304' THEN '31'
				WHEN  '310' THEN '32'
				WHEN  '321' THEN '33'
				WHEN  '330' THEN '34'
				WHEN  '331' THEN '35'
				WHEN  '332' THEN '36'
				WHEN  '333' THEN '37'
				WHEN  '392' THEN '38'
				WHEN  '393' THEN '39'
				WHEN  '125' THEN '4'
				WHEN  '394' THEN '40'
				WHEN  '400' THEN '41'
				WHEN  '401' THEN '42'
				WHEN  '402' THEN '43'
				WHEN  '403' THEN '43'
				WHEN  '404' THEN '44'
				WHEN  '405' THEN '45'
				WHEN  '406' THEN '46'
				WHEN  '407' THEN '47'
				WHEN  '408' THEN '48'
				WHEN  '409' THEN '49'
				WHEN  '131' THEN '5'
				WHEN  '410' THEN '50'
				WHEN  '411' THEN '51'
				WHEN  '412' THEN '52'
				WHEN  '413' THEN '53'
				WHEN  '414' THEN '54'
				WHEN  '415' THEN '55'
				WHEN  '416' THEN '56'
				WHEN  '417' THEN '57'
				WHEN  '421' THEN '58'
				WHEN  '422' THEN '59'
				WHEN  '140' THEN '6'
				WHEN  '423' THEN '60'
				WHEN  '424' THEN '61'
				WHEN  '425' THEN '62'
				WHEN  '426' THEN '63'
				WHEN  '500' THEN '64'
				WHEN  '501' THEN '65'
				WHEN  '502' THEN '66'
				WHEN  '503' THEN '67'
				WHEN  '145' THEN '7'
				WHEN  '146' THEN '8'
				WHEN  '147' THEN '9'
			END
		END
		WHEN '002' THEN
			CASE RTRIM(LTRIM(SUBSTRING(SourceRecord, 724, 3)))
			WHEN '000' THEN '68'
			WHEN '100' THEN '69'
			WHEN '102' THEN '71'
			WHEN '103' THEN '72'
			WHEN '104' THEN '73'
			WHEN '125' THEN  '4'
			WHEN '300' THEN '74'
			WHEN '301' THEN '30'
			WHEN '302' THEN '75'
			WHEN '401' THEN '42'
			WHEN '402' THEN '76'
			WHEN '403' THEN '47'
			WHEN '404' THEN '77'
			WHEN '405' THEN '53'
			WHEN '406' THEN '54'
			WHEN '407' THEN '55'
			WHEN '408' THEN '56'
			WHEN '409' THEN '78'
			WHEN '410' THEN '61'
			WHEN '499' THEN '80'
			WHEN '101' THEN '70'
			END
		END
	ELSE ''
	END CountryClassification
	,Concat(Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 12, 50)))) Country_Cif
	,COU.Id
	,LMPCoInsurer.TargetDBValue
	,LMPProductID.TargetDBValue
	,T.FileName
FROM 
	--[SBAB_src].[StagingSourceFiles] t
	[ski_int].[SSIS_Src_RawData] t
	LEFT JOIN Policy p (NOLOCK) ON LTRIM(RTRIM(SUBSTRING(t.SourceRecord, 386, 16))) = p.sOldPolicyNo
	LEFT JOIN CFG.Country COU on COU.Alpha2Code = t.Country
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMSOLID 
		ON LMSOLID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 402, 8)))
		AND LMSOLID.Type = 'SOLID_KE_LTI'
		AND LMSOLID.SourceGroup = 'SOL_ID'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPlanType 
		ON LMPlanType.SourceDBValue = CONCAT(RTRIM(LTRIM(SUBSTRING(SourceRecord, 716, 5))),'_',t.Country,'_',RTRIM(LTRIM(SUBSTRING(SourceRecord, 433, 2))))
		AND LMPlanType.Type = 'Plan_Type_LTI'
		AND LMPlanType.SourceGroup = 'Plan_Type'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCurrencyCode
		ON LMPCurrencyCode.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 620, 3)))
		AND LMPCurrencyCode.Type = 'Currency_Code'
		AND LMPCurrencyCode.SourceGroup = 'Currency_Code'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPCoInsurer  
		ON LMPCoInsurer.SourceDBValue = t.Country
		AND LMPCoInsurer.Type = 'CoInsurer_LTI'
		AND LMPCoInsurer.SourceGroup = 'CoInsurer'
	LEFT JOIN [ski_int].[SSISIntegrationLookupMap] LMPProductID 
		ON LMPProductID.SourceDBValue = RTRIM(LTRIM(SUBSTRING(SourceRecord, 618, 2))) +'_' + t.Country
		AND LMPProductID.Type = 'Product_Code'
		AND LMPProductID.SourceGroup = 'Scheme_Code'
WHERE 
	SUBSTRING(SourceRecord, 1, 1) = '2'
	AND ProcessStatus = 0
	AND CONVERT(DATE, DateImported) = @Date;

	UPDATE 
		[SBAB_src].[DailyLTINewBusiness] 
	SET 
		sCustomer_First_Name = '.' 
	WHERE 
		sSegment = '001' and sCustomer_First_Name = ''
---------------------------------------------------------------------------------------------------------------------------
--Update the source record to processed to eliminate processing duplicates
---------------------------------------------------------------------------------------------------------------------------
--Validated Records
------------------------------------------------------------------
UPDATE 
	SSF
SET 
	ProcessStatus = 1
FROM 
	[ski_int].[SSIS_Src_RawData] SSF
	--SBAB_src.StagingSourceFiles SSF
WHERE 
	ProcessStatus = 0
	AND SSF.FileName LIKE '%lti.newbus%'
	AND SUBSTRING(SourceRecord, 1, 1) = '2'
	AND CONVERT(DATE, SSF.DateImported) = @Date;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This gets only the trailer row from the file with a count of records in the file to insert into the Audit table
--select * SBAB_Src.StagingFileProcessAudit
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

--DECLARE @Date DATE = (SELECT CONVERT(DATE, DATEADD(DD, - 1, GETDATE())))
INSERT INTO SBAB_Src.StagingFileProcessAudit
SELECT SUBSTRING(SourceRecord, 1, 1) Record_ID
                ,SUBSTRING(SourceRecord, 2, 8) Count
                ,filename FileName
                ,DateImported ProcessDate
                ,Country
                ,NULL cTotalPremiumReceived
FROM [ski_int].[SSIS_Src_RawData]
--SBAB_src.StagingSourceFiles
WHERE SUBSTRING(SourceRecord, 1, 1) = 9
                AND FileName LIKE '%lti.newbus%'
                AND CONVERT(DATE, DateImported) = @Date
                
EXCEPT
                
SELECT Record_ID
                ,Count
                ,sFileName
                ,dtDAte
                ,sCountry
                ,cTotalPremiumReceived
FROM SBAB_Src.StagingFileProcessAudit
WHERE sFileName LIKE '%lti.newbus%'
                AND CONVERT(DATE, dtDate) = @Date
                
UNION
                
-------------------------------------------------------------------------------------------------------------------------------------------------
--alter a record for audit table with count of records for the specific file,country and date
-------------------------------------------------------------------------------------------------------------------------------------------------
SELECT 0 AS Record_ID
                ,COUNT(*) Count
                ,SSF.FileName FileName
                ,DateImported ProcessDate
                ,SSF.Country Country
                ,NULL cTotalPremiumReceived
FROM [ski_int].[SSIS_Src_RawData] SSF
--SBAB_src.StagingSourceFiles SSF
JOIN [SBAB_src].[DailyLTINewBusiness] TMP ON TMP.iSourceFileID = SSF.ID
WHERE 
	SSF.FileName LIKE '%lti.newbus%'
    AND CONVERT(DATE, DateImported) = @Date
GROUP BY 
	SSF.FileName
    ,DateImported
    ,SSF.Country
                
EXCEPT
                
SELECT Record_ID
                ,Count
                ,sFileName
                ,dtDate
                ,sCountry
                ,cTotalPremiumReceived
FROM SBAB_Src.StagingFileProcessAudit
WHERE 
	sFileName LIKE '%lti.newbus%'
    AND CONVERT(DATE, dtDate) = @Date

TRUNCATE TABLE [ski_int].[SSIS_Src_RawData];
--TRUNCATE TABLE SBAB_src.StagingSourceFiles;

SET NOCOUNT ON;

END


/*
select * from SBAB_src.StagingSourceFiles order by 1;
select * from [ski_int].[SSIS_Src_RawData] order by 1;
select * from [SBAB_src].[DailyLTINewBusiness]
*/