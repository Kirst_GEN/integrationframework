Declare 
	  @execution_id bigint
	, @Reference_id bigint
	, @Country VARCHAR(10) = 'NA'
	, @FileName VARCHAR(255) = 'fcb.NA.lti.newbus'
	, @ProcessToExecute VARCHAR(100) = 'Import Daily LTI New - NA'
	, @ProcessType VARCHAR(100) = 'Import'

SET @reference_id =
	(
	SELECT e.reference_id
	FROM SSISDB.catalog.environment_references e
	INNER JOIN SSISDB.catalog.projects p
	ON e.project_id = p.project_id
	WHERE p.name = @ProjectName
	)

EXEC [SSISDB].[catalog].[create_execution] 
	@package_name=N'ImportMultiple.dtsx',
    @execution_id=@execution_id OUTPUT,
    @project_name=N'Genasys Integration Framework',	
	@folder_name=N'Genasys_Integration_Framework',
    @use32bitruntime=False,
    @reference_id=null

SELECT @execution_id

EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,
	  @object_type=20,
      @parameter_name=N'Country',
      @parameter_value=@Country
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,
	  @object_type=20,
	  @parameter_name=N'FileName',
      @parameter_value=@FileName
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,
	  @object_type=20,
	  @parameter_name=N'ProcessToExecute',
      @parameter_value=@ProcessToExecute
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,
	  @object_type=20,
	  @parameter_name=N'ProcessType',
      @parameter_value=@ProcessType

EXEC [SSISDB].[catalog].[start_execution] @execution_id
GO