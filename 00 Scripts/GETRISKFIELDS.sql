DECLARE @RiskTypeId AS INT
DECLARE @ProductId AS INT
 
SET @ProductId = 251
SET @RiskTypeId = 153
 
SELECT
    PR.sProductName
    ,LRT.sDescription
    ,FC.sFieldCode
	,'''' + FC.sFieldCode + '''' + ',' AS 'SFIELDCODEVALUE'
    ,sCustomGroup AS 'GROUP HEADER'
    ,FC.sDescription AS 'FIELD DESCRIPTION'
    ,LFT.sDescription AS 'FIELD TYPE'
    ,FC.slookupgroup AS 'LOOKUP GROUP'
    ,CASE WHEN PRFR.bRequired = 0 THEN 'Optional' WHEN PRFR.bRequired = 1 THEN 'MANDatory' ELSE '' END AS 'REQUIRED'
    ,'RiskFieldRel' as 'sTable'
    ,PRFR.iRiskTypeID
    ,PRFR.iSeq
FROM
    RiskFieldRel PRFR WITH (NOLOCK)
    INNER JOIN FieldCodes FC WITH (NOLOCK) ON FC.sFieldCode = PRFR.sFieldCode
    INNER JOIN ProductRiskTypes PRT WITH (NOLOCK) ON prt.iRiskTypeID = PRFR.iRiskTypeID   
    INNER JOIN Lookup LRT WITH (NOLOCK) ON LRT.iIndex = prt.iRiskTypeID AND LRT.sGroup = 'RiskType'
    INNER JOIN Lookup LFT WITH (NOLOCK) ON LFT.iIndex = fc.iFieldType AND LFT.sGroup = 'FieldTypes'
    INNER JOIN Products pr WITH (NOLOCK) ON PR.iProductID = PRT.iProductID
WHERE
    PRT.iProductID = @ProductId
    AND PRFR.iRiskTypeID = @RiskTypeId
UNION ALL
SELECT
    pr.sProductName
    ,LRT.sDescription
    ,FC.sFieldCode
	,'''' + FC.sFieldCode + '''' + ',' AS 'SFIELDCODEVALUE'
    ,sCustomGroup AS 'GROUP HEADER'
    ,FC.sDescription AS 'FIELD DESCRIPTION'
    ,LFT.sDescription AS 'FIELD TYPE'
    ,PRFR.slookupgroup AS 'LOOKUP GROUP'
    ,CASE WHEN PRFR.bRequired = 0 THEN 'Optional' WHEN PRFR.bRequired = 1 THEN 'MANDatory' ELSE '' END AS 'REQUIRED'
    ,'ProductRiskFieldRel' as 'sTable'
    ,PRFR.iRiskTypeID
    ,PRFR.iSeq
FROM
    ProductRiskFieldRel PRFR WITH (NOLOCK)
    INNER JOIN FieldCodes FC WITH (NOLOCK) ON FC.sFieldCode = PRFR.sFieldCode
    INNER JOIN ProductRiskTypes PRT WITH (NOLOCK) ON PRT.iRiskTypeID = PRFR.iRiskTypeID
    INNER JOIN Lookup LRT WITH (NOLOCK) ON LRT.iIndex = PRT.iRiskTypeID AND LRT.sGroup = 'RiskType'
    INNER JOIN Lookup LFT WITH (NOLOCK) ON LFT.iIndex = FC.iFieldType AND LFT.sGroup = 'FieldTypes'
    INNER JOIN Products PR WITH (NOLOCK) ON PR.iProductID = PRT.iProductID
WHERE
    PRFR.iProductID = @ProductId
    AND PRFR.iRiskTypeID = @RiskTypeId
ORDER BY 3
    