ALTER PROCEDURE [SKI_INT].[Export_Sample] (@TableName SYSNAME)
AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name
		FROM sys.tables t
		JOIN sys.columns c ON t.object_id = c.object_id
			AND t.NAME = 'Policy'
		) a

	SET @sDataRecord = (
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)
	SET @SQLInsertHeaderCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''Policy'' ,0'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE 
				WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
				ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
		        JOIN sys.columns c ON t.object_id = c.object_id
			        AND t.NAME = 'Policy'
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = 'INSERT INTO ' + N'' + @TableName + ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''Policy'' ,0 FROM dbo.Policy'
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ' INSERT INTO ' + N'' + @TableName + ' SELECT COUNT(*) ,GetDate() ,''Policy'' ,0 FROM dbo.Policy'

	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
END


GO