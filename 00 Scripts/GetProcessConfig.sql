--====================================================================================================
--GET CURRENT PROCESS SETUP - NAMIBIA
--====================================================================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily LTI New - NA','fcb.NA.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - NA','fcb.NA.sti.newbus'--STI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - NA','fcb.NA.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - NA','fcb.NA.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Miscellaneous','Import Daily CrossSell - NA','fcb.NA.fnrl.crosssell'--LTI CROSS SELL

--====================================================================================================
--GET CURRENT PROCESS SETUP - UGANDA
--====================================================================================================
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily LTI New - UG','fcb.UG.lti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'Import','Import Daily STI New - UG','fcb.UG.sti.newbus'--LTI NEW BUS
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily LTI Prem - UG','fcb.UG.lti.premiums'--LTI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'CashImport','Import Daily STI Prem - UG','fcb.UG.sti.premiums'--STI CASH IMPORT
EXEC [ski_int].[SSIS_GetProcessConfig] 'Miscellaneous','Import Daily CrossSell - UG','fcb.UG.fnrl.crosssell'--LTI CROSS SELL
