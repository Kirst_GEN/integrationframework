
--drop table [ski_int].[SSIS_Daily_NewBusiness]
/****** Object:  Table [ski_int].[SSIS_Daily_NewBusiness]    Script Date: 21/12/2017 11:55:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ski_int].[SSIS_Daily_NewBusiness](
	[iID] [int] IDENTITY(1,1) NOT NULL,
	[iImportRecNo] [bigint] NULL,
	[dtProcessDate] [datetime] NULL,
	[ReferenceField] [varchar](50) NULL,
	
	[CH_LastName] [varchar](30) NULL,
	[CH_FirstName] [varchar](30) NULL,
	[CH_Initials] [varchar](8) NULL,
	[CH_CountryID] [int] NOT NULL,

	[CD_TITLE] [varchar](15) NULL,
	[CD_IDTYPE] [int] NOT NULL,
	[CD_IDNO] [varchar](max) NULL,
	[CD_DOB] [date] NULL,
	[CD_GENDER] [int] NOT NULL,
	[CD_AGE] [int] NULL,
	[CD_EMAILDOCS] [int] NOT NULL,
	[CD_DONOTPOST] [int] NOT NULL,
	[CD_CELLNO] [varchar](max) NULL,
	[CD_EMAIL] [varchar](max) NULL,
	
	[AR_AddressType] [varchar](30) NULL,
	[AR_AddressLine1] [varchar](50) NULL,
	[AR_AddressLine2] [varchar](50) NULL,
	[AR_AddressLine3] [varchar](50) NULL,
	[AR_PostalCode] [varchar](13) NULL,
	
	[AP_AddressType] [varchar](30) NULL,
	[AP_AddressLine1] [varchar](50) NULL,
	[AP_AddressLine2] [varchar](50) NULL,
	[AP_AddressLine3] [varchar](50) NULL,
	[AP_PostalCode] [varchar](13) NULL,
	
	[PH_Bank] [varchar](50) NULL,
	[PH_AccHolderName] [varchar](50) NULL,
	[PH_AccountNumber] [varchar](16) NULL,
	[PH_AccountType] [varchar](15) NULL,
	[PH_BranchCode] [varchar](10) NULL,
	[PH_PaymentMethod] [int] NOT NULL,
	[PH_sOldPolicyNo] [varchar](50) NULL,
	[PH_PolicyStatus] [int] NULL,
	[PH_CoverTerm] [int] NULL,
	[PH_PaymentTerm] [int] NULL,
	[PH_PaymentStatus] [int] NULL,
	[PH_CoverStartDate] [datetime] NULL,
	[PH_PolicyType] [varchar](30) NULL,
	[PH_iCollection] [int] NULL,
	[PH_Currency] [int] NULL,
	[PH_sFinAccount] [varchar](15) NULL,
	[PH_CountryID] [int] NULL,
	
	[PD_P_POLICYSOURCE] [varchar](30) NULL,
	
	[RD_R_RISKCPC] [varchar](100) NULL,
	[RD_R_INSVALUE] [float] NULL
) ON [PRIMARY]
GO


