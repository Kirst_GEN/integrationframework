
/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.3156)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
	select * from ski_int.SSIS_Daily_NewBusiness
	select * from riskdetails
*/

ALTER PROCEDURE [SKI_INT].[SSIS_Normalize_Import]
AS
BEGIN

	--========================================================================================================================
	--CLEAR PROCESSING TABLE DATA
	--========================================================================================================================
	TRUNCATE TABLE ski_int.SSIS_Daily_NewBusiness
	
	--========================================================================================================================
	--POPULATE PROCESSING TABLE
	--========================================================================================================================
	INSERT INTO ski_int.SSIS_Daily_NewBusiness
	--select * from ski_int.SSIS_Daily_NewBusiness
	SELECT 
		-1 AS iImportRecNo
		, GetDate() AS DateProcessed
		, 'TEST1'
		, 'Tester' AS CH_LastName
		, ISNULL('FirstName','') AS CH_FirstName
		, ISNULL('FN','') AS CH_Initials
		, 206 AS CH_CountryID
		, ISNULL('MR',0) AS C_TITLE
		, 1 AS C_IDTYPE
		, ISNULL('8102105062086','') AS C_IDNO
		, ISNULL('1981/10/02', '1900/01/01') AS C_DOB
		, '1' AS C_GENDER
		, ISNULL('35','') AS C_AGE
		, 0 AS C_EMAILDOCS
		, 0 AS C_DONOTPOST
		, '0828253057' AS C_CELLNO
		, 'Kirsten@genasys.co.za' AS C_EMAIL
		
		, 'Physical' AS AR_AddressType
		, '1 Test Street' AS A_AddressLine1
		, 'AR_AddressLine2' AS AR_AddressLine2
		, 'AR_AddressLine3' AS AR_AddressLine3
		, '0000' AS AR_PostalCode
		
		, 'Postal' AS AP_AddressType
		, '1 Test Street' AS AP_AddressLine1
		, 'AP_AddressLine2' AS AP_AddressLine2
		, 'AP_AddressLine3' AS AP_AddressLine3
		, '0000' AS AP_PostalCode
		
		, '' AS PH_Bank
		, '' AS PH_AccHolderName
		, '' AS PH_AccountNumber
		, '' AS PH_AccountType
		, '' AS PH_BranchCode
		, 2  AS PH_PaymentMethod
		, '' as PH_sOldPolicyNo
		
		, 1 AS PH_PolicyStatus
		, 1 AS PH_CoverTerm
		, 1 AS PH_PaymentTerm
		, 1 AS PH_PaymentStatus
		, '15 June 2018' AS PH_CoverStartDate
		, 'PL' AS PH_PolicyType
		, 1 AS PH_iCollection
		, 0 AS PH_Currency
		, 0 AS PH_sFinAccount
		, 206 AS PH_CountryID
		--POLICYDETAILS------------------------------------------------------------------------------------------------------
		, 0 AS PD_P_POLICYSOURCE
		, '0.00 @ 1200.0000000000 @ 100.00000 [     1200.00]' AS RD_R_RiskCPC
		, 10000 As RD_R_INSVALUE
		
	--========================================================================================================================
	--WRITE RECORDS TO LOG TABLE
	--========================================================================================================================
	INSERT INTO ski_int.SSIS_Daily_NewBusiness_Log
	SELECT * FROM ski_int.SSIS_Daily_NewBusiness

SET NOCOUNT ON;

END


