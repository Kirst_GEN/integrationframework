

CREATE PROCEDURE [SKI_INT].[SSIS_AddFTPConfig]
	
    @UserName VARCHAR(255),
	@Password VARCHAR(255),
    @Port int,
	@Host VARCHAR(255),
	@Protocol VARCHAR(20),
    @LogonType VARCHAR(20),
	@FolderName VARCHAR(255),
	@WinSCP_Directory VARCHAR(255),
	@SFTP_PrivateKeyLocation VARCHAR(255),
    @SFTP_PassPhrase VARCHAR(1000),
	@SFTP_FingerPrint VARCHAR(1000)  
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISSFTPUser];

	INSERT INTO [ski_int].[SSISSFTPUser]
	(
		 UserName
		,Password
		,SFTP_Host
		,SFTP_Port
		,SFTP_Protocol
		,SFTP_LogonType
		,SFTP_FolderName
		,WinSCP_Directory
		,SFTP_PrivateKeyLocation
		,SFTP_PassPhrase
		,SFTP_FingerPrint
	)
	VALUES
	(
		 @UserName
		,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@Password))
		,@Host
		,@Port
		,@Protocol
		,@LogonType
		,@FolderName
		,@WinSCP_Directory
		,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@SFTP_PrivateKeyLocation))
		,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@SFTP_PassPhrase))
		,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@SFTP_FingerPrint))
	);
		

END

GO





CREATE PROCEDURE [SKI_INT].[SSIS_AddEmailConfig]
	
    @MailServer VARCHAR(8000),
    @MailPort INT = 0,
    @MailUserName VARCHAR(100) = NULL,
    @MailPassword VARCHAR(8000) 
	
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISEmailConfig];

	INSERT INTO [ski_int].[SSISEmailConfig]
	(
		MailServer,
		MailPort,
		MailUserName,
		MailPassword
	)
	VALUES
	(
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailServer)),
		@MailPort,
		@MailUserName,
		CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@MailPassword))
	);
		

END

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_AddSSISUser]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_AddSSISUser]
    @pUserName VARCHAR(100) = NULL,
    @pPassword VARCHAR(8000) 
AS
BEGIN
    SET NOCOUNT ON

	TRUNCATE TABLE [ski_int].[SSISBatchUser];

	INSERT INTO [ski_int].[SSISBatchUser](UserName,Password)
	  VALUES(@pUserName,CONVERT(VARBINARY(8000),ENCRYPTBYPASSPHRASE('**********',@pPassword)));
		

END

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_CanContinue]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONTINUE NEXT STEP INDICATOR FOR THE PROCESS
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_CanContinue]
(
		@ProcessName VARCHAR(100)
		,@PackageName VARCHAR(100)
)
AS

BEGIN

	--==============================================================================================================
	--GET PACKAGE ID
	--==============================================================================================================
	DECLARE @PackageLogID INT;
	
	SELECT TOP 1 
		@PackageLogID = PackageLogID
	FROM
		[ski_int].[SSISPackageLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
	ORDER BY
		EndTime DESC;
		
	--==============================================================================================================
	--GET CONTINUE NEXT STEP INDICATOR
	--==============================================================================================================
	SELECT TOP 1 
		ContinueNextStep
	FROM
		[ski_int].[SSISProgressLog] (NOLOCK) 
	WHERE 
		ProcessName = @ProcessName
		AND ProcessName = @ProcessName
		AND ProgressLogID = @PackageLogID
	ORDER BY
		EndTime DESC;

END 





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_DataErrorLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_DataErrorLog]
	  @PackageLogID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorSource NVARCHAR(255)
	, @SourceRecord NVARCHAR(255)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISDataErrorLog(
		PackageLogID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorSource
		,SourceRecord
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorSource
	, @SourceRecord
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EmailsToBeSent_Insert]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [SKI_INT].[SSIS_EmailsToBeSent_Insert]
		@PackageLogID INT
		,@ProcessName VARCHAR(255)
		,@JobReference VARCHAR(100)
		,@From VARCHAR(200)
		,@TO VARCHAR(400)
		,@CC VARCHAR(200)
		,@BCC VARCHAR(200)
		,@Subject VARCHAR(200)
		,@Body VARCHAR(8000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO [ski_int].[SSISMailsToBeSent](
		PackageLogID
		,ProcessName
		,JobReference
		,MailFrom
		,MailTO
		,MailCC
		,MailBCC
		,MailSubject
		,MailBody
		,DateToSend
		,Sent
		,DateSent
	)
	VALUES (
		@PackageLogID
		,@ProcessName
		,@JobReference
		,@From
		,@TO
		,@CC
		,@BCC
		,@Subject
		,@Body
		,GetDate()
		,0
		,NULL
	)
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndExtractLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndExtractLog]
  @ExtractLogID INT
, @ExtractCount INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastExtractDateTime DATETIME, @SQL NVARCHAR(255)
	SELECT @SQL = N'SELECT @LastExtractDateTime = ISNULL(MAX(ModifiedDate), ''1900-01-01'') FROM ski_int.imp_' + TableName FROM ski_int.SSISExtractLog WHERE ExtractLogID = @ExtractLogID
	EXEC sp_executeSQL @SQL, N'@LastExtractDateTime DATETIME OUTPUT', @LastExtractDateTime OUTPUT
	
	UPDATE ski_int.SSISExtractLog
	SET
	  EndTime = GetDate()
	, ExtractCount = @ExtractCount
	, LastExtractDateTime = @LastExtractDateTime
	, Success = 1
	WHERE ExtractLogID = @ExtractLogID

	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndJob]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT UPDATES THE [ski_int].[SSIS_EndJob] TALE ON COMPLETION OF THE JOB
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [SKI_INT].[SSIS_EndJob]
	@SSISJobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN

	--==============================================================================================================
	--UPDATE JOB
	--==============================================================================================================
	UPDATE 
		[ski_int].[SSISJob] 
	SET
		EndTime = GetDate()
		,JobReference = @JobReference 
		,Success = @Success
	WHERE 
		SSISJobID = @SSISJobID;
	

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndPackageLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndPackageLog]
	@JobID INT
	,@JobReference NVARCHAR(100)
	,@Success INT

AS

BEGIN


	UPDATE 
		ski_int.SSISPackageLog 
	SET
		EndTime = GetDate()
		,Success = @Success
		,JobReference = @JobReference
	WHERE 
		JobID = @JobID;

	UPDATE 
		ski_int.SSISProgressLog 
	SET
		EndTime = GetDate()
		,Success = @Success
	WHERE 
		JobID = @JobID;
	

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_EndProgressLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_EndProgressLog]
  @PackageLogID INT
  ,@Success INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ski_int.SSISProgressLog SET
	  EndTime = GetDate()
	, Success = @Success
	WHERE ProgressLogID = @PackageLogID

	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ErrorLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_ErrorLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100) 
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ErrorNumber NVARCHAR(100)
	, @ErrorMessage NVARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISErrorLog(
		JobID
		,ProcessName
		,JobReference
		,PackageName
		,TaskName
		,ErrorNumber
		,ErrorMessage
		,ErrorTime
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ErrorNumber
	, @ErrorMessage
	, GetDate()
	)
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ExecutePackages]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--====================================================================================================
--DESCRIPTION :      THIS SCRIPT EXECUTES A SSIS PACKAGE
--DATE               :      2016-01-18    
--AUTHOR             :      KIRSTEN HARRIS / MARIUS PRETORIUS 
--CLIENT             :      ALL
--====================================================================================================

--==================================================
--EXAMPLE OF HOW TO EXECUTE A PACKAGE
--==================================================
/*
       EXEC EXECUTE_SSIS_Packages  @Package_Direction = 'EXPORT',
                                                       @package_name1='FlatFiles.dtsx',
                                                       @folder_name1='TFG Integration',
                                                       @project_name1='SKiXtract',
                                                       @use32bitruntime1=0,
                                                       @reference_id1=null, 
                                                       @sTableNameToInsertDataInto='[ski_int].[TEST_EXPORT_SSISDB_20160511191039]',
                                                       @FileName='TEST_EXPORT_SSISDB.txt',
                                                       @FileLocation='\\\\kirsten-laptop\\Ski\\',
                                                       @sFileArchiveLocation='\\\\kirsten-laptop\\Ski\\Archive\\'

*/
--==================================================

--====================================================================================================
--CREATE PROC
--====================================================================================================
CREATE PROC [SKI_INT].[SSIS_ExecutePackages]
       ( 
              @Package_Direction nvarchar(15),  
              @Xtract_Package_Name nvarchar(100),  
              @Xtract_Folder_Name nvarchar(100),
              @Xtract_Project_Name nvarchar(100),
              @Xtract_Use32bitruntime bit, 
              @Xtract_Reference_id nvarchar(20),
              @Xtract_STableNameToInsertDataInto nvarchar (100),
              @Xtract_FileName nvarchar(100),
              @Xtract_FileLocation nvarchar(250),
              @Xtract_SFileArchiveLocation nvarchar(250)
       )

AS

       BEGIN

              --==================================================
              --DECLARE EXECUTION ID WHICH WILL BE YOUR PACKAGE ID AT RUNTIME
              --==================================================
              DECLARE @Exec_id BIGINT = 0

              --==================================================
              --CREATE THE PACKAGE EXECUTION
              --==================================================
              EXEC [SSISDB].[catalog].[create_execution] 
                           @package_name        =      @Xtract_Package_Name,
                           @folder_name         =      @Xtract_Folder_Name,
                           @project_name        =      @Xtract_Project_Name,
                           @use32bitruntime     =      FALSE, 
                           @reference_id        =      NULL,            
                           @execution_id        =      @Exec_id OUTPUT  

              --==================================================
              --EXPORT PACKAGE EXECUTION
              --==================================================
              IF @Package_Direction = 'EXPORT' 

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileName', @parameter_value = @Xtract_FileName
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'FileLocation', @parameter_value = @Xtract_FileLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sFileArchiveLocation', @parameter_value = @Xtract_sFileArchiveLocation
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sTableNameToCreate', @parameter_value = @Xtract_sTableNameToInsertDataInto

                     END 

              IF @Package_Direction = 'IMPORT' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 20, @parameter_name = N'ChildPackageToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

              IF @Package_Direction = 'FLASH' 
              --==================================================
              --IMPORT PACKAGE EXECUTION
              --==================================================

                     BEGIN 

                           --==================================================
                           --SET THE PACKAGE EXECUTION PARAMETERS
                           --==================================================
                           EXEC [SSISDB].[catalog].[set_execution_parameter_value] @Exec_id,  @object_type = 30, @parameter_name = N'sProcessToExecute', @parameter_value = @Xtract_Package_Name
       
                     END 

       END

       --==================================================
       --START THE PACKAGE EXECUTION
       --==================================================
       EXEC [SSISDB].[catalog].[start_execution] @Exec_id;





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetFlashProcessConfig]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetFlashProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		--INNER JOIN [ski_int].[SSISCommunicationType] CT (NOLOCK) ON
		--	C.CommunicationTypeId = CT.Id
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISSript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISSript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISSript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISSript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISSript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISSript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISSript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetJobSchedulerResults]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_GetJobSchedulerResults]

AS

BEGIN

	--==============================================================================================================
	--SELECT
	--==============================================================================================================
	SELECT 
	[sJOB].[name] AS [JobName]
    , CASE 
        WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
        ELSE CAST(
                CAST([sJOBH].[run_date] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [LastRunDateTime]
    , CASE [sJOBH].[run_status]
        WHEN 0 THEN 'Failed'
        WHEN 1 THEN 'Succeeded'
        WHEN 2 THEN 'Retry'
        WHEN 3 THEN 'Canceled'
        WHEN 4 THEN 'Running' -- In Progress
      END AS [LastRunStatus]
    , STUFF(
            STUFF(RIGHT('000000' + CAST([sJOBH].[run_duration] AS VARCHAR(6)),  6)
                , 3, 0, ':')
            , 6, 0, ':') 
        AS [LastRunDuration (HH:MM:SS)]
    , [sJOBH].[message] AS [LastRunStatusMessage]
    , CASE [sJOBSCH].[NextRunDate]
        WHEN 0 THEN NULL
        ELSE CAST(
                CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [NextRunDateTime]
	FROM 
		[msdb].[dbo].[sysjobs] AS [sJOB]
		LEFT JOIN (
					SELECT
						[job_id]
						, MIN([next_run_date]) AS [NextRunDate]
						, MIN([next_run_time]) AS [NextRunTime]
					FROM [msdb].[dbo].[sysjobschedules]
					GROUP BY [job_id]
				) AS [sJOBSCH]
			ON [sJOB].[job_id] = [sJOBSCH].[job_id]
		LEFT JOIN (
					SELECT 
						[job_id]
						, [run_date]
						, [run_time]
						, [run_status]
						, [run_duration]
						, [message]
						, ROW_NUMBER() OVER (
												PARTITION BY [job_id] 
												ORDER BY [run_date] DESC, [run_time] DESC
						  ) AS RowNumber
					FROM [msdb].[dbo].[sysjobhistory]
					WHERE [step_id] = 0
				) AS [sJOBH]
			ON [sJOB].[job_id] = [sJOBH].[job_id]
			AND [sJOBH].[RowNumber] = 1
	ORDER BY [JobName]

END 








GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcess]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcess]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id
		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 





GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcessConfig]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--drop proc [SKI_INT].[SSIS_GetProcessConfig]
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
		,SQLStagingCommand.Name SQLStagingCommand
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id


		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
		LEFT JOIN [ski_int].[SSISScript] SQLStagingCommand (NOLOCK) ON
			P.ScriptId = SQLStagingCommand.ScriptId
			AND SQLStagingCommand.ScriptTypeID = 8
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
		,ISNULL(SQLStagingCommand,'') SQLStagingCommand
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 

GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_GetProcessToExecute]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE FILE NAME THAT IS REQUESTED FOR EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_GetProcessToExecute] 
(
		@sFileDirection VARCHAR(50)
		,@sPackageNameToExecute VARCHAR(50)
		,@sFileName VARCHAR(255)
)
AS

BEGIN

	--==============================================================================================================
	--GET RELEVANT INFO OF PACKAGE TO EXECUTE
	--==============================================================================================================
	SELECT 
		MF.sPackageName
		,MF.sFileLocation
		,CASE WHEN MF.sPackageName = 'ClaimsMigration' THEN MF.sFileName ELSE MF.sFileName END AS 'sFileName'
		,MF.sFileProcessedLocation  
		,MF.sPackageFolderName
		,MF.sPackageProjectName
		,MS.iFileFormatID
	FROM 
		[ski_int].[IntegrationMasterFiles] MF (NOLOCK)
		INNER JOIN [ski_int].[IntegrationMasterScripts] MS (NOLOCK) ON 
			MF.ID = MS.iIntegrationMasterFileID
	WHERE 
		MF.sFileDirection = @sFileDirection 
		AND MF.sPackageName = @sPackageNameToExecute
		AND MF.sFileName = @sFileName
		AND MF.iStatusID = 1 


END 




GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_ProgressLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSIS_ProgressLog]
	  @JobID INT
	, @ProcessName NVARCHAR(255)
	, @JobReference NVARCHAR(100)
	, @PackageName NVARCHAR(100)
	, @TaskName NVARCHAR(100)
	, @ContinueNextStep INT
	, @Message NVARCHAR(255)
	,@Success INT
AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO ski_int.SSISProgressLog (
	  JobID
	, ProcessName
	, JobReference
	, PackageName
	, TaskName
	, ContinueNextStep
	, Message
	, StartTime
	, Success
	)
	VALUES (
	  @JobID
	, @ProcessName
	, @JobReference
	, @PackageName
	, @TaskName
	, @ContinueNextStep
	, @Message
	, GetDate()
	, @Success
	)
		
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartExtractLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_StartExtractLog]
  @PackageLogID INT
, @ProcessName NVARCHAR(255)
, @TableName NVARCHAR(100)
AS
BEGIN
	DECLARE @LastExtractDateTime	DATETIME
	SET NOCOUNT ON;
	
	SELECT @LastExtractDateTime = ISNULL(MAX(LastExtractDateTime), '1900-01-01')
	FROM ski_int.SSISExtractLog
	WHERE TableName = @TableName
		
	INSERT INTO ski_int.SSISExtractLog (
	  PackageLogID
	, ProcessName
	, TableName
	, StartTime
	)
	VALUES (
	  @PackageLogID
	, @ProcessName
	, @TableName
	, GetDate()
	)

	SELECT 
	  CAST(Scope_Identity() AS INT) ExtractLogID
	, CONVERT(NVARCHAR(23), @LastExtractDateTime, 121) LastExtractDateTimeString
	
	SET NOCOUNT OFF;
END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartJob]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES AN ENTRY [ski_int].[SSISJob] EVERY TIME A NEW SSIS JOB IS EXECUTED
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================

CREATE PROCEDURE [SKI_INT].[SSIS_StartJob]
	@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)

AS

BEGIN
	--==============================================================================================================
	--SAMPLE EXECUTE
	--==============================================================================================================
	--EXEC [ski_int].[SSIS_StartJob] 'Import Daily LTI File - BW','REFERENCE1'

	--==============================================================================================================
	--INSERT JOB
	--==============================================================================================================
	INSERT INTO [ski_int].[SSISJob]
	(
		ProcessName
		,JobReference
		,StartTime
		,EndTime
		,DateCreated
		,Success
	)
	VALUES 
	(
		@ProcessName
		,@JobReference
		,GetDate()
		,NULL
		,GetDate()
		,0
	);

	--==============================================================================================================
	--RETURN LATEST JOB ID
	--==============================================================================================================
	SELECT CAST(Scope_Identity() AS INT) SSISJobID;

END







GO

/****** Object:  StoredProcedure [SKI_INT].[SSIS_StartPackageLog]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SKI_INT].[SSIS_StartPackageLog] 
	@JobID INT
	,@ProcessName NVARCHAR(255)
	,@JobReference NVARCHAR(100)
	,@PackageName NVARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO ski_int.SSISPackageLog 
	(
		JobID,
		ProcessName,
		JobReference,
		PackageName,
		StartTime
	)
	VALUES 
	(
		@JobID,
		@ProcessName,
		@JobReference,
		@PackageName,
		GetDate()
	);

	SELECT MAX(PackageLogID) FROM ski_int.SSISPackageLog;

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSISFileLocations_Insert]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSISFileLocations_Insert]
	 @LocationId VARCHAR(100)
	,@FileName VARCHAR(100)
	,@FileType INT
	,@FileHasHeaderRow INT
	,@FileHasFooterRow INT
	,@PickupLocation VARCHAR(255)
	,@ProcessedLocation VARCHAR(255)
	,@ArchiveLocation VARCHAR(255)
	,@LogFileLocation VARCHAR(255)
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISFileLocations]
		(
			LocationId
			,FileName
			,FileType
			,FileHasHeaderRow
			,FileHasFooterRow
			,PickupLocation
			,ProcessedLocation
			,ArchiveLocation
			,LogFileLocation
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			 @LocationId
			,@FileName 
			,@FileType
			,@FileHasHeaderRow 
			,@FileHasFooterRow 
			,@PickupLocation 
			,@ProcessedLocation 
			,@ArchiveLocation 
			,@LogFileLocation 
			,@UserLastUpdated 
			,GetDate()
		   );

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSISProcess_Insert]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSISProcess_Insert]
	@Name VARCHAR(100)
	,@Description VARCHAR(400)
	,@ProcessTypeId INT
	,@CommunicationId INT
	,@FileLocationId INT
	,@ScriptId INT
	,@SSISDBCatalogFolderName VARCHAR(100)
	,@SSISDBCatalogProjectName VARCHAR(100)
	,@SSISDBCatalogPackageName VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISProcess]
		(
			Name
			,Description
			,ProcessTypeId
			,CommunicationId
			,FileLocationId
			,ScriptId
			,SSISDBCatalogFolderName
			,SSISDBCatalogProjectName
			,SSISDBCatalogPackageName
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@Name 
			,@Description 
			,@ProcessTypeId 
			,@CommunicationId 
			,@FileLocationId 
			,@ScriptId 
			,@SSISDBCatalogFolderName 
			,@SSISDBCatalogProjectName 
			,@SSISDBCatalogPackageName 
			,@Enabled 
			,@UserLastUpdated 
			,GetDate()
		   );

END


GO

/****** Object:  StoredProcedure [SKI_INT].[SSISScript_Insert]    Script Date: 21/05/2018 12:46:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SKI_INT].[SSISScript_Insert]
	@ScriptId INT
	,@ScriptTypeId INT
	,@Name VARCHAR(100)
	,@Enabled INT
	,@UserLastUpdated VARCHAR(50)

AS

BEGIN

	INSERT INTO [ski_int].[SSISScript]
		(
			ScriptId
			,ScriptTypeId
			,Name
			,Enabled
			,UserLastUpdated
			,DateLastUpdated
		)
	VALUES 
           (
			@ScriptId
			,@ScriptTypeId
			,@Name
			,@Enabled
			,@UserLastUpdated
			,GetDate()
		   );

END


GO


	
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES A TABLE TABLE
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_IntegrationCreateTable] 
(
		@TableName SYSNAME
)
AS

BEGIN

	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'CREATE TABLE ' + N'' + @TableName + '([iID] [INT] IDENTITY(1,1) NOT NULL, [sDataRecord] [VARCHAR](MAX) NULL, [dtDate] [DATETIME] NULL, [sFileName] [VARCHAR](250) NULL, [iProcessStatus] [INT] NULL)'

	EXEC(@SQLCommand);

END 
GO

--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT CREATES A TABLE TABLE
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--CLIENT		:	TFG
--==============================================================================================================
CREATE PROCEDURE [SKI_INT].[SSIS_IntegrationDropTable] 
(
		@TableName SYSNAME
)
AS

BEGIN

	--==============================================================================================================
	--CREATE EXPORTED DATA TABLE
	--==============================================================================================================
	DECLARE 
		@SQLCommand varchar(2000)

	SET @SQLCommand = 'DROP TABLE ' + N'' + @TableName

	EXEC(@SQLCommand);

END 