
--===========================================================================================================================================================================================
--START CONFIG
--===========================================================================================================================================================================================
--truncate table [ski_int].[SSISProcess]
--select * from [ski_int].[SSISProcess] 
SET IDENTITY_INSERT [ski_int].[SSISProcess] ON 
INSERT INTO [ski_int].[SSISProcess] 
       ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (1, N'1', N'DELTA_CLAIMS_OUTSTANDING', 2, 1, 1, 1, N'SKI_Automation', N'GenasysExportFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT INTO [ski_int].[SSISProcess] 
       ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (2, N'1', N'DELTA_CLAIMS_PAYMENTS', 2, 1, 1, 2, N'SKI_Automation', N'GenasysExportFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
INSERT INTO [ski_int].[SSISProcess] 
       ([Id], [Name], [Description], [ProcessTypeId], [CommunicationId], [FileLocationId], [ScriptId], [SSISDBCatalogFolderName], [SSISDBCatalogProjectName], [SSISDBCatalogPackageName], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (3, N'1', N'DELTA_PREMIUMS', 2, 1, 1, 3, N'SKI_Automation', N'GenasysExportFramework', N'ExportMultiple', 1, N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISProcess] OFF

--truncate table [ski_int].[SSISFileLocations]
--select * from ski_int.SSISFileType
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] ON 
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation] ,[UserLastUpdated], [DateLastUpdated]) 
VALUES (1, 1, 'CLAIMS_OUTSTANDING', 3, 0, 0, N'\\kirsten-lap\Export Framework\Pickup\', N'\\kirsten-lap\Export Framework\Processed\', N'\\kirsten-lap\Export Framework\Archive\', N'\\kirsten-lap\Export Framework\Logfiles\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation] ,[UserLastUpdated], [DateLastUpdated]) 
VALUES (2, 1, 'CLAIMS_PAYMENTS', 3, 0, 0, N'\\kirsten-lap\Export Framework\Pickup\', N'\\kirsten-lap\Export Framework\Processed\', N'\\kirsten-lap\Export Framework\Archive\', N'\\kirsten-lap\Export Framework\Logfiles\', N'Integration', GETDATE())
INSERT [ski_int].[SSISFileLocations] ([Id], [LocationId], [FileName], [FileType], [FileHasHeaderRow], [FileHasFooterRow], [PickupLocation], [ProcessedLocation], [ArchiveLocation], [LogFileLocation] ,[UserLastUpdated], [DateLastUpdated]) 
VALUES (3, 1, 'PREMIUMS', 3, 0, 0, N'\\kirsten-lap\Export Framework\Pickup\', N'\\kirsten-lap\Export Framework\Processed\', N'\\kirsten-lap\Export Framework\Archive\', N'\\kirsten-lap\Export Framework\Logfiles\', N'Integration', GETDATE())
SET IDENTITY_INSERT [ski_int].[SSISFileLocations] OFF

--truncate table [ski_int].[SSISScript]
/*
select * from [ski_int].[SSISProcess] 
select * from [ski_int].[SSISScript]
*/
SET IDENTITY_INSERT [ski_int].[SSISScript] ON 

INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (1, 1, 8, N'[SKI_INT].[SSIS_Staging_Delta_ClaimOutstanding]', 1, N'Integration', GETDATE())

INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (2, 1, 1, N'[SKI_INT].[SSIS_Export_Data]', 1, N'Integration', GETDATE())

INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (3, 1, 2, N'SKI_INT.DELTA_STAGING_CLAIMS', 1, N'Integration', GETDATE())

INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (4, 2, 8, N'[SKI_INT].[SSIS_Staging_Delta_ClaimPayments]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (5, 2, 1, N'[SKI_INT].[SSIS_Export_Data]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (6, 2, 2, N'SKI_INT.DELTA_STAGING_CLAIMPAYMENTS', 1, N'Integration', GETDATE())

INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (7, 3, 8, N'[SKI_INT].[SSIS_Staging_Delta_Premiums]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (8, 3, 1, N'[SKI_INT].[SSIS_Export_Data]', 1, N'Integration', GETDATE())
INSERT [ski_int].[SSISScript] ([Id], [ScriptId], [ScriptTypeId], [Name], [Enabled], [UserLastUpdated], [DateLastUpdated]) 
VALUES (9, 3, 2, N'SKI_INT.DELTA_STAGING_PREMIUMS', 1, N'Integration', GETDATE())

SET IDENTITY_INSERT [ski_int].[SSISScript] OFF

--===========================================================================================================================================================================================
--END CONFIG
--===========================================================================================================================================================================================

--===========================================================================================================================================================================================
--CHECK CONFIG
--===========================================================================================================================================================================================
EXEC SKI_INT.SSIS_GetProcessConfig 'EXPORT','DELTA_CLAIMS_OUTSTANDING','CLAIMS_OUTSTANDING'
EXEC SKI_INT.SSIS_GetProcessConfig 'EXPORT','DELTA_CLAIMS_PAYMENTS','CLAIMS_PAYMENTS'
EXEC SKI_INT.SSIS_GetProcessConfig 'EXPORT','DELTA_PREMIUMS','PREMIUMS'

