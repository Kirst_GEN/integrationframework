--exec [SKI_INT].[SSIS_Export_Data_Select]  'dbo.Policy','Test',1,0,'DEV'
--select * from sys.columns

ALTER PROCEDURE [SKI_INT].[SSIS_Export_Data_Select] 
	(
	  @SourceTableName SYSNAME
	, @DestinationTableName SYSNAME
	, @HasHeader INT = 1
	, @HasFooter INT = 0
	, @Environment VARCHAR(10)
	)

AS
BEGIN
	--==============================================================================================================
	--INSERT DATA INTO TEMP TABLE
	--==============================================================================================================
	DECLARE @SQLInsertDataCommand VARCHAR(MAX)
		,@SQLInsertHeaderCommand VARCHAR(MAX)
		,@SQLInsertFooterCommand VARCHAR(2000)
		,@sDataRecord VARCHAR(MAX)

	SET @sDataRecord = ''
	SET @SQLInsertHeaderCommand = ''
	SET @SQLInsertDataCommand = ''
	SET @SQLInsertFooterCommand = ''

	SELECT @sDataRecord = @sDataRecord + Name + '|'
	FROM (
		SELECT c.name, column_id
		FROM sys.tables t
		JOIN sys.schemas sc
			ON sc.schema_id = t.schema_id
		JOIN sys.columns c 
			ON t.object_id = c.object_id
			AND sc.name + '.' + t.NAME = @SourceTableName
		) a 
	ORDER BY 
		column_id

	SET @sDataRecord = 
			(
			SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord))
			)

	--==========================================================================================================================================================================
	--SET HEADERCOMMAND
	--==========================================================================================================================================================================
	SET @SQLInsertHeaderCommand = ''
	IF @HasHeader = 1
	BEGIN
		SET @SQLInsertHeaderCommand = ' SELECT sDataRecord = ''' + @sDataRecord + ''',GetDate() ,''' + @SourceTableName + ''' ,0'
	END;
	
	--==========================================================================================================================================================================
	--SET DETAIL COMMAND
	--==========================================================================================================================================================================
    SET @sDataRecord = ''
    SET @SQLInsertDataCommand = ''

    SELECT @sDataRecord = @sDataRecord + NAME + ' + '
    FROM (
	   SELECT 
            CASE 
				WHEN system_type_id = 61 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + '],111) + '' '' + CONVERT(CHAR(10),[' + NAME + '],114))),'''') + ''|''' 
				WHEN system_type_id = 62 THEN 'ISNULL(RTRIM(LTRIM(str([' + NAME + ']))),'''') + ''|''' 
				--WHEN system_type_id = 62 THEN 'ISNULL(RTRIM(LTRIM(CONVERT(CHAR(10),[' + NAME + ']))),'')  + ''|''' 
				ELSE 'ISNULL(RTRIM(LTRIM([' + NAME + '])),'''') + ''|''' 
            END AS 'Name'
	        FROM (
		        SELECT CASE 
				        WHEN patindex('%[.]%', c.NAME) > 0
					        THEN SUBSTRING(c.NAME, patindex('%[.]%', c.NAME) + 1, LEN(c.NAME))
				        ELSE c.NAME
				        END AS 'Name'
                        , c.system_type_id
		        FROM sys.tables t
				JOIN sys.schemas sc
					ON sc.schema_id = t.schema_id
		        JOIN sys.columns c 
					ON t.object_id = c.object_id
			        AND sc.name + '.' + t.NAME = @SourceTableName
		    ) a
	    ) b

    SET @sDataRecord = (
		    SELECT SUBSTRING(@sDataRecord, 0, LEN(@sDataRecord) - 7)
		    )
    SET @SQLInsertDataCommand = ' SELECT sDataRecord =  ' + @sDataRecord + ',GetDate() ,''' + @SourceTableName + ''' ,0 FROM ' + @SourceTableName
	
	--==========================================================================================================================================================================
	--SET FOOTER COMMAND
	--==========================================================================================================================================================================
	SET @SQLInsertFooterCommand = ''
	IF @HasHeader = 1
	BEGIN
		SET @SQLInsertFooterCommand = ' SELECT COUNT(*) ,GetDate() , ''' + @SourceTableName + ''' ,0 FROM ' + @SourceTableName
	END;

	--==========================================================================================================================================================================
	--EXECUTE ALL SQL
	--==========================================================================================================================================================================
	EXEC (@SQLInsertHeaderCommand + @SQLInsertDataCommand + @SQLInsertFooterCommand);
	print (@SQLInsertHeaderCommand);
END


GO

exec [SKI_INT].[SSIS_Export_Data_Select]  'dbo.Policy','Test',1,0,'DEV'