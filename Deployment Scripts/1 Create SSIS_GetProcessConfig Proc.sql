
/****** Object:  StoredProcedure [ski_int].[SSIS_GetProcessConfig]    Script Date: 15/02/2018 6:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==============================================================================================================
--DESCRIPTION	:	THIS SCRIPT RETURNS THE CONFIG VALUES THAT IS REQUESTED FOR SUSPENSION EXECUTION
--DATE			:	2016-01-18	
--AUTHOR		:	MARIUS PRETORIUS	
--==============================================================================================================
CREATE PROCEDURE [ski_int].[SSIS_GetProcessConfig]
(
		@ProcessType VARCHAR(50)
		,@ProcessToExecute VARCHAR(255)
		,@FileName VARCHAR(50)
)
AS

BEGIN

	--==============================================================================================================
	--SAMPLE
	--==============================================================================================================
	--PT.Description				= 'Import'				@FileDirection
	--P.SSISDBCatalogPackageName	= 'DailyImport'			@ProcessToExecute
	--FL.FileName					= 'fcb.BW.lti.newbus'	@FileName

	--EXEC [ski_int].[SSIS_GetProcess] 'Import','Import Daily LTI File - BW','fcb.BW.lti.newbus'
	--==============================================================================================================
	--INSERT RELEVANT INFO FOR PROCESS INTO TEMP TABLE
	--==============================================================================================================
	SELECT DISTINCT 
		P.SSISDBCatalogPackageName
		,FL.FileName
		,FL.PickupLocation
		,FL.ProcessedLocation
		,FL.ArchiveLocation
		,P.SSISDBCatalogFolderName
		,P.SSISDBCatalogProjectName
		,FL.FileType FileTypeId
		,FT.Description FileTypeDescription
		,EmailProcessFailure.Email EmailProcessFailureEmailAddress
		,EmailProcessFailure.CommunicationTypeID EmailProcessFailureID
		,EmailProcessErrors.Email EmailProcessErrorsEmailAddress
		,EmailProcessErrors.CommunicationTypeID EmailProcessErrorsID
		,ISNULL(EmailProgress.Email,'') EmailProgressEmailAddress
		,EmailProgress.CommunicationTypeID EmailProgressID
		,ISNULL(EmailExceptions.Email,'') EmailExceptionsEmailAddress
		,EmailExceptions.CommunicationTypeID EmailExceptionsID
		,FL.FileHasHeaderRow FileHasHeaderRow
		,FL.FileHasFooterRow FileHasFooterRow
		,SQLCommand.Name SQLCommand
		,SKiImporterTable.Name SKiImporterTable
		,ASQueryName.Name ASQueryName
		,FlashImporterTableName.Name FlashImporterTableName
		,SKiImporterSPPName.Name SKiImporterSPPName
		,ExceptionsCommand.Name ExceptionsCommand
		,ExceptionsTableName.Name ExceptionsTableName
	INTO 
		#ProcessToExecute
	FROM
		[ski_int].[SSISProcess] P (NOLOCK)
		INNER JOIN [ski_int].[SSISProcessType] PT (NOLOCK) ON
			P.ProcessTypeID = PT.Id
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessFailure (NOLOCK) ON
			P.CommunicationId = EmailProcessFailure.CommunicationId
			AND EmailProcessFailure.CommunicationTypeID = 1
		LEFT JOIN [ski_int].[SSISCommunication] EmailProcessErrors (NOLOCK) ON
			P.CommunicationId = EmailProcessErrors.CommunicationId
			AND EmailProcessErrors.CommunicationTypeID = 2
		LEFT JOIN [ski_int].[SSISCommunication] EmailProgress (NOLOCK) ON
			P.CommunicationId = EmailProgress.CommunicationId
			AND EmailProgress.CommunicationTypeID = 3
		LEFT JOIN [ski_int].[SSISCommunication] EmailExceptions (NOLOCK) ON
			P.CommunicationId = EmailExceptions.CommunicationId
			AND EmailExceptions.CommunicationTypeID = 4
		INNER JOIN [ski_int].[SSISFileLocations] FL (NOLOCK) ON
			P.FileLocationId = FL.LocationId
		INNER JOIN [ski_int].[SSISFileType] FT (NOLOCK) ON
			FL.FileType = FT.Id


		LEFT JOIN [ski_int].[SSISScript] SQLCommand (NOLOCK) ON
			P.ScriptId = SQLCommand.ScriptId
			AND SQLCommand.ScriptTypeID = 1
		LEFT JOIN [ski_int].[SSISScript] SKiImporterTable (NOLOCK) ON
			P.ScriptId = SKiImporterTable.ScriptId
			AND SKiImporterTable.ScriptTypeID = 2
		LEFT JOIN [ski_int].[SSISScript] ASQueryName (NOLOCK) ON
			P.ScriptId = ASQueryName.ScriptId
			AND ASQueryName.ScriptTypeID = 3
		LEFT JOIN [ski_int].[SSISScript] FlashImporterTableName (NOLOCK) ON
			P.ScriptId = FlashImporterTableName.ScriptId
			AND FlashImporterTableName.ScriptTypeID = 4
		LEFT JOIN [ski_int].[SSISScript] SKiImporterSPPName (NOLOCK) ON
			P.ScriptId = SKiImporterSPPName.ScriptId
			AND SKiImporterSPPName.ScriptTypeID = 5
		LEFT JOIN [ski_int].[SSISScript] ExceptionsCommand (NOLOCK) ON
			P.ScriptId = ExceptionsCommand.ScriptId
			AND ExceptionsCommand.ScriptTypeID = 6
		LEFT JOIN [ski_int].[SSISScript] ExceptionsTableName (NOLOCK) ON
			P.ScriptId = ExceptionsTableName.ScriptId
			AND ExceptionsTableName.ScriptTypeID = 7
	WHERE 
		PT.Description = @ProcessType
		AND FL.FileName = @FileName
		AND P.Description = @ProcessToExecute
		AND P.Enabled = 1;

	--==============================================================================================================
	--GET RELEVANT INFO OF PROCESS TO EXECUTE
	--==============================================================================================================
	SELECT DISTINCT 
		ISNULL(SSISDBCatalogFolderName,'') SSISDBCatalogFolderName
		,ISNULL(SSISDBCatalogProjectName,'') SSISDBCatalogProjectName
		,ISNULL(SSISDBCatalogPackageName,'') SSISDBCatalogPackageName
		,ISNULL(FileName,'') FileName
		,ISNULL(PickupLocation,'') PickupLocation
		,ISNULL(ProcessedLocation,'') ProcessedLocation
		,ISNULL(ArchiveLocation,'') ArchiveLocation
		,ISNULL(FileTypeId,'') FileTypeId
		,ISNULL(FileTypeDescription,'') FileTypeDescription
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessFailure.EmailProcessFailureEmailAddress 
			FROM #ProcessToExecute EmailProcessFailure
			WHERE EmailProcessFailure.EmailProcessFailureID = RawData.EmailProcessFailureID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessFailure]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProcessErrors.EmailProcessErrorsEmailAddress 
			FROM #ProcessToExecute EmailProcessErrors
			WHERE EmailProcessErrors.EmailProcessErrorsID = RawData.EmailProcessErrorsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProcessErrors]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailProgress.EmailProgressEmailAddress 
			FROM #ProcessToExecute EmailProgress
			WHERE EmailProgress.EmailProgressID = RawData.EmailProgressID
			FOR XML PATH('')), 1, 1, ''),'') [EmailProgress]
		,ISNULL(STUFF((SELECT DISTINCT '; ' + EmailExceptions.EmailExceptionsEmailAddress 
			FROM #ProcessToExecute EmailExceptions
			WHERE EmailExceptions.EmailExceptionsID = RawData.EmailExceptionsID
			FOR XML PATH('')), 1, 1, ''),'') [EmailExceptions]
		,ISNULL(FileHasHeaderRow,0) FileHasHeaderRow
		,ISNULL(FileHasFooterRow,0) FileHasFooterRow
		,ISNULL(SQLCommand,'') SQLCommand
		,ISNULL(SKiImporterTable,'') SKiImporterTable
		,ISNULL(ASQueryName,'') ASQueryName
		,ISNULL(FlashImporterTableName,'') FlashImporterTableName
		,ISNULL(SKiImporterSPPName,'') SKiImporterSPPName
		,ISNULL(ExceptionsCommand,'') ExceptionsCommand
		,ISNULL(ExceptionsTableName,'') ExceptionsTableName
	FROM
		#ProcessToExecute RawData;

	--==============================================================================================================
	--DROP TEMP TABLE
	--==============================================================================================================
	DROP TABLE #ProcessToExecute;

END 








